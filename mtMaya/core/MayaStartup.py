import os
import json
import maya.cmds as cmds
import maya.mel as mel
#================================================


path = os.path.abspath(__file__)
path = os.path.normpath(path)
path = os.path.split(path)[0]
iconsFolder = os.path.join(path, "icons")


def BoolFromString(string):
        if string.lower() == "true":
                return True
        return False


class ShelfButton():
        @staticmethod
        def CreateShelf(shelfName):
                #create shelf if it doesn`t exists
                if not cmds.shelfLayout(shelfName, query = True, exists = True):
                        mel.eval("string $evString = `addNewShelfTab \"" + shelfName + "\"`;")


        @staticmethod
        #delete all buttons on specified shelf
        def DeleteShelfButtons(shelfName):
                shelfButtons = cmds.shelfLayout(shelfName, query = True, childArray = True)
                if shelfButtons:
                        for shelfButton in shelfButtons:
                                cmds.deleteUI(shelfButton)


        def __init__(self, annotation, annotationM=None, annotationD=None, i="greenLight_d", hi=None):
                self.icon = i
                self.highlightIcon = hi
                self.annotation = annotation
                self.icon = os.path.join(iconsFolder, i + ".png")
                self.mi = []

                if not self.highlightIcon:
                        self.highlightIcon = self.icon
                else:
                        self.highlightIcon = os.path.join(iconsFolder, hi + ".png")
                if annotationM or annotationD:
                        self.annotation = "Click             : " + self.annotation
                        if annotationM:
                                self.annotation = self.annotation + "\nMiddle Click  : " + annotationM
                        if annotationD:
                                self.annotation = self.annotation + "\nDouble Click : " + annotationD

                if not os.path.exists(self.icon):
                        raise RuntimeError("No Icon " + self.icon)

                if not os.path.exists(self.highlightIcon):
                        raise RuntimeError("No Icon " + self.highlightIcon)


        def OnClick(self, *args):
                pass


        def OnMiddleClick(self, *args):
                pass


        def OnDoubleClick(self, *args):
                pass


        def PutOnShelf(self, shelf="glat"):
            cmds.shelfButton(annotation = self.annotation, 
                i1 = self.icon, 
                hi = self.highlightIcon, 
                sourceType = "python", 
                command = self.OnClick, 
                dgc = self.OnMiddleClick, 
                doubleClickCommand = self.OnDoubleClick, 
                mi = self.mi, 
                parent = shelf)


        def PutOnShelfMenu(self, shelf="glat", menuItem="glat"):
            cmds.shelfButton(annotation = self.annotation, 
                i1 = self.icon, 
                hi = self.highlightIcon, 
                sourceType = "python", 
                dgc = self.OnMiddleClick, 
                doubleClickCommand = self.OnDoubleClick, 
                menuItem = [menuItem, self.OnClick],
                menuItemPython = 0,
                parent = shelf)


ShelfButton.CreateShelf("ISMaya")
ShelfButton.DeleteShelfButtons("ISMaya")


#-BUTTON----------------------------------------------------------------
command="""import test as test
reload(test)
test.Run()
"""

button = ShelfButton(annotation="Test Command", i="feather_d" , hi="feather_h")
button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import ExportGroup as exportGroup
exportGroup.ExportSelection()
"""        

button = ShelfButton(annotation="Export Selection", i="gear_arrow_green" , hi="gear_arrow_yellow")
button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import Utilities as utils
utils.FindMissingTextures()
"""

button = ShelfButton(annotation="Restore Textures", i="fussball" , hi="fussball")
button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import ExportGroup as export
export.CreateGroupForSelection()
"""

button = ShelfButton(annotation="Create Export Group For Selection", i="folder_plus" , hi="folder_plus")
button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import ExportGroup as exportGroup
exportGroup.CreateLayerForSelection()
"""

button = ShelfButton(annotation="Create Layer for selected Export Groups", i="layers" , hi="layers")
button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import Config as isConfig
isConfig.ShowWindow()
"""

button = ShelfButton(annotation="Config Window", i="book" , hi="book")
button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import Utilities as utils
reload(utils)
utils.ExtractSelection()
"""

button = ShelfButton(annotation="Config Window", i="watermelon3" , hi="watermelon3")
button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import maya.mel as mel
for p in range(100):
            mel.eval('PolySelectTraverse 1')
"""

button = ShelfButton(annotation="Config Window", i="octopus" , hi="octopus")
button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import Utilities as utils
reload(utils)
utils.CreateLocatorBySelection()
"""

button = ShelfButton(annotation="Config Window", i="locator" , hi="locator")
button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import MT.MT as mt
reload(mt)
mt.CreateTieLocator("point", "aim")
"""

button = ShelfButton(annotation="Railroad Tie Locator.\nuse locator with name \"aim\" as child of target object", 
                     i="railRoad_locator_green" , hi="railRoad_locator_yellow")
button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import MT.MT as mt
reload(mt)
mt.CreateTieEndLocatorByRef()
"""

button = ShelfButton(annotation="Copy target locator and rename it as 'end'\nSelect mesh and locator", 
                     i="railRoad_anchor_green" , hi="railRoad_anchor_yellow")
button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import MT.MT as mt
reload(mt)
mt.ScaleOld()
"""

button = ShelfButton(annotation="Scale old Trains",
                     i="check" , hi="check")
button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import MT.MT as mt
reload(mt)
mt.FlipPoints()
"""

button = ShelfButton(annotation="Flip Points", 
                     i="flipFlop_point" , hi="flipFlop_point")
button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import MT.MT as mt
reload(mt)
mt.FlipPoints2()
"""

button = ShelfButton(annotation="Flip Points2",
                     i="flipFlop_point" , hi="flipFlop_point")
button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import MT.MT as mt
reload(mt)
mt.RenamePoints()
"""

button = ShelfButton(annotation="Rename Points", 
                     i="hammer" , hi="hammer")
button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import MT.Palette as palette
reload(palette)
palette.ToLamberts()
"""

button = ShelfButton(annotation="To Lamberts", 
                     i="iceCream_arrowIn_green" , hi="iceCream_arrowIn_yellow")
button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import MT.Palette as palette
reload(palette)
palette.ToTexture()
"""

button = ShelfButton(annotation="Find on Texture", 
                     i="palette_arrowIn_green" , hi="palette_arrowIn_yellow")

button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import MT.Palette as palette
reload(palette)
palette.ToNewBakedTexture()
"""

button = ShelfButton(annotation="To New Texture", 
                     i="checkerColour_arrow_green" , hi="checkerColour_arrow_yellow")

button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import MT.Palette as palette
reload(palette)
palette.SelectionToCell()
"""

button = ShelfButton(annotation="Map selection to atlas cell", 
                     i="checkerColour_uv" , hi="checkerColour_uv")

button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------



#-BUTTON----------------------------------------------------------------
command="""import MT.MT as mt
reload(mt)
mt.TieLocatorBySubObject()
"""

button = ShelfButton(annotation="Selection to Locator.\nCreate locator between two selected objects or components", 
                     i="pinYellow" , hi="pinYellow")

button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import MT.MT as mt
reload(mt)
mt.LookAtNext()
"""

button = ShelfButton(annotation="Tie Locator. First selected object looks at second selected",
                     i="pinYellow" , hi="pinYellow")

button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------


#-BUTTON----------------------------------------------------------------
command="""import MT.MT as mt
reload(mt)
mt.ChainExportGroups()
"""

button = ShelfButton(annotation="Chain two export grous.\nFirst selected will be chained to end of second", 
                     i="chainYellow" , hi="chainYellow")

button.OnClick = command
button.PutOnShelf("ISMaya")
#-----------------------------------------------------------------------
""" use this to start in Maya
    import MayaStartup as ms
    reload(ms)
"""