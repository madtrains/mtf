import os
import re
import maya.cmds as cmds
import maya.mel as mel
import UnityAsset as uAsset
import Config as cfg
import UI.Window as window
import MayaTransform as mTransform


MATERIAL_TYPES = ['lambert', 'blinn']
MATERIAL_CONNECTIONS = ['color', 'ambientColor']
DEFAULT_TEXTURE_EXTENSION = 'tga'


def FindMissingTextures():
    config = cfg.Config()
    if not config.IsValid:
        window.ISError("Config is not valid. Please setup config")
        return

    missingTextures = []

    for materialType in MATERIAL_TYPES:
        for material in cmds.ls(type=materialType):
            materialHasMissingTextures = False
            for connectionName in MATERIAL_CONNECTIONS:
                connections = cmds.listConnections('{0}.{1}'.format(material, connectionName))
                if connections:
                    for connect in connections:
                        path = cmds.getAttr('{0}.fileTextureName'.format(connect))
                        if not os.path.exists(path):
                            missingTextures.append(connect)

    if len(missingTextures) > 0:
        for missingTexture in missingTextures:
            path = cmds.getAttr('{0}.fileTextureName'.format(missingTexture))
            basename = os.path.basename(path)
            unityAssets = uAsset.UnityAsset.Walk(config.unityAssets, extension=DEFAULT_TEXTURE_EXTENSION)
            for png in unityAssets:
                name = basename.split(".")
                if len(name) <= 1:
                    continue
                name = name[0]
                if name == png.name:
                    cmds.setAttr('{0}.fileTextureName'.format(missingTexture), png.path, type='string')
                    print('Texture restored: {0}\n{1}'.format(basename, png.path))


def ExtractSelection():
    def SelectionToNumbers():
        objectName = ""
        numbers = []
        selection = cmds.ls(sl=True, fl=True, l=True)
        for sel in selection:
            reg = re.findall(r"\.f\[\d+\]", sel, re.IGNORECASE)
            if reg:
                number = reg[0][3:-1]
                numbers.append(number)
                objectName = sel.split(".f[")[0]
        return objectName, numbers

    def clearFaces(longName, numbers):
        result = []
        for n in numbers:
            result.append(longName + ".f[" + n + "]")
        cmds.delete(result)

    objectName, active = SelectionToNumbers()
    cmds.select(objectName + ".f[*]", tgl=True)
    objectName, inactive = SelectionToNumbers()
    m1 = mTransform.MayaTransform(objectName)
    m2 = m1.Duplicate()
    clearFaces(m1.LongName, active)
    clearFaces(m2.LongName, inactive)


def CreateLocatorBySelection():
    selection = cmds.ls(sl=True, l=True)
    cmds.select(cl=True)
    for s in selection:
        m = mTransform.MayaTransform(s)
        locName = m.Name + "_lc"
        if ":" in m.Name:
            locName = m.Name.split(":")[1]
        loc = cmds.spaceLocator(name=locName, p=(0, 0, 0))
        ml = mTransform.MayaTransform(loc)
        ml.SetAttr("translateX", m.WorldPosition[0])
        ml.SetAttr("translateY", m.WorldPosition[1])
        ml.SetAttr("translateZ", m.WorldPosition[2])
        ml.SetAttr("rotateX", m.GetAttr("rotateX"))
        ml.SetAttr("rotateY", m.GetAttr("rotateY"))
        ml.SetAttr("rotateZ", m.GetAttr("rotateZ"))
