import os
import re
import maya.cmds as cmds
import maya.mel as mel
import UnityAsset as uAsset
import Config as cfg
import UI.Window as window
import MayaTransform as mTransform

reload(mTransform)

def GetCheckSelectionAlert():
    selection = cmds.ls(sl=True, l=True)
    if len(selection) < 1:
        window.ISError("Select al least one object")
        return None
    return selection

#for shelf button
def ExportSelection():
    selection = GetCheckSelectionAlert()
    if (selection is None):
        return

    for sel in selection:
        transform = mTransform.MayaTransform(sel)
        if not ExportGroup.IsCorrectExportGroupName(transform.Name):
            window.ISError("No __export ending in group name: {0}".format(transform.LongName))
            continue
       
        exportGroup = ExportGroup(sel)
        exportGroup.Export()
            

#for shelf button
def CreateGroupForSelection():
    selection = GetCheckSelectionAlert()
    if (selection is None):
        return

    #collect transforms
    mTransforms = []
    for node in selection:
        newMTransform = mTransform.MayaTransform(node)
        mTransforms.append(newMTransform)
           
    #check transforms
    checkedTransforms = []
    for transform in mTransforms:
        if ExportGroup.IsCorrectExportGroupName(transform.Name):
            window.ISError("{0} is already an Export Group".format(transform.Name))
        elif not transform.Type == "mesh" and not transform.Type == "joint":
            window.ISError("{0} is not a mesh".format(transform.Name))
        else:
            checkedTransforms.append(transform)

    if len(checkedTransforms) == 0:
        return None

    #create group
    assetName = checkedTransforms[0].Name
    message = "Use {0} as Asset Name?".format(assetName)
    if not window.YesNoWindow("ExportGroup", message, buttons=["Use", "New Name"]):
        assetName = window.TypeInWindow("Enter Asset Name")

    groupName = "{0}__export".format(assetName)
    group = cmds.group(name=groupName, empty=True)
    group = ExportGroup(group)

    for cTransform in checkedTransforms:
        cTransform.Parent = group

    return group
    

#for shelf button
def CreateLayerForSelection():
    selection = GetCheckSelectionAlert()
    if (selection is None):
        return

    for sel in selection:
        transform = mTransform.MayaTransform(sel)
        if not ExportGroup.IsCorrectExportGroupName(transform.Name):
            window.ISError("No __export ending in group name: {0}".format(node))
            continue
        
    exportGroup = ExportGroup(sel)
    exportGroup.CreateLayer()


class ExportGroup(mTransform.MayaTransform):
    """A Maya group, based on MayaTransform group and is used for export
    Name pattern of ExportGroup should be:
    assetName__export
    """
    @staticmethod
    def IsCorrectExportGroupName(name):
        """Is present name correct for ExportGroup?
        Arguments:
            name {str} - name for comparison
        Returns:
            [bool] - Is present name correct/incorrect for ExportGroup
        """
        if re.match(".+__export$", name, re.IGNORECASE):
            return True
        return False


    def __init__(self, node):
        if not ExportGroup.IsCorrectExportGroupName(node):
            cmds.error("No __export ending in group name: {0}".format(node))
            return None
        super(ExportGroup, self).__init__(node)
        self._objects = []
        self._tx = 0.0
        self._ty = 0.0
        self._tz = 0.0
        self._rx = 0.0
        self._ry = 0.0
        self._rz = 0.0
    

    @property
    def AssetName(self):
        """Name of Asset
        Returns:
            [str] - Asset Name
        """
        split = self.Name.split("__")
        return split[0]


    @AssetName.setter
    def AssetName(self, value):
        """Renames ExportGroup by Asset Name
        Arguments:
            {str} - Asset`s new name
        """
        self.Rename('{0}__export'.format(value))


    @property
    def LayerName(self):
        layerName = self.AssetName + "_layer"
        return layerName


    def CreateLayer(self):
        allLayers = cmds.ls(type="displayLayer")
        if not self.LayerName in allLayers:
            newLayer = cmds.createDisplayLayer(name=self.LayerName, empty=True)
            self.SetLayer(newLayer)


    def ShowOnlyMyLayer(self):
        allLayers = cmds.ls(type="displayLayer")
        for layer in allLayers:
            if re.match(".+_layer$", layer):
                if not layer == self.LayerName:
                    cmds.setAttr("{0}.visibility {1}".format(layer, 0))
                else:
                    cmds.setAttr("{0}.visibility {1}".format(self.LayerName, 1))
                

    def Unpack(self):
        self._tx = self.GetAttr("translateX")
        self._ty = self.GetAttr("translateY")
        self._tz = self.GetAttr("translateZ")

        self._rx = self.GetAttr("rotateX")
        self._ry = self.GetAttr("rotateY")
        self._rz = self.GetAttr("rotateZ")

        self.SetAttr("translateX", 0)
        self.SetAttr("translateY", 0)
        self.SetAttr("translateZ", 0)

        self.SetAttr("rotateX", 0)
        self.SetAttr("rotateY", 0)
        self.SetAttr("rotateZ", 0)
        
        for obj in self._objects:
            obj.Unparent()


    def Pack(self):
        for obj in self._objects:
            obj.Parent = self.LongName

        self.SetAttr("translateX", self._tx)
        self.SetAttr("translateY", self._ty)
        self.SetAttr("translateZ", self._tz)

        self.SetAttr("rotateX", self._rx)
        self.SetAttr("rotateY", self._ry)
        self.SetAttr("rotateZ", self._rz)
        

    def SelectContent(self):
        self._objects[0].Select()
        self.Log("selected: {0}".format(self._objects[0].Name))
        if len(self._objects) > 1:
            for i in range(len(self._objects)):
                self._objects[i].Select(add=True)
                self.Log("selected: {0}".format(self._objects[i].Name))
        count = len(cmds.ls(sl=True, l=True, type="transform"))
        self.Log("Selected: {0}".format(count))
            

    def Export(self, silent=False):
        config = cfg.Config()
        if not config.IsValid:
            window.ISError("Config is not valid. Please setup config")
            return

        self._objects = self.Children
        unityAssets = uAsset.UnityAsset.Walk(config.unityAssets, extension="fbx")
        for asset in unityAssets:
            if asset.name == self.AssetName:
                self.Unpack()
                self.SelectContent()
                errorMessage = ""
                try:
                    self.ExportCommand(asset.path)
                except Exception as ex:
                    errorMessage = "Can not export group {0} {1}".format(self.AssetName, ex)
                    self.Log(errorMessage, False)
                    if silent:
                        pass
                    else:
                        window.OkWindow(errorMessage)

                self.Pack()
                if not silent and not errorMessage:
                    window.OkWindow("export", "Exported: {0}".format(asset.path))
                return

        #no asset found 
        if not silent:
            window.OkWindow("export", "No path found for: {0}".format(self.AssetName))
        self.Select()
    

    def ExportCommand(self, path):
        mel.eval('file -force -options "v=0;" -typ "FBX export" -pr -es "{0}"'.format(path))


    def Log(self, message, error=False):
        if not error:
            print ("{0}: {1}".format("Export", message))
        else:
            cmds.error("{0}: {1}".format("Export", message))