import math
import maya.cmds as cmds
import maya.mel as mel
import maya.OpenMaya as om
import re


class MayaTransform(object):
    #region statica
    @staticmethod
    def Scan():
        """Scans Scene for Transforms to search
        Returns:
            [list] - list of Transforms, which were found
        """
        result = []
        for node in cmds.ls(type="transform"):
            mt = MayaTransform(node)
            result.append(mt)
        return result

    
    @staticmethod
    def FindNew(oldScan):
        """Scans Scene for new Transforms. It uses old scan and then compares it to new scan
        Can be useful to search imported nodes. Make scan, then import, than findNew
        Arguments:
            oldScan {list} - previous scan
        Returns:
            [list] - list of new Transforms
        """
        new = []
        newScan = MayaTransform.Scan()
        for n in newScan:
            if not n in oldScan:
                new.append(n)
        return new


    @staticmethod
    def FindNameInScan(oldScan, name, longName=False):
        """Scans found Transforms and returns first found. By short or long name
        Arguments:
            longName {bool} - should we use long name for comparison
        Returns:
            [MayaTransform/None] - found MayaTransform or None
        """
        for mayaTransform in oldScan:
            if longName:
                if mayaTransform.longname == name:
                    return mayaTransform
            else:
                if mayaTransform.name == name:
                    return mayaTransform
        return None
        

    
    @staticmethod
    def GetShortName(string):
        """Extracts short node name from long or short node name
        Arguments:
            string {str} - node full or short name
        Returns:
            [str] - short name of node
        """
        if re.match("^\|", string):
            string = string[1:]
        names = string.split("|")
        string = names[-1]
        return string


    @staticmethod
    def GetLongName(node):
        """Method to get long node name from short or long node name by maya ls command
        Arguments:
            node {str} - node short or long name
        Returns:
            [str] - long name of node
        """
        long = cmds.ls(node, long=True, type="transform")
        if long and len(long) > 0:
            return long[0]
        return ""


    @staticmethod
    def Size(bbi):
        """Get size of bbi (Boundig Box Invisible, which is returned by cmds.xform)
        Arguments:
            bbi {list} - bbi (Boundig Box Invisible, from xform)
        Returns:
            [tuple] - Size of bbi
        """
        x = bbi[3] - bbi[0]
        y = bbi[4] - bbi[1]
        z = bbi[5] - bbi[2]
        return (x, y, z)


    @staticmethod
    def Center(bbi):
        """Get center of bbi (Boundig Box Invisible, which is returned by cmds.xform)
        Arguments:
            bbi {list} - bbi (Boundig Box Invisible, from xform)
        Returns:
            [tuple] - Center of bbi
        """
        size = MayaTransform.Size(bbi)
        x = size[0]/2 + bbi[0]
        y = size[1]/2 + bbi[1]
        z = size[2]/2 + bbi[2]
        return (x, y, z)


    @staticmethod
    def Lerp(a, b, t):
        """Lerps between a and b by t
        Arguments:
            a - value for 0
            b - value for 1
            t {float} - interpolation coefficient
        Returns:
            Lerped value
        """
        a = float(a)
        b = float(b)
        t = float(t)
        value = (t * b) + ((1-t) * a)
        return value


    @staticmethod
    def InverseLerp(a, b, value):
        """Inverse Lerps between a and b by t
        Arguments:
            a - value for 0
            b - value for 1
            t {float} - interpolation coefficient
        Returns:
            Lerped value
        """
        a = float(a)
        b = float(b)
        value = float(value)
        halfA = value - a
        halfB = b-a
        return (halfA / halfB)
    #endregion


    #region Default
    def __init__(self, node):
        self._name = ''
        self._longName = ''
        self._longName = MayaTransform.GetLongName(node)
        if self._longName:
            self._name = MayaTransform.GetShortName(self._longName)


    def __eq__(self, other):
        if other is None:
            return False
        
        if isinstance(other, MayaTransform):
            if self._longName == other._longName:
                return True
        return False


    def __bool__(self):
        return self.Exists
    #endregion


    #region Properties
    @property
    def Name(self):
        """Name of Transform
        Returns:
            [str] - Name of Transform
        """
        return self._name


    @Name.setter
    def Name(self, value):
        """Renames Transform
        Arguments:
            {str} - Transform`s new name
        """
        self.Rename(value)


    @property
    def LongName(self):
        """Long Name of Transform
        Returns:
            [str] - Long Name i.e. path of Transform
        """
        return self._longName
        

    @property
    def Exists(self):
        """Does Transform exist in scene? False for 'dead' nodes
        Returns:
            [bool] - Does node exist in scene or not
        """
        result = cmds.ls(self._longName)
        if result and len(result) > 0:
            return True
        return False


    @property
    def Parent(self):
        """Parent of Transform
        Returns:
            [MayaTransform] - Parent of Transform as MayaTransform
        """
        if self.Exists:
            listOfRelatives = cmds.listRelatives(self.LongName, p=True, type='transform', f=True)
            if listOfRelatives > 0:
                return MayaTransform(listOfRelatives[0])
        return None


    @Parent.setter
    def Parent(self, value):
        """Set Transform`s Parent
        Arguments:
            {str / MayaTransform} - Target parent
        """
        if isinstance(value, MayaTransform):
            value = value.LongName
        try:
            result = cmds.parent(self._longName, value)
            temp = MayaTransform(result[0])
            self._name = temp.Name
            self._longName = temp.LongName
        except Exception, ex:
            cmds.warning('Can not parent {0} to {1}. Exception : {2}'.format(self._longName, value, ex))


    @property
    def Shapes(self):
        shapes = cmds.listRelatives(self.LongName, shapes=True, fullPath=True)
        if shapes:
            return shapes
        return []


    @property
    def Children(self):
        """Get children of Transform or children and grand-children, if allDescendents is True
        Returns:
            [list{MayaTransform}] - list of children as MayaTransform
        """
        return self.GetChildren()


    @property 
    def Type(self):
        nodeType = cmds.nodeType(self.LongName)
        if nodeType == 'transform':
            for shape in self.Shapes:
                shapeNodeType = cmds.nodeType(shape)
                return shapeNodeType
        return nodeType


    @property
    def WorldPivot(self):
        """Transform`s world pivot
        Returns:
            [tuple] - world pivot coordinates
        """
        xf = cmds.xform(self._longName, ws=True, q=True, rp=True)
        return (xf[0], xf[1], xf[2])


    @property
    def WorldPosition(self):
        """Transform`s world position
        Returns:
            [list] - world position coordinates
        """
        return cmds.xform(self._longName, translation=True, ws=True, q=True)


    @property
    def Bbi(self):
        """Transform`s Bbi (Bounding box invisible) from xform
        This includes the bounding boxes of all invisible children which are not included using the boundingBox flag.
        Returns:
            [list] - in following order: xmin ymin zmin xmax ymax zmax
        """
        return cmds.xform(self._longName, bbi=True, q=True, ws=True)


    @property
    def XMin(self):
        """Transform`s Bbi X Minimal Value
        Returns:
            [float] - X Minimal
        """
        return self.Bbi[0]


    @property
    def YMin(self):
        """Transform`s Bbi Y Minimal Value
        Returns:
            [float] - Y Minimal
        """
        return self.Bbi[1]


    @property
    def zMin(self):
        """Transform`s Bbi Z Minimal Value
        Returns:
            [float] - Z Minimal
        """
        return self.Bbi[2]


    @property
    def XMax(self):
        """Transform`s Bbi X Maximal Value
        Returns:
            [float] - X Maximal
        """
        return self.Bbi[3]


    @property
    def YMax(self):
        """Transform`s Bbi Y Maximal Value
        Returns:
            [float] - Y Maximal
        """
        return self.Bbi[4]


    @property
    def ZMax(self):
        """Transform`s Bbi Z Maximal Value
        Returns:
            [float] - Z Maximal
        """
        return self.Bbi[5]


    @property
    def BbiSize(self):
        """Get size of bbi (Boundig Box Invisible, which is returned by cmds.xform)
        Returns:
            [tuple] - Size of bbi
        """
        return MayaTransform.Size(self.Bbi)


    @property
    def BbiCenter(self):
        """Get center of bbi (Boundig Box Invisible, which is returned by cmds.xform)
        Returns:
            [tuple] - Center of bbi
        """
        return MayaTransform.Center(self.Bbi)


    @property
    def Visibility(self):
        """Is Visibility enabled
        Returns:
            [bool] - Visibility state
        """
        return cmds.getAttr(self._longName + '.visibility')


    @Visibility.setter
    def Visibility(self, value):
        """Set Transform`s visibility
        Arguments:
            {bool} - Visibility value
        """
        cmds.setAttr(self._longName + '.visibility', value)
    #endregion


    #region Methods
    def Select(self, add=False):
        """Selects Transform if it exists
        Arguments:
            add {bool} - Additive Selection
        """
        if self.Exists:
            cmds.select(self._longName, add=add)
            


    def Delete(self):
        """Deletes Transform if it exists
        """
        if self.Exists:
            cmds.delete(self._longName)


    def Rename(self, newName):
        """Renames Transform
        Arguments:
            newName {str} - New name
        """
        result = cmds.rename(self.LongName, newName)
        resultTransform = MayaTransform(result)
        self._name = resultTransform.Name
        self._longName = resultTransform.LongName


    def Unparent(self):
        """Parents Transform to world
        """
        if self.Parent:
            result = cmds.parent(self._longName, w=True)
            temp = MayaTransform(result[0])
            self._name = temp.Name
            self._longName = temp.LongName


    def Duplicate(self, rr=True):
        """Duplicates Transform it it exists
        Arguments:
            rr {bool} - return only the root nodes of the new hierarchy or not
        Returns:
            [MayaTransform] - New MayaTransform
        """
        if self.Exists:
            copy = cmds.duplicate(self._longName, rr=rr)
            if copy:
                result = MayaTransform(copy[0])
                return result
            else:
                cmds.error("Duplicate failed")
        return None

    
    def CenterPivot(self):
        """Set pivot points to the center of the Transform's bounding box
        """
        cmds.xform(self._longName, cpc=True)


    def ResetTransform(self):
        """Reset Transform`s attributes
        """
        cmds.makeIdentity(self._longName, apply=False, t=True, r=True, s=True, n=True, pn=True)


    def FreezeTransform(self, t=True, r=True, s=True, n=True, pn=True):
        """Freeze Transform`s attributes
            t {bool} - 	If this flag is true, only the translation is applied to the shape.
                    The translation will be changed to 0, 0, 0. If neither translate nor rotate nor scale flags are specified,
                    then all (t, r, s) are applied. (Note: the translate flag is not meaningful when applied to joints,
                    since joints are made to preserve their world space position. This flag will have no effect on joints.)
            r {bool} - 	If this flag is true, only the rotation is applied to the shape.
                    The rotation will be changed to 0, 0, 0. If neither translate nor rotate nor scale flags 
                    are specified, then all (t, r, s) are applied.
            s {bool} - 	If this flag is true, only the scale is applied to the shape.
                    The scale factor will be changed to 1, 1, 1. If neither translate nor rotate nor scale flags
                    are specified, then all (t, r, s) are applied.
            n {bool} - 	If this flag is set to 1, the normals on polygonal objects will be frozen. This flag is valid
                    only when the -apply flag is on. If this flag is set to 2, the normals on polygonal objects will
                    be frozen only if its a non-rigid transformation matrix. ie, a transformation that does not 
                    contain shear, skew or non-proportional scaling. The default behaviour is not to freeze normals.
        """
        cmds.makeIdentity(self._longName, apply=True, t=t, r=r, s=s, n=n, pn=pn)


    def GetAttr(self, attr):
        """Returns value of Transform`s target attribute
        Arguments:
            attr {str} - name of target attribute
        """
        return cmds.getAttr('{0}.{1}'.format(self._longName, attr))


    def SetAttr(self, attr, value):
        """Sets value of Transform`s target attribute
        Arguments:
            attr {str} - name of taret attribute
        Returns:
            Value of target attribute
        """
        cmds.setAttr('{0}.{1}'.format(self._longName, attr), value)


    def LockAttr(self, attr, value):
        """Locks/Unlocks Transform`s target attributes
        Arguments:
            attr {} - return only the root nodes of the new hierarchy or not
        Returns:
            [MayaTransform] - New MayaTransform
        """
        cmds.setAttr('{0}.{1}'.format(self._longName, attr), lock=value)


    @property
    def UVShells(self):
        selList = om.MSelectionList()
        selList.add(self.LongName)
        selListIter = om.MItSelectionList(selList, om.MFn.kMesh)
        pathToShape = om.MDagPath()
        selListIter.getDagPath(pathToShape)
        meshNode = pathToShape.fullPathName()
        uvSets = cmds.polyUVSet(meshNode, query=True, allUVSets =True)
        allSets = []
        for uvset in uvSets:
            shapeFn = om.MFnMesh(pathToShape)
            shells = om.MScriptUtil()
            shells.createFromInt(0)
            # shellsPtr = shells.asUintPtr()
            nbUvShells = shells.asUintPtr()
 
            uArray = om.MFloatArray()   #array for U coords
            vArray = om.MFloatArray()   #array for V coords
            uvShellIds = om.MIntArray() #The container for the uv shell Ids

            shapeFn.getUVs(uArray, vArray)
            shapeFn.getUvShellsIds(uvShellIds, nbUvShells, uvset)
 
            # shellCount = shells.getUint(shellsPtr)
            shells = {}
            for i, n in enumerate(uvShellIds):
                if n in shells:
			        # shells[n].append([uArray[i],vArray[i]])
                    shells[n].append( '%s.map[%i]' % ( self.LongName, i ) )
                else:
			        # shells[n] = [[uArray[i],vArray[i]]]
                    shells[n] = [ '%s.map[%i]' % ( self.LongName, i ) ]
            allSets.append({uvset: shells})
        return allSets


    def AssignLambert1(self):
        """Assgins Lambert1 to Transform
        """
        cmds.sets(self._longName, e=True, forceElement='initialShadingGroup')


    def GetChildren(self, allDescendents=False):
        """Get children of Transform or children and grand-children, if allDescendents is True
        Arguments:
            allDescendents {bool} - should method retur all grand-children
        Returns:
            [list{MayaTransform}] - list of children as MayaTransform
        """
        result = []
        children = cmds.listRelatives(self.LongName, ad=allDescendents,  children=True, f=True, type="transform")
        if children:
            for child in children:
                mt = MayaTransform(child)
                result.append(mt)
        return result


    def SetLayer(self, layer):
        cmds.editDisplayLayerMembers(layer, self.LongName,  noRecurse=True)


    def ExtractByCopying(self, facesArray):
        faceNumber = []
        for face in facesArray:
            result = re.match(".f\[\d+]")
            if result:
               number = re.match("\d")
               if number:
                   numberString = number.group(0)
                   faceNumber.append(numberString)
        print(faceNumber)
    #endregion