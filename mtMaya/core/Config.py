import os
import re
import ConfigParser
import tempfile
import UI.WindowSimple as windowSimple
import UI.Window as window
import maya.cmds as cmds

LOCAL_APPDATA = os.getenv("LOCALAPPDATA")
CONFIG_FOLDER = os.path.join(LOCAL_APPDATA, "ISMaya")
CONFIG_FILE = os.path.join(CONFIG_FOLDER, "config.cfg").replace("\\", "/")

reload(windowSimple)
reload(window)


def ShowWindow():
    configWindow = ConfigWindow()
    configWindow.Show()

        
class ConfigWindow(windowSimple.WindowSimple):
    def __init__(self):
        super(ConfigWindow, self).__init__(False, "configWindow", "Config Window")
        cmds.button(label='Set', parent=self.Layout, c=self.OnButtonSet, w=self.Width - 2)
        self.textName = cmds.text(label="", parent=self.Layout)
        self.textAssetsPath = cmds.text(label="", parent=self.Layout)
        self.config = Config()
        self.SetUIByConfig()


    def SetUIByConfig(self):
        self.Name = "Project: " + self.config.name
        self.AssetsPath = "Assets Folder:" + self.config.unityAssets


    @property
    def Name(self):
        return cmds.text(self.textName, label=True, q=True)
   

    @Name.setter
    def Name(self, value):
        cmds.text(self.textName, e=True, label=value)


    @property
    def AssetsPath(self):
        return cmds.text(self.textAssetsPath, label=True, q=True)
   

    @AssetsPath.setter
    def AssetsPath(self, value):
        cmds.text(self.textAssetsPath, e=True, label=value)


    def OnButtonSet(self, *args):
        path = cmds.fileDialog2(fm=3, cap='Choose Folder', hne=True)
        if isinstance(path, list):
            path = path[0]

        if not re.match(".+Assets$", path, re.IGNORECASE):
            window.ISError("Please Select Unity Assets folder")
            return

     
        last = os.path.split(path)[0]
        last = os.path.split(last)[1]
        name = window.TypeInWindow("ProjectName", text=last)
        self.config.unityAssets = path
        self.config.name = name
        self.SetUIByConfig()
        self.config.Save()
      

class Config(object):
    def __init__(self):
        self.name = ""
        self.unityAssets = ""
        self.Load()


    def IsValid(self):
        if self.name == "" or self.unityAssets == "":
            return False
        return True


    def Load(self):
        if not os.path.exists(CONFIG_FILE):
            return

        try:
            config = ConfigParser.ConfigParser()
            if not config.read(CONFIG_FILE):    
                return

        except Exception as ex:
            window.ISError('Can not read config file: {0}\n{0}'.format(CONFIG_FILE, ex))
            return
            

        if config.has_option('project', 'name'):
                self.name = config.get('project', 'name')

        if config.has_option('project', 'unityAssets'):
                self.unityAssets = config.get('project', 'unityAssets')


    def Save(self):
        if not self.IsValid:
            window.ISError("Can not save Invalid Project. Name or UnityAssets are empty")
            return

        config = ConfigParser.ConfigParser()
        config.add_section('project')
        config.set('project', 'name', self.name)
        config.set('project', 'unityAssets', self.unityAssets)

        if not os.path.exists(CONFIG_FOLDER):
            os.makedirs(CONFIG_FOLDER)

        try:
            with open(CONFIG_FILE, 'wb') as configFile:
                config.write(configFile)

        except Exception as ex:
            window.ISError("Can not write config file: {0}\n{1}".format(CONFIG_FILE, ex))


