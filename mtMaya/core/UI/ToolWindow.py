import maya.cmds as cmds
import maya.mel as mel
import re


#window with nice buttons and filter
class ToolWindow(object):
        def __init__(self, windowName="toolSet", title="toolSet", itemLabel="Tools", mainButtonIcon="greenLight_d", mainCommand = ""):
                self.windowName = windowName
                self.title = title
                self.itemLabel = itemLabel
                self.elements = []
                self.elementsFiltered = []
                
                self.window = None
                self.mainLayout = None
                self.toolLayout = None
                self.fl = None
                self.contentLayout = None
                self.filterTextField = None
                self.mainButtonIcon = mainButtonIcon
                self.mainCommand = mainCommand
                self.CreateWindow()
                

        def CreateContent(self):
                pass
        

        def CreateButton(self, name, icon, command, annotation=""):
                button = ToolButton(self, name, command, icon, annotation)
                """
                        iconPath = path.PATHSTORAGE.GetIcon(icon)
                        cmds.iconTextButton(style = "iconAndTextHorizontal",
                                            i1 = iconPath,
                                            label = name,
                                            parent = self.contentLayout,
                                            command = command,
                                            highlightImage =  iconPath,
                                            annotation = annotation)
                                            """
                self.Update()
        

        def CreateWindow(self):
                if cmds.window(self.windowName, exists=True):
                        cmds.deleteUI(self.windowName)

                self.window = cmds.window(self.windowName, title=self.title, resizeToFitChildren=False, sizeable=True)

                self.mainLayout = cmds.formLayout(numberOfDivisions=100, parent=self.windowName)
                self.CreateToolSet()
                self.CreateFrame()
                
                cmds.formLayout(self.mainLayout, edit=True,
                                attachForm=[(self.toolLayout, 'top', 2), (self.toolLayout, 'left', 2), (self.toolLayout, 'right', 2),
                                            (self.fl, 'top', 40), (self.fl, 'left', 2), (self.fl, 'right', 2), (self.fl, 'bottom', 2)])
                
                cmds.showWindow(self.windowName)
                self.CreateContent()
                self.Update()
                

        def CreateToolSet(self):
                self.toolLayout = cmds.rowLayout(numberOfColumns=8,
                                                         cw=((1, 34), (2, 50), (3, 80), (4, 16), (5, 16)),
                                                         adjustableColumn=3,
                                                         columnAlign=(1, 'right'),
                                                 columnAttach=[(1, 'both', 0), (2, 'both', 0), (3, 'both', 0), (4, 'both', 0), (5, 'both', 0)],
                                                 parent=self.mainLayout)
        
                cmds.symbolButton(image=path.PATHSTORAGE.GetIcon(self.mainButtonIcon), command=self.mainCommand)
                cmds.text(label="Filter")
                self.filterTextField = cmds.textField(cc=self.Update)
                cmds.symbolButton(image=path.PATHSTORAGE.GetIcon("small_delete"), command=self.Button_Cross)
                cmds.symbolButton(image=path.PATHSTORAGE.GetIcon("small_refresh"), command=self.Button_Refresh)
                

        def CreateFrame(self):
                self.fl = cmds.frameLayout(label=self.itemLabel, borderStyle='in', parent=self.mainLayout)
                self.contentLayout = cmds.scrollLayout(parent=self.fl, childResizable=True)                


        def Update(self, *args):
                text = cmds.textField(self.filterTextField, query=True, text=True)
                text = re.sub('^\s', '', text)
                text = re.sub('\s$', '', text)
                text = re.sub('\s',  '.*', text)
                text = '.*' + text + '.*'

                children = cmds.scrollLayout(self.contentLayout, query=True, childArray=True)
                if children:
                        for child in children:
                                cmds.deleteUI(child)

                self.elementsFiltered = []
                for element in self.elements:
                        if self.Match(text, element):
                                self.elementsFiltered.append(element)
                                element.Draw()
                              
                                  
        def Match(self, text, element):
                if re.match(text, element.name, re.IGNORECASE):
                        return True
                

        def Button_Cross(self, *args):
                cmds.textField(self.filterTextField, edit=True, text='')
                self.Update()


        def Button_Refresh(self, *args):
                self.Update()