import maya.cmds as cmds
import maya.mel as mel
     
#=====================================================================================================================================================================================
def YesNoWindow(title, message=None, buttons = ["Yes", "Cancel"]):
        if not message:
                message = title
        answer = cmds.confirmDialog(title = title, message = message, button = buttons, defaultButton = buttons[0], cancelButton = buttons[1], dismissString = buttons[1])
        if answer == buttons[0]:
                return True
        return False


def ChooseWindow(title, message, buttons = ["Yes", "Cancel"]):
        answer = cmds.confirmDialog(title = title, message = message, button = buttons, defaultButton = buttons[0], cancelButton = buttons[1], dismissString = buttons[1])
        return answer


def OkWindow(title, message=None, button = "OK", defaultButton = "OK", icon = "information"):
        #icon = "critical"
        #icon = "warning"
        if not message:
                message = title
        cmds.confirmDialog(message = message, title = title, button = button, defaultButton = defaultButton, icon = icon)


def ISError(message):
        OkWindow("Error", message, icon = "critical")
        raise RuntimeError(message)


def TypeInWindow(message, buttons=['OK', 'Cancel'], title='Enter Text', text=""):
    result = cmds.promptDialog(title=title, message=message, button=buttons, text=text,
                               defaultButton=buttons[0], cancelButton=buttons[1], dismissString='Cancel')
    if result == buttons[0]:
        text = cmds.promptDialog(query=True, text=True)
        return text
    return None


def GetFolderWindow(message):
    result = cmds.fileDialog2(fm=3, okc='Folder', cap=message)
    return result[0]


def Trace(message):
    print (message)
    mel.eval('trace -where(" ' + message + '")')