import maya.cmds as cmds
import maya.mel as mel


class ToolButton(object):
        def __init__(self, window, name, onClick, icon="bulb_green", annotation="", link = None):
                self.name = name
                self.link = link
                self.window = window
                self.icon = icon
                self.onClick = onClick
                self.annotation = annotation
                if not self in self.window.elements:
                        self.window.elements.append(self)
                        

        def Draw(self):
                iconPath = path.PATHSTORAGE.GetIcon(self.icon)
                cmds.iconTextButton(style = "iconAndTextHorizontal", i1 = iconPath, label = self.name, parent = self.window.contentLayout, command = self.onClick, highlightImage =  iconPath, annotation = self.annotation)