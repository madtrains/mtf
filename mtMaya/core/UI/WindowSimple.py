import maya.cmds as cmds
import maya.mel as mel
import UI.WindowBase as wBase


class WindowSimple(wBase.WindowBase):
        def __init__(self, sizeable=False, name="window", title="Window", width=400, height=400, retain=False, closeIfExists=True):
                super(WindowSimple, self).__init__(sizeable = sizeable, name=name, title=title, width=width, height=height, retain=retain, closeIfExists=closeIfExists)
                self.DeleteIf()
                        
                self.window = cmds.window(self.name, title=self.title, s=self.sizeable, retain=self.retain)
                self.mainLayout = cmds.columnLayout(parent=self.window)
              

        @property
        def Width(self):
                return cmds.columnLayout(self.mainLayout, query=True, width=True)


        @property
        def Height(self):
            return cmds.columnLayout(self.mainLayout, query=True, height=True)


        @Height.setter
        def Height(self, value):
            cmds.columnLayout(self.mainLayout, edit=True, height=value)


        @property
        def Layout(self):
            return self.mainLayout