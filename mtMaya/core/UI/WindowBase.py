import maya.cmds as cmds
import maya.mel as mel


class WindowBase(object):
        def __init__(self, sizeable=False, name="window", title="Window", width= 400, height = 300, retain=False, closeIfExists=True):
                self.retain        = retain         #do not destroy window when closing, re show it by cmds.showWindow(self.window)
                self.closeIfExists = closeIfExists
                self.sizeable      = sizeable
                self.name          = name
                self.title         = title
                self.width         = width
                self.height        = height
                
                self.window        = None
                self.mainLayout    = None


        def DeleteIf(self):
                if self.name and self.closeIfExists and cmds.window(self.name, query = True, exists = True):
                        cmds.deleteUI(self.name, window = True)


        def Show(self):
                cmds.showWindow(self.window)
                

        def Sizeable(self, value):
                cmds.window(self.name, edit=True, sizeable=value)


        @property
        def Title(self):
            return cmds.window(self.window, query=True, title=value)


        @Title.setter
        def Title(self, value):
            cmds.window(self.window, edit=True, title=value)