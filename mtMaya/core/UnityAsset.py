import os


class UnityAsset(object):
    """Packed filename, path, extension and path to meta file of Unity Asset file
    """
    #region Statica
    @staticmethod
    def Walk(path, extension=None):
        """Walks all folders relative and collects files as UnityPath
        Arguments:
            extension {str} - collect files only by defined extension
        Returns:
            [list] - list UnityPath
        """
        assets = []
        for root, dirs, files in os.walk(path, topdown=False):
            for name in files:
                split = name.split(".")
                currentExtension = split[-1]
                add = False

                if extension is None:
                    add = True
                else:
                    if currentExtension == extension:
                        add = True

                if add:
                    cleanName = split[0]
                    fullPath = os.path.join(root, name)
                    fullPath = fullPath.replace("\\", "/")
                    uAsset = UnityAsset(cleanName, extension, fullPath)
                    assets.append(uAsset)
        return assets
        #endregion


    def __init__(self, name, extension, path):
        self.name = name
        self.extension = extension
        self.path = path
        self.meta = None
        meta = self.path + ".meta"
        if os.path.exists(meta):
            self.meta = meta