import os
import sys
import imageio
import numpy
import cv2
import re
import math
import random

import maya.cmds as cmds
from MayaTransform import MayaTransform
import UI.Window as window

#region colorLibrary
COLOR_NAMES = []
COLOR_NAMES.append({"name" : "maroon", "r" : 128, "g" : 0, "b" : 0 })
COLOR_NAMES.append({"name" : "dark red", "r" : 139 , "g" : 0 , "b" : 0})
COLOR_NAMES.append({"name" : "brown", "r" : 165 , "g" : 42 , "b" : 42})
COLOR_NAMES.append({"name" : "firebrick", "r" : 178 , "g" : 34 , "b" : 34})
COLOR_NAMES.append({"name" : "crimson", "r" : 220 , "g" : 20 , "b" : 60})
COLOR_NAMES.append({"name" : "red", "r" : 255 , "g" : 0 , "b" : 0})
COLOR_NAMES.append({"name" : "tomato", "r" : 255 , "g" : 99 , "b" : 71})
COLOR_NAMES.append({"name" : "namecoral ", "r" : 255 , "g" : 127 , "b" : 80})
COLOR_NAMES.append({"name" : "indian red", "r" : 205 , "g" : 92 , "b" : 92})
COLOR_NAMES.append({"name" : "light coral", "r" : 240 , "g" : 128 , "b" : 128})
COLOR_NAMES.append({"name" : "dark salmon", "r" : 233 , "g" : 150 , "b" : 122})
COLOR_NAMES.append({"name" : "salmon", "r" : 250 , "g" : 128 , "b" : 114})
COLOR_NAMES.append({"name" : "light salmon", "r" : 255 , "g" : 160 , "b" : 122})
COLOR_NAMES.append({"name" : "orange red", "r" : 255 , "g" : 69 , "b" : 0})
COLOR_NAMES.append({"name" : "dark orange", "r" : 255 , "g" : 140 , "b" : 0})
COLOR_NAMES.append({"name" : "orange", "r" : 255 , "g" : 165 , "b" : 0})
COLOR_NAMES.append({"name" : "gold", "r" : 255 , "g" : 215 , "b" : 0})
COLOR_NAMES.append({"name" : "dark golden rod", "r" : 184 , "g" : 134 , "b" : 11})
COLOR_NAMES.append({"name" : "golden rod", "r" : 218 , "g" : 165 , "b" : 32})
COLOR_NAMES.append({"name" : "pale golden rod", "r" : 238 , "g" : 232 , "b" : 170})
COLOR_NAMES.append({"name" : "dark khaki", "r" : 189 , "g" : 183 , "b" : 107})
COLOR_NAMES.append({"name" : "khaki", "r" : 240 , "g" : 230 , "b" : 140})
COLOR_NAMES.append({"name" : "olive", "r" : 128 , "g" : 128 , "b" : 0})
COLOR_NAMES.append({"name" : "yellow", "r" : 255 , "g" : 255 , "b" : 0})
COLOR_NAMES.append({"name" : "yellow green", "r" : 154 , "g" : 205 , "b" : 50})
COLOR_NAMES.append({"name" : "dark olive green", "r" : 85 , "g" : 107 , "b" : 47})
COLOR_NAMES.append({"name" : "olive drab", "r" : 107 , "g" : 142 , "b" : 35})
COLOR_NAMES.append({"name" : "lawn green", "r" : 124 , "g" : 252 , "b" : 0})
COLOR_NAMES.append({"name" : "chart reuse", "r" : 127 , "g" : 255 , "b" : 0})
COLOR_NAMES.append({"name" : "green yellow", "r" : 173 , "g" : 255 , "b" : 47})
COLOR_NAMES.append({"name" : "dark green", "r" : 0 , "g" : 100 , "b" : 0})
COLOR_NAMES.append({"name" : "green", "r" : 0 , "g" : 128 , "b" : 0})
COLOR_NAMES.append({"name" : "forest green", "r" : 34 , "g" : 139 , "b" : 34})
COLOR_NAMES.append({"name" : "lime", "r" : 0 , "g" : 255 , "b" : 0})
COLOR_NAMES.append({"name" : "lime green", "r" : 50 , "g" : 205 , "b" : 50})
COLOR_NAMES.append({"name" : "light green", "r" : 144 , "g" : 238 , "b" : 144})
COLOR_NAMES.append({"name" : "pale green", "r" : 152 , "g" : 251 , "b" : 152})
COLOR_NAMES.append({"name" : "dark sea green", "r" : 143 , "g" : 188 , "b" : 143})
COLOR_NAMES.append({"name" : "medium spring green", "r" : 0 , "g" : 250 , "b" : 154})
COLOR_NAMES.append({"name" : "spring green", "r" : 0 , "g" : 255 , "b" : 127})
COLOR_NAMES.append({"name" : "sea green", "r" : 46 , "g" : 139 , "b" : 87})
COLOR_NAMES.append({"name" : "medium aqua marine", "r" : 102 , "g" : 205 , "b" : 170})
COLOR_NAMES.append({"name" : "medium sea green", "r" : 60 , "g" : 179 , "b" : 113})
COLOR_NAMES.append({"name" : "light sea green", "r" : 32 , "g" : 178 , "b" : 170})
COLOR_NAMES.append({"name" : "dark slate gray", "r" : 47 , "g" : 79 , "b" : 79})
COLOR_NAMES.append({"name" : "teal", "r" : 0 , "g" : 128 , "b" : 128})
COLOR_NAMES.append({"name" : "dark cyan", "r" : 0 , "g" : 139 , "b" : 139})
COLOR_NAMES.append({"name" : "aqua", "r" : 0 , "g" : 255 , "b" : 255})
COLOR_NAMES.append({"name" : "cyan", "r" : 0 , "g" : 255 , "b" : 255})
COLOR_NAMES.append({"name" : "light cyan", "r" : 224 , "g" : 255 , "b" : 255})
COLOR_NAMES.append({"name" : "dark turquoise", "r" : 0 , "g" : 206 , "b" : 209})
COLOR_NAMES.append({"name" : "turquoise", "r" : 64 , "g" : 224 , "b" : 208})
COLOR_NAMES.append({"name" : "medium turquoise", "r" : 72 , "g" : 209 , "b" : 204})
COLOR_NAMES.append({"name" : "pale turquoise", "r" : 175 , "g" : 238 , "b" : 238})
COLOR_NAMES.append({"name" : "aqua marine", "r" : 127 , "g" : 255 , "b" : 212})
COLOR_NAMES.append({"name" : "powder blue", "r" : 176 , "g" : 224 , "b" : 230})
COLOR_NAMES.append({"name" : "cadet blue", "r" : 95 , "g" : 158 , "b" : 160})
COLOR_NAMES.append({"name" : "steel blue", "r" : 70 , "g" : 130 , "b" : 180})
COLOR_NAMES.append({"name" : "corn flower blue", "r" : 100 , "g" : 149 , "b" : 237})
COLOR_NAMES.append({"name" : "deep sky blue", "r" : 0 , "g" : 191 , "b" : 255})
COLOR_NAMES.append({"name" : "dodger blue", "r" : 30 , "g" : 144 , "b" : 255})
COLOR_NAMES.append({"name" : "light blue", "r" : 173 , "g" : 216 , "b" : 230})
COLOR_NAMES.append({"name" : "sky blue", "r" : 135 , "g" : 206 , "b" : 235})
COLOR_NAMES.append({"name" : "light sky blue", "r" : 135 , "g" : 206 , "b" : 250})
COLOR_NAMES.append({"name" : "midnight blue", "r" : 25 , "g" : 25 , "b" : 112})
COLOR_NAMES.append({"name" : "navy", "r" : 0 , "g" : 0 , "b" : 128})
COLOR_NAMES.append({"name" : "dark blue", "r" : 0 , "g" : 0 , "b" : 139})
COLOR_NAMES.append({"name" : "medium blue", "r" : 0 , "g" : 0 , "b" : 205})
COLOR_NAMES.append({"name" : "blue", "r" : 0 , "g" : 0 , "b" : 255})
COLOR_NAMES.append({"name" : "royal blue", "r" : 65 , "g" : 105 , "b" : 225})
COLOR_NAMES.append({"name" : "blue violet", "r" : 138 , "g" : 43 , "b" : 226})
COLOR_NAMES.append({"name" : "indigo", "r" : 75 , "g" : 0 , "b" : 130})
COLOR_NAMES.append({"name" : "dark slate blue", "r" : 72 , "g" : 61 , "b" : 139})
COLOR_NAMES.append({"name" : "slate blue", "r" : 106 , "g" : 90 , "b" : 205})
COLOR_NAMES.append({"name" : "medium slate blue", "r" : 123 , "g" : 104 , "b" : 238})
COLOR_NAMES.append({"name" : "medium purple", "r" : 147 , "g" : 112 , "b" : 219})
COLOR_NAMES.append({"name" : "dark magenta", "r" : 139 , "g" : 0 , "b" : 139})
COLOR_NAMES.append({"name" : "dark violet", "r" : 148 , "g" : 0 , "b" : 211})
COLOR_NAMES.append({"name" : "dark orchid", "r" : 153 , "g" : 50 , "b" : 204})
COLOR_NAMES.append({"name" : "medium orchid", "r" : 186 , "g" : 85 , "b" : 211})
COLOR_NAMES.append({"name" : "purple", "r" : 128 , "g" : 0 , "b" : 128})
COLOR_NAMES.append({"name" : "thistle", "r" : 216 , "g" : 191 , "b" : 216})
COLOR_NAMES.append({"name" : "plum", "r" : 221 , "g" : 160 , "b" : 221})
COLOR_NAMES.append({"name" : "violet", "r" : 238 , "g" : 130 , "b" : 238})
COLOR_NAMES.append({"name" : "magenta", "r" : 255 , "g" : 0 , "b" : 255})
COLOR_NAMES.append({"name" : "orchid", "r" : 218 , "g" : 112 , "b" : 214})
COLOR_NAMES.append({"name" : "medium violet red", "r" : 199 , "g" : 21 , "b" : 133})
COLOR_NAMES.append({"name" : "pale violet red", "r" : 219 , "g" : 112 , "b" : 147})
COLOR_NAMES.append({"name" : "deep pink", "r" : 255 , "g" : 20 , "b" : 147})
COLOR_NAMES.append({"name" : "hot pink", "r" : 255 , "g" : 105 , "b" : 180})
COLOR_NAMES.append({"name" : "light pink", "r" : 255 , "g" : 182 , "b" : 193})
COLOR_NAMES.append({"name" : "pink", "r" : 255 , "g" : 192 , "b" : 203})
COLOR_NAMES.append({"name" : "antique white", "r" : 250 , "g" : 235 , "b" : 215})
COLOR_NAMES.append({"name" : "beige", "r" : 245 , "g" : 245 , "b" : 220})
COLOR_NAMES.append({"name" : "bisque", "r" : 255 , "g" : 228 , "b" : 196})
COLOR_NAMES.append({"name" : "blanched almond", "r" : 255 , "g" : 235 , "b" : 205})
COLOR_NAMES.append({"name" : "wheat", "r" : 245 , "g" : 222 , "b" : 179})
COLOR_NAMES.append({"name" : "corn silk", "r" : 255 , "g" : 248 , "b" : 220})
COLOR_NAMES.append({"name" : "lemon chiffon", "r" : 255 , "g" : 250 , "b" : 205})
COLOR_NAMES.append({"name" : "light golden rod yellow", "r" : 250 , "g" : 250 , "b" : 210})
COLOR_NAMES.append({"name" : "light yellow", "r" : 255 , "g" : 255 , "b" : 224})
COLOR_NAMES.append({"name" : "saddle brown", "r" : 139 , "g" : 69 , "b" : 19})
COLOR_NAMES.append({"name" : "sienna", "r" : 160 , "g" : 82 , "b" : 45})
COLOR_NAMES.append({"name" : "chocolate", "r" : 210 , "g" : 105 , "b" : 30})
COLOR_NAMES.append({"name" : "peru", "r" : 205 , "g" : 133 , "b" : 63})
COLOR_NAMES.append({"name" : "sandy brown", "r" : 244 , "g" : 164 , "b" : 96})
COLOR_NAMES.append({"name" : "burly wood", "r" : 222 , "g" : 184 , "b" : 135})
COLOR_NAMES.append({"name" : "tan", "r" : 210 , "g" : 180 , "b" : 140})
COLOR_NAMES.append({"name" : "rosy brown", "r" : 188 , "g" : 143 , "b" : 143})
COLOR_NAMES.append({"name" : "moccasin", "r" : 255 , "g" : 228 , "b" : 181})
COLOR_NAMES.append({"name" : "navajo white", "r" : 255 , "g" : 222 , "b" : 173})
COLOR_NAMES.append({"name" : "peach puff", "r" : 255 , "g" : 218 , "b" : 185})
COLOR_NAMES.append({"name" : "misty rose", "r" : 255 , "g" : 228 , "b" : 225})
COLOR_NAMES.append({"name" : "lavender blush", "r" : 255 , "g" : 240 , "b" : 245})
COLOR_NAMES.append({"name" : "linen", "r" : 250 , "g" : 240 , "b" : 230})
COLOR_NAMES.append({"name" : "old lace", "r" : 253 , "g" : 245 , "b" : 230})
COLOR_NAMES.append({"name" : "papaya whip", "r" : 255 , "g" : 239 , "b" : 213})
COLOR_NAMES.append({"name" : "sea shell", "r" : 255 , "g" : 245 , "b" : 238})
COLOR_NAMES.append({"name" : "mint cream", "r" : 245 , "g" : 255 , "b" : 250})
COLOR_NAMES.append({"name" : "slate gray", "r" : 112 , "g" : 128 , "b" : 144})
COLOR_NAMES.append({"name" : "light slate gray", "r" : 119 , "g" : 136 , "b" : 153})
COLOR_NAMES.append({"name" : "light steel blue", "r" : 176 , "g" : 196 , "b" : 222})
COLOR_NAMES.append({"name" : "lavender", "r" : 230 , "g" : 230 , "b" : 250})
COLOR_NAMES.append({"name" : "floral white", "r" : 255 , "g" : 250 , "b" : 240})
COLOR_NAMES.append({"name" : "alice blue", "r" : 240 , "g" : 248 , "b" : 255})
COLOR_NAMES.append({"name" : "ghost white", "r" : 248 , "g" : 248 , "b" : 255})
COLOR_NAMES.append({"name" : "honeydew", "r" : 240 , "g" : 255 , "b" : 240})
COLOR_NAMES.append({"name" : "ivory", "r" : 255 , "g" : 255 , "b" : 240})
COLOR_NAMES.append({"name" : "azure", "r" : 240 , "g" : 255 , "b" : 255})
COLOR_NAMES.append({"name" : "snow", "r" : 255 , "g" : 250 , "b" : 250})
COLOR_NAMES.append({"name" : "black", "r" : 0 , "g" : 0 , "b" : 0})
COLOR_NAMES.append({"name" : "dim grey", "r" : 105 , "g" : 105 , "b" : 105})
COLOR_NAMES.append({"name" : "grey", "r" : 128 , "g" : 128 , "b" : 128})
COLOR_NAMES.append({"name" : "dark grey", "r" : 169 , "g" : 169 , "b" : 169})
COLOR_NAMES.append({"name" : "silver", "r" : 192 , "g" : 192 , "b" : 192})
COLOR_NAMES.append({"name" : "light grey", "r" : 211 , "g" : 211 , "b" : 211})
COLOR_NAMES.append({"name" : "gainsboro", "r" : 220 , "g" : 220 , "b" : 220})
COLOR_NAMES.append({"name" : "white smoke", "r" : 245 , "g" : 245 , "b" : 245})
COLOR_NAMES.append({"name" : "white", "r" : 255 , "g" : 255 , "b" : 255})
#endregion

CELL_SIZE = 8

#Utilities
#region Utilities
def From1RelativeTo255(value):
    return round(value * 255)

    
def From255RelativeTo1(value):
    return value / 255.0


def From255RelativeTo1Render(value):
    if value == 255:
        return 1.0

    elif value == 0:
        return 0.0

    for i in range(256):
        r1 = From255RelativeTo1(i)
        r, g, b = cmds.colorManagementConvert(toDisplaySpace=[float(r1), 0.0, 0.0])
        r255 = From1RelativeTo255(r)
        if r255 >= value:
            return r1


def GetTexturePathDialog():
    multipleFilters = "Textures (*.tga *.png);;Targa (*.tga);;PNG (*.png);;All Files (*.*)"
    texture = cmds.fileDialog2(fileFilter=multipleFilters, dialogStyle=1)
    if texture is None or len(texture) == 0:
        cmds.warning("Error selecting texture {0}".format(texture))
    texture = texture[0]
    return texture
#endregion

class Palette(object):
    def __init__(self, path):
        self.path = path
        if os.path.exists(path):
            self.image = numpy.array(imageio.imread(path))
            self.channels = cv2.split(self.image)
            self.width = self.image.shape[1]
            self.height = self.image.shape[0]


    #generate texture - best of all 512 px and 64 variations
    def GenerateTexture(self, size, variationsNumber, path=None):
        step = 256.0 / variationsNumber
        array = numpy.empty([size, size, 3])
        if path is None:
            path = self.path

        x = 0
        y = 0
        pxCounter = 0
        for r in range(variationsNumber):
            for g in range(variationsNumber):
                for b in range(variationsNumber):
                    array[y][x] = [r * step, g * step, b * step]
                    pxCounter += 1
                    x += 1
                    if x >= size:
                        x = 0
                        y += 1
        #print(pxCounter)
                
        imageio.imwrite(path, array.astype(numpy.uint8), format='TARGA-FI')

    #0 - 1 to pixels
    def UVCoordinatesToPixels(self, u, v):
        x = round(u * (self.width))
        y = round((1 - v) * (self.height))
        xInt = int(x)
        yInt = int(y)
        if xInt > 0:
            xInt -=1
        if yInt > 0:
            yInt -=1
        return xInt, yInt


    def UVCoordinatesToColor(self, u, v):
        x, y = self.UVCoordinatesToPixels(u, v)
        r = g = b = a = 1
        if len(self.channels) == 4:
            r, g, b, a = self.image[y][x]
        else:
            r, g, b = self.image[y][x]
        return r, g, b, x, y


    def FindColor(self, r, g, b):
        print("\tSearching for color: {0} {1} {2}".format(r, g, b))
        dx = 0
        dy = 0
        dif = sys.maxint
        rr = 0
        gr = 0
        br = 0
        for yCell in range(self.height / CELL_SIZE):
            yPx = int((yCell * CELL_SIZE) + (CELL_SIZE / 2.0))
            for xCell in range(self.width / CELL_SIZE):
                xPx = int((xCell * CELL_SIZE) + (CELL_SIZE / 2.0))

                pixel = self.image[yPx][xPx]
                cr = pixel[0]
                cg = pixel[1]
                cb = pixel[2]
                if cr == r and cg == g and cb == b:
                    print("\t\tExact color was found: rgb: {0},{1},{2} x:{3} y:{4}".format(r, g, b, xCell, yCell))
                    return xPx, yPx, r, g, b

                cDiff = abs(r - cr) + abs(g - cg) + abs(b - cb)
                if cDiff < dif:
                    dif = cDiff
                    dx = xPx
                    dy = yPx
                    rr = cr
                    gr = cg
                    br = cb
        
        #print ('r:{0} g:{1} b:{2} sumary difference {3}'.format(rr, gr, br, dif))
        print("\t\tSimilar Color was found: rgb: {0},{1},{2} x:{3} y:{4}".format(rr, gr, br, dx, dy))
        return dx, dy, rr, gr, br

      
    def FindUVCoordinates(self, r1, g1, b1):
        r255 = From1RelativeTo255(r1)
        g255 = From1RelativeTo255(g1)
        b255 = From1RelativeTo255(b1)

        x, y, rf, gf, bf = self.FindColor(r255, g255, b255)
        xAdd = 1.0 / (self.width * 2.0)
        yAdd = 1.0 / (self.height * 2.0)
        resultX = float(x) / float(self.width)
        resultY = float(y) / float(self.height)
        resultY = 1.0 - resultY
        return resultX + xAdd, resultY - yAdd


class Rect(object):
    def __init__(self):
        self.xMin = 0
        self.xMax = 0
        self.yMin = 0
        self.yMax = 0


    def __init__(self, xMin, xMax, yMin, yMax):
        self.xMin = xMin
        self.xMax = xMax
        self.yMin = yMin
        self.yMax = yMax


    def InRec(self, x, y):
        if (x >= self.xMin and x < self.xMax and y >= self.yMin and y < self.yMax):
            return True
        return False


    def StretchBBW(self, x, y):
        if x > self.xMax:
            self.xMax = x
        if y > self.yMax:
            self.yMax = y
        if x < self.xMin:
            self.xMin = x
        if y < self.yMin:
            self.yMin = y
        

class Mesh(MayaTransform):
    def GetColorLamberts(self):
        processedSE = []
        result = []

        for material in self.Materials:
            mt = material[0]
            sg = material[1]
            nt = cmds.nodeType(mt)

            if nt != "lambert":
                cmds.warning("Non-Lamberts will be not processed {0} {1}".format(mt, mesh.Name))
                continue

            textureFiles = cmds.listConnections("{0}.color".format(mt), type="file")
            if textureFiles is not None and len(textureFiles) > 0:
                cmds.warning("Materials with texture will not be processed {0} {1}".format(mt, self.Name))
                continue

            if sg in processedSE:
                continue

            print("Processing: {0} (Shading Engine {1})".format(mt, sg))
            newLambert = Lambert()
            newLambert.InitByNode(mt,sg)
            if not newLambert in result:
                result.append(newLambert)

        return result

    #bake new texture by lamberts
    def BakeTextureByLamberts(self, lamberts, cellSize, path):
        numberOfMaterials = len(lamberts)

        #check materials
        for i in range(len(lamberts) - 1):
            l1 = lamberts[i]
            for j in range(i + 1, len(lamberts)):
                l2 = lamberts[j]
                for f in l1.faces:
                    for f2 in l2.faces:
                        if f == f2:
                            message = "WARNING {0} present in two materials {1} and {2}\nFix by assign lambert1 and back".format(
                                f, l1.material, l2.material)
                            cmds.select(f)
                            window.ISError(message)
                            return

        #have to find size of texture
        cells = 2 #we will start with 2x2 cells
        pxSize = cells * cellSize

        for b in range(1, 10):
            cellNumber = 2**b
            colors = cellNumber * cellNumber
            px =  cellNumber * cellSize
            if colors >= numberOfMaterials:
                print ('{0} colors will take texture {1}x{1} px.'.format(numberOfMaterials, px))
                cells = cellNumber
                pxSize = cellNumber * cellSize
                break

        bugFaces = []
        materialIndex = 0
        
        step = 1.0 / float(cellNumber)
        halfCellStep = step / 2.0
        array = numpy.empty([pxSize, pxSize, 3])
        allCells = cells * cells
        a = 0
        #processing texture and materials
        for cellX in range(cells):
            u = float(cellX) * step + halfCellStep

            for cellY in range(cells):
                v = float(cellY) * step + halfCellStep
                v = 1.0 - v
                a += 1
                lambert = lamberts[materialIndex]
                myFaces = []
                myUVs = []
                print('Processing {0} Cell: X,Y:{1}x{2} allCells:{3}/{4}'.format(lambert.material, cellX, cellY, a, allCells))
                for face in lambert.faces:
                    if not re.match("{0}.f\[\d+.*".format(self.Name), face):
                        print('\tface {0} from other, mesh continue'.format(face))
                        continue
                    else:
                        myFaces.append(face)
                try:
                    cmds.polyPlanarProjection(myFaces, md="b")
                    myUVs = cmds.polyListComponentConversion(myFaces, ff=True, tuv=True, internal=True)
                    cmds.select(cl=True)
                    for uv in myUVs:
                        cmds.polyEditUV(uv, relative=False, uValue=u, vValue=v)

                    for x in range(cellSize):
                        xV = x + cellX * cellSize
                        for y in range(cellSize):
                            yV = y + cellY * cellSize
                            array[yV][xV] = [lambert.r, lambert.g, lambert.b]
                
                    materialIndex += 1
                    if materialIndex >= numberOfMaterials:
                        imageio.imwrite(path, array.astype(numpy.uint8), format='TARGA-FI')
                        message = 'Ready to write texture: Index:{0}/{1}'.format(
                            materialIndex, numberOfMaterials)
                        cmds.warning(message)
                        return path

                except Exception as ex:
                    message = "Can not process faces and uvs. ex: {0}\n{1}\n{2}".format(ex, self.Name, myFaces)
                    cmds.warning(message)
                    cmds.select(myFaces)
                    window.OkWindow("Error", message, icon='critical')
                    return ''
            

    def ToLamberts(self, paletteTexturePath):
        allLamberts = []
        palette = Palette(paletteTexturePath)
        allSets = self.UVShells
        set0 = allSets[0]
        uvSetName = set0.keys()[0]
        print ("Using first UV Set: {0}".format(uvSetName))
        uvSet0 = set0[uvSetName]
        for shell in uvSet0:
            points = uvSet0[shell]
            firstPoiint = points[0]
            u, v = cmds.polyEditUV(firstPoiint, query=True)
            bbw = Rect(u, u, v, v)
            for point in points:
                u, v = cmds.polyEditUV(firstPoiint, query=True)
                bbw.StretchBBW(u, v)
            cmds.select(points)
            r, g, b, x, y = palette.UVCoordinatesToColor(bbw.xMin, bbw.yMin)
            lambert = Lambert()
            lambert.InitByPoints(r, g, b, points)
            
            #cl = ColorLambert(r, g, b, uvs=points)
            found = False
            for otherLambert in allLamberts:
                if otherLambert.IsEqual(lambert):
                    found = True
                    otherLambert.AddPoints(lambert)
                    break

            if not found:
                print("Color: {0} {1} {2} Using uvs: {3} {4}".format(r, g, b, x, y))
                allLamberts.append(lambert)

        for l in allLamberts:
            tableName = l.getNameFromList()
            node, r, g, b = l.CreateNode()
            cmds.rename(node, self.Name + "_" + tableName)
            print("Creating lambert node: {0} rgb: {1}, {2}, {3}".format(tableName, r, g, b))
            

    def FromLambertsToTexture(self, paletteTexturePath):
        bugFaces = []
        processedSE = []
        cmds.select(cl=True)
        palette = Palette(paletteTexturePath)
        for material in self.Materials:
            mt = material[0]
            sg = material[1]
            nt = cmds.nodeType(mt)
            if nt != "lambert":
                cmds.warning("Non-Lamberts will be not processed {0} {1}".format(mt, mesh.Name))
                continue

            textureFiles = cmds.listConnections("{0}.color".format(mt), type="file")
            if textureFiles is not None and len(textureFiles) > 0:
                cmds.warning("Materials with texture will not be processed {0} {1}".format(mt, self.Name))
                continue

            if sg in processedSE:
                continue

            print("Processing: {0} (Shading Engine {1})".format(mt, sg))

            colors = cmds.getAttr("{0}.color".format(mt))
            r, g, b = cmds.colorManagementConvert(toDisplaySpace=[colors[0][0], colors[0][1], colors[0][2]])
            u, v = palette.FindUVCoordinates(r, g, b)

            processedSE.append(sg)
            faces = []
            members = cmds.sets(sg, q=True)
            for item in members:
                faces.append(item)
            
            cmds.select(cl=True)
            try:
                cmds.polyPlanarProjection(faces, md="b")
            except Exception as ex:
                print ("Can not project planar {0} {1}".format(ex, material))
                for f in faces:
                    print ("\t{0}".format(f))
                    if f not in bugFaces:
                        bugFaces.append(f)
                
            uvs = cmds.polyListComponentConversion(faces, ff=True, tuv=True, internal=True)
            cmds.select(cl=True)
            for uv in cmds.ls(uvs, fl=True):
                cmds.polyEditUV(uv, relative=False, uValue=u, vValue=v)

        if len(bugFaces) > 0:
            cmds.select(bugFaces)


    @property
    def Materials(self):
        """Materials of transform
        Returns:
            [[str, str]] - list of arrays where 0 is material and 1 is shadingEngine
        """
        result = []
        for shape in self.Shapes:
            shadingEngines = cmds.listConnections(shape, type='shadingEngine')
            if shadingEngines is None:
                continue
            for se in cmds.listConnections(shape, type='shadingEngine'):
                for connect in cmds.listConnections("{0}.surfaceShader".format(se)):
                    cValue = [connect, se]
                    if not cValue in result:
                        result.append(cValue)
        return result


class Lambert(object):
    def __init__(self):
        self.r = 0.0 #from 0 to 255
        self.g = 0.0 #from 0 to 255
        self.b = 0.0 #from 0 to 255
        self.uvs = []
        self.faces = []
        self.lambertNode = None
        self.shadingGroup = None


    def getNameFromList(self):
        currentName = "color"
        currentLength = sys.maxsize
        for n in COLOR_NAMES:
            lc = (self.r - n["r"], self.g - n["g"], self.b - n["b"])
            sq = (lc[0] * lc[0] + lc[1] * lc[1] + lc[2] * lc[2])
            length = math.sqrt(sq)
            if length < currentLength:
                currentLength = length
                currentName = n["name"]
        print("diff {0}".format(currentLength))
        return currentName


    def InitByNode(self, material, shadingGroup):
        self.material = material
        self.shadingGroup = shadingGroup
        colors = cmds.getAttr("{0}.color".format(self.material))
        rConv, gConv, bConv = cmds.colorManagementConvert(toDisplaySpace=[colors[0][0], colors[0][1], colors[0][2]])
        self.r = From1RelativeTo255(rConv)
        self.g = From1RelativeTo255(gConv)
        self.b = From1RelativeTo255(bConv)
        self.faces = []
        self.uvs = []
        members = cmds.sets(self.shadingGroup, q=True)
        for face in members:
            self.faces.append(face)


    def InitByPoints(self, r, g, b, uvs):
        self.r = r
        self.g = g
        self.b = b
        self.uvs = uvs


    def CreateNode(self):
        node = cmds.shadingNode("lambert", asShader=True)

        rRender = From255RelativeTo1Render(self.r)
        gRender = From255RelativeTo1Render(self.g)
        bRender = From255RelativeTo1Render(self.b)
        cmds.setAttr("{0}.color".format(node), rRender, gRender, bRender, type="double3")
        #create a shading group
        shadingGroup=cmds.sets(renderable=True, noSurfaceShader=True, empty=True)
        #connect the shader to the shading group
        cmds.connectAttr('%s.outColor' %node, '%s.surfaceShader'%shadingGroup)

        #convert uvs to faces
        self.faces = []
        for uv in self.uvs:
            convert = cmds.polyListComponentConversion(uv, fuv=True, tf=True)
            for f in convert:
                self.faces.append(f)
                
        #assin shading group to faces
        cmds.sets(self.faces, e=True, forceElement=shadingGroup)
        return node, self.r, self.g, self.b


    def IsEqual(self, other):
        if self.r == other.r and self.g == other.g and self.b == other.b:
            return True
        return False


    def AddPoints(self, other):
        self.uvs.append(other.uvs)
                    

def ToLamberts():
    selection = cmds.ls(sl=True, l=True, type='transform')
    if len(selection) < 1:
        window.ISError("Select al least one object")

    for sel in selection:
        mesh = Mesh(sel)
        materials = mesh.Materials
        if len(materials) != 1:
            if len(materials) == 0:
                cmds.warning("No materials assigned to {0}".format(mesh.Name))
                continue

            elif len(materials) > 1:
                cmds.warning("More than one material assigned to {0}. Using first {1}".format(mesh.Name, materials[0][0]))

        material = materials[0][0]
        shadingEngine = materials[0][1]
        textureFiles = cmds.listConnections("{0}.color".format(material), type="file")
        path = ""
        if textureFiles:
            firstTexture = textureFiles[0]
            currentPath = cmds.getAttr("{0}.fileTextureName".format(firstTexture))
            if os.path.exists(currentPath):
                print("{0} using texture {1}".format(mesh.Name, path))
                path = currentPath

        if not path:
            print("No texture found for mesh: {0}".format(mesh.Name))
            continue

        mesh.ToLamberts(path)


def ToTexture():
    selection = cmds.ls(sl=True, l=True, type='transform')
    if len(selection) < 1:
        window.ISError("Select al least one object")

    multipleFilters = "Textures (*.tga *.png);;Targa (*.tga);;PNG (*.png);;All Files (*.*)"
    texture = cmds.fileDialog2(fileFilter=multipleFilters, dialogStyle=1)
    if texture is None or len(texture) == 0:
        cmds.warning("Error selecting texture {0}".format(texture))
    texture = texture[0]
    
    for sel in selection:
        mesh = Mesh(sel)
        materials = mesh.Materials
        if len(materials) == 0:
            cmds.warning("No materials found for {0}".format(mesh.Name))
            continue

        mesh.FromLambertsToTexture(texture)


def ToNewBakedTexture():
    selection = cmds.ls(sl=True, l=True, type='transform')
    if len(selection) < 1:
        window.ISError("Select al least one object")

    for sel in selection:
        mesh = Mesh(sel)
        lamberts = mesh.GetColorLamberts()
        path = GetTexturePathDialog()
        path = mesh.BakeTextureByLamberts(lamberts, CELL_SIZE, path)
        if path is not None and os.path.exists(path):
            node = cmds.shadingNode("lambert", asShader=True, name='{0}_lambert'.format(mesh.Name))
            fileNode = cmds.shadingNode("file", asTexture=True, isColorManaged=True)
            cmds.connectAttr("{0}.outColor".format(fileNode), "{0}.color".format(node))
            cmds.setAttr("{0}.fileTextureName".format(fileNode), path, type="string")
            #create a shading group
            shadingGroup=cmds.sets(renderable=True, noSurfaceShader=True, empty=True)
            #connect the shader to the shading group
            cmds.connectAttr('%s.outColor' %node, '%s.surfaceShader'%shadingGroup)
            cmds.sets(mesh.LongName, e=True, forceElement=shadingGroup)
        else:
            message = 'No Lambert Node was baked for mesh {0}'.format(mesh.Name)
            window.ISError(message)
        return


def SelectionToCell():
    selection = cmds.ls(sl=True, l=True, fl=True)
    if len(selection) < 1:
        window.ISError("Select al least one object")

    size = window.ChooseWindow(title='AtlasSize', message='Choose Atlas Size', buttons=['2', '4', '8', '16', '32'])
    size = int(size)

    u, v = CellToUv(0, 0, size)
    print('U: {0} V: {1}'.format(u, v))
    result = []

    for sel in selection:
        cmds.select(cl=True)
        cmds.polyPlanarProjection(sel, md="b")
        cmds.select(cl=True)
        convert = cmds.polyListComponentConversion(sel, ff=True, tuv=True )
        for uv in convert:
            cmds.polyEditUV(uv, relative=False, uValue=u, vValue=v)
            result.append(uv)

    cmds.select(result)


def CellToUv(cellX, cellY, size):
    step = 1.0 / float(size)
    halfCellStep = step / 2.0
    u = float(cellX) * step + halfCellStep
    v = float(cellY) * step + halfCellStep
    v = 1.0 - v
    return u, v

def GenerateTexture(cellSize, textureSize, path):
    lineCount = textureSize / cellSize
    allCellsCount = lineCount * lineCount
    root = int(math.floor(allCellsCount ** (1.0/3.0)))
    allCellsCount = root * root * root
    step = 1.0 / float(allCellsCount)
    halfCellStep = step / 2.0
    colorStep = 255.0 / float(root-1)
    colors = []
    for r in range(root):
        rv = round(r * colorStep)
        for g in range(root):
            gv = round(g * colorStep)
            for b in range(root):
                bv = round(b * colorStep)
                colors.append([rv, gv, bv])

    print("Number of colors: {0}".format(allCellsCount))
    array = numpy.empty([textureSize, textureSize, 3])
    cellIndex = 0
    for cellX in range(lineCount):
        u = float(cellX) * step + halfCellStep
        for cellY in range(lineCount):
            v = float(cellY) * step + halfCellStep
            for x in range(cellSize):
                xV = x + cellX * cellSize
                for y in range(cellSize):
                    yV = y + cellY * cellSize
                    array[yV][xV] = colors[cellIndex]
            cellIndex += 1
            if cellIndex >= allCellsCount:
                imageio.imwrite(path, array.astype(numpy.uint8), format='TARGA-FI')
                return


def countStep(max):
    step = 1.0 / float(max)
    for i in range(max):
        print(i * step)

def Lerp(a, b, t):
    """Lerps between a and b by t
    Arguments:
        a - value for 0
        b - value for 1
        t {float} - interpolation coefficient
    Returns:
        Lerped value
    """
    a = float(a)
    b = float(b)
    t = float(t)
    value = (t * b) + ((1-t) * a)
    return value

def InverseLerp(a, b, value):
        """Inverse Lerps between a and b by t
        Arguments:
            a - value for 0
            b - value for 1
            t {float} - interpolation coefficient
        Returns:
            Lerped value
        """
        a = float(a)
        b = float(b)
        value = float(value)
        halfA = value - a
        halfB = b-a
        return (halfA / halfB)
#not ready yet
def UVToCall(u, v, size):
    pass

'''
import MT.Palette as pt
reload(pt)
mesh = pt.Mesh("bld_house")
mesh.ToLamberts("C:/mt/mtUnity/Assets/Art/_tex/color_cheker.tga")

import MT.Palette as pt
reload(pt)
p = pt.Palette("C:/mt/mtUnity/Assets/Art/_tex/color_cheker.tga")
p.FindColor(230, 229, 15)

'''
    
