import re
import maya.cmds as cmds
import maya.mel as mel
from MayaTransform import MayaTransform as MayaTransform


def Log(message):
    print("MT: {0}".format(message))


def CreateTieLocator(name, targetName):
    target = None
    selection = cmds.ls(sl=True, fl=True)
    if not selection:
        cmds.warning("Nothing selected")
        return

    if len(selection) <= 0:
        cmds.warning("Nothing selected")
        return

    cmds.select(cl=True)

    for item in selection:
        if not re.match(".+\.f\[\d+\]$", item):
            cmds.warning("No face selected")
            continue

        Log("item : {0}".format(item))
        face = item

        node = None
        split = face.split(".")
        nodeName = split[0]
        faceNumber = split[1]
        faceNumber = faceNumber[2:-1]
        faceNumber = int(faceNumber)

        
        nodeType = cmds.nodeType(nodeName)
        if nodeType == "mesh":
            Log("Mesh Mode: Working with shape: {0}".format(nodeName))
            relatives = cmds.listRelatives(nodeName, type="transform", p=True)
            Log("Shape relatives number: {0}".format(len(relatives)))
            if len(relatives) == 0:
                cmds.warning("No transform relatives found for ".format(nodeName))
                return

            nodeName = relatives[0]
            node = MayaTransform(nodeName)

            if not node.Exists:
                Log('Node is not defined for selection\n{0}'.format(selection))
                return

            Log('{0} defined as mesh'.format(node.Name))

        elif nodeType == "transform":
            Log("Transform Node: Working with transform: {0}".format(nodeName))
            node = MayaTransform(nodeName)

        else:
            message = "Working with {0} is {1}. Do not know how to process".format(nodeName, nodeType)
            cmds.warning(message)
            return
        
        for child in node.Children:
            if child.Name == targetName:
                target = child
                break
        #defining all ties faces
        cmds.polySelect(node.LongName, r=True, ets=faceNumber)
        shell = cmds.ls(sl=True)
        xf = cmds.xform(shell, bbi=True, q=True, ws=True)
        #ties faces bounding box center
        center = MayaTransform.Center(xf)
        cmds.select(cl=True)

        locator = cmds.spaceLocator()[0]
        locator = MayaTransform(locator)
        locator.SetAttr("translateX", center[0])
        locator.SetAttr("translateY", center[1])
        locator.SetAttr("translateZ", center[2])
        locator.Parent = node
        
        a = 1
        for n in node.Children:
            if re.match("{0}\d+$".format(name), n.Name):
                a = a + 1
        Log("Last Tie number found {0}".format(a))

        locatorName = "{0}{1}".format(name, a)
        Log("Locators Name: {0}".format(locatorName))
        locator.Name = locatorName

        if not target == None:
            Log("No Target Given")
            ct = cmds.normalConstraint(face, locator.LongName,  weight=1, aimVector=(0, 1, 0),
                                  worldUpType="object", worldUpObject=target.LongName)
            
            cmds.delete(ct)
            
        else:
            Log("Target Given")
            ct = cmds.normalConstraint(face, locator.LongName,  weight=1, aimVector=(0, 1, 0),
                                  worldUpType="scene")

            cmds.delete(ct)

        #cmds.move(0, -tieThickness/2.0, 0, locator.LongName,  r=True, os=True, wd=True)

    #cmds.hilite(node.LongName, r=True)
    #mel.eval("changeSelectMode -component")
    #cmds.selectType(alo=False, smp=False, sme=False, smf=True, smu=False, pv=False, pe=False, pf=True, puv=False)
    cmds.select(selection)


def CreateTieEndLocatorByRef():
    selection = cmds.ls(sl=True, fl=True)
    if not selection:
        cmds.warning("Nothing selected")
        return

    if not len(selection) == 2:
        cmds.warning("Select two objects")
        return

    mesh = None
    loc = None

    for sel in selection:
        tr = MayaTransform(sel)
        if tr.Type == "mesh":
            mesh = tr
        elif tr.Type == "locator":
            loc = tr

    if mesh is None or loc is None:
        cmds.warning("Can not define mesh or locator")
        return

    cmds.select(cl=True)
    newLoc = loc.Duplicate()
    newLoc.Parent = mesh
    newLoc.Rename("end")
    newLoc.Select()


def ScaleOld():
    selection = cmds.ls(sl=True, fl=True)
    if not selection:
        cmds.warning("Nothing selected")
        return

    for sel in selection:
        cmds.select(sel)
        cmds.scale(7.589, 7.589,7.589, r=True)

    cmds.select(selection)


def RenamePoints():
    selection = cmds.ls(sl=True, fl=True)
    if not selection:
        cmds.warning("Nothing selected")
        return

    lst = []
    for p in selection:
        locator = MayaTransform(p)
        locator.Rename("tempPoint1")
        lst.append(locator)

    i = 1
    for p in lst:
        p.Rename("point{0}".format(i))
        i += 1


def FlipPoints():
    selection = cmds.ls(sl=True, fl=True)
    if not selection:
        cmds.warning("Nothing selected")
        return

    for p in selection:
        locator = MayaTransform(p)
        r = locator.GetAttr("rotateY")
        r1 = r - 90
        locator.SetAttr("rotateY", r1)


def FlipPoints2():
    selection = cmds.ls(sl=True, fl=True)
    if not selection:
        cmds.warning("Nothing selected")
        return

    for p in selection:
        locator = MayaTransform(p)
        cmds.rotate(90, 0, 0, locator.LongName, r=True, os=True, fo=True)


def TieLocatorBySubObject():
    selection = cmds.ls(sl=True, fl=True)
    if not selection:
        cmds.warning("Nothing selected")
        return

    if len(selection) != 2:
        cmds.warning("Please select two objects")
        return

    cmds.select(cl=True)

    locators = []
    for sel in selection:
        bbi = cmds.xform(sel, bbi=True, q=True)
        center = MayaTransform.Center(bbi)
        loc = cmds.spaceLocator()[0]
        cmds.select(cl=True)
        loc = MayaTransform(loc)
        loc.SetAttr('translateX', center[0])
        loc.SetAttr('translateY', center[1])
        loc.SetAttr('translateZ', center[2])
        locators.append(loc)
    cmds.aimConstraint(locators[0].LongName, locators[1].LongName,
                        offset=(0, 0, 0), weight=1, aimVector=(1, 0, 0),
                        upVector=(0, 1, 0), worldUpType="scene")

    cmds.aimConstraint(locators[1].LongName, locators[0].LongName,
                        offset=(0, 0, 0), weight=1, aimVector=(1, 0, 0),
                        upVector=(0, 1, 0), worldUpType="scene")

    locCenter = cmds.spaceLocator()[0]
    cmds.select(cl=True)
    locCenter = MayaTransform(locCenter)

    pointConstraint = cmds.pointConstraint(locators[0].LongName,  locators[1].LongName, 
                                           locCenter.LongName,
                                           mo=False, skip='y', weight=0.5)[0]

    orientConstraint = cmds.orientConstraint(locators[0].LongName, locCenter.LongName, 
                                             offset=(0, 0, 0), skip="x")
    locators[0].Delete()
    locators[1].Delete()
    locCenter.Rename("point1")
    cmds.select(selection)

def LookAtNext():
    selection = cmds.ls(sl=True, fl=True)
    if not selection:
        cmds.warning("Nothing selected")
        return

    if len(selection) != 2:
        cmds.warning("Please select two objects")
        return

    cmds.select(cl=True)

    loc = MayaTransform(selection[0])
    aim = MayaTransform(selection[1])
    
    node = cmds.aimConstraint(aim.LongName, loc.LongName,
                        offset=(0, 0, 0), weight=1, aimVector=(0, 0, 1),
                        upVector=(0, 1, 0), worldUpType="scene", worldUpVector=(0, 1, 0))
    cmds.delete(node[0])
    
    cmds.select(selection)

def ChainExportGroups():
    selection = cmds.ls(sl=True, fl=True)
    if not selection:
        cmds.warning("Nothing selected")
        return

    if len(selection) != 2:
        cmds.warning("Please select two objects")
        return

    cmds.select(cl=True)
    first = MayaTransform(selection[0])
    second = MayaTransform(selection[1])
    end = None

    for child in second.Children:
        if child.Name == "end":
            end = child

    if child is None:
        cmds.warning("NO end locator found")
        return
    
    pos = cmds.xform(end.LongName, q=True, ws=True, rp=True)
    rot = cmds.xform(end.LongName, q=True, ws=True, ro=True)

    first.SetAttr("translateX", pos[0])
    first.SetAttr("translateY", pos[1])
    first.SetAttr("translateZ", pos[2])
    first.SetAttr("rotateX", rot[0])
    first.SetAttr("rotateY", rot[1])
    first.SetAttr("rotateZ", rot[2])
        
