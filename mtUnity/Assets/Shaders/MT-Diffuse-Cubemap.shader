// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "MT/Diffuse Cubemap" {
Properties {
    _MainTex ("Base (RGB)", 2D) = "white" {}
	_Cube ("Reflection Cubemap", Cube) = "_Skybox" {}
	[PowerSlider(1.0)] _Coeff("Overlay", Range(0.0, 1.0)) = 0.0	
}
SubShader {
    Tags { "RenderType"="Opaque" }
    LOD 150

CGPROGRAM
#pragma surface surf Lambert noforwardadd

sampler2D _MainTex;
samplerCUBE _Cube;
uniform half _Coeff;

struct Input {
    float2 uv_MainTex;
	float3 worldRefl;
};

void surf (Input IN, inout SurfaceOutput o) {
    fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
	fixed4 reflcol = texCUBE (_Cube, IN.worldRefl);
    o.Albedo = lerp(c.rgb, reflcol, _Coeff);
    o.Alpha = c.a;
}
ENDCG
}

Fallback "Mobile/VertexLit"
}
