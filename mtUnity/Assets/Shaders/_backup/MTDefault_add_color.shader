// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Simplified Bumped Specular shader. Differences from regular Bumped Specular one:
// - no Main Color nor Specular Color
// - specular lighting directions are approximated per vertex
// - writes zero to alpha channel
// - Normalmap uses Tiling/Offset of the Base texture
// - no Deferred Lighting support
// - no Lightmap support
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "MT/_BACKUP/Default Additive Color" {
Properties {
    [PowerSlider(5.0)] _Shininess ("Shininess", Range (0.03, 1)) = 0.05
	[PowerSlider(5.0)] _AmbientPower ("AmbientPower", Range (0.0, 1.0)) = 1
    _MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
    _Color("Main Color", Color) = (1.0, 1.0, 1.0, 1.0)
	_AmbColor("AmbientColor", Color) = (1.0, 1.0, 1.0, 1.0)
	[PowerSlider(1.0)] _Coeff ("Overlay", Range(0.0, 1.0)) = 0.0
}
SubShader {
    Tags { "RenderType"="Opaque" }
    LOD 250

CGPROGRAM
#pragma surface surf MobileBlinnPhong exclude_path:prepass nolightmap noforwardadd halfasview interpolateview

inline fixed4 LightingMobileBlinnPhong (SurfaceOutput s, fixed3 lightDir, fixed3 halfDir, fixed atten)
{
    fixed diff = max (0, dot (s.Normal, lightDir));
    fixed nh = max (0, dot (s.Normal, halfDir));
    fixed spec = pow (nh, s.Specular*128) * s.Gloss;

    fixed4 c;
    c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec) * atten;
    UNITY_OPAQUE_ALPHA(c.a);
    return c;
}

sampler2D _MainTex;
half _Shininess;
half _AmbientPower;
uniform half4 _Color;
uniform half4 _AmbColor;
uniform half _Coeff;

struct Input {
    float2 uv_MainTex:TEXCOORD0;
	float2 uv_AmbTex:TEXCOORD0;
};

void surf (Input IN, inout SurfaceOutput o) {
    fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
    o.Albedo = lerp(tex.rgb + (_AmbColor * _AmbientPower),  _Color.rgb, _Coeff);
    o.Gloss = tex.a;
    o.Alpha = tex.a;
    o.Specular = _Shininess;
    //o.Normal = UnpackNormal (tex2D(_BumpMap, IN.uv_MainTex));
	
}
ENDCG
}

FallBack "Mobile/VertexLit"
}
