// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Simplified Bumped Specular shader. Differences from regular Bumped Specular one:
// - no Main Color nor Specular Color
// - specular lighting directions are approximated per vertex
// - writes zero to alpha channel
// - Normalmap uses Tiling/Offset of the Base texture
// - no Deferred Lighting support
// - no Lightmap support
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "MT/_BACKUP/Default Multiply AO" {
Properties {
    [PowerSlider(5.0)] _Shininess ("Shininess", Range (0.03, 1)) = 0.078125
	[PowerSlider(5.0)] _AmbientPower ("AmbientPower", Range (0.0, 1.0)) = 0
    _MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
	_AmbTex ("Ambient (RGB)", 2D) = "white" {}
	_AoTex ("Ambient Occlusion (RGB)", 2D) = "white" {}
    [NoScaleOffset] _BumpMap ("Normalmap", 2D) = "bump" {}
}
SubShader {
    Tags { "RenderType"="Opaque" }
    LOD 250

CGPROGRAM
#pragma surface surf MobileBlinnPhong exclude_path:prepass nolightmap noforwardadd


inline fixed4 LightingMobileBlinnPhong (SurfaceOutput s, fixed3 lightDir, fixed3 halfDir, fixed atten)
{
    fixed diff = max (0, dot (s.Normal, lightDir));
    fixed nh = max (0, dot (s.Normal, halfDir));
    fixed spec = pow (nh, s.Specular*128) * s.Gloss;

    fixed4 c;
    c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec) * atten;
    UNITY_OPAQUE_ALPHA(c.a);
    return c;
}

sampler2D _MainTex;
sampler2D _AmbTex;
sampler2D _BumpMap;
sampler2D _AoTex;
half _Shininess;
half _AmbientPower;

struct Input {
    float2 uv_MainTex:TEXCOORD0;
	float2 uv_AmbTex:TEXCOORD0;
	float2 uv2_AoTex:TEXCOORD1;
};

void surf (Input IN, inout SurfaceOutput o) {
    fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
	fixed4 amb = tex2D(_AmbTex, IN.uv_AmbTex);
	fixed4 ao = tex2D(_AoTex, IN.uv2_AoTex);
    o.Albedo = tex.rgb * (amb.rgb + _AmbientPower) * (ao.rgb);
    o.Gloss = tex.a * ao.rgb;
    o.Alpha = tex.a;
    o.Specular = _Shininess * ao.rgb;
    o.Normal = UnpackNormal (tex2D(_BumpMap, IN.uv_MainTex));
	
}
ENDCG
}

FallBack "Mobile/VertexLit"
}
