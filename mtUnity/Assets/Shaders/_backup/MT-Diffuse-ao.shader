// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "MT/_BACKUP/Diffuse AO" {
Properties {
    _MainTex ("Base (RGB)", 2D) = "white" {}
	_AoTex ("Ambient Occlusion (RGB)", 2D) = "white" {}
}
SubShader {
    Tags { "RenderType"="Opaque" }
    LOD 150

CGPROGRAM
#pragma surface surf Lambert noforwardadd

sampler2D _MainTex;
sampler2D _AoTex;

struct Input {
    float2 uv_MainTex:TEXCOORD0;
	float2 uv2_AoTex:TEXCOORD1;
};

void surf (Input IN, inout SurfaceOutput o) {
    fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
	fixed4 ao = tex2D(_AoTex, IN.uv2_AoTex);
    o.Albedo = c.rgb * ao.rgb;
    o.Alpha = c.a;
}
ENDCG
}

Fallback "Mobile/VertexLit"
}
