// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "MT/_BACKUP/Diffuse Vertex Color" {
Properties {
    _MainTex ("Base (RGB)", 2D) = "white" {}
	[PowerSlider(1.0)] _Coeff ("Vertex Color", Range(0.0, 1.0)) = 0.0
}
SubShader {
    Tags { "RenderType"="Opaque" }
    LOD 150

CGPROGRAM
#pragma surface surf Lambert noforwardadd vertex:vert

sampler2D _MainTex;
uniform half _Coeff;

struct Input {
    float2 uv_MainTex;
	float3 vertColor;
};

void vert(inout appdata_full v, out Input o)	
		{	
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.vertColor = v.color;
		}
		

void surf (Input IN, inout SurfaceOutput o) {
    fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
    o.Albedo = c.rgb * lerp(IN.vertColor.rgb, 1.0, 1.0 - _Coeff );
	//o.Albedo = c.rgb + IN.vertColor.rgb * _Coeff;
    o.Alpha = c.a;
}
ENDCG
}

Fallback "Mobile/VertexLit"
}
