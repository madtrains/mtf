Shader "MT/Diffuse-Outline" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_OutlineColor ("Outline Color", Color) = (0,0,0,1)
		_OutlineWidth ("Outline Width", Range(0.0, 1.0)) = 0.05
	}
	SubShader 
	{
		
		Tags { "RenderType"="Opaque" }
		LOD 150

		Pass 
		{
			CGPROGRAM
			#pragma vertex vert
            #pragma fragment frag            
			#include "UnityCG.cginc"
			
			//Cull Front // Отсекает лицевые полигоны меша
			//ZWrite Off // Отключает запись в буфер глины

			struct appdata 
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f 
			{
				float4 pos : SV_POSITION;
				float3 color : COLOR0;
			};

			float _OutlineWidth;
			float4 _OutlineColor;

			v2f vert(appdata v) 
			{
				v2f o;
				float3 norm = mul((float3x3)UNITY_MATRIX_IT_MV, v.normal);
				float4 pos = UnityObjectToClipPos(v.vertex);
				
				o.pos = UnityObjectToClipPos(v.vertex + float4(norm, 0) * _OutlineWidth);
				o.color = _OutlineColor.rgb;
				return o;
			}

			half4 frag(v2f IN) : SV_Target 
			{
				return half4(IN.color, 1.0);
			}
			ENDCG
		}
		
		Pass
		{
			CGPROGRAM
			struct Input 
			{
				float2 uv_MainTex;
			};
			
			sampler2D _MainTex;

			void surf (Input IN, inout SurfaceOutput o) 
			{
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
				o.Albedo = c.rgb;
				o.Alpha = c.a;
			}
			ENDCG
		}
	}
	Fallback "Mobile/VertexLit"
}
