// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "MT/DiffuseRecolor" {
Properties 
{
    _MainTex ("Base (RGB)", 2D) = "white" {}
	_Color ("Main Color", Color) = (1,1,1,1)
	_Position2D("Position", Vector) = (0, 0, 0, 0)
}
SubShader {
    Tags { "RenderType"="Opaque" }
    LOD 150

CGPROGRAM
#pragma surface surf Lambert noforwardadd

sampler2D _MainTex;
fixed4 _Color;
fixed4 _Position2D;


struct Input 
{
    float2 uv_MainTex;
};

void surf (Input IN, inout SurfaceOutput o) 
{
    fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
	float2 uv = IN.uv_MainTex;	
	if (uv.x > _Position2D[0] && uv.x <  _Position2D[2] && uv.y > _Position2D[1] && uv.y <  _Position2D[3])
		o.Albedo = _Color.rgb;
	else
		o.Albedo = c.rgb;
    o.Alpha = c.a;
}
ENDCG
}

Fallback "Mobile/VertexLit"
}
