// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "MT/Gear" {
Properties 
{
    _MainTex ("Base (RGB)", 2D) = "white" {}
	_Color ("Main Color", Color) = (1,1,1,1)
}
SubShader {
    LOD 150
	

CGPROGRAM
#pragma surface surf Lambert

sampler2D _MainTex;
fixed4 _Color;


struct Input 
{
    float2 uv_MainTex;
};

void surf(Input IN, inout SurfaceOutput o)
{
    fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
    
	if (IN.uv_MainTex.x > 0.5f && IN.uv_MainTex.y < 0.5f)
	{
		o.Albedo = _Color;
	}
	else		
	{
		o.Albedo = c.rgb;
	}
    //o.Alpha = c.a;
}
ENDCG
}

Fallback "Mobile/VertexLit"
}
