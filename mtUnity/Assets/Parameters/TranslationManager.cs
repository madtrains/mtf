﻿using MTParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;


public class TranslationManager
{
	//language id (English=0, German=1, Russian=2)
	public enum Language
	{
		en = 0,
		ge = 1,
		ru = 2
	}

	public Language? Lang
	{
		get { return _currentLanguage; }
	}

	private MTParameters.Language[] _languages;

	private Language _defaultlanguage => Language.en;
	private Language _currentLanguage { get; set; }
	private Dictionary<string, string> _strings { get { return _languages[(int)_currentLanguage].StringsDict; } }

	public Action<Language> OnLanguageChanged;

	public TranslationManager(GameParameters gameParameters)
	{
		_languages = gameParameters.Languages;
	}

	public static TranslationManager Init(GameParameters gameParameters)
	{
		var tm = new TranslationManager(gameParameters);

		var langName = GameStarter.Instance.PlayerManager.GetCurrentLanguage();

		tm.SetCurrentLanguage((Language)langName);

		return tm;
	}

	public void SetCurrentLanguage(Language languageName)
	{
		_currentLanguage = _languages.Length > (int)languageName ? languageName : _defaultlanguage;

		if (OnLanguageChanged != null)
			OnLanguageChanged(_currentLanguage);
	}

	public string[] GetKeys(string startsWith) { return _strings.Keys.Where(k => k.StartsWith(startsWith)).ToArray(); }
	public string GetTableString(string key, params (string, string)[] values)
	{
		if (!_strings.ContainsKey(key))
			return key;

		var result = _strings[key];

		foreach (var pair in values)
		{
			result = result.Replace("{" + pair.Item1 + "}", pair.Item2);
		}
		return result;

	}
	public string GetTableString(string key)
	{
		if (_strings.ContainsKey(key))
			return _strings[key];
		return key;
	}

	public void AddDevString(string key, string value)
	{
		if (_strings.ContainsKey(key)) { _strings[key] = value; } else { _strings.Add(key, value); }
	}
}