﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace MTParameters
{
	public class SV
	{
        #region Statica
		private static float StringToFloat(string input, bool convertToComa)
		{
			try
			{
				if (convertToComa)
					input = input.Replace('.', ',');

				float result = float.Parse(input);
				return result;
			}
			catch (Exception ex)
			{
				Debug.LogErrorFormat("Can not convert string {0} to float\n{1}", input, ex);
			}

			return 0.0f;
		}


		private static string ClearString(string input)
		{
			return input.Replace("\r", "");
		}
        #endregion

        //getters
        public string[] GetColumnByHeader(string header)
        {
            List<string> column = new List<string>();
            int columnIndex = -1;
            for (int i = 0; i < ColumnsNumber; i++)
            {
                string currentHeader = Headers[i];
                if (currentHeader == header)
                {
                    columnIndex = i;
                    break;
                }
            }

            for (int i = 0; i < LinesNumber; i++)
            {
                column.Add(Lines[i][columnIndex]);
            }
            return column.ToArray();
        }


        public string GetValueByHeader(int lineNumber, string header)
        {
            string[] column = this.GetColumnByHeader(header);
            string result = column[lineNumber];
            return result;
        }


        public int GetIntByHeader(int lineNumber, string header, bool asIDString)
        {
            string stringValue = GetValueByHeader(lineNumber, header);
            if (asIDString)
            {
                stringValue = stringValue.Split('_')[1];
            }

            int result = ParseString(stringValue);
            return result;
        }

        public bool GetBoolByHeader(int lineNumber, string header)
        {
            string stringValue = GetValueByHeader(lineNumber, header);
            bool result = stringValue.ToLower() == "true";
            return result;
        }


        public float GetFloatByHeader(int lineNumber, string header, bool convertToComa)
        {
            string stringValue = GetValueByHeader(lineNumber, header);
            if (convertToComa)
            {
                stringValue = stringValue.Replace(".", ",");
            }

            float result;
            bool isParseComplete = float.TryParse(stringValue, out result);
            if (!isParseComplete)
            {
                Debug.LogErrorFormat("Can not parse float value from string: {0}. Maybe you need to change \"Convert To Coma\" value in Editor Settings Asset", stringValue);
                return float.MinValue;
            }
            return result;
        }


        public int ParseString(string input)
        {
            int result = -1;
            bool isParseComplete = Int32.TryParse(input, out result);
            if (!isParseComplete)
            {
                Debug.LogErrorFormat("Can not parse int value int string {0}", input);
                return int.MinValue;
            }
            return result;
        }


        public int[] GetIntArrayByHeader(int lineNumber, string header, char separator='|')
        {
            string stringValue = GetValueByHeader(lineNumber, header);
            string[] strings = stringValue.Split(separator);
            int[] result = new int[strings.Length];
            for (int i = 0; i < strings.Length; i++)
            {
                result[i] = ParseString(strings[i]);
            }

            return result;            
        }


        public string[] Headers { get { return headers; } }
        public List<string[]> Lines { get { return lines; } }
        public int ColumnsNumber { get { return columnsNumber; } }
        public int LinesNumber { get { return linesNumber; } }

        //fields
        private string[] headers;
        private List<string[]> lines;
        private int columnsNumber;
        private int linesNumber;


        public SV()
        {
            lines = new List<string[]>();
        }


        public SV(string inputTextString, string splitter)
        {
            lines = new List<string[]>();
            string[] splitedLines = inputTextString.Split('\n');
            if (splitedLines.Length <= 1)
                return;

            string headerLine = splitedLines[0];
            headerLine = headerLine.Replace("\r", "");
            this.headers = headerLine.Split(splitter);

            columnsNumber = headers.Length;
            linesNumber = splitedLines.Length - 1;
            for (int i = 1; i < splitedLines.Length; i++)
            {
                string line = splitedLines[i];
                line = line.Replace("\r", "");
                //line = line.Replace(",", ".");
                string[] tabbedSting = line.Split(splitter);
                lines.Add(tabbedSting);
            }
        }
    }
}
