﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Text;
using MTBundles;
using MTCore;
using System.Net;
using MTTown;
using System.Threading.Tasks;
using System;

namespace MTParameters
{
	[CustomEditor(typeof(ParametersHolder))]
	public class ParametersProcessor : Editor
	{
		public static readonly string PATH_PARAMS_JSON = "Assets/Parameters/Resources/gp.json";
		public static readonly string PATH_PARAMS_MANIFEST = "Assets/Parameters/Resources/gpManifest.json";
		public static readonly string PATH_ASSET = "Assets/Parameters/GameParameters.asset";
		public static readonly string PATH_ASSET_COPY = "Assets/Parameters/GameParametersUpload.asset";
		public delegate T ElementDelegate<T>(SV sv, int index) where T : BaseParameters;

		public static ParametersHolder ParametersHolder
		{
			get
			{
				return AssetDatabase.LoadAssetAtPath<ParametersHolder>(PATH_ASSET);
			}
		}


		private static List<T> GetParametersListFromSV<T>(string svPath, ElementDelegate<T> element) where T : BaseParameters
		{
			SV sv = GetSV(svPath, EditorSettings.Settings.Splitter);
			List<T> lst = new List<T>();
			for (int i = 0; i < sv.Lines.Count; i++)
			{
				T el = element(sv, i);
				lst.Add(el);
			}
			return lst;
		}


		[MenuItem("MT/BuildHub/_Update SV")]
		public static async Task ProcessParamsCompele()
		{
			try
			{
				int totalSteps = 7;  // Number of steps in the process
				int currentStep = 0;

				EditorUtility.DisplayProgressBar("Processing Parameters", "Updating TranslationSV...", currentStep / (float)totalSteps);
				UpdateTranslationSV();
				currentStep++;

				EditorUtility.DisplayProgressBar("Processing Parameters", "Updating TownsSV...", currentStep / (float)totalSteps);
				UpdateTownsSV();
				currentStep++;

				EditorUtility.DisplayProgressBar("Processing Parameters", "Updating ResourcesSV...", currentStep / (float)totalSteps);
				UpdateResourcesSV();
				currentStep++;

				EditorUtility.DisplayProgressBar("Processing Parameters", "Updating TrainSV...", currentStep / (float)totalSteps);
				UpdateTrainSV();
				currentStep++;

				EditorUtility.DisplayProgressBar("Processing Parameters", "Updating CoachSV...", currentStep / (float)totalSteps);
				UpdateCoachSV();
				currentStep++;

				EditorUtility.DisplayProgressBar("Processing Parameters", "Updating TownBuildingsSV...", currentStep / (float)totalSteps);
				UpdateTownBuildingsSV();
				currentStep++;

				EditorUtility.DisplayProgressBar("Processing Parameters", "Updating CapibarasSV...", currentStep / (float)totalSteps);
				UpdateCapibarasSV();
				currentStep++;

				// Update the progress bar for the async part
				EditorUtility.DisplayProgressBar("Processing Parameters", "Processing remaining parameters...", 1f);
				await ProcessParameters(false);
			}
			finally
			{
				// Clear the progress bar
				EditorUtility.ClearProgressBar();
			}
		}


		[MenuItem("MT/Process Game Parameters")]
		public static async Task ProcessParametersWithCheck()
		{
			await ProcessParameters(true);
		}
		public static async Task ProcessParameters(bool withCheck)
		{
			try
			{
				EditorUtility.DisplayProgressBar("Processing Parameters", "Loading Assets...", 0.1f);
				Debug.Log("Loading ParametersHolder and GameParameters...");
				ParametersHolder parametersHolder = AssetDatabase.LoadAssetAtPath<ParametersHolder>(PATH_ASSET);
				GameParameters parameters = parametersHolder.gameParameters;

				#region CSVs
				Debug.Log("Starting CSV processing...");

				// Process Languages
				EditorUtility.DisplayProgressBar("Processing Parameters", "Updating Languages...", 0.2f);
				SV languagesCSV = GetSV(GetParametersTable("translation"), EditorSettings.Settings.Splitter);
				List<Language> languages = new List<Language>();
				string[] keys = languagesCSV.GetColumnByHeader("Key");
				for (int i = 1; i < languagesCSV.ColumnsNumber; i++)
				{
					string name = languagesCSV.Headers[i];
					string[] values = languagesCSV.GetColumnByHeader(name);
					languages.Add(new Language(name, keys, values));
				}
				Debug.Log("Languages processed.");

				// Process Towns
				EditorUtility.DisplayProgressBar("Processing Parameters", "Updating Towns...", 0.3f);
				List<Town> towns = GetParametersListFromSV<Town>(GetParametersTable("towns"),
					(SV csv, int index) =>
					{
						int id = csv.GetIntByHeader(index, "id", true);
						string name = csv.GetValueByHeader(index, "Name");
						string translationKey = csv.GetValueByHeader(index, "TranslationKey");
                        string prefabBundleName = csv.GetValueByHeader(index, "PrefabBundleName");
                        int[] resourcesIDs = csv.GetIntArrayByHeader(index, "Resources");
						int[] neighboursIDs = csv.GetIntArrayByHeader(index, "NeighboursID");
						int[] buildings = csv.GetIntArrayByHeader(index, "Buildings");
						int[] depotIDs = csv.GetIntArrayByHeader(index, "DepotIDs");
						return new Town(id, name, translationKey, prefabBundleName, resourcesIDs, neighboursIDs, buildings, depotIDs);
					});
				Debug.Log("Towns processed.");

				// Process Town Buildings
				EditorUtility.DisplayProgressBar("Processing Parameters", "Updating Town Buildings...", 0.4f);
				List<TownBuilding> townBuildings = GetParametersListFromSV<TownBuilding>(GetParametersTable("townBuildings"),
					(SV csv, int index) =>
					{
						int id = csv.GetIntByHeader(index, "id", true);
						string translationKey = csv.GetValueByHeader(index, "TranslationKey");
						int[] resourcesIndexes = csv.GetIntArrayByHeader(index, "Resources");
						return new TownBuilding(id, translationKey, resourcesIndexes);
					});
				Debug.Log("Town Buildings processed.");

				// Process Resources
				EditorUtility.DisplayProgressBar("Processing Parameters", "Updating Resources...", 0.5f);
				List<Resource> resources = GetParametersListFromSV<Resource>(GetParametersTable("resources"),
					(SV csv, int index) =>
					{
						int id = csv.GetIntByHeader(index, "id", true);
						string name = csv.GetValueByHeader(index, "Name");
						string translationKey = csv.GetValueByHeader(index, "TranslationKey");
						bool isCargo = csv.GetBoolByHeader(index, "IsCargo");
						bool showInUICargoCounters = csv.GetBoolByHeader(index, "ShowInUICargoCounters");
						int resultsOrder = csv.GetIntByHeader(index, "ResultsOrder", false);
						return new Resource(id, name, translationKey, isCargo, showInUICargoCounters, resultsOrder);
					});
				Debug.Log("Resources processed.");

				// Process Trains
				EditorUtility.DisplayProgressBar("Processing Parameters", "Updating Trains...", 0.6f);
				List<Train> trains = GetParametersListFromSV<Train>(GetParametersTable("locomotives"),
					(SV csv, int index) =>
					{
						int id = csv.GetIntByHeader(index, "id", true);
						string name = csv.GetValueByHeader(index, "Name");
						string prefabBundleName = csv.GetValueByHeader(index, "PrefabBundleName");
						float speedFactor = csv.GetFloatByHeader(index, "SpeedFactor", EditorSettings.Settings.ConvertToComa);
						float speed = csv.GetFloatByHeader(index, "Speed", EditorSettings.Settings.ConvertToComa);
						float speedIncrease = csv.GetFloatByHeader(index, "SpeedIncrease", EditorSettings.Settings.ConvertToComa);
						float overSpeedFactor = csv.GetFloatByHeader(index, "OverSpeedFactor", EditorSettings.Settings.ConvertToComa);
						float flipTime = csv.GetFloatByHeader(index, "FlipTime", EditorSettings.Settings.ConvertToComa);
						int hitPoints = csv.GetIntByHeader(index, "HitPoints", false);
						int price = csv.GetIntByHeader(index, "Price", false);
						int cogCoinsPrice = csv.GetIntByHeader(index, "CogCoinsPrice", false);
						string indispensableCoach = csv.GetValueByHeader(index, "IndispensableCoach");
						float tractionPower = csv.GetFloatByHeader(index, "TractionPower", false);
						bool isExpress = csv.GetBoolByHeader(index, "Express");
						string type = csv.GetValueByHeader(index, "Type");
						string model = csv.GetValueByHeader(index, "Model");
						return new Train(id, name, prefabBundleName, speedFactor, speed, speedIncrease, overSpeedFactor, flipTime, indispensableCoach, tractionPower, hitPoints, price, cogCoinsPrice, isExpress, type, model);
					});
				Debug.Log("Trains processed.");

				// Process Coaches
				EditorUtility.DisplayProgressBar("Processing Parameters", "Updating Coaches...", 0.7f);
				List<Coach> coaches = GetParametersListFromSV<Coach>(GetParametersTable("coaches"),
					(SV csv, int index) =>
					{
						int id = csv.GetIntByHeader(index, "id", true);
						string name = csv.GetValueByHeader(index, "Name");
                        string prefabBundleName = csv.GetValueByHeader(index, "PrefabBundleName");
                        int[] resourcesArray = csv.GetIntArrayByHeader(index, "Resources");
						int price = csv.GetIntByHeader(index, "Price", false);
						int cogCoinsPrice = csv.GetIntByHeader(index, "CogCoinsPrice", false);
						bool isExpress = csv.GetBoolByHeader(index, "Express");
						string type = csv.GetValueByHeader(index, "Type");
						string model = csv.GetValueByHeader(index, "Model");
						int capacity = csv.GetIntByHeader(index, "Capacity", false);
						float weight = csv.GetFloatByHeader(index, "Weight", false);
						return new Coach(id, name, prefabBundleName, resourcesArray, price, cogCoinsPrice, isExpress, type, model, capacity, weight);
					});
				Debug.Log("Coaches processed.");

				// Process Capibaras
				EditorUtility.DisplayProgressBar("Processing Parameters", "Updating Capibaras...", 0.8f);
				List<MTCapibara> capibaras = GetParametersListFromSV<MTCapibara>(GetParametersTable("capibaras"),
					(SV csv, int index) =>
					{
						string id = csv.GetValueByHeader(index, "id");
						int character = csv.GetIntByHeader(index, "Character", false);
						int priority = csv.GetIntByHeader(index, "Priority", false);
						string breed = csv.GetValueByHeader(index, "Breed");
						string features = csv.GetValueByHeader(index, "Features");
						string targets = csv.GetValueByHeader(index, "Targets");
						string rewards = csv.GetValueByHeader(index, "Rewards");
						string rewarder = csv.GetValueByHeader(index, "Rewarder");
						bool isActive = csv.GetBoolByHeader(index, "IsActive");
						return MTCapibara.Create(id, character, breed, features, targets, rewards, rewarder, priority, isActive);
					});
				Debug.Log("Capibaras processed.");

				parameters.SetTables(languages, towns, resources, trains, coaches, townBuildings, capibaras);
				Debug.Log("All tables have been set to GameParameters.");
				#endregion

				#region Comparison with current json
				EditorUtility.DisplayProgressBar("Processing Parameters", "Comparing with current JSON...", 0.9f);
				string text = GetText(PATH_PARAMS_JSON);
				if (text != null)
				{
					GameParameters resourcesParams = GameParameters.FromJson<GameParameters>(text);
					if (!withCheck || parameters.ToString().GetHashCode() != resourcesParams.ToString().GetHashCode())
					{
						Debug.Log("Deleting old JSON files...");
						FileUtil.DeleteFileOrDirectory(PATH_PARAMS_JSON);
						FileUtil.DeleteFileOrDirectory(PATH_PARAMS_MANIFEST);

						Debug.Log("Creating new JSON files...");
						parameters.Write(PATH_PARAMS_JSON);
						JsonManifest gameParametersJsonManifest = parametersHolder.UpdateManifestAndPlusVersion<GameParameters>(parameters, PATH_PARAMS_MANIFEST);

						string message = $"GameParameters Are Not Equal\n{PATH_PARAMS_JSON} and {PATH_ASSET}\nSend Params to server?";
						
						if (!withCheck || EditorUtility.DisplayDialog("Process Parameters", message, "Send", "Cancel"))
						{
							Debug.Log("Sending parameters to server...");

							byte[] bytesParameters = parameters.GetBytes();
							byte[] bytesManifest = gameParametersJsonManifest.GetBytes();

							var bsm = new BundlesServerManager();
							EditorUtility.DisplayProgressBar("Processing Parameters", "Sending parameters to server...", 1.0f);
							await bsm.SendParamsToServerAsync(InBuildParameters.InBuildParametersInstance.GameParameters, InBuildParameters.InBuildParametersInstance.GameParametersManifest, bytesParameters, bytesManifest);
							Debug.Log("Parameters successfully sent to server.");
						}
					}
				}
				#endregion

				AssetDatabase.Refresh();
				Debug.Log("Asset Database refreshed.");
			}
			catch (Exception e)
			{
				Debug.LogError(e);
			}
			finally
			{
				EditorUtility.ClearProgressBar();
				Debug.Log("Process Parameters completed.");
			}
		}

		[MenuItem("MT/Update Tables/Translation")]
		public static void UpdateTranslationSV()
		{
			UpdateSV("translation");
		}


		[MenuItem("MT/Update Tables/Towns")]
		public static void UpdateTownsSV()
		{
			UpdateSV("towns");
		}


		[MenuItem("MT/Update Tables/Resources")]
		public static void UpdateResourcesSV()
		{
			UpdateSV("resources");
		}

		[MenuItem("MT/Update Tables/Trains")]
		public static void UpdateTrainSV()
		{
			UpdateSV("locomotives");
		}

		[MenuItem("MT/Update Tables/Coaches")]
		public static void UpdateCoachSV()
		{
			UpdateSV("coaches");
		}


		[MenuItem("MT/Update Tables/TownBuildings")]
		public static void UpdateTownBuildingsSV()
		{
			UpdateSV("townBuildings");
		}


		[MenuItem("MT/Update Tables/Capibaras")]
		public static void UpdateCapibarasSV()
		{
			UpdateSV("capibaras");
		}

		private static string GetPattern(string fileName)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(@".+");
			sb.Append(fileName);
			sb.Append(@".*\.");
			sb.Append(EditorSettings.Settings.Extension);
			sb.Append("$");
			return sb.ToString();
		}

		private static string GetParametersTable(string fileName)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("Assets/Parameters/sv/");
			sb.Append(fileName);
			sb.Append(".txt");
			return sb.ToString();
		}


		private static void UpdateSV(string name)
		{
			var inBuildPath = GetParametersTable(name);
			var namePattern = GetPattern(name);

			Debug.LogFormat("Trying to Update csv by pattern: {0}", namePattern);

			string inBuildAbsolutePath = EditorUtilities.RelativePathToAbsolute(inBuildPath);
			bool inBuildExists = File.Exists(inBuildAbsolutePath);

			DirectoryInfo di = Directory.GetParent(Application.dataPath);
			string csvFolder = Path.Combine(di.ToString(), EditorSettings.Settings.DownloadPath);

			if (!Directory.Exists(csvFolder))
			{
				Directory.CreateDirectory(csvFolder);
			}

			DownloadGoogleSheet($"{EditorSettings.Settings.DownloadPath}/{name}.{EditorSettings.Settings.Extension}", EditorSettings.Settings.FileId, EditorSettings.Settings.GetGidByName(name), EditorSettings.Settings.Extension);

			string latestDonwloadedCSV = EditorUtilities.GetLatestFile(csvFolder, namePattern);
			if (string.IsNullOrEmpty(latestDonwloadedCSV))
			{
				Debug.LogWarningFormat("No latest sv found for pattern {0} among downloads \n{1}", namePattern, EditorSettings.Settings.DownloadPath);
				return;
			}
			bool latestDownloadedCSVExists = !string.IsNullOrEmpty(latestDonwloadedCSV);
			if (!latestDownloadedCSVExists && !inBuildExists)
			{
				Debug.LogErrorFormat("No sv found in folder {0} by pattern: {1} No In-Build sv: {2} either",
					csvFolder, namePattern, inBuildPath);
				return;
			}
			//no in-build - overwrite
			if (!inBuildExists)
			{
				EditorUtilities.ReplaceAssetByExtraFile(latestDonwloadedCSV, inBuildPath);
				Debug.LogFormat("No In-Build file found, using latest downloaded SV {0}", latestDonwloadedCSV);
			}
			//in-build present, need to compare
			else
			{
				Debug.LogFormat("Working with dowloaded sv: {0}", latestDonwloadedCSV);
				bool isNewer = EditorUtilities.IsNewer(latestDonwloadedCSV, inBuildAbsolutePath);
				if (isNewer)
				{
					bool identical = EditorUtilities.AreIdenticalByMD5(latestDonwloadedCSV, inBuildAbsolutePath);
					if (!identical)
					{
						EditorUtilities.ReplaceAssetByExtraFile(latestDonwloadedCSV, inBuildPath);
						Debug.LogFormat("Replacing: {0} by {1}", inBuildPath, latestDonwloadedCSV);
					}
				}
			}
			Debug.LogFormat("Done!");
		}


		private static string GetText(string path)
		{
			TextAsset textAsset = AssetDatabase.LoadAssetAtPath<TextAsset>(path);
			if (textAsset == null)
			{
				Debug.LogErrorFormat("Can not load text asset at path: {0}", path);
				return null;
			}

			string resultString = textAsset.text;
			if (string.IsNullOrEmpty(resultString))
			{
				Debug.LogErrorFormat("Empty text asset at path: ", path);
				return null;
			}
			return resultString;
		}


		private static SV GetSV(string path, string splitter)
		{
			string inputString = GetText(path);
			if (inputString == null)
				return null;

			SV csv = new SV(inputString, splitter);
			return csv;
		}
		private static void DownloadGoogleSheet(string fileName, string fileID, string gid, string format = "tsv")
		{
			string url = $"https://docs.google.com/spreadsheets/d/{fileID}/export?format={format}&gid={gid}";

			using (WebClient client = new WebClient())
			{
				try
				{
					string path = Path.Combine(Directory.GetParent(Application.dataPath).ToString(), fileName);
					Debug.Log("Download Google Sheet from " + url);

					client.DownloadFile(url, path);
					AssetDatabase.Refresh();
					Debug.Log("Downloaded Google Sheet to " + path);
				}
				catch (System.Exception e)
				{
					Debug.LogError("Failed to download Google Sheet: " + e.Message);
				}
			}
		}
		[MenuItem("MT/BuildHub/_Build_Resource_Enum")]
		public static void GenerateEnumFromResources()
		{
			try
			{
				string enumName = "MTResources";

				// Path to save the generated enum
				string enumFilePath = Path.Combine(Application.dataPath, "Scripts", $"{enumName}.cs");

				// Ensure the directory exists
				string directory = Path.GetDirectoryName(enumFilePath);
				if (!Directory.Exists(directory))
				{
					Directory.CreateDirectory(directory);
				}

				// Start building the enum
				StringBuilder enumBuilder = new StringBuilder();
				enumBuilder.AppendLine("public enum " + enumName);
				enumBuilder.AppendLine("{");

				// Process Resources
				EditorUtility.DisplayProgressBar("Processing Parameters", "Updating Resources...", 0.5f);
				List<Resource> resources = GetParametersListFromSV<Resource>(GetParametersTable("resources"),
					(SV csv, int index) =>
					{
						int id = csv.GetIntByHeader(index, "id", true);
						string name = csv.GetValueByHeader(index, "Name");
						string translationKey = csv.GetValueByHeader(index, "TranslationKey");
						bool isCargo = csv.GetBoolByHeader(index, "IsCargo");
						bool showInUICargoCounters = csv.GetBoolByHeader(index, "ShowInUICargoCounters");
						int resultsOrder = csv.GetIntByHeader(index, "ResultsOrder", false);
						return new Resource(id, name, translationKey, isCargo, showInUICargoCounters, resultsOrder);
					});

				foreach (var resource in resources)
				{
					string resourceName = resource.Name.Replace(" ", "_").Replace("/", "_").Replace("\\", "_");
					enumBuilder.AppendLine($"    {resourceName} = {resource.ID}, // TranslationKey: {resource.TranslationKey}, IsCargo: {resource.IsCargo}, ShowInUICargoCounters: {resource.ShowInUICargoCounters}, ResultsOrder: {resource.ResultsOrder}");
				}

				enumBuilder.AppendLine("}");

				// Write the enum to a file
				File.WriteAllText(enumFilePath, enumBuilder.ToString());
				Debug.Log($"Enum {enumName} generated at: {enumFilePath}");

				// Refresh assets in the project
				AssetDatabase.Refresh();
			}
			finally
			{
				EditorUtility.ClearProgressBar();
			}
		}

	}
}