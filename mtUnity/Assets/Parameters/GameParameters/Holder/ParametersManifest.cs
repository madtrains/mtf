﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace MTParameters
{
    [System.Serializable]
    public class GameParametersManifest
	{
		public int VersionMajor { get { return versionMajor; } }
		public int VersionMinor { get { return versionMinor; } }
		public int Hash { get { return hash; } set { hash = value; } }
		public string Date { get { return date; } set { date = value; } }


		public static int ParametersToHash(GameParameters parameters)
		{
			return parameters.ToString().GetHashCode();
		}


		public GameParametersManifest(GameParameters parameters, int versionMajor, int versionMinor)
		{
			this.SetNowTime();
			this.hash = ParametersToHash(parameters);
			this.versionMajor = versionMajor;
			this.versionMinor = versionMinor;
		}


		public void Write(string path)
		{
			string stringManifest = JsonUtility.ToJson(this);
			byte[] bytesManifest = Encoding.UTF8.GetBytes(stringManifest);
			File.WriteAllBytes(path, bytesManifest);
		}


		public string Version
		{
			get
			{
				StringBuilder st = new StringBuilder();
				st.Append(VersionMajor);
				st.Append(".");
				st.Append(VersionMinor);
				return st.ToString();
			}
		}


	


		[SerializeField] private int versionMajor;
		[SerializeField] private int versionMinor;
		[SerializeField] private int hash;
		[SerializeField] private string date;


		public void SetNowTime()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(DateTime.Now.Year);
			stringBuilder.Append(".");
			stringBuilder.Append(DateTime.Now.Month.ToString("00"));
			stringBuilder.Append(".");
			stringBuilder.Append(DateTime.Now.Day.ToString("00"));
			stringBuilder.Append("\t");
			stringBuilder.Append(DateTime.Now.Hour.ToString("00"));
			stringBuilder.Append(":");
			stringBuilder.Append(DateTime.Now.Minute.ToString("00"));
			stringBuilder.Append(":");
			stringBuilder.Append(DateTime.Now.Second.ToString("00"));
			this.date = stringBuilder.ToString();
			Debug.LogFormat("GameParameters date set: {0}", date);
		}


		public bool IsFresher(GameParametersManifest other)
		{
			if (this.VersionMajor > other.VersionMajor)
				return true;
			if (this.VersionMinor > other.VersionMinor)
				return true;
			return false;
		}


		public void IncreaseVersionMajor()
		{
			versionMajor++;
		}


		public void IncreaseVersionMinor()
		{
			versionMinor++;
		}


		public void SetVersionManually(int major, int minor)
		{
			this.versionMajor = major;
			this.versionMinor = minor;
		}


		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("version:");
			stringBuilder.Append(versionMajor);
			stringBuilder.Append(".");
			stringBuilder.Append(versionMinor);
			stringBuilder.Append("\n");
			stringBuilder.Append("hash:");
			stringBuilder.Append(hash);
			stringBuilder.Append("\n");
			stringBuilder.Append("time:");
			stringBuilder.Append(this.date);
			string stringManifest = stringBuilder.ToString();
			return stringManifest;
		}
	}
}