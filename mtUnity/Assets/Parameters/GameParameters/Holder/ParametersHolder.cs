﻿using UnityEngine;

namespace MTParameters
{
    [CreateAssetMenu(menuName = "Game Parameters")]
    public class ParametersHolder : ProtoHolder
	{
        public GameParameters gameParameters;

		public void Update()
		{
			
		}


        protected override void OnValidate()
        {
            base.OnValidate();
            Update();
        }
	}
}