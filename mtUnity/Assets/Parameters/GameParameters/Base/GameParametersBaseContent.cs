﻿using MTCore;
using System;
using UnityEngine;

namespace MTParameters
{
	public partial class GameParametersBase
	{
		public int CoachesNumber { get { return coachesNumber; } }
		public CameraParameters[] Cameras { get { return cameras; } }
		public TrainShared TrainShared { get { return trainShared; } }
		public UIMainParameters UIMainParameters { get { return uIMainParameters; } }
		public Cargos Cargo { get { return cargos; } }
		public Language[] Languages { get { return languges; } }
		public Town[] Towns { get { return towns; } }
		public Resource[] Resources { get { return resources; } }
		public Train[] Trains { get { return trains; } }
		public Coach[] Coaches { get { return coaches; } }
		public TownParameters TownParameters { get { return townParameters; } }
		public TownBuilding[] TownBuildings { get { return townBuildings; } }
		public MTCapibara[] Capibaras { get { return capibaras; } }
		public Miscellaneous Miscellaneous { get { return miscellaneous; } }
		public Kreator.Kreator[] Kreator { get { return kreator; } }
		public TownTrips TownTrips { get { return townTrips; } }
		public MTParameters.Flipper.FlipperParameters FlipperParameters { get { return flipper; } }
		public SFD.SFDParameters SFDParameters { get { return sfd; } }
		public Customization Customization { get { return customization; } }

		[SerializeField][Range(3, 12)] private int coachesNumber;
		[SerializeField] private CameraParameters[] cameras;
		[SerializeField] private TrainShared trainShared;
		[SerializeField] private Cargos cargos;
		[SerializeField] private UIMainParameters uIMainParameters;
		[SerializeField] private TownParameters townParameters;
		[SerializeField] private Miscellaneous miscellaneous;
		[SerializeField] private Kreator.Kreator[] kreator;
		[SerializeField] private MTParameters.TownTrips townTrips;
		[SerializeField] private MTParameters.Flipper.FlipperParameters flipper;
		[SerializeField] private SFD.SFDParameters sfd;
		[SerializeField] private Customization customization;

		//tables content
		[HideInInspector][SerializeField] private Language[] languges;
		[HideInInspector][SerializeField] private Town[] towns;
		[HideInInspector][SerializeField] private Resource[] resources;
		[HideInInspector][SerializeField] private Train[] trains;
		[HideInInspector][SerializeField] private Coach[] coaches;
		[HideInInspector][SerializeField] private TownBuilding[] townBuildings;
		[HideInInspector][SerializeField] private MTCapibara[] capibaras;


		public delegate void CycleResourcesIDsDel(int ID);

		public void CycleResourceIDs(CycleResourcesIDsDel cycleResourcesIDs, bool cargosOnly = false)
		{
			for (int i = 0; i < GameStarter.GameParameters.Resources.Length; i++)
			{
				Resource resource = GameStarter.GameParameters.Resources[i];
				int resourceID = resource.ID;
				if (cargosOnly && !resource.IsCargo)
					continue;
				cycleResourcesIDs(resourceID);
			}
		}



		public void ApplyCameraParameters(int index, CameraParameters parameters)
		{
			cameras[index] = parameters;
		}
	}
}