﻿using MTCore;
using System.Collections.Generic;
using UnityEngine;

namespace MTParameters
{
    [System.Serializable]
    public partial class GameParametersBase : ProtoParameters
    {
        public static T GetElement<T>(T[] targetArray, string name) where T : NamedParameters
        {
            foreach (T element in targetArray)
            {
                if (element.Name == name)
                    return element;
            }
            return null;
        }


        private Dictionary<MTCore.TripSize, List<MTParameters.Flipper.TripContent>> roadMap;

        public virtual void LoadPostProcess()
        {
            for (int i = 0; i < languges.Length; i++)
            {
                languges[i].CreateStringsDict();
            }
            roadMap = new Dictionary<MTCore.TripSize, List<MTParameters.Flipper.TripContent>>();
            foreach(MTParameters.Flipper.TripContent trip in this.flipper.Trips)
            {
                if (!roadMap.ContainsKey(trip.TripSize))
                    roadMap[trip.TripSize] = new List<MTParameters.Flipper.TripContent> ();
                roadMap[trip.TripSize].Add(trip);
            }
        }

        public MTParameters.Flipper.TripContent GetRandomTripContent(MTCore.TripSize tripSize)
        {
            List<MTParameters.Flipper.TripContent> tc = roadMap[tripSize];
            return tc[Random.Range(0, tc.Count)];
        }

        public void SetTables(List<Language> languages, List<Town> towns, 
            List<Resource> resources,
            List<Train> trains, List<Coach> coaches, 
            List<TownBuilding> townBuildings,
            List<MTCapibara> capibaras)
        {
            this.languges = languages.ToArray();
            this.towns = towns.ToArray();
            this.resources = resources.ToArray();
            this.trains = trains.ToArray();
            this.coaches = coaches.ToArray();
            this.capibaras = capibaras.ToArray();
        }
    }
}