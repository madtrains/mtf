﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTParameters
{
    [System.Serializable]
    public class UIMainParameters : BaseParameters
	{
		public float ButtonTransitionSpeed { get { return buttonTransitionSpeed; } }
		public float ButtonMinimalScale { get { return buttonMinimalScale; } }

		[Tooltip("How Fast will be button scaled")]
		[SerializeField] private float buttonTransitionSpeed = 1f;
		[SerializeField] private float buttonMinimalScale = 0.8f;

		//[Tooltip("How Many Carriages we need to place on correct rails to flip? If 0 - only train")]
		//[Range(0,2)]
	}
}