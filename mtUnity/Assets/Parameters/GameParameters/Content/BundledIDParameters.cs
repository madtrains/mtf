﻿using UnityEngine;

namespace MTParameters
{
    [System.Serializable]
	public class BundledIDParameters : NamedIDParameters
	{
		public string MainBundle { get { return mainBundle; } }

		[SerializeField] protected string mainBundle;

		public BundledIDParameters(int id, string name, string mainBundle)
			: base (id, name)
        {
			this.mainBundle = mainBundle;
        }
	}
}