﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTParameters
{
    [System.Serializable]
    public class TownBuilding : IDParameters
	{
        public TownBuilding(int id, string translationKey, int[] resources) : base (id)
        {
            this.translationKey = translationKey;
            this.resources = resources;
        }


        public string TranslationKey { get { return translationKey; } }
        public int[] Resources { get { return resources; } }

        [SerializeField] private string translationKey;
        [SerializeField] private int[] resources;
    }
}