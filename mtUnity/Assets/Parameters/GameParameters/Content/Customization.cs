﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTParameters
{
    [System.Serializable]
    public class Customization : BaseParameters
    {
        public Color GetColor(int index)
        {
            return colors[index];
        }

        public Color[] Colors { get { return colors; } }

        [SerializeField] private Color[] colors;

        public static Color RandomNamedColor { get { return NamedColors[Random.Range(0, NamedColors.Length)].Color; } }

        public static Color256[] NamedColors = new Color256[]
        {
            new Color256("maroon", 128f, 0f, 0f),
            new Color256("maroon", 128f, 0f, 0f),
            new Color256("dark red", 139f, 0f, 0f),
            new Color256("brown", 165f, 42f, 42f),
            new Color256("firebrick", 178f, 34f, 34f),
            new Color256("crimson", 220f, 20f, 60f),
            new Color256("red", 255f, 0f, 0f),
            new Color256("tomato", 255f, 99f, 71f),
            new Color256("namecoral ", 255f, 127f, 80f),
            new Color256("indian red", 205f, 92f, 92f),
            new Color256("light coral", 240f, 128f, 128f),
            new Color256("dark salmon", 233f, 150f, 122f),
            new Color256("salmon", 250f, 128f, 114f),
            new Color256("light salmon", 255f, 160f, 122f),
            new Color256("orange red", 255f,69f, 0f),
            new Color256("dark orange", 255f, 140f, 0f),
            new Color256("orange", 255f, 165f, 0f),
            new Color256("gold", 255f, 215f, 0f),
            new Color256("dark golden rod", 184f, 134f, 11f),
            new Color256("golden rod", 218f, 165f, 32f),
            new Color256("pale golden rod", 238f, 232f, 170f),
            new Color256("dark khaki", 189f, 183f, 107f),
            new Color256("khaki", 240f, 230f, 140f),
            new Color256("olive", 128f, 128f, 0f),
            new Color256("yellow", 255f, 255f  , 0f),
            new Color256("yellow green", 154f, 205f  , 50f),
            new Color256("dark olive green", 85f, 107f  , 47f),
            new Color256("olive drab", 107f, 142f  , 35f),
            new Color256("lawn green", 124f, 252f  , 0f),
            new Color256("chart reuse", 127f, 255f, 0f),
            new Color256("green yellow", 173f, 255f, 47f),
            new Color256("dark green", 0f, 100f  , 0f),
            new Color256("green", 0f, 128f  , 0f),
            new Color256("forest green", 34f, 139f  , 34f),
            new Color256("lime", 0f, 255f  , 0f),
            new Color256("lime green", 50f, 205f  , 50f),
            new Color256("light green", 144f, 238f  , 144f),
            new Color256("pale green", 152f, 251f  , 152f),
            new Color256("dark sea green", 143f, 188f  , 143f),
            new Color256("medium spring green", 0f, 250f  , 154f),
            new Color256("spring green", 0f, 255f  , 127f),
            new Color256("sea green", 46f, 139f  , 87f),
            new Color256("medium aqua marine", 102f, 205f  , 170f),
            new Color256("medium sea green", 60f, 179f  , 113f),
            new Color256("light sea green", 32f, 178f  , 170f),
            new Color256("dark slate gray", 47f, 79f  , 79f),
            new Color256("teal", 0f, 128f  , 128f),
            new Color256("dark cyan", 0f, 139f  , 139f),
            new Color256("aqua", 0f, 255f  , 255f),
            new Color256("cyan", 0f, 255f  , 255f),
            new Color256("light cyan", 224f, 255f  , 255f),
            new Color256("dark turquoise", 0f, 206f  , 209f),
            new Color256("turquoise", 64f, 224f  , 208f),
            new Color256("medium turquoise", 72f, 209f  , 204f),
            new Color256("pale turquoise", 175f, 238f  , 238f),
            new Color256("aqua marine", 127f, 255f  , 212f),
            new Color256("powder blue", 176f, 224f  , 230f),
            new Color256("cadet blue", 95f, 158f  , 160f),
            new Color256("steel blue", 70f, 130f  , 180f),
            new Color256("corn flower blue", 100f, 149f  , 237f),
            new Color256("deep sky blue", 0f, 191f  , 255f),
            new Color256("dodger blue", 30f, 144f  , 255f),
            new Color256("light blue", 173f, 216f  , 230f),
            new Color256("sky blue", 135f, 206f  , 235f),
            new Color256("light sky blue", 135f, 206f  , 250f),
            new Color256("midnight blue", 25f, 25f  , 112f),
            new Color256("navy", 0f, 0f  , 128f),
            new Color256("dark blue", 0f, 0f  , 139f),
            new Color256("medium blue", 0f, 0f  , 205f),
            new Color256("blue", 0f, 0f  , 255f),
            new Color256("royal blue", 65f, 105f  , 225f),
            new Color256("blue violet", 138f, 43f  , 226f),
            new Color256("indigo", 75f, 0f  , 130f),
            new Color256("dark slate blue", 72f, 61f  , 139f),
            new Color256("slate blue", 106f, 90f  , 205f),
            new Color256("medium slate blue", 123f, 104f  , 238f),
            new Color256("medium purple", 147f, 112f  , 219f),
            new Color256("dark magenta", 139f, 0f  , 139f),
            new Color256("dark violet", 148f, 0f  , 211f),
            new Color256("dark orchid", 153f, 50f  , 204f),
            new Color256("medium orchid", 186f, 85f  , 211f),
            new Color256("purple", 128f, 0f  , 128f),
            new Color256("thistle", 216f, 191f  , 216f),
            new Color256("plum", 221f, 160f  , 221f),
            new Color256("violet", 238f, 130f  , 238f),
            new Color256("magenta", 255f, 0f  , 255f),
            new Color256("orchid", 218f, 112f  , 214f),
            new Color256("medium violet red", 199f, 21f  , 133f),
            new Color256("pale violet red", 219f, 112f  , 147f),
            new Color256("deep pink", 255f, 20f  , 147f),
            new Color256("hot pink", 255f, 105f  , 180f),
            new Color256("light pink", 255f, 182f  , 193f),
            new Color256("pink", 255f, 192f  , 203f),
            new Color256("antique white", 250f, 235f  , 215f),
            new Color256("beige", 245f, 245f  , 220f),
            new Color256("bisque", 255f, 228f  , 196f),
            new Color256("blanched almond", 255f, 235f  , 205f),
            new Color256("wheat", 245f, 222f  , 179f),
            new Color256("corn silk", 255f, 248f  , 220f),
            new Color256("lemon chiffon", 255f, 250f  , 205f),
            new Color256("light golden rod yellow", 250f, 250f  , 210f),
            new Color256("light yellow", 255f, 255f  , 224f),
            new Color256("saddle brown", 139f, 69f  , 19f),
            new Color256("sienna", 160f, 82f  , 45f),
            new Color256("chocolate", 210f, 105f  , 30f),
            new Color256("peru", 205f, 133f  , 63f),
            new Color256("sandy brown", 244f, 164f  , 96f),
            new Color256("burly wood", 222f, 184f  , 135f),
            new Color256("tan", 210f, 180f  , 140f),
            new Color256("rosy brown", 188f, 143f  , 143f),
            new Color256("moccasin", 255f, 228f  , 181f),
            new Color256("navajo white", 255f, 222f  , 173f),
            new Color256("peach puff", 255f, 218f  , 185f),
            new Color256("misty rose", 255f, 228f  , 225f),
            new Color256("lavender blush", 255f, 240f  , 245f),
            new Color256("linen", 250f, 240f  , 230f),
            new Color256("old lace", 253f, 245f  , 230f),
            new Color256("papaya whip", 255f, 239f  , 213f),
            new Color256("sea shell", 255f, 245f  , 238f),
            new Color256("mint cream", 245f, 255f  , 250f),
            new Color256("slate gray", 112f, 128f  , 144f),
            new Color256("light slate gray", 119f, 136f  , 153f),
            new Color256("light steel blue", 176f, 196f  , 222f),
            new Color256("lavender", 230f, 230f  , 250f),
            new Color256("floral white", 255f, 250f  , 240f),
            new Color256("alice blue", 240f, 248f  , 255f),
            new Color256("ghost white", 248f, 248f  , 255f),
            new Color256("honeydew", 240f, 255f  , 240f),
            new Color256("ivory", 255f, 255f  , 240f),
            new Color256("azure", 240f, 255f  , 255f),
            new Color256("snow", 255f, 250f  , 250f),
            new Color256("black", 0f, 0f  , 0f),
            new Color256("dim grey", 105f, 105f  , 105f),
            new Color256("grey", 128f, 128f  , 128f),
            new Color256("dark grey", 169f, 169f  , 169f),
            new Color256("silver", 192f, 192f  , 192f),
            new Color256("light grey", 211f, 211f  , 211f),
            new Color256("gainsboro", 220f, 220f  , 220f),
            new Color256("white smoke", 245f, 245f  , 245f),
            new Color256("white", 255f, 255f  , 255f),
        };

    }

    public class Color256
    {
        public string name;
        public float r;
        public float g;
        public float b;

        public Color256(string name, float r, float g, float b)
        {
            this.name = name;
            this.r = r;
            this.g = g;
            this.b = b;
        }


        public Color Color { get { return new Color(Mathf.InverseLerp(0, 255f, r), Mathf.InverseLerp(0, 255f, g), Mathf.InverseLerp(0, 255f, b)); } }
    }
}