﻿using UnityEngine;

namespace MTParameters
{
    [System.Serializable]
	public class Probability : BaseParameters
	{
        

        [Tooltip("0: Always Disabled, 1: Always Enabled")]
        [SerializeField] [Range(0f, 1f)]protected float probability;

        public bool On
        {
            get
            {
                if (probability == 0)
                    return false;
                float randomValue = UnityEngine.Random.Range(0.01f, 1f);
                return probability >= randomValue;
            }
        }
    }
}