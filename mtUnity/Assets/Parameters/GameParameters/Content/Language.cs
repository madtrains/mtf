﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTParameters
{
    [System.Serializable]
    public class Language : NamedParameters
	{
        public Language(string name, string[] keys, string[] values)
        {
            this.name = name;
            this.keys = keys;
            this.values = values;
            CreateStringsDict();
        }

        public void CreateStringsDict()
        {
            this.stringsDict = new Dictionary<string, string>();
            for (int i = 0; i < keys.Length; i++)
            {
                this.stringsDict.Add(keys[i], values[i]);
            }
        }


        public string GetString(string key)
        {
            return stringsDict.ContainsKey(key)? stringsDict[key] : key;
        }


		public Dictionary<string, string> StringsDict { get { return stringsDict; } }

        private Dictionary<string, string> stringsDict;
        [SerializeField] private string[] keys;
        [SerializeField] private string[] values;
    }
}