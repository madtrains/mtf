﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTParameters
{
	[System.Serializable]
	public class NamedIDParameters : IDParameters
	{
		public string Name { get { return name; } }

		[SerializeField] protected string name;

		public NamedIDParameters(int id, string name) : base(id)
        {
			this.name = name;
			this.id = id;
        }
	}
}