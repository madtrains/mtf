﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTParameters
{
    [System.Serializable]
    public class Coach : BundledIDParameters
	{        
        public List<int> Resources { get { return resources; } }
        public int Price { get { return price; } }
        public int CogCoinsPrice { get { return cogCoinsPrice; } }
        public bool IsExpress { get { return isExpress; } }
        public string Type { get { return type; } }
        public string Model { get { return model; } }
        public int Capacity { get { return capacity; } }
        public float Weight { get { return weight; } }

        [SerializeField] private List<int> resources;
        [SerializeField] private int price;
        [SerializeField] private int cogCoinsPrice;
        [SerializeField] private bool isExpress;
        [SerializeField] private string type;
        [SerializeField] private string model;
        [SerializeField] private int capacity;
        [SerializeField] private float weight;


        public Coach(int id, string name, string bundleName, int[] resources, 
            int price, int cogCoinsPrice, bool isExpress, string type, string model, int capacity, float weight)
                :base(id, name, bundleName)
        {
            this.resources = new List<int>(resources);
            this.price = price;
            this.cogCoinsPrice = cogCoinsPrice;
            this.isExpress = isExpress;
            this.type = type;
            this.model = model;
            this.capacity = capacity;
            this.weight = weight;
        }
    }
}