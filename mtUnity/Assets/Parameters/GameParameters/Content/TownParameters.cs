﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTParameters
{
	[System.Serializable]
	public class TownParameters : BaseParameters
	{
        public float CameraFlySpeed { get { return cameraFlySpeed; } }
        public float CameraAngleSpeed { get { return cameraAngleSpeed; } }

        [SerializeField] private float cameraFlySpeed;
        [SerializeField] private float cameraAngleSpeed;
        [SerializeField] private float startCameraSpeed;


    }
}