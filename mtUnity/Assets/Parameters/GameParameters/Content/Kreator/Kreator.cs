﻿using System.Collections.Generic;
using UnityEngine;

namespace MTParameters.Kreator
{
    [System.Serializable]
    public class Kreator : NamedParameters
    {
        public int EmptyLineChunksNumber { get { return clearLineChunksNumber; } }
        [SerializeField] private int clearLineChunksNumber;

        public PickUpParams PickUpParams { get { return pickUp; } }
        [SerializeField] private PickUpParams pickUp;

        public DirectionsCollection Directions { get { return directions; } }
        [SerializeField] private DirectionsCollection directions;

        public float Density { get { return density; } }
        [SerializeField][Range(1f, 3f)] private float density = 1f;

        public BlockContent BlockContent { get { return blockContent; } }
        [SerializeField] private BlockContent blockContent;

        public LevelContent LevelContent { get { return levelContent; } }
        [SerializeField] private LevelContent levelContent;

        public Exit Exit { get { return exit; } }
        [SerializeField] private Exit exit;

        public int PassengersLimit { get { return passengersLimit; } }
        [Tooltip("Количество пассажиров, которые создаются на весь уровень, должно быть больше чем на самой большой станции")]
        [SerializeField] private int passengersLimit;
    }

    [System.Serializable]
    public class PickUpParams
    {
        public float Percentage { get { return percentage; } }
        [SerializeField][Range(0.05f, 1f)] private float percentage;

        public float CoinsPercentage { get { return coinsPercentage; } }
        [SerializeField][Range(0f, 1f)] private float coinsPercentage;

        public int SilverCoins { get { return silverCoins; } }
        [SerializeField][Range(0f, 50)] private int silverCoins;

        public Probability Ticket { get { return ticket; } }
        [SerializeField] private Probability ticket;

        public int PickUpTrimStart { get { return pickUpTrimStart; } }
        [SerializeField] int pickUpTrimStart;

        public int PickUpTrimEnd { get { return pickUpTrimEnd; } }
        [SerializeField] int pickUpTrimEnd;
    }

    [System.Serializable]
    public class BlockContent
    {
        public int ObstaclesRandom { get { return obstacles.Random; } }
        [SerializeField] private RandomCountParameters obstacles;

        public int DirectionChangeDeadly { get { return changeDeadly.Random; } }
        [SerializeField] private RandomCountParameters changeDeadly;

        public int DirectionChangeFlippable { get { return changeFlippable.Random; } }
        [SerializeField] private RandomCountParameters changeFlippable;

        public int RollerCoasterRandom { get { return rollerCoaster.Count; } }
        [SerializeField] private RandomPresenceParameter rollerCoaster;

        public int DeadEnd { get { return deadEnd.Random; } }
        [SerializeField] private RandomCountParameters deadEnd;

        public int SmallAvoidance { get { return smallAvoidance.Random; } }
        [SerializeField] private RandomCountParameters smallAvoidance;

        public MinMax SmallAvoidanceChunks { get { return smallAvoidanceChunks; } }
        [SerializeField] private MinMax smallAvoidanceChunks;

        public int Mail { get { return mail.Random; } }
        [SerializeField] private RandomCountParameters mail;
    }

    [System.Serializable]
    public class LevelContent
    {
        public RandomPresenceParameter PropStation { get { return propStation; } }
        [SerializeField] private RandomPresenceParameter propStation;

        public RandomPresenceParameter PlaneFork { get { return planeFork; } }
        [SerializeField] private RandomPresenceParameter planeFork;

        public RandomCountParameters CoinsForks { get { return coinsForks; } }
        [SerializeField] private RandomCountParameters coinsForks;

        public RandomCountParameters RepairForks { get { return repairForks; } }
        [SerializeField] private RandomCountParameters repairForks;

        public RandomPresenceParameter EmbankmentDeadEnd { get { return embankmentDeadEnd; } }
        [SerializeField] private RandomPresenceParameter embankmentDeadEnd;
    }


    [System.Serializable]
    public class RandomCountParameters : BaseParameters
    {
        public int Random
        {
            get
            {
                if (min > max)
                    return 0;

                return UnityEngine.Random.Range(min, (max + 1));
            }
        }

        public List<int> GenerateBlockIndexes(int blocksCount)
        {
            List<int> result = new List<int>();
            int count = Random;
            if (count == 0)
                return result;
            count = Mathf.Clamp(count, 0, blocksCount);

            while (result.Count < count)
            {
                int index = UnityEngine.Random.Range(0, blocksCount - count);
                if (!result.Contains(index))
                    result.Add(index);
            }
            return result;
        }
        

        public override void OnBeforeSerialize()
        {
            base.OnBeforeSerialize();
            if (min < 0)
                min = 0;

            if (max < min)
                max = min;
        }

        [SerializeField] protected int min;
        [SerializeField] protected int max;

        
    }

    [System.Serializable]
    public class RandomPresenceParameter : BaseParameters
    {
        public int Count { get { return Present ? 1 : 0; } }
        public bool Present { get { return weight >= UnityEngine.Random.Range(0f, 1f); } }

        [SerializeField][Range(0f, 1f)] protected float weight;

        public int BlockNumber(int blockCount)
        {
            if (!Present)
                return -1;
            else
                return Random.Range(0, blockCount);
        }
    }

    [System.Serializable]
    public class PresenceParameter : BaseParameters
    {
        public bool On { get { return on; } }
        [SerializeField] protected bool on = true;
    }

    [System.Serializable]
    public class AutoNameParameters : BaseParameters
    {
        [HideInInspector] [SerializeField] protected string name;

        public override void OnBeforeSerialize()
        {
            base.OnBeforeSerialize();
            this.name = SetName();

        }
        protected virtual string SetName()
        {
            return "AutoName";
        }
    }

    [System.Serializable]
    public class DirectionsCollection : BaseParameters
    {
        public DirectionParameters Flat { get { return flat; } }
        [SerializeField] private DirectionParameters flat;
        public Up5 Up5 { get { return up5; } }
        [SerializeField] private Up5 up5;

        public Up10 Up10 { get { return up10; } }
        [SerializeField] private Up10 up10;

        public Up15 Up15 { get { return up15; } }
        [SerializeField] private Up15 up15;

        public Up20 Up20 { get { return up20; } }
        [SerializeField] private Up20 up20;

        public Up25 Up25 { get { return up25; } }
        [SerializeField] private Up25 up25;

        public Up30 Up30 { get { return up30; } }
        [SerializeField] private Up30 up30;

        public Up35 Up35 { get { return up35; } }
        [SerializeField] private Up35 up35;

        public Up40 Up40 { get { return up40; } }
        [SerializeField] private Up40 up40;

        public Up45 Up45 { get { return up45; } }
        [SerializeField] private Up45 up45;

        public Up50 Up50 { get { return up50; } }
        [SerializeField] private Up50 up50;

        public Up55 Up55 { get { return up55; } }
        [SerializeField] private Up55 up55;

        public Up60 Up60 { get { return up60; } }
        [SerializeField] private Up60 up60;

        public Up65 Up65 { get { return up65; } }
        [SerializeField] private Up65 up65;

        public Up70 Up70 { get { return up70; } }
        [SerializeField] private Up70 up70;

        public Up75 Up75 { get { return up75; } }
        [SerializeField] private Up75 up75;

        public Up80 Up80 { get { return up80; } }
        [SerializeField] private Up80 up80;

        public Up85 Up85 { get { return up85; } }
        [SerializeField] private Up85 up85;

        public Up90 Up90 { get { return up90; } }
        [SerializeField] private Up90 up90;

        public Down5 Down5 { get { return down5; } }
        [SerializeField] private Down5 down5;

        public Down10 Down10 { get { return down10; } }
        [SerializeField] private Down10 down10;

        public Down15 Down15 { get { return down15; } }
        [SerializeField] private Down15 down15;

        public Down20 Down20 { get { return down20; } }
        [SerializeField] private Down20 down20;

        public Down25 Down25 { get { return down25; } }
        [SerializeField] private Down25 down25;

        public Down30 Down30 { get { return down30; } }
        [SerializeField] private Down30 down30;

        public Down35 Down35 { get { return down35; } }
        [SerializeField] private Down35 down35;

        public Down40 Down40 { get { return down40; } }
        [SerializeField] private Down40 down40;

        public Down45 Down45 { get { return down45; } }
        [SerializeField] private Down45 down45;

        public Down50 Down50 { get { return down50; } }
        [SerializeField] private Down50 down50;

        public Down55 Down55 { get { return down55; } }
        [SerializeField] private Down55 down55;

        public Down60 Down60 { get { return down60; } }
        [SerializeField] private Down60 down60;

        public Down65 Down65 { get { return down65; } }
        [SerializeField] private Down65 down65;

        public Down70 Down70 { get { return down70; } }
        [SerializeField] private Down70 down70;

        public Down75 Down75 { get { return down75; } }
        [SerializeField] private Down75 down75;

        public Down80 Down80 { get { return down80; } }
        [SerializeField] private Down80 down80;

        public Down85 Down85 { get { return down85; } }
        [SerializeField] private Down85 down85;

        public Down90 Down90 { get { return down90; } }
        [SerializeField] private Down90 down90;
    }




    [System.Serializable]
    public class DirectionParameters : BaseParameters
    {
        public float Angle { get { return GetAngle(); } }

        public bool Active 
        { 
            get 
            { 
                return active && weight > 0;
            }
        }
        public float Weight { get { return weight; } }

        [SerializeField] protected bool active = true;
        [SerializeField] protected float weight = 10;

        protected virtual float GetAngle()
        {
            return 0f;
        }
    }

    [System.Serializable] public class Up5 : DirectionParameters { protected override float GetAngle() { return -5; } }
    [System.Serializable] public class Up10 : DirectionParameters { protected override float GetAngle() { return -10; } }
    [System.Serializable] public class Up15 : DirectionParameters { protected override float GetAngle() { return -15; } }
    [System.Serializable] public class Up20 : DirectionParameters { protected override float GetAngle() { return -20; } }
    [System.Serializable] public class Up25 : DirectionParameters { protected override float GetAngle() { return -25; } }
    [System.Serializable] public class Up30 : DirectionParameters { protected override float GetAngle() { return -30; } }
    [System.Serializable] public class Up35 : DirectionParameters { protected override float GetAngle() { return -35; } }
    [System.Serializable] public class Up40 : DirectionParameters { protected override float GetAngle() { return -40; } }
    [System.Serializable] public class Up45 : DirectionParameters { protected override float GetAngle() { return -45; } }
    [System.Serializable] public class Up50 : DirectionParameters { protected override float GetAngle() { return -50; } }
    [System.Serializable] public class Up55 : DirectionParameters { protected override float GetAngle() { return -55; } }
    [System.Serializable] public class Up60 : DirectionParameters { protected override float GetAngle() { return -60; } }
    [System.Serializable] public class Up65 : DirectionParameters { protected override float GetAngle() { return -65; } }
    [System.Serializable] public class Up70 : DirectionParameters { protected override float GetAngle() { return -70; } }
    [System.Serializable] public class Up75 : DirectionParameters { protected override float GetAngle() { return -75; } }
    [System.Serializable] public class Up80 : DirectionParameters { protected override float GetAngle() { return -80; } }
    [System.Serializable] public class Up85 : DirectionParameters { protected override float GetAngle() { return -85; } }
    [System.Serializable] public class Up90 : DirectionParameters { protected override float GetAngle() { return -90; } }
    [System.Serializable] public class Down5 : DirectionParameters { protected override float GetAngle() { return 5; } }
    [System.Serializable] public class Down10 : DirectionParameters { protected override float GetAngle() { return 10; } }
    [System.Serializable] public class Down15 : DirectionParameters { protected override float GetAngle() { return 15; } }
    [System.Serializable] public class Down20 : DirectionParameters { protected override float GetAngle() { return 20; } }
    [System.Serializable] public class Down25 : DirectionParameters { protected override float GetAngle() { return 25; } }
    [System.Serializable] public class Down30 : DirectionParameters { protected override float GetAngle() { return 30; } }
    [System.Serializable] public class Down35 : DirectionParameters { protected override float GetAngle() { return 35; } }
    [System.Serializable] public class Down40 : DirectionParameters { protected override float GetAngle() { return 40; } }
    [System.Serializable] public class Down45 : DirectionParameters { protected override float GetAngle() { return 45; } }
    [System.Serializable] public class Down50 : DirectionParameters { protected override float GetAngle() { return 50; } }
    [System.Serializable] public class Down55 : DirectionParameters { protected override float GetAngle() { return 55; } }
    [System.Serializable] public class Down60 : DirectionParameters { protected override float GetAngle() { return 60; } }
    [System.Serializable] public class Down65 : DirectionParameters { protected override float GetAngle() { return 65; } }
    [System.Serializable] public class Down70 : DirectionParameters { protected override float GetAngle() { return 70; } }
    [System.Serializable] public class Down75 : DirectionParameters { protected override float GetAngle() { return 75; } }
    [System.Serializable] public class Down80 : DirectionParameters { protected override float GetAngle() { return 80; } }
    [System.Serializable] public class Down85 : DirectionParameters { protected override float GetAngle() { return 85; } }
    [System.Serializable] public class Down90 : DirectionParameters { protected override float GetAngle() { return 90; } }
}