using UnityEngine;
using UnityEditor;


[CustomPropertyDrawer(typeof(MTParameters.SFD.CellCoordinates))]
public class CellCoordinatesDrawer : UberPropertyDrawer
{
    /*
    protected virtual string Name()
    {
        return "Flat 0";
    }
    */

    protected override void Draw(Rect position, SerializedProperty property, GUIContent label, Rect main)
    {
        base.Draw(position, property, label, main);

        //
        int fieldWidth = 65;
        int nameWidth = (int)(main.width - 20 * 2 - fieldWidth * 2);
        Rect r1 = new Rect(main.x, main.y, nameWidth, main.height);   //label
        Rect r2 = new Rect(r1.max.x, main.y, 20, main.height); //label X
        Rect r3 = new Rect(r2.max.x, main.y, fieldWidth, main.height); //X
        Rect r4 = new Rect(r3.max.x, main.y, 20, main.height); //label Y
        Rect r5 = new Rect(r4.max.x, main.y, fieldWidth, main.height); //Y
        
        //Rect r5 = new Rect(r4.max.x, main.y, left, main.height); //prop weight

        SerializedProperty x = property.FindPropertyRelative("x");
        SerializedProperty y = property.FindPropertyRelative("y");

        GUI.Label(r1, property.displayName);
        GUI.Label(r2, "X: ");
        EditorGUI.PropertyField(r3, x, GUIContent.none, false);
        GUI.Label(r4, "Y: ");
        EditorGUI.PropertyField(r5, y, GUIContent.none, false);
       
    }
}
