using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class KreatorSettingsPropertyDrawer : UberPropertyDrawer
{
    public static readonly Color ErrorColor = new Color(1f, 0.2f, 0f, 0.1f);
    public static readonly Color OkColor = new Color(0.2f, 1f, 0f, 0.01f);
}

[CustomPropertyDrawer(typeof(MTParameters.Kreator.DirectionParameters))]
public class DirectionParametersDrawer : KreatorSettingsPropertyDrawer
{
    protected virtual string Name()
    {
        return "Flat 0";
    }

    protected override void Draw(Rect position, SerializedProperty property, GUIContent label, Rect main)
    {
        base.Draw(position, property, label, main);

        //
        Rect r1 = new Rect(main.x, main.y, 60, main.height);   //label
        Rect r2 = new Rect(r1.max.x, main.y, 30, main.height); //label on
        Rect r3 = new Rect(r2.max.x, main.y, 30, main.height); //prop checkBox
        Rect r4 = new Rect(r3.max.x, main.y, 50, main.height); //label weight
        int left = (int)((main.width - r1.width - r2.width - r3.width - r4.width));
        Rect r5 = new Rect(r4.max.x, main.y, left, main.height); //prop weight

        SerializedProperty active = property.FindPropertyRelative("active");
        SerializedProperty weight = property.FindPropertyRelative("weight");

        GUI.Label(r1, Name());
        GUI.Label(r2, "ON: ");
        EditorGUI.PropertyField(r3, active, GUIContent.none, false);
        GUI.Label(r4, "Weight: ");
        EditorGUI.PropertyField(r5, weight, GUIContent.none, false);
        
        if (active.boolValue == false || weight.floatValue == 0f)
            EditorGUI.DrawRect(main, ErrorColor);
        else
            EditorGUI.DrawRect(main, OkColor);
    }
}

[CustomPropertyDrawer(typeof(MTParameters.Kreator.RandomCountParameters))]
public class RangeParametersDrawer : KreatorSettingsPropertyDrawer
{
    protected override void Draw(Rect position, SerializedProperty property, GUIContent label, Rect main)
    {
        base.Draw(position, property, label, main);
        
        //main label
        Rect r1 = new Rect(main.x, main.y, 120, main.height);  
        GUI.Label(r1, property.displayName);

        int rubber = (int)((main.width - r1.width) / 2f);
        //label min
        Rect r2 = new Rect(r1.max.x, main.y, 35, main.height);   //label min
        GUI.Label(r2, "Min: ");

        //field min
        Rect r3 = new Rect(r2.max.x, main.y, rubber - r2.width, main.height);   //prop Min
        SerializedProperty min = property.FindPropertyRelative("min");
        EditorGUI.PropertyField(r3, min, GUIContent.none, false);

        //label max
        Rect r4 = new Rect(r3.max.x, main.y, 35, main.height);   //label max
        GUI.Label(r4, " Max: ");

        Rect r5 = new Rect(r4.max.x, main.y, rubber - r4.width, main.height); //prop Max
        SerializedProperty max = property.FindPropertyRelative("max");
        EditorGUI.PropertyField(r5, max, GUIContent.none, false);

        if (min.intValue > max.intValue || (min.intValue == 0f && max.intValue == 0f)) 
        {
            EditorGUI.DrawRect(main, ErrorColor);
        }
        else
        {
            EditorGUI.DrawRect(main, OkColor);
        }
    }
}

[CustomPropertyDrawer(typeof(MTParameters.Kreator.Up5))]
public class Up5ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Up 5";
    }
}

[CustomPropertyDrawer(typeof(MTParameters.Kreator.Up10))]
public class Up10ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Up 10";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Up15))]
public class Up15ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Up 15";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Up20))]
public class Up20ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Up 20";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Up25))]
public class Up25ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Up 25";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Up30))]
public class Up30ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Up 30";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Up35))]
public class Up35ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Up 35";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Up40))]
public class Up40ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Up 40";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Up45))]
public class Up45ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Up 45";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Up50))]
public class Up50ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Up 50";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Up55))]
public class Up55ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Up 55";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Up60))]
public class Up60ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Up 60";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Up65))]
public class Up65ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Up 65";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Up70))]
public class Up70ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Up 70";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Up75))]
public class Up75ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Up 75";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Up80))]
public class Up80ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Up 80";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Up85))]
public class Up85ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Up 85";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Up90))]
public class Up90ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Up 90";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Down5))]
public class Down5ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Down 5";
    }
}

[CustomPropertyDrawer(typeof(MTParameters.Kreator.Down10))]
public class Down10ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Down 10";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Down15))]
public class Down15ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Down 15";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Down20))]
public class Down20ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Down 20";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Down25))]
public class Down25ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Down 25";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Down30))]
public class Down30ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Down 30";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Down35))]
public class Down35ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Down 35";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Down40))]
public class Down40ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Down 40";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Down45))]
public class Down45ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Down 45";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Down50))]
public class Down50ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Down 50";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Down55))]
public class Down55ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Down 55";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Down60))]
public class Down60ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Down 60";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Down65))]
public class Down65ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Down 65";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Down70))]
public class Down70ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Down 70";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Down75))]
public class Down75ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Down 75";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Down80))]
public class Down80ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Down 80";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Down85))]
public class Down85ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Down 85";
    }
}
[CustomPropertyDrawer(typeof(MTParameters.Kreator.Down90))]
public class Down90ParametersDrawer : DirectionParametersDrawer
{
    protected override string Name()
    {
        return "Down 90";
    }
}
