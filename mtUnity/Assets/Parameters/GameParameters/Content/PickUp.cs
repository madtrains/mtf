﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTParameters
{
    [System.Serializable]
    public class PickUp : IDParameters
	{
        public PickUp(int id, string translationKey) : base (id)
        {
            this.translationKey = translationKey;
        }


        public string TranslationKey { get { return translationKey; } }
        [SerializeField] private string translationKey;
    }
}