﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTParameters
{
    [System.Serializable]
    public class Resource : NamedIDParameters
	{
        public Resource(int id, string name, string translationKey, 
            bool isCargo, bool showInUICargoCounters,
            int resultsOrder) : base(id, name)
        {
            this.translationKey = translationKey;
            this.isCargo = isCargo;
            this.showInUICargoCounters = showInUICargoCounters;
            this.resultsOrder = resultsOrder;
        }

        public string TranslationKey { get { return translationKey; } }
        [SerializeField] private string translationKey;
        public bool IsCargo { get { return isCargo; } }
        [SerializeField] private bool isCargo;
        public bool ShowInUICargoCounters { get { return showInUICargoCounters; } }
        [SerializeField] private bool showInUICargoCounters;
        public int ResultsOrder { get { return resultsOrder; } }
        [SerializeField] private int resultsOrder;
    }
}