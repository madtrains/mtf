﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTParameters
{
	[System.Serializable]
	public class IDParameters : BaseParameters
	{
		public int ID { get { return id; } }

		[SerializeField] protected int id;

		public IDParameters(int id)
        {
			this.id = id;
        }
	}
}