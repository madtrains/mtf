﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTParameters
{
    [System.Serializable]
    public class PhysForces
    {
        public float Multiplier { get { return multiplier; } }
        public float Mix { get { return mix; } }

        [SerializeField] private float multiplier;
        [SerializeField] [Range(0f,1f)] private float mix;

        //[SerializeField] private PhysForce locomotive;
    }


    [System.Serializable]
    public class PhysForce
    {
        
    }
}