﻿using UnityEngine;

namespace MTParameters
{
    [System.Serializable]
    public class Train : BundledIDParameters
	{
        public bool HasIndispensableCoach { get { return !string.IsNullOrEmpty(indispensableCoach); } }

        public float SpeedFactor { get { return speedFactor; } }
        public float Speed { get { return speed; } }
        public float SpeedIncrease { get { return speedIncrease; } }
        public float OverSpeedFactor { get { return overSpeedFactor; } }
        public float FlipTime { get { return flipTime; } }
        public string IndispensableCoach { get { return indispensableCoach; } }
        public float TractionPower { get { return tractionPower; } }

        public int HitPoints { get { return hitPoints; } }
        public int Price { get { return price; } }
        public int CogCoinsPrice { get { return cogCoinsPrice; } }
        public bool IsExpress { get { return isExpress; } }
        public string Type { get { return type; } }
        public string Model { get { return model; } }

        [SerializeField] private float speedFactor;
        [SerializeField] private float speed;
        [SerializeField] private float speedIncrease;
        [SerializeField] private float overSpeedFactor;
        [SerializeField] private float flipTime;
        [SerializeField] private string indispensableCoach;
        [SerializeField] private float tractionPower;
        [SerializeField] private int hitPoints;
        [SerializeField] private int price;
        [SerializeField] private int cogCoinsPrice;
        [SerializeField] private bool isExpress;
        [SerializeField] private string type;
        [SerializeField] private string model;


        public Train(int id, string name, string bundleName, float speedFactor, float speed, float speedIncrease, float overSpeedFactor,
            float flipTime, string indispensableCoach, float tractionPower, int hitPoints,
            int price, int cogCoinsPrice, bool isExpress, string type, string model)
                : base(id, name, bundleName)
        {
            this.speedFactor = speedFactor;
            this.speed = speed;
            this.speedIncrease = speedIncrease;
            this.overSpeedFactor = overSpeedFactor;
            this.flipTime = flipTime;
            this.indispensableCoach = indispensableCoach;
            this.tractionPower = tractionPower;
            this.hitPoints = hitPoints;
            this.price = price;
            this.cogCoinsPrice = cogCoinsPrice;
            this.isExpress = isExpress;
            this.type = type;
            this.model = model;
        }

        public void ChangeSpeeds(float speed, float flipTime)
        {
            this.speed = speed;
            this.flipTime = flipTime;
        }

        public MinMax SpeedAbsolute(TrainShared shared)
        {
            float max = shared.SpeedAngleDownModifier.Modifier * this.speed;
            return new MinMax(shared.StartSpeed, max);
        }
    }
}