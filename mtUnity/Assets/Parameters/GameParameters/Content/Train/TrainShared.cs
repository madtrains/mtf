﻿using System;
using UnityEngine;

namespace MTParameters
{
    [System.Serializable]
    public class TrainShared : BaseParameters
	{
        public float StartSpeed { get { return startSpeed; } }
        public AnimationCurve FlipCurve { get { return flipCurve; } }
        public int RailsToFlipCount { get { return railsToFlipCount; } }
        public float DoubleFlipMultiplier { get { return doubleFlipMultiplier; } }
        public PhysForces TrainPhysForce { get { return trainPhysForce; } }

        public float CameraLowSpeedOffset { get { return cameraLowSpeedOffset; } }
        public float CameraHighSpeedOffset { get { return cameraHighSpeedOffset; } }
        public ValueModifier CameraAngleOffset { get { return cameraAngleOffset; } }
        public ValueModifier SpeedAngleDownModifier { get { return speedAngleDownModifier; } }
        public ValueModifier SpeedAngleUpModifier { get { return speedAngleUpModifier; } }
        public OverWeightPenalty OverweightPenalty { get { return overweightPenalty; } }

        public ValueModifier AngleDownDollyModifier { get { return angleDownDollyModifier; } }

        public float OverSpeedFactor { get { return overSpeedFactor; } }
        public float DecelerationSpeed { get { return  decelerationSpeed; } }
        public float CameraMoveSpeed { get { return cameraMoveSpeed; } }
        public float CameraReturnSpeed { get { return сameraReturnSpeed; } }

        public float TukPauseShort { get { return tukPauseShort; } }
        public float TukPauseLong { get { return tukPauseLong; } }

        public float SpeedDown { get { return speedDown; } }
        public float SpeedRefill { get { return speedRefill; } }
        public float FlipRefillValue { get { return flipRefillValue; } }
        public float OneShotValue { get { return oneShotValue; } }
        public float OneShotTime { get { return oneShotValue / speedDown; } }

        
        //public float OnStationSfdSpeed { get { return onStationSfdSpeed; } }
        public float ExiSfdSpeed { get { return exiSfdSpeed; } }
        
        public AnimationCurve SpeedIncrease { get { return speedIncrease; } }

        public float MaxSpeed { get { return maxSpeed; } }

        public float TownLowSpeed { get { return townLowSpeed; } }
        public float TownFullSpeed { get { return townFullSpeed; } }
        public AnimationCurve ArrivePlatformCurve { get { return arrivePlatform; } }


        [Tooltip("Start speed in flipper")]
        [SerializeField] private float startSpeed;
        [SerializeField] private AnimationCurve flipCurve;
        [SerializeField] private int railsToFlipCount = 1;
        [SerializeField] private float doubleFlipMultiplier = 1f;
        [SerializeField] private PhysForces trainPhysForce;

        [Tooltip("Camera offset depending on maximal speed")]
        [SerializeField] private float cameraLowSpeedOffset;
        [Tooltip("Camera offset depending on maximal speed")]
        [SerializeField] private float cameraHighSpeedOffset;
        [Tooltip("Camera offset depending on trains angle")]
        [SerializeField] private ValueModifier cameraAngleOffset;
        [Tooltip("Множитель зависящий от угла, когда поезд едет вниз")]
        [SerializeField] private ValueModifier speedAngleDownModifier;
        [Tooltip("Множитель зависящий от угла, когда поезд едет вверх")]
        [SerializeField] private ValueModifier speedAngleUpModifier;

        [Tooltip("Множители влияющие на скорости при перегрузе")]
        [SerializeField] private OverWeightPenalty overweightPenalty;


        [Tooltip("Dolly depending on angle, when going down")]
        [SerializeField] private ValueModifier angleDownDollyModifier;

        [Tooltip("Множитель при переходе от своей основной скорости к набору максимальной")]
        [SerializeField] private float overSpeedFactor;
        [Tooltip("Deceleration Speed for all Locomotives")]
        [SerializeField] private float decelerationSpeed;
        [Tooltip("Camera move speed")]
        [SerializeField] private float cameraMoveSpeed;
        [Tooltip("Camera parameters interpolation speed")]
        [SerializeField] private float сameraReturnSpeed;

        [Tooltip("0--0-----0--0 Sound short interval")]
        [SerializeField] private float tukPauseShort;

        [Tooltip("0--0-----0--0 Sound Long interval")]
        [SerializeField] private float tukPauseLong;

        [Tooltip("Air")]
        [SerializeField] private float speedDown = 0.6f;
        [SerializeField] private float speedRefill = 0.05f;
        [SerializeField] private float flipRefillValue = 0.05f;
        [SerializeField][Range(0.1f, 1f)] private float oneShotValue = 0.45f;

        [Tooltip("Speed")]
        //[SerializeField] private float onStationSfdSpeed;
        [SerializeField] private float exiSfdSpeed;
        [SerializeField] private AnimationCurve speedIncrease;
        [SerializeField] private float maxSpeed;
        [SerializeField] private float townLowSpeed;
        [SerializeField] private float townFullSpeed;
        [SerializeField] private AnimationCurve arrivePlatform;
    }
    /*
    [System.Serializable]
    public class OverWeightPenalty : FromToModifier
    {
        public float Get(float overweight)
        {
            return 1f - (1f - this.modifier) * this.InverseLerp(overweight);
        }
    }
    */

    [System.Serializable]
    public class OverWeightPenalty
    {
        [Tooltip("Угол при котором влияние углового коэффициента будет максимальным")]
        [SerializeField] private float angle;

        public float MaxOverweight { get { return maxOverweight; } }
        [Tooltip("Максимальный относительный перевес при котором езда невозможна")]
        [SerializeField] private float maxOverweight;

        [Tooltip("Множитель влияющий на максимальную скорость")][Range(0.01f, 1f)]
        [SerializeField] private float absMultiplier;
        [Tooltip("Множитель влияющий на разгон")][Range(0.01f, 1f)]
        [SerializeField] private float speedUpMultiplier;
        [Tooltip("Множитель влияющий на время флипа")] [Range(1f, 3f)]
        [SerializeField] private float flipMultiplier;
        [Tooltip("Множитель влияющий на скорость при подъёме")][Range(0.01f, 1f)]
        [SerializeField] private float angleMultiplier;

        public float T { get { return overweightT; } }
        private float overweightT;

        public void SetRatio(float ratio)
        {
            overweightT = Mathf.InverseLerp(1f, this.maxOverweight, ratio);
        }

        public float GetAngleMultiplier(float angle)
        {
            float angleT = Mathf.InverseLerp(1f, this.angle, Mathf.Abs(angle));
            return GetPenaltyMultiplier(this.angleMultiplier, angleT * overweightT);
        }

        public float AbsMultiplier { get { return GetPenaltyMultiplier(absMultiplier, overweightT); } }
        public float SpeedUpMultiplier { get { return GetPenaltyMultiplier(speedUpMultiplier, overweightT); } }

        public float FlipMultiplier { get { return Mathf.Lerp(1f, flipMultiplier, overweightT); } }

        private float GetPenaltyMultiplier(float multiplier, float t)
        {
            return Mathf.Lerp(multiplier, 1f, 1-t);
        }
    }

    [System.Serializable]
    public class CapValues
    {
        public float MaxSpeed { get { return maxSpeed; } }
        [SerializeField] private float maxSpeed;

        public float FlipTime { get { return flipTime; } }
        [SerializeField] private float flipTime;

        public float Lives { get { return lives; } }
        [SerializeField] private float lives;

        public float TractionPower { get { return tractionPower; } }
        [SerializeField] private float tractionPower;
    }
}
