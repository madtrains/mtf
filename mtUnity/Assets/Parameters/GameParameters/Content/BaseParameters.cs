﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTParameters
{
	[System.Serializable]
	public class BaseParameters : ISerializationCallbackReceiver
	{
		public virtual void OnAfterDeserialize()
		{
			//Debug.Log("Value changed");
		}


		public virtual void OnBeforeSerialize()
		{
		}
	}
}