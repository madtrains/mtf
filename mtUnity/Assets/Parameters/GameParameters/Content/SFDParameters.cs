﻿using UnityEngine;

namespace MTParameters.SFD
{
    [System.Serializable]
    public class SFDParameters : BaseParameters
	{
        public CellCoordinates ReloadButton { get { return reloadButton; } }
        [Tooltip("Координаты кнопки перегрузки")]
        [SerializeField] private CellCoordinates reloadButton;

        public int TownTripLines { get { return townTripLines; } }
        [Tooltip("Количество строк путей в городе")]
        [SerializeField] private int townTripLines;

        public CameraParameters TownSFDCamera { get { return townSFDCamera; } }
        [Tooltip("Настройки камеры в городе")]
        [SerializeField] private MTParameters.CameraParameters townSFDCamera;

        public Assemble TownAssemble { get { return townAssemble; } }
        [Tooltip("Размер табло в городе")]
        [SerializeField] private Assemble townAssemble;


        public CellCoordinates TripsCorner { get { return tripsCorner; } }
        [Tooltip("Угловая клетка путей в городе")]
        [SerializeField] private CellCoordinates tripsCorner;

        public CellCoordinates TownResultsCorner { get { return townResultsCorner; } }
        [Tooltip("Угловая клетка верхней строки результатов в городе")]
        [SerializeField] private CellCoordinates townResultsCorner;

        public float TownResultsPause { get { return townResultsPause; } }
        [Tooltip("Задержка между ресурсами в результатах")]
        [SerializeField] private float townResultsPause;

        public Assemble ExitFlipper { get { return exitFlipper; } }
        [Tooltip("Размер табло на выезде из флиппера")]
        [SerializeField] private Assemble exitFlipper;
    }

    [System.Serializable]
    public class CellCoordinates
    {
        public CellCoordinates (int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int X { get { return x; } }
        [SerializeField] private int x;

        public int Y { get { return y; } }
        [SerializeField] private int y;
    }

    [System.Serializable]
    public class Assemble
    {
        public Assemble(float height, CellCoordinates size, MinMax switchCount, MinMax cellSpeeds)
        {
            this.height = height;
            this.size = size;
            this.switchCount = switchCount;
            this.cellSpeeds = cellSpeeds;
        }

        public float Height { get { return height; } }
        [SerializeField] private float height;

        public CellCoordinates Size { get { return size; } }
        [SerializeField] private CellCoordinates size;

        public MinMax SwitchCount { get { return switchCount; } }
        [SerializeField] private MinMax switchCount;

        public MinMax CellSpeeds { get { return cellSpeeds; } }
        [SerializeField] private MinMax cellSpeeds;
    }
}
