﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTParameters
{
    [System.Serializable]
    public class Cargos : BaseParameters
	{

        public MinMax WaitTime { get { return waitTime; } }
        public MinMax FlyTime { get { return flyTime; } }
        public float ArcHeight { get { return arcHeight; } }
        public float FlipperPassengerScale { get { return flipperPassengerScale; } }
        public float FlipperPassengerMoveSpeed { get { return flipperPassengerMoveSpeed.Random; } }
        


        [SerializeField] private MinMax waitTime;
        [SerializeField] private MinMax flyTime;
        [SerializeField] private float arcHeight;
        [SerializeField] private float flipperPassengerScale;
        [SerializeField] private MinMax flipperPassengerMoveSpeed;
        
    }
}