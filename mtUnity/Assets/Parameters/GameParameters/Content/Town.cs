﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTParameters
{
    [System.Serializable]
    public class Town : BundledIDParameters
	{
        public Town(int id, string name, string translationKey, string prefabBundleName, 
            int[] resources, int[] neighbours, int[] buildings, int[] depotIDs)
            : base(id, name, prefabBundleName)
        {
            this.translationKey = translationKey;
            this.resources = resources;
            this.neighbours = neighbours;
            this.buildings = buildings;
            this.depotIDs = depotIDs;
        }

        public string TranslationKey { get { return translationKey; } }
        public int[] Resources { get { return resources; } }
        public int[] Neighbours { get { return neighbours; } }
        public int[] Buildings { get { return buildings; } }
        public int[] DepotIDs { get { return depotIDs; } }

        [SerializeField] private string translationKey;
        [SerializeField] private int[] resources;
        [SerializeField] private int[] neighbours;
        [SerializeField] private int[] buildings;
        [SerializeField] private int[] depotIDs;
    }
}