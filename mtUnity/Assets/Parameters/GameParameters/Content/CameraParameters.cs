﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTParameters
{
	[System.Serializable]
	public class CameraParameters : NamedParameters
	{
        public float Angle { get { return angle; } }
        public float AngleUp { get { return angleUp; } }
        public float Dolly { get { return dolly; } }
        public float Fov { get { return fov; } }

        [SerializeField] private float angle;
        [SerializeField] private float angleUp;
        [SerializeField] private float dolly;
        [SerializeField] private float fov;


        public static CameraParameters Copy(CameraParameters target)
        {
            CameraParameters newparams = new CameraParameters(target.Name,
                target.Angle, target.AngleUp, target.Dolly, target.Fov);
            return newparams;
        }


        public CameraParameters()
        {
            this.name = "";
            this.angle = 0f;
            this.angleUp = 0f;
            this.dolly = 0f;
            this.fov = 0f;
        }


        public CameraParameters(string name, float angle, float angleUp, float dolly, float fov)
        {
            this.name = name;
            this.angle = angle;
            this.angleUp = angleUp;
            this.dolly = dolly;
            this.fov = fov;
        }


        public void Lerp(CameraParameters a, CameraParameters b, float t, ref CameraParameters result)
        {
            result.angle = Mathf.Lerp(a.Angle, b.Angle, t);
            result.angleUp = Mathf.Lerp(a.AngleUp, b.AngleUp, t);
            result.dolly = Mathf.Lerp(a.Dolly, b.Dolly, t);
            result.fov = Mathf.Lerp(a.Fov, b.Fov, t);
        }
    }
}