﻿using System;
using UnityEngine;

namespace MTParameters.Flipper
{
    [System.Serializable]
    public class FlipperParameters : BaseParameters
    {
        public float CollisionImpulseMutiplier { get { return collisionImpulseMutiplier; } }
        [Tooltip("Множитель импульса при столкновении поезда с препятствием")]
        [SerializeField] private float collisionImpulseMutiplier;

        public float HornThreshold { get { return hornThreshold; } }
        [SerializeField] private float hornThreshold;

        public float DoubleTapThreshold { get { return doubleTapThreshold; } }
        [SerializeField] private float doubleTapThreshold;
        public float PassengerMinimalExitPercentage { get { return passengerMinimalExitPercentage; } }
        [Tooltip("На первой станции выйдет X*100% пассажиров")]
        [SerializeField][Range(0f, 1f)] private float passengerMinimalExitPercentage;

        public float PassengersStep { get { return passengersStep; } }
        [Tooltip("Шаг пассажиров")]
        [SerializeField] private float passengersStep = 1.5f;

        public float PassengerRotateSpeed { get { return passengerRotateSpeed; } }
        [Tooltip("Скорость поворота пассажиров при ходьбе")]
        [SerializeField] private float passengerRotateSpeed = 12f;

        public float HornCargoLoadDelay { get { return hornCargoLoadDelay; } }
        [Tooltip("Пауза между погрузкой груза при сборе гудком")]
        [SerializeField] private float hornCargoLoadDelay = 0.05f;

        public float HornCargoLoadStep { get { return hornCargoLoadStep; } }
        [Tooltip("Шаг покадрового счётчика ожидания при погрузке гудком")]
        [SerializeField] private float hornCargoLoadStep = 0.01f;


        public MinMax UnboardPause { get => unboardPause; }
        [Tooltip("паузы при высадке пассажиров")]
        [SerializeField] private MinMax unboardPause;

        public MinMax UnloadPause { get => unloadPause; }
        [Tooltip("паузы при высадке пассажиров")]
        [SerializeField] private MinMax unloadPause;

        public ExitConstructor ExitConstructor { get { return exitConstructor; } }
        [SerializeField] private ExitConstructor exitConstructor;

        public TripContent[] Trips { get { return trips; } }
        [SerializeField] private TripContent[] trips;

        public Station[] Stations { get { return stations; } }
        [Tooltip("настройки создания станции для каждого размера")]
        [SerializeField] private Station[] stations;
    }

    [System.Serializable]
    public class Station : NamedParameters
    {
        public ControlSpeedLine BreakingLine { get { return breakingLine; } }
        [Tooltip("Дистанция торможения и скорость перед табло. При проезде этого отрезка камера повернётся на табло")]
        [SerializeField] private ControlSpeedLine breakingLine;

        public float PlatformSpeed { get { return platformSpeed; } }
        [Tooltip("Скорость прохождения платформы")]
        [SerializeField] private float platformSpeed;

        public int DisplayLineIdleChunks { get { return Mathf.Max(displayLineChunks, 1); } }
        [Tooltip("Линия проезда вдоль табло, где скорость остаётся без изменений")]
        [SerializeField] private int displayLineChunks;

        public int SwitchersLine { get { return switchersLine; } }
        [Tooltip("Длинна линии для переключателей")]
        [SerializeField] private int switchersLine;

        public float SwitcherOffset { get { return switcherOffset; } }
        [Tooltip("Шаг отступа при установке переключателей")]
        [SerializeField] private float switcherOffset;

        public int PlatformLineChunksCount { get { return platformLineChunksCount; } }
        [Tooltip("Длинна линии под станцию, подбирается под длинну платформ")]
        [SerializeField] private int platformLineChunksCount;

        public int PlatformDepth { get { return platformDepth; } }
        [Tooltip("Глубина платформы, подбирается под ширину платформ")]
        [SerializeField] private int platformDepth;

        
        public int GreenLightOffset { get { return greenLightOffset; } }
        [Tooltip("Отступ для подлёта камеры и включения зелёного света")]
        [SerializeField] [Range(0, 18)] private int greenLightOffset;

        public int RedLightOffset { get { return redLightOffset; } }
        [Tooltip("Расстояние прибавляемое к длинне платформы для активной погрузки")]
        [SerializeField] private int redLightOffset;


        public int LeaveLength { get { return leaveLength; } }
        [Tooltip("Дистанция восстановления камеры")]
        [SerializeField] private int leaveLength;

        public SFD.Assemble SfdAssemble { get { return sfdAssemble; } }
        [Tooltip("Настройки табло")]
        [SerializeField] private SFD.Assemble sfdAssemble;
    }

    [System.Serializable]
    public class ExitConstructor : BaseParameters
    {
        public int FreewayCount { get { return freewayCount; } }
        [Tooltip("Пустая линия обычных чанков перед финальной станцией")]
        [SerializeField] int freewayCount;

        public int PlatformWholeLineCount { get { return platformWholeLineCount; } }
        [Tooltip("Общая длинна всей линии")]
        [SerializeField] int platformWholeLineCount;


        public int PlatformStopPartCount { get { return platformStopPartCount; } }
        [Tooltip("Часть общей линии под торможение")]
        [SerializeField] int platformStopPartCount;


        public float LeaveCamFlySpeed { get { return leaveCamFlySpeed; } }
        [Tooltip("Скорость отлёта финальной камеры")]
        [SerializeField] float leaveCamFlySpeed;

        public float LeaveSpeed { get { return leaveSpeed; } }
        [Tooltip("Скорость убытия поезда")]
        [SerializeField] float leaveSpeed;
    }

    [System.Serializable]
    public class TripContent : BaseParameters
    {
        public MTCore.TripSize TripSize { get { return tripSize; } }
        [SerializeField] MTCore.TripSize tripSize;

        public MTCore.TripSize[] Stations { get { return stations; } }
        [SerializeField] MTCore.TripSize[] stations;
    }

    [System.Serializable]
    public class ControlSpeedLine : BaseParameters
    {
        public int ChunksCount { get { return chunksCount; } }
        [SerializeField] private int chunksCount;


        public float Speed { get { return speed; } }
        [SerializeField] private float speed;
    }
}