﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTParameters
{
	[System.Serializable]
	public class NamedParameters : BaseParameters
	{
        public string Name { get { return name; } }

        [SerializeField] protected string name;
	}
}