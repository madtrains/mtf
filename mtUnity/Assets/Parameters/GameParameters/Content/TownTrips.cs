﻿using UnityEngine;

namespace MTParameters
{
    [System.Serializable]
    public class TownTrips : BaseParameters
    {
        public int RandomResource { get { return tripsResources[UnityEngine.Random.Range(0, tripsResources.Length)]; } }

        public bool SecondResource
        {
            get
            {
                return UnityEngine.Random.Range(0.0f, 0.99f) < secondResourceProbability;
            }
        }

        public bool ThirdResource
        {
            get
            {
                return SecondResource && UnityEngine.Random.Range(0.0f, 0.99f) < thirdResourceProbability;
            }
        }

        [SerializeField] [Range(0f, 1f)] private float secondResourceProbability;
        [SerializeField] [Range(0f, 1f)] private float thirdResourceProbability;
        [SerializeField] private int[] tripsResources;
    }
}