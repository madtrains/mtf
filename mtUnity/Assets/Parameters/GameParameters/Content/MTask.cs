﻿using UnityEngine;

namespace MTParameters
{
    [System.Serializable]
    public class MTask : IDParameters
    {
        public MTask(int id, string mainTextKey, int conditionCurrency, int conditionValue, int rewardCurrency, int rewardValue)
            : base(id)
        {
            this.mainTextKey = mainTextKey;
            this.conditionCurrency = conditionCurrency;
            this.conditionValue = conditionValue;
            this.rewardCurrency = rewardCurrency;
            this.rewardValue = rewardValue;
        }

        public string MainTextKey { get { return mainTextKey; } }
        [SerializeField] private string mainTextKey;

        public string AuxTextKey { get { return auxTextKey; } }
        [SerializeField] private string auxTextKey;

        public int ConditionCurrency { get { return conditionCurrency; } }
        [SerializeField] private int conditionCurrency;

        public int ConditionValue { get { return conditionValue; } }
        [SerializeField] private int conditionValue;

        public int RewardCurrency { get { return rewardCurrency; } }
        [SerializeField] private int rewardCurrency;

        public int RewardValue { get { return rewardValue; } }
        [SerializeField] private int rewardValue;
    }
}