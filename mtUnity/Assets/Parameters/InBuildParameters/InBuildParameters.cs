﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace MTParameters
{
	public enum BuildType { Stage, Production }

	[CreateAssetMenu(menuName = "InBuild Parameters")]
	public class InBuildParameters : ScriptableObject
	{
		public static InBuildParameters InBuildParametersInstance
		{
			get
			{
				if (instance == null)
				{
					instance = Resources.Load<InBuildParameters>("InBuildParameters");
				}

				return instance;
			}
		}

		private static InBuildParameters instance;

		public BuildType BuildType { get { return buildType; } }
		public bool Offline { get { return offlineBuild || offline; } }
		public bool OfflineBuild { get { return offlineBuild; } }
		public bool ForceUpdate { get { return forceUpdate; } }
		public string ServerUrl { get { return serverUrl; } }
		public string BundlePrefix { get { return bundlePrefix; } }
		public string BundleFolder { get { return bundleFolder; } }
		public string LocalStoragePath
		{
			get
			{
				var path =
#if UNITY_EDITOR
				 Directory.GetParent(Application.dataPath).FullName;
#else
			 Application.persistentDataPath;
#endif
				if (offlineBuild)
				{
					path = Path.Combine(path, "Assets/StreamingAssets");
				}

				return path;
			}
		}
		public TextAsset GameParameters { get { return gp; } }
		public TextAsset GameParametersManifest { get { return gpManifest; } }


		[SerializeField] private BuildType buildType;
		[SerializeField] private bool offline;
		[SerializeField] private bool offlineBuild;
		[SerializeField] private bool forceUpdate;
		[SerializeField] private string serverUrl;
		[SerializeField] private string bundlePrefix;
		[SerializeField] private string bundleFolder;

		[SerializeField] private TextAsset gp;
		[SerializeField] private TextAsset gpManifest;
	}
}