﻿using MTCore;
using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace MTParameters
{
    public class GameParametersViewer : EditorWindow
    {
        public static readonly Color HEADER_COLOR = new Color(0f, 1f, 0f, 0.2f);
        public static readonly Color HEADER_COLOR2 = new Color(1f, 1f, 0f, 0.12f);
        public static readonly Color LINE_COLOR = new Color(0f, 1f, 0.7f, 0.05f);
        public static readonly Color TOWN_LINE = new Color(1f, 1f, 0.0f, 0.1f);

        private int languageIndex;
        private Language language;
        private Vector2 scrollPos;
        bool showTranslation = false;
        bool showTowns = false;
        bool showTrains = false;
        bool showCapibaras = false;
        bool showResources = true;

        private static GameParameters _gp;


        [MenuItem("MT/Game Parameters Viewer #&4")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(GameParametersViewer));
            _gp = GetParameters();
        }


        public static GameParameters GetParameters()
        {
            GameParameters result = GetParamsByPath<GameParameters>("gp");
            result.LoadPostProcess();
            return result;
        }


        public string GetTranslation(string key)
        {
            if (_gp == null)
                return "";
            TranslationManager tManager = new TranslationManager(_gp);
            return tManager.GetTableString(key);
        }


        private void OnGUI()
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Language: ");
            languageIndex = EditorGUILayout.IntSlider(languageIndex, 0, _gp.Languages.Length-1);
            language = _gp.Languages[languageIndex];

            
            GUILayout.EndHorizontal();

            scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
            EditorGUILayout.BeginVertical();

            #region Resources
            showResources = EditorGUILayout.Foldout(showResources, "Resources");
            Rect r = GUILayoutUtility.GetLastRect();
            EditorGUI.DrawRect(r, HEADER_COLOR);
            if (showResources)
            {
                //Resources
                for (int j = 0; j < _gp.Resources.Length; j++)
                {
                    //Resource resource
                    Resource resource = _gp.Resources[j];
                    StringBuilder resourceBuilder = new StringBuilder();
                    resourceBuilder.Append(j);
                    resourceBuilder.Append(": ");
                    resourceBuilder.Append(GetTranslation(resource.TranslationKey));
                    resourceBuilder.Append("| Is Cargo: ");
                    resourceBuilder.Append(resource.IsCargo);
                    resourceBuilder.Append(" {");
                    resourceBuilder.Append(resource.ResultsOrder);
                    resourceBuilder.Append("}");
                    DrawLine(resourceBuilder.ToString(), LINE_COLOR);
                }
            }
            #endregion
            #region Towns
            showTowns = EditorGUILayout.Foldout(showTowns, "Towns");
            Rect r2 = GUILayoutUtility.GetLastRect();
            EditorGUI.DrawRect(r2, HEADER_COLOR);
            if (showTowns)
            {
                for (int j = 0; j < _gp.Towns.Length; j++)
                {
                    //Town
                    Town town = _gp.Towns[j];
                    StringBuilder townName = new StringBuilder();
                    townName.Append("Town: ");
                    townName.Append(j);
                    townName.Append(" ");
                    townName.Append(GetTranslation(town.TranslationKey));
                    DrawLine(townName.ToString(), HEADER_COLOR2);

                    StringBuilder st = new StringBuilder();
                    st = new StringBuilder();
                    st.Append("Prefab: ");
                    st.Append(town.MainBundle);

                    DrawLine(st.ToString(), LINE_COLOR);

                    st = new StringBuilder();
                    st.Append("Resources: ");
                    foreach (int resource in town.Resources)
                    {
                        Resource currentResource = _gp.Resources[resource];
                        st.Append("\n");
                        st.Append(GetTranslation(currentResource.TranslationKey));
                    }

                    DrawLine(st.ToString(), LINE_COLOR);

                    st = new StringBuilder();
                    st.Append("Neighbours: ");
                    foreach (int neighbour in town.Neighbours)
                    {
                        Town neighbourTown = _gp.Towns[neighbour];
                        st.Append("\n");
                        st.Append(GetTranslation(neighbourTown.TranslationKey));
                    }
                    DrawLine(st.ToString(), LINE_COLOR);

                    /*
                    st = new StringBuilder();
                    st.Append("Town Buildings: ");
                    foreach (int bld in town.Buildings)
                    {
                        TownBuilding tb = EditorGameParameters.TownBuildings[bld];
                        st.Append("\n");
                        st.Append(GetTranslation(tb.TranslationKey));
                    }

                    DrawLine(st.ToString(), LINE_COLOR);
                    */
                }
            }
            #endregion

            #region Trains
            showTrains = EditorGUILayout.Foldout(showTrains, "Trains/Coaches");
            Rect r3 = GUILayoutUtility.GetLastRect();
            EditorGUI.DrawRect(r3, HEADER_COLOR);
            if (showTrains)
            {
                DrawLine("Trains", HEADER_COLOR2);
                DrawLine(string.Format("Speed Offset: {0} Angle Offset: {1}",
                    _gp.TrainShared.CameraHighSpeedOffset, _gp.TrainShared.CameraAngleOffset),
                    LINE_COLOR);
                for (int j = 0; j < _gp.Trains.Length; j++)
                {
                    Train train = _gp.Trains[j];
                    StringBuilder trainString = new StringBuilder();
                    trainString.Append("Id ");
                    trainString.Append(train.ID);
                    trainString.Append(" Speed Factor: ");
                    trainString.Append(train.SpeedFactor);
                    trainString.Append(" Speed : ");
                    trainString.Append(train.Speed);
                    trainString.Append(" SpeedIncrease : ");
                    trainString.Append(train.SpeedIncrease);
                    trainString.Append(" FlipTime:");
                    trainString.Append(train.FlipTime);
                    trainString.Append(" Traction Power: ");
                    trainString.Append(train.TractionPower);    
                    trainString.Append(" Hit Points : ");
                    trainString.Append(train.HitPoints);
                    trainString.Append(" Price : ");
                    trainString.Append(train.Price);
                    trainString.Append(" CogPrice : ");
                    trainString.Append(train.CogCoinsPrice);
                    trainString.Append(" Indispensabe: ");
                    trainString.Append(train.IndispensableCoach);
                    if (train.IsExpress)
                        trainString.Append(" (Express)");
                    trainString.Append(" Bundle: ");
                    trainString.Append(train.MainBundle);
                    DrawLine(trainString.ToString(), LINE_COLOR);
                }

                //Coaches
                DrawLine("Coaches", HEADER_COLOR2);
                for (int j = 0; j < _gp.Coaches.Length; j++)
                {
                    Coach coach = _gp.Coaches[j];
                    StringBuilder coachString = new StringBuilder();
                    coachString.Append("Id ");
                    coachString.Append(coach.ID);
                    coachString.Append(" ($");
                    coachString.Append(coach.Price);
                    coachString.Append("/ *");
                    coachString.Append(coach.CogCoinsPrice);
                    coachString.Append(")");
                    coachString.Append(" Capacity:");
                    coachString.Append(coach.Capacity);
                    coachString.Append(". ");
                    coachString.Append("Weight: ");
                    coachString.Append(coach.Weight);
                    coachString.Append(" Bundle: ");
                    coachString.Append(coach.MainBundle);

                    if (coach.IsExpress)
                        coachString.Append(" (Express)");
                    if (coach.Resources != null)
                    {
                        foreach (int k in coach.Resources)
                        {
                            //this coach can not move resources
                            if (k < 0)
                            {
                                coachString.Append("\n\t");
                                coachString.Append("None");
                            }
                            else
                            {
                                string n = GetTranslation(_gp.Resources[k].TranslationKey);
                                coachString.Append("\n\t");
                                coachString.Append(n);
                            }
                        }
                    }
                    DrawLine(coachString.ToString(), LINE_COLOR);
                }
            }
            #endregion

            #region Capibaras
            showCapibaras = EditorGUILayout.Foldout(showCapibaras, "Capibaras");
            Rect r4 = GUILayoutUtility.GetLastRect();
            EditorGUI.DrawRect(r4, HEADER_COLOR);
            if (showCapibaras)
            {
                DrawLine("Capibaras", HEADER_COLOR2);
                for (int j = 0; j < _gp.Capibaras.Length; j++)
                {
                    MTCapibara capibara = _gp.Capibaras[j];
                    StringBuilder taskString = new StringBuilder();
                    taskString.Append("Id: ");
                    taskString.Append(capibara.Id);
                    taskString.Append(" ");
					taskString.Append("Breed: ");
					taskString.Append(capibara.Breed);
					taskString.Append("\n\t");
					taskString.Append("Features: ");
					taskString.Append(capibara.Features);
					taskString.Append("\n\t");
					taskString.Append("IsActive: ");
					taskString.Append(capibara.IsActive);
					taskString.Append(" ");
					taskString.Append("Priority: ");
					taskString.Append(capibara.Priority);
					taskString.Append("\n\t");
					taskString.Append(language.GetString($"Capibara.{capibara.Id}.Title"));
                    taskString.Append("\n\t");
					taskString.Append(language.GetString($"Capibara.{capibara.Id}.Description"));
					taskString.Append("\n\t");
					taskString.Append(capibara.Targets);
                    taskString.Append(" => ");
                    taskString.Append(capibara.Rewards);
					taskString.Append(" => ");
					taskString.Append("rewarder: " + (int.TryParse(capibara.Rewarder, out int rewarder)? ((MTRewarderType)rewarder).ToString() : "empry") );
					DrawLine(taskString.ToString(), LINE_COLOR);
                }
            }
            #endregion

            //Language
            showTranslation = EditorGUILayout.Foldout(showTranslation, "Translation");
            Rect r5 = GUILayoutUtility.GetLastRect();
            EditorGUI.DrawRect(r5, HEADER_COLOR);
            if (showTranslation)
            {
                string languageName = _gp.Languages[languageIndex].Name;
                DrawLine(languageName, HEADER_COLOR2);
                int i = 0;
                foreach (KeyValuePair<string, string> kvp in _gp.Languages[languageIndex].StringsDict)
                {
                    StringBuilder st = new StringBuilder();
                    st.Append(kvp.Key);
                    st.Append(": ");
                    st.Append(kvp.Value);
                    DrawLine(st.ToString(), LINE_COLOR);
                    i++;
                }
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndScrollView();
        }


        private void DrawLine(string text, Color colour)
        {
            GUILayout.Label(text);
            Rect r = GUILayoutUtility.GetLastRect();
            EditorGUI.DrawRect(r, colour);
        }


        private static T GetParamsByPath<T>(string path) where T: ProtoParameters
        {
            TextAsset textAsset = Resources.Load<TextAsset>(path);
            if (textAsset == null)
            {
                Debug.LogErrorFormat("Can not load text asset at path: {0}", path);
                return null;
            }

            T t = ProtoParameters.FromJson<T>(textAsset.text);
            if (t == null)
            {
                Debug.LogErrorFormat("Can not convert text to {1} : {0}", textAsset.text, typeof(T));
                return null;
            }

            return t;
        }
    }
}   