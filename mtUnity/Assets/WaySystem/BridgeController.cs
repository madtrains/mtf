﻿using MTRails;
using Newtonsoft.Json.Bson;
using UnityEngine;

namespace WaySystem
{
    public class BridgeController : UberBehaviour
    {
        public static int hOn = Animator.StringToHash("on");
        public static int hBridgeUp = Animator.StringToHash("bridgeUp");

        [SerializeField] private Animator animator;
        [SerializeField] private RRCollision rrCollision;
        [SerializeField] private Transform move;
        private bool down;

        public void CockTheBridge()
        {
            animator.Play(hBridgeUp);
        }

        private void Awake()
        {
            rrCollision.OnHornEntersTrigger += (horn) =>
            {
                if (!down)
                {
                    BridgeDown();
                    down = true;
                }
            };
        }

        private void BridgeDown()
        {
            animator.SetTrigger(hOn);
        }
    }
}