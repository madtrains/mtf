﻿using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.BackGround
{
    public class CameraFollower : UberBehaviour
    {
        [SerializeField] private Vector3 offset;


        private void LateUpdate()
        {
            transform.position = GameStarter.Instance.MainComponents.GameCamera.transform.position + offset;
        }
    }
}