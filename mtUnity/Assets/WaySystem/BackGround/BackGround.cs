﻿using MTTown;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.BackGround
{
    public class BackGround : UberBehaviour
    {
        public AudioSource Music { get { return music; } }

        [SerializeField] protected GameObject wall;
        [SerializeField] protected GameObject floor;
        [SerializeField] protected Transform end;
        [SerializeField] protected Transform sun;
        [SerializeField] protected bool lights;
        [SerializeField] protected AudioSource music;

        [SerializeField] TownLight lightPreset;


        public void Apply()
        {
            GameStarter.Instance.MainComponents.GameLight.Set(lightPreset);
        }

        public GameObject Copy(bool asCorner)
        {
            GameObject newWall = GameObject.Instantiate<GameObject>(wall);
            if (!asCorner)
            {
                GameObject newFloor = GameObject.Instantiate<GameObject>(floor);
                newFloor.transform.SetParent(newWall.transform);
            }
            else
            {
                newWall.transform.Rotate(Vector3.up * 90f);
                for (int i = 0; i < newWall.transform.childCount; i++)
                {
                    Transform child = newWall.transform.GetChild(i);
                    Renderer rend = child.GetComponent<Renderer>();
                    rend.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                }
            }
            return newWall;
        }

        public void Close(float distacne)
        {
            float half = distacne / 2f;
            if (sun != null)
                sun.position = new Vector3(sun.transform.position.x, sun.transform.position.y, half);
        }

        public Vector3 EndGlobal
        {
            get
            {
                return end.position;
            }
        }

        public float Offset { get { return end.transform.localPosition.z; } }

        public void Lights()
        {
            Environment.LightingDevice[] lightingDevices = FindObjectsOfType<Environment.LightingDevice>();
            foreach (Environment.LightingDevice lightingDevice in lightingDevices)
            {
                lightingDevice.On = this.lights;
            }
        }
    }
}