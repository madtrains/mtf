﻿namespace WaySystem.PickUp
{
    public enum Type { Coin, CogCoin, Prize, GearRepair, Ticket, CoinSilver, CoinGold }
}