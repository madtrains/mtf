﻿using Trains;
using UnityEngine;

namespace WaySystem.PickUp
{
    public class PickUp : Triggers.TrainTrigger
    {
        public static int hOffset = Animator.StringToHash("offset");

        public Type PickUpType { get { return type; } }
        [SerializeField] private Type type;
        [SerializeField] private Animator animator;
        [SerializeField] private VisibilityChecker visibilityChecker;
        [SerializeField] private GameObject phys;


        protected virtual void Start()
        {
            float value = Random.Range(0f, 1f);
            animator.SetFloat(hOffset, value);
            visibilityChecker.OnVisibilityChanged = VisibilityChanged;
        }

        protected virtual void VisibilityChanged(bool value)
        {
            Activate(phys, value);
        }

        public void Off(Train train)
        {
            Activate(this.gameObject, false);
        }
    }
}