﻿using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.Cargos
{
    public class CargoPassengers : Cargo
    {
        public enum DivisionMode { Half2Half, AllToUnload, HalfsAndRandom }

        public CargoPoint[] CargoPoints { get { return cargoPoints; } }
        public Passenger[] Collection { get { return collection; } }

        [SerializeField] private CargoPoint[] cargoPoints;
        [SerializeField] protected DivisionMode divisionMode;
        [SerializeField] [Range(0.1f, 0.3f)] protected float randomDivider = 0.3f;
        private Passenger[] collection;
        private Passenger[] passengers;
        private List<Passenger> inTrainLoad;
        private List<Passenger> ontoPlatformUnload;

        public override void Prepare()
        {
            base.Prepare();
            Place();
            //погрузка в вагоны разрешена - делим пассажиров пополам посадка/высадка
            switch (divisionMode)
            {
                case DivisionMode.Half2Half:
                {
                    inTrainLoad = new List<Passenger>(passengers);
                    ontoPlatformUnload = new List<Passenger>();
                    while (ontoPlatformUnload.Count < inTrainLoad.Count)
                    {
                        Passenger unloadPassenger =
                            GetRandomAndPop<Passenger>(inTrainLoad);
                        ontoPlatformUnload.Add(unloadPassenger);
                        unloadPassenger.Mode = Mode.OutOfTrainUnload;
                        unloadPassenger.Reset();
                    }

                    foreach (Passenger passenger in inTrainLoad)
                    {
                        passenger.Mode = Mode.InTrainLoad;
                        passenger.Reset();
                    }
                    break;
                }
                case DivisionMode.AllToUnload:
                {
                    inTrainLoad = new List<Passenger>();
                    ontoPlatformUnload = new List<Passenger>(passengers);
                    foreach (Passenger passenger in ontoPlatformUnload)
                    {
                        passenger.Mode = Mode.OutOfTrainUnload;
                        passenger.Reset();
                    }
                    break;
                }

                case DivisionMode.HalfsAndRandom:
                {
                    int half = Mathf.FloorToInt((float)passengers.Length / 2f);
                    int randomCount = Mathf.FloorToInt((float)half * randomDivider);
                    randomCount = UnityEngine.Random.Range(0, randomCount);
                    int loadCount = half - randomCount;

                    inTrainLoad = new List<Passenger>(passengers);
                    ontoPlatformUnload = new List<Passenger>();
                    while (inTrainLoad.Count > loadCount)
                    {
                        Passenger unloadPassenger =
                            GetRandomAndPop<Passenger>(inTrainLoad);
                        if (unloadPassenger == null)
                        {
                            Debug.LogErrorFormat("NULL REF Passenger");
                            break;
                        }
                        ontoPlatformUnload.Add(unloadPassenger);
                        unloadPassenger.Mode = Mode.OutOfTrainUnload;
                        unloadPassenger.Reset();
                    }

                    foreach (Passenger passenger in inTrainLoad)
                    {
                        passenger.Mode = Mode.InTrainLoad;
                        passenger.Reset();
                    }
                    break;
                }
            }
        }

        public void AssignCollection(Passenger[] collection)
        {
            this.collection = collection;
        }

        public override CargoElement LoadUnload(Mode mode)
        {
            switch (mode)
            {
                case (Mode.InTrainLoad):
                    return GetRandomAndPop<Passenger>(inTrainLoad);
                case (Mode.OutOfTrainUnload):
                    return GetRandomAndPop<Passenger>(ontoPlatformUnload);
                default:
                    return GetRandomAndPop<Passenger>(inTrainLoad);
            }
        }

        private void Place()
        {
            if (collection.Length < cargoPoints.Length)
            {
                Debug.LogErrorFormat(
                    "Passengers collection count of {0} is not enough for cargo {1}. {2}",
                    collection.Length, cargoPoints.Length, this.gameObject.name);
                return;
            }
            for (int i = 0; i < Mathf.Min(collection.Length, cargoPoints.Length); i++)
            {
                Passenger passenger = collection[i];
                PlacePassenger(passenger, i);
            }
        }

        private void PlacePassenger(Passenger passenger, int index)
        {
            CargoPoint cargoPoint = cargoPoints[index];
            passenger.transform.SetParent(
                cargoPoint.transform, false);

            passenger.transform.localPosition = Vector3.zero;
            passenger.transform.localRotation = Quaternion.identity;
            passenger.BaseTransform = cargoPoint.transform;
            passenger.PassengerWay = cargoPoint.PassengerWay;
            if (passengers == null)
            {
                passengers = new Passenger[cargoPoints.Length];
            }
            passengers[index] = passenger;
        }

        protected override int GetUnloadCount()
        {
            return ontoPlatformUnload.Count;
        }
    }
}