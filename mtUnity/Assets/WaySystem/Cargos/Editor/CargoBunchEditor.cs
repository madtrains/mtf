﻿using UnityEngine;
using UnityEditor;

namespace WaySystem.Cargos
{

    [CustomEditor(typeof(CargoBunch))]
    public class CargoBunchEditor : UberEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            CargoBunch cargoBunch = (CargoBunch)target;
            if (GUILayout.Button("MakeDocks"))
            {
                cargoBunch.ProcessElements((ce) => 
                { 
                    if (ce.BaseTransform != null) 
                    { 
                        DestroyImmediate(ce.BaseTransform);
                    }
                    GameObject ceBaseGO = new GameObject(ce.name + "_base");
                    ceBaseGO.transform.SetParent(ce.transform.parent);
                    ceBaseGO.transform.position = ce.transform.position;
                    ceBaseGO.transform.rotation = ce.transform.rotation;
                    ce.BaseTransform = ceBaseGO.transform;
                    ce.FindAndFillRenderers();
                });
            }
        }
    }
}