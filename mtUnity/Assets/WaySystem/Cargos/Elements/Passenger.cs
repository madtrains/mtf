﻿using MTCharacters;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

namespace WaySystem.Cargos
{
    public class Passenger : CargoElement
    {
        public CargoPassengerWay PassengerWay { get { return way; } set { way = value; } }

        public void Set(CharacterLogic characterLogic, float goAwayMoveSpeed, float goAwayRotateSpeed)
        {
            this.characterLogic = characterLogic;
            this.goAwayMoveSpeed = goAwayMoveSpeed;
            this.goAwayRotateSpeed = goAwayRotateSpeed;
        }

        [SerializeField] private float goAwayMoveSpeed;
        [SerializeField] private float goAwayRotateSpeed;
        [SerializeField] private CharacterLogic characterLogic;

        private bool goAway;
        private List<Point> points;
        private CargoPassengerWay way;
        private float cSpeed;
        private float cT;

        protected override void ResetAsInTrainLoad()
        {
            base.ResetAsInTrainLoad();
            goAway = false;
            characterLogic.AnimState = AnimState.Basic;
            transform.position = baseTransform.position;
            transform.forward = baseTransform.forward;
        }

        protected override void SetVisibility(bool visible)
        {
            Activate(characterLogic.Graphics.gameObject, visible);
        }

        protected override void ResetAsOnPlatformUnload()
        {
            base.ResetAsOnPlatformUnload();
            goAway = false;
            characterLogic.AnimState = AnimState.Basic;
            transform.position = baseTransform.position;
            transform.forward = baseTransform.forward * -1f;
        }

        protected override void FlyEnd()
        {
            base.FlyEnd();
            if (Mode == Mode.OutOfTrainUnload)
            {
                GoAway();
            }
                
        }

        public void GoAway()
        {
            characterLogic.AnimState = AnimState.Walk;
            points = new List<Point>();
            points.Add(new Point(transform));
            foreach(Marker m in way.Points)
            {
                points.Add(new Point(m.transform));
            }
            goAway = true;
            CalculateSpeed();
        }

        private void CalculateSpeed()
        {
            float distance = Vector3.Distance(points[0].Position, points[1].Position);
            cSpeed = goAwayMoveSpeed / distance;
            cT = 0;
        }

        protected override void Update()
        {
            base.Update();

            if (!goAway)
                return;
            //transform.Translate(points[0].Position, Space.World);
            /*
            
            */
            cT += Time.deltaTime * cSpeed;
            cT = Mathf.Clamp(cT, 0f, 1f);
            if (cT == 1f)
            {
                if (points.Count > 2)
                {
                    points.RemoveAt(0);
                    CalculateSpeed();
                }
                else
                {
                    GoneAway();
                }
            }

            transform.position = Vector3.Lerp(
                points[0].Position, points[1].Position,
                cT);

            Vector3 relative = points[1].Position - transform.position;
            if (relative.sqrMagnitude >  2f)
            {
                //Debug.LogFormat("{0} has rotation vector {1} / {2}", gameObject.name, relative, relative.sqrMagnitude);
                Quaternion rotation = Quaternion.LookRotation(relative);
                rotation = Quaternion.Euler(0f, rotation.eulerAngles.y, 0f);

                transform.rotation = Quaternion.Lerp(
                    transform.rotation, rotation,
                    Time.deltaTime * goAwayRotateSpeed);
            }
        }

        private void GoneAway()
        {
            goAway = false;
            characterLogic.AnimState = AnimState.Basic;
            SetVisibility(false);
        }
    }
}