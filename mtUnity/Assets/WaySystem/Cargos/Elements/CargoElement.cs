﻿using UnityEngine;

namespace WaySystem.Cargos
{
    public enum Mode { InTrainLoad, OutOfTrainUnload }
    public class CargoElement : TimerBeh
    {
        public Transform BaseTransform { get { return baseTransform; } set { baseTransform = value; } }
        public Mode Mode { get { return mode; } set { SetMode(value); } }
        public delegate void OnFlyEnd();
        public Renderer[] Renderers { get { return renderers; } set { renderers = value; } }

        [SerializeField] protected Renderer[] renderers;
        [SerializeField] protected Transform baseTransform;
        protected Transform targetTransform;
        protected OnFlyEnd onFlyEnd;

        private enum State { Shown, Waiting, Fly, Hidden }

        private float arcHeight;
        private float waitTime;
        private float flyTime;
        private Mode mode;
        private bool hideOnLoadEnd;
        private bool orient;

        public bool Visible { set { this.SetVisibility(value); } }

        public virtual void Init()
        {
            
        }

        public virtual void Reset()
        {
            switch (mode)
            {
                case (Mode.InTrainLoad):
                {
                    ResetAsInTrainLoad();
                    break;
                }
                case (Mode.OutOfTrainUnload):
                {
                    ResetAsOnPlatformUnload();
                    break;
                }
            }
        }

        protected virtual void ResetAsInTrainLoad()
        {
            if (baseTransform == null)
                Debug.LogErrorFormat("base transform is null {0}", this.name);
            this.transform.position = baseTransform.position;
            Visible = true;
        }

        protected virtual void ResetAsOnPlatformUnload()
        {
            Visible = false;
        }

        private void SetMode(Mode mode)
        {
            this.mode = mode;
            switch (mode)
            {
                case (Mode.InTrainLoad):
                {
                    Visible = true;
                    break;
                }
                case (Mode.OutOfTrainUnload):
                {
                    Visible = false;
                    break;
                }
            }
        }

        protected virtual void SetVisibility(bool state)
        {
            if (renderers == null || renderers.Length== 0)
            {
                Debug.LogErrorFormat("Render Link is null or number of renderers is 0 ({0} {1})", this.gameObject.name, this.transform.position);
            }
            foreach(Renderer renderer in renderers)
            {
                renderer.enabled = state;
            }
        }

        public void Run(
            Transform targetTransform, float arcHeight, float waitTime, float flyTime,
            OnFlyEnd onFlyEnd, bool hideLoadOnEnd, bool orient)
        {
            this.targetTransform = targetTransform;
            this.arcHeight = arcHeight;
            this.waitTime = waitTime;
            this.flyTime = flyTime;
            this.OnEnd += OnWaitEnd;
            Launch(this.waitTime);
            this.onFlyEnd = onFlyEnd;
            this.hideOnLoadEnd = hideLoadOnEnd;
            this.orient = orient;
        }

        public void FindAndFillRenderers()
        {
            System.Collections.Generic.List<Renderer> rnd = new System.Collections.Generic.List<Renderer>();
            foreach(var c in gameObject.GetComponents<Renderer>())
            {
                if (!rnd.Contains(c))
                    rnd.Add(c);
            }
            foreach(var c in gameObject.GetComponentsInChildren<Renderer>())
            {
                if(!rnd.Contains(c))
                    rnd.Add(c);
            }
            renderers = rnd.ToArray();
        }

        protected virtual void OnWaitEnd()
        {
            this.OnEnd -= OnWaitEnd;
            this.OnEnd += FlyEnd;
            this.OnTimeChanged += Fly;
            Launch(this.flyTime);
            if (UberBehaviour.RandomBool)
                GameStarter.Instance.MainComponents.SoundManager.Shared.Play(MTSound.SharedNames.CargoWhoosh);
            else
                GameStarter.Instance.MainComponents.SoundManager.Shared.Play(MTSound.SharedNames.CargoWhoosh2);
            switch (mode)
            {
                //вылетающий из поезда груз становится видимым сразу
                case (Mode.OutOfTrainUnload):
                {
                    this.transform.position = this.targetTransform.position;
                    Visible = true;
                    break;
                }
            }
            //GameStarter.Instance.Train.SoundCargo();
        }

        protected virtual void FlyEnd()
        {
            this.OnEnd -= FlyEnd;
            this.OnTimeChanged -= Fly;
            onFlyEnd?.Invoke();
            onFlyEnd = null;
            switch (mode)
            {
                case (Mode.InTrainLoad):
                {
                    if (hideOnLoadEnd)
                    {
                        Visible = false;
                    }
                    break;
                }
            }
        }

        private void Fly(float t)
        {
            Vector3 middle = Vector3.Lerp(baseTransform.position, targetTransform.position, 0.5f);
            middle = middle + targetTransform.up * arcHeight;
            //Load
            Vector3 start = baseTransform.position;
            Vector3 end = targetTransform.position;

            Quaternion identity = baseTransform.rotation;
            Quaternion targetRotation = targetTransform.rotation;
            //Unload
            if (mode == Mode.OutOfTrainUnload)
            {
                start = targetTransform.position;
                end = baseTransform.position;
                identity = targetTransform.rotation;
                targetRotation = baseTransform.rotation;
            }

            Vector3 result = 
                Vector3.Lerp(
                    Vector3.Lerp(start, middle, t),
                    Vector3.Lerp(middle, end, t),
                    t);
            transform.position = result;

            if (orient)
                transform.rotation = Quaternion.Slerp(identity, targetRotation, t);
        }
    }
}