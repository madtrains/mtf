﻿using Trains;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.Cargos
{
    public class CargoPoint : UberBehaviour
    {
        public static readonly float SIZE = 0.5f;

        public CargoPassengerWay PassengerWay { get { return passengerWay; } }
        [SerializeField] private CargoPassengerWay passengerWay; 


        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Draw();
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            Draw();
        }

        private void Draw()
        {
            Gizmos.DrawWireSphere(transform.position, SIZE);
            Gizmos.DrawSphere(transform.position, 0.2f);
            if (passengerWay != null)
            {
                Gizmos.DrawLine(transform.position, passengerWay.Points[0].transform.position);
            }
        }
    }
}