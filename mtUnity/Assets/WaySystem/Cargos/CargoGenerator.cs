﻿using UnityEngine;
using WaySystem.Collection;

namespace WaySystem.Cargos
{
    public class CargoGenerator : UberBehaviour
    {
        [SerializeField] private CargoElement refElement;

        public void Init()
        {
            refElement.Visible = false;
        }

        public CargoElement Generate()
        {
            CargoElement cargoElement = Instantiate<CargoElement>(refElement);
            cargoElement.transform.position = refElement.transform.position;
            cargoElement.transform.rotation = refElement.transform.rotation;
            cargoElement.Visible = true;
            cargoElement.Init();
            return cargoElement;
        }
    }
}