﻿using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.Cargos
{
    /// <summary>
    /// Родительский класс любого груза, который стоит на платформе
    /// </summary>
    public class Cargo : UberBehaviour
    {
        public int ResourceID { get { return resourceID; } }

        public int PlatformLength { get { return platformLength; } }
        [SerializeField] protected int platformLength;

        public int PlatformWidth { get { return platformWidth; } }
        [SerializeField] protected int platformWidth;

        [SerializeField] protected int resourceID;

        public int UnloadPlacesCount { get { return GetUnloadCount(); } }

        public virtual void Init(int resourceID)
        {
            this.resourceID = resourceID;
        }

        /// <summary>
        /// при въезде позда на другой блок груз назначается на станцию этого блока
        /// </summary>
        public virtual void Prepare() { }

        public virtual CargoElement LoadUnload(Mode mode) { return null; }

        public T GetRandomAndPop<T>(List<T> list) where T : CargoElement 
        {
            if (list.Count == 0)
                return null;
            int randomIndex = Random.Range(0, list.Count);
            T cargoElement = list[randomIndex];
            list.RemoveAt(randomIndex);
            return cargoElement;
        }

        protected virtual int GetUnloadCount()
        {
            return 0;
        }
    }
}