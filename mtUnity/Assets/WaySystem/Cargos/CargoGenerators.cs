﻿using UnityEngine;

namespace WaySystem.Cargos
{
    public class CargoGenerators : Cargo
    {
        [SerializeField] protected CargoGenerator[] cargoGenerators;
        private int i;
        private float delay;

        public override void Init(int resourceID)
        {
            base.Init(resourceID);
            foreach(CargoGenerator generator in cargoGenerators) 
            { 
                generator.Init();
            };
        }

        public override CargoElement LoadUnload(Mode mode)
        {
            CargoElement result = cargoGenerators[i].Generate();
            i++;
            if (i >= cargoGenerators.Length)
                i = 0;
            return result;
        }
    }
}