﻿using Trains;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

namespace WaySystem.Cargos
{
    public class CargoPassengerWay : UberBehaviour
    {
        public Marker[] Points { get { return points; } }
        [SerializeField] private Marker[] points;
    }
}