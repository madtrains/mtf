﻿using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.Cargos
{
    public delegate void ProcessCargoElement(CargoElement cargoElement);

    public class CargoBunch : Cargo
    {
        [SerializeField] protected CargoElement[] elementsToLoad;
        [SerializeField] protected CargoElement[] elementsToUnload;
        protected List<CargoElement> toLoad;
        protected List<CargoElement> toUnload;

        public override void Init(int resourceID)
        {
            base.Init(resourceID);
            foreach(CargoElement element in elementsToLoad)
            {
                element.Init();
            }

            foreach(CargoElement element in elementsToUnload)
            {
                element.Init();
            }
        }

        public override void Prepare()  
        {
            base.Prepare();
            toLoad = new List<CargoElement>();
            foreach (CargoElement el in elementsToLoad)
            {
                el.Mode = Mode.InTrainLoad;
                el.Reset();
                toLoad.Add(el);
            }

            toUnload = new List<CargoElement>();
            foreach(CargoElement el in elementsToUnload)
            {
                el.Mode = Mode.OutOfTrainUnload;
                el.Reset();
                toUnload.Add(el);
            }
        }

        public override CargoElement LoadUnload(Mode mode)
        {
            switch(mode)
            {
                case (Mode.InTrainLoad):
                {
                    if (toLoad.Count > 0)
                    {
                        CargoElement cargoElement = toLoad[0];
                        toLoad.RemoveAt(0);
                        return cargoElement;
                    }
                    else
                    {
                        return null;
                    }
                }

                case (Mode.OutOfTrainUnload):
                {
                    if(toUnload.Count > 0)
                    {
                        CargoElement cargoElement = toUnload[0];
                        toUnload.RemoveAt(0);
                        return cargoElement;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return null;
        }

        public void ProcessElements(ProcessCargoElement pce)
        {
            foreach(CargoElement ce in elementsToLoad)
            {
                pce(ce);
            }

            foreach(CargoElement ce in elementsToUnload)
            {
                pce(ce);
            }
        }

        protected override int GetUnloadCount()
        {
            return toUnload.Count;
        }
    }
}