using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.Sequences
{
    /// <summary>
    /// ������������ ��� ��������� ������ �������� �������������������
    /// ��������� ��������
    /// </summary>
    [System.Serializable]
    public class Sequence : WeightedElement
    {
        public Sequence(List<Element> elementList, Color color, float length)
        {
            this.elements = elementList.ToArray();
            this.debugColor = color;
            this.length = length;
        }

        public Element[] Elements { get { return elements; } }
        public Color DebugColor { get { return debugColor; } }
        public int Difficulty { get { return difficulty; } }
        public float Length { get { return length; } }

        [SerializeField] private Element[] elements;
        [SerializeField] private float length;
        [SerializeField] private Color debugColor;
        [SerializeField] private int difficulty;

        //������������ �����
        public Sequence Scaled(float t)
        {
            List<Element> elements = new List<Element>();
            foreach (NumberedSequence ns in NumberedSequences)
            {
                if (ns.isScalable)
                {
                    int scaledCount = Mathf.RoundToInt( t * (float)ns.count);
                    for (int i = 0; i < scaledCount; i++)
                    {
                        elements.Add(ns.element);
                    }
                }
                else
                {
                    elements.Add(ns.element);
                }
            }
            return new Sequence(elements, debugColor, length * t);
        }

        public void AssignElements(Element[] elements)
        {
            this.elements = elements;
        }

        //������������� ������������������ � ������������������ � ����������� ������ ����������
        public List<NumberedSequence> NumberedSequences
        {
            get
            {
                List<NumberedSequence> sequences = new List<NumberedSequence>();
                Element brick = new Element();
                brick.Type = WayChunkType.Brick;
                brick.Flipped = false;

                int count = 0;
                for (int i = 0; i < elements.Length; i++)
                {
                    Element element = elements[i];
                    if (element.Type == WayChunkType.Brick)
                    {
                        count += 1;
                    }
                    else
                    {
                        if (count > 0)
                        {
                            sequences.Add(new NumberedSequence(brick, count, true));
                            count = 0;
                        }
                        sequences.Add(new NumberedSequence(element, 1, false));
                    }
                    //��������� �������
                    if (i == elements.Length -1)
                    {
                        if (count > 0 && element.Type == WayChunkType.Brick)
                        {
                            sequences.Add(new NumberedSequence(brick, count, true));
                        }
                    }
                }
                return sequences;
            }
        }
    }


    [System.Serializable]
    public class NumberedSequence
    {
        public NumberedSequence(Element element, int count, bool isScalable)
        {
            this.element = element;
            this.count = count;
            this.isScalable = isScalable;
        }

        public Element element;
        public int count;
        public bool isScalable;
    }
}