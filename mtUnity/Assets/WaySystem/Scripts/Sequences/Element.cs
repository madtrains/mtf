using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.Sequences
{
    /// <summary>
    /// используется для текстовой записи линейных последовательностей
    /// текстовых пресетов
    /// </summary>
    [System.Serializable]
    public class Element
    {
        public WayChunkType Type { get { return wayChunkType; } set { wayChunkType = value; } }
        public bool Flipped { get { return flipped; } set { flipped = value; } }


        [SerializeField] protected WayChunkType wayChunkType;
        [SerializeField] protected bool flipped;
    }
}