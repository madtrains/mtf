﻿using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.Sequences
{
    [CreateAssetMenu(menuName = "SequenceCollection")]
    [System.Serializable]
    public class SequencesCollection : ScriptableObject
    {
        public Sequence[] Sequences { get { return sequences; } }
        public Sequence[] sequences;

        public void AssignSequence(Element[] elements, int targetIndex)
        {
            sequences[targetIndex].AssignElements(elements);
        }
    }
}