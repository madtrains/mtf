using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/*
namespace WaySystem.Sequences
{
    public class SequenceEditor : EditorWindow
    {
        [MenuItem("MT/WaySystem/Sequence Editor")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(SequenceEditor));
        }

        private Collection.Collection collection;
        private SequencesCollection sequenceCollectionSO;
        private Transform root;
        private int index;
        private float density;

        private void OnGUI() 
        {
            int labelWidth = 120;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Main Collection", GUILayout.MaxWidth(labelWidth));
            collection = EditorGUILayout.ObjectField(
                collection, typeof(Collection.Collection), true
                ) as Collection.Collection;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Sequence", GUILayout.MaxWidth(labelWidth));
            sequenceCollectionSO = EditorGUILayout.ObjectField(
                sequenceCollectionSO,
                typeof(SequencesCollection), true) as SequencesCollection;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Root", GUILayout.MaxWidth(labelWidth));
            root = EditorGUILayout.ObjectField(
                root,
                typeof(Transform), true) as Transform;
            EditorGUILayout.EndHorizontal();

            
            if (sequenceCollectionSO != null)
            {
                EditorGUILayout.BeginHorizontal();
                System.Text.StringBuilder st = new System.Text.StringBuilder();
                st.Append("El: '");
                st.Append(sequenceCollectionSO.Sequences[index].Name);
                st.Append(" '");
                EditorGUILayout.LabelField(st.ToString(), GUILayout.MaxWidth(labelWidth));
                if (sequenceCollectionSO.Sequences.Length >= 1)
                {
                    index = EditorGUILayout.IntSlider(index, 0, sequenceCollectionSO.Sequences.Length - 1);
                }
                if (index >= sequenceCollectionSO.Sequences.Length)
                {
                    index = sequenceCollectionSO.Sequences.Length - 1;
                }
                EditorGUILayout.EndHorizontal();
            }
            if (root!=null && collection != null && sequenceCollectionSO != null)
            {
                density = 
                    EditorGUILayout.Slider(density, 1f, 8);
                if (GUILayout.Button("Load"))
                {
                    Clear(root);
                    Sequence seq = sequenceCollectionSO.Sequences[index];
                    if (density != 1f)
                    {
                        seq = seq.Scaled(density);
                    }
                    seq.Scaled(5);
                    Debug.LogFormat("Load {0}", seq.Name);
                    for (int i = 0; i < seq.Elements.Length; i++)
                    {
                        Element el = seq.Elements[i];
                        WayElementBehaviour wayChunkBehaviour = collection.Get(
                            el.Type, el.Flipped);
                        WayChunkBehaviour instantiated = PrefabUtility.InstantiatePrefab(wayChunkBehaviour, root) as WayChunkBehaviour;
                        if (el.Flipped)
                        {
                            ObstacleWayChunkBehaviour flippable = instantiated as ObstacleWayChunkBehaviour;
                            if (flippable != null)
                                flippable.RotateFlipTransform();
                        }
                    }
                    Align(root);
                }
            }
            

            if (sequenceCollectionSO != null && root != null)
            {
                if (GUILayout.Button("Align"))
                {
                    Align(root);
                }

                if (GUILayout.Button("Save"))
                {
                    List<Element> lst = new List<Element>();
                    for (int i = 0; i < root.childCount; i++)
                    {
                        WayChunkBehaviour wb = root.GetChild(i).GetComponent<WayChunkBehaviour>();
                        Element el = new Element();
                        el.Type = wb.SequenceChunkElement.Type;
                        el.Flipped = wb.SequenceChunkElement.Flipped;
                        lst.Add(el);
                    }
                    sequenceCollectionSO.AssignSequence(lst.ToArray(), index);
                    EditorUtility.SetDirty(sequenceCollectionSO);
                    AssetDatabase.SaveAssets();
                                
                    Clear(root);
                }
            }
        }

        private void Align(Transform root)
        {
            WayChunkBehaviour previous = null;

            for (int i = 0; i < root.childCount; i++)
            {
                Transform child = root.GetChild(i);
                WayChunkBehaviour wayChunkBehaviour = child.GetComponent<WayChunkBehaviour>();
                WayChunkBehaviour current = wayChunkBehaviour;
                if (i == 0)
                {
                    current.transform.localPosition = Vector3.zero;
                    current.transform.localRotation = Quaternion.identity;
                }
                else
                {
                    current.transform.position = previous.MainDockGlobal.Position;
                    current.transform.rotation = previous.MainDockGlobal.Rotation;
                }
                previous = current;
            }
        }

        private void Clear(Transform root)
        {
            if (root.childCount > 0)
            {
                GameObject.DestroyImmediate(root.GetChild(0).gameObject);
                Clear(root);
            }
            else
                return;
        }
    }
}
*/
