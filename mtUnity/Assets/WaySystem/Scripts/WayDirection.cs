using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

namespace WaySystem
{
    public enum WayDirection
    {
        Right,
        Up,
        UpRight,
        DownRight,
        Down,
        DownLeft,
        Left,
        UpLeft,
        Undefined
    }


    public static class WayDirectionUtilities
    {
        public static readonly float[] ANGLES = new float[]
        {
            0,    //Right
            -90f, //Up
            -45f, //UpRight
            45f,  //DownRight
            90f,  //Down
            135,  //DownLeft
            -180, //Left
            -135, //Upleft
            0f    //Undefined
        };

        public static readonly Vector3[] V3 = new Vector3[]
        {
            Vector3.forward, //Right
            Vector3.up, //Up 
            new Vector3(0, 1, 1).normalized, //UpRight
            new Vector3(0, -1, 1).normalized, //DownRight
            Vector3.down, //Down
            new Vector3(0, -1, -1).normalized, //DownLeft
            Vector3.back, //Left
            new Vector3(0, 1, -1).normalized, //UpLeft
            Vector3.zero //undefinded
        };

        public static readonly float THRESHOLD = 0.1f;

        public static WayDirection Vector3ToDirection(Vector3 vector3)
        {
            Vector3 aligned = VectorToNearestAligned(vector3);

            if (aligned == Vector3.up)
                return WayDirection.Up;
            else if (aligned == Vector3.forward)
                return WayDirection.Right;
            else if (aligned == Vector3.down)
                return WayDirection.Down;
            else if (aligned == Vector3.back)
                return WayDirection.Left;

            else if (aligned == UpRigt)
                return WayDirection.UpRight;
            else if (aligned == DownRight)
                return WayDirection.DownRight;
            else if (aligned == DownLeft)
                return WayDirection.DownLeft;
            else if (aligned == UpLeft)
                return WayDirection.UpLeft;

            return WayDirection.Undefined;
        }


        public static Vector3 DirectionToVector(WayDirection direction)
        {
            return V3[(int)direction];
        }


        public static Vector3 VectorToNearestAligned(Vector3 input)
        {
            float angleMin = float.MaxValue;
            Vector3 result = Vector3.zero;

            for (int i = 0; i < 8; i++)
            {
                Vector3 cur = V3[i];
                float angle = Vector3.Angle(input, cur);
                if (angle <= angleMin)
                {
                    angleMin = angle;
                    result = cur;
                }
            }

            return result;
        }


        public static float DirectionToAngle(WayDirection direction)
        {
            return ANGLES[(int)direction];
        }


        public static WayDirection Add(WayDirection first, WayDirection second)
        {
            float angle = DirectionToAngle(first);
            Matrix4x4 m = Matrix4x4.Rotate(Quaternion.Euler(angle, 0, 0));
            Vector3 resultV3 = m.MultiplyVector(DirectionToVector(second));
            WayDirection result = Vector3ToDirection(resultV3);
            return result;
        }


        #region Presets
        public static Vector3 UpRigt { get { return upRight; } }


        public static Vector3 DownRight { get { return downRight; } }


        public static Vector3 DownLeft { get { return downLeft; } }


        public static Vector3 UpLeft { get { return upLeft; } }


        private static Vector3 upRight = new Vector3(0, 1, 1).normalized;
        private static Vector3 downRight = new Vector3(0, -1, 1).normalized;
        private static Vector3 downLeft = new Vector3(0, -1, -1).normalized;
        private static Vector3 upLeft = new Vector3(0, 1, -1).normalized;
        #endregion
    }
}