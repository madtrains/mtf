using System.Collections.Generic;
using UnityEngine;


namespace WaySystem
{
    [System.Serializable]
    public class WayPoint : Point
    {
        public WayPoint(Vector3 position, Quaternion rotation, float offset) : base(position, rotation) 
        { 
            this.offset = offset;
        }
        public float Offset { get { return offset; } set { offset = value; } }

        [SerializeField] protected float offset;
    }
}