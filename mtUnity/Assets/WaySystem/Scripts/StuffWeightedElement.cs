﻿using System.Collections.Generic;
using UnityEngine;

namespace WaySystem
{

    [System.Serializable]
    public class StuffWeightedElement : WeightedElement
    {
        public StuffBehaviour Stuff { get { return stuff; } }
        [SerializeField] protected StuffBehaviour stuff;
    }
}