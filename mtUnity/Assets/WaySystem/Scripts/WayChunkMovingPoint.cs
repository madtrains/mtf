using System.Collections.Generic;
using UnityEngine;

namespace WaySystem
{
    [System.Serializable]
    public class WayChunkMovingPoint
    {
        public float Offset
        {
            get { return offsetLocal;  }
            set { SetOffsetAndCalculate(value);}
        }

        public float OffsetGlobal
        {
            get { return offsetGlobal; }
        }

        public Point Point { get { return globalPoint; } }
        public WayChunk WayChunk { get { return wayChunk; } }
        public Point TransformToGlobal (Point pt)
        {
            /*
            if (wayChunk.Transform == null)
            {
                Debug.LogErrorFormat("Null Transform. Probably next to: {0}", wayChunk.Previous.Transform.name);
                return null;
            }
            */
            return pt.Transformed(wayChunk.Transform);
        }

        public bool IsEnableForEventTriggering
        {
            get { return isEnableForEventTriggering; }
            set { isEnableForEventTriggering = value; }
        }

        protected bool isEnableForEventTriggering;
        protected float offsetLocal;
        protected float offsetGlobal;
        protected Point globalPoint;
        protected WayChunk wayChunk;

        public void Place(WayChunk wayChunk, float startOffset)
        {
            this.wayChunk = wayChunk;
            this.offsetLocal = startOffset;
            this.offsetGlobal = startOffset;
            Calculate();
        }

        public void Move(float delta)
        { 
            this.offsetLocal += delta;
            this.offsetGlobal += delta;
            Calculate();
        }

        public void SetOffsetAndCalculate(float newOffset)
        {
            this.offsetLocal += newOffset;
            this.offsetGlobal += newOffset;
            Calculate();
        }

        public void PlaceOffsetedFromOther(WayChunkMovingPoint other, float offset)
        {
            this.Place(other.wayChunk, other.offsetLocal + offset);
        }

        private void Calculate()
        {
            //���� ���������� �� ��������� ����
            if (this.offsetLocal > wayChunk.Length)
            {
                float left = this.offsetLocal - wayChunk.Length;
                if (wayChunk.Next != null)
                {
                    wayChunk = wayChunk.Next;
                    //��� ����� ������� ��� ������ �������
                    if (isEnableForEventTriggering)
                    {
                        //Debug.LogFormat("Next: {0}", wayChunk.Transform.name);
                        if (wayChunk.OnEnter != null)
                        {
                            wayChunk.OnEnter();
                        }
                    }
                    Change(wayChunk);
                    this.offsetLocal = left;
                    Calculate();
                    return;
                }
                //�������� � ��������� �����
                else
                {
                    this.offsetLocal = WayChunk.Length;
                    this.globalPoint = TransformToGlobal(WayChunk.LastPoint);
                }
            }
            //���� ����� �����
            if (this.offsetLocal < 0)
            {
                //����� ����� �����
                if (wayChunk.Previous != null)
                {
                    wayChunk = wayChunk.Previous;
                    Change(wayChunk);
                    this.offsetLocal = wayChunk.Length + this.offsetLocal;
                    Calculate();
                    return;
                }
                //�������� � ������ �����
                else
                {
                    this.offsetLocal = 0f;
                    this.globalPoint = TransformToGlobal(wayChunk.FirstPoint);
                }
            }
            //��������� ������� �������
            Point localPointOnChunk = wayChunk.GetPointByOffset(this.offsetLocal);
            this.globalPoint = TransformToGlobal(localPointOnChunk);
        }

        protected virtual void Change(WayChunk wayChunk)
        {

        }
    }
}