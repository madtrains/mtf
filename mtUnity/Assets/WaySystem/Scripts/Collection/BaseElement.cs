﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace WaySystem.Collection
{
    /// <summary>
    /// Контейнер хранения элементов в коллекции
    /// </summary>
    [System.Serializable]
    public class BaseElement
    {
        public virtual WayElementBehaviour Get()
        {
            return Get(false);
        }

        public virtual WayElementBehaviour Get(bool flipped)
        {
            return null;
        }
    }


    [System.Serializable]
    public class ElementSingle : BaseElement
    {
        public WayElementBehaviour Element { get { return Get(); } }
        public override WayElementBehaviour Get(bool flipped)
        {
            return main;
        }

        [SerializeField] protected WayElementBehaviour main;
    }


    [System.Serializable]
    public class ElementPair : ElementSingle
    {
        public WayElementBehaviour Flipped { get { return Get(true); } }
        public override WayElementBehaviour Get(bool flipped)
        {
            if (flipped)
                return this.flipped;
            else
                return main;
        }

        [SerializeField] protected WayElementBehaviour flipped;
    }


    [System.Serializable]
    public class ObstacleList : BaseElement
    {
        public ObstacleWayChunkBehaviour Obstacle { get { return obstacle; } }
        [SerializeField] private ObstacleWayChunkBehaviour obstacle;

        
        public GameObject VisualRandom
        {
            get
            {
                return visuals[Random.Range(0, visuals.Length)];
            }
        }

        [SerializeField] private GameObject[] visuals;
   
    }


    [System.Serializable]
    public class WeightedWayElementBehaviour : WeightedElement
    {
        public WayElementBehaviour WayElementBehaviour { get { return wayChunkBehaviour; } }
        [SerializeField] protected WayElementBehaviour wayChunkBehaviour;
    }

    
}