﻿using MTCore;
using System.Collections.Generic;
using TutorialFlipper;
using UnityEngine;
using WaySystem.Additions;
using WaySystem.Additions.Switch;

namespace WaySystem.Collection
{
    [CreateAssetMenu(menuName = "WaySystemCollection")]
    [System.Serializable]
    public class Collection : ScriptableObject
    {
        public Bounds sceneUp;
        public Bounds sceneDown;
        public Bounds sceneLeft;
        public Bounds sceneRight;

        public ElementSingle enter;

        public BricksCollection bricks;
        public SpecialBricks specialBricks;
        public ForksDoubleStraight forksDoubleStraight;
        public ElementSingle exit;
        public ElementPair loopUp;
        public ObstacleList obstaclesElements;
        public ObstacleList obstaclesLongElements;
        public ElementSingle deadEnd;
        public Embankments embankments;
        public MinMax changeAnlgesMinimumValues;
        public MinMax platformLineAnglesRange;
        public Sequences.SequencesCollection obstaclesSequenceCollection;
        public StuffWeightedElement[] stuffElementsArray;
        public StuffWeightedElement[] startStuffElementsArray;
        public PickUps pickUps;
        public BackGround.BackGround[] backGrounds;
        public Triggers.Semaphore semaphore;
        public CargoSection cargosSection;
        public Switch switcher;
        public SwitchSources switchSources;
        public Helpers helpers;
        public Triggers.TrainTutorialTrigger trainTutorialTrigger;
        public ConstructionChunk constructionChunk;
        public Transform sfdPlatz;
        public PlatformExit platformExit;
        public Albatros albatros;
        public ElPole elPole;

        public WayChunkBehaviour Construct(
            ConstructionDirection direction,
            ChunkSides sides,
            WayChunkFlipType flipType, 
            WayChunkFlipDirections wayChunkFlipDirections, StoffActionType wayChunkActionType)
        {
            //container
            Transform container = new GameObject().transform;
            Transform flipTransform =
                new GameObject("flip").transform;
            flipTransform.SetParent(container, false);

            Transform end =
                ConstructInstantiate(direction, sides, flipTransform);

            List<Point> points = new List<Point>()
            {
                new Point(Vector3.zero, Quaternion.identity),
                new Point(end)
            };

            WayChunk wayChunk =
                new WayChunk(
                    container.transform,
                    flipTransform, flipType, 
                    wayChunkFlipDirections, wayChunkActionType,
                    points);

            WayChunkBehaviour wcb =
                container.gameObject.AddComponent<WayChunkBehaviour>();
            wcb.WayChunk = wayChunk;

            return wcb;
        }

        private Transform ConstructInstantiate(
            ConstructionDirection direction,
            ChunkSides sides, 
            Transform target)
        {
            ChunkContent content = constructionChunk.GetChunkContent(direction);
            //clone
            switch (sides)
            {
                case ChunkSides.RailsUp:
                {
                    Instantiate<Transform>(content.rails.up, target);
                    Instantiate<Transform>(content.grass.down, target);
                    Instantiate<Transform>(content.tie, target);
                    break;
                }
                case ChunkSides.RailsDown:
                {
                    Instantiate<Transform>(content.grass.up, target);
                    Instantiate<Transform>(content.rails.down, target);
                    Instantiate<Transform>(content.tie, target);
                    break;
                }
                default:
                {
                    Instantiate<Transform>(content.rails.up, target);
                    Instantiate<Transform>(content.rails.down, target);
                    Instantiate<Transform>(content.tie, target);
                    break;
                }
            }
            return content.end;
        }
    }


    [System.Serializable]
    public class CargoPack
    {
        [SerializeField] public Cargos.CargoBunch bunch;
    }
    

    [System.Serializable]
    public class BricksCollection
    {
        public ConstructionBricks Get(ConstructionDirection direction)
        {
            switch(direction)
            {
                case ConstructionDirection.Up:return up;
                case ConstructionDirection.Down: return down;
                case ConstructionDirection.Left:return left;
                case ConstructionDirection.Right:return right;

                default:
                    return flat;
            }
        }

        [SerializeField] ConstructionBricks flat;
        [SerializeField] ConstructionBricks up;
        [SerializeField] ConstructionBricks down;
        [SerializeField] ConstructionBricks left;
        [SerializeField] ConstructionBricks right;

    }


    [System.Serializable]
    public class SpecialBricks
    {
        public WayElementBehaviour NoTie { get { return noTie; } }
        [SerializeField] WayElementBehaviour noTie;

        public Transform FlatUpNoTie { get { return flatUpNoTie; } }
        [SerializeField] Transform flatUpNoTie;

        public Transform Tie { get { return tie; } }
        [SerializeField] Transform tie;
    }


    [System.Serializable]
    public class ConstructionBricks
    {
        public WayElementBehaviour GetConstructionBrick(ChunkSides sides)
        {
            switch (sides)
            {
                case ChunkSides.Both:
                    return both;
                case ChunkSides.RailsUp:
                    return railsUp;
                case ChunkSides.RailsDown:
                    return railsDown;

                default: return both;
            }
        }

        [SerializeField] protected WayElementBehaviour both;
        [SerializeField] protected WayElementBehaviour railsUp;
        [SerializeField] protected WayElementBehaviour railsDown;
    }

    [System.Serializable]
    public class ForksDoubleStraight
    {
        public ForkPack Get (bool leftSide=false)
        {
            if (leftSide)
                return left;
            return right;
        }
        [SerializeField] ForkPack left;
        [SerializeField] ForkPack right;
    }


    [System.Serializable]
    public class ForkPack
    {
        public ForkWayChunkBehaviour Fork { get { return fork; } }
        public ConvergenceWayChunkBehaviour Convergence { get { return convergence; } }

        [SerializeField] ForkWayChunkBehaviour fork;
        [SerializeField] ConvergenceWayChunkBehaviour convergence;
    }

    [System.Serializable]
    public class CargoSection
    {
        public Platform Platform { get { return platform; } }
        [SerializeField] private Platform platform;

        public PropStation PropStation { get { return propStation; } }
        [SerializeField] private PropStation propStation;

        public Cargos.CargoGenerators Mail { get { return mail; } }
        [SerializeField] private Cargos.CargoGenerators mail;

        public CargoType Passengers { get { return passengers; } }
        [SerializeField] private CargoType passengers;

        public CargoType Wood { get { return wood; } }
        [SerializeField] private CargoType wood;

        public CargoType Freight { get { return freight; } }
        [SerializeField] private CargoType freight;





    }

    [System.Serializable]
    public class Passengers
    {
        public WaySystem.Cargos.CargoPassengers CargoPassengers { get { return passengers; } }
        [SerializeField] private WaySystem.Cargos.CargoPassengers passengers;
    }

    [System.Serializable]
    public class CargoType
    {
        public WaySystem.Cargos.Cargo Get(TripSize tripSize)
        {
            switch(tripSize) 
            {
                case (TripSize.S):
                    return small.Random;
                case (TripSize.M):
                    return medium.Random;
                case (TripSize.L):
                    return large.Random;
                default: 
                    return medium.Random;
            }
        }

        [SerializeField] CargoSize small;
        [SerializeField] CargoSize medium;
        [SerializeField] CargoSize large;
    }

    [System.Serializable]
    public class CargoSize
    {
        public WaySystem.Cargos.Cargo Random
        { 
            get {
                return cargos[UnityEngine.Random.Range(0, cargos.Length - 1)];
            }
        }
        [SerializeField] private WaySystem.Cargos.Cargo[] cargos;
    }

    [System.Serializable]
    public class PickUps
    {
        public WaySystem.PickUp.PickUp Get(WaySystem.PickUp.Type tp)
        {
            return lst[(int)tp];
        }

        [SerializeField] private WaySystem.PickUp.PickUp[] lst;
    }

    [System.Serializable]
    public class Embankments
    {
        public WayElementBehaviour GetElement(ConstructionDirection direction)
        {
            switch(direction)
            {
                case(ConstructionDirection.Left):
                    return left.Element;
                case (ConstructionDirection.Right):
                    return right.Element;
                default:
                    return line5.Element;
            }
        }

        public ElementSingle start;
        public ElementSingle end;
        public ElementSingle line5;
        public ElementSingle tunnel;
        public ElementSingle left;
        public ElementSingle right;
        public ElementSingle bridgeStart;
        public ElementSingle bridge10;
        public ElementSingle bridgeEnd;
    }
}
