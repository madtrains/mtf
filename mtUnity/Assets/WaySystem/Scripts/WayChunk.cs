using MTRails;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace WaySystem
{
    [System.Serializable]
    public class WayChunk : Way, IWayElement
    {
        #region Getters
        public WayChunkFlipType FlipType { get { return flipType; } set { flipType = value; } }

        public WayChunkFlipDirections FlipDirections { get { return flipDirections; } }
        public StoffActionType ActionType { get { return actionType; } }
        WayChunk IWayElement.LinkDock { get { return this; } }

        public RRCollision LinkedCollision
        {
            get { return linkedCollision; }
            set { AssignCollision(value); }
        }
        public Transform Transform
        {
            get
            {
                return mainTransform;
            }
            set
            {
                mainTransform = value;
            }
        }
        public Transform FlipTransform
        {
            get
            {
                if (flipTransform == null)
                    Debug.LogErrorFormat("Null Flip Transform {0}", Transform.name);
                return flipTransform;
            }
            set { flipTransform = value; }
        }
        public Point Dock
        {
            get
            {
                return LastPoint.Transformed(this.mainTransform);
            }
        }

        public WayChunk Previous { get { return previous; } set { previous = value; } }
        public WayChunk Next { get { return next; } set { next = value; } }

        #endregion
        public UnityAction OnEnter;

        [Header("Main:")]
        [SerializeField] protected WayChunkFlipType flipType;
        [SerializeField] protected Transform mainTransform;
        [SerializeField] protected Transform flipTransform;
        [Header("Special Attributes:")]
        [SerializeField] protected WayChunkFlipDirections flipDirections;
        [SerializeField] protected StoffActionType actionType;

        [Header("Linked Chunks:")]
        [SerializeField] protected WayChunk previous;
        [SerializeField] protected WayChunk next;

        [Header("Aux Links:")]
        [SerializeField] protected RRCollision linkedCollision;

        public WayChunk(
            Transform transform,
            Transform flipTransform,
            WayChunkFlipType flipType,
            WayChunkFlipDirections flipDirections,
            StoffActionType actionType,
            List<Point> points
            ) : base(points)
        {
            this.mainTransform = transform;
            this.flipTransform = flipTransform;
            this.SetAttributes(flipType, flipDirections, actionType);
        }

        public void SetStoffActionType(StoffActionType stoffActionType)
        {
            this.actionType = stoffActionType;
        }

        public void SetOneSideFlip()
        {
            this.flipDirections = WayChunkFlipDirections.HereOnly;
        }

        private void SetAttributes(
            WayChunkFlipType flipType,
            WayChunkFlipDirections flipDirections,
            StoffActionType wayChunkActionType)
        {
            this.flipType = flipType;
            this.flipDirections = flipDirections;
            this.actionType = wayChunkActionType;
        }

        private void AssignCollision(RRCollision collision)
        {
            this.linkedCollision = collision;
        }

        public void AssignNewWay(Way way)
        {
            this.points = way.Points;
            this.length = way.Length;
        }

        public void Link(WayChunk previous)
        {
            this.Previous = previous;
            previous.Next = this;
        }

        void IWayElement.DockTo(Point dock)
        {
            throw new System.NotImplementedException();
        }
    }
}