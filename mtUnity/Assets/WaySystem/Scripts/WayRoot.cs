using MTTown;
using System.Collections.Generic;
using System.Reflection;
using TutorialFlipper;
using UnityEngine;
using static MTUi.FloatingSpriteTimer;

namespace WaySystem
{
    public class WayRoot : UberBehaviour
    {
        //public delegate void ProcessChunk(WayChunk chunk);

        [SerializeField] protected List<WayChunk> wayChunks;

        public WayChunk FirstChunk { get { return firstChunk; } set { firstChunk = value; } }
        [SerializeField] protected WayChunk firstChunk;


        public float TrainStartOffset { get { return trainStartOffset; } set { trainStartOffset = value; } }
        [SerializeField] protected float trainStartOffset;


        public void AddWayChunk(WayChunk chunk)
        {
            if (wayChunks == null)
                wayChunks = new List<WayChunk>();
            wayChunks.Add(chunk);
        }

        public void AddWayChunk(List<WayChunk> chunks)
        {
            if (wayChunks == null)
                wayChunks = new List<WayChunk>();
            wayChunks.AddRange(chunks);
        }


        public virtual void Prepare()
        {

        }


        private void OnDrawGizmosSelected()
        {
            if (wayChunks == null || wayChunks.Count == 0)
                return;
            Gizmos.color = Color.blue;
            foreach (WayChunk wc in wayChunks)
            {
                if (wc.ActionType == StoffActionType.DefaultHorn)
                        Gizmos.color = Color.yellow;
                else if (wc.ActionType == StoffActionType.Nothing)
                {
                    Gizmos.color = Color.red;
                }
                if (wc.FlipType == WayChunkFlipType.AfterPass)
                    Gizmos.DrawCube(wc.FirstPoint.Transformed(wc.Transform).Position, Vector3.one * 0.4f);
                else
                    Gizmos.DrawSphere(wc.FirstPoint.Transformed(wc.Transform).Position, 0.2f);
                
                if (wc.Next != null)
                {
                    int steps = 10;
                    float step = 1f / (float)steps;
                    Vector3 from = wc.Dock.Position;
                    Gizmos.color = Color.blue;
                    for (int i = 1; i < steps; i++)
                    {
                        float t = i * step;
                        Vector3 to = Bezier(from, wc.Next.Dock.Position, t);
                        Gizmos.DrawLine(from, to);
                        from = to;
                    }
                }
            }
        }

        private Vector3 Bezier(Vector3 from, Vector3 to, float t)
        {
            Vector3 middle = Vector3.Lerp(from, to, 0.5f);
            middle += Vector3.up * 0.3f;
            Vector3 result =
                Vector3.Lerp(
                    Vector3.Lerp(from, middle, t),
                    Vector3.Lerp(middle, to, t),
                    t);
            return result;
        }
    }
}