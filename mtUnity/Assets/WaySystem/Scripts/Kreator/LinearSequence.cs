using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using WaySystem.Kreator.PickUpDistribution;

namespace WaySystem.Kreator
{
    public class LinearSequence : IWayElement
    {
        public delegate void ProcessWayChunkBehaviour(WayChunkBehaviour wcb);

        public List<WayChunkBehaviour> Elements { get { return elements; } }
        public List<WayChunk> Chunks { get { return chunks; } }
        public Transform Root { get { return parent; } }

        public WayChunk LinkDock { get { return Last.WayChunk; } }

        public Point Dock { get { return end; } }

        public float SummLength { get { return summLength; } }

        public string Name
        {
            get { return sequenceName; }
            set { sequenceName = value; }
        }

        protected List<WayChunkBehaviour> elements;
        protected List<WayChunk> chunks;
        protected Point end;
        protected KreatorShared shared;
        protected Transform parent;
        protected WayChunk previousWayChunk;
        protected string sequenceName;
        protected float summLength;



        public LinearSequence(
            Point target,
            KreatorShared shared,
            Transform parent)
        {
            elements = new List<WayChunkBehaviour>();
            chunks = new List<WayChunk>();
            end = target.Rounded;
            this.shared = shared;
            this.parent = parent;
            this.sequenceName = "ls";
        }

        public LinearSequence(
            WayChunk target,
            KreatorShared shared,
            Transform parent) : this(target.Dock, shared, parent)
        {
            previousWayChunk = target;
        }

        public WayChunkBehaviour First { get { return elements[0]; } }

        public WayChunkBehaviour Last { get { return elements[LastIndex]; } }
        public int LastIndex { get { return elements.Count - 1; } }

        public WayChunkBehaviour Middle
        {
            get
            {
                int index = Mathf.FloorToInt(elements.Count / 2f);
                return elements[index];
            }
        }

        public void Link(WayChunk previous)
        {
            First.WayChunk.Link(previous);
        }


        public WayChunkBehaviour AddBrick(
            ConstructionDirection direction,
            ChunkSides sides)
        {
            WayElementBehaviour source =
                shared.collection.bricks.Get(direction).GetConstructionBrick(sides);

            WayChunkBehaviour newBrick = GameObject.Instantiate<WayElementBehaviour>(
                source, parent) as WayChunkBehaviour;
            //newBrick.Init();

            StringBuilder sb = new StringBuilder();
            sb.Append("_");
            sb.Append(direction);
            sb.Append("_");
            sb.Append(sides);
            AddElement(newBrick, sb.ToString());
            return newBrick;
        }

        public List<WayChunkBehaviour> AddBricks(ConstructionDirection direction,
            ChunkSides sides,
            int count)
        {
            List<WayChunkBehaviour> result = new List<WayChunkBehaviour>();
            for (int i = 0; i < count; i++)
            {
                WayChunkBehaviour newBrick = AddBrick(direction, sides);
                result.Add(newBrick);
            }
            return result;
        }

        public List<WayChunkBehaviour> AddClearLine(int count)
        {
            int length =
                shared.clearLineLengthByTrainSize;
            return AddBricks(
                ConstructionDirection.Flat,
                ChunkSides.Both,
                length);
        }

        public void Add(LinearSequence other, string name)
        {
            other.ProcessWayChunkBehaviours((newElement) =>
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(this.sequenceName);
                sb.Append("_");
                sb.Append(elements.Count);
                sb.Append("_");
                sb.Append(name);

                newElement.gameObject.name = sb.ToString();
                newElement.transform.SetParent(parent, false);

                this.elements.Add(newElement);
                this.chunks.Add(newElement.WayChunk);
                summLength += newElement.WayChunk.Length;

                if (elements.Count > 1)
                {
                    WayChunkBehaviour previous = elements[elements.Count - 2];
                    WayChunkBehaviour next = elements[elements.Count - 1];
                    next.Link(previous.WayChunk);
                }
                //��� ������ ���� � ���� ���������� ��� ����� � ���
                else if (previousWayChunk != null)
                {
                    newElement.Link(previousWayChunk);
                }
                
            });
            end = elements.Last().WayChunk.Dock;
        }    

        public WayChunkBehaviour FillForkLine(float distance)
        {
            Transform clone =
                GameObject.Instantiate<Transform>(shared.collection.specialBricks.FlatUpNoTie);
            Transform scale = clone.GetChild(0);
            scale.localScale = new Vector3(1f, 1f, distance);

            List<Point> points = new List<Point>()
            {
                new Point(Vector3.zero, Quaternion.identity),
                new Point(Vector3.forward * distance, Quaternion.identity)
            };

            WayChunk wayChunk =
                new WayChunk(
                    clone, clone, 
                    WayChunkFlipType.Never, 
                    WayChunkFlipDirections.DefaultBothSides, StoffActionType.DefaultHorn,
                    points);

            WayChunkBehaviour wcb =
                clone.gameObject.AddComponent<WayChunkBehaviour>();
            wcb.WayChunk = wayChunk;
            AddElement(wcb, "line");
            int count = (int)Mathf.Floor(distance);
            float step = distance / Mathf.Floor(distance);
            float currentStep = step / 2f;
            for (int i = 0; i < count; i++)
            {
                Transform tie =
                    GameObject.Instantiate<Transform>(shared.collection.specialBricks.Tie);
                tie.transform.SetParent(clone, false);
                tie.localPosition = Vector3.forward * (currentStep + i * step);
            }
            return wcb;
        }

        public WayChunkBehaviour AddConstruct(
            ConstructionDirection direction,
            ChunkSides sides,
            WayChunkFlipType flipType,
            WayChunkFlipDirections wayChunkFlipDirections,
            StoffActionType wayChunkActionType)
        {
            WayChunkBehaviour wcb =
                shared.collection.Construct(
                    direction, sides, flipType, wayChunkFlipDirections, wayChunkActionType);
            wcb.transform.SetParent(parent, true);
            this.AddElement(wcb, "construct");
            return wcb;
        }

        public OberList<WayChunkBehaviour> AddConstruct(
            ConstructionDirection direction,
            ChunkSides sides,
            WayChunkFlipType flipType, 
            WayChunkFlipDirections wayChunkFlipDirections,
            StoffActionType wayChunkActionType,
            int count)
        {
            OberList<WayChunkBehaviour> result = new OberList<WayChunkBehaviour>();
            for (int i = 0; i < count; i++)
            {
                WayChunkBehaviour current = 
                    AddConstruct(
                        direction, sides, flipType, wayChunkFlipDirections, wayChunkActionType);
                result.Add(current);
            }
            return result;
        }

        public List<WayChunkBehaviour> BendUpOrDown(float targetAngle, ChunkSides chunkSides)
        {
            Point target = elements.Count > 0 ? Last.WayChunk.Dock : previousWayChunk.Dock;
            if (target.AngleX == targetAngle)
                return new List<WayChunkBehaviour>();

            float angle = CalculateAngle(target, targetAngle);
            int chunksAmount = GetChunksAmount(angle);

            ConstructionDirection direction =
                Mathf.Sign(angle) < 0 ? ConstructionDirection.Up : ConstructionDirection.Down;

            List<WayChunkBehaviour> result = AddBricks(direction, chunkSides, chunksAmount);
            return result;
        }

        public WayChunkBehaviour InstantiateAdd(WayElementBehaviour source)
        {
            WayChunkBehaviour newBrick = GameObject.Instantiate<WayElementBehaviour>(
                source, parent) as WayChunkBehaviour;

            StringBuilder sb = new StringBuilder();
            sb.Append("_");
            sb.Append(source.name);
            AddElement(newBrick, sb.ToString());
            return newBrick;
        }

        public void InitBy(AssemblieLine assemblieLine, string name)
        {
            foreach(WayChunkBehaviour wcb in assemblieLine.List)
            {
                this.AddElement(wcb, name);
            }
        }

        public void ProcessWayChunkBehaviours(ProcessWayChunkBehaviour @delegate)
        {
            foreach(WayChunkBehaviour el in elements)
            {
                @delegate(el);
            }
        }

        public float CompareEndDockZ(LinearSequence other)
        {
            return this.Last.WayChunk.Dock.Position.z - other.Last.WayChunk.Dock.Position.z;
        }

        protected void AddElement(WayChunkBehaviour newElement, string name)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(this.sequenceName);
            sb.Append("_");
            sb.Append(elements.Count);
            sb.Append("_");
            sb.Append(name);

            newElement.Init();
            newElement.gameObject.name = sb.ToString();
            newElement.transform.SetParent(parent, false);
            newElement.DockTo(end);

            elements.Add(newElement);
            chunks.Add(newElement.WayChunk);
            end = newElement.Dock;

            if (elements.Count > 1)
            {
                WayChunkBehaviour previous = elements[elements.Count - 2];
                WayChunkBehaviour next = elements[elements.Count - 1];
                next.Link(previous.WayChunk);
            }
            //��� ������ ���� � ���� ���������� ��� ����� � ���
            else if (previousWayChunk != null)
            {
                newElement.Link(previousWayChunk);
            }
            summLength += newElement.WayChunk.Length;
        }

        protected float CalculateAngle(Point point, float targetAngle)
        {
            Vector3 vectorFrom = point.Vector();
            Vector3 vectorTo = Kreator.AngleXToVector(targetAngle);
            float diffAbsAngle = Vector3.Angle(vectorFrom, vectorTo);
            diffAbsAngle = Mathf.Round(diffAbsAngle);
            Vector3 cross = Vector3.Cross(vectorFrom, vectorTo);
            float sign = Mathf.Sign(cross[0]);
            float result = diffAbsAngle * sign;
            return result;
        }

        protected int GetChunksAmount(float diffAngle)
        {
            int numberOfChunks = (int)Mathf.RoundToInt(Mathf.Abs(diffAngle) / 5f);
            return numberOfChunks;
        }

        void IWayElement.DockTo(Point dock)
        {
            throw new System.NotImplementedException();
        }
    }

    public class ObstacleSequence : LinearSequence
    {
        public bool IsFlipped {  get { return isFlipped; } }
        private bool isFlipped;

        public List<PickUpDistribution.PickUpBrickPreDock> PickUpPreDocks { get { return pickUpPreDocks; } }
        private List<PickUpDistribution.PickUpBrickPreDock> pickUpPreDocks;

        public ObstacleSequence(
            WaySystem.Sequences.Sequence preset, WayChunk target, KreatorShared shared, Transform parent, bool flipped)
            : base(target, shared, parent)
        {
            this.isFlipped = flipped;
            pickUpPreDocks = new List<PickUpDistribution.PickUpBrickPreDock>();
            for (int i = 0; i < preset.Elements.Length; i++)
            {
                Sequences.Element el = preset.Elements[i];
                bool elFlipped = el.Flipped;
                if (isFlipped)
                    elFlipped = !elFlipped;
                switch (el.Type)
                {
                    case (WayChunkType.Obstacle):
                    {
                        ObstacleWayChunkBehaviour obstacleWayChunkBehaviour =
                            AddObstacle(
                                shared.collection.obstaclesElements.Obstacle,
                                shared.collection.obstaclesElements.VisualRandom,
                                elFlipped);
                        foreach (Transform brick in obstacleWayChunkBehaviour.Bricks)
                        {
                            this.pickUpPreDocks.Add(
                                new (brick, elFlipped ? 
                                    PossiblePositions.StraightOnly : 
                                    PossiblePositions.FlippedOnly));
                        }
                        break;
                    }
                        
                    case (WayChunkType.ObstacleLong):
                    {
                        ObstacleWayChunkBehaviour obstacleWayChunkBehaviour =
                            AddObstacle(
                                shared.collection.obstaclesLongElements.Obstacle,
                                shared.collection.obstaclesLongElements.VisualRandom,
                                elFlipped);
                        foreach(Transform brick in obstacleWayChunkBehaviour.Bricks)
                        {
                                this.pickUpPreDocks.Add(
                                    new(brick, elFlipped ?
                                        PossiblePositions.StraightOnly :
                                        PossiblePositions.FlippedOnly));
                        }
                        break;
                    }
                        
                    default:
                    {
                        WayChunkBehaviour wcb =
                                this.AddBrick(ConstructionDirection.Flat, ChunkSides.Both);
                        this.pickUpPreDocks.Add(
                                    new(wcb.WayChunk.Transform));
                        break;
                    }
                }
            }
        }

        private ObstacleWayChunkBehaviour AddObstacle(
            ObstacleWayChunkBehaviour obstacleSource,
            GameObject visualSource,
            bool flipped)
        {
            ObstacleWayChunkBehaviour clone =
                GameObject.Instantiate<ObstacleWayChunkBehaviour>(obstacleSource, this.parent);
            //result.Init();

            GameObject visual =
                GameObject.Instantiate<GameObject>(visualSource, clone.ObstacleRoot);

            ObstacleVisual obstacleVisual = 
                visual.GetComponent<ObstacleVisual>();
            obstacleVisual?.Init();

            clone.WayChunk.LinkedCollision.OnTrainCollision += () =>
            {
                clone.WayChunk.FlipType = WayChunkFlipType.Always;
            };

            StringBuilder sb = new StringBuilder();
            sb.Append("el_");
            sb.Append("_");
            sb.Append(obstacleSource.name);
            sb.Append("_");
            sb.Append(visualSource.name);
            AddElement(clone, sb.ToString());
            if (flipped)
                clone.FlipObstacle(flipped);
            return clone;
        }


    }
}