using MTCharacters;
using MTSound;
using MTTown.Characters;
using UnityEngine;
using WaySystem.Kreator.PickUpDistribution;

namespace WaySystem.Kreator
{
    public class KreatorShared
    {
        /// <summary>
        /// ��������� ��������
        /// </summary>
        public Collection.Collection collection;
        public MTCharacters.CharacterFactory.PassengersSO passengersSO;
        public MTParameters.Kreator.Kreator kreatorParameters;
        public MTParameters.Flipper.FlipperParameters flipperParameters;
        public WaySystem.Enter enter;
        public WayRootFlipper wayRoot;
        public Transform mainRoot;
        public Transform additionalRoot;
        public WeightList<WeightedWayDirection> wayDirections;
        public WeightList<Sequences.Sequence> obstaclesWeightedSequence;
        public BackGround.BackGround firstBackground;
        public int clearLineLengthByTrainSize;
        public Vector3 bkgPos;
        //public ExitManager exitManager;
        //public Cargos.Passenger[] passengers;
        public PickUpDistributor pickUpDistributor;
        public StationMasterAlbert stationsmaster;

        /// <summary>
        /// ������� ����
        /// </summary>
        public WayChunk chunk;

        /// <summary>
        /// ������ ������ �������� ����� ������, ������ ������� ����������� (�� ��� ����� ����� ������� WayRoot ��� �������� ����)
        /// </summary>
        public OberList<WayElementBehaviour> mainLineElements;

        public WeightList<StuffWeightedElement> stuffBehs;
        public WeightList<StuffWeightedElement> startStuffBehs;

        public KreatorShared(
            Collection.Collection collection,
            MTCharacters.CharacterFactory.PassengersSO passengersSO,
            WeightList<WeightedWayDirection> weightedWayDirections,
            MTParameters.Kreator.Kreator kreatorParameters,
            MTParameters.Flipper.FlipperParameters flipperParameters,
            int clearLineLengthByTrainSize,
            Transform mainRoot=null, Transform additionalRoot=null)
        {
            this.collection = collection;
            this.passengersSO = passengersSO;
            this.flipperParameters = flipperParameters;
            this.obstaclesWeightedSequence = WeightList<Sequences.Sequence>.Create(
                collection.obstaclesSequenceCollection.Sequences);
            this.kreatorParameters = kreatorParameters;
            this.wayDirections = weightedWayDirections;

            this.mainLineElements = new OberList<WayElementBehaviour>();
            this.clearLineLengthByTrainSize = clearLineLengthByTrainSize;

            stuffBehs = 
                WeightList<StuffWeightedElement>.Create(
                    collection.stuffElementsArray);
            startStuffBehs = 
                WeightList<StuffWeightedElement>.Create(
                    collection.startStuffElementsArray);

            this.mainRoot = mainRoot;
            this.additionalRoot = additionalRoot;
            if (this.mainRoot == null)
                this.mainRoot = new GameObject("mainRoot").transform;
            if (this.additionalRoot == null)
                this.additionalRoot = new GameObject("additionalRoot").transform;
            this.wayRoot = this.mainRoot.gameObject.AddComponent<WayRootFlipper>();
            this.wayRoot.Init();
            this.pickUpDistributor = new PickUpDistributor(this);
        }
    }
    /*
    public class CoinLine
    {
        public CoinLine(KreatorShared kreatorShared)
        {
            this.kShared = kreatorShared;
            this.elements = new List<WayChunkBehaviour>();
        }
        public List<WayChunkBehaviour> elements;
        private KreatorShared kShared;

        public void Add(WayChunkBehaviour chunk)
        {
            elements.Add(chunk);
        }

        public void Add(List<WayChunkBehaviour> elements)
        {
            this.elements.AddRange(elements);
        }

        public void SetCoins(KreatorShared kShared, int trimStart, int trimEnd, float percentage)
        {
            CoinSequence cs = 
                new CoinSequence(kShared, elements, trimStart, trimEnd, percentage);
        }
    }
    */
}
