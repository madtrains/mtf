using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace WaySystem.Kreator
{
    public class Block
    {
        public WayChunk FirstWayChunk { get { return firstChunk; } }
        public Transform Root { get { return root; } }

        public BlockManager BlockManager { get { return blockManager; } set { blockManager = value; } } 

        [SerializeField] private List<SubBlock> subBlocks;
        [SerializeField] private Transform root;
        [SerializeField] private WayChunk firstChunk;
        [SerializeField] private BlockManager blockManager;

        public Block(int count, Transform parent)
        {
            //c����� ������
            this.subBlocks = new List<SubBlock>();
            StringBuilder sb = new StringBuilder();
            sb.Append("Block_");
            sb.Append(count);
            root = new GameObject(sb.ToString()).transform;
            root.SetParent(parent, true);
        }


        public void Add(SubBlock subBlock)
        {
            this.subBlocks.Add(subBlock);
        }

        public void AddSubBlockAtRandomPlace(SubBlock subBlock)
        {
            int index = Random.Range(0, subBlocks.Count);
            subBlocks.Insert(index, subBlock);
        }

        public void AddAtTheBegininig(SubBlock subBlock)
        {
            subBlocks.Insert(0, subBlock);
        }

        public void AddPennult(SubBlock subBlock)
        {
            subBlocks.Insert(subBlocks.Count, subBlock);
        }

        public void AddSecond(SubBlock subBlock)
        {
            subBlocks.Insert(1, subBlock);
        }

        public void SetFirstChunk(WayChunk chunk)
        {
            this.firstChunk = chunk;
        }

        public void Process()
        {
            foreach (SubBlock subBlock in subBlocks)
            {
                subBlock.Process();
            }
        }
    }
}