using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.Kreator.SequenceProcessor
{
    public class SequenceProcessor<T>
    {
        protected List<T> elements;
        public SequenceProcessor(List<T> elements)
        {
            this.elements = elements;
        }

        protected virtual void Process()
        {
            bool isCollecting = false;
            List<T> list = new List<T>();
            for (int i = 0; i < elements.Count; i++)
            {
                T item = elements[i];
                if (!isCollecting)
                {
                    if (CheckForStart(item))
                    {
                        isCollecting = true;
                        list = new List<T>() { item };
                    }
                }
                else
                {
                    list.Add(item);
                    if (CheckForBreak(item) || i == (elements.Count - 1))
                    {
                        isCollecting = false;
                        ProcessSequence(list);
                    }
                }
            }
        }

        protected virtual bool CheckForStart(T element)
        {
            return false;
        }

        protected virtual bool CheckForBreak(T element)
        {
            return false;
        }


        protected virtual void ProcessElement(T element)
        {

        }

        protected virtual void ProcessSequence(List<T> elements)
        {
            foreach (T element in elements)
            {
                ProcessElement(element);
            }
        }
    }
}