using MTDebug;
using System.Collections.Generic;
using UnityEngine;


namespace WaySystem.Kreator.PickUpDistribution
{
    public class PickUpDistributor
    {
        public static float Y_OFFSET = 1.32f;
        public static float Z_OFFSET = 0.5f;
        private KreatorShared kreatorShared;
        private CutsCollection cuts;
        private Line current;
        private bool currentFlipped;
        private delegate void ProcessCut(Cut cut);

        public PickUpDistributor(KreatorShared kreatorShared)
        {
            this.kreatorShared = kreatorShared;
            this.cuts = new CutsCollection();
            currentFlipped = UberBehaviour.RandomBool;
            Start();
        }


        public void Add(List<PickUpBrickPreDock> preDocks)
        {
            foreach (PickUpBrickPreDock pre in preDocks)
            {
                current.Add(pre);
            }
        }

        public void Add(LinearSequence linearSequence)
        {
            foreach (WayChunkBehaviour element in linearSequence.Elements)
            {
                current.Add(element.WayChunk.Transform);
            }
        }

        public void Add(List<WayChunkBehaviour> wayChunkBehaviours)
        {
            foreach (WayChunkBehaviour element in wayChunkBehaviours)
            {
                current.Add(element.WayChunk.Transform);
            }
        }

        public void Close()
        {
            //Debug.LogFormat("Count Before Trim: {0}", current.Count);
            int trimStart = kreatorShared.kreatorParameters.PickUpParams.PickUpTrimStart;
            int trimEnd = kreatorShared.kreatorParameters.PickUpParams.PickUpTrimEnd;

            //line is too short
            if (current.Count <= (trimStart + trimEnd))
            {
                Debug.LogFormat(
                "Can not trim line count of: {0}",
                current.Count);
                Start();
                return;
            }

            current.RemoveRange(0, trimStart);

            if (trimEnd > 0)
            {
                current.RemoveRange(current.Count - trimEnd, trimEnd);
            }


            //line is too short
            if (current.Count < 2)
            {
                Start();
                return;
            }

            //������� ����� ����
            Line modified = new Line();
            for (int i = 0; i < current.Count; i += 2)
            {
                modified.Add(current[i]);
            }
            current = modified;



            /*
            Color color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
            foreach (BrickLink l in current)
            {
                DebugManager.CreatePrimitive(
                    color, "",
                    new Point(l.transform.position, Quaternion.identity), 0.7f, 1f);
            }
            */
            LineToCuts();
            Start();
        }

        public void Process()
        {
            //Color color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
            int absCount = this.cuts.TotalCount;
            if (kreatorShared.kreatorParameters.PickUpParams.Percentage < 1f)
            {
                float fCount = 
                    (float)this.cuts.TotalCount *
                    kreatorShared.kreatorParameters.PickUpParams.Percentage;
                int targetCount = Mathf.CeilToInt(fCount);
                this.cuts.Decimation(targetCount);
                /*
                while (this.cuts.TotalCount > targetCount)
                {
                    this.cuts.RandomRemove();
                }
                */
            }
            int totalCount = this.cuts.TotalCount;
            //Debug.LogFormat("Kreator PickUps {0}/{1}", totalCount, absCount);

            if (kreatorShared.kreatorParameters.PickUpParams.Ticket.On)
            {
                this.cuts.RandomRemove((cut) =>
                {
                    int index = Mathf.CeilToInt(((float)cut.Count / 2f));
                    WaySystem.PickUp.PickUp pickUp =
                            GameObject.Instantiate<WaySystem.PickUp.PickUp>(
                                kreatorShared.collection.pickUps.Get(PickUp.Type.Ticket),
                                cut[index].Point.Position,
                                cut[index].Point.Rotation, kreatorShared.additionalRoot);
                });
            }


            if (kreatorShared.kreatorParameters.PickUpParams.SilverCoins > 0)
            {
                for (int i=0; i< kreatorShared.kreatorParameters.PickUpParams.SilverCoins; i++)
                {
                    this.cuts.RandomRemove((cut) =>
                    {
                        int index = Mathf.CeilToInt(((float)cut.Count / 2f));
                        WaySystem.PickUp.PickUp pickUp =
                                GameObject.Instantiate<WaySystem.PickUp.PickUp>(
                                    kreatorShared.collection.pickUps.Get(PickUp.Type.CoinSilver),
                                    cut[index].Point.Position,
                                    cut[index].Point.Rotation, kreatorShared.additionalRoot);
                    });
                }
            }

            ProcessByPercentage(
                kreatorShared.kreatorParameters.PickUpParams.CoinsPercentage,
                totalCount,
                (cut) =>
                {
                    if (cut.Count == 1)
                        return;
                    foreach (PickUpBrickDock el in cut)
                    {
                        WaySystem.PickUp.PickUp pickUp =
                            GameObject.Instantiate<WaySystem.PickUp.PickUp>(
                                kreatorShared.collection.pickUps.Get(PickUp.Type.Coin), 
                                el.Point.Position, 
                                el.Point.Rotation, kreatorShared.additionalRoot);
                    }
                });

            ProcessCuts();
        }

        public void ProcessCuts()
        {
            //����������� �������� ��������� ����� ���� �������� � ���������
            foreach (Cut cut in cuts)
            {
                foreach (PickUpBrickDock el in cut)
                {
                    if (cut.Count == 1)
                        continue;
                    WaySystem.PickUp.PickUp pickUp =
                        GameObject.Instantiate<WaySystem.PickUp.PickUp>(
                            kreatorShared.collection.pickUps.Get(PickUp.Type.CogCoin),
                            el.Point.Position,
                            el.Point.Rotation, kreatorShared.additionalRoot);
                }
            }
        }

        private void ProcessByPercentage(float percentage, int totalCount, ProcessCut processCut)
        {
            float fCount = (float)totalCount * percentage;
            int targetCount = Mathf.CeilToInt(fCount);
            int counter = 0;
            while (counter < targetCount)
            {
                int cutCount = this.cuts.RandomRemove(processCut);
                counter += cutCount;
            }
        }

        private void Start()
        {
            current = new Line();
        }

        private void LineToCuts()
        {
            if (current.Count < 2)
                return;

            int number = length.GetRandomElement().number;
            if (current.Count < number)
            {
                LineToCuts();
                return;
            }

            Cut cut = new Cut();
            for (int i = 0; i < number; i++)
            {
                PickUpBrickPreDock pre = current[i];
                bool fits = false;
                switch(pre.possiblePositions)
                {
                    case (PossiblePositions.Both):
                    {
                        fits = true;
                        break;
                    }

                    case (PossiblePositions.StraightOnly):
                    {
                        fits = currentFlipped != true;
                        break;
                    }

                    case (PossiblePositions.FlippedOnly):
                    {
                        fits = currentFlipped == true;
                        break;
                    }
                }

                if (fits)
                {
                    cut.Add(new PickUpBrickDock(pre, currentFlipped));
                }
                else
                {
                    currentFlipped = !currentFlipped;
                    if (cut.Count >=2) 
                    {
                        cuts.Add(cut);
                        current.RemoveRange(0, cut.Count);
                    }
                    LineToCuts();
                    return;
                }
            }
            cuts.Add(cut);
            current.RemoveRange(0, cut.Count);
            currentFlipped = !currentFlipped;
            LineToCuts();
        }

        private class Cut : List<PickUpBrickDock>
        {
            public Cut()
            {
                
            }

            public new void Add(PickUpBrickDock brickLink)
            {
                base.Add(brickLink);
            }
        }

        private class Line : List<PickUpBrickPreDock>
        {
            public void Add(Transform transform)
            {
                this.Add(new PickUpBrickPreDock(transform));
            }
        }

        private class CutsCollection : List<Cut>
        {
            public int TotalCount { get { return totalCount; } }

            public CutsCollection()
            {
                totalCount = 0;
            }

            private int totalCount = 0;

            public new void Add(Cut cut)
            {
                base.Add(cut);
                totalCount += cut.Count;
            }

            public int RandomRemove(ProcessCut process = null)
            {
                int index = Random.Range(0, Count);
                int atCount = this[index].Count;
                Cut cut = this[index];
                if (process != null)
                    process(cut);
                int result = cut.Count;
                RemoveAt(index);
                totalCount -= atCount;
                return result;
            }


            //������������ �� �������� ��������, ��� ����
            //������ ����� �������  ����� ��� �� �������� 10 � ������
            //� ������� ������� � 2 ������� 100 � 1
            public void Decimation(int targetCount)
            {
                float totalWeight = 0f;
                List<float> weights = new List<float>();
                for (int i = 0; i < this.Count; i++)
                {
                    bool canDivide = ((float)i % 5) == 0;
                    float currentWeight = canDivide ? 10f : 1f;
                    if (this[i].Count == 2)
                        currentWeight = 100f;
                    weights.Add(currentWeight);
                    totalWeight += currentWeight;
                }
                while (totalCount > targetCount) 
                {
                    RemoveByWeight();
                }

                void RemoveByWeight()
                {
                    // ���������� ��������� ����� � ��������� �� 0 �� ������ ����
                    float randomValue = UnityEngine.Random.Range(0, totalWeight);

                    // �������� ������� � ������������ � �����
                    float cumulativeWeight = 0f;
                    for (int i = 0; i < Count; i++)
                    {
                        cumulativeWeight += weights[i];
                        if (randomValue <= cumulativeWeight)
                        {
                            float weight = weights[i];
                            totalWeight -= weight;
                            /*
                            Cut c = this[i];
                            Color color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
                            foreach (var el in c)
                            {
                                DebugManager.CreatePrimitive(
                                        color, "",
                                        el.Point, 0.7f, 1f);
                            }
                            */
                            weights.RemoveAt(i);
                            totalCount -= this[i].Count;
                            RemoveAt(i);
                        }
                    }
                }
            }
        }

        private class WeightedNumber : WeightedElement
        {
            public WeightedNumber(float weight, int number)
            {
                this.weight = weight;
                this.number = number;
            }
            public int number;
        }
                private static WeightList<WeightedNumber> length = 
            new WeightList<WeightedNumber>
        {
            new WeightedNumber(5f, 2),
            new WeightedNumber(40f, 3),
            new WeightedNumber(25f, 4),
            new WeightedNumber(15f, 5),
            new WeightedNumber(10f, 6)
        };
    }

    public enum PossiblePositions { Both, StraightOnly, FlippedOnly }

    public class PickUpBrickPreDock
    {
        public PickUpBrickPreDock(Transform transform) : this(transform, PossiblePositions.Both)
        {
            
        }

        public PickUpBrickPreDock(Transform transform, PossiblePositions possiblePositions)
        {
            this.transform = transform;
            this.possiblePositions = possiblePositions;
        }



        public Transform transform;
        public PossiblePositions possiblePositions;
    }

    public class PickUpBrickDock
    {
        /*
        public Transform Transform { get { return transform; } }
        private Transform transform;
        */
        public Point Point { get { return point; } }
        private Point point;
        

        public PickUpBrickDock(PickUpBrickPreDock preDock, bool flipped) : this(preDock.transform, flipped)
        {
            
        }

        public PickUpBrickDock(Transform transform, bool flipped)
        {

            Vector3 offset = new Vector3(
                0f,
                flipped ? -PickUpDistributor.Y_OFFSET : PickUpDistributor.Y_OFFSET,
                PickUpDistributor.Z_OFFSET);

            Vector3 position = transform.TransformPoint(offset);
            Quaternion rotation = transform.rotation;
            point = new Point(position, rotation);
        }
    }
}
