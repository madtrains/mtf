using MTCharacters;
using MTCharacters.CharacterFactory;
using MTCore;
using MTTown.Characters;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TutorialFlipper;
using UnityEngine;
using WaySystem.Cargos;
using WaySystem.Kreator.SubBlocks;

namespace WaySystem.Kreator
{
    public enum StationType { Double, DoubleV, Station3, Station3Side, Station4 }

    public partial class Kreator
    {
        public static readonly string EDITOR_FULL_LOGL_PATH = "Assets/kreator.log";

        public static Vector3 AngleXToVector(float angle)
        {
            Quaternion rot = Quaternion.Euler(angle, 0, 0);
            Matrix4x4 m = new Matrix4x4();
            m.SetTRS(Vector3.zero, rot, Vector3.one);
            return m.MultiplyVector(Vector3.forward);
        }

        public TutorialManager TutManager { get { return GameStarter.Instance.TutorialManager; } }

        private KreatorShared kShared;
        /// <summary>
        /// �����
        /// </summary>
        private OberList<Block> blocks;

        /// <summary>
        /// �������� �������� �������
        /// </summary>
        /// <param name="collection">������ ��������� ������� ����</param>
        /// <param name="mainRoot">��� ��� ������� ����� ������������� ������� ������</param>
        /// <param name="additionalRoot">��� ��� ������� ����� ������������� ��� �������������� ������� ����</param>
        /// <param name="platformSizes"></param>
        /// <param name="obstaclesCouns"></param>
        /// <param name="directionChangeCount"></param>
        /// <param name="rollerCoasterCount"></param>
        public Kreator(
            Collection.Collection collection,
            PassengersSO passengersSO,
            MTParameters.Kreator.Kreator kreatorParameters,
            MTParameters.Flipper.FlipperParameters flipperParameters,
            int trainLength,
            Transform mainRoot = null, Transform additionalRoot = null)
        {
            ///�����������
            WeightList<WeightedWayDirection> directions =
                CreateDirectionsCollection(kreatorParameters.Directions);

            this.kShared = new KreatorShared(
                collection, passengersSO, directions, kreatorParameters, flipperParameters,
                trainLength + kreatorParameters.EmptyLineChunksNumber,
                mainRoot, additionalRoot);

            ///�����
            this.blocks = new OberList<Block>();
#if (UNITY_EDITOR)
            RemoveTXTFile();
#endif
        }

        public WayRootFlipper CreateFlipper(MTParameters.Flipper.TripContent tripContent,
            List<int> cargos)
        {
            //int[] resourcesIDs = trip.ResourcesIDs.ToArray();
            //int blocksCount = GetPlatformCount(trip);
            //���� � ������ ���� ���������
            ClonePassengers(
                    kShared.kreatorParameters.PassengersLimit,
                    kShared.flipperParameters.PassengersStep,
                    kShared.flipperParameters.PassengerRotateSpeed);

            int blocksCount = tripContent.Stations.Length + 1;

            int propStationBlock = kShared.kreatorParameters.LevelContent.PropStation.BlockNumber(blocksCount);
            int planeForkBlock = kShared.kreatorParameters.LevelContent.PlaneFork.BlockNumber(blocksCount);
            int embankmentDeadEndBlock = kShared.kreatorParameters.LevelContent.EmbankmentDeadEnd.BlockNumber(blocksCount);
            List<int> coinsForksIndexes = kShared.kreatorParameters.LevelContent.CoinsForks.GenerateBlockIndexes(blocksCount);
            List<int> repairForksIndexes = kShared.kreatorParameters.LevelContent.RepairForks.GenerateBlockIndexes(blocksCount);

            float min = kShared.flipperParameters.PassengerMinimalExitPercentage;
            float step = (float)tripContent.Stations.Length;
            step = 1f / (step - 1f);


            for (int i = 0; i < blocksCount; i++)
            {
                Block block = CreateAddBlock();

                for (int j = 0; j < kShared.kreatorParameters.BlockContent.DirectionChangeFlippable; j++)
                    block.AddSubBlockAtRandomPlace(new DirectionChange(block, kShared, true));
                for (int j = 0; j < kShared.kreatorParameters.BlockContent.DirectionChangeDeadly; j++)
                    block.AddSubBlockAtRandomPlace(new DirectionChange(block, kShared, false));
                for (int j = 0; j < kShared.kreatorParameters.BlockContent.ObstaclesRandom; j++)
                    block.AddSubBlockAtRandomPlace(new Obstacles(block, kShared));
                for (int j = 0; j < kShared.kreatorParameters.BlockContent.RollerCoasterRandom; j++)
                    block.AddSubBlockAtRandomPlace(new RollerCoaster(block, kShared));

                for (int j = 0; j < kShared.kreatorParameters.BlockContent.DeadEnd; j++)
                    block.AddSubBlockAtRandomPlace(new DeadEnd(block, kShared));
                for (int j = 0; j < kShared.kreatorParameters.BlockContent.Mail; j++)
                    block.AddSubBlockAtRandomPlace(new Mail(block, kShared));
                for (int j = 0; j < kShared.kreatorParameters.BlockContent.SmallAvoidance; j++)
                    block.AddSubBlockAtRandomPlace(new SmallAvoidance(block, kShared, kShared.kreatorParameters.BlockContent.SmallAvoidanceChunks));

                if (i == propStationBlock)
                    block.AddSubBlockAtRandomPlace(new PropStation(block, kShared));

                if (i == planeForkBlock)
                    block.AddSubBlockAtRandomPlace(new PlaneFork(block, kShared, 12, 6));
                if (i== embankmentDeadEndBlock)
                    block.AddSubBlockAtRandomPlace(new DeadEmbankment(block, kShared));
                if (coinsForksIndexes.Contains(i))
                {
                    block.AddSubBlockAtRandomPlace(new CoinsFork(block, kShared));
                }

                if (repairForksIndexes.Contains(i))
                {
                    block.AddSubBlockAtRandomPlace(new RepairFork(block, kShared));
                }
                /*
                //debug
                {
                    int randomResourceID =
                                    secondaryCargoIDs[UnityEngine.Random.Range(0, secondaryCargoIDs.Length)];
                    StationDouble stationDouble = new StationDouble(
                        block, kShared, TripSize.S,
                        primaryCargoID, randomResourceID,
                        kShared.kreatorParameters.Station, 0);
                    block.Add(stationDouble);
                }
                */
                

                block.AddAtTheBegininig(new BlockEnterLine(block, kShared));
                if (i == 0)
                {
                    //debug
                    for(int j = 0; j < 10; j++)
                    {
                        //block.AddAtTheBegininig(new PlaneFork(block, kShared, 24, 3));
                        //block.AddAtTheBegininig(new CoinsFork(block, kShared, 2));
                    }
                    block.AddAtTheBegininig(new Enter(block, kShared));

                    if (GameStarter.Instance.DebugManager.DebugSettings.isActive && GameStarter.Instance.DebugManager.DebugSettings.DebugChunksAtStart)
                    {
                        //block.AddSecond(new DebugSubBlock(block, kShared));
                        block.AddSecond(new DeadEmbankment(block, kShared));
                    }
                }

                bool lastBlock = i == blocksCount - 1;
                //����������� ����
                if (!lastBlock)
                {
                    TripSize stationSize = tripContent.Stations[i];
                    //0-doble
                    List<StationType> stationVariants = new List<StationType>();
                    //������� ������� ���� ����� ��������� ������ ���� �������� �� ����� ����
                    if (cargos.Count <= 2)
                    {
                        stationVariants.Add(StationType.Double);
                        stationVariants.Add(StationType.DoubleV);
                    }

                    //������� ������� ���� ������
                    stationVariants.Add(StationType.Station3);


                    //������� ������� ���������� ������ ���� �������� ���� �� 2
                    if (cargos.Count >=2)
                    {
                        stationVariants.Add(StationType.Station3Side);
                        stationVariants.Add(StationType.Station4);
                    }
                    StationType stationType =
                        stationVariants[Random.Range(0, stationVariants.Count - 1)];
                    switch (stationType)
                    {
                        case (StationType.Station3):
                        {
                            block.Add(new Station3(
                                block, kShared,
                                stationSize,
                                cargos,
                                kShared.flipperParameters,
                                (i, blocksCount)));
                            break;
                        }
                        case (StationType.Station3Side):
                        {
                            block.Add(new Station3Side(
                                block, kShared,
                                stationSize,
                                cargos,
                                kShared.flipperParameters,
                                (i, blocksCount)));
                            break;
                        }
                        case (StationType.Station4):
                        {
                            block.Add(new Station4(
                                block, kShared,
                                stationSize,
                                cargos,
                                kShared.flipperParameters,
                                (i, blocksCount)));
                            break;
                        }
                        case (StationType.Double):
                        {
                            block.Add(new StationDouble(
                                block, kShared, stationSize,
                                cargos,
                                kShared.flipperParameters,
                                (i, blocksCount)));
                            break;
                        }

                        case (StationType.DoubleV):
                        {
                            block.Add(new StationDoubleV(
                                block, kShared, stationSize,
                                cargos,
                                kShared.flipperParameters,
                                (i, blocksCount)));
                            break;
                        }
                    }
                }
                //��������� ����
                else
                {
                    block.Add(
                        new Exit(block, kShared, kShared.kreatorParameters.Exit)
                        );
                }
                    
                block.Process();
            }
           
            CloseWayRoot();
            ProcessBackGrounds();
            ProcessPickUp();
            return kShared.wayRoot;
        }

        private SubBlock CreateDeadEnd(Block block, KreatorShared kShared)
        {
            float t = UnityEngine.Random.Range(0f, 1f);
            if (t > 0.7f)
            {
                return new DeadEnd(block, kShared);
            }
            else
            {
                return new DeadEmbankment(block, kShared);
            }
        }

        private void CreateStationDouble()
        {

        }

        private Block CreateAddBlock()
        {
            Block block = new Block(blocks.Count, kShared.wayRoot.transform);
            blocks.Add(block);
            return block;
        }

        #region �����
        /// <summary>
        /// ����������� ����� ��������
        /// </summary>
        private void ProcessBackGrounds()
        {
            //������������ ���������
            while (true)
            {
                int index =
                    UberBehaviour.GetArrayRandomIndex<BackGround.BackGround>(
                        kShared.collection.backGrounds);
                if (GameStarter.Instance.BackGroundIndex != index)
                {
                    GameStarter.Instance.BackGroundIndex = index;
                    break;
                }
            }
            BackGround.BackGround bkgRef = kShared.collection.backGrounds[GameStarter.Instance.BackGroundIndex];
            BackGround.BackGround backGround = GameObject.Instantiate<BackGround.BackGround>(bkgRef);
            kShared.firstBackground = backGround;
            backGround.Apply();
            float offset = 200;
            Vector3 bkgPos = kShared.enter.bkgPos + (Vector3.forward * -offset);
            backGround.transform.position = bkgPos;
            while(true)
            {
                bkgPos = bkgPos + Vector3.forward * backGround.Offset;
                GameObject current = backGround.Copy(false);
                current.transform.SetParent(kShared.additionalRoot, true);
                current.transform.position = bkgPos;
                if (bkgPos.z > kShared.bkgPos.z)
                    break;
            }
            GameObject corner = backGround.Copy(true);
            corner.transform.position = new Vector3(bkgPos.x, bkgPos.y, kShared.bkgPos.z);
            backGround.Close(kShared.bkgPos.z);
            backGround.Lights();
            GameStarter.Instance.MainComponents.SoundManager.Music = backGround.Music;
            kShared.wayRoot.AddBackGround(backGround);
        }

        private void ClonePassengers(int passengersCount, float passengersStep, float passengerRotateSpeed)
        {
            Vector3 hiddenCorner = new Vector3(0f, 5100f, 0f);
            Vector3[] karree = Karree.Create(
                hiddenCorner, passengersCount, passengersStep);

            CharacterFactory characterFactory = new CharacterFactory(
                kShared.passengersSO);

            Passenger[] passengersCollection = CreatePassengers(
                characterFactory, passengersCount, hiddenCorner,
                passengerRotateSpeed);

            foreach (Passenger passenger in passengersCollection)
            {
                passenger.Init();
            }

            kShared.wayRoot.AssignPassengerCollection(passengersCollection);
            kShared.stationsmaster = new GameObject("stationsMaster").AddComponent<StationMasterAlbert>();
            CharacterGraphics graphics = characterFactory.CreateMan(
                (c) => { },
                new OverrideName(), //hair color
                new OverrideName("Johan"), //character
                new OverrideName(), //dress color
                new OverrideHeight(188f),
                new OverrideAddition("Cylinder", true), //hat
                new OverrideAddition(), //hairs
                new OverrideAddition(), //facial hairs
                new OverrideAddition("Zigare", true) //smoker
            );
            kShared.stationsmaster.AssignGraphics(graphics);
            kShared.stationsmaster.Init(characterFactory.MainAnimCollection, AnimCollection.Collection.IdlePatient);
            kShared.stationsmaster.AssignBubble(GameStarter.Instance.UIRoot.PostFlipper.Bubble, graphics.Head);
            kShared.stationsmaster.Offset = new Vector2(-10, 6);
            kShared.stationsmaster.Character = GameCharacter.Albert;
        }

        private void ProcessPickUp()
        {
            kShared.pickUpDistributor.Process();
        }

        private void CloseWayRoot()
        {
            kShared.wayRoot.Close();
        }

        private WeightList<WeightedWayDirection> CreateDirectionsCollection(MTParameters.Kreator.DirectionsCollection directions)
        {
            List<WeightedWayDirection> wd = new List<WeightedWayDirection>();
            if (directions.Flat.Active)
                wd.Add(CreateWWDirection(directions.Flat));

            if (directions.Up5.Active)  { wd.Add(CreateWWDirection(directions.Up5));  }
            if (directions.Up10.Active) { wd.Add(CreateWWDirection(directions.Up10)); }
            if (directions.Up15.Active) { wd.Add(CreateWWDirection(directions.Up15)); }
            if (directions.Up20.Active) { wd.Add(CreateWWDirection(directions.Up20)); }
            if (directions.Up25.Active) { wd.Add(CreateWWDirection(directions.Up25)); }
            if (directions.Up30.Active) { wd.Add(CreateWWDirection(directions.Up30)); }
            if (directions.Up35.Active) { wd.Add(CreateWWDirection(directions.Up35)); }
            if (directions.Up40.Active) { wd.Add(CreateWWDirection(directions.Up40)); }
            if (directions.Up45.Active) { wd.Add(CreateWWDirection(directions.Up45)); }
            if (directions.Up50.Active) { wd.Add(CreateWWDirection(directions.Up50)); }
            if (directions.Up55.Active) { wd.Add(CreateWWDirection(directions.Up55)); }
            if (directions.Up60.Active) { wd.Add(CreateWWDirection(directions.Up60)); }
            if (directions.Up65.Active) { wd.Add(CreateWWDirection(directions.Up65)); }
            if (directions.Up70.Active) { wd.Add(CreateWWDirection(directions.Up70)); }
            if (directions.Up75.Active) { wd.Add(CreateWWDirection(directions.Up75)); }
            if (directions.Up80.Active) { wd.Add(CreateWWDirection(directions.Up80)); }
            if (directions.Up85.Active) { wd.Add(CreateWWDirection(directions.Up85)); }
            if (directions.Up90.Active) { wd.Add(CreateWWDirection(directions.Up90)); }

            if (directions.Down5.Active)  { wd.Add(CreateWWDirection(directions.Down5));  }
            if (directions.Down10.Active) { wd.Add(CreateWWDirection(directions.Down10)); }
            if (directions.Down15.Active) { wd.Add(CreateWWDirection(directions.Down15)); }
            if (directions.Down20.Active) { wd.Add(CreateWWDirection(directions.Down20)); }
            if (directions.Down25.Active) { wd.Add(CreateWWDirection(directions.Down25)); }
            if (directions.Down30.Active) { wd.Add(CreateWWDirection(directions.Down30)); }
            if (directions.Down35.Active) { wd.Add(CreateWWDirection(directions.Down35)); }
            if (directions.Down40.Active) { wd.Add(CreateWWDirection(directions.Down40)); }
            if (directions.Down45.Active) { wd.Add(CreateWWDirection(directions.Down45)); }
            if (directions.Down50.Active) { wd.Add(CreateWWDirection(directions.Down50)); }
            if (directions.Down55.Active) { wd.Add(CreateWWDirection(directions.Down55)); }
            if (directions.Down60.Active) { wd.Add(CreateWWDirection(directions.Down60)); }
            if (directions.Down65.Active) { wd.Add(CreateWWDirection(directions.Down65)); }
            if (directions.Down70.Active) { wd.Add(CreateWWDirection(directions.Down70)); }
            if (directions.Down75.Active) { wd.Add(CreateWWDirection(directions.Down75)); }
            if (directions.Down80.Active) { wd.Add(CreateWWDirection(directions.Down80)); }
            if (directions.Down85.Active) { wd.Add(CreateWWDirection(directions.Down85)); }
            if (directions.Down90.Active) { wd.Add(CreateWWDirection(directions.Down90)); }
            return WeightList<WeightedWayDirection>.Create(wd);

            //settings.Directions
        }

        private WeightedWayDirection CreateWWDirection(MTParameters.Kreator.DirectionParameters dParams)
        {
            WeightedWayDirection wwd = new WeightedWayDirection();
            wwd.Name = dParams.GetType().ToString();
            wwd.Angle = dParams.Angle;
            wwd.Weight = dParams.Weight;
            return wwd;
        }
        #endregion

        private Passenger[] CreatePassengers(
            CharacterFactory characterFactory, int count, Vector3 corner,
            float passengerRotateSpeed)
        {
            GameObject passengers = new GameObject();
            passengers.transform.position = corner;
            Passenger[] result = new Passenger[count];
            Vector3[] karree = Karree.Create(corner, count, 1.2f);
            for (int i = 0; i < count; i++)
            {
                CharacterGraphics graphics =
                    characterFactory.CreateRandom(UberBehaviour.RandomBool, (container) => { });

                StringBuilder sb = new StringBuilder("passenger_");
                sb.Append(graphics.name);
                GameObject passengerGO = new GameObject(sb.ToString());
                passengerGO.transform.position = karree[i];
                passengerGO.layer = LayerMask.NameToLayer("Passengers");

                CharacterLogic characterLogic =
                    passengerGO.AddComponent<CharacterLogic>();
                characterLogic.AssignGraphics(graphics);
                characterLogic.Init(characterFactory.MainAnimCollection, AnimCollection.Collection.Idle);

                Passenger passenger =
                    passengerGO.gameObject.AddComponent<WaySystem.Cargos.Passenger>();
                passenger.Set(
                    characterLogic,
                    GameStarter.GameParameters.Cargo.FlipperPassengerMoveSpeed,
                    passengerRotateSpeed);
                result[i] = passenger;
                //additional scale
                passenger.transform.localScale =
                    passenger.transform.localScale *
                        GameStarter.GameParameters.Cargo.FlipperPassengerScale;
                //RigidBody
                Rigidbody rig = passengerGO.AddComponent<Rigidbody>();
                rig.useGravity = false;
                rig.constraints = RigidbodyConstraints.FreezeAll;
                //Collider
                CapsuleCollider col =
                    passengerGO.AddComponent<CapsuleCollider>();
                col.radius = 0.45f;
                col.height = 2f;
                col.center = Vector3.up;
                passengerGO.transform.SetParent(kShared.additionalRoot, true);
            }
            return result;
        }

        #region Log
        public static void Log(int tabs, params object[] args)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (tabs > 0)
            {
                for (int i = 0; i < tabs; i++)
                {
                    sb.Append('\t');
                }
            }
            foreach (object obj in args)
            {
                sb.Append(obj);
            }
            //Debug.LogFormat("KreatoR: {0}", sb.ToString());
#if (UNITY_EDITOR)
            LogToTxt(sb.ToString());
#endif
        }

        private static void LogToTxt(string line)
        {
            StreamWriter writer = new StreamWriter(EDITOR_FULL_LOGL_PATH, true);
            writer.WriteLine(line);
            writer.Close();
        }

        private static void RemoveTXTFile()
        {
            if (File.Exists(EDITOR_FULL_LOGL_PATH))
            {
                File.Delete(EDITOR_FULL_LOGL_PATH);
            }
        }
        #endregion

        #region ����������
        private float GetNewVerticalAngle(Point dock)
        {
            float newAngle = kShared.wayDirections.GetRandomElement().Angle;
            float currentAngle = dock.AngleX;
            float difference = newAngle - currentAngle;

            if (newAngle != currentAngle && Mathf.Abs(difference) >= 15)
            {
                //Debug.LogFormat("New Vertical Angle: {0} from: {1} difference: {2}", newAngle, currentAngle, difference);
                return newAngle;
            }
            else
            {
                return GetNewVerticalAngle(dock);
            }
        }

        private float GetNewVerticalAngleUp(Point dock)
        {
            float newAngle = kShared.wayDirections.GetRandomElement().Angle;
            float currentAngle = dock.AngleX;
            if (newAngle != currentAngle && newAngle < kShared.collection.changeAnlgesMinimumValues.Min)
            {
                return newAngle;
            }
            else
            {
                return GetNewVerticalAngleUp(dock);
            }
        }

        private float GetPlatformAngle(Point dock)
        {
            float newAngle = kShared.wayDirections.GetRandomElement().Angle;
            float currentAngle = dock.AngleX;
            if (newAngle != currentAngle &&
                newAngle >= kShared.collection.platformLineAnglesRange.Min &&
                newAngle <= kShared.collection.platformLineAnglesRange.Max)
            {
                return newAngle;
            }
            else
            {
                return GetPlatformAngle(dock);
            }
        }

        private float GetNewVerticalAngleDown(Point dock)
        {
            float newAngle = kShared.wayDirections.GetRandomElement().Angle;
            float currentAngle = dock.AngleX;
            if (newAngle != currentAngle && newAngle > kShared.collection.changeAnlgesMinimumValues.Max)
            {
                return newAngle;
            }
            else
            {
                return GetNewVerticalAngleDown(dock);
            }
        }
        #endregion
    }
}
