using MTRails;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.GraphicsBuffer;
using static UnityEngine.Networking.UnityWebRequest;

namespace WaySystem.Kreator.SubBlocks
{
    public class PropStation : AvoidableBlock
    {
        public PropStation(Block targetBlock, KreatorShared kreatorShared, int additionalChunks=4)
                : base(targetBlock, kreatorShared, "PropStation")
        {
            this.additionalChunks = additionalChunks;
        }

        private int additionalChunks;
        public override void Process()
        {
            base.Process();
            base.AvoidFloorAndCeilingByHeights(ChunkSides.Both, 8);
            this.baseLine.BendUpOrDown(0, ChunkSides.Both);
            if (this.baseLine.Elements.Count > 0)
                Include(this.baseLine);
            Additions.PropStation propStation =
                GameObject.Instantiate<Additions.PropStation>(kShared.collection.cargosSection.PropStation);
            
            propStation.transform.SetParent(this.root, false);
            propStation.Init();
            LinearSequence line =
                CreateLinearSequence(
                    kShared.chunk,
                    Mathf.CeilToInt(propStation.Length),ChunkSides.Both,
                    ConstructionDirection.Flat, "oneSide");

            propStation.transform.position = line.First.Dock.Position;
            propStation.transform.rotation = line.First.Dock.Rotation;
            line.ProcessWayChunkBehaviours((wc) => { wc.WayChunk.SetOneSideFlip(); });
            line.AddBricks(ConstructionDirection.Flat, ChunkSides.Both, additionalChunks);
            kShared.pickUpDistributor.Add(line);
            kShared.chunk = line.Last.WayChunk;
            Close();
        }
    }
}
