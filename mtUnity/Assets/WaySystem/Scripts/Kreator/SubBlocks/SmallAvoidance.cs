using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.Kreator
{
    public class SmallAvoidance : AvoidableBlock
    {
        public readonly static float WALL_X_MIN_LIMIT = -5f;
        public readonly static float WALL_X_MAX_LIMIT = 10f;
        private enum Direction {  Left, Right, Up, Down };
        private MinMax limits;

        public SmallAvoidance(Block targetBlock, KreatorShared kreatorShared, MinMax limits)
               : base(targetBlock, kreatorShared, "Small Avoidance")
        {
            this.limits = limits;
        }

        public override void Process()
        {
            base.Process();
            AvoidFloorAndCeilingByHeights(ChunkSides.Both, 0);

            Direction randomDirection = (Direction) UnityEngine.Random.Range(0, System.Enum.GetValues(typeof(Direction)).Length);
            int number = limits.RandomInt;

            
            //������ �����, �� ������� ������ � �����, �������������� ����� �������
            if (randomDirection == Direction.Left && kShared.chunk.Dock.Position.x < WALL_X_MIN_LIMIT)
            {
                randomDirection = Direction.Right;
            }

            if (randomDirection == Direction.Right && kShared.chunk.Dock.Position.x > WALL_X_MAX_LIMIT)
            {
                randomDirection = Direction.Left;
            }



            LinearSequence line = CreateLinearSequence(kShared.chunk);
            switch(randomDirection)
            {
                case Direction.Left:
                {
                    line.AddBricks(ConstructionDirection.Left, ChunkSides.Both, number);
                    line.AddBricks(ConstructionDirection.Right, ChunkSides.Both, number);
                    break;
                }
                case Direction.Right:
                {
                    line.AddBricks(ConstructionDirection.Right, ChunkSides.Both, number);
                    line.AddBricks(ConstructionDirection.Left, ChunkSides.Both, number);
                    break;
                }

                case Direction.Up:
                {
                    line.AddBricks(ConstructionDirection.Up, ChunkSides.Both, number);
                    line.AddBricks(ConstructionDirection.Down, ChunkSides.Both, number);
                    break;
                }
                case Direction.Down:
                {
                    line.AddBricks(ConstructionDirection.Down, ChunkSides.Both, number);
                    line.AddBricks(ConstructionDirection.Up, ChunkSides.Both, number);
                    break;
                }
            }
            kShared.pickUpDistributor.Add(line);
            kShared.chunk = line.Last.WayChunk;
            Close();
        }
    }
}