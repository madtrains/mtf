using Triggers;
using TutorialFlipper.Steps;
using UnityEngine;

namespace WaySystem.Kreator
{
    public class PlatformForkBlockTutorial
    {
        private static readonly string PLATFORM_KEY_STRING = "Flipper.Tutorial.Platform";
        public static readonly int SWITCHER_TRIGGER_INDEX = 6;
        public static readonly float FLY_HIGH_SPEED = 40;
        public static readonly float FLY_LOW_SPEED = 18;

        public PlatformForkBlockTutorial(Block targetBlock,
            KreatorShared kreatorShared,
            int breakingDistance, float lowAcceleration)
        { }

        public  void Process()
        {
            /*
            base.Process();
            fork.Switch.VisualSwitchOnly = true;
            TrainTutorialTrigger platformTrigger =
                CreateTutorialTrigger(
                    switchLine.List[SWITCHER_TRIGGER_INDEX].MainDockGlobal);

            Point switcherPoint = new Point(fork.Switch.transform.TransformPoint(
                new Vector3(-1.5f, 0, 0)), fork.Switch.transform.rotation);
            Point semaphorePoint = new Point(fork.Semaphore3.SemaphorePoints[0]);
            Point platform1 = fork.ResourcePlatforms[0].CalculatePlatformFloor();
            Point platform2 = fork.ResourcePlatforms[1].CalculatePlatformFloor();
            Point mailPlatform = fork.MailPlatforms[0].CalculatePlatformFloor();

            //steps
            platformTrigger.AddStep(
                new TrainStop());
            platformTrigger.AddStep(
                new SetFLipperScreenVisibility(false));
            platformTrigger.AddStep(
                new DialogAndArrow(
                    GameStarter.Instance.TranslationManager.GetTableString(PLATFORM_KEY_STRING),
                    false,
                    kShared.helpers,
                    switcherPoint, 8f, 12f));
            platformTrigger.AddStep(
                new CameraFlyTo(
                    semaphorePoint.Position,
                    GameStarter.GameParameters.Cameras[2],
                    FLY_HIGH_SPEED));
            platformTrigger.AddStep(
                new DialogAndArrow(
                    GameStarter.Instance.TranslationManager.GetTableString(PLATFORM_KEY_STRING + ".2"),
                    false,
                    kShared.helpers,
                    semaphorePoint, 6f, 10f));

            platformTrigger.AddStep(
                new CameraFlyTo(
                    platform1.Position,
                    GameStarter.GameParameters.Cameras[2],
                    FLY_LOW_SPEED));
            platformTrigger.AddStep(
                new DialogAndArrow(
                    GameStarter.Instance.TranslationManager.GetTableString(PLATFORM_KEY_STRING + ".3"),
                    false,
                    kShared.helpers,
                    platform1, 2f, 6f));

            platformTrigger.AddStep(
                new CameraFlyTo(
                    platform2.Position,
                    GameStarter.GameParameters.Cameras[2],
                    FLY_LOW_SPEED));
            platformTrigger.AddStep(
                new DialogAndArrow(
                    GameStarter.Instance.TranslationManager.GetTableString(PLATFORM_KEY_STRING + ".4"),
                    false,
                    kShared.helpers,
                    platform2, 2f, 6f));
            platformTrigger.AddStep(
                new CameraFlyTo(
                    mailPlatform.Position,
                    GameStarter.GameParameters.Cameras[2],
                    FLY_LOW_SPEED));
            platformTrigger.AddStep(
                new DialogAndArrow(
                    GameStarter.Instance.TranslationManager.GetTableString(PLATFORM_KEY_STRING + ".5"),
                    false,
                    kShared.helpers,
                    mailPlatform, 1.7f, 4.7f));
            platformTrigger.AddStep(
                new ReturnCamera(FLY_HIGH_SPEED));
            platformTrigger.AddStep(
                new DialogAndArrow(
                    GameStarter.Instance.TranslationManager.GetTableString(PLATFORM_KEY_STRING + ".6"),
                    true,
                    kShared.helpers,
                    switcherPoint, 8f, 12f));
            platformTrigger.AddStep(
                new SetFLipperScreenVisibility(true));
            platformTrigger.AddStep(
                new Delegate(() =>
                {
                    fork.Switch.VisualSwitchOnly = false;
                    fork.Switch.SetAsPredefined(1);
                }));

            var waitForClikStep = new Step();
            fork.Switch.OnSwitch += (value) =>
            {
                waitForClikStep.Done();
                fork.Switch.Disable();
            };
            platformTrigger.AddStep(waitForClikStep);
            platformTrigger.AddStep(
                new TrainGo(
                    Trains.Train.EngineSpeedState.Acceleration, true, true));
            */
        }
    }
}