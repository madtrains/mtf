using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.Kreator
{
    public delegate void EnterCommand();
    public class BlockEnterLine : SubBlock
    {

        public BlockEnterLine(Block targetBlock, KreatorShared kreatorShared)
                : base(targetBlock, kreatorShared, "Enter Line")
        {
            
        }
        

        public override void Process()
        {
            base.Process();
            LinearSequence line = new LinearSequence(kShared.chunk, kShared, root);
            line.AddBrick(ConstructionDirection.Flat, ChunkSides.Both);
            Include(line);
            this.block.SetFirstChunk(line.First.WayChunk);
            this.lines.Add(line);
        }
    }
}