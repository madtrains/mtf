using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.Kreator
{
    public class RollerCoaster : AvoidableBlock
    {
        private int bendCount;
        public RollerCoaster(Block targetBlock, KreatorShared kreatorShared, int bendCount=8)
               : base(targetBlock, kreatorShared, "Roller Coaster")
        {
            this.bendCount = bendCount;
        }

        public override void Process()
        {
            base.Process();
            kShared.pickUpDistributor.Close();
            FlipRandomly();
            base.AvoidFloorAndCeilingByHeights(UnflippableChunkSides, 6);
            
            LinearSequence lineBend = CreateLinearSequence(
                kShared.chunk, bendCount, UnflippableChunkSides, ConstructionDirection.Right, 
                "alignHorizontalLineLine");
            lineBend.AddBricks(ConstructionDirection.Left, UnflippableChunkSides, bendCount);
            stuff.Add(lineBend);
            WayChunkBehaviour loop = GameObject.Instantiate(
                kShared.collection.loopUp.Get(flipped), root) as WayChunkBehaviour;
            loop.DockTo(lineBend.Last.WayChunk.Dock);
            loop.Link(lineBend.Last.WayChunk);
            stuff.Add(loop.WayChunk);

            LinearSequence afterLine = CreateLinearSequence(
                loop.WayChunk, bendCount, UnflippableChunkSides, ConstructionDirection.Flat, "postLine");
            stuff.Add(afterLine);
            afterLine.AddClearLine(kShared.clearLineLengthByTrainSize);

            kShared.chunk = afterLine.Last.WayChunk;
            Close();
        }
    }
}