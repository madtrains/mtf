using MTRails;
using UnityEngine;

namespace WaySystem.Kreator.SubBlocks
{
    public class DeadEnd : RandomFork
    {
        public DeadEnd(Block targetBlock, KreatorShared kreatorShared)
                : base(targetBlock, kreatorShared, 1, 8, "DeadEnd")
        {
            this.bendIsDeadEnd = UberBehaviour.RandomBool;
        }

        private bool bendIsDeadEnd;
        private RRCollision rrCollision;

        protected override void CreateForkSleeves()
        {
            //����� ��� �����
            if (this.bendIsDeadEnd)
            {
                this.bendLine =
                    CreateLinearSequence(fork.Secondary, bendChunksCount, ChunkSides.RailsUp, this.firstDirection, "bendLine");
                WayChunkBehaviour rrWayChunkBehaviour = bendLine.InstantiateAdd(
                    kShared.collection.deadEnd.Element);
                rrCollision = rrWayChunkBehaviour.WayChunk.LinkedCollision;
                this.straightLine = CreateLinearSequence(fork.Primary,
                    4, ChunkSides.RailsUp, ConstructionDirection.Flat, "straightLine");

                //straightLine.AddBrick(ConstructionDirection.Flat, ChunkSides.RailsUp);
                /*
                straightLine.Last.WayChunk.OnEnter += () => { 
                    GameStarter.Instance.Train.Command_FullThrottle(); };
                */
                //straightLine.Add(CreateRandomEmbankment(straightLine.Last.WayChunk), "embankment");
                stuff.Add(straightLine);
                //������ �����
                LinearSequence emptyPostLine =
                    CreateClearLine(straightLine.Last.WayChunk, "emptyLine");
               
                kShared.pickUpDistributor.Add(emptyPostLine);
                kShared.chunk = emptyPostLine.Last.WayChunk;
            }

            //����� �� ������ �����
            else
            {
                this.straightLine = CreateFillLine(fork.Primary, 2f, "straightLine");
                WayChunkBehaviour deadEndChunkBehaviour = straightLine.InstantiateAdd(
                    kShared.collection.deadEnd.Element);
                rrCollision = deadEndChunkBehaviour.WayChunk.LinkedCollision;

                this.bendLine =
                    CreateLinearSequence(fork.Secondary, bendChunksCount, ChunkSides.RailsUp, this.firstDirection, "bendLine");
                bendLine.AddBricks(this.secondDirection, ChunkSides.RailsUp, bendChunksCount + 6);
                stuff.Add(bendLine);
                /*
                var fullThrottleChunk = bendLine.AddBrick(ConstructionDirection.Flat, ChunkSides.RailsUp);
                fullThrottleChunk.WayChunk.OnEnter +=
                    () => { GameStarter.Instance.Train.Command_FullThrottle(); };
                */

                //������ �����
                LinearSequence emptyPostLine =
                    CreateClearLine(bendLine.Last.WayChunk, "emptyLine");
                kShared.pickUpDistributor.Add(emptyPostLine);
                kShared.chunk = emptyPostLine.Last.WayChunk;
            }

            GameObject firstIcon = this.bendIsDeadEnd ?
                kShared.collection.switchSources.arrow :
                kShared.collection.switchSources.cross;

            GameObject secondIcon = this.bendIsDeadEnd ?
                kShared.collection.switchSources.cross :
                kShared.collection.switchSources.arrow;
           
            CreateSwitcher(firstIcon, secondIcon);

            rrCollision.OnTrainCollision += () =>
            {
                switcher.Enable();
            };
            Close();
        }
    }
}