using MTCore;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.Kreator
{
    [System.Serializable]
    public class Station4 : Station3
    {
        public Station4(
            Block targetBlock, KreatorShared kreatorShared,
            TripSize stationSize,
            List<int> cargos,
            MTParameters.Flipper.FlipperParameters flipperParameters,
            (int, int) indexes)
                : base(
                      targetBlock, 
                      kreatorShared, 
                      stationSize, 
                      cargos,
                      flipperParameters,
                      indexes, "Station4")
        { }

        int offsetMiddle = 6;
        int bendVCount = 10;

        protected override void CalculateWallOffset()
        {
            base.CalculateWallOffset();
            if (leftSide)
            {
                this.wallOffset = 4 * StationParameters.PlatformDepth;
            }
            else
            {
                this.wallOffset = 2 * StationParameters.PlatformDepth + 3;
            }
            this.wallOffset += BKG_ADDITIONAL_OFFSET;
        }

        protected override void CreateMainPart()
        {
            ConstructionDirection there = leftSide ? ConstructionDirection.Left : ConstructionDirection.Right;
            ConstructionDirection back = leftSide ? ConstructionDirection.Right : ConstructionDirection.Left;
            ConstructionDirection upDownThere = leftSide ? ConstructionDirection.Up : ConstructionDirection.Down;
            ConstructionDirection upDownBack = leftSide ? ConstructionDirection.Down : ConstructionDirection.Up;

            WayChunk primaryDock = kShared.chunk;
            // ������ ��������
            ForkWayChunkBehaviour fork = CreateFork(leftSide, primaryDock, "fork");
            ConvergenceWayChunkBehaviour convergence = CreateConvergence(leftSide, fork.Primary, "convergence");

            fork.Primary.OnEnter += () => { stationManager.SwitchersPassed(); };
            fork.Secondary.OnEnter += () => { stationManager.SwitchersPassed(); };

            LinearSequence offsetLine =
                CreateStationLine(fork.Primary, ConstructionDirection.Flat, offsetMiddle, "offsetLine");

            ForkWayChunkBehaviour fork2 = CreateFork(leftSide, offsetLine.Last.WayChunk, "fork2");
            ConvergenceWayChunkBehaviour convergence2 = CreateConvergence(leftSide, fork2.Primary, "convergence2");

            LinearSequence secondLineStart = CreateStationLine(
                fork2.Secondary,
                there,
                FORK_BEND_45, "secondLineStart");
            float bendDistance = Mathf.Abs(fork.Primary.Dock.Position.x - secondLineStart.Last.WayChunk.Dock.Position.x);
            float line45FlatDistance = (StationParameters.PlatformDepth  + 5) - (bendDistance * 2f);
            line45FlatDistance = SIN_45_INV * line45FlatDistance;
            int line45Count = Mathf.CeilToInt(line45FlatDistance);

            secondLineStart.Add(CreateStationLine(
                secondLineStart.Last.WayChunk,
                ConstructionDirection.Flat,
                line45Count, ""), "line");
            secondLineStart.Add(CreateStationLine(
                secondLineStart.Last.WayChunk,
                back,
                BEND_45, ""), "back");
            LinearSequence secondLineCenter = CreateStationLine(
                secondLineStart.Last.WayChunk,
                ConstructionDirection.Flat,
                StationParameters.PlatformLineChunksCount, "seconLineCenter");

            //��������� 3
            float platform3WayLength = fork.Primary.Length + offsetLine.SummLength + fork2.Secondary.Length + secondLineStart.SummLength;
            platformCreationInfos.Add(new PlatformCreationInfo(
                fork.Primary, secondLineCenter,
                new Additions.PlatformAdditionalInfo(3, GetSFDTextureLine(1, 3), platform3WayLength)));
 
            LinearSequence secondLineEnd = CreateStationLine(
                secondLineCenter.Last.WayChunk,
                back,
                BEND_45, "secondLineEnd");
            secondLineEnd.Add(CreateStationLine(
                secondLineEnd.Last.WayChunk,
                ConstructionDirection.Flat,
                line45Count, ""), "line");
            secondLineEnd.Add(CreateStationLine(
                secondLineEnd.Last.WayChunk,
                there,
                FORK_BEND_45 - 1, ""), "there");
            PlaceConvergence(convergence2, secondLineEnd.Last.WayChunk.Dock.Position, fork2.Primary.Dock.Rotation);

            LinearSequence firstLineStart = CreateStationLine(
                fork.Secondary,
                there,
                FORK_BEND_45, "firstLineStart");

            firstLineStart.Add(ThereAndBackAgain(
                firstLineStart.Last.WayChunk,
                upDownThere, bendVCount,
                upDownBack, bendVCount, ""), "back");

            float sideOffset = !leftSide ?
                firstLineStart.Last.WayChunk.Dock.Position.x - secondLineCenter.Last.WayChunk.Dock.Position.x :
                secondLineCenter.Last.WayChunk.Dock.Position.x - firstLineStart.Last.WayChunk.Dock.Position.x;
            Debug.LogFormat("sideOffset {0} ", sideOffset);
            int sideOffsetCount = 0;
            if (sideOffset < offsetMiddle)
            {
                sideOffsetCount = Mathf.CeilToInt(SIN_45_INV * (offsetMiddle - sideOffset));
                firstLineStart.Add(CreateStationLine(
                    firstLineStart.Last.WayChunk,
                    ConstructionDirection.Flat,
                    sideOffsetCount, ""), "lineAdd");
            }

            firstLineStart.Add(CreateStationLine(
                    firstLineStart.Last.WayChunk,
                    back,
                    BEND_45, ""), "back");

            LinearSequence firstLineCenter = CreateStationLine(
                firstLineStart.Last.WayChunk,
                ConstructionDirection.Flat,
                StationParameters.PlatformLineChunksCount + offsetMiddle * 2, "firstLineCenter");

            //��������� 2
            float platform2WayLength = fork.Secondary.Length + firstLineStart.SummLength;
            platformCreationInfos.Add(new PlatformCreationInfo(
                fork.Secondary, firstLineCenter,
                new Additions.PlatformAdditionalInfo(2, GetSFDTextureLine(2), platform2WayLength)));


            LinearSequence firstLineEnd = CreateStationLine(
                firstLineCenter.Last.WayChunk,
                back,
                BEND_45, "firstLineEnd");

            firstLineEnd.Add(ThereAndBackAgain(
                firstLineEnd.Last.WayChunk,
                upDownBack, bendVCount,
                upDownThere, bendVCount, ""), "back");

            if (sideOffsetCount > 0)
            {
                firstLineEnd.Add(CreateStationLine(
                    firstLineEnd.Last.WayChunk,
                    ConstructionDirection.Flat,
                    sideOffsetCount, ""), "lineAdd");
            }
            firstLineEnd.Add(CreateStationLine(
                firstLineEnd.Last.WayChunk,
                there,
                FORK_BEND_45 - 1, ""), "there");
            PlaceConvergence(convergence, firstLineEnd.Last.WayChunk.Dock.Position, fork.Primary.Dock.Rotation);
            LinearSequence offsetFillLine = CreateCentralScaleLine(convergence2.Primary, convergence.Primary, "offsetFillLine");
            
            LinearSequence middleLine = CreateStationLine(fork2.Primary, ConstructionDirection.Flat, offsetMiddle, "middleLine");

            ForkWayChunkBehaviour fork3 = CreateFork(!leftSide, middleLine.Last.WayChunk, "fork3");
            ConvergenceWayChunkBehaviour convergence3 = CreateConvergence(!leftSide, fork3.Primary, "convergence3");


            LinearSequence fourthLine = CreateCentralScaleLine(fork3.Primary, convergence2.Primary, "fourthLine");

            LinearSequence thirdLineStart = CreateStationLine(
                fork3.Secondary,
                back,
                FORK_BEND_45, "thirdLineStart");

            thirdLineStart.Add(CreateStationLine(
                thirdLineStart.Last.WayChunk,
                ConstructionDirection.Flat,
                line45Count, ""), "line");

            thirdLineStart.Add(CreateStationLine(
                thirdLineStart.Last.WayChunk,
                there,
                BEND_45, ""), "line");

            LinearSequence thirdLineCenter = CreateStationLine(
                thirdLineStart.Last.WayChunk,
                ConstructionDirection.Flat,
                StationParameters.PlatformLineChunksCount, "thirdLineCenter");

            //��������� 4
            float platform4WayLength = 
                fork.Primary.Length + offsetLine.SummLength + fork2.Primary.Length + middleLine.SummLength + 
                fork3.Secondary.Length + thirdLineStart.SummLength;
            platformCreationInfos.Add(new PlatformCreationInfo(
                fork.Primary, thirdLineCenter,
                new Additions.PlatformAdditionalInfo(4, GetSFDTextureLine(1, 4), platform4WayLength)));
            int thirdLineAddCount = Mathf.CeilToInt(convergence.Primary.Dock.Position.z - thirdLineCenter.Last.WayChunk.Dock.Position.z);
            //Debug.LogWarningFormat("Station4: add {0}-{1}={2}", convergence.Primary.DockGlobal.Position.z, thirdLineCenter.Last.WayChunk.DockGlobal.Position.z, thirdLineAddCount);
            thirdLineAddCount += offsetMiddle;

            LinearSequence thirdLineEnd = 
                CreateStationLine(
                thirdLineCenter.Last.WayChunk,
                ConstructionDirection.Flat,
                thirdLineAddCount, "thirdLineEnd");
            thirdLineEnd.Add(CreateStationLine(
                thirdLineEnd.Last.WayChunk,
                there,
                BEND_45, ""), "back");
            thirdLineEnd.Add(CreateStationLine(
                thirdLineEnd.Last.WayChunk,
                ConstructionDirection.Flat,
                line45Count, ""), "line");

            thirdLineEnd.Add(CreateStationLine(
                thirdLineEnd.Last.WayChunk,
                back,
                FORK_BEND_45 - 1, ""), "back");

            PlaceConvergence(convergence3, thirdLineEnd.Last.WayChunk.Dock.Position, fork3.Primary.Dock.Rotation);
            LinearSequence middleLine2 = CreateFillLine(convergence.Primary, convergence3.Primary, "middleLine2");
            LinearSequence postLine = CreateStationLine(convergence3.Primary, ConstructionDirection.Flat, 3, "postLine");
            this.semaphoreLineTarget = postLine.Last.WayChunk;

            #region �������������
            Point switcherPoint = new Point(switchersLine.First.transform);
            Vector3 switcherOffset = 
                Vector3.forward * StationParameters.SwitcherOffset;
            //������������� 2
            Point switcher2Point = new Point(
                switcherPoint.Position + switcherOffset,
                switcherPoint.Rotation);
            Point switcher3Point = new Point(
                switcher2Point.Position + switcherOffset,
                switcherPoint.Rotation);

            TripleSwitcherManager tripleSwitcherManager = new TripleSwitcherManager(
                kShared.collection.switcher,
                switcherPoint, switcher2Point, switcher3Point,
                kShared.collection.switchSources.numbers[1], kShared.collection.switchSources.numbers[2],
                kShared.collection.switchSources.numbers[1], kShared.collection.switchSources.numbers[3],
                kShared.collection.switchSources.numbers[1], kShared.collection.switchSources.numbers[4],
                StationParameters.SwitchersLine);
            tripleSwitcherManager.OnSwitch = (primary, secondary, tertiary) =>
            {
                this.stationManager.WayNumber = CheckSwitcher(primary, secondary, tertiary);
                switch (this.stationManager.WayNumber)
                {
                    case (2):
                    {
                        LinkChain(primaryDock, fork.Secondary,
                                firstLineStart, firstLineCenter, firstLineEnd,
                                convergence.Secondary, middleLine2, convergence3.Primary, postLine);
                        break;
                    }

                    case (3):
                    {
                        LinkChain(primaryDock, fork.Primary, offsetLine, 
                            fork2.Secondary, secondLineStart, secondLineCenter, secondLineEnd,
                            convergence2.Secondary, offsetFillLine, 
                            convergence.Primary, middleLine2, convergence3.Primary, postLine);
                        break;
                    }

                    case (4):
                    {
                        LinkChain(primaryDock, fork.Primary, offsetLine, fork2.Primary, middleLine, fork3.Secondary,
                            thirdLineStart, thirdLineCenter, thirdLineEnd, convergence3.Secondary, postLine);
                        break;
                    }

                    default: 
                    {
                        LinkChain(primaryDock, fork.Primary, offsetLine, fork2.Primary, middleLine, fork3.Primary, 
                            fourthLine, convergence2.Primary, offsetFillLine, convergence.Primary, middleLine2, 
                            convergence3.Primary, postLine);
                        break;
                    }
                }
            };

            stuff.Add(fork.Primary);
            stuff.Add(offsetLine);
            stuff.Add(fork2.Primary);
            stuff.Add(middleLine);
            stuff.Add(fork3.Primary);
            stuff.Add(fourthLine);
            stuff.Add(convergence2.Primary);
            stuff.Add(offsetFillLine);
            stuff.Add(convergence.Primary);
            stuff.Add(middleLine2);
            stuff.Add(convergence3.Primary);
            stuff.Add(postLine);

            stuffSecondary.ProcessAndReopen(false);
            stuffSecondary.Add(firstLineStart);
            stuffSecondary.Add(firstLineCenter);
            stuffSecondary.Add(firstLineEnd);
            stuffSecondary.ProcessAndReopen(false);
            stuffSecondary.Add(secondLineStart);
            stuffSecondary.Add(secondLineCenter);
            stuffSecondary.Add(secondLineEnd);
            stuffSecondary.ProcessAndReopen(false);
            stuffSecondary.Add(thirdLineStart);
            stuffSecondary.Add(thirdLineCenter);
            stuffSecondary.Add(thirdLineEnd);
            stuffSecondary.ProcessAndReopen(false);

            OnPreClose += () => { tripleSwitcherManager.Randomize(); };

            #endregion
        }

        private int CheckSwitcher(SwitcherManager.Switcher s1, SwitcherManager.Switcher s2, SwitcherManager.Switcher s3)
        {
            if (s1 == SwitcherManager.Switcher.Secondary)
                return 2;
            if (s2 == SwitcherManager.Switcher.Secondary)
                return 3;
            if (s3 == SwitcherManager.Switcher.Secondary)
                return 4;
            return 1;
        }

    }
}