using MTCore;
using System.Collections.Generic;
using UnityEngine;
using WaySystem.Additions;

namespace WaySystem.Kreator
{
    [System.Serializable]
    public class Station3 : StationBase
    {
        public Station3(
            Block targetBlock, KreatorShared kreatorShared,
            TripSize stationSize,
            List<int> cargos,
            MTParameters.Flipper.FlipperParameters flipperParameters,
            (int, int) indexes, string name = "Station3")
                : base(
                      targetBlock,
                      kreatorShared, 
                      stationSize,
                      cargos,
                      flipperParameters,
                      indexes, 3,
                      name)
        {

        }
        

        protected WayChunk semaphoreLineTarget;
        protected LinearSequence semaphoreLine;
        protected LinearSequence switchersLine;

        protected override void CalculateWallOffset()
        {
            base.CalculateWallOffset();
            this.wallOffset = this.StationParameters.PlatformDepth * 3;
            this.wallOffset += BKG_ADDITIONAL_OFFSET;
        }

        public override void Process()
        {
            base.Process();
            ProcessAppendSFDBreakingLine();
            this.switchersLine = ProcessAppendSwitchersLine();

            CreateMainPart();
            //������ ����� ��� ��������
            this.semaphoreLine =
                CreateClearLine(semaphoreLineTarget, "semaphoreLine");
            kShared.pickUpDistributor.Add(semaphoreLine);
            kShared.chunk = semaphoreLine.Last.WayChunk;
            WayChunkBehaviour semaphoreChunk = semaphoreLine.Middle;
            Triggers.Semaphore semaphore =
                GameObject.Instantiate<Triggers.Semaphore>(
                    kShared.collection.semaphore, root);
            semaphore.Init(semaphoreChunk.WayChunk, 0f, semaphoreChunk.transform.position);

            #region ����������� ��������
            foreach (KeyValuePair<int, int> kvp in DistributeResourcesRandomly(cargos))
            {
                //wayIndex = kvp.Key : resourceID = kvp.Value; 
                Platform platform = CreatePlatform(
                        platformCreationInfos[kvp.Key],
                        kvp.Value,
                        stationSize);
            }
            stationManager.SetUnloadLimits(indexes.currentIndex, indexes.totalCount);
            #endregion
            Close();
        }

        protected virtual void CreateMainPart()
        {
            ConstructionDirection there = leftSide ? ConstructionDirection.Left : ConstructionDirection.Right;
            ConstructionDirection back = leftSide ? ConstructionDirection.Right : ConstructionDirection.Left;

            //������ ��������
            ForkWayChunkBehaviour fork = CreateFork(leftSide, kShared.chunk);
            fork.Primary.OnEnter += () => { stationManager.SwitchersPassed(); };
            fork.Secondary.OnEnter += () => { stationManager.SwitchersPassed(); };

            //���������
            ConvergenceWayChunkBehaviour convergence = CreateConvergence(leftSide, fork.Primary);

            //������ ��������
            ForkWayChunkBehaviour fork2 = CreateFork(!leftSide, fork.Primary, "fork2");
            ConvergenceWayChunkBehaviour convergence2 = CreateConvergence(!leftSide, fork2.Primary, "convergence2");

            BowH bendBow =
                new BowH(
                    this, fork.Secondary,
                    StationParameters.PlatformDepth,
                    StationParameters.PlatformLineChunksCount,
                    there, "bendBow_1");
            bendBow.BaseProcess();
            stuffSecondary.ProcessAndReopen(false);
            stuffSecondary.Add(bendBow.Start);
            stuffSecondary.Add(bendBow.Center);
            stuffSecondary.Add(bendBow.End);
            stuffSecondary.ProcessAndReopen(false);

            //��������� ���� 2
            float way2PreviousLength = fork.Secondary.Length + bendBow.Start.SummLength;
            platformCreationInfos.Add((new PlatformCreationInfo(
                fork.Secondary, bendBow.Center,
                new Additions.PlatformAdditionalInfo(2, GetSFDTextureLine(2, true), way2PreviousLength))));

            //������� ������� ��������
            PlaceConvergence(convergence, bendBow.End.Last.Dock.Position, fork.Dock.Rotation);

            LinearSequence centralLine = CreateCentralScaleLine(fork2.Primary, convergence.Primary, "centralLine");
            BaseProcess(centralLine);

            //��������� ���� 1
            float way1PreviousLength = fork.Primary.Length + fork2.Primary.Length;
            platformCreationInfos.Add((new PlatformCreationInfo(
                fork.Primary, centralLine,
                new Additions.PlatformAdditionalInfo(1, GetSFDTextureLine(1, 1), way1PreviousLength))));


            // ����� �� ������ �������� ������� ��� � ��������� 3
            BowH bendBow2 =
                 new BowH(
                     this, fork2.Secondary,
                     this.StationParameters.PlatformDepth,
                     this.StationParameters.PlatformLineChunksCount,
                     back, "bendBow_2");
            bendBow2.BaseProcess();
            stuffSecondary.ProcessAndReopen(false);
            stuffSecondary.Add(bendBow2.Start);
            stuffSecondary.Add(bendBow2.Center);
            stuffSecondary.Add(bendBow2.End);
            stuffSecondary.ProcessAndReopen(false);

            //��������� ���� 3
            float way3PreviousLength = fork.Primary.Length + fork2.Secondary.Length + bendBow2.Start.SummLength;
            platformCreationInfos.Add((new PlatformCreationInfo(
               fork.Primary, bendBow2.Center,
               new Additions.PlatformAdditionalInfo(3, GetSFDTextureLine(1, 3), way3PreviousLength))));
            /*
            LinearSequence convergenceMiddleLine =
                CreateLinearSequence(convergence.Primary, kShared.flipperParameters.StationSFD.PreForkLineChunksCount,
                ChunkSides.RailsUp, ConstructionDirection.Flat, "convergenceMiddleLine");
            stuff.Add(convergenceMiddleLine);
            BaseProcess(convergenceMiddleLine);
            */

            convergence2.DockTo(convergence.Primary.Dock);
            convergence2.Link(convergence.Primary, bendBow2.End.Last.WayChunk);
            stuff.Add(convergence2.Primary);
            LinearSequence postLine = 
                CreateStationLine(convergence2.Primary, ConstructionDirection.Flat, 3, "postLine");
            semaphoreLineTarget = postLine.Last.WayChunk;
            #region �������������
            Point switcherPoint = new Point(switchersLine.First.transform);
            //������������� 2
            Point switcher2Point = new Point(
                switcherPoint.Position + Vector3.forward * StationParameters.SwitcherOffset,
                switcherPoint.Rotation);

            DoubleSwitcherManager doubleSwitcherManager = new DoubleSwitcherManager(
                kShared.collection.switcher,
                switcherPoint, switcher2Point,
                kShared.collection.switchSources.numbers[1],
                kShared.collection.switchSources.numbers[2],
                kShared.collection.switchSources.numbers[1],
                kShared.collection.switchSources.numbers[3],
                StationParameters.SwitchersLine);
            doubleSwitcherManager.OnSwitch = (primary, secondary) =>
            {
                //1-*
                if (primary == SwitcherManager.Switcher.Primary)
                {
                    //1-1
                    if (secondary == SwitcherManager.Switcher.Primary)
                    {
                        LinkChain(
                            switchersLine, fork.Primary,
                            fork2.Primary, centralLine,
                            convergence.Primary, convergence2.Primary,
                            postLine);
                        stationManager.WayNumber = 1;
                    }
                    //1-3
                    else if (secondary == SwitcherManager.Switcher.Secondary)
                    {
                        LinkChain(switchersLine, fork.Primary, 
                            fork2.Secondary, bendBow2.Start, bendBow2.Center, bendBow2.End,
                            convergence2.Secondary, postLine);
                        stationManager.WayNumber = 3;
                    }
                }
                //2-*
                else if (primary == SwitcherManager.Switcher.Secondary)
                {
                    LinkChain(switchersLine, fork.Secondary, bendBow.Start, bendBow.Center, bendBow.End,
                           convergence.Secondary, convergence2.Primary, postLine);
                    stationManager.WayNumber = 2;
                }
            };
            this.OnPreClose += () =>
            {
                doubleSwitcherManager.Randomize();
            };
            #endregion
        }
    }
}