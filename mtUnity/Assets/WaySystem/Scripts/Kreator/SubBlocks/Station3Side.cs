using MTCore;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.Kreator
{
    [System.Serializable]
    public class Station3Side : Station3
    {
        public Station3Side(
            Block targetBlock, KreatorShared kreatorShared,
            TripSize stationSize,
            List<int> cargos,
            MTParameters.Flipper.FlipperParameters flipperParameters,
            (int, int) indexes)
                : base(
                      targetBlock, 
                      kreatorShared, 
                      stationSize, 
                      cargos,
                      flipperParameters,
                      indexes, string.Format("Station3Side"))
        { }


        protected override void CalculateWallOffset()
        {
            base.CalculateWallOffset();
            if (leftSide)
            {
                this.wallOffset = StationParameters.PlatformDepth * 5;
            }
            else
            {
                this.wallOffset = StationParameters.PlatformDepth + 2;
            }
            this.wallOffset += BKG_ADDITIONAL_OFFSET;
        }

        protected override void CreateMainPart()
        {
            //leftSide = true;
            ConstructionDirection there = leftSide ? ConstructionDirection.Left : ConstructionDirection.Right;
            ConstructionDirection back = leftSide ? ConstructionDirection.Right : ConstructionDirection.Left;

            // ������ ��������
            ForkWayChunkBehaviour fork = CreateFork(leftSide, kShared.chunk, "fork");
            ConvergenceWayChunkBehaviour convergence = CreateConvergence(leftSide, fork.Primary, "convergence");

            fork.Primary.OnEnter += () => { stationManager.SwitchersPassed(); };
            fork.Secondary.OnEnter += () => { stationManager.SwitchersPassed(); };

            LinearSequence way2Start =
                BowHStart(fork.Secondary, there, StationParameters.PlatformDepth, "way2Start");


            // ������ ��������
            ForkWayChunkBehaviour fork2 = CreateFork(leftSide, way2Start.Last.WayChunk, "fork2");
            ConvergenceWayChunkBehaviour convergence2 = CreateConvergence(leftSide, fork2.Primary, "convergence2");

            LinearSequence way2CenterOffset =
                CreateStationLine(
                    fork2.Primary,
                    ConstructionDirection.Flat,
                    StationParameters.PlatformDepth + 1,
                    "way2CenterOffset");

            LinearSequence way2Center =
                CreateStationLine(
                    way2CenterOffset.Last.WayChunk,
                    ConstructionDirection.Flat,
                    StationParameters.PlatformLineChunksCount,
                    "way2Center");

            LinearSequence way2End =
                BowHEnd(way2Center.Last.WayChunk, back, StationParameters.PlatformDepth, "way2End");
            way2End.Link(way2Center.Last.WayChunk);
            PlaceConvergence(convergence2, way2End.Last.WayChunk.Dock.Position, fork2.Dock.Rotation);

            //��������� ���� 2
            float platform2PreviousLength = way2Start.SummLength + fork.Primary.Length + way2CenterOffset.SummLength;
            platformCreationInfos.Add(new PlatformCreationInfo(
                way2Start.First.WayChunk, way2Center,
                new Additions.PlatformAdditionalInfo(2, GetSFDTextureLine(2, 2), platform2PreviousLength)));

            LinearSequence way3Start =
                BowHStart(fork2.Secondary, there, StationParameters.PlatformDepth, "way3Start");

            LinearSequence way3Center =
                CreateLinearSequence(
                    way3Start.Last.WayChunk,
                    StationParameters.PlatformLineChunksCount,
                    ChunkSides.RailsUp, 
                    ConstructionDirection.Flat,
                    "way3Center");


            LinearSequence way3End =
                CreateLinearSequence(way3Center.Last.WayChunk, 7, ChunkSides.RailsUp, back, "way3End");

            //��������� ���� 3
            float platform3PreviousLength = way2Start.SummLength + fork.Secondary.Length + way3Start.SummLength;
            platformCreationInfos.Add((new PlatformCreationInfo(
                way2Start.First.WayChunk, way3Center,
                new Additions.PlatformAdditionalInfo(3, GetSFDTextureLine(2, 3), platform3PreviousLength))));

            //������ ����� �����
            WayChunk w3Last = way3End.Last.WayChunk;
            float �athetusLength = Mathf.Abs(w3Last.Dock.Position.x - fork.DockPrimary.Position.x);
            �athetusLength = �athetusLength - Mathf.Abs(convergence.SecondaryOffset.x);
            float angle = Mathf.Abs(w3Last.Dock.Rotation.eulerAngles.y);
            angle = UberBehaviour.TransformToPrettyAngle(angle);
            angle = Mathf.Abs(angle);
            float sin = Mathf.Sin(Mathf.Deg2Rad * angle);
            float scale = 1f / Mathf.Abs(sin);
            LinearSequence obliqueLine = CreateFillLine(w3Last, �athetusLength * scale, "obliqueLine");
            PlaceConvergence(convergence, obliqueLine.Last.WayChunk.Dock.Position, fork.Dock.Rotation);

            LinearSequence middleLine1 = CreateCentralScaleLine(fork.Primary, convergence2.Primary, "middle1");
            //��������� ���� 1
            platformCreationInfos.Add((new PlatformCreationInfo(
                middleLine1.First.WayChunk, middleLine1,
                new Additions.PlatformAdditionalInfo(1, GetSFDTextureLine(1, true), 0))));
   
            LinearSequence middleLine2 = CreateCentralScaleLine(convergence2.Primary, convergence.Primary, "middle2");
            this.semaphoreLineTarget = convergence.Primary;

            #region �������������
            Point switcherPoint = new Point(switchersLine.First.transform);
            //������������� 2
            Point switcher2Point = new Point(
                switcherPoint.Position + Vector3.forward * StationParameters.SwitcherOffset,
                switcherPoint.Rotation);

            DoubleSwitcherManager doubleSwitcherManager = new DoubleSwitcherManager(
                kShared.collection.switcher,
                switcherPoint, switcher2Point,
                kShared.collection.switchSources.numbers[1],
                kShared.collection.switchSources.numbers[2],
                kShared.collection.switchSources.numbers[2],
                kShared.collection.switchSources.numbers[3],
                StationParameters.SwitchersLine);
            doubleSwitcherManager.OnSwitch = (primary, secondary) =>
            {
                //1-*
                if (primary == SwitcherManager.Switcher.Primary)
                {
                    LinkChain(
                        switchersLine, fork.Primary, middleLine1, 
                        convergence2.Primary, middleLine2, convergence.Primary,
                        semaphoreLine);

                    stationManager.WayNumber = 1;
                }
                //2-*
                else if (primary == SwitcherManager.Switcher.Secondary)
                {
                    //2-2
                    if (secondary == SwitcherManager.Switcher.Primary)
                    {
                        LinkChain(switchersLine, fork.Secondary, 
                            way2Start, fork2.Primary, 
                            way2CenterOffset, way2Center, way2End, 
                            convergence2.Secondary, middleLine2, convergence.Primary, semaphoreLine);
                        stationManager.WayNumber = 2;
                    }
                    //2-3
                    else if (secondary == SwitcherManager.Switcher.Secondary)
                    {
                        LinkChain(switchersLine, fork.Secondary,
                            way2Start, fork2.Secondary, 
                            way3Start, way3Center, way3End, obliqueLine, 
                            convergence.Secondary, semaphoreLine);
                        stationManager.WayNumber = 3;
                    }
                }
            };
            #endregion

            stuff.Add(fork.Primary);
            stuff.Add(middleLine1);
            stuff.Add(convergence2.Primary);
            stuff.Add(middleLine2);
            stuff.Add(convergence.Primary);


            stuffSecondary.ProcessAndReopen(false);
            stuffSecondary.Add(way2Start);
            stuffSecondary.Add(way2CenterOffset);
            stuffSecondary.Add(way2Center);
            stuffSecondary.Add(way2End);
            stuffSecondary.ProcessAndReopen(false);

            stuffSecondary.ProcessAndReopen(false);
            stuffSecondary.Add(way3Start);
            stuffSecondary.Add(way3Center);
            stuffSecondary.Add(way3End);
            stuffSecondary.Add(obliqueLine);
            stuffSecondary.ProcessAndReopen(false);

            stuffSecondary.Add(middleLine1);

            OnPreClose += () => { doubleSwitcherManager.Randomize(); };
            
        }
    }
}