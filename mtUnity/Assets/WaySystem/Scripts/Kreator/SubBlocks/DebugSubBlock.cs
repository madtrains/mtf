using UnityEngine;

namespace WaySystem.Kreator
{
    public class DebugSubBlock : SubBlock
    {
        public DebugSubBlock(Block targetBlock, KreatorShared kreatorShared)
               : base(targetBlock, kreatorShared, "DebugSubBlock") 
        {
            
        }


        public override void Process()
        {
            base.Process();
            //testRemove
            var line = CreateLinearSequence(kShared.chunk, 2, ChunkSides.Both);
                
            line.InstantiateAdd(kShared.collection.embankments.start.Element);
            line.InstantiateAdd(kShared.collection.embankments.line5.Element);
            line.InstantiateAdd(kShared.collection.embankments.line5.Element);
            line.InstantiateAdd(kShared.collection.embankments.line5.Element);
            line.InstantiateAdd(kShared.collection.embankments.bridgeStart.Element);
            line.InstantiateAdd(kShared.collection.embankments.bridge10.Element);
            line.InstantiateAdd(kShared.collection.embankments.bridge10.Element);

            var bc = line.InstantiateAdd(kShared.collection.embankments.bridge10.Element).GetComponent<BridgeController>();
            bc.CockTheBridge();
            line.InstantiateAdd(kShared.collection.embankments.bridge10.Element);

            line.InstantiateAdd(kShared.collection.embankments.bridge10.Element);

            bc = line.InstantiateAdd(kShared.collection.embankments.bridge10.Element).GetComponent<BridgeController>();
            bc.CockTheBridge();

            line.InstantiateAdd(kShared.collection.embankments.bridge10.Element);
            line.InstantiateAdd(kShared.collection.embankments.bridge10.Element);
            line.InstantiateAdd(kShared.collection.embankments.line5.Element);
            line.InstantiateAdd(kShared.collection.embankments.line5.Element);
            line.InstantiateAdd(kShared.collection.embankments.line5.Element);
            line.InstantiateAdd(kShared.collection.embankments.end.Element);
            
            kShared.chunk = line.Last.WayChunk;
            Close();
        }
    }
}