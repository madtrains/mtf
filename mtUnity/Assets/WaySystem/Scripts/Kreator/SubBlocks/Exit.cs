using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WaySystem.Additions;
using WaySystem.Additions.Switch;

namespace WaySystem.Kreator
{
    public class Exit : FlatGreenBlock
    {
        public Exit(Block targetBlock, KreatorShared kreatorShared, MTParameters.Kreator.Exit exitParameters)
                : base(targetBlock, kreatorShared, "Exit")
        { 
            this.parameters = exitParameters;
        }

        public ExitManager ExitManager { get { return exitManager; } }

        protected static readonly int EXIT_WALL_OFFSET = 22;
        private MTParameters.Kreator.Exit parameters;
        protected WayChunkBehaviour firstHalf;
        private WaySystem.Exit exit;
        private ExitManager exitManager;

        public override void Process()
        {
            base.Process();
            exitManager = new ExitManager(kShared.wayRoot, kShared.flipperParameters)
            {
                ExitParameters = this.parameters
            };
            this.block.BlockManager = exitManager;
            kShared.wayRoot.Add(exitManager);

            GameStarter.Instance.Train.ProcessExitManager(exitManager);
            kShared.pickUpDistributor.Close();
            LinearSequence clearLine =
                CreateLinearSequence(kShared.chunk, kShared.flipperParameters.ExitConstructor.FreewayCount, ChunkSides.RailsUp);
            stuff.Add(clearLine);
            Include(clearLine);
            WayTeleport teleport =
                clearLine.First.WayChunk.Transform.gameObject.AddComponent<WayTeleport>();
            teleport.Link(
                () => 
                {
                    exitManager.Prepare();
                },
                kShared.chunk);

            Log(0, "Count Wall Offset");
            //������������ �� �������/�����
            int chunksCount =
                WallOffsetCount(kShared.chunk, EXIT_WALL_OFFSET, ONE_STEP_30, MIN_30);

            //���������� ������ ������ ���� ����� ��������� �� ������
            ConstructionDirection thereDirection =
                chunksCount > 0 ?
                    ConstructionDirection.Left : ConstructionDirection.Right;
            ConstructionDirection backAgainDirection =
                thereDirection == ConstructionDirection.Left ?
                    ConstructionDirection.Right : ConstructionDirection.Left;

            //�����
            LinearSequence mainLine =
                CreateLinearSequence(
                    kShared.chunk, ROUND30,
                    ChunkSides.RailsUp, ConstructionDirection.Left, "mainLine");

            int abs = Mathf.Abs(chunksCount);
            if (abs > 0)
                mainLine.AddBricks(ConstructionDirection.Flat, ChunkSides.RailsUp, Mathf.Abs(abs));

            //������������ ������� 
            var backToLine =
                mainLine.AddBricks(ConstructionDirection.Right, ChunkSides.RailsUp, ROUND30);

            var lineFirstChunk = mainLine.AddBrick(ConstructionDirection.Flat, ChunkSides.RailsUp);
            backToLine.First<WayChunkBehaviour>().WayChunk.OnEnter += () => 
            { 
                exitManager.OnBreaking?.Invoke(kShared.flipperParameters.ExitConstructor.PlatformStopPartCount + 9); 
            };

            PlatformExit platformExit = GameObject.Instantiate(
                kShared.collection.platformExit, root) as PlatformExit;
            platformExit.DockTo(lineFirstChunk.WayChunk);
            kShared.stationsmaster.transform.position = platformExit.MainCharacter.transform.position;
            kShared.stationsmaster.transform.rotation = platformExit.MainCharacter.transform.rotation;
            UberBehaviour.Activate(platformExit.Switcher.gameObject, false);

            platformExit.Passengers.AssignCollection(kShared.wayRoot.PassengersCollection);

            exitManager.Sfd = platformExit.SfdRoot;
            exitManager.PlatformExit = platformExit;
            exitManager.StationMaster = kShared.stationsmaster;


            //������ �����, ����� �����, ���� �����
            mainLine.AddBricks(ConstructionDirection.Flat, ChunkSides.RailsUp, kShared.flipperParameters.ExitConstructor.PlatformStopPartCount);
            WayChunk sfdStopChunk = mainLine.Last.WayChunk;
            int stopLength = mainLine.Elements.Count;

            mainLine.AddBricks(
                ConstructionDirection.Flat, 
                ChunkSides.RailsUp, 
                kShared.flipperParameters.ExitConstructor.PlatformWholeLineCount - kShared.flipperParameters.ExitConstructor.PlatformStopPartCount);
            mainLine.Last.NeedStuff = false;
            Include(mainLine);
            stuff.Add(mainLine);

            exit = GameObject.Instantiate(
                kShared.collection.exit.Element, root) as WaySystem.Exit;
            exit.Init();
            exit.DockTo(kShared.chunk.Dock);
            kShared.bkgPos = exit.BkgPos;
            exitManager.LeaveCam = exit.LeaveCam;

            exit.EventChunkContinue.WayChunk.OnEnter += () => {
                exitManager.OnFlipperRelaunch?.Invoke() ;
            };
            exit.EventChunkExit.WayChunk.OnEnter += () => {
                exitManager.OnFlipperExitToTown?.Invoke();
            };
            clearLine.First.WayChunk.OnEnter += () =>
            {
                exitManager.Prepare();
            };

            SwitcherPosition town = new SwitcherPosition(
                kShared.collection.switchSources.town,
                () => {
                    exit.Fork.Secondary.Link(mainLine.Last.WayChunk);
                }, true);

            SwitcherPosition cross = new SwitcherPosition(
                kShared.collection.switchSources.cross,
                () => {
                    
                }, false);

            SwitcherPosition nextRound = new SwitcherPosition(
                kShared.collection.switchSources.arrow,
                () => {
                    exit.Fork.Primary.Link(mainLine.Last.WayChunk);
                }, true);

            platformExit.Switcher.Init(
                new List<SwitcherPosition>(){ town, cross, nextRound }, 3);

            
            stuff.Add(exit.Fork.Primary);
            Close();
        }

        protected override void Close()
        {
            base.Close();
            exit.DestroyBehaviours();
        }
    }
}