using MTRails;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.Kreator.SubBlocks
{
    public class TestSubBlock : SubBlock
    {
        public TestSubBlock(Block targetBlock, KreatorShared kreatorShared, int count)
                : base(targetBlock, kreatorShared, "TestSubBlock") { this.flatLineChunksCount = count; }


        private int flatLineChunksCount;

        public override void Process()
        {
            base.Process();
            LinearSequence line = CreateLinearSequence(kShared.chunk, this.flatLineChunksCount, ChunkSides.RailsUp);
            kShared.chunk = line.Last.WayChunk;
            stuff.Add(line);
            Close();
        }
    }
}