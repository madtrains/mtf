namespace WaySystem.Kreator
{
    public class Obstacles : AvoidableBlock
    {
        public Obstacles(Block targetBlock, KreatorShared kreatorShared)
               : base(targetBlock, kreatorShared, "Obstacles") 
        {
            this.difficulty = 0;
        }

        private int difficulty;

        public override void Process()
        {
            base.Process();
            FlipRandomly();
            ChunkSides sides = ChunkSides.Both;
            bool flippableAvoidance = UberBehaviour.RandomBool;
            if (!flippableAvoidance)
                sides = UnflippableChunkSides;

            Sequences.Sequence preset =
                     kShared.obstaclesWeightedSequence.GetRandomElement();

            //��������� �� ���������
            while (preset.Difficulty > this.difficulty)
            {
                preset =
                     kShared.obstaclesWeightedSequence.GetRandomElement();
            }
      
            Prediction avoidance = this.AvoidFloorAndCeilingByHeights(sides, 0);

            ObstacleSequence obstacleSequence = CreateObstacleSequence(preset, kShared.chunk, flipped);
            kShared.pickUpDistributor.Add(obstacleSequence.PickUpPreDocks);
            Include(obstacleSequence);
            Close();
        }
    }
}