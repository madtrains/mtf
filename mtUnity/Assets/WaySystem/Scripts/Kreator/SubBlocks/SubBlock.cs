using System;
using System.Collections.Generic;
using System.Text;
using Triggers;
using TutorialFlipper;
using UnityEngine;


namespace WaySystem.Kreator
{
    [System.Serializable]
    public class SubBlock
    {
        public static MinMax HEIGHTS
        {
            get
            {
                return new MinMax(-10, 150);
            }
        }

        protected static int count;

        public SubBlock(Block targetBlock, KreatorShared shared, string name)
        {
            this.block = targetBlock;
            this.kShared = shared;
            this.id = count++;
            this.stuff = new Stuff(this);
            this.stuffSecondary = new Stuff(this);
            lines = new List<LinearSequence>();
            toRemove = new List<Component>();
            StringBuilder sb = new StringBuilder();
            this.id = count ++;
            sb.Append(name);
            sb.Append("_");
            sb.Append(this.id);
            this.name = sb.ToString();
            this.root = new GameObject(this.name).transform;
        }
 

        protected Block block;
        /// <summary>
        /// ������ �� ����� ����� ����� ������� � KreatoR��
        /// </summary>
        protected KreatorShared kShared;
        protected bool flipped;
        protected Transform root;
        protected List<LinearSequence> lines;
        protected List<Component> toRemove;
        protected Stuff stuff;
        protected Stuff stuffSecondary;
        protected WayChunk first;
        protected string name;
        protected int id;
        /// <summary>
        /// ���������� �����, ����� ��� �������� ���������
        /// </summary>
        protected Action OnPreClose;


        public virtual void Process()
        {
            this.root.SetParent(block.Root);
            Log(0, "Start");
        }

        protected virtual void Close()
        {
            OnPreClose?.Invoke();
            stuff.ProcessAndReopen();
            stuffSecondary.ProcessAndReopen();
            foreach (LinearSequence line in lines)
            {
                foreach (WayChunkBehaviour wayChunkBehaviour in line.Elements)
                {
                    kShared.wayRoot.AddWayChunk(wayChunkBehaviour.WayChunk);
                    toRemove.Add(wayChunkBehaviour);
                }
            }

            foreach (Component component in toRemove)
            {
                GameObject.Destroy(component);
            }
        }

        public void FlipRandomly()
        {
            float v = UnityEngine.Random.value;
            if (v > 0.5)
            {
                flipped = true;
            }
            else
            {
                flipped = false;
            }
        }

        public ChunkSides UnflippableChunkSides { get { return flipped ? ChunkSides.RailsDown : ChunkSides.RailsUp; } }

        public void LinkChain(params IWayElement[] elements)
        {
            if (elements.Length < 2)
                return;

            for (int i = 1; i < elements.Length; i++)
            {
                IWayElement current = elements[i];
                IWayElement previous = elements[i-1];
                current.Link(previous);
            }
        }

        #region Utilities
        protected virtual float GetRandomAngle()
        {
            float newAngle = kShared.wayDirections.GetRandomElement().Angle;
            return newAngle;
        }

        protected virtual float GetCheckRandomAngle(Point point)
        {
            float newAngle = GetRandomAngle();
            float currentAngle = point.AngleX;
            float difference = newAngle - currentAngle;

            if (newAngle != currentAngle && Mathf.Abs(difference) >= 15)
            {
                return newAngle;
            }
            else
            {
                return GetCheckRandomAngle(point);
            }
        }

        protected float GetRandomLimitedAngle(float min, float max, float current)
        {
            float randomAngle = GetRandomAngle();
            if (randomAngle >= min &&
                randomAngle <= max &&
                randomAngle != current)
            {
                return randomAngle;
            }
            else
                return GetRandomLimitedAngle(min, max, current);
        }

        protected Way WayChunksToWay(List<WayChunk> elements)
        {
            List<Point> points = new List<Point>();
            for (int i = 0; i < elements.Count; i++)
            {
                WayChunk element = elements[i];
                for (int j = 0; j < element.Points.Count; j++)
                {
                    Point point = element.Points[j].Transformed(element.Transform);
                    if (points.Count == 0)
                    {
                        points.Add(point);
                    }
                    else
                    {
                        if (!points[points.Count - 1].IsEqual(point))
                        {
                            points.Add(point);
                        }
                    }
                }
            }

            Way result = new Way(points);
            return result;
        }

        protected TrainTutorialTrigger CreateTutorialTrigger(Point point)
        {
            TrainTutorialTrigger trigger =
                        GameObject.Instantiate<TrainTutorialTrigger>(kShared.collection.trainTutorialTrigger);
            trigger.transform.SetParent(kShared.additionalRoot, false);
            trigger.transform.position = point.Position;
            trigger.transform.rotation = point.Rotation;
            Tutorial tutorial = new Tutorial();
            trigger.AssignTutor(tutorial);
            return trigger;
        }

        protected void Log(int tabs = 0, params object[] args)
        {
            List<object> list = new List<object>
            {
                string.Format("{0}: ", name),
            };
            list.AddRange(args);
            Kreator.Log(1 + tabs, list.ToArray());
        }
        #endregion

        #region Linear Sequence
      
        protected virtual LinearSequence CreateLinearSequence(WayChunk target)
        {
            LinearSequence line = new LinearSequence(target, kShared, this.root);
            this.lines.Add(line);
            return line;
        }

        protected virtual LinearSequence CreateLinearSequence(
            WayChunk target, int count, 
            ChunkSides sides = ChunkSides.Both, 
            ConstructionDirection direction = ConstructionDirection.Flat, 
            string name = "ls")
        {
            LinearSequence line = CreateLinearSequence(target);
            line.Name = name;
            line.AddBricks(direction, sides, count);
            this.lines.Add(line);
            return line;
        }

        

        protected virtual ObstacleSequence CreateObstacleSequence(WaySystem.Sequences.Sequence preset, WayChunk target, bool flipped)
        {
            ObstacleSequence obstacleSequence = new ObstacleSequence(
                preset, target, kShared, this.root, flipped);
            this.lines.Add(obstacleSequence);
            return obstacleSequence;
        }

        protected virtual LinearSequence CreateClearLine(WayChunk target, string name)
        {
            return CreateLinearSequence(target, kShared.clearLineLengthByTrainSize, ChunkSides.Both, ConstructionDirection.Flat, name);
        }

        protected virtual LinearSequence CreateFillLine(WayChunk target, float length, string name="FillLine")
        {
            LinearSequence line =
                new LinearSequence(target, kShared, root);
            line.Name = name;
            line.FillForkLine(length);
            this.lines.Add(line);
            return line;
        }

        protected virtual LinearSequence CreateFillLine(WayChunk target, WayChunk to, string name="FillLine")
        {
            LinearSequence line =
                new LinearSequence(target, kShared, root);
            line.Name = name;
            float length = Vector3.Distance(target.Dock.Position, to.Transform.position);
            return CreateFillLine(target, length, name);
        }

        protected virtual LinearSequence InitByAssemblieLine(AssemblieLine assemblieLine, string name)
        {
            Point point = new Point(assemblieLine.transform);
            LinearSequence line =
                new LinearSequence(point, kShared, root);

            line.InitBy(assemblieLine, name);
            this.lines.Add(line);
            return line;
        }

        protected void Include(LinearSequence target)
        {
            if (target == null)
            {
                Debug.LogErrorFormat("Can not include null line");
                return;
            }
                
            if (target.Elements.Count == 0)
            {
                Debug.LogErrorFormat("Can not include line without chunks");
                return;
            }
                
            this.kShared.chunk = target.Last.WayChunk;
            Log(0, "shared chunk: '",
                kShared.chunk.Transform.name, "' ",
                kShared.chunk.Transform.position, " ",
                kShared.chunk.Transform.parent.name);
            if (!lines.Contains(target))
                lines.Add(target);
        }
        #endregion

        #region Instantiate
        protected ForkWayChunkBehaviour Instantiate(
            ForkWayChunkBehaviour source, Point target)
        {
            ForkWayChunkBehaviour clone = 
                this.InstantiateAndDock<ForkWayChunkBehaviour>(source, target, root);
            kShared.wayRoot.AddWayChunk(clone.Primary);
            kShared.wayRoot.AddWayChunk(clone.Secondary);
            this.toRemove.Add(clone);
            return clone;
        }

        protected ConvergenceWayChunkBehaviour Instantiate(
            ConvergenceWayChunkBehaviour source, Point target)
        {
            ConvergenceWayChunkBehaviour clone = 
                this.InstantiateAndDock<ConvergenceWayChunkBehaviour>(source, target, root);

            kShared.wayRoot.AddWayChunk(clone.Primary);
            kShared.wayRoot.AddWayChunk(clone.Secondary);
            this.toRemove.Add(clone);
            return clone;
        }

        protected Additions.Switch.Switch Instantiate(Additions.Switch.Switch source, Point dock)
        {
            Additions.Switch.Switch switcher = 
                InstantiateAndDock<Additions.Switch.Switch>(source, dock, root);
            return switcher;
        }

        private T InstantiateAndDock<T>(T source, Point dock, Transform parent) where T : UberBehaviour
        {
            T result =
                GameObject.Instantiate<T>(source);
            result.transform.SetParent(parent, false);
            Point rounded = dock.Rounded;
            result.transform.position = rounded.Position;
            result.transform.rotation = rounded.Rotation;
            return result;
        }
        #endregion

        #region Stuff
        [System.Serializable]
        protected class Stuff
        {
            public Stuff(SubBlock sBlock)
            {
                this.chunks = new List<WayChunk>();
                this.sBlock = sBlock;
            }

            private List<WayChunk> chunks;
            private SubBlock sBlock;

            public void Add(LinearSequence linearSequence)
            {
                this.chunks.AddRange(linearSequence.Chunks);
            }

            public void Add(List<WayChunkBehaviour> lst)
            {
                foreach (WayChunkBehaviour chunk in lst)
                {
                    Add(chunk.WayChunk);
                }
            }

            public void Add(WayChunk el)
            {
                this.chunks.Add(el);
            }

            public void ProcessAndReopen(bool setBarrier=true)
            {
                if (chunks.Count == 0)
                    return;
                Way way = sBlock.WayChunksToWay(chunks);
                Process(way, sBlock.flipped, setBarrier);
                chunks = new List<WayChunk>();
            }

            private void Process(
                Way stuffWay, bool flipped, bool setBarrier = true)
            {
                List<StuffBehaviour> result = new List<StuffBehaviour>();
                float lengthLeft = stuffWay.Length;
                float stuffWayPosition = 0;
                StuffWeightedElement previous = null;
                bool first = true;
                while (lengthLeft > 2f)
                {
                    StuffWeightedElement stuffEl =
                        sBlock.kShared.stuffBehs.GetRandomElement();
                    if (first && setBarrier)
                    {
                        stuffEl =
                            sBlock.kShared.startStuffBehs.GetRandomElement();
                    }
                    if (stuffEl != previous)
                    {
                        bool create = false;
                        previous = stuffEl;
                        if (first)
                        {
                            first = false;
                            create = true;
                        }

                        float sizeZ = stuffEl.Stuff.Bounds[0].size.z;
                        if (!create)
                            create = sizeZ <= lengthLeft;

                        if (!create)
                            continue;

                        lengthLeft = lengthLeft - sizeZ;
                        float half = sizeZ / 2f;
                        stuffWayPosition += half;
                        StuffBehaviour stuffBehaviour =
                            GameObject.Instantiate<StuffBehaviour>(
                                stuffEl.Stuff, sBlock.kShared.additionalRoot);
                        result.Add(stuffBehaviour);
                        Point point =
                            stuffWay.GetPointByOffset(stuffWayPosition);

                        stuffBehaviour.transform.position = point.Position;
                        stuffBehaviour.transform.rotation = point.Rotation;
                        foreach (Transform tr in stuffBehaviour.Transforms)
                        {
                            float ofst = tr.localPosition.z;
                            Point sPoint =
                                stuffWay.GetPointByOffset(stuffWayPosition + ofst);
                            tr.position =
                                new Vector3(tr.position.x, sPoint.Position.y, tr.position.z);
                            tr.rotation = sPoint.Rotation;
                            stuffBehaviour.transform.SetParent(sBlock.kShared.additionalRoot, true);
                        }
                        stuffWayPosition += half;
                        stuffBehaviour.Flip = flipped;
                    }
                }

                foreach (StuffBehaviour stuffBehaviour in result)
                {
                    GameObject.Destroy(stuffBehaviour);
                }
            }
        }
        #endregion
    }
}