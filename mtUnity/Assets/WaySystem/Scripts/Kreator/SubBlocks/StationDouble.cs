using MTCore;
using System.Collections.Generic;
using UnityEngine;
using WaySystem.Additions;

namespace WaySystem.Kreator
{
    [System.Serializable]
    public class StationDouble : StationBase
    {
        public StationDouble(
            Block targetBlock, 
            KreatorShared kreatorShared,
            TripSize stationSize,
            List<int> cargos,
            MTParameters.Flipper.FlipperParameters flipperParameters,
            (int, int) indexes, string name = "StationDouble")
                : base(
                      targetBlock, 
                      kreatorShared,
                      stationSize, 
                      cargos,
                      flipperParameters,
                      indexes,
                      2,
                      name)
        {
            
        }

        protected ForkWayChunkBehaviour fork;
        protected ConvergenceWayChunkBehaviour convergence;

        protected ConstructionDirection there;
        protected ConstructionDirection backAgain;
        protected BowBase bow;
        protected LinearSequence centralLine;


        protected override void CalculateWallOffset()
        {
            base.CalculateWallOffset();
            if (leftSide)
            {
                this.wallOffset = StationParameters.PlatformDepth * 3;
            }   
            else
            {
                this.wallOffset = StationParameters.PlatformDepth;
            }
            this.wallOffset += BKG_ADDITIONAL_OFFSET;

            this.there = this.leftSide ? ConstructionDirection.Left : ConstructionDirection.Right;
            this.backAgain = this.leftSide ? ConstructionDirection.Right : ConstructionDirection.Left;
        }

        public override void Process()
        {
            base.Process();
            ProcessAppendSFDBreakingLine();
            LinearSequence switchersLine = ProcessAppendSwitchersLine();

            //�������� 
            this.fork = CreateFork(leftSide, kShared.chunk);
            this.convergence = CreateConvergence(leftSide, fork.Primary);
            this.fork.Primary.OnEnter += () => { stationManager.SwitchersPassed(); };
            this.fork.Secondary.OnEnter += () => { stationManager.SwitchersPassed(); };


            CreateCentralPart();

            //������ ����� ��� ��������
            LinearSequence emptyPostLine =
                CreateClearLine(convergence.Primary, "convergence2PostLine");
            kShared.pickUpDistributor.Add(emptyPostLine);
            kShared.chunk = emptyPostLine.Last.WayChunk;
            WayChunkBehaviour semaphoreChunk = emptyPostLine.Middle;
            Triggers.Semaphore semaphore =
                GameObject.Instantiate<Triggers.Semaphore>(
                    kShared.collection.semaphore, root);
            semaphore.Init(semaphoreChunk.WayChunk, 0f, semaphoreChunk.transform.position);

            SingleSwitcherManager singleSwitcherManager =
                new SingleSwitcherManager(
                    kShared.collection.switcher,
                    switchersLine.First.Dock,
                    kShared.collection.switchSources.numbers[1],
                    kShared.collection.switchSources.numbers[2],
                    StationParameters.SwitchersLine);

            singleSwitcherManager.OnSwitch = (value) =>
            {
                switch (value)
                {
                    //1 ������ �����
                    case (SwitcherManager.Switcher.Primary):
                    {
                        fork.Primary.Link(switchersLine.Last.WayChunk);
                        convergence.Primary.Link(this.centralLine.Last.WayChunk);
                        emptyPostLine.First.WayChunk.Link(convergence.Primary);
                        stationManager.WayNumber = 1;
                        break;
                    }
                    //2 �����
                    case (SwitcherManager.Switcher.Secondary):
                    {
                        fork.Secondary.Link(switchersLine.Last.WayChunk);
                        convergence.Secondary.Link(bow.End.Last.WayChunk);
                        emptyPostLine.First.WayChunk.Link(convergence.Secondary);
                        stationManager.WayNumber = 2;
                        break;
                    }
                }
            };
            singleSwitcherManager.Randomize();

            # region ����������� ��������
            foreach (KeyValuePair<int, int> kvp in DistributeResourcesRandomly(this.cargos))
            {
                //wayIndex = kvp.Key : resourceID = kvp.Value; 
                Platform platform = CreatePlatform(
                        platformCreationInfos[kvp.Key],
                        kvp.Value,
                        stationSize);
            }
            stationManager.SetUnloadLimits(indexes.currentIndex, indexes.totalCount);
            #endregion

            if (this.leftSide)
            {
                stuff.Add(fork.Primary);
                stuff.Add(centralLine);
                stuff.Add(convergence.Primary);

                stuffSecondary.ProcessAndReopen(false);
                stuffSecondary.Add(fork.Secondary);
                stuffSecondary.Add(bow.Start);
                stuffSecondary.Add(bow.Center);
                stuffSecondary.Add(bow.End);
                stuffSecondary.Add(convergence.Secondary);
                stuffSecondary.ProcessAndReopen(false);
            }
            else
            {
                stuff.Add(fork.Secondary);
                bow.Process((wcb) =>
                {
                    stuff.Add(wcb.WayChunk);
                });
                stuff.Add(convergence.Secondary);

                stuffSecondary.ProcessAndReopen(false);
                stuffSecondary.Add(fork.Primary);
                stuffSecondary.Add(centralLine);
                stuffSecondary.Add(convergence.Primary);
                stuffSecondary.ProcessAndReopen(false);
            }
            Close();
        }

        protected virtual void CreateCentralPart()
        {
            this.bow =
                new BowH(
                    this, fork.Secondary,
                    StationParameters.PlatformDepth,
                    StationParameters.PlatformLineChunksCount,
                    there, "bendBow");

            bow.BaseProcess();
            PlatformCreationInfo bendPlatformCreationInfo = new PlatformCreationInfo(
                bow.Start.First.WayChunk, bow.Center,
                new Additions.PlatformAdditionalInfo(2, GetSFDTextureLine(2), bow.Start.SummLength));
            platformCreationInfos.Add(bendPlatformCreationInfo);

            //������� ��������
            PlaceConvergence(
                this.convergence, 
                bow.End.Last.WayChunk.Dock.Position,
                this.fork.Primary.Dock.Rotation);

            this.centralLine = CreateCentralScaleLine(fork.Primary, convergence.Primary, "centralLine");
            convergence.Link(centralLine.Last.WayChunk, bow.End.Last.WayChunk);

            //��������� ���� 1
            PlatformCreationInfo linePlatformCreationInfo = new PlatformCreationInfo(
                centralLine.First.WayChunk, centralLine,
                new Additions.PlatformAdditionalInfo(1, GetSFDTextureLine(1), 0f));
            platformCreationInfos.Add(linePlatformCreationInfo);
        }
    }
}