using UnityEngine;
using System.Collections.Generic;
using WaySystem.Kreator.PickUpDistribution;

namespace WaySystem.Kreator.SubBlocks
{
    public class RepairFork : RandomFork
    {
        public RepairFork(Block targetBlock, KreatorShared kreatorShared)
                : base(targetBlock, kreatorShared, 4, 5, "RepairFork") { }

        public override void Process()
        {
            base.Process();
            Vector3 offset = new Vector3(
                        0f, 
                        PickUpDistributor.Y_OFFSET, 
                        PickUpDistributor.Z_OFFSET + 2);

            Vector3 position = eventAtBend ?
                bendLine.Last.WayChunk.Dock.Position + offset : 
                straightLine.Last.WayChunk.Dock.Position + offset;

            WaySystem.PickUp.PickUp pickUp =
                        GameObject.Instantiate<WaySystem.PickUp.PickUp>(
                            kShared.collection.pickUps.Get(PickUp.Type.GearRepair),
                            position,
                            Quaternion.identity);

            List<WayChunkBehaviour> bend =
               bendLine.AddBricks(ConstructionDirection.Flat, ChunkSides.RailsUp, 4);
            List<WayChunkBehaviour> straight =
                straightLine.AddBricks(ConstructionDirection.Flat, ChunkSides.RailsUp, 4);

            ConvergenceWayChunkBehaviour convergence = Convergence(bendLine.Last.WayChunk, straightLine.Last.WayChunk, false);
            LinearSequence endLine = CreateEndLine(convergence, 2);
             CreateSwitcher(convergence, endLine.First.WayChunk,
                kShared.collection.switchSources.numbers[1],
                kShared.collection.switchSources.numbers[2]);
            Close();
        }
    }
}