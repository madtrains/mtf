using System.Collections.Generic;
using UnityEngine;
using WaySystem.Kreator.SequenceProcessor;

namespace WaySystem.Kreator.SubBlocks
{
    public class CoinsFork : RandomFork
    {
        public CoinsFork(Block targetBlock, KreatorShared kreatorShared, int maximalObstaclesCount=5)
                : base(targetBlock, kreatorShared, 4, 5, "CoinsFork")
        {
            this.obstaclesCount = Random.Range(2, maximalObstaclesCount);
        }

        private LinearSequence coinsLine;
        private int obstaclesCount;

        public override void Process()
        {
            base.Process();
            stuff.ProcessAndReopen();
            straightLine.AddClearLine(kShared.clearLineLengthByTrainSize);
            bendLine.AddClearLine(kShared.clearLineLengthByTrainSize);

            //eventAtBend - ������� �� ������
            WayChunk obstacleDockChunk = eventAtBend ? straightLine.Last.WayChunk : bendLine.Last.WayChunk;
            WayChunk coinsDockChunk = !eventAtBend ? straightLine.Last.WayChunk : bendLine.Last.WayChunk;
            WayChunk lastObstacleChunk =
                    CreateObstacleLines(obstacleDockChunk, obstaclesCount);
            coinsLine =
                    CreateLinearSequence(
                        coinsDockChunk,
                        obstacleDockChunk.Dock.FloorIntDistance(lastObstacleChunk.Dock));

            WayChunk bendLineEnd = eventAtBend ? coinsLine.Last.WayChunk : lastObstacleChunk;
            WayChunk straightLineEnd = eventAtBend ? lastObstacleChunk : coinsLine.Last.WayChunk;
            

            //����� ������� �� ������ �����
            PickUpDistribution.PickUpDistributor pickUpDistributor = 
                new PickUpDistribution.PickUpDistributor(kShared);
            pickUpDistributor.Add(coinsLine);
            pickUpDistributor.Close();
            pickUpDistributor.ProcessCuts();

            ConvergenceWayChunkBehaviour convergence = 
                Convergence(bendLineEnd, straightLineEnd, true);
            LinearSequence endLine = CreateEndLine(convergence, 3);
            CreateSwitcher(convergence, endLine.First.WayChunk, kShared.collection.switchSources.numbers[1], kShared.collection.switchSources.numbers[2]);

            Close();
        }
    }
}