using MTRails;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WaySystem.Kreator.SequenceProcessor;

namespace WaySystem.Kreator.SubBlocks
{
    public class PlaneFork : RandomFork
    {
        public PlaneFork(Block targetBlock, KreatorShared kreatorShared, int blankLineCount, int obstaclesCount)
                : base(targetBlock, kreatorShared, 5, 6, "PlaneFork")
        {
            this.obstaclesCount = obstaclesCount;
            this.blankLineCount = blankLineCount;
        }

        private int blankLineCount;
        private int obstaclesCount;
        private Albatros albatros;



        public override void Process()
        {
            base.Process();

            this.bendLine.AddBricks(ConstructionDirection.Flat, ChunkSides.RailsUp, this.blankLineCount);
            this.straightLine.AddBricks(ConstructionDirection.Flat, ChunkSides.RailsUp, this.blankLineCount);

            //������� � ���
            if (!leftSide)
            {
                CreateAddPlaneLine(straightLine);
                stuff.Add(this.bendLine);
                stuff.ProcessAndReopen();
                CreateAddHardLine(bendLine);
               
            }
            //������ ������ � ���
            else
            {
                stuff.Add(this.straightLine);
                stuff.ProcessAndReopen();
                CreateAddPlaneLine(bendLine);
                CreateAddHardLine(straightLine);
            }

            
            int zDiffer = Mathf.FloorToInt(bendLine.CompareEndDockZ(straightLine));
            //����� ������� ��� ������ �����
            if (zDiffer > 0)
            {
                straightLine.AddBricks(ConstructionDirection.Flat, ChunkSides.Both, zDiffer);
            }
            else
            {
                bendLine.AddBricks(ConstructionDirection.Flat, ChunkSides.Both,Mathf.Abs(zDiffer));
            }



            ConvergenceWayChunkBehaviour convergence = 
                Convergence(bendLine.Last.WayChunk, straightLine.Last.WayChunk, true);
            LinearSequence endLine = CreateEndLine(convergence, 2);
            CreateSwitcher(convergence, endLine.First.WayChunk,
               kShared.collection.switchSources.numbers[1],
               kShared.collection.switchSources.numbers[2]);
            Close();
        }

        /// <summary>
        /// returns planeChunk
        /// </summary>
        /// <param name="target"></param>
        private void CreateAddPlaneLine(LinearSequence target)
        {
            int lines5Count = 8;
            target.InstantiateAdd(kShared.collection.embankments.start.Element);
            target.InstantiateAdd(kShared.collection.embankments.line5.Element);
            WayChunk runChunk = target.Last.WayChunk;
            runChunk.OnEnter += () =>
            {
                albatros.Run();
            };
            for (int i = 0; i < lines5Count-1; i++)
            {
                target.InstantiateAdd(kShared.collection.embankments.line5.Element);
            }
            target.InstantiateAdd(kShared.collection.embankments.tunnel.Element);
            target.InstantiateAdd(kShared.collection.embankments.tunnel.Element);
            for (int i = 0; i < lines5Count; i++)
            {
                target.InstantiateAdd(kShared.collection.embankments.line5.Element);
            }
            WayChunk planeChunk = target.Last.WayChunk;
            target.InstantiateAdd(kShared.collection.embankments.end.Element);
            target.AddClearLine(kShared.clearLineLengthByTrainSize);
            CreateAddObstacleLines(target, 2);
            CreateAlbatros(planeChunk.Transform.position);
        }

        private void CreateAddHardLine(LinearSequence target)
        {
            List<WayChunkBehaviour> clearLine = 
                target.AddClearLine(kShared.clearLineLengthByTrainSize);
            clearLine.First().WayChunk.OnEnter = () =>
            {
                GameObject.Destroy(this.albatros.gameObject);
            };
            CreateAddObstacleLines(target, 6);
        }

        private void CreateAlbatros(Vector3 planePosition)
        {
            this.albatros = GameObject.Instantiate<Albatros>(kShared.collection.albatros);
            planePosition = planePosition + Vector3.up * 7f;
            Quaternion planeRotation = Quaternion.LookRotation(Vector3.back);
            albatros.transform.position = planePosition;
            albatros.transform.rotation = planeRotation;
            albatros.Init(new List<PickUp.PickUp>() {
                kShared.collection.pickUps.Get(PickUp.Type.GearRepair),
                kShared.collection.pickUps.Get(PickUp.Type.Ticket),
                kShared.collection.pickUps.Get(PickUp.Type.CoinGold)
            });
        }
    }
}