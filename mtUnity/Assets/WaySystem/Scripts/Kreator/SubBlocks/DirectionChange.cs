using System;

namespace WaySystem.Kreator
{
    public class DirectionChange : AvoidableBlock
    {
        private bool flippable;

        public DirectionChange(Block targetBlock, KreatorShared kreatorShared, bool flippable)
                : base(targetBlock, kreatorShared, "Direction Change")
        {
            this.flippable = flippable;
        }

        public override void Process()
        {
            base.Process();
            if (!flippable)
            {
                kShared.pickUpDistributor.Close();
            }
            FlipRandomly();
            ChunkSides sides = ChunkSides.Both;
            if (!this.flippable)
                sides = UnflippableChunkSides;
            Prediction avoidance = this.AvoidFloorAndCeilingByHeights(sides, 8);

            //����������� ��� �������
            if (avoidance != Prediction.OK)
            {
                Log(0, "Direction changed already by avoidance");
                Close();
                return;
            }

            float newAngle = GetCheckRandomAngle(kShared.chunk.Dock);
            var bendChunks = this.baseLine.BendUpOrDown(newAngle, sides);
            Log(0, "Direction changed regularly to ", newAngle);
            
            if (sides != ChunkSides.Both)
            {
                stuff.Add(bendChunks);
                int chunksLeft = MIN_UNFLIPPABLE_CHUNKS_COUNT - bendChunks.Count;
                if (chunksLeft > 0)
                {
                    var addChunks = baseLine.AddBricks(
                        ConstructionDirection.Flat, sides, chunksLeft);
                    stuff.Add(addChunks);
                }
                baseLine.AddClearLine(kShared.clearLineLengthByTrainSize);
                Log(0, "Adding Clear line to unflippable bend");
            }
            else
            {
                kShared.pickUpDistributor.Add(bendChunks);
            }

            Include(baseLine);
            Close();
        }
    }
}