using MTCore;
using MTParameters.Flipper;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.Kreator
{
    [System.Serializable]
    public class StationDoubleV : StationDouble
    {
        public StationDoubleV(
            Block targetBlock,
            KreatorShared kreatorShared,
            TripSize stationSize,
            List<int> cargos,
            MTParameters.Flipper.FlipperParameters flipperParameters,
            (int, int) indexes)
                : base(
                      targetBlock, 
                      kreatorShared, 
                      stationSize,
                      cargos,
                      flipperParameters,
                      indexes, "StationDoubleVertical")
        {
            
        }

        private int bendH = 2;
        private int bendV = 20;

        protected override void CalculateWallOffset()
        {
            base.CalculateWallOffset();
            if (leftSide)
            {
                this.wallOffset = (StationParameters.PlatformDepth * 2);
            }
            else
            {
                this.wallOffset = StationParameters.PlatformDepth;
            }
            this.wallOffset += BKG_ADDITIONAL_OFFSET;
        }

        override protected void CreateCentralPart()
        {
            ConstructionDirection thereH = this.leftSide ? ConstructionDirection.Left : ConstructionDirection.Right;
            ConstructionDirection thereV = this.leftSide ? ConstructionDirection.Up : ConstructionDirection.Down;
            this.bow =
                new BowHV(
                    this, fork.Secondary,
                    bendH, bendV,
                    StationParameters.PlatformLineChunksCount,
                    thereH,
                    thereV, "bendBow");

            bow.BaseProcess();
            PlatformCreationInfo bendPlatformCreationInfo = new PlatformCreationInfo(
                bow.Start.First.WayChunk, bow.Center,
                new Additions.PlatformAdditionalInfo(2, GetSFDTextureLine(2), bow.Start.SummLength));
            platformCreationInfos.Add(bendPlatformCreationInfo);

            //������� ��������
            PlaceConvergence(
                this.convergence,
                bow.End.Last.WayChunk.Dock.Position,
                this.fork.Primary.Dock.Rotation);

            this.centralLine = CreateCentralScaleLine(fork.Primary, convergence.Primary, "centralLine");
            convergence.Link(centralLine.Last.WayChunk, bow.End.Last.WayChunk);

            //��������� ���� 1
            PlatformCreationInfo linePlatformCreationInfo = new PlatformCreationInfo(
                centralLine.First.WayChunk, centralLine,
                new Additions.PlatformAdditionalInfo(1, GetSFDTextureLine(1), 0f));
            platformCreationInfos.Add(linePlatformCreationInfo);

            bow.BaseProcess();
        }
    }
}