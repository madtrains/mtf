using MTRails;
using UnityEngine;
using WaySystem.Additions;

namespace WaySystem.Kreator.SubBlocks
{
    public class DeadEmbankment : RandomFork
    {
        public DeadEmbankment(Block targetBlock, KreatorShared kreatorShared)
                : base(targetBlock, kreatorShared, 1, 8, "DeadEnd")
        {
            this.bendIsDeadEnd = UberBehaviour.RandomBool;
            polesLine = new PolesLine(kShared.collection.elPole, kShared.additionalRoot);
        }

        private bool bendIsDeadEnd;
        private RRCollision rrCollision;
        PolesLine polesLine;

        protected override void CreateForkSleeves()
        {
            this.bendLine =
                    CreateLinearSequence(fork.Secondary, bendChunksCount, ChunkSides.RailsUp, this.firstDirection, "bendLine");
            WayChunkBehaviour rrWayChunkBehaviour = bendLine.InstantiateAdd(
                kShared.collection.deadEnd.Element as WayChunkBehaviour);
            rrCollision = rrWayChunkBehaviour.WayChunk.LinkedCollision;

            stuff.Add(bendLine);

            this.straightLine = CreateLinearSequence(fork.Primary,
                4, ChunkSides.RailsUp, ConstructionDirection.Flat, "straightLine");

            float tunnels = UnityEngine.Random.Range(0f, 1f);
            int randomBendsCount = UnityEngine.Random.Range(0, 6);

            straightLine.InstantiateAdd(kShared.collection.embankments.start.Element);
            CreateLinePair(straightLine);
            for (int j = 0; j < randomBendsCount; j++)
            {
                straightLine.InstantiateAdd(kShared.collection.embankments.GetElement(ConstructionDirection.Right));
                polesLine.Clone(straightLine.Last.transform);
            }
            polesLine.Close();

            //--����--
            straightLine.InstantiateAdd(kShared.collection.embankments.bridgeStart.Element);
            straightLine.InstantiateAdd(kShared.collection.embankments.bridge10.Element);

            var bc = straightLine.InstantiateAdd(kShared.collection.embankments.bridge10.Element).GetComponent<BridgeController>();
            bc.CockTheBridge();
            straightLine.InstantiateAdd(kShared.collection.embankments.bridge10.Element);

            straightLine.InstantiateAdd(kShared.collection.embankments.bridge10.Element);
            bc = straightLine.InstantiateAdd(kShared.collection.embankments.bridge10.Element).GetComponent<BridgeController>();
            bc.CockTheBridge();

            straightLine.InstantiateAdd(kShared.collection.embankments.bridge10.Element);
            straightLine.InstantiateAdd(kShared.collection.embankments.bridgeEnd.Element);
            //--����--

            for (int j = 0; j < randomBendsCount; j++)
            {
                straightLine.InstantiateAdd(kShared.collection.embankments.GetElement(ConstructionDirection.Left));
                polesLine.Clone(straightLine.Last.transform);
            }
            CreateLinePair(straightLine);
            straightLine.InstantiateAdd(kShared.collection.embankments.tunnel.Element);
            polesLine.Clone(straightLine.Last.transform);
            if (tunnels > 0.5f)
            {
                straightLine.InstantiateAdd(kShared.collection.embankments.tunnel.Element);
                polesLine.Clone(straightLine.Last.transform);
            }
            CreateLinePair(straightLine);
            for (int j = 0; j < randomBendsCount; j++)
            {
                straightLine.InstantiateAdd(kShared.collection.embankments.GetElement(ConstructionDirection.Left));
                polesLine.Clone(straightLine.Last.transform);
            }
            for (int j = 0; j < randomBendsCount; j++)
            {
                straightLine.InstantiateAdd(kShared.collection.embankments.GetElement(ConstructionDirection.Right));
                polesLine.Clone(straightLine.Last.transform);
            }
            CreateLinePair(straightLine);
            straightLine.InstantiateAdd(kShared.collection.embankments.end.Element);
            polesLine.Clone(straightLine.Last.transform);

            polesLine.Close();
            
            //������ �����
            LinearSequence emptyPostLine =
                CreateClearLine(straightLine.Last.WayChunk, "emptyLine");

            kShared.pickUpDistributor.Add(emptyPostLine);
            kShared.chunk = emptyPostLine.Last.WayChunk;

            GameObject firstIcon = kShared.collection.switchSources.arrow;
            GameObject secondIcon = kShared.collection.switchSources.cross;
           
            CreateSwitcher(firstIcon, secondIcon);

            rrCollision.OnTrainCollision += () =>
            {
                switcher.Enable();
            };
            Close();
        }

        private void CreateLinePair(LinearSequence target)
        {
            target.InstantiateAdd(kShared.collection.embankments.line5.Element);
            polesLine.Clone(target.Last.transform);
            target.InstantiateAdd(kShared.collection.embankments.line5.Element);
        }

    }
}