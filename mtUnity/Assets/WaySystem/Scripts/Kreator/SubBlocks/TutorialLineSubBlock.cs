namespace WaySystem.Kreator
{
    public class TutorialLineSubBlock : SubBlock
    {
        private static readonly int FIRST_LINE_COUNT = 40;
        private static readonly int COINS_LINE_COUNT = 30;
        private static readonly int PASS_LINE_COUNT = 28;
        private static readonly int EMPTY_LINE_COUNT = 12;
        private static readonly string START_KEY_STRING = "Flipper.Tutorial.Start";
        private static readonly string OBSTACLE_HIT_KEY_STRING = "Flipper.Tutorial.ObstacleHitPoint";
        private static readonly string OBSTACLE_DEATH_KEY_STRING = "Flipper.Tutorial.ObstacleDeath";
        private static float CAMERA_FLY_SPEED = 40;

        public WayChunkBehaviour enterChunkBehaviour;

        public TutorialLineSubBlock(Block targetBlock, KreatorShared kreatorShared)
                : base(targetBlock, kreatorShared, "Tutorial Line") { }

        public override void Process()
        {
            /*
            #region Enter copypaste
            kreatorShared.enter =
                InstantiateAtTheEnd(
                    ProjectElement(kreatorShared.collection.enter.Get(false))
                    ).First as WaySystem.Enter;

            kreatorShared.wayRoot.FirstChunk = kreatorShared.enter.GetLineChunk(0).WayChunk;
            kreatorShared.wayRoot.TrainStartOffset = kreatorShared.enter.TrainStartOffset;
            kreatorShared.wayRoot.CamDock = kreatorShared.enter.CamDock;

            WayChunkBehaviour cameraEventChunk =
                kreatorShared.enter.GetLineChunk(kreatorShared.enter.CameraEventChunkIndex);
            cameraEventChunk.WayChunk.OnEnter += () =>
            {
                GameStarter.Instance.Train.Command_Enter_Start(kreatorShared.enter);
            };
            #endregion


            //������ ����� ����� �� �������
            OberList<WayElementBehaviour> firstLine =
                InstantiateAtTheEnd(ProjectLine(kreatorShared.currentPoint, FIRST_LINE_COUNT, null));
            TrainTutorialTrigger startTrigger =
                CreateTutorialTrigger(
                    firstLine.List[16].MainDockGlobal);
            startTrigger.AddStep(
                new TrainStop());
            startTrigger.AddStep(new SetFLipperScreenVisibility(false));
            startTrigger.AddStep(new Dialog(
                GameStarter.Instance.TranslationManager.GetTableString(START_KEY_STRING),
                false));

            //�������
            WayChunkBehaviour semaphoreWcb = firstLine.Last as WayChunkBehaviour;
            kreatorShared.enter.Semaphore.Init(semaphoreWcb.WayChunk.WayGroup, 0f, semaphoreWcb.transform.position, 0);
            Vector3 semaphoreVisualPosition = kreatorShared.enter.Semaphore.transform.position + new Vector3(-2.5f, 0f, 0.65f);
            Point semaphoreCameraTarget = new Point(semaphoreVisualPosition, Quaternion.identity);
            startTrigger.AddStep(
                new CameraFlyTo(
                    semaphoreVisualPosition,
                    GameStarter.GameParameters.Cameras[5],
                    CAMERA_FLY_SPEED));

            startTrigger.AddStep(
                new DialogAndArrow(
                        GameStarter.Instance.TranslationManager.GetTableString(START_KEY_STRING + ".2"),
                        false,
                        kreatorShared.helpers,
                        semaphoreCameraTarget, 5f, 9f));

            startTrigger.AddStep(
                new Dialog(
                    GameStarter.Instance.TranslationManager.GetTableString(START_KEY_STRING + ".3"),
                    false));

            //���������
            InstantiateAtTheEnd(ProjectLine(kreatorShared.currentPoint, EMPTY_LINE_COUNT, null));

            //����� � ���������
            OberList<WayElementBehaviour> coinsLine =
                InstantiateAtTheEnd(ProjectLine(kreatorShared.currentPoint, COINS_LINE_COUNT, null));
            Point coinsTarget = coinsLine.List[COINS_LINE_COUNT / 2].MainDockGlobal;
            startTrigger.AddStep(
                new CameraFlyTo(
                    coinsTarget.Position,
                    GameStarter.GameParameters.Cameras[5],
                    CAMERA_FLY_SPEED));
            startTrigger.AddStep(
                new DialogAndArrow(
                    GameStarter.Instance.TranslationManager.GetTableString(START_KEY_STRING + ".4"),
                    false,
                    kreatorShared.helpers,
                    coinsTarget, 1.7f, 5.7f));
            startTrigger.AddStep(
                new Dialog(
                    GameStarter.Instance.TranslationManager.GetTableString(START_KEY_STRING + ".5"),
                    true));
            startTrigger.AddStep(
                new ReturnCamera(CAMERA_FLY_SPEED));
            startTrigger.AddStep(
                new SetFLipperScreenVisibility(true));
            startTrigger.AddStep(
                new TrainGo(Trains.Train.EngineSpeedState.Acceleration, true, true));

            //����������� �������
            OberList<WayChunkBehaviour> coinsList = new OberList<WayChunkBehaviour>();
            coinsLine.Iterate((el) =>{ 
                WayChunkBehaviour wcb = el as WayChunkBehaviour;
                if (wcb != null) 
                { 
                    coinsList.Add(wcb);
                }
            });
            StuffWithCoins(coinsList, 0, 0, 1f);

            Log(1, "Obstacles. (", kreatorShared.mainLineElements.Last.gameObject.name, ")");

            //���������
            var eLine = InstantiateAtTheEnd(ProjectLine(kreatorShared.currentPoint, EMPTY_LINE_COUNT, null));
            enterChunkBehaviour = eLine.First as WayChunkBehaviour;

            //�������� ����� � ������� � ���� ��������
            OberList<WayElementBehaviour> preObstacleLine = 
                InstantiateAtTheEnd(ProjectLine(kreatorShared.currentPoint, PASS_LINE_COUNT, null));

            TrainTutorialTrigger obstacleTrigger =
                CreateTutorialTrigger(
                    preObstacleLine.First.MainDockGlobal);
            obstacleTrigger.AddStep(
                new TrainStop());
            obstacleTrigger.AddStep(
                new SetFLipperScreenVisibility(false));

            OberList<WayElementBehaviour> els = InstantiateAtTheEnd(ProjectObstacles(3, null, false));
            InstantiateAtTheEnd(ProjectLine(kreatorShared.currentPoint, 4, null));
            els.Append(InstantiateAtTheEnd(ProjectObstacles(3, null, true)));
            InstantiateAtTheEnd(ProjectLine(kreatorShared.currentPoint, 8, null));
            els.Append(InstantiateAtTheEnd(ProjectObstacles(10, null, true)));
            List<Point> obstacleColliderCentres = new List<Point>();
            els.Iterate((el) =>
            {
                WayChunkBehaviour wayChunkBehaviour = el as WayChunkBehaviour;
                if (wayChunkBehaviour.WayChunk.FlipType == WayChunkFlipType.AfterPass)
                {
                    if (wayChunkBehaviour.WayChunk.LinkedCollision.CollisionType ==
                        TrainCollisionType.HitPoint)
                    {
                        obstacleColliderCentres.Add(wayChunkBehaviour.WayChunk.LinkedCollision.ColliderCenter);
                    }
                }
            });
            obstacleTrigger.AddStep(
                new CameraFlyTo(
                    obstacleColliderCentres[0].Position,
                    GameStarter.GameParameters.Cameras[5],
                    CAMERA_FLY_SPEED));
            obstacleTrigger.AddStep(
                new DialogAndArrow(
                    GameStarter.Instance.TranslationManager.GetTableString(OBSTACLE_HIT_KEY_STRING),
                    false,
                    kreatorShared.helpers,
                    obstacleColliderCentres[0],
                    2f, 6f));

            Point flipPoint = new Point(obstacleColliderCentres[0].Position, Quaternion.Euler(180f, 0f, 0f));
            obstacleTrigger.AddStep(
                new DialogAndArrow(
                    GameStarter.Instance.TranslationManager.GetTableString(OBSTACLE_HIT_KEY_STRING + ".2"),
                    false,
                    kreatorShared.helpers,
                    flipPoint,
                    2f, 6f));

            int middleObstacleIndex = 6;
            obstacleTrigger.AddStep(
                new CameraFlyTo(
                    obstacleColliderCentres[middleObstacleIndex].Position,
                    GameStarter.GameParameters.Cameras[5],
                    CAMERA_FLY_SPEED));

            obstacleTrigger.AddStep(
                new DialogAndArrow(
                    GameStarter.Instance.TranslationManager.GetTableString(OBSTACLE_HIT_KEY_STRING + ".3"),
                    false,
                    kreatorShared.helpers,
                    obstacleColliderCentres[middleObstacleIndex],
                    2f, 6f));
            obstacleTrigger.AddStep(
                new Dialog(
                    GameStarter.Instance.TranslationManager.GetTableString(OBSTACLE_HIT_KEY_STRING + ".4"),
                    true));

            obstacleTrigger.AddStep(new SetFLipperScreenVisibility(true));
            obstacleTrigger.AddStep(
                new ReturnCamera(CAMERA_FLY_SPEED));

            obstacleTrigger.AddStep(new UIHighlight(
                        GameStarter.Instance.TranslationManager.GetTableString(OBSTACLE_HIT_KEY_STRING + ".5"),
                        0.5f,
                        GameStarter.Instance.UIRoot.Flipper.HornRect, Popup.Placing.LeftDown));
            obstacleTrigger.AddStep(
                new TrainGo(Trains.Train.EngineSpeedState.Acceleration, true, true));

            //���������
            InstantiateAtTheEnd(ProjectLine(kreatorShared.currentPoint, EMPTY_LINE_COUNT, null));

            //������, ������� ����
            Log(1, "Pass line. (", kreatorShared.mainLineElements.Last.gameObject.name, ")");
            OberList<WayElementBehaviour> pLine =
                InstantiateAtTheEnd(ProjectLine(kreatorShared.currentPoint, PASS_LINE_COUNT, null));
            TrainTutorialTrigger obstacleDeathTrigger =
                CreateTutorialTrigger(
                    pLine.First.MainDockGlobal);
            obstacleDeathTrigger.AddStep(
                new TrainStop());
            obstacleDeathTrigger.AddStep(new SetFLipperScreenVisibility(false));

            kreatorShared.flipped = true;
            OberList<WayElementBehaviour> round = InstantiateAtTheEnd(ProjectRound(-45f));
            OberList<WayChunkBehaviour> roundWayChunkBehaviours = new OberList<WayChunkBehaviour>();
            round.Iterate(
                (roundEl) =>
                {
                    roundWayChunkBehaviours.Add(roundEl as WayChunkBehaviour);
                });
            var dc = new SequenceProcessor.DecorationsSequence(kreatorShared, roundWayChunkBehaviours);
                      
            int middleIndex = Mathf.FloorToInt(round.List.Count / 2);
            Point up = round.List[middleIndex].MainDockGlobal;
            Point down = new Point(up.Position, up.Rotation * Quaternion.Euler(Vector3.right * 180f));

            obstacleDeathTrigger.AddStep(
                new CameraFlyTo(
                    up.Position,
                    GameStarter.GameParameters.Cameras[5],
                    CAMERA_FLY_SPEED));

            obstacleDeathTrigger.AddStep(
                new DialogAndArrow(
                    GameStarter.Instance.TranslationManager.GetTableString(OBSTACLE_DEATH_KEY_STRING),
                    false,
                    kreatorShared.helpers,
                    up,
                    2f, 6f));

            RRCollision rrCollision = (round.First as WayChunkBehaviour).WayChunk.LinkedCollision;
            obstacleDeathTrigger.AddStep(
                new DialogAndArrow(
                    GameStarter.Instance.TranslationManager.GetTableString(OBSTACLE_DEATH_KEY_STRING + ".2"),
                    false,
                    kreatorShared.helpers,
                    new Point(rrCollision.PointerTransform),
                    rrCollision.Height, rrCollision.Height + 4f));

            obstacleDeathTrigger.AddStep(
                new DialogAndArrow(
                    GameStarter.Instance.TranslationManager.GetTableString(OBSTACLE_DEATH_KEY_STRING + ".3"),
                    true,
                    kreatorShared.helpers,
                    down,
                    1.5f, 5.5f));
            obstacleDeathTrigger.AddStep(
                new SetFLipperScreenVisibility(true));
            obstacleDeathTrigger.AddStep(new ReturnCamera(CAMERA_FLY_SPEED));
            obstacleDeathTrigger.AddStep(new UIHighlight(
                        GameStarter.Instance.TranslationManager.GetTableString(OBSTACLE_DEATH_KEY_STRING + ".4"),
                        0.5f,
                        GameStarter.Instance.UIRoot.Flipper.IndicatorRect, Popup.Placing.RightDown));
            obstacleDeathTrigger.AddStep(
                new TrainGo(Trains.Train.EngineSpeedState.Acceleration, true, true));

            kreatorShared.flipped = false;
            InstantiateAtTheEnd(ProjectLine(kreatorShared.currentPoint, PASS_LINE_COUNT, null));
            */
        }

        /*
        private Projection ProjectObstacles(int index, WayElementBehaviourDelegate onInstantiated=null, bool flipSequence=false)
        {
            /*
            //������ ���������� ���������� �� ��������
            Sequences.Sequence obstaclesSequence =
                kreatorShared.obstaclesWeightedSequence.GetElementAt(index);
            Log(2, "Tutorial Obstacles: ", obstaclesSequence.Name, " index: ", index,
                " elements count:", obstaclesSequence.Elements.Length);

            Projection obstacleProjection = 
                new Projection(kreatorShared);

            for (int i = 0; i < obstaclesSequence.Elements.Length; i++)
            {
                Sequences.Element el = obstaclesSequence.Elements[i];
                bool elFlipped = el.Flipped;
                if (flipSequence)
                    elFlipped = !elFlipped;
                WayElementBehaviour wayElBehaviour =
                    kreatorShared.collection.Get(el.Type, elFlipped);

                obstacleProjection.AddElement(wayElBehaviour, (el) => {
                    el.SetFlipped(elFlipped);
                    if (onInstantiated != null)
                        onInstantiated(wayElBehaviour);
                });
                Log(2, "Obstacle: ", el.Type);
            }
            return obstacleProjection;

            return null;
        }
                    */
    }
}