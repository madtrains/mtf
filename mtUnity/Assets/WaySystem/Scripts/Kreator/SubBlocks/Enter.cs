using System;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;


namespace WaySystem.Kreator
{

    public class Enter : SubBlock
    {
        public Enter(Block targetBlock, KreatorShared kreatorShared)
                : base(targetBlock, kreatorShared, "Enter")
        {
        }

        public override void Process()
        {
            base.Process();

            kShared.enter =
                GameObject.Instantiate(
                    kShared.collection.enter.Element, this.root) as WaySystem.Enter;

            kShared.enter.Init();
            foreach (WaySystem.WayChunkBehaviour wcb in kShared.enter.Line.List)
            {
                wcb.WayChunk.SetStoffActionType(StoffActionType.Nothing);
            }
            
            kShared.wayRoot.FirstChunk = kShared.enter.GetLineChunk(0).WayChunk;
            kShared.wayRoot.TrainStartOffset = kShared.enter.TrainStartOffset;
            kShared.wayRoot.CamDock = kShared.enter.CamDock;

            WayChunkBehaviour cameraEventChunk =
                kShared.enter.GetLineChunk(kShared.enter.CameraEventChunkIndex);
            cameraEventChunk.WayChunk.OnEnter += () =>{
                GameStarter.Instance.Train.ProcessEnter(kShared.enter);
            };

            var emptyLine =
                CreateLinearSequence(
                    kShared.enter.LastWayChunkBehaviour.WayChunk);
            emptyLine.AddClearLine(kShared.clearLineLengthByTrainSize);
            


            kShared.enter.Semaphore.Init(
                emptyLine.Last.WayChunk,
                0f,
                emptyLine.Last.transform.position);
            emptyLine.AddClearLine(kShared.clearLineLengthByTrainSize);

            kShared.chunk = emptyLine.Last.WayChunk;
            Close();
        }
    }
}