using MTCore;
using MTParameters.Flipper;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using WaySystem.Additions;

namespace WaySystem.Kreator
{
    [System.Serializable]
    public class StationBase : FlatGreenBlock
    {
        /// <summary>
        /// ����������� ���������� ���������� ������ ��� ������ �� �������� �� ��������
        /// </summary>
        protected static readonly int BEND_CONSTANT = 6;


        protected static readonly int BKG_ADDITIONAL_OFFSET = 7;

        /// <summary>
        /// ���������� ������ ��� �������� � ������ ����� �� 45 ��������
        /// </summary>
        protected static readonly int BEND_45 = 9;
        protected static readonly int FORK_BEND_45 = 3;

        protected static float SIN_45 = Mathf.Sin(Mathf.Deg2Rad * 45f);
        protected static float SIN_45_INV = 1f / SIN_45;

        public StationBase(
            Block targetBlock, 
            KreatorShared kreatorShared,
            TripSize stationSize,
            List<int> cargos,
            MTParameters.Flipper.FlipperParameters flipperParameters,
            (int, int)indexes, int maxPlatformsCount,
            string name)
                : base(targetBlock, kreatorShared, string.Format("{0}_{1}", name, stationSize))
        {
            this.stationSize = stationSize;
            this.cargos = cargos;
            this.indexes = indexes;
            this.maxPlatformsCount = maxPlatformsCount;
            stationManager = new StationManager(kShared.wayRoot, kShared.flipperParameters, stationSize);
        }
        protected bool leftSide;
        protected TripSize stationSize;
        protected List<int> cargos;
        protected (int currentIndex, int totalCount) indexes;
        protected int wallOffset;
        protected int bendChunksCount;
        protected List<PlatformCreationInfo> platformCreationInfos;
        protected int maxPlatformsCount;

        public MTParameters.Flipper.Station StationParameters { get { return kShared.flipperParameters.Stations[(int)this.stationSize]; } }

        public StationManager StationManager { get { return this.stationManager; } }
        protected StationManager stationManager;

        public WayChunk ankunftChunk;


        public override void Process()
        {
            base.Process();
            this.leftSide = UberBehaviour.RandomBool;
            this.platformCreationInfos = new List<PlatformCreationInfo>();
            this.block.BlockManager = stationManager;
            this.kShared.wayRoot.Add(stationManager);

            block.FirstWayChunk.OnEnter += () =>
            {
                stationManager.Prepare();
            };
            CalculateWallOffset();
            ProcessAppendAlign();
        }

        protected virtual LinearSequence CreateStationLine(
            WayChunk target, ConstructionDirection direction, int count,
            string name)
        {
            LinearSequence line = CreateLinearSequence(target);
            line.Name = name;
            line.AddBricks(direction, ChunkSides.RailsUp, count);
            this.lines.Add(line);
            BaseProcess(line);
            return line;
        }

        protected virtual void CalculateWallOffset()
        {
            this.wallOffset = 32;
        }

        protected virtual void ProcessAppendAlign()
        {
            Debug.LogFormat("Wall offset {0} {1}", wallOffset, name);
            Log(0, "Count Wall Offset: ", wallOffset);
            //������������ �� �������/�����
            int chunksCount =
                WallOffsetCount(kShared.chunk,
                this.wallOffset, ONE_STEP_30, MIN_30);
            if (chunksCount != 0)
            {
                //���������� ������ ������ ���� ����� ��������� �� ������
                ConstructionDirection thereDirection =
                    chunksCount > 1 ?
                        ConstructionDirection.Right : ConstructionDirection.Left;

                ConstructionDirection backAgainDirection =
                    thereDirection == ConstructionDirection.Left ?
                        ConstructionDirection.Right : ConstructionDirection.Left;

                // ����� �� �������
                LinearSequence alignLine =
                    CreateStationLine(
                        kShared.chunk, thereDirection, ROUND30,
                        "alignWallLine");

                int abs = Mathf.Abs(chunksCount);
                alignLine.AddBricks(ConstructionDirection.Flat, ChunkSides.RailsUp, Mathf.Abs(abs));

                //������������ �������
                alignLine.AddBricks(backAgainDirection, ChunkSides.RailsUp, ROUND30);
                Include(alignLine);
                stuff.Add(alignLine);
                BaseProcess(alignLine);
            }

            //��������
            WayTeleport teleport =
                kShared.chunk.Transform.gameObject.AddComponent<WayTeleport>();
            teleport.Link(() =>
            {
                stationManager.Prepare();
            }, kShared.chunk);
        }

        protected virtual LinearSequence ProcessAppendSFDBreakingLine()
        {
            // ������ ����� ����������
            LinearSequence breakingLine = CreateStationLine(
                kShared.chunk, ConstructionDirection.Flat,
                StationParameters.BreakingLine.ChunksCount, "breakingLine");

            stuff.Add(breakingLine);

            breakingLine.First.WayChunk.OnEnter += () =>
            {
                stationManager.OnStationBreaking?.Invoke();
            };

            // �����
            Transform sfd =
                GameObject.Instantiate(kShared.collection.sfdPlatz, root);
            sfd.transform.position = breakingLine.Last.Dock.Position;
            sfd.transform.rotation = breakingLine.Last.Dock.Rotation;
            stationManager.Sfd = sfd.GetChild(0);

            // displayIdleLine
            LinearSequence displayLine  = CreateStationLine(
                breakingLine.Last.WayChunk, ConstructionDirection.Flat,
                StationParameters.DisplayLineIdleChunks, "displayIdleLine");
            stuff.Add(displayLine);

            /*
            // displayAccelerationLine
            LinearSequence displayAccelerationLine = CreateStationLine(
                displayIdleLine.Last.WayChunk, ConstructionDirection.Flat,
                StationParameters.DisplayLineAccelerationChunks, "displayAccelerationLine");
            stuff.Add(displayAccelerationLine);
            displayAccelerationLine.First.WayChunk.OnEnter += () =>
            {
                stationManager.OnDisplayLine?.Invoke();
            };
            */
            Include(displayLine);
            return displayLine;
        }

        protected virtual LinearSequence ProcessAppendSwitchersLine()
        {
            LinearSequence switchersLine = CreateStationLine(
                kShared.chunk, ConstructionDirection.Flat,
                StationParameters.SwitchersLine, "switchersLine");
            stuff.Add(switchersLine);
            
            Include(switchersLine);
            return switchersLine;
        }

        protected virtual LinearSequence CreateCentralScaleLine(WayChunk from, WayChunk to, string name)
        {
            float length = to.Transform.position.z - from.Dock.Position.z;
            int floor = Mathf.FloorToInt(length - 1);
            length = length - floor;

            LinearSequence centralLine =
                CreateLinearSequence(from, floor,
                ChunkSides.RailsUp, ConstructionDirection.Flat, name);

            centralLine.FillForkLine(length);
            to.Link(centralLine.Last.WayChunk);
            BaseProcess(centralLine);
            return centralLine;
        }

        protected Platform CreatePlatform(PlatformCreationInfo platformCreationInfo, int resourceID, TripSize size)
        {
            //default
            Platform platformClone =
                GameObject.Instantiate<Platform>(kShared.collection.cargosSection.Platform, root);

            switch (resourceID)
            {
                case (-1):
                {
                    platformClone.Length = 12;
                    platformClone.Width = 5;
                    platformClone.SemaphoreLight(Semaphore3.Light.Off);
                    break;
                }
                //passengers
                case (1):
                {
                    Cargos.Cargo cargo = kShared.collection.cargosSection.Passengers.Get(size);
                    Cargos.CargoPassengers passengers =
                            GameObject.Instantiate<Cargos.Cargo>(cargo) as Cargos.CargoPassengers;
                    passengers.AssignCollection(kShared.wayRoot.PassengersCollection);
                    platformClone.Length = passengers.PlatformLength;
                    platformClone.Width = passengers.PlatformWidth;
                    passengers.transform.position = platformClone.transform.position;
                    passengers.transform.rotation = platformClone.transform.rotation;
                    platformClone.Cargo = passengers;
                    platformClone.SemaphoreLight(Semaphore3.Light.Yellow);
                    break;
                }
                    
                //mail
                case (2):
                {
                    //length = 10;
                    break;
                }

                case (5):
                {
                    Cargos.Cargo cargo = kShared.collection.cargosSection.Wood.Get(size);
                    Cargos.CargoBunch cargoBunch =
                            GameObject.Instantiate<Cargos.Cargo>(cargo) as Cargos.CargoBunch;
                    platformClone.Length = cargoBunch.PlatformLength;
                    platformClone.Width = cargoBunch.PlatformWidth;
                    cargoBunch.transform.position = platformClone.transform.position;
                    cargoBunch.transform.rotation = platformClone.transform.rotation;
                    platformClone.Cargo = cargoBunch;
                    platformClone.SemaphoreLight(Semaphore3.Light.Yellow);
                    break;
                }

                case (7):
                {
                    Cargos.Cargo cargo = kShared.collection.cargosSection.Freight.Get(size);
                    Cargos.CargoBunch cargoBunch =
                            GameObject.Instantiate<Cargos.Cargo>(cargo) as Cargos.CargoBunch;
                    platformClone.Length = cargoBunch.PlatformLength;
                    platformClone.Width = cargoBunch.PlatformWidth;
                    cargoBunch.transform.position = platformClone.transform.position;
                    cargoBunch.transform.rotation = platformClone.transform.rotation;
                    platformClone.Cargo = cargoBunch;
                    platformClone.SemaphoreLight(Semaphore3.Light.Yellow);    
                    break;
                }
            }
            if (platformCreationInfo.TargetLinearSequence.SummLength < platformClone.Length)
            {
                Debug.LogErrorFormat(
                    "Station is too short for platform {0}. CargoID {1}. Line {2}",
                    platformClone.Length, resourceID,
                    platformCreationInfo.TargetLinearSequence.SummLength);
            }

            float central = platformCreationInfo.TargetLinearSequence.SummLength / 2f;
            float offset = central - (platformClone.Length / 2f);
            int index = Mathf.FloorToInt(offset);
            index = Mathf.Clamp(index, 0, platformCreationInfo.TargetLinearSequence.Chunks.Count);
            WayChunk anchor = platformCreationInfo.TargetLinearSequence.Chunks[index];
            
            platformClone.AdditionalInfo = platformCreationInfo.AdditionalInfo;
            platformClone.AdditionalInfo.OnLinePositionOffset = offset;
            platformClone.transform.position = anchor.Transform.position;
            platformClone.InitAlignCargo(resourceID);
            platformClone.gameObject.name = string.Format("platform_{0}", platformCreationInfo.AdditionalInfo.WayNumber);
            stationManager.AddPlatform(platformClone);

            platformCreationInfo.EventChunk.OnEnter += () =>
            {
                //Debug.LogFormat("Check Arrive: {0} {1} {2}", platformClone.name, platformClone.AdditionalInfo.ArrivalLength, info.checkArriveChunk.Transform.name);
                stationManager.PlatormEventChunk();
            };
            return platformClone;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resourcesIDs"></param>
        /// <returns>wayNumber:id</returns>
        protected Dictionary<int, int> DistributeResourcesRandomly(List<int> input)
        {
            Dictionary<int, int> result = new Dictionary<int, int>();
            List<int> shuffle = new List<int>(input);
            if (input.Count < maxPlatformsCount)
            {
                for (int k = 0; k < maxPlatformsCount - input.Count; k ++) 
                {
                    shuffle.Add(-1);
                }
            }

            shuffle = ShuffleResourcesIDs(shuffle);

            for (int i = 0; i < shuffle.Count; i++)
            {
                result[i] = shuffle[i];
            }
            return result;
        }

        private List<int> ShuffleResourcesIDs(List<int> input)
        {
            List<int> result = input;
            System.Random rng = new System.Random();
            int n = result.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                int value = result[k];
                result[k] = result[n];
                result[n] = value;
            }
            return result;
        }

        protected ForkWayChunkBehaviour CreateFork(bool leftSide, WayChunk target, string name = "fork")
        {
            ForkWayChunkBehaviour fork = Instantiate(
                kShared.collection.forksDoubleStraight.Get(leftSide).Fork,
                target.Dock);
            toRemove.Add(fork);
            fork.Link(target);
            fork.Primary.SetStoffActionType(StoffActionType.Nothing);
            fork.Secondary.SetStoffActionType(StoffActionType.Nothing);
            kShared.wayRoot.AddWayChunk(fork.Primary);
            kShared.wayRoot.AddWayChunk(fork.Secondary);
            fork.gameObject.name = name;
            return fork;
        }

        protected ConvergenceWayChunkBehaviour CreateConvergence(bool leftSide, WayChunk target, string name="convergence")
        {
            // ���������
            ConvergenceWayChunkBehaviour convergence =
                Instantiate(
                    kShared.collection.forksDoubleStraight.Get(leftSide).Convergence,
                    target.Dock);
            toRemove.Add(convergence);
            convergence.Primary.Link(target);
            convergence.Primary.SetStoffActionType(StoffActionType.Nothing);
            convergence.Secondary.SetStoffActionType(StoffActionType.Nothing);
            kShared.wayRoot.AddWayChunk(convergence.Primary);
            kShared.wayRoot.AddWayChunk(convergence.Secondary);
            convergence.gameObject.name = name;
            return convergence;
        }

        protected void BaseProcess(LinearSequence linearSequence)
        {
            linearSequence.ProcessWayChunkBehaviours( (wcb) =>
            {
                wcb.WayChunk.SetStoffActionType(StoffActionType.Nothing);
            });
        }

        protected void PlaceConvergence(
            ConvergenceWayChunkBehaviour convergence,
            Vector3 bendChunkPosition,
            Quaternion baseRotation)
        {
            // ������� ������� ��������
            Vector3 offset = bendChunkPosition + convergence.SecondaryOffset;
            Point convergenceDock = new Point(offset, baseRotation);
            convergence.DockTo(convergenceDock);
        }

        protected class BowBase
        {
            public BowBase(StationBase stationBase, WayChunk target, string name) 
            {
                this.stationBase = stationBase;
            }

            public BowBase(LinearSequence start, LinearSequence center, LinearSequence end, StationBase stationBase)
            {
                this.start = start;
                this.center = center;
                this.end = end;
                this.stationBase = stationBase;
            }

            public LinearSequence Start { get { return start; } }
            public LinearSequence Center { get { return center; } }
            public LinearSequence End { get { return end; } }


            protected LinearSequence start;
            protected LinearSequence center;
            protected LinearSequence end;
            protected StationBase stationBase;

            public void Process(LinearSequence.ProcessWayChunkBehaviour process)
            {
                start.ProcessWayChunkBehaviours(process);
                center.ProcessWayChunkBehaviours(process);
                end.ProcessWayChunkBehaviours(process);
            }

            public List<WayChunk> Chunks 
            { 
                get 
                {
                    List<WayChunk> result = new List<WayChunk>();
                    Process((wcb)=> { result.Add(wcb.WayChunk); });
                    return result;
                } 
            }

            public void BaseProcess()
            {
                Process((wcb) => 
                {
                    //this.stationBase.toRemove.Add(wcb);
                    wcb.WayChunk.SetStoffActionType(StoffActionType.Nothing);
                });
                
            }
        }

        protected class BowH : BowBase
        {
            public BowH(
                StationBase stationBase, WayChunk target, 
                int bendCount, int lineCount,
                ConstructionDirection there, string name) : base(stationBase, target, name)
            {
                ConstructionDirection back =
                    there == ConstructionDirection.Right ?
                    ConstructionDirection.Left :
                    ConstructionDirection.Right;
                //this.back = back;

                this.start =
                    stationBase.CreateLinearSequence(target, bendCount,
                    ChunkSides.RailsUp, there, name + "_start");

                this.start.AddBricks(
                    back, ChunkSides.RailsUp,
                    bendCount + BEND_CONSTANT);

                center = stationBase.CreateLinearSequence(
                    this.start.Last.WayChunk,
                    lineCount,
                    ChunkSides.RailsUp,
                    ConstructionDirection.Flat, name + "_middle");

                end = stationBase.CreateLinearSequence(center.Last.WayChunk,
                    bendCount + BEND_CONSTANT,
                    ChunkSides.RailsUp,
                    back, name + "_end");
                end.AddBricks(
                    there, ChunkSides.RailsUp, bendCount - 1);
            }
            /*
             center.ProcessWayChunkBehaviours((el) => {
                    el.WayChunk.SetStoffActionType(StoffActionType.Nothing);
                });
            */
        }

        protected class BowHV : BowBase
        {
            public BowHV(StationBase stationBase, WayChunk target, 
                int bendHCount, int bendVCount, int lineCount,
                ConstructionDirection thereH,
                ConstructionDirection thereV,
                string name) : base(stationBase, target, name)
            {

                ConstructionDirection backH =
                    thereH == ConstructionDirection.Right ?
                    ConstructionDirection.Left :
                    ConstructionDirection.Right;    

                ConstructionDirection backV =
                   thereV == ConstructionDirection.Up ?
                   ConstructionDirection.Down :
                   ConstructionDirection.Up;

                //horizontal there and back
                this.start = stationBase.CreateLinearSequence(
                    target,
                    bendHCount,
                    ChunkSides.RailsUp, 
                    thereH, "_start");
                this.start.AddBricks(backH, ChunkSides.RailsUp, bendHCount + BEND_CONSTANT);

                //vertical there
                this.start.AddBricks(thereV, ChunkSides.RailsUp, bendVCount);
                this.start.AddBricks(backV, ChunkSides.RailsUp, bendVCount);

                center = stationBase.CreateLinearSequence(
                    this.start.Last.WayChunk,
                    lineCount,
                    ChunkSides.RailsUp,
                    ConstructionDirection.Flat, name + "_middle");

                end = stationBase.CreateLinearSequence(center.Last.WayChunk,
                    bendVCount,
                    ChunkSides.RailsUp,
                    backV, name + "_end");
                this.end.AddBricks(thereV, ChunkSides.RailsUp, bendVCount);

                //horizontal back and there
                end.AddBricks(backH, ChunkSides.RailsUp, bendHCount + BEND_CONSTANT);
                this.end.AddBricks(thereH, ChunkSides.RailsUp, bendHCount-1);
            }
        }

        
        protected LinearSequence ThereAndBackAgain(
            WayChunk target,
            ConstructionDirection there, int countThere, 
            ConstructionDirection back, int countBack, string name)
        {
            //horizontal there and back
            LinearSequence linearSequence =
                CreateLinearSequence(target, countThere, ChunkSides.RailsUp, there, name);
            linearSequence.AddBricks(back, ChunkSides.RailsUp, countBack);
            return linearSequence;
        }

        protected LinearSequence BowHStart(WayChunk target, ConstructionDirection there, int bendCount, string name)
        {
            ConstructionDirection back =
                    there == ConstructionDirection.Right ?
                    ConstructionDirection.Left :
                    ConstructionDirection.Right;
            return ThereAndBackAgain(target, there, bendCount, back, bendCount + BEND_CONSTANT, name);
        }

        protected LinearSequence BowHEnd(WayChunk target, ConstructionDirection there, int bendCount, string name)
        {
            ConstructionDirection back =
                    there == ConstructionDirection.Right ?
                    ConstructionDirection.Left :
                    ConstructionDirection.Right;
            return ThereAndBackAgain(target, there, bendCount + BEND_CONSTANT, back, bendCount - 1, name);
        }

        protected class PlatformCreationInfo
        {
            public PlatformCreationInfo(WayChunk chunk, LinearSequence targetLinearSequence, PlatformAdditionalInfo additionalInfo)
            {
                this.eventChunk = chunk;
                this.targetLinearSequence = targetLinearSequence;
                this.additionalInfo = additionalInfo;
            }

            public WayChunk EventChunk {  get { return eventChunk; } }    
            /// <summary>
            /// ���� �� ������� ���������� �����, ��� ������� �������
            /// </summary>
            private WayChunk eventChunk;

            public LinearSequence TargetLinearSequence { get { return targetLinearSequence; } }
            /// <summary>
            /// ������ ����� ����, �� ������ ������� ����� ������ �� ��������� ��������� �� � ������
            /// </summary>
            private LinearSequence targetLinearSequence;

            public PlatformAdditionalInfo AdditionalInfo { get { return additionalInfo; } }
            private PlatformAdditionalInfo additionalInfo;
        }


        protected List<SFD.Texture> GetSFDTextureLine(int wayNumber, bool addDashAndX=false)
        {
            List<SFD.Texture> result = new List<SFD.Texture>();
            result.AddRange(GameStarter.Instance.SplitFlapDisplay.GetTexturesByInt(wayNumber));
            if (addDashAndX)
            {
                result.Add(SFD.Texture.dash);
                result.Add(SFD.Texture.x);
            }
            return result;
        }

        public  List<SFD.Texture> GetSFDTextureLine(int wayNumber, int wayNumber2)
        {
            List<SFD.Texture> result = GetSFDTextureLine(wayNumber);
            result.Add(SFD.Texture.dash);
            result.AddRange(GameStarter.Instance.SplitFlapDisplay.GetTexturesByInt(wayNumber2));
            return result;
        }


        protected void ProcessNoBarrierLine(List<WayChunk> wayChunks)
        {
            stuffSecondary.ProcessAndReopen(false);

            foreach (WayChunk wayChunk in wayChunks) 
            {
                stuffSecondary.Add(wayChunk);
            }
            stuffSecondary.ProcessAndReopen(false);
        }
    }
}