using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using WaySystem.Additions;

namespace WaySystem.Kreator
{
    public class Mail : AvoidableBlock
    {
        public Mail(Block targetBlock, KreatorShared kreatorShared)
               : base(targetBlock, kreatorShared, "Mail")
        {
            
        }

        public override void Process()
        {
            base.Process();
            AvoidFloorAndCeilingByHeights(ChunkSides.Both, 0);
            if (kShared.chunk.Dock.AngleX != 0)
            {
                var alignChunks = this.baseLine.BendUpOrDown(0, ChunkSides.Both);
                this.baseLine.AddBricks(ConstructionDirection.Flat, ChunkSides.Both, 8);
                Include(baseLine);
            }
            LinearSequence line = CreateLinearSequence(kShared.chunk, 20, ChunkSides.Both, ConstructionDirection.Flat, "mailLine");
            line.ProcessWayChunkBehaviours((wc) => { wc.WayChunk.SetOneSideFlip(); });

            Platform platformClone =
                GameObject.Instantiate<Platform>(kShared.collection.cargosSection.Platform, root);

            Cargos.CargoGenerators cargo = kShared.collection.cargosSection.Mail;
            Cargos.CargoGenerators cargoGeneratorsClone =
                    GameObject.Instantiate<Cargos.CargoGenerators>(cargo);
            platformClone.Length = cargoGeneratorsClone.PlatformLength;
            platformClone.Width = cargoGeneratorsClone.PlatformWidth;
            platformClone.transform.position = line.First.WayChunk.Transform.position;
            platformClone.Cargo = cargoGeneratorsClone;
            platformClone.InitAlignCargo(2);
            platformClone.gameObject.name = string.Format("mailPlatform");
            platformClone.SemaphoreLight(Semaphore3.Light.White);
            
            kShared.pickUpDistributor.Add(line);
            kShared.chunk = line.Last.WayChunk;
            Close();
        }
    }
}