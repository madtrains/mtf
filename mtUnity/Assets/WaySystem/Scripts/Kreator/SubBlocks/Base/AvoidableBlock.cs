using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.Kreator
{
    public class AvoidableBlock : SubBlock
    {
        /// <summary>
        /// ����� � �������� �� ��� (������) � ������ �� ����� ���� ������ ����� ���������� ������
        /// </summary>
        public static readonly int MIN_UNFLIPPABLE_CHUNKS_COUNT = 6;

        public static readonly float ONE_STEP_45 = Mathf.Sin(Mathf.Deg2Rad * 45);
        public static readonly float ONE_STEP_30 = Mathf.Sin(Mathf.Deg2Rad * 30);

        public static readonly float MIN_30 = 3.068525f;
        public static readonly float MIN_45 = 6.70836f;

        /// <summary>
        /// ���������� ������ ������ ��� ������ �� ���� � 30 ��������
        /// </summary>
        public static readonly int ROUND30 = 6;

        /// <summary>
        /// ���������� ������ ������ ��� ������ �� ���� � 45 ��������
        /// </summary>
        public static readonly int ROUND45 = 9;

        public enum Prediction { OK, High, Low, Near, Far }
        protected LinearSequence baseLine;

        public AvoidableBlock(Block targetBlock, KreatorShared kreatorShared, string name)
                : base(targetBlock, kreatorShared, name) {}

        public override void Process()
        {
            base.Process();
            baseLine = CreateLinearSequence(kShared.chunk);
            baseLine.Name = "baseLine";
        }

        /// <summary>
        /// ��������� ���� ������ �� ������� ������ �� �����
        /// </summary>
        /// <param name="wayChunk">���� �������</param>
        /// <param name="wallOffset">�������� ������ �� �����</param>
        /// <param name="oneStep">���������� ��������� �� ������ ������ ������� ����� ��� ������� ����</param>
        /// <param name="minOffset">���������� �������� �������� ���� � �������</param>
        /// <returns></returns>
        protected int WallOffsetCount(WayChunk wayChunk, float wallOffset, float oneStep, float minOffset)
        {
            //����� �����
            float wallX = kShared.enter.bkgPos.x;
            //����� ����
            float edgeX = wallX + wallOffset;
            
            float xAbsOffset =
                Mathf.Abs(wayChunk.Dock.Position.x - edgeX);

            //����������, ������� ������ ������� �����
            float offsetLeft = xAbsOffset - minOffset;
            
            int chunkCount = 0;
            if (offsetLeft >= 1)
            {
                chunkCount = Mathf.CeilToInt(offsetLeft / oneStep);
            }

            //�� ������ ��� ���� - ���� ��������� �����
            if (wayChunk.Dock.Position.x > edgeX)
                chunkCount *= -1;

            //���������� ������
            Log(1, 
                "Wall X: ", wallX, " Edge X: ", edgeX, 
                " Chunks count and direction by sign: ", chunkCount);
            return chunkCount;
        }

        protected Prediction AvoidFloorAndCeilingByHeights(ChunkSides chunkSides, int flatChunksCount, bool disableClearLine=false)
        {
            Prediction result = Prediction.OK;
            if (HEIGHTS.BelongsInclusive(kShared.chunk.Dock.Position.y))
                return result;

            float newAngle = 0;
            //������� �����, ���� ���� ������
            if (kShared.chunk.Dock.Position.y < HEIGHTS.Min)
            {
                newAngle = GetRandomLimitedAngle(
                    -90, -45, kShared.chunk.Dock.AngleX);
                result = Prediction.Low;
                Log(0, "Avoiding Floor by Height. Too LOW :", kShared.chunk.Dock.Position.y, " newAngle :", newAngle);
            }

            else if (kShared.chunk.Dock.Position.y > HEIGHTS.Max)
            {
                newAngle = GetRandomLimitedAngle(
                    45, 90, kShared.chunk.Dock.AngleX);
                result = Prediction.High;
                Log(0, "Avoiding Ceiling by Height. Too HIGH :", kShared.chunk.Dock.Position.y, " newAngle :", newAngle);
            }

            List<WayChunkBehaviour> bend = baseLine.BendUpOrDown(newAngle, chunkSides);
            //������
            //���������� ������
            if (chunkSides != ChunkSides.Both)
            {
                int leftChunks = MIN_UNFLIPPABLE_CHUNKS_COUNT - bend.Count;
                if (leftChunks  > 0) 
                {
                    List<WayChunkBehaviour> fixedAppend = 
                        baseLine.AddBricks(ConstructionDirection.Flat, chunkSides, leftChunks);
                    Log(0, "ChunkSides: ", chunkSides, " adding ", leftChunks, " flat chunks by Minimal Unflippable count");
                }
            }
            //���������� ������ ������
            if (flatChunksCount > 0)
            {
                Log(0, "Adding ", flatChunksCount, " flat chunks to avoidance bend");
                baseLine.AddBricks(ConstructionDirection.Flat, chunkSides, flatChunksCount);
            }

            //� �������
            if (chunkSides != ChunkSides.Both)
            {
                stuff.Add(baseLine);
                //��� ������� �� ���������� ������ ����� � �����
                if (!disableClearLine)
                {
                    Log(0, "Adding Clear Line");
                    baseLine.AddClearLine(kShared.clearLineLengthByTrainSize);
                }
            }
            if (chunkSides == ChunkSides.Both)
                kShared.pickUpDistributor.Add(baseLine);
            Include(baseLine);
            return result;
        }

        private void PredictAndAvoid(Vector3 size, Color color, ChunkSides chunkSides, bool addClearLineIfNeeded=true)
        {
            Prediction prediction = Predict(kShared.chunk.Dock, size, color);
            if (prediction == Prediction.High || prediction == Prediction.Low)
            {
                switch (prediction)
                {
                    case Prediction.High:
                    {
                        float correctionAngle = GetRandomLimitedAngle(
                            45f, 90f, kShared.chunk.Dock.AngleX);
                        baseLine.BendUpOrDown(correctionAngle, chunkSides);
                        Log(0, "Avoiding Ceiling by prediction. New angle ", correctionAngle);
                        break;
                    }
                    case Prediction.Low:
                    {
                        float correctionAngle = GetRandomLimitedAngle(
                            -90, -45f, kShared.chunk.Dock.AngleX);
                        baseLine.BendUpOrDown(correctionAngle, chunkSides);
                        Log(0, "Avoiding Floor by prediction. New angle ", correctionAngle);
                        break;
                    }
                }
            }
        }

        private Prediction Predict(Point point, Vector3 size, Color color, bool draw=false)
        {
            Matrix4x4 matrix = Matrix4x4.TRS(
                point.Position,
                point.Rotation,
                Vector3.one);
            Vector3 offsetedPosition = matrix.MultiplyPoint(Vector3.forward * (size.z / 2f));
            matrix = Matrix4x4.TRS(
                offsetedPosition,
                point.Rotation,
                Vector3.one);
            Bounds bounds = new Bounds(Vector3.zero, size);
            Bounds bounds2 = BoundedUnit.TransformBounds(matrix, bounds);
            if (draw)
            {
                MTDebug.DebugManager.CreatePrimitive(
                    color, "center", new Point(offsetedPosition, point.Rotation));
                DebugDrawBounds(bounds2, color);
            }

            if (bounds2.Intersects(kShared.collection.sceneUp))
            {
                Log(0, "Predicted High. Point: ", point.Position);
                return Prediction.High;
            }

            else if (bounds2.Intersects(kShared.collection.sceneDown))
            {
                Log(0, "Predicted Low. Point: ", point.Position);
                return Prediction.Low;
            }

            else
            {
                return Prediction.OK;
            }
        }

        private void DebugDrawBounds(Bounds bounds, Color color)
        {
            MTDebug.DebugManager.CreatePrimitive(
                    PrimitiveType.Cube, color, 0.35f,
                    bounds.center,
                    bounds.size, Quaternion.identity, true, this.root.name + "_boundCube");
        }
    }
}