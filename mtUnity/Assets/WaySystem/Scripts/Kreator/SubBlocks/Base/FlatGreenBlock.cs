namespace WaySystem.Kreator
{
    public class FlatGreenBlock : AvoidableBlock
    {
        public FlatGreenBlock(Block targetBlock, KreatorShared kreatorShared, string name)
               : base(targetBlock, kreatorShared, name)
        {
        }

        public override void Process()
        {
            base.Process();
            this.AvoidFloorAndCeilingByHeights(ChunkSides.RailsUp, 6, true);
            if (kShared.chunk.Dock.AngleX != 0)
            {
                var alignChunks = this.baseLine.BendUpOrDown(0, ChunkSides.RailsUp);
                stuff.Add(alignChunks);
                Include(baseLine);
                Log(0, "Flattening way in case of FlatGreenBlock");
            }
            baseLine.ProcessWayChunkBehaviours((wcb) =>
            {
                wcb.WayChunk.SetStoffActionType(StoffActionType.Nothing);
            });
            kShared.pickUpDistributor.Close();
        }

        
    }
}