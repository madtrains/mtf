using System.Collections.Generic;
using System.Text;
using UnityEngine;
using WaySystem.Additions.Switch;

namespace WaySystem.Kreator.SubBlocks
{
    public class RandomFork : FlatGreenBlock
    {
        public readonly static float WALL_X_MIN_LIMIT = -10f;
        public readonly static float WALL_X_MAX_LIMIT = 15f;

        public RandomFork(Block targetBlock, KreatorShared kreatorShared, 
            int bendChunksCount, int preForkLineLength, string name)
                : base(targetBlock, kreatorShared, name)
        {
            this.bendChunksCount = bendChunksCount;
            this.leftSide = UberBehaviour.RandomBool;
            this.firstDirection =
                leftSide ? ConstructionDirection.Left : ConstructionDirection.Right;
            this.secondDirection =
                leftSide ? ConstructionDirection.Right : ConstructionDirection.Left;
            this.preForkLineLength = preForkLineLength;
            this.eventAtBend = UberBehaviour.RandomBool;
        }

        protected int bendChunksCount;
        protected int preForkLineLength;
        protected bool leftSide;
        protected bool eventAtBend;
        protected ConstructionDirection firstDirection;
        protected ConstructionDirection secondDirection;
        protected ForkWayChunkBehaviour fork;
        protected LinearSequence preForkLine;
        protected Switch switcher;
        protected LinearSequence bendLine;
        protected LinearSequence straightLine;

        public override void Process()
        {
            base.Process();
            //������ � �����, ��� ������ ������
            if (kShared.chunk.Dock.Position.x < WALL_X_MIN_LIMIT)
            {
                this.leftSide = false;
                this.firstDirection = ConstructionDirection.Right;
                this.secondDirection = ConstructionDirection.Left;
            }

            else if (kShared.chunk.Dock.Position.x > WALL_X_MAX_LIMIT)
            {
                this.leftSide = true;
                this.firstDirection = ConstructionDirection.Left;
                this.secondDirection = ConstructionDirection.Right;
            }

            this.preForkLine =
                CreateLinearSequence(kShared.chunk, this.preForkLineLength, ChunkSides.RailsUp);
            Include(preForkLine);

            this.fork = Instantiate(
                kShared.collection.forksDoubleStraight.Get(leftSide).Fork,
                preForkLine.Dock);
            fork.Link(preForkLine.Last.WayChunk);
            stuff.Add(preForkLine);
            CreateForkSleeves();
        }

        protected virtual void CreateForkSleeves()
        {
            this.bendLine =
                CreateLinearSequence(
                    fork.Secondary, bendChunksCount, ChunkSides.RailsUp, this.firstDirection, "bendLine");
            bendLine.AddBricks(
                this.secondDirection, ChunkSides.RailsUp, bendChunksCount + 6);

            straightLine =
                CreateFillLine(fork.Primary,
                bendLine.Last.Dock.Position.z - fork.Primary.Dock.Position.z, "straightLine");

            //������ ������� - ������� � ���
            if (!this.leftSide)
            {
                stuff.Add(fork.Secondary);
                stuff.Add(bendLine);
            }
            //����� ������� - ������ ������ � ���
            else
            {
                stuff.Add(fork.Primary);
                stuff.Add(straightLine);
            }
        }

        /// <summary>
        /// ������ �������� 
        /// </summary>
        /// <param name="lastBendLineChunk">��������� ���� ����� � �������</param>
        /// <param name="lastStraightLineChunk">��������� ���� ������ �����</param>
        protected ConvergenceWayChunkBehaviour Convergence(
            WayChunk lastBendLineChunk, 
            WayChunk lastStraightLineChunk,
            bool processSecondaryStuff)
        {
            LinearSequence closeBendLine = CreateLinearSequence(lastBendLineChunk, this.bendChunksCount + 6, ChunkSides.RailsUp, this.secondDirection, "CloseBendLine");
            closeBendLine.AddBricks(
                    firstDirection, ChunkSides.RailsUp, this.bendChunksCount - 1);

            ConvergenceWayChunkBehaviour convergence =
                Instantiate(
                    kShared.collection.forksDoubleStraight.Get(this.leftSide).Convergence,
                    fork.DockPrimary);
            convergence.DockByBend(closeBendLine.Last.WayChunk.Dock, fork);
            //���������� ��������
            LinearSequence closeGreenLine =
                CreateFillLine(lastStraightLineChunk, lastStraightLineChunk.Dock.Distance(convergence.InPrimaryGlobal), "convergenceFillLine");

            
            convergence.Link(closeGreenLine.Last.WayChunk, closeBendLine.Last.WayChunk);

            //������ ������� - ������� � ���
            if (!this.leftSide)
            {
                stuff.Add(closeBendLine);
                stuff.Add(convergence.Secondary);
                if (processSecondaryStuff)
                {
                    stuffSecondary.Add(closeGreenLine);
                    stuffSecondary.Add(convergence.Primary);
                }
            }
            //����� ������� - ������ ������ � ���
            else
            {
                stuff.Add(closeGreenLine);
                stuff.Add(convergence.Primary);
                if (processSecondaryStuff)
                {
                    stuffSecondary.Add(closeBendLine);
                    stuffSecondary.Add(convergence.Secondary);
                }
            }
            return convergence;
        }

        protected LinearSequence CreateEndLine(ConvergenceWayChunkBehaviour convergence, int unflippablePartCount)
        {
            LinearSequence endLine =
                    CreateLinearSequence(convergence.Primary, unflippablePartCount, ChunkSides.RailsUp);
            stuff.Add(endLine);
            List<WayChunkBehaviour> clearPart = 
                endLine.AddClearLine(kShared.clearLineLengthByTrainSize);

            kShared.pickUpDistributor.Add(clearPart);
            kShared.chunk = endLine.Last.WayChunk;
            return endLine;
        }

        //kShared.collection.switchSources.numbers[1]
        /// <summary>
        /// ������ ������������� � ��������� ��� � ��������� � ���������
        /// </summary>
        /// <param name="convergence">��������</param>
        /// <param name="postConvergenceChunk">��������� ���� ����� ��������</param>
        /// <param name="iconSource1">������ ������� �������� �������������</param>
        /// <param name="iconSource2">������ ������� �������� �������������</param>
        protected void CreateSwitcher(ConvergenceWayChunkBehaviour convergence,
            WayChunk postConvergenceChunk,
            GameObject iconSource1, GameObject iconSource2)
        {
            this.switcher =
                Instantiate(
                    kShared.collection.switcher,
                    new Point(preForkLine.First.transform));
            
            SwitcherPosition one =
                new SwitcherPosition(iconSource1, () =>
                {
                    fork.Primary.Link(preForkLine.Last.WayChunk);
                    postConvergenceChunk.Link(convergence.Primary);
                }, true);

            SwitcherPosition two =
                new SwitcherPosition(iconSource2, () =>
                {
                    fork.Secondary.Link(preForkLine.Last.WayChunk);
                    postConvergenceChunk.Link(convergence.Secondary);
                }, true);

            //�������������
            this.switcher.Init(new List<SwitcherPosition>()
            {
                one, two
            }, this.preForkLineLength);
            switcher.SetRandomly();
        }

        protected void CreateSwitcher(
            GameObject iconSource1, GameObject iconSource2)
        {
            this.switcher =
                Instantiate(
                    kShared.collection.switcher,
                    new Point(preForkLine.First.transform));

            SwitcherPosition one =
                new SwitcherPosition(iconSource1, () =>
                {
                    fork.Primary.Link(preForkLine.Last.WayChunk);
                }, true);

            SwitcherPosition two =
                new SwitcherPosition(iconSource2, () =>
                {
                    fork.Secondary.Link(preForkLine.Last.WayChunk);
                }, true);

            //�������������
            this.switcher.Init(new List<SwitcherPosition>()
            {
                one, two
            }, this.preForkLineLength);
            switcher.SetRandomly();
        }

        protected WayChunk CreateObstacleLines(WayChunk target, int linesCount)
        {
            WayChunk currentTarget = target;
            for (int i = 0; i < linesCount; i++)    
            {
                Sequences.Sequence preset =
                     kShared.obstaclesWeightedSequence.GetRandomElement();

                ObstacleSequence obstacleSequence = CreateObstacleSequence(preset, currentTarget, flipped);
                currentTarget = obstacleSequence.Last.WayChunk;
            }
            return currentTarget;
        }

        protected void CreateAddObstacleLines(LinearSequence target, int count)
        {
            List<ObstacleSequence> lines = new List<ObstacleSequence>();
            WayChunk currentTarget = target.Last.WayChunk;
            for (int i = 0; i < count; i++)
            {
                Sequences.Sequence preset =
                     kShared.obstaclesWeightedSequence.GetRandomElement();

                ObstacleSequence obstacleSequence = 
                    CreateObstacleSequence(preset, currentTarget, flipped);
                currentTarget = obstacleSequence.Last.WayChunk;
                lines.Add(obstacleSequence);
            }
            for (int i = 0; i < lines.Count; i++)
            {
                StringBuilder name = new StringBuilder("obstacleLine_");
                target.Add(lines[i], name.Append(i).ToString());
            }  
        }
    }
}