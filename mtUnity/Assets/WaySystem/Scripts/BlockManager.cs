namespace WaySystem
{
    [System.Serializable]
    public class BlockManager : TrainControlManager
    {
        public BlockManager(WayRootFlipper wayRootFlipper, MTParameters.Flipper.FlipperParameters flipperParameters) : base()
        {
            this.wayRootFlipper = wayRootFlipper;
            this.flipperParameters = flipperParameters;
        }

        public float Z { get { return GetZ(); } }

        protected virtual float GetZ () { return 0; }


        protected  WayRootFlipper wayRootFlipper;
        public MTParameters.Flipper.FlipperParameters FlipperParameters { get { return this.flipperParameters; } }
        protected MTParameters.Flipper.FlipperParameters flipperParameters;
    }
}