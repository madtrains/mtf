using UnityEngine;

namespace WaySystem
{
    [System.Serializable]
    public class Point
    {
        public static Point Identity
        {
            get { return new Point(Vector3.zero, Quaternion.identity); }
        }
        public Vector3 Position { get { return position; } }
        public Quaternion Rotation { get { return rotation; } }

        public float AngleX
        {
            get
            {
                return PrettyAngle(rotation.eulerAngles.x);
            }
        }

        public Matrix4x4 Matrix
        { 
            get 
            { 
                return Matrix4x4.TRS(position, rotation, Vector3.one);
            } 
        }

        public Point Rounded
        {
            get
            {
                Quaternion rounded = RoundWithStep5(this.rotation);
                return new Point(this.position, rounded);
            }
        }

        [SerializeField] private Vector3 position;
        [SerializeField] private Quaternion rotation;

        public Point(Transform target)
        {
            this.position = target.position;
            this.rotation = target.rotation;
        }

        public Point(Vector3 position, Quaternion rotation)
        {
            this.position = position;
            this.rotation = rotation;
        }

        public static Point Lerp(Point a, Point b, float t)
        {
            Vector3 position = Vector3.Lerp(a.position, b.position, t);
            Quaternion rotation = Quaternion.Lerp(a.rotation, b.rotation, t);
            Point result = new Point(position, rotation);
            return result;
        }

        public bool IsEqual(Point other)
        {
            return this.position == other.position && this.rotation == other.rotation;
        }

        public Point Transformed(Matrix4x4 matrix)
        {
            Vector3 newPosition = matrix.MultiplyPoint(this.position);
            Quaternion newRotation = matrix.rotation * this.rotation;
            Point result = new Point(newPosition, newRotation);
            return result;
        }

        public Point Transformed(Transform transform)
        {
            return Transformed(transform.localToWorldMatrix);
        }

        public Point TransformOtherPoint(Point other)
        {
            return other.Transformed(this.Matrix);
        }

        public Vector3 Vector()
        {
            return Matrix4x4.TRS(Vector3.zero, rotation, Vector3.one).MultiplyVector(Vector3.forward);
        }

        public float Distance(Point other)
        {
            return Vector3.Distance(this.position, other.position);
        }

        public int FloorIntDistance(Point other)
        {
            float distacne = Distance(other);
            return Mathf.FloorToInt(distacne);
        }

        private float PrettyAngle(float value)
        {
            float floatValue = value;
            if (floatValue > 180)
            {
                floatValue = floatValue - 360;
            }

            int intValue = (int)floatValue;
            return intValue;
        }

        private int RoundWithStep5(float value)
        {
            float rounded = Mathf.Round(value / 5f);
            rounded = rounded * 5f;
            int result = Mathf.RoundToInt(rounded);
            return result;
        }

        private Quaternion RoundWithStep5(Quaternion target)
        {
            Vector3 euler = target.eulerAngles;
            euler = new Vector3(
                RoundWithStep5(euler.x),
                RoundWithStep5(euler.y),
                RoundWithStep5(euler.z));
            return Quaternion.Euler(euler);
        }
    }
}
