using MTTown;
using System.Collections.Generic;
using TutorialFlipper;
using UnityEngine;

namespace WaySystem
{
    public class WayRootTown : WayRoot 
    {
        [SerializeField] private WayLinker linker;

        public override void Prepare()
        {
            base.Prepare();
            this.wayChunks = linker.Link();
            FirstChunk = linker.First;
        }
    }
}