using UnityEngine;

namespace WaySystem
{
    public enum Prediction { OK, High, Low, Near, Far }

    public class BoundedUnit : UberBehaviour
    {
        public static Bounds MeshToBounds(Mesh mesh, Matrix4x4 m)
        {
            float xMin = System.Single.MaxValue;
            float xMax = System.Single.MinValue;

            float yMin = System.Single.MaxValue;
            float yMax = System.Single.MinValue;

            float zMin = System.Single.MaxValue;
            float zMax = System.Single.MinValue;

            for (int i = 0; i < mesh.vertexCount; i++)
            {
                Vector3 pos = mesh.vertices[i];
                pos = m.MultiplyPoint(pos);
                if (pos.x < xMin)
                    xMin = pos.x;
                if (pos.x > xMax)
                    xMax = pos.x;
                if (pos.y < yMin)
                    yMin = pos.y;
                if (pos.y > yMax)
                    yMax = pos.y;
                if (pos.z < zMin)
                    zMin = pos.z;
                if (pos.z > zMax)
                    zMax = pos.z;
            }
            float xCenter = (float)System.Math.Round(((xMin + xMax) / 2f), 3);
            float YCenter = (float)System.Math.Round(((yMin + yMax) / 2f), 3);
            float ZCenter = (float)System.Math.Round(((zMin + zMax) / 2f), 3);
            Vector3 center = new Vector3(xCenter, YCenter, ZCenter);

            float xSize = (float)System.Math.Round(xMax - xMin, 3);
            float ySize = (float)System.Math.Round(yMax - yMin, 3);
            float zSize = (float)System.Math.Round(zMax - zMin, 3);
            Vector3 size = new Vector3(xSize, ySize, zSize);
            Bounds bounds = new Bounds(center, size);
            return bounds;
        }

        public static Bounds TransformBounds(Transform targetTransform, Bounds bounds)
        {
            Vector3 center = targetTransform.TransformPoint(bounds.center);

            // transform the local extents' axes
            Vector3 extents = bounds.extents;
            Vector3 axisX = targetTransform.TransformVector(extents.x, 0, 0);
            Vector3 axisY = targetTransform.TransformVector(0, extents.y, 0);
            Vector3 axisZ = targetTransform.TransformVector(0, 0, extents.z);

            // sum their absolute value to get the world extents
            extents.x = Mathf.Abs(axisX.x) + Mathf.Abs(axisY.x) + Mathf.Abs(axisZ.x);
            extents.y = Mathf.Abs(axisX.y) + Mathf.Abs(axisY.y) + Mathf.Abs(axisZ.y);
            extents.z = Mathf.Abs(axisX.z) + Mathf.Abs(axisY.z) + Mathf.Abs(axisZ.z);
            Bounds transformed = new Bounds(center, extents * 2);
            return transformed;
        }

        public static Bounds TransformBounds(Matrix4x4 m, Bounds bounds)
        {
            Vector3 center = m.MultiplyPoint(bounds.center);
            Vector3 extents = bounds.extents;

            Vector3 axisX = m.MultiplyVector(new Vector3(extents.x, 0f, 0f));
            Vector3 axisY = m.MultiplyVector(new Vector3(0, extents.y, 0));
            Vector3 axisZ = m.MultiplyVector(new Vector3(0, 0, extents.z));
            extents.x = Mathf.Abs(axisX.x) + Mathf.Abs(axisY.x) + Mathf.Abs(axisZ.x);
            extents.y = Mathf.Abs(axisX.y) + Mathf.Abs(axisY.y) + Mathf.Abs(axisZ.y);
            extents.z = Mathf.Abs(axisX.z) + Mathf.Abs(axisY.z) + Mathf.Abs(axisZ.z);
            Bounds transformed = new Bounds(center, extents * 2);
            return transformed;
        }

        public Bounds[] Bounds { get { return bounds; } set { bounds = value; } }

        [Header("Bounds:")]
        [SerializeField] protected Bounds[] bounds;

        public Prediction Predict(
            Bounds up, Bounds down,
            Bounds left, Bounds right)
        {
            foreach (Bounds bds in bounds)
            {
                Prediction prediction =
                    CheckBounds(bds, up, down, left, right);
                if (prediction != Prediction.OK)
                    return prediction;
            }
            return Prediction.OK;
        }

        private Prediction CheckBounds(
            Bounds bounds,
            Bounds up, Bounds down,
            Bounds left, Bounds right)
        {
            if (bounds.Intersects(up)){
                return Prediction.High;
            }
            if (bounds.Intersects(down)){
                return Prediction.Low;
            }
            if (bounds.Intersects(left)){
                return Prediction.Near;
            }
            if (bounds.Intersects(right)){
                return Prediction.Far;
            }
            return Prediction.OK;
        }
    }
}