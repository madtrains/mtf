namespace WaySystem
{
    public class WayTeleport : UberBehaviour
    {
        public delegate void OnTeleportCommand();

        public void Link(OnTeleportCommand onTeleportCommand, WayChunk wayChunk)
        {
            this.linkedChunk = wayChunk;
            this.onTeleport = onTeleportCommand;
        }

        public WayChunk Chunk { get { return linkedChunk; } set { linkedChunk = value; } }
        private WayChunk linkedChunk;
        private OnTeleportCommand onTeleport;

        public void OnTeleport()
        {
            onTeleport.Invoke();
        }



    }
}