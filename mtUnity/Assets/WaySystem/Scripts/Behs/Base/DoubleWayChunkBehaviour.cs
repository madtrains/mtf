using UnityEngine;

namespace WaySystem
{
    public class DoubleWayChunkBehaviour : BaseChunkBehaviour
    {
        public WayChunk Primary
        { 
            get { return primary; }
            set { primary = value; }
        }

        public WayChunk Secondary
        {
            get { return secondary; }
            set { secondary = value; }
        }

        [SerializeField] protected WayChunk primary;
        [SerializeField] protected WayChunk secondary;


        protected override Point GetDock()
        {
            return primary.Dock;
        }

        public Point DockPrimary
        {
            get
            {
                return primary.Dock;
            }
        }

        public Point DockSecondary
        {
            get
            {
                return secondary.Dock;
            }
        }

        public override void Link(WayChunk previous)
        {
            primary.Link(previous);
        }

        private void OnDrawGizmos()
        {
            if (primary.Transform == null || primary.Points.Count == 0)
            {
                return;
            }

            Gizmos.color = Color.red;
            Gizmos.matrix = primary.Transform.localToWorldMatrix;
            Vector3 localCenter = Vector3.Lerp(primary.FirstPoint.Position, primary.LastPoint.Position, 0.5f);
            Gizmos.DrawWireCube(primary.FirstPoint.Position, new Vector3(0.3f, 0.3f, 0.3f));
        }
    }
}