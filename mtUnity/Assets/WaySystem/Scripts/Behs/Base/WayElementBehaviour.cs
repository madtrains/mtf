using System.Net.Http.Headers;
using UnityEngine;

namespace WaySystem
{
    /// <summary>
    /// ������� ����. ��� �������� ���������� ���� ����������� �� ����� ������
    /// </summary>
    public class WayElementBehaviour : UberBehaviour, IWayElement
    {

        public WayChunk LinkDock { get { return GetLinkDock(); } }

        public Point Dock { get { return GetDock(); } }


        protected virtual Point GetDock()
        {
            return new Point(transform);
        }

        public virtual void DockTo(Point point)
        {
            this.transform.position = point.Position;
            this.transform.rotation = point.Rotation;
        }

        protected virtual WayChunk GetLinkDock()
        {
            return null;
        }

        public virtual void Link(WayChunk previous)
        {

        }

        public virtual void Init()
        {

        }

        
    }
}