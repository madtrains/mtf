using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem
{
    public class BaseChunkBehaviour : WayElementBehaviour
    {
        public bool NeedStuff { get { return needStuff; } set { needStuff = value; } }
        [SerializeField] protected bool needStuff;

        public bool FlippedStuff { get { return flippedStuff; } }
        [SerializeField] protected bool flippedStuff;
    }
}