using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem
{
    public class WayChunkBehaviour : BaseChunkBehaviour
    {
        public WayChunk WayChunk
        { 
            get { return wayChunk; }
            set { wayChunk = value; }
        }

        [SerializeField] protected WayChunk wayChunk;

        protected override Point GetDock()
        {
            return wayChunk.Dock;
        }

        protected override WayChunk GetLinkDock()
        {
            return wayChunk;
        }

        public override void Link(WayChunk previous)
        {
            this.wayChunk.Link(previous);
        }

        private void OnDrawGizmos()
        {
            if (wayChunk.Transform == null || wayChunk.Points.Count == 0)
            {
                return;
            }

            Gizmos.color = Color.yellow;
            Gizmos.matrix = wayChunk.Transform.localToWorldMatrix;
            Vector3 localCenter = Vector3.Lerp(wayChunk.FirstPoint.Position, wayChunk.LastPoint.Position, 0.5f);
            Gizmos.DrawWireCube(wayChunk.FirstPoint.Position, new Vector3(0.3f, 0.3f, 0.3f));
        }
    }
}