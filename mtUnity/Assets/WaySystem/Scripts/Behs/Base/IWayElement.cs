namespace WaySystem
{

    public interface IWayElement
    {
        public WayChunk LinkDock { get; }

        public void Link(IWayElement previous) { this.Link(previous.LinkDock); }
        public void Link(WayChunk previous) { }

        public Point Dock { get; }
        public void DockTo(IWayElement target) { this.DockTo(target.Dock); }
        public void DockTo(Point dock);
    }

    public interface IAddition
    {
        public void DockTo(UnityEngine.Transform other) { }

        public void DockTo(WayChunk chunk) { }
    }
}
