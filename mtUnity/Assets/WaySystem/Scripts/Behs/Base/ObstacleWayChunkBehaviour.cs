using UnityEngine;

namespace WaySystem
{
    public class ObstacleWayChunkBehaviour : WayChunkBehaviour
    {
        public Transform ObstacleRoot { get { return obstacleRoot; } }
        public Transform[] Bricks { get { return bricks; } }

        [SerializeField] protected Transform obstacleRoot;
        [SerializeField] protected Transform[] bricks;


        public void FlipObstacle(bool target)
        {
            if (target != IsFlippedByTransform)
            {
                RotateFlipTransform();
            }
        }

        public bool IsFlippedByTransform
        {
            get
            {
                if (Mathf.Abs(obstacleRoot.localEulerAngles.z) < Mathf.Epsilon)
                {
                    //Debug.Log("Not Flipped");
                    return false;
                }
                else
                {
                    //Debug.Log("Flipped");
                    return true;
                }
            }
        }

        public void RotateFlipTransform()
        {
            if (IsFlippedByTransform)
            {
                obstacleRoot.localEulerAngles = Vector3.zero;
                return;
            }
            else
            {
                obstacleRoot.localEulerAngles =
                    new Vector3(0, 0, -180);
                return;
            }
        }
    }
}