using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace WaySystem
{
    public class SharedWayChunkBehaviourEditor : UberEditor
    {
        public static readonly Color[] DrawColors = { Color.yellow, Color.red, Color.black };

        public static void DrawWayChunk(Transform transform, WayChunk wayChunk, Color color)
        {
            if (transform == null)
                return;

            if (wayChunk.Transform == null || wayChunk.Points.Count == 0)
            {
                Handles.matrix = transform.localToWorldMatrix;
                Handles.color = Color.blue;
                Handles.CubeHandleCap(0, transform.position, transform.rotation, 0.4f, EventType.Repaint);
                return;
            }

            Handles.matrix = wayChunk.Transform.localToWorldMatrix;
            Handles.color = color;
            Handles.Label(wayChunk.FirstPoint.Position, wayChunk.Transform.name);
            Handles.SphereHandleCap(0, wayChunk.FirstPoint.Position, wayChunk.FirstPoint.Rotation, 0.2f, EventType.Repaint);
            Handles.ArrowHandleCap(0, wayChunk.FirstPoint.Position, wayChunk.FirstPoint.Rotation, 0.4f, EventType.Repaint);
            for (int i = 1; i < wayChunk.Points.Count; i++)
            {
                WayPoint previous = wayChunk.Points[i - 1];
                WayPoint current = wayChunk.Points[i];
                Handles.DrawLine(previous.Position, current.Position);
                Handles.SphereHandleCap(0, current.Position, current.Rotation, 0.2f, EventType.Repaint);
                Handles.ArrowHandleCap(0, current.Position, current.Rotation, 0.4f, EventType.Repaint);
                Handles.Label(current.Position, i.ToString());
            }
            Vector3 upFirst = wayChunk.FirstPoint.Position + Vector3.up * 0.5f;
            Vector3 upLast = wayChunk.LastPoint.Position + Vector3.up * 0.5f;
            if (wayChunk.Previous != null)
            {
                Handles.Label(upFirst, wayChunk.Previous.Transform.name);
            }
            else
            {
                Handles.SphereHandleCap(0, upFirst, wayChunk.FirstPoint.Rotation, 0.1f, EventType.Repaint);
            }
            if (wayChunk.Next != null)
            {
                Handles.Label(upLast, wayChunk.Next.Transform.name);
            }
            else
            {
                Handles.SphereHandleCap(0, upLast, wayChunk.LastPoint.Rotation, 0.1f, EventType.Repaint);
            }
        }

        public static bool Match(string reg, Transform transform)
        {
            Regex regex = new Regex(reg);
            MatchCollection matchCollection = regex.Matches(transform.name);
            if (matchCollection.Count > 0)
            {
                return true;
            }
            return false;
        }

        public static Way WayByTransformsChildren(Transform root, bool transformsPivotAsStart)
        {
            Transform[] children = UberBehaviour.GetChildren(root);
            if (children == null || children.Length == 0)
            {
                Debug.LogErrorFormat("Children Array is null or length is 0 : {0}", root.name);
                Selection.activeGameObject = root.gameObject;
                return null;
            }

            Point end = null;
            List<Point> points = new List<Point>();
            if (transformsPivotAsStart)
                points.Add(new Point(root));

            foreach (Transform child in children)
            {
                if (Match(@"^point\d+", child))
                {
                    Point wp = new Point(child);
                    points.Add(wp);
                    Debug.LogFormat("Point : {0} added as point", child.position);
                }

                else if (Match("^end", child))
                {
                    end = new Point(child);
                    Debug.LogFormat("Point : {0} defined as end", child.position);
                }
            }

            if (end != null)
                points.Add(end);

            Way result = new Way(points);
            return result;
        }

        public static string HelpString
        {
            get
            {
                System.Text.StringBuilder help = new System.Text.StringBuilder();
                help.Append("For Way Creation");
                help.Append("\nUse Object with children:");
                help.Append("\npoint1, point2...");
                help.Append("\nend");
                help.Append("\npoints must be ordered manualy");
                return help.ToString();
            }
        }

        [SerializeField] protected bool transformsPivotAsStart = false;
        [SerializeField] protected Transform root;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            transformsPivotAsStart =
                EditorGUILayout.Toggle(
                    "Transform`s Pivot as Start Point", transformsPivotAsStart);
            EditorGUILayout.HelpBox(HelpString, MessageType.Info);
        }
    }

    [CustomEditor(typeof(WayChunkBehaviour))]
	public class WayChunkBehaviourEditor : SharedWayChunkBehaviourEditor
    {
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
            WayChunkBehaviour wayChunkBehaviour = target as WayChunkBehaviour;
            EditorGUILayout.BeginHorizontal();
            root =
                EditorGUILayout.ObjectField(root, typeof(Transform), true) as Transform;
            string text = root == null ? "Crete as NEVER from root transform" : "Crete Way from " + root.name;

            if (GUILayout.Button(text))
            {
                if (root == null)
                {
                    root = wayChunkBehaviour.transform;
                    wayChunkBehaviour.WayChunk.FlipType = WayChunkFlipType.Never;

                }
             
                if (wayChunkBehaviour.WayChunk.Transform == null)
                    wayChunkBehaviour.WayChunk.Transform = wayChunkBehaviour.transform;
                if (wayChunkBehaviour.WayChunk.FlipTransform == null)
                    wayChunkBehaviour.WayChunk.FlipTransform = wayChunkBehaviour.transform;
                Way way = 
                    WayByTransformsChildren(root, transformsPivotAsStart);
                wayChunkBehaviour.WayChunk.AssignNewWay(way);
                
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Separator();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append("Points :");
			sb.Append(wayChunkBehaviour.WayChunk.Points.Count);
            if (wayChunkBehaviour.WayChunk.Points.Count > 0)
			{
                Vector3 vector = wayChunkBehaviour.Dock.Vector();
                sb.Append("\nVector: ");
                sb.Append(vector);
                sb.Append("\nAngleX: ");
                sb.Append(wayChunkBehaviour.Dock.AngleX);
                sb.Append("\nVector Angles: ");
                sb.Append(Quaternion.LookRotation(vector).eulerAngles);
                sb.Append("\nPrevious: ");
				if (wayChunkBehaviour.WayChunk.Previous != null)
					sb.Append(wayChunkBehaviour.WayChunk.Previous.Transform.name);
                sb.Append("\nNext: ");
                if (wayChunkBehaviour.WayChunk.Next != null)
                    sb.Append(wayChunkBehaviour.WayChunk.Next.Transform.name);

                sb.Append("\nWay Length: ");
                sb.Append(wayChunkBehaviour.WayChunk.Length);   
            }
            

            EditorGUILayout.HelpBox(sb.ToString(), MessageType.Info);
		}

		protected override void OnSceneGUI(SceneView sceneView)
		{
			base.OnSceneGUI(sceneView);
            WayChunkBehaviour wayChunkBehaviour = target as WayChunkBehaviour;
            WayChunk wayChunk = wayChunkBehaviour.WayChunk;
            DrawWayChunk(wayChunk.Transform, wayChunk, DrawColors[0]);
			ProcessSnapKey();
		}

		protected virtual void ProcessSnapKey()
		{
			Event e = Event.current;
			switch (e.type)
			{
				case EventType.KeyDown:
					{
						if (Event.current.keyCode == (KeyCode.J))
						{
							Snap();
						}
					}
					break;
			}
		}

		protected void DrawError()
        {
			Transform transform = (target as GameObject).transform;
			Handles.color = Color.red;
			Handles.SphereHandleCap(
					0,
					transform.position,
					transform.rotation,
					0.5f,
					EventType.Repaint);
		}

		protected void OnButonMakeLinks()
		{
			WayChunkBehaviour wayChunkBehaviour = target as WayChunkBehaviour;
			wayChunkBehaviour.WayChunk.Transform = wayChunkBehaviour.transform;
		}

		protected void Snap()
		{
			WayElementBehaviour me = target as WayElementBehaviour;
			WayElementBehaviour[] others = FindObjectsOfType<WayElementBehaviour>();
			foreach (WayElementBehaviour other in others)
			{
				if (other == me)
					continue;

				WayChunkBehaviour wayChunkBehaviour = other as WayChunkBehaviour;
				if (wayChunkBehaviour == null)
					continue;

                float distance = Vector3.Distance(me.transform.position, wayChunkBehaviour.Dock.Position);
                Debug.Log(distance);
                if (distance < 0.5f)
                {
                    me.transform.position = wayChunkBehaviour.Dock.Position;
                    me.transform.rotation = wayChunkBehaviour.Dock.Rotation;
                    break;
                }
            }
		}

		



	}

	[CustomEditor(typeof(ObstacleWayChunkBehaviour))]
	public class ObstacleWayChunkBehaviourEditor : WayChunkBehaviourEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
			ObstacleWayChunkBehaviour flippable = target as ObstacleWayChunkBehaviour;
			if (flippable.ObstacleRoot != null)
			{
				if (GUILayout.Button("Rotate FlipTransform"))
				{
					flippable.RotateFlipTransform();
				}
			}
		}
    }

    public class DoubleWayChunkBehaviourEditor : SharedWayChunkBehaviourEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            DoubleWayChunkBehaviour doubleWayChunkBehaviour =
                target as DoubleWayChunkBehaviour;

            EditorGUILayout.BeginHorizontal();
            root =
                EditorGUILayout.ObjectField(root, typeof(Transform), true) as Transform;
            if (GUILayout.Button("Create Primary Way"))
            {
                if (root != null)
                {
                    if (doubleWayChunkBehaviour.Primary.Transform == null)
                        doubleWayChunkBehaviour.Primary.Transform = doubleWayChunkBehaviour.transform;
                    Way way =
                        WayByTransformsChildren(root, transformsPivotAsStart);
                    doubleWayChunkBehaviour.Primary.AssignNewWay(way);
                }
            }
            if (GUILayout.Button("Create Secondary Way"))
            {
                if (root != null)
                {
                    if (doubleWayChunkBehaviour.Secondary.Transform == null)
                        doubleWayChunkBehaviour.Secondary.Transform = doubleWayChunkBehaviour.transform;
                    Way way =
                        WayByTransformsChildren(root, transformsPivotAsStart);
                    doubleWayChunkBehaviour.Secondary.AssignNewWay(way);
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        protected override void OnSceneGUI(SceneView sceneView)
        {
            base.OnSceneGUI(sceneView);
            DoubleWayChunkBehaviour doubleWayChunkBehaviour = 
                target as DoubleWayChunkBehaviour;
            DrawWayChunk(doubleWayChunkBehaviour.transform, doubleWayChunkBehaviour.Primary, Color.yellow);
            DrawWayChunk(doubleWayChunkBehaviour.transform, doubleWayChunkBehaviour.Secondary, Color.red);
        }
    }

    [CustomEditor(typeof(ForkWayChunkBehaviour))]
    public class ForkWayChunkBehaviourEditor : DoubleWayChunkBehaviourEditor
    {
    }

    [CustomEditor(typeof(ConvergenceWayChunkBehaviour))]
    public class ConvergenceWayChunkBehaviourEditor : DoubleWayChunkBehaviourEditor
    {
    }
}