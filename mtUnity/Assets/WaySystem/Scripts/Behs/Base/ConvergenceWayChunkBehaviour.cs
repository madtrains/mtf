using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem
{
    public class ConvergenceWayChunkBehaviour : DoubleWayChunkBehaviour
    {

        public void DockByBend(Point dock, ForkWayChunkBehaviour fork)
        {
            Vector3 offset = dock.Position + this.SecondaryOffset;
            Point convergenceDock = new Point(offset, fork.Dock.Rotation);
            this.DockTo(convergenceDock);
        }
        
        public Vector3 SecondaryOffset
        {
            get
            {
                return primary.Points[0].Position -
                        secondary.Points[0].Position;
            }
        }

        public Point InPrimaryGlobal { get { return primary.FirstPoint.Transformed(primary.Transform); } }
        public Point InSecondaryGlobal { get { return secondary.FirstPoint.Transformed(secondary.Transform); } }

        public void Link(WayChunk primary, WayChunk secondary)
        {
            this.primary.Link(primary);
            this.secondary.Link(secondary);
        }
    }
}