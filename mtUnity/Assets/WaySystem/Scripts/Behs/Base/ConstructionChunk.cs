using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem
{
    public enum ConstructionDirection { Flat, Left, Right, Up, Down }
    public enum ChunkSides { Both, RailsUp, RailsDown }
    public class ConstructionChunk : UberBehaviour
    {
        public ChunkContent GetChunkContent(ConstructionDirection direction)
        {
            switch(direction) 
            {
                case ConstructionDirection.Left:
                    return left;
                case ConstructionDirection.Right:
                    return right;
                case ConstructionDirection.Up:
                    return up;
                case ConstructionDirection.Down:
                    return down;
                default:
                    return flat;
            }
        }
        public ChunkContent flat;
        public ChunkContent left;
        public ChunkContent right;
        public ChunkContent up;
        public ChunkContent down;
        public Bounds bounds;
    }

    [System.Serializable]
    public class ChunkContent
    {
        public Transform end;
        public Transform tie;
        public UpDownPair rails;
        public UpDownPair grass;
    }

    [System.Serializable]
    public class UpDownPair
    {
        public Transform up;
        public Transform down;
    }
}