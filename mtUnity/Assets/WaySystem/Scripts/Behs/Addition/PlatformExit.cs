using UnityEngine;
using WaySystem.Cargos;

namespace WaySystem.Additions
{
    public class PlatformExit : UberBehaviour, IAddition
    {
        public Switch.Switch Switcher { get { return switcher; } }
        [SerializeField] private Switch.Switch switcher;

        public CargoPassengers Passengers { get { return passengers; } }
        [SerializeField] private CargoPassengers passengers;

        public Transform MainCharacter { get { return mainCharacter; } }
        [SerializeField] private Transform mainCharacter;

        [SerializeField] private UberColorizer[] colorizers;

        public TimerBeh Timer { get { return timer; } }
        [SerializeField] private TimerBeh timer;

        public CargoGenerators FreightReloader { get => freightReloader; }
        [SerializeField] private CargoGenerators freightReloader;

        public void Prepare()
        {
            if (passengers.Collection != null)
                passengers.Prepare();
        }

        public void DockTo(Transform other)
        {
            this.transform.position = other.position;
            this.transform.rotation = other.rotation;
            foreach (UberColorizer uc in this.colorizers)
            {
                uc.SetRandomColor();
            }
        }

        public void DockTo(WayChunk chunk)
        {
            this.transform.position = chunk.Dock.Position;
            this.transform.rotation = chunk.Dock.Rotation;
            foreach (UberColorizer uc in this.colorizers)
            {
                uc.SetRandomColor();
            }
        }

        public Transform SfdRoot { get { return sfdRoot; } }

        [SerializeField] private Transform sfdRoot;
    }
}