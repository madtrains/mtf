using MTRails;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem
{
    public class StuffPhysBehaviour : StuffBehaviour
    {
        public RRCollision Collision 
        { 
            get 
            {
                if (!flipped)
                    return collisionDefault;
                else
                    return collisionFlipped;
            }
        }

        [SerializeField] private bool flipped;
        [SerializeField] private RRCollision collisionDefault;
        [SerializeField] private RRCollision collisionFlipped;

        protected override void SetFlipped(bool value)
        {
            this.flipped = value;
            Activate(collisionDefault.gameObject, !flipped);
            Activate(collisionFlipped.gameObject, flipped);
        }
    }
}