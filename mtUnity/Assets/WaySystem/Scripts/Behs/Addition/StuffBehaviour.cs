using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem
{
    public class StuffBehaviour : BoundedUnit
    {
        public Transform[] Transforms { get { return transforms; } }

        [SerializeField] protected Transform[] transforms;

        protected virtual void SetFlipped(bool value)
        {
            foreach (Transform tr in transforms)
            {
                if (!value)
                {
                    tr.Rotate(Vector3.right * 180);
                }
                //flipped
                else
                {

                }
            }
        }

        public bool Flip { set { SetFlipped(value); } }
    }
}