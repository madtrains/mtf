using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;
using MTLightNCamera;

namespace WaySystem.Additions
{
	[CustomEditor(typeof(Platform))]
	public class PlatformEditor : UberEditor
	{
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            Platform platform = target as Platform;
            if (platform == null)
            {
                EditorGUILayout.HelpBox(string.Format("NO Platform"), MessageType.Error);
                return;
            }

            platform.Length = UberSlider("Length: ", platform.Length, UberSliderType.asInt, 5, 36);
            platform.Width = UberSlider("Width: ", platform.Width, UberSliderType.asInt, 2, 36);
        }
    }
}