using UnityEngine;

namespace WaySystem.Additions.Switch
{
    public class SwitchSources : UberBehaviour
    {
        public GameObject arrow;
        public GameObject town;
        public GameObject cross;
        public GameObject[] resources;
        public GameObject[] numbers;
    }
}
