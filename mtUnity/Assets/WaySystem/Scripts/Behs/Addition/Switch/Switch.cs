using System.Collections.Generic;
using Triggers;
using UnityEngine;

namespace WaySystem.Additions.Switch
{
    public delegate void OnSwitchDelegate();

    public class Switch : MTCore.RayCastTap
    {
        public UnityEngine.Events.UnityAction OnFirstSwtich;

        [SerializeField] protected Transform arrow;
        [SerializeField] protected ContentHolder onDisableHide;
        [SerializeField] protected Slot[] slots;
        [SerializeField] protected AudioSource arrowAudioSource;
        [SerializeField] protected TrainTrigger triggerDisable;
        private List<Slot> switchSlots;
        protected int currentSwitchIndex = -1;

        public bool VisualSwitchOnly { set { visualSwitchOnly = value; } }

        protected bool disabled;
        protected bool visualSwitchOnly;
        bool wasSwitched = false;

        public override void Tap()
        {
            base.Tap();
            if (disabled)
                return;

            currentSwitchIndex++;
            if (currentSwitchIndex >= switchSlots.Count)
            {
                currentSwitchIndex = 0;
            }
            MakeSwitch(true);
        }

        public void SetRandomly()
        {
            currentSwitchIndex = Random.Range(0, switchSlots.Count);
            MakeSwitch(false);
        }

        private void MakeSwitch(bool sound)
        {
            if(!wasSwitched)
            {
                wasSwitched = true;
                OnFirstSwtich?.Invoke();
            }

            if (sound)
                arrowAudioSource.Play();

            Slot targetSwitchSlot = switchSlots[currentSwitchIndex];
            SetArrow(targetSwitchSlot);

            if (visualSwitchOnly)
                return;

            targetSwitchSlot.OnSwitch?.Invoke();
        }

        public void SetArrow(Slot slot)
        {
            arrow.transform.localEulerAngles =
                Vector3.right * slot.Angle;
        }

        public virtual void Init(
            List<SwitcherPosition> switcherInfos,
            float triggerDisableOffset)
        {
            triggerDisable.Init();
            triggerDisable.OnTrain += (train) =>
            {
                Disable();
            };
            triggerDisable.transform.localPosition =
                Vector3.forward * triggerDisableOffset;

            switchSlots = new List<Slot>();
            for (int i = 0; i < switcherInfos.Count; i++)
            {
                
                if (switcherInfos[i].isClickable)
                {
                    slots[i].Init(switcherInfos[i].iconSource, switcherInfos[i].@delegate);
                    switchSlots.Add(slots[i]);
                    slots[i].OnDirectTap = DirectTap;
                }
                else
                {
                    slots[i].Init(switcherInfos[i].iconSource);
                }
            }
        }

        public void Disable()
        {
            disabled = true;
            onDisableHide.IsActive = false;
        }

        public void Enable()
        {
            disabled = false;
            onDisableHide.IsActive = true;
        }

        public void DirectTap(Slot slot)
        {
            if (disabled)
                return;

            Debug.LogWarningFormat("Direct Tap to Slot :#{0}", slot.Index);
            if (currentSwitchIndex != slot.Index)
            {
                currentSwitchIndex = slot.Index;
                MakeSwitch(true);
            }
            else
            {
                currentSwitchIndex++;
                if (currentSwitchIndex >= switchSlots.Count)
                {
                    currentSwitchIndex = 0;
                }
                MakeSwitch(true);
            }
        }
    }


    [System.Serializable]
    public class Slot
    {
        public UnityEngine.Events.UnityAction<Slot> OnDirectTap;
        public float Angle { get { return angle; } }
        public Transform SlotTransform { get { return slot; } }

        public OnSwitchDelegate OnSwitch { get { return @delegate; } }
        public int Index { get { return index; } }


        [SerializeField] Transform slot;
        [SerializeField] float angle;
        [SerializeField] private OnSwitchDelegate @delegate;
        [SerializeField] private MTCore.RayCastTap tap;
        [SerializeField] private int index;

        public void Init(GameObject iconSource)
        {
            UberBehaviour.Activate(
                GameObject.Instantiate<GameObject>(iconSource, slot),
                true);

            if (tap != null)
                tap.OnTap = () => { OnDirectTap?.Invoke(this); };
        }

        public void Init(GameObject iconSource, OnSwitchDelegate @delegate)
        {
            Init(iconSource);
            this.@delegate = @delegate;
        }
    }

    public class SwitcherPosition
    {
        public SwitcherPosition(GameObject iconSource, OnSwitchDelegate @delegate, bool isClickable)
        {
            this.iconSource = iconSource;
            this.@delegate = @delegate;
            this.isClickable = isClickable;
        }
        public GameObject iconSource;
        public OnSwitchDelegate @delegate;
        public bool isClickable;
    }
}
