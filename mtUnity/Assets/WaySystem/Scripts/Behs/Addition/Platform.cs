using MTTown;
using System.Collections.Generic;
using UnityEngine;

namespace WaySystem.Additions
{
    public class Platform : UberBehaviour
    {
        public int CargoResourceID { get { return cargo == null ? -1 : cargo.ResourceID; } }

        public bool Empty { get { return cargo == null; } }

        public Cargos.Cargo Cargo { get { return cargo; } set { cargo = value; } }

        public float BoardingLength { get { return Length - 0.8f; } }
        public CamDock CamDock { get { return camDock; } }

        public float UnloadPercentage { get { return unloadPercentage; } set { unloadPercentage = value; } }

        public PlatformAdditionalInfo AdditionalInfo { get { return addInfo; } set { addInfo = value; } }

        [SerializeField] private Cargos.Cargo cargo;
        [SerializeField] private bool isOpened;
        [SerializeField] private CamDock camDock;
        [SerializeField] private Semaphore3[] semaphore3;
        [SerializeField] private Transform[] lengthPoints;
        [SerializeField] private Transform[] widthPoints;
        /// <summary>
        /// ������ ���� ���������, ����� ��� ��� ����� ����� �� �������
        /// </summary>
        [SerializeField] private Transform offset;

        /// <summary>
        /// ������� ��� ������������ ���� ������ �� �������
        /// </summary>
        [SerializeField] private Transform camDockDepth;
        [SerializeField] private SkinnedMeshRenderer skinnedMeshRenderer;
        [SerializeField] private float unloadPercentage;
        [SerializeField] private Vector3 dockOffset;
        [SerializeField] private PlatformAdditionalInfo addInfo;

        public float Width
        {
            get
            {
                float value = widthPoints[0].transform.localPosition.x;
                return -value;
            }
            set
            {
                foreach (Transform transform in widthPoints)
                {
                    transform.localPosition = new Vector3(-value, transform.localPosition.y, transform.localPosition.z);
                }
                Set();
            }
        }

        public float Length
        {
            get
            {
                return lengthPoints[0].transform.localPosition.z;
            }
            set
            {
                foreach (Transform transform in lengthPoints)
                {
                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, value);
                }
                Set();
            }
        }


        public Point CalculatePlatformFloor()
        {
            /*
            BoxCollider boxCollider = tap.GetComponent<BoxCollider>();
            Vector3 position = boxCollider.transform.localToWorldMatrix.MultiplyPoint(new Vector3(boxCollider.center.x, 1f, boxCollider.center.z));
            return new Point(position, transform.rotation);
            */
            return null;
        }

        public bool IsOpened
        { 
            get { return isOpened; }
            set { SetOpen(value); }
        }

        public void InitAlignCargo(int resourceID)
        {
            if (cargo == null)
                return;
            cargo.transform.position = transform.position;
            cargo.transform.rotation = transform.rotation;
            cargo.Init(resourceID);
        }

        public void Prepare()
        {
            if (cargo == null)
                return;
            cargo.Prepare();
        }

        public void Off()
        {
            SemaphoreLight(Semaphore3.Light.Off);
        }

        protected virtual void SetOpen(bool value)
        {
            isOpened = value;
            SemaphoreLight(value ? Semaphore3.Light.Green : Semaphore3.Light.Red);
        }

        public void SemaphoreLight(Semaphore3.Light light)
        {
            foreach (var semaphore in semaphore3)
            {
                semaphore.Activate(light);
            }
        }

        private void Set()
        {
            Bounds old = skinnedMeshRenderer.localBounds;
            Bounds boundsNew =
                new Bounds(
                    new Vector3(-Width / 2f - 1, old.center.y, -Length / 2f),
                    new Vector3(Width / 2f + 1, old.size.y, Length / 2f));
            skinnedMeshRenderer.ResetLocalBounds();
            skinnedMeshRenderer.localBounds = boundsNew;
            CamDock.transform.position = new Vector3(
                camDockDepth.transform.position.x,
                offset.transform.position.y,
                offset.transform.position.z + Length / 2f) + this.dockOffset;
        }

        private void OnDrawGizmos()
        {
            Vector3 offsetPos = offset.position + new Vector3(-Width / 2f, 1f, Length / 2f);
            Gizmos.color = Color.green;
            Gizmos.DrawWireCube(offsetPos, new Vector3(-Width, 1, Length));
            Gizmos.color = Color.blue;
            Gizmos.DrawWireCube(offsetPos, new Vector3(-Width +0.45f, 1, Length - 0.45f));
        }
    }

    [System.Serializable]
    public class PlatformAdditionalInfo
    {
        public PlatformAdditionalInfo(int wayNumber, List<SFD.Texture> sfdLine, float previousLineLength)
        {
            this.wayNumber = wayNumber;
            this.sfdLine = sfdLine;
            this.previousLineLength = previousLineLength;
            //this.leaveLength = leaveLength;

        }
        /// <summary>
        /// ����� ���� �� ������� ����� ��� ���������
        /// </summary>
        public int WayNumber { get { return wayNumber; } }
        private int wayNumber;

        /// <summary>
        /// ���������� �� �����
        /// </summary>
        public List<SFD.Texture> SFDLine { get { return sfdLine; } }
        private List<SFD.Texture> sfdLine;

        public float PreviousLineLength { get { return previousLineLength; } }
        private float previousLineLength;

        public float OnLinePositionOffset { get { return onLinePositionOffset; } set { onLinePositionOffset = value; } }
        private float onLinePositionOffset;
    }
}