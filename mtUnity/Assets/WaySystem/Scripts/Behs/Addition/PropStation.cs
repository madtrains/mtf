using UnityEngine;

namespace WaySystem.Additions
{
    public class PropStation : UberBehaviour
    {
        public float Length { get { return length; } }
        [SerializeField] private float length;
        [SerializeField] private UberColorizer[] colorizers;

        public void Init()
        {
            foreach (UberColorizer uc in this.colorizers)
            {
                uc.SetRandomColor();
            }
        }
    }
}