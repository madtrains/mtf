using System.Collections.Generic;
using UnityEngine;

namespace WaySystem
{
    [System.Serializable]
    public class WayLinker
    {
        public WayChunk First { get { return first.WayChunk; } }

        [SerializeField] private TownLine[] lines;
        [SerializeField] private WayChunkBehaviour first;


        public List<WayChunk> Link()
        {
            List<WayChunk> wayChunks = new List<WayChunk>();

            foreach (TownLine line in lines)
            {
                line.Link();
            }

            return wayChunks;
        }
    }

    [System.Serializable]
    public class TownLine
    {
        public void Link()
        {
            if (elements.Length < 2)
                return;

            for (int i = 1; i < elements.Length; i++)
            {
                LinkEl current = elements[i];
                LinkEl previous = elements[i-1];
                current.Link(previous);
            }
        }
        [SerializeField] private string name;
        [SerializeField] private LinkEl[] elements;
    }


    [System.Serializable]
    public class LinkEl
    {
        public enum LinkMode { WayChunkBehaviour, DoublePrimary, DoubleSecondary }

        public void Link(LinkEl previous)
        {
            WayChunk.Previous = previous.WayChunk;
            previous.WayChunk.Next = WayChunk;
        }

        public WayChunk WayChunk { get { return GetWayChunk(); } }

        private WayChunk GetWayChunk()
        {
            switch (mode)
            {
                case (LinkMode.WayChunkBehaviour):
                {
                    return (wayElementBehaviour as WayChunkBehaviour).WayChunk;
                }

                case (LinkMode.DoublePrimary):
                {
                    return (wayElementBehaviour as DoubleWayChunkBehaviour).Primary;
                }

                case (LinkMode.DoubleSecondary):
                {
                    return (wayElementBehaviour as DoubleWayChunkBehaviour).Secondary;
                }

            }
            return null;
        }

        [SerializeField] private WayElementBehaviour wayElementBehaviour;
        [SerializeField] private LinkMode mode;

        /*
        { fork.Primary.Link(preForkLine.Last.WayChunk);
                        //������ ����� �����
                        fork2.Primary.Link(preFork2Line.Last.WayChunk);}
        */
    }
}