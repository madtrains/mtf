using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;
using System;

namespace WaySystem
{
	[CustomEditor(typeof(BoundedUnit))]
	public class BoundedUnitEditor : UberEditor
	{
		protected GameObject mainRefObject;

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			EditorGUILayout.BeginHorizontal();
			mainRefObject = EditorGUILayout.ObjectField(mainRefObject, typeof(GameObject), true) as GameObject;
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.HelpBox("Bounds: ", MessageType.Info);
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("Add Bounds by Target Mesh")) 
			{
				OnButtonAddBoundsByMesh(); 
			}
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			EditorGUILayout.Separator();
		}

		protected override void OnSceneGUI(SceneView sceneView)
		{
			base.OnSceneGUI(sceneView);
			DrawScene();
		}

		protected virtual void DrawScene()
		{
			BoundedUnit boundedUnit = target as BoundedUnit;
			Handles.color = Color.yellow;
			Handles.matrix = boundedUnit.transform.localToWorldMatrix;
			if (boundedUnit.Bounds == null)
            {
				Debug.LogError("No Bounds");
				return;
			}
				
			foreach (Bounds bound in boundedUnit.Bounds)
			{
				if (bound.size != Vector3.zero)
				{

					Handles.DrawWireCube(bound.center, bound.size);
				}
			}
			Handles.matrix = new Matrix4x4();
		}


		protected void OnButtonAddBoundsByMesh()
		{
            if (mainRefObject == null)
			{
				Debug.LogErrorFormat("Ref object for bounds creation is null");
				return;
			}
            BoundedUnit boundedUnit = target as BoundedUnit;
			List<Bounds> boundsList = new List<Bounds>(boundedUnit.Bounds);
            Mesh mesh = SearchMesh(mainRefObject);
            if (mesh != null)
            {
                Bounds bounds = MeshToBounds(mesh, boundedUnit.transform.localToWorldMatrix);
                boundsList.Add(bounds);
            }

			if (boundsList.Count > 0)
			{
				boundedUnit.Bounds = boundsList.ToArray();
			}
		}

        protected void OnButtonCubeBounds()
        {
            BoundedUnit boundedUnit = target as BoundedUnit;
            Transform lastChild = boundedUnit.transform.GetChild(boundedUnit.transform.childCount - 1);
            Vector3 center = lastChild.localPosition;
            Vector3 scale = lastChild.localScale;
            Bounds b = new Bounds(
                new Vector3(
                    (float)Math.Round(center.x, 1),
                    (float)Math.Round(center.y, 1),
                    (float)Math.Round(center.z, 1)),
                new Vector3(
                    (float)Math.Round(scale.x, 1),
                    (float)Math.Round(scale.y, 1),
                    (float)Math.Round(scale.z, 1))
                );
            boundedUnit.Bounds = new Bounds[] { b };
        }

        protected Mesh SearchMesh(GameObject go)
        {
			MeshFilter meshFilter = go.GetComponent<MeshFilter>();
			if (meshFilter != null && meshFilter.sharedMesh != null)
				return meshFilter.sharedMesh;
            else
            {
                for (int i = 0; i < go.transform.childCount; i++)
                {
					Transform child = go.transform.GetChild(i);
					meshFilter = go.GetComponent<MeshFilter>();
					if (meshFilter != null && meshFilter.sharedMesh != null)
						return meshFilter.sharedMesh;
				}
            }
			return null;
		}

		protected Bounds MeshToBounds(Mesh mesh, Matrix4x4 m)
		{
			float xMin = System.Single.MaxValue;
			float xMax = System.Single.MinValue;

			float yMin = System.Single.MaxValue;
			float yMax = System.Single.MinValue;

			float zMin = System.Single.MaxValue;
			float zMax = System.Single.MinValue;

			for (int i = 0; i < mesh.vertexCount; i++)
			{
				Vector3 pos = mesh.vertices[i];
				pos = m.MultiplyPoint(pos);
				if (pos.x < xMin)
					xMin = pos.x;
				if (pos.x > xMax)
					xMax = pos.x;
				if (pos.y < yMin)
					yMin = pos.y;
				if (pos.y > yMax)
					yMax = pos.y;
				if (pos.z < zMin)
					zMin = pos.z;
				if (pos.z > zMax)
					zMax = pos.z;
			}
			float xCenter = (float) System.Math.Round(((xMin + xMax) / 2f), 3);
			float YCenter = (float)System.Math.Round(((yMin + yMax) / 2f), 3);
			float ZCenter = (float)System.Math.Round(((zMin + zMax) / 2f), 3);
			Vector3 center = new Vector3(xCenter, YCenter, ZCenter);

			float xSize = (float) System.Math.Round(xMax - xMin, 3);
			float ySize = (float) System.Math.Round(yMax - yMin, 3);
			float zSize = (float) System.Math.Round(zMax - zMin, 3);
			Vector3 size = new Vector3(xSize, ySize, zSize);
			Bounds bounds = new Bounds(center, size);
			return bounds;
		}

		protected Bounds Combine(Bounds a, Bounds b)
		{
			float xMin = a.min.x;
			if (b.min.x < a.min.x)
				xMin = b.min.x;

			float yMin = a.min.y;
			if (b.min.y < a.min.y)
				yMin = b.min.y;

			float zMin = a.min.z;
			if (b.min.z < a.min.z)
				zMin = b.min.z;


			float xMax = a.max.x;
			if (b.max.x > a.max.x)
				xMax = b.max.x;

			float yMax = a.max.y;
			if (b.max.y > a.max.y)
				yMax = b.max.y;

			float zMax = a.max.z;
			if (b.max.z > a.max.z)
				zMax = b.max.z;

			float xCenter = (xMin + xMax) / 2f;
			float YCenter = (yMin + yMax) / 2f;
			float ZCenter = (zMin + zMax) / 2f;
			Vector3 center = new Vector3(xCenter, YCenter, ZCenter);

			float xSize = xMax - xMin;
			float ySize = yMax - yMin;
			float zSize = zMax - zMin;
			Vector3 size = new Vector3(xSize, ySize, zSize);
			Bounds bounds = new Bounds(center, size);
			return bounds;
		}


	}
}