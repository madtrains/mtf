using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;

namespace WaySystem
{
	[CustomEditor(typeof(AssemblieLine))]
	public class AssemblieLineEditor : UberEditor
	{
        public AssemblieLine AssemblieLine { get { return target as AssemblieLine; } }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Collect Line"))
            {
                OnButtonCollectLine();
            }
            if (GUILayout.Button("Align"))
            {
                OnButtonAlign();
            }
        }

        private void OnButtonCollectLine()
        {
            AssemblieLine assemblieLine = target as AssemblieLine;
            Transform root = assemblieLine.transform;
            List<WayChunkBehaviour> lst = new List<WayChunkBehaviour>();
            for (int i = 0; i < root.childCount; i++)
            {
                Transform child = root.GetChild(i);
                WayChunkBehaviour wayElementBehaviour = child.GetComponent<WayChunkBehaviour>();
                if (wayElementBehaviour != null) 
                {
                    lst.Add(wayElementBehaviour);
                }
                else
                {
                    Debug.LogErrorFormat("Not WayChunkBehaviour under AssemblieLine {0} {1}", child.name, assemblieLine.name);
                }
            }
            assemblieLine.AssignList(lst);
        }

        private void OnButtonAlign()
        {
            AssemblieLine assemblieLine = target as AssemblieLine;
            assemblieLine.Align();
        }

        protected void DrawScene()
        {
            int a = 0;
            foreach (var item in AssemblieLine.List)
            {
                Handles.matrix = Matrix4x4.identity;
                Handles.Label(item.transform.position, a.ToString());
                //WayChunkBehaviourEditor.DrawWayChunk(item.WayChunk);
                a += 1;
            }
        }
    }
}