using UnityEditor;

namespace WaySystem
{
    [CustomEditor(typeof(WayElementBehaviour))]
	public class WayElementBehaviourEditor : BoundedUnitEditor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
		}

		protected override void OnSceneGUI(SceneView sceneView)
		{
			base.OnSceneGUI(sceneView);
		}
	}
}