using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;
using WaySystem.Kreator;

namespace WaySystem
{
    public class AssemblieLine : WayElementBehaviour
    {
       
        public WayChunkBehaviour[] List { get { return list; } }
        [SerializeField] private WayChunkBehaviour[] list;

        protected override Point GetDock()
        {
            return list[list.Length -1].Dock;
        }

        public void AssignList(List<WayChunkBehaviour> lst)
        {
            this.list = lst.ToArray();
        }

        public void Align()
        {
            if (list.Length < 2)
                return;
            for (int i = 1; i < list.Length; i++)
            {
                WayChunkBehaviour previous = list[i - 1];
                WayChunkBehaviour next = list[i];
                next.transform.position = previous.Dock.Position;
                next.transform.rotation = previous.Dock.Rotation;
            }
        }

        public void LinkElements()
        {
            if (list.Length < 2)
                return;

            for (int i = 1; i < list.Length; i++)
            {
                WayChunkBehaviour previous = list[i-1];
                WayChunkBehaviour next = list[i];
                previous.WayChunk.Next = next.WayChunk;
                next.WayChunk.Previous = previous.WayChunk;
            }
        }

        public void DestroyBehaviours()
        {
            foreach(WayChunkBehaviour wcb in  list)
            {
                Destroy(wcb);
            }
        }

        public override void Link(WayChunk previous)
        {
            this.list[0].WayChunk.Link(previous);
        }
    }
}