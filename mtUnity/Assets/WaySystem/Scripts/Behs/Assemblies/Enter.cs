using MTTown;
using UnityEngine;

namespace WaySystem
{
    public class Enter : Assembly
    {
        public Vector3 bkgPos { get { return bkgPoint.position; } }
        public Triggers.Semaphore Semaphore { get { return semaphore; } }
        public AssemblieLine Line { get { return line; } }
        public CamDock CamDock { get { return camDock; } }
        public float TrainStartOffset { get { return trainStartOffset; } }
        public int CameraEventChunkIndex { get { return cameraEventChunkIndex; } }
        public float CameraEventLength { get { return cameraEventLength; } }

        [SerializeField] private Transform bkgPoint;
        [SerializeField] private Triggers.Semaphore semaphore;
        [SerializeField] private AssemblieLine line;
        [SerializeField] private CamDock camDock;
        [SerializeField] private float trainStartOffset;
        [SerializeField] private int cameraEventChunkIndex;
        [SerializeField] private float cameraEventLength;

        public WayChunkBehaviour LastWayChunkBehaviour {get { return line.List[line.List.Length - 1]; }}

        public WayChunkBehaviour GetLineChunk(int index)
        {
            return line.List[index];
        }

        public override void Init()
        {
            base.Init();
            line.LinkElements();
        }
    }
}