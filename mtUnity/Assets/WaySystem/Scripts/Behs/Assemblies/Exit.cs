using MTTown;
using UnityEngine;

namespace WaySystem
{
    public class Exit : Assembly
    {
        public Vector3 BkgPos { get { return bkgPoint.position; } }
        public ForkWayChunkBehaviour Fork { get { return fork; } }
        public WayChunkBehaviour EventChunkExit { get { return lastExit; } }
        public WayChunkBehaviour EventChunkContinue { get { return lastContinue; } }

        public CamDock LeaveCam { get { return leaveCam; } }

        [SerializeField] private Transform bkgPoint;
        [SerializeField] private WayChunkBehaviour lastExit;
        [SerializeField] private WayChunkBehaviour lastContinue;

        [SerializeField] private ForkWayChunkBehaviour fork;
        [SerializeField] private CamDock leaveCam;
        [SerializeField] private AssemblieLine primaryLine;
        [SerializeField] private AssemblieLine secondaryLine;
        protected override Point GetDock()
        {
            return lastExit.WayChunk.Dock;
        }

        public override void Init()
        {
            base.Init();
            primaryLine.LinkElements();
            secondaryLine.LinkElements();
            primaryLine.Link(fork.Primary);
            secondaryLine.Link(fork.Secondary);
        }
        

        public void DestroyBehaviours()
        {
            primaryLine.DestroyBehaviours();
            secondaryLine.DestroyBehaviours();
        }
    }
}