using MTCore;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WaySystem.Additions;
using WaySystem.Additions.Switch;

namespace WaySystem
{
    [System.Serializable]
    public class StationManager : BlockManager
    {
        public StationManager(
            WayRootFlipper wayRootFlipper,
            MTParameters.Flipper.FlipperParameters flipperParameters,
            TripSize size)
                : base(wayRootFlipper, flipperParameters)
        {
            this.size = size;
        }

        /// <summary>
        /// �������� ����� � ��������
        /// </summary>
        public UnityEngine.Events.UnityAction OnStationBreaking;

        /// <summary>
        /// �������� ����� � �� �������, ����� ���-�� ������ � ������� � ����������
        /// </summary>
        public UnityEngine.Events.UnityAction OnDisplayLine;


        /// <summary>
        /// ���� � ���������
        /// </summary>
        public UnityEngine.Events.UnityAction<Platform> OnWayToPlatforms;


        /// <summary>
        /// �������� �������������
        /// </summary>
        public UnityEngine.Events.UnityAction<Platform> OnSwitchersPassed;

        public Transform Sfd { get { return sfd; } set { sfd = value; } }

        public MTParameters.Flipper.Station StationParameters { get { return this.flipperParameters.Stations[(int)this.size]; } }
        public int WayNumber { get { return currentWayNumber; } set 
        { 
            currentWayNumber = value;
            //Debug.LogWarningFormat("Station switched to way: {0}", value);
        }}

        /// <summary>
        /// ������������ ������ �� �������� ��� ���� ������� �� ���������� ���������� �� ��������
        /// </summary>
        /// <param name="stationIndex"></param>
        /// <param name="totalStationsCount"></param>
        public void SetUnloadLimits(int stationIndex, int totalStationsCount)
        {
            float step = (float)totalStationsCount;
            step = 1f / (step - 2f);

            foreach(KeyValuePair<int, Platform> pair in resourceAndPlatforms)
            {
                switch(pair.Key)
                {
                    //������
                    case ((int)Trains.LOADS_ENUM.PASSENGERS):
                    {

                        float unloadPercentage = 0;
                        if (stationIndex > 0)
                        {
                            unloadPercentage =
                                Mathf.Lerp(
                                    GameStarter.GameParameters.FlipperParameters.PassengerMinimalExitPercentage, 
                                    1f,
                                    ((stationIndex - 1) * step));
                        }
                        pair.Value.UnloadPercentage = unloadPercentage;
                        break;
                    }

                    //�����
                    case ((int)Trains.LOADS_ENUM.FREIGHT):
                    {
                        /*
                        float unloadPercentage = (float)totalStationsCount;
                        unloadPercentage = 1f / (unloadPercentage - 1f);
                        pair.Value.UnloadPercentage = unloadPercentage;
                        */
                        //����� ����� ���������� ������� ������� ���������
                        pair.Value.UnloadPercentage = 1f;
                        break;
                    }
                }
            }
        }

        public float UnloadPercentage 
        {
            get { return unloadPercentage; }
            set 
            { 
                //���������� ���������� ����� �� �������� �� ���� ������
                unloadPercentage = value;
                
            }}

        [SerializeField] private Transform sfd;
        [SerializeField] private float unloadPercentage;
        [SerializeField] private Dictionary<int, Platform> resourceAndPlatforms;
        [SerializeField] private Dictionary<int, Platform> wayNPlatforms;
        [SerializeField] private TripSize size;
        [SerializeField] private int currentWayNumber;

        public void SwitchersPassed()
        {

            Platform platform = (wayNPlatforms ?? new Dictionary<int, Platform>()).ContainsKey(currentWayNumber) ? 
                wayNPlatforms[currentWayNumber] : null;
            OnSwitchersPassed?.Invoke(platform);
        }

        public void PlatormEventChunk()
        {
            OnWayToPlatforms?.Invoke(wayNPlatforms.ContainsKey(currentWayNumber) ? wayNPlatforms[currentWayNumber] : null);
        }

        public void AddPlatform(Platform platform)
        {
            if (wayNPlatforms == null)
                wayNPlatforms = new Dictionary<int, Platform>();
            if (resourceAndPlatforms == null)
                resourceAndPlatforms = new Dictionary<int, Platform>();

            wayNPlatforms.Add(platform.AdditionalInfo.WayNumber, platform);
            if (!platform.Empty)
            {
                resourceAndPlatforms[platform.CargoResourceID] = platform;
            }
        }

        public Platform GetPlatform(int resourceID)
        {
            return resourceAndPlatforms.ContainsKey(resourceID) ?
                resourceAndPlatforms[resourceID] : null;
        }

        public void Prepare()
        {
            GameStarter.Instance.SplitFlapDisplay.CloseAll(true);
            foreach (KeyValuePair<int, Platform> keyValuePair in wayNPlatforms)
            {
                keyValuePair.Value.Prepare();
            }
            GameStarter.Instance.SplitFlapDisplay.Dock(this.sfd, StationParameters.SfdAssemble);
        }

        public void Show()
        {
            var keys = wayNPlatforms.Keys.ToList();
            keys.Sort();
            for (int i = 0; i < keys.Count; i++)
            {
                int key = keys[i];
                int resourceID = wayNPlatforms[key].Empty ? -1 : wayNPlatforms[key].CargoResourceID;
                GameStarter.Instance.SplitFlapDisplay.ShowStation(
                    i, wayNPlatforms[key].AdditionalInfo.SFDLine, resourceID);
            }
        }

        protected override float GetZ()
        {
            return sfd.transform.position.z;
        }
    }


    public class SwitcherManager
    {
        public enum Switcher { Primary, Secondary }


        public virtual void Randomize()
        {

        }

        

        protected virtual void Check()
        {

        }
    }

    public class SingleSwitcherManager : SwitcherManager
    {
        public Action<Switcher> OnSwitch;

        protected Switcher main;

        private Switch switcher;

        public SingleSwitcherManager(
            Switch switchSource, Point point,
            GameObject iconSource1, 
            GameObject iconSource2,
            float disableDistance)
        {
            this.switcher = GameObject.Instantiate(
                switchSource,
                point.Position,
                point.Rotation);

            SwitcherPosition sw1pos1 = new SwitcherPosition(
                iconSource1,
                () =>
                {
                    main = SwitcherManager.Switcher.Primary;
                    Check();
                }, true);

            SwitcherPosition sw1pos2 = new SwitcherPosition(
                iconSource2,
                () =>
                {
                    main = SwitcherManager.Switcher.Secondary;
                    Check();
                }, true);

            switcher.Init(new List<SwitcherPosition>() { sw1pos1, sw1pos2 },
                disableDistance);
        }

        protected override void Check()
        {
            base.Check();
            OnSwitch?.Invoke(main);
        }

        public override void Randomize()
        {
            base.Randomize();
            this.switcher.SetRandomly();
        }
    }

    public class DoubleSwitcherManager : SwitcherManager
    {
        public Action<Switcher, Switcher> OnSwitch;
        private Switch switcher1;
        private Switch switcher2;
        [SerializeField] private Switcher first;
        [SerializeField] private Switcher second;


        public DoubleSwitcherManager(
            Switch switchSource, Point point, Point point2,
            GameObject iconSource1, GameObject iconSource2,
            GameObject iconSource3, GameObject iconSource4,
            float disableDistance)
        {
            this.switcher1 = GameObject.Instantiate(
                switchSource,
                point.Position,
                point.Rotation);

            this.switcher2 = GameObject.Instantiate(
                switchSource,
                point2.Position,
                point2.Rotation);

            SwitcherPosition sw1pos1 = new SwitcherPosition(
                iconSource1,
                () =>
                {
                    first = SwitcherManager.Switcher.Primary;
                    Check();
                }, true);

            SwitcherPosition sw1pos2 = new SwitcherPosition(
                iconSource2,
                () =>
                {
                    first = SwitcherManager.Switcher.Secondary;
                    Check();
                }, true);

            switcher1.Init(new List<SwitcherPosition>() { sw1pos1, sw1pos2 },
                disableDistance);

            SwitcherPosition sw2pos1 = new SwitcherPosition(
                iconSource3,
                () =>
                {
                    second = SwitcherManager.Switcher.Primary;
                    Check();
                }, true);

            SwitcherPosition sw2pos2 = new SwitcherPosition(
                iconSource4,
                () =>
                {
                    second = SwitcherManager.Switcher.Secondary;
                    Check();
                }, true);

            switcher2.Init(new List<SwitcherPosition>() { sw2pos1, sw2pos2 },
                disableDistance);
        }

        public override void Randomize()
        {
            base.Randomize();
            this.switcher1.SetRandomly();
            this.switcher2.SetRandomly();
        }

        protected override void Check()
        {
            base.Check();
            OnSwitch?.Invoke(first, second);
        }
    }

    public class TripleSwitcherManager : SwitcherManager
    {
        public Action<Switcher, Switcher, Switcher> OnSwitch;
        private Switch switcher1;
        private Switch switcher2;
        private Switch switcher3;
        [SerializeField] private Switcher first;
        [SerializeField] private Switcher second;
        [SerializeField] private Switcher third;



        public TripleSwitcherManager(
            Switch switchSource, Point point, Point point2, Point point3,
            GameObject iconSource1, GameObject iconSource2, 
            GameObject iconSource3, GameObject iconSource4, 
            GameObject iconSource5, GameObject iconSource6,
            float disableDistance)
        {
            this.switcher1 = GameObject.Instantiate(
                switchSource,
                point.Position,
                point.Rotation);

            this.switcher2 = GameObject.Instantiate(
                switchSource,
                point2.Position,
                point2.Rotation);

            this.switcher3 = GameObject.Instantiate(
                switchSource,
                point3.Position,
                point3.Rotation);

            SwitcherPosition sw1pos1 = new SwitcherPosition(
                iconSource1,
                () =>
                {
                    first = SwitcherManager.Switcher.Primary;
                    Check();
                }, true);

            SwitcherPosition sw1pos2 = new SwitcherPosition(
                iconSource2,
                () =>
                {
                    first = SwitcherManager.Switcher.Secondary;
                    Check();
                }, true);

            switcher1.Init(new List<SwitcherPosition>() { sw1pos1, sw1pos2 },
                disableDistance);

            SwitcherPosition sw2pos1 = new SwitcherPosition(
                iconSource3,
                () =>
                {
                    second = SwitcherManager.Switcher.Primary;
                    Check();
                }, true);

            SwitcherPosition sw2pos2 = new SwitcherPosition(
                iconSource4,
                () =>
                {
                    second = SwitcherManager.Switcher.Secondary;
                    Check();
                }, true);

            switcher2.Init(new List<SwitcherPosition>() { sw2pos1, sw2pos2 },
                disableDistance);

            SwitcherPosition sw3pos1 = new SwitcherPosition(
                iconSource5,
                () =>
                {
                    third = SwitcherManager.Switcher.Primary;
                    Check();
                }, true);

            SwitcherPosition sw3pos2 = new SwitcherPosition(
                iconSource6,
                () =>
                {
                    third = SwitcherManager.Switcher.Secondary;
                    Check();
                }, true);

            switcher3.Init(new List<SwitcherPosition>() { sw3pos1, sw3pos2 },
                disableDistance);
        }

        public override void Randomize()
        {
            base.Randomize();
            this.switcher1.SetRandomly();
            this.switcher2.SetRandomly();
            this.switcher3.SetRandomly();
        }

        protected override void Check()
        {
            base.Check();
            OnSwitch?.Invoke(first, second, third);
        }
    }
}