using MTTown;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using WaySystem.Additions;
using WaySystem.Cargos;

namespace WaySystem
{
    public class WayRootFlipper : WayRoot
    {
        public delegate void ProcessStationManager(StationManager stationManager);
        public CamDock CamDock { get { return cameraDock; } set { cameraDock = value; } }

        public int BlockManagersCount { get { return blockManagers.Count; } }

        public Passenger[] PassengersCollection { get { return collection; } }

        [SerializeField] private List<BlockManager> blockManagers;
        [SerializeField] private List<StationManager> stationManagers;
        [SerializeField] private ExitManager exitManager;
        [SerializeField] private BackGround.BackGround backGround;
        
        [SerializeField] private CamDock cameraDock;
        private List<float> wayProgressDots;
        private Passenger[] collection;

        public void Init()
        {
            blockManagers = new List<BlockManager>();
            stationManagers = new List<StationManager>();
        }    



        public void SetTrainZ(float z)
        {
            for (int i = 1; i < wayProgressDots.Count; i++)
            {
                if (z < wayProgressDots[i])
                {
                    float lerped =
                        Mathf.InverseLerp(wayProgressDots[i - 1], wayProgressDots[i], z);
                    GameStarter.Instance.UIRoot.Flipper.SetWayProgress(i - 1, lerped);
                    break;
                }
            }
        }

        public void AssignPassengerCollection(Passenger[] collection)
        {
            this.collection = collection;
        }

        public void ProcessStationManagers(ProcessStationManager process)
        {
            foreach(StationManager stationManager in stationManagers)
            {
                process(stationManager);
            }
        }

        public void AddBackGround(BackGround.BackGround backGround)
        {
            this.backGround = backGround;
        }

        public void Add(BlockManager blockManager)
        {
            blockManagers.Add(blockManager);
        }

        public void Add(StationManager stationManager)
        {
            blockManagers.Add(stationManager);
            stationManagers.Add(stationManager);
        }

        public void Add(ExitManager exitManager)
        {
            blockManagers.Add(exitManager);
            this.exitManager = exitManager;
        }

        public void Close()
        {
            wayProgressDots = new List<float>();
            wayProgressDots.Add(0f);
            for (int i = 0; i < blockManagers.Count; i++)
            {
                wayProgressDots.Add(blockManagers[i].Z);
            }
        }
    }
}