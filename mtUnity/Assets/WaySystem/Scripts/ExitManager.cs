using EventChain;
using MTCore;
using MTTown;
using MTTown.Characters;
using System.Collections.Generic;
using UnityEngine;
using WaySystem.Additions;

namespace WaySystem
{
    [System.Serializable]
    public class ExitManager : BlockManager
    {
        public ExitManager(WayRootFlipper wayRootFlipper, MTParameters.Flipper.FlipperParameters flipperParameters) : base(wayRootFlipper, flipperParameters) { }
        public UnityEngine.Events.UnityAction<float> OnBreaking;
        public UnityEngine.Events.UnityAction OnTrainDepart;
        /// <summary>
        /// ���������� �����, ����� ���� � �������
        /// </summary>
        public UnityEngine.Events.UnityAction OnShow;
        public UnityEngine.Events.UnityAction OnFlipperRelaunch;
        public UnityEngine.Events.UnityAction OnFlipperExitToTown;

        protected override float GetZ()
        {
            return sfd.transform.position.z;
        }

        public Transform Sfd { get { return sfd; } set { sfd = value; } }
        [SerializeField] private Transform sfd;
        [SerializeField] private int currentWay;

        public MTParameters.Kreator.Exit ExitParameters { get { return exit; } set { exit = value; } }
        [SerializeField] private MTParameters.Kreator.Exit exit;

        public PlatformExit PlatformExit { get { return platformExit; } set { platformExit = value; } }
        [SerializeField] private PlatformExit platformExit;

        public StationMasterAlbert StationMaster { get { return stationMaster; } set { stationMaster = value; } }
        [SerializeField] private StationMasterAlbert stationMaster;

        [SerializeField] private Dictionary<int, Platform> wayNPlatforms;

        public CamDock LeaveCam { get { return leaveCam; } set { leaveCam = value; } }
        [SerializeField] private CamDock leaveCam;

        public FlipperExitChainSteps ExitChainSteps { get => exitChainSteps; }
        FlipperExitChainSteps exitChainSteps;


        public void Prepare()
        {
            GameStarter.Instance.SplitFlapDisplay.CloseAll(true);
            GameStarter.Instance.SplitFlapDisplay.Dock(this.sfd, GameStarter.GameParameters.SFDParameters.ExitFlipper);
            platformExit.Prepare();
            GameStarter.Instance.UIRoot.PostFlipper.OnWhistleButton = WhistleButtonTap;
            exitChainSteps = new FlipperExitChainSteps(this);
            GameStarter.Instance.UIRoot.PostFlipper.Whistle = false;
        }

        private void WhistleButtonTap()
        {
            OnTrainDepart?.Invoke();
            GameStarter.Instance.UIRoot.PostFlipper.Hide();
        }


        public class FlipperExitChainSteps : EventChain.Chain
        {
            public Dictionary<int, int> ResourcesToShowOnSFD { set => resourcesToShowOnSFD = value; }
            public bool IsTrainUnload { set => trainStopEvent.IsTrainUnload = value; }
            public bool IsTrainStopped { set => trainStopEvent.IsTrainStopped = value; }

            private ExitManager exitManager;
            private Dictionary<int, int> resourcesToShowOnSFD;
            private TrainStop trainStopEvent;
            private SingleConditionStep sfd;
            private SingleConditionStep hideFlipperUI;
            private SingleConditionStep postFlipperUI;
            private Step capibara;
            private SingleConditionStep switcher;

            public FlipperExitChainSteps(ExitManager exitManager) : base()
            {
                this.exitManager = exitManager;
                trainStopEvent = new TrainStop();
                Add(trainStopEvent);
                //���������� ��������, ����� �����������
                trainStopEvent.OnDone = () =>
                {
                };

                hideFlipperUI = new EventChain.SingleConditionStep(() =>
                {
                    GameStarter.Instance.UIRoot.Flipper.Hide();
                    GameStarter.Instance.UIRoot.Flipper.OnHidden = () =>
                    {
                        hideFlipperUI.�ondition = true;
                        GameStarter.Instance.UIRoot.Flipper.OnHidden = null;
                    };
                });
                Add(hideFlipperUI);

                sfd = new EventChain.SingleConditionStep(() =>
                {
                    //��������� ���� �� ��� �� �����
                    //���������� �� ����� ��� ������� ��������� ��
                    //��� �������,
                    //� �� ������ �� �������
                    //�� ����� ������������ ���������� �����
                    bool resultsAreGatheredAndHaveToBeShown = false;
                    int lineIndex = 0;
                    GameStarter.Instance.UIRoot.IterateOrderedUIResources((orderedUIresource) =>
                    {
                        if(lineIndex < GameStarter.GameParameters.SFDParameters.ExitFlipper.Size.Y)
                        {
                            //���� � ������ � ������� / ��������� ������ ����
                            if(resourcesToShowOnSFD.ContainsKey(orderedUIresource.Parameters.ID) &&
                                resourcesToShowOnSFD[orderedUIresource.Parameters.ID] > 0)
                            {
                                GameStarter.Instance.SplitFlapDisplay.ShowFlipperResultsLine(
                                        lineIndex,
                                        orderedUIresource.Parameters.ID, //������
                                        resourcesToShowOnSFD[orderedUIresource.Parameters.ID]); //����������
                                lineIndex += 1;
                                resultsAreGatheredAndHaveToBeShown = true;
                            }
                        }
                    });
                    if (resultsAreGatheredAndHaveToBeShown)
                    {
                        GameStarter.Instance.SplitFlapDisplay.Delay(4f, () => 
                        { 
                            this.sfd.�ondition = true;
                            GameStarter.Instance.SplitFlapDisplay.CloseAll(false);
                        });
                    }
                    else
                    {
                        this.sfd.�ondition = true;
                    }
                });
                Add(sfd);

                postFlipperUI = new EventChain.SingleConditionStep(() =>
                {
                    GameStarter.Instance.UIRoot.PostFlipper.Show();
                    GameStarter.Instance.UIRoot.PostFlipper.OnShown = () =>
                    {
                        postFlipperUI.�ondition = true;
                        GameStarter.Instance.UIRoot.PostFlipper.OnShown = null;
                        GameStarter.Instance.SplitFlapDisplay.SwtichDisplayToTripsMode(false);
                    };
                });
                Add(postFlipperUI);

                //switcher
                switcher = new SingleConditionStep(() =>
                {
                    UberBehaviour.Activate(exitManager.platformExit.Switcher.gameObject, true);
                    GameStarter.Instance.MainComponents.SoundManager.Shared.Play(MTSound.SharedNames.WipeIN);
					this.switcher.�ondition = true;
					exitManager.stationMaster.HideBubble();
				});
                Add(switcher);

                exitManager.platformExit.Switcher.OnFirstSwtich = () =>
                {
					GameStarter.Instance.UIRoot.PostFlipper.Whistle = true;
					exitManager.stationMaster.ShowUpdateBubble();
				};
                
                //show whistle button & check Capibaras
                //Step  �� ����������� ���
                Add(new Step(() => 
                {
					GameStarter.Instance.GamePlayManager.UpdateCollection(MTDataCollection.Finish_flipper);
					GameStarter.Instance.GamePlayManager.RunCheckers();
                }));
            }

            private class TrainStop : EventChain.ConditionStep
            {
                public bool IsTrainUnload { set { isTrainUnloaded = value; CheckRunNext(); } }
                public bool IsTrainStopped { set { isTrainStopped = value; CheckRunNext(); } }

                private bool isTrainUnloaded;
                private bool isTrainStopped;

                protected override bool AreConditionsAccomplished()
                {
                    if(isTrainUnloaded && isTrainStopped)
                    {
                        return true;
                    }
                    return false;
                }
            }

            private class SFD : EventChain.ConditionStep
            {
                public SFD(StepDelegate @delegate) : base(@delegate) { }

                public bool AreResultsShown { set { areResultsShown = value; CheckRunNext(); } }
                private bool areResultsShown;

                protected override bool AreConditionsAccomplished()
                {
                    if(areResultsShown)
                    {
                        return true;
                    }
                    return false;
                }
            }
        }
    }
}