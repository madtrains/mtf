using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

namespace WaySystem
{
    public enum WaySwitchOrder { Primary, Secondary, Tertiary }

    [System.Serializable]
    public class Way
    {

        public static bool IsNullOrEmpty(Way way)
        {
            if (way == null)
                return true;
            return way.Empty;
        }

        #region getters
        public WayPoint FirstPoint { get { return points[0]; } }
        public WayPoint LastPoint { get { return points[points.Count-1]; } }
        public List<WayPoint> Points { get { return points; } }
        public float Length { get { return length; } }
        public bool Empty
        {
            get
            {
                if (points == null)
                    return true;
                return !(points.Count >= 2);
            }
        }
        #endregion

        #region fields
        [SerializeField] protected List<WayPoint> points;
        [SerializeField] protected float length;
        #endregion

        #region Constructors
        public Way(List<Point> points)
        {
            length = 0;
            this.points = new List<WayPoint>();
            if (points.Count >= 2)
            {
                this.points.Add(new WayPoint(points[0].Position, points[0].Rotation, 0));
                for (int i = 1; i < points.Count; i++)
                {
                    Point previousPoint = points[i - 1];
                    Point currentPoint = points[i];
                    float stepDistance = currentPoint.Distance(previousPoint);
                    length += stepDistance;
                    this.points.Add(
                        new WayPoint(
                            currentPoint.Position, currentPoint.Rotation, length));
                }
            }
            else
                throw new InvalidWayException("Way should be created from at least 2 points");
        }
        #endregion

        public Point GetPointByOffset(float offset)
        {
            for (int i = 1; i < points.Count; i++)
            {
                if (offset < points[i].Offset)
                {
                    float offsetPrevious = points[i - 1].Offset;
                    float offsetNext = points[i].Offset;
                    float t = Mathf.InverseLerp(offsetPrevious, offsetNext, offset);
                    Point result = Point.Lerp(points[i - 1], points[i], t);
                    return result;
                }
            }
            return points[points.Count-1];
        }

        ////<summary>
        ///����� ��������� ����� �� ������� � ��������� �����������. ����� ����� �� ������������ �� ����� ����. ��� ������� ����� �������� � ��������� �������
        ///<param name="position">
        ///��������� ����������
        ///</param>
        ///<param name="accuracy">
        ///�������� ������. ��� ���� �������� ��� ������ ������
        ///</param>
        ////</summary>
        public Point GetNearestPoint(Transform transform, Vector3 position, float accuracy)
        {
            float currentPos = 0;
            List<Point> points = new List<Point>();
            while (currentPos < length)
            {
                Point local = GetPointByOffset(currentPos);
                Point global = local.Transformed(transform);
                points.Add(global);
                currentPos += accuracy;
            }
            Point lastPointLocal = GetPointByOffset(length);
            Point lastPointGlobal = lastPointLocal.Transformed(transform);
            points.Add(lastPointGlobal);

            points.Sort((x, y) => Vector3.Distance(x.Position, position).CompareTo(Vector3.Distance(y.Position, position)));
            return points[0];
        }

        /*
        ////<summary>
        ///����� ��������� ����� ������� � �����������. ����� ����� �� ������������ �� ����� ����. ��� ������� ����� �������� � ��������� �������
        ///<param name="position">
        ///��������� ����������
        ///</param>
        ///<param name="accuracy">
        ///�������� ������. ��� ���� �������� ��� ������ ������
        ///</param>
        ////</summary>
        public float GetNearesOffset(Vector3 position, float accuracy)
        {
            float currentPos = 0;
            List<float> points = new List<float>();
            while (currentPos < length)
            {
                points.Add(currentPos);
                currentPos += accuracy;
            }
            points.Add(length);
            points.Sort((x, y) => Vector3.Distance(GetPointGlobal(x).Position, position).CompareTo(Vector3.Distance(GetPointGlobal(y).Position, position)));
            return points[0];
        }
        */
    }

    public class InvalidWayException : System.SystemException
    {
        public InvalidWayException() : base() {}

        public InvalidWayException(string message) : base(message) { }
        public InvalidWayException(string message, System.Exception innerException) : base(message, innerException) { }
        protected InvalidWayException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}