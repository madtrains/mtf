using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace WaySystem
{
    [System.Serializable]
    public class WayChunkScoutPoint : WayChunkMovingPoint
    {
        public delegate void ProcessWayChunk(WayChunk wayChunk);

        private bool loaded;
        private ProcessWayChunk @delegate;

        public void On(ProcessWayChunk process,float length)
        {
            this.@delegate = process;
            this.loaded = true;
            Move(length);
        }

        public void Off()
        {
            this.@delegate = null;
            this.loaded = false;
        }

        protected override void Change(WayChunk wayChunk)
        {
            base.Change(wayChunk);
            if (loaded)
                @delegate(wayChunk);
        }
    }
}