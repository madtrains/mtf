using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WeightedWayDirection : WeightedElement
{
    public float Angle { get { return angle; } set { angle = value; } }
    [SerializeField] private float angle;
}
