namespace WaySystem
{
    [System.Serializable]
    public enum WayChunkType
    {
        Undefined,
        Brick,
        Up,
        Down,
        Obstacle,
        ObstacleLong,
        Left,
        Right
    }

    [System.Serializable]
    public enum WayChunkFlipType 
    { 
        Always, AfterPass, Never
    }

    [System.Serializable]
    public enum WayChunkFlipDirections
    {
        DefaultBothSides, HereOnly, ThereOnly
    }

    [System.Serializable]
    public enum StoffActionType
    {
        DefaultHorn, Nothing
    }
}