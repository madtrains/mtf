using UnityEngine;

namespace MTSound
{
    public enum SoundType { SFX, Ambient, Train, TyDyh, Music }

    public class SoundPack<T> : UberBehaviour where T : MTAudioSource
    {
        [SerializeField] protected T[] mtAudioSources;

        public virtual void Init()
        {
            for (int i = 0; i < mtAudioSources.Length; i++) 
            {
                mtAudioSources[i].Init(this.transform);
            }
        }
    }

    public delegate void ProcessSource(AudioSource source);

    [System.Serializable]
    public class MTAudioSource
    {
        public float Pitch { set { SetPitch(value); } }
        public float Volume { set { SetVolume(value); } }

        [SerializeField] protected string name;
        [SerializeField] protected AudioClip clip;
        [SerializeField] protected int count = 1;
        [SerializeField] protected SoundType soundType;

        protected AudioSource[] sources;
        protected int index;

        public virtual void Init(Transform parent)
        {
            sources = new AudioSource[count];
            for (int i = 0; i < count; i++)
            {
                AudioSource source = new GameObject(clip.name).AddComponent<AudioSource>();
                source.transform.SetParent(parent, false);
                source.clip = this.clip;
                source.spatialBlend = 0f;
                source.outputAudioMixerGroup = GameStarter.Instance.MainComponents.SoundManager.AssignMixer(soundType);
                sources[i] = source;
            }
        }

        public virtual void Play()
        {
            sources[index].Play();
            index++;
            if (index >= sources.Length)
                index = 0;
        }

        public virtual void Stop()
        {
            foreach(AudioSource source in sources)
            {
                source.Stop();
            }
        }

        public virtual void SetPitch(float value)
        {
            foreach (AudioSource source in sources)
            {
                source.pitch = value;
            }
        }

        public virtual void SetVolume(float value)
        {
            foreach (AudioSource source in sources)
            {
                source.volume = value;
            }
        }

        public virtual void Process(ProcessSource @delegate)
        {
            foreach (AudioSource source in sources)
            {
                @delegate(source);
            }
        }
    }
}
