using System.Collections.Generic;
using UnityEngine;

namespace MTSound
{
	public enum SharedNames
	{
		Coin,
		PopJar,
		Button,
		ButtonSoft,
		MaintenanceOpen,
		MaintenanceClose,
		BigCog,
		CargoWhoosh,
		CargoWhoosh2,
		WipeIN,
		Magic
	}

	[System.Serializable]
    public class SharedAudioSource : MTAudioSource
    {
        public SharedNames EnumName { get { return enumName; } }
        [SerializeField] private SharedNames enumName;
    }

    public class SoundsShared : SoundPack<SharedAudioSource>
    {
        private Dictionary<SharedNames, SharedAudioSource> sources;


        public void Play(SharedNames name)
        {
            sources[name].Play();
        }

        public void Stop(SharedNames name)
        {
            sources[name].Stop();
        }

        public override void Init()
        {
            base.Init();
            sources = new Dictionary<SharedNames, SharedAudioSource>();
            foreach (SharedAudioSource shAS in mtAudioSources)
            {
                sources.Add(shAS.EnumName, shAS);
            }
        }
    }
}
