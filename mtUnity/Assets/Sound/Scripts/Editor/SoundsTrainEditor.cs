using UnityEditor;
using UnityEngine;

namespace MTSound
{
    [CustomEditor(typeof(SoundsTrain))]
    public class SoundsTrainEditor : UberEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            SoundsTrain soundsTrain = (SoundsTrain)target;
            if (GUILayout.Button("Recreate"))
            {
                soundsTrain.CreateTyDyhSequence();
            }
        }
    }
}
