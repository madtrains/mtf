using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngineInternal;

namespace MTSound
{
    public class SoundManager : UberBehaviour
    {
   
        public AudioMixerGroup AssignMixer(SoundType soundType)
        {
            switch (soundType) 
            {
                case SoundType.Ambient:
                {
                    return ambient;
                }
                case SoundType.Train:
                {
                    return train;
                }
                case SoundType.TyDyh:
                {
                    return tyDyh;
                }
                default :
                {
                    return sfx;
                }
            }
        }
        public AudioSource Music { get { return musicSource; } set { AssignMusic(value); } }
        public SoundsTrain Train { get { return soundsTrain; } set { soundsTrain = value; } }
        public SoundsShared Shared { get { return shared; } set { shared = value; } }

        [SerializeField] private AudioMixer mixer;
        [SerializeField] private AudioMixerGroup sfx;
        [SerializeField] private AudioMixerGroup ambient;
        [SerializeField] private AudioMixerGroup music;
        [SerializeField] private AudioMixerGroup train;
        [SerializeField] private AudioMixerGroup tyDyh;


        private AudioSource musicSource;
        private SoundsShared shared;
        private SoundsTrain soundsTrain;
        
        //[SerializeField] private SoundsLevel soundsLevel;


        public AudioSource CreateSimpleSFX(GameObject go, AudioClip clip)
        {
            AudioSource result = go.AddComponent<AudioSource>();
            result.outputAudioMixerGroup = sfx;
            SetAudioSource(result, clip, false, false);
            return result;
        }

        public void SetAudioSource(AudioSource source, AudioClip clip, bool playOnAwake, bool loop)
        {
            source.clip = clip;
            source.playOnAwake = playOnAwake;
            source.loop = loop;
        }

        private void AssignMusic(AudioSource source)
        {
            musicSource = source;
            source.outputAudioMixerGroup = music;
        }

        public void ChangeMusicVolume(float volume)
        {
            mixer.SetFloat("Music", SliderToVolume(volume));
        }

        public void ChangeSFXVolume(float volume)
        {
            
            mixer.SetFloat("SFX", SliderToVolume(volume));
        }

        private float SliderToVolume(float value)
        {
            float result = (1 - Mathf.Sqrt(value)) * -20f;
            return result;
            //float vol = Mathf.Log10(volume * 20f);
        }
    }
}