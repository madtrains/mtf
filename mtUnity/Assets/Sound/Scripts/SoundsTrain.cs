using System.Collections.Generic;
using UnityEngine;
using static MTSound.SoundsTrain;


namespace MTSound
{
    //0--0-------0--0-------
    //tuk1-short-tuk2-long-tuk3-short-tuk4-long
    public delegate void Play();
    [System.Serializable]
    public class Clip
    {
        public Clip(Play play, float delay)
        {
            this.play = play;
            this.delay = delay;
        }
        public Play play;
        public float delay;
    }

    public class ClipSequence
    {
        public ClipSequence()
        {
            clips = new List<Clip>();
        }

        public List<Clip> clips;
        private int index;
        public float delay;
        public void Update(float speed)
        {
            delay += Time.deltaTime * speed;
            Clip clip = clips[index];
            if (delay > clip.delay)
            {
                clip.play();
                delay = 0;
                index += 1;
                if (index >= clips.Count)
                    index = 0;
            }
        }
    }

    [System.Serializable]
    public class TrainAudioSource : MTAudioSource
    {
        public SoundsTrain.Names EnumName { get { return enumName; } }
        [SerializeField] private SoundsTrain.Names enumName;
    }

    public class SoundsTrain : SoundPack<TrainAudioSource>
    {
        private Dictionary<Names, TrainAudioSource> sources;
        public enum Names
        { 
            Copper,
            Silver,
            Gold,
            GearRepair,
            Ticket,
            GreenLight,
            RedLight,
            SkipPlatform,
            Hit,
            HitNoPoint,
            HornHit,
            Death,
            Flip,
            TyDyh,
            TyDyh2,
            BreakPress,
            BreakRelease
        }


        public override void Init()
        {
            base.Init();
            sources = new Dictionary<Names, TrainAudioSource>();
            foreach (TrainAudioSource mtAS in mtAudioSources)
            {
                sources.Add(mtAS.EnumName, mtAS);
            }
            CreateTyDyhSequence();
        }

        public void Play(Names name)
        {
            sources[name].Play();
        }

        public void Stop(Names name)
        {
            sources[name].Stop();
        }

        public void Delegate(Names name, ProcessSource @delegate)
        {
            sources[name].Process(@delegate);
        }


        public void CreateTyDyhSequence()
        {
            tyDyhSequence = new ClipSequence();
            tyDyhSequence.clips.Add(new Clip(() => { sources[Names.TyDyh].Play(); }, 0f));
            tyDyhSequence.clips.Add(new Clip(() => { }, p1));
            tyDyhSequence.clips.Add(new Clip(() => { sources[Names.TyDyh2].Play(); }, 2f));
            tyDyhSequence.clips.Add(new Clip(() => { }, p2));
            tyDyhSequence.clips.Add(new Clip(() => { sources[Names.TyDyh].Play(); }, 0f));
            tyDyhSequence.clips.Add(new Clip(() => { }, p1));
            tyDyhSequence.clips.Add(new Clip(() => { sources[Names.TyDyh2].Play(); }, 2f));
            tyDyhSequence.clips.Add(new Clip(() => { }, p2));

        }

        [SerializeField] private float p1;
        [SerializeField] private float p2;
        /*
        
        */
        private ClipSequence tyDyhSequence;
        [SerializeField] private MinMax tyDyhPitch;
        [SerializeField] private MinMax tyDyhVolume;
        [SerializeField] private MinMax tyDyhAbsSpeed;


        public void PickUp(WaySystem.PickUp.Type pickUpType)
        {
            switch (pickUpType) 
            {
                case WaySystem.PickUp.Type.Coin:
                {
                    sources[Names.Copper].Play();
                    break;
                }

                case WaySystem.PickUp.Type.CoinSilver:
                {
                    sources[Names.Silver].Play();
                    break;
                }
                    
                case WaySystem.PickUp.Type.CoinGold:
                {
                    sources[Names.Gold].Play();
                    break;
                }
                    
                case WaySystem.PickUp.Type.GearRepair:
                {
                    sources[Names.GearRepair].Play();
                    break;
                }
                    
                case WaySystem.PickUp.Type.Ticket:
                {
                    sources[Names.Ticket].Play();
                    break;
                }
                    
                default:
                {
                    GameStarter.Instance.MainComponents.SoundManager.Shared.Play(MTSound.SharedNames.Coin);
                    break;
                }
            }
        }

        public void Semaphore(bool on)
        {
            (on ? sources[Names.GreenLight] : sources[Names.RedLight]).Play();
        }

        public void Skip()
        {
           // skipPlatform.Play();
        }

        public void BreakPress()
        {
            sources[Names.BreakPress].Play();
        }

        public void BreakRelease()
        {
            sources[Names.BreakPress].Stop();
            sources[Names.BreakRelease].Play();
        }

        public void Breaking(bool status)
        {
            if (status)
            {
                sources[Names.BreakPress].Play();
            }
                
            else
            {
                sources[Names.BreakPress].Stop();
            }
                
            
        }

        public void Flip()
        {
            sources[Names.Flip].Play();
        }

        public void Hit()
        {
            sources[Names.Hit].Play();
        }

        public void HitNoPoint()
        {
            sources[Names.HitNoPoint].Play();
        }

        public void HornHit()
        {
            sources[Names.HornHit].Play();
        }

        public void Death()
        {
            sources[Names.Death].Play();
        }

        public void UpdateMoveSounds(float speed, float engineSpeed, float acceleration)
        {
            //��-���
            tyDyhSequence.Update(engineSpeed);

            float t = tyDyhAbsSpeed.InverseLerp(engineSpeed);
            sources[Names.TyDyh].Pitch = tyDyhPitch.Lerp(t);
            sources[Names.TyDyh2].Pitch = tyDyhPitch.Lerp(t);

            sources[Names.TyDyh].Volume = tyDyhVolume.Lerp(t);
            sources[Names.TyDyh2].Volume = tyDyhVolume.Lerp(t);
        }

        /*
        private void Cyl(Color color)
        {
            float size = 0.9f;
            Quaternion rot = GameStarter.Instance.Train.transform.rotation * Quaternion.Euler(Vector3.forward * 90);
            MTDebug.DebugManager.CreatePrimitive(PrimitiveType.Cylinder, color, 0.4f,
                GameStarter.Instance.Train.transform.position,
                Vector3.one * size,
                rot, true);
        }
        */
    }
}