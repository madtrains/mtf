﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace MTLightNCamera
{
	[CustomEditor(typeof(GameLight))]
	public class GameLightEditor : UberEditor
    {
        private readonly int ANGLE_MIN = -179;
        private readonly int ANGLE_MAX = 179;
        private readonly int ANGLE_UP_MIN = 0;
        private readonly int ANGLE_UP_MAX = 80;
        
        private float dolly;
        private HiddenFloat angle;
        private HiddenFloat angleUp;
        private HiddenFloat fov;
        

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
            GameLight light = target as GameLight;
			if (light == null)
			{
				EditorGUILayout.HelpBox(string.Format("NO Light"), MessageType.Error);
				return;
			}

            light.AngleUp = UberSlider("AngleUp: ", light.AngleUp, UberSliderType.asAngleInt, ANGLE_UP_MIN, ANGLE_UP_MAX);
            light.Angle = UberSlider("Angle: ", light.Angle, UberSliderType.asAngleInt, ANGLE_MIN, ANGLE_MAX);
            light.Intensity = UberSlider("Intensity: ", light.Intensity, UberSliderType.asFloat, 0, 3);
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Color: ", GUILayout.Width(LABEL_WIDTH));
            light.Color = EditorGUILayout.ColorField(light.Color);
            EditorGUILayout.EndHorizontal();
        }
	}
}