﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace MTUi
{

	[CustomEditor(typeof(UICamera))]
	public class UICameraEditor : Editor
	{
		private int angleValue;
		private float upValue;
		private int axisValue;
		private float dollyValue;

		private Texture2D target2DTexture;

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			UICamera uiCamera = target as UICamera;
			if (uiCamera.Camera == null)
			{	
				EditorGUILayout.HelpBox(string.Format("NO Camera"), MessageType.Error);
				return;
			}

			if (uiCamera.Up != null)
			{
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("Angle", GUILayout.Width(55));

				angleValue = (int)uiCamera.Up.localEulerAngles.y;
				if (angleValue > 180)
				{
					angleValue = angleValue - 360;
				}

				angleValue = EditorGUILayout.IntSlider(angleValue, -179, 179);
				uiCamera.Up.localEulerAngles = new Vector3(0, angleValue, 0);
				EditorGUILayout.EndHorizontal();

				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("Up", GUILayout.Width(55));
				upValue = uiCamera.Up.localPosition.y;
				upValue = EditorGUILayout.Slider(upValue, -5f, 5f);
				uiCamera.Up.localPosition = new Vector3(0, upValue, 0);
				EditorGUILayout.EndHorizontal();

				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("Axis", GUILayout.Width(55));
				axisValue = (int)uiCamera.Rotator.localEulerAngles.x;
				axisValue = EditorGUILayout.IntSlider(axisValue, -90, 90);
				uiCamera.Rotator.localEulerAngles = new Vector3(axisValue, 0, 0);
				EditorGUILayout.EndHorizontal();

				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("Dolly", GUILayout.Width(55));
				dollyValue = uiCamera.Camera.transform.localPosition.z * -1;
				dollyValue = EditorGUILayout.Slider(dollyValue, 0, 20f);
				uiCamera.Camera.transform.localPosition = new Vector3(0, 0, -dollyValue);
				EditorGUILayout.EndHorizontal();
			}
			
			
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("Save Texture"))
			{
				SaveToFile();
			}
			if (GUILayout.Button("Null"))
            {
				uiCamera.Camera.targetTexture = null;	
			}
			target2DTexture = EditorGUILayout.ObjectField(target2DTexture, typeof(Object), true) as Texture2D;
			EditorGUILayout.EndHorizontal();
		}


		public void SaveToFile()
		{
			if (target2DTexture == null)
			{
				Debug.LogError("No Target Texture Set!!");
				return;
			}

			UICamera uiCamera = target as UICamera;
			RenderTexture rt = uiCamera.Camera.targetTexture;
			if (rt == null)
            {
				if (target2DTexture == null)
                {
					Debug.LogErrorFormat("No Taret Texture field set {0}", uiCamera.gameObject.name);
					return;
				}
				rt = new RenderTexture(target2DTexture.width, target2DTexture.height, -1);
				uiCamera.Camera.targetTexture = rt;
			}

			CameraClearFlags before = uiCamera.Camera.clearFlags;
			uiCamera.Camera.clearFlags = CameraClearFlags.SolidColor;
			RenderTexture.active = rt;
			Texture2D tex = new Texture2D(rt.width, rt.height, TextureFormat.ARGB32, false);
			tex.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
			

			byte[] bytes;
			bytes = tex.EncodeToPNG();
			string path = AssetDatabase.GetAssetPath(target2DTexture);
			Debug.Log(path);
			System.IO.File.Delete(path);
			System.IO.File.WriteAllBytes(path, bytes);
			AssetDatabase.ImportAsset(path);
			System.DateTime last = System.IO.File.GetLastWriteTime(path);

			
			uiCamera.Camera.clearFlags = before;
			
			RenderTexture.active = null;
		}
	}
}