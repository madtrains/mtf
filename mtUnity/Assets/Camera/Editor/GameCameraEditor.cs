﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MTRails;

namespace MTLightNCamera
{
    [CustomEditor(typeof(GameCamera))]
    public class GameCameraEditor : UberEditor
    {
        private readonly int ANGLE_MIN = -179;
        private readonly int ANGLE_MAX = 179;
        private readonly int ANGLE_UP_MIN = 0;
        private readonly int ANGLE_UP_MAX = 80;
        private readonly int DOLLY_MIN = 0;
        private readonly int DOLLY_MAX = 255;
        private readonly int FOV_MIN = 20;
        private readonly int FOV_MAX = 60;

        private float testWayDistance;

        private Object camDockObj;
        private MTTown.CamDock camDock;



        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GameCamera gameCamera = target as GameCamera;
            if (gameCamera == null)
            {
                EditorGUILayout.HelpBox(string.Format("NO Camera"), MessageType.Error);
                return;
            }

            gameCamera.AngleUp = UberSlider("AngleUp: ", gameCamera.AngleUp, UberSliderType.asAngleInt, ANGLE_UP_MIN, ANGLE_UP_MAX);
            gameCamera.Angle = UberSlider("Angle: ", gameCamera.Angle, UberSliderType.asAngleInt, ANGLE_MIN, ANGLE_MAX);
            //gameCamera.Dolly = EditorGUILayout.Slider("Dolly: ", gameCamera.Dolly, DOLLY_MIN, DOLLY_MAX);
            gameCamera.Dolly = UberSlider("Dolly: ", gameCamera.Dolly, UberSliderType.asFloat, DOLLY_MIN, DOLLY_MAX);
            gameCamera.Fov = UberSlider("Fov: ", gameCamera.Fov, UberSliderType.asInt, FOV_MIN, FOV_MAX);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.HelpBox("Cam Dock Target:\n(Use it to set values to GameParameters)",
                MessageType.Info);
            camDockObj = EditorGUILayout.ObjectField(camDockObj, typeof(Object), true);

            if (camDockObj != null)
            {
                camDock = (camDockObj as GameObject).GetComponent<MTTown.CamDock>();
            }
            if (camDock != null)
            {
                if (GUILayout.Button("Apply to Object and GP"))
                {
                    ;
                    camDock.transform.position = gameCamera.transform.position;
                    float angle = PrettyAngle(gameCamera.Angle);
                    MTParameters.CameraParameters newParams = new MTParameters.CameraParameters("params",
                        angle, gameCamera.AngleUp, gameCamera.Dolly, gameCamera.Fov);
                    camDock.CameraParams = newParams;

                }
            }

            EditorGUILayout.EndHorizontal(); 
        }
    }
}