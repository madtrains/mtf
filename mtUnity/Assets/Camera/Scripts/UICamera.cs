﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTUi
{
	public class UICamera : MonoBehaviour
	{
		public Transform Dock { get { return dock; } }
		public Transform Up { get { return up; } }
		public Transform Rotator { get { return rotator; } }
		public Camera Camera { get { return cam; } }
        public Animator Animator { get { return animator; } }


        [SerializeField] private Transform dock;
		[SerializeField] private Transform up;
		[SerializeField] private Transform rotator;
		[SerializeField] private Camera cam;
        [SerializeField] private Animator animator;
		[SerializeField] private bool disableAfterFirstUpdate;

		private bool wasUpdated;


		public static Vector3[] GetSquareCoordinates(int number, int step, Vector3 start)
		{
			float sq = Mathf.Sqrt(number);
			int upperSq = Mathf.CeilToInt(sq);

			List<Vector3> coordinates = new List<Vector3>();
			for (int x=0; x < upperSq; x++)
			{
				for (int z = 0; z < upperSq; z++)
				{
					Vector3 currentCoordinates = new Vector3(x * step, 0, z*step);
					currentCoordinates = currentCoordinates + start;
					coordinates.Add(currentCoordinates);
				}
			}

			Vector3[] result = new Vector3[number];
			for (int i = 0; i < number; i++)
			{
				result[i] = coordinates[i];
			}

			return result;
		}
		private void Update()
		{
			if (!wasUpdated)
			{
				wasUpdated = true;
				FirstUpdate();
			}
		}


		private void FirstUpdate()
		{
			if (disableAfterFirstUpdate)
				Camera.enabled = false;
		}


		public bool CameraEnabled
		{
			get
			{
				return Camera.enabled;
			}
			
			set
			{
				if (!wasUpdated && !value)
					return;
				Camera.enabled = value;
			}
		}
	}
}
