﻿using MTTown;
using UnityEngine;


namespace MTLightNCamera
{
    public class GameLight : GameLightNCameraBase
    {
        public void Set(TownLight townLight)
        {
            Color = townLight.Color;
            Intensity = townLight.Intensity;
            Angle = townLight.Angle;
            AngleUp = townLight.AngleUp;
            RenderSettings.ambientLight = townLight.Ambient;
            ShadowsStrength = townLight.ShadowsStrength;
		}

        public float ShadowsStrength
        {
            get { return lght.shadowStrength; }
            set { lght.shadowStrength = value; }
        }

        public Color Color
        {
            get { return lght.color; }
            set { lght.color = value; }
        }

        public float Intensity
        {
            get { return lght.intensity; }
            set { lght.intensity = value; }
        }

        [SerializeField] private Light lght;
    }
}