﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MTParameters;


namespace MTLightNCamera
{
    public class GameLightNCameraBase : TimerBeh
    {
        public virtual void CheckAndApplyParams(MTParameters.CameraParameters parameters)
        {
            if (Angle != parameters.Angle)
                Angle = parameters.Angle;

            if (AngleUp != parameters.AngleUp)
                AngleUp = parameters.AngleUp;
        }

        public virtual void ApplyParams(MTParameters.CameraParameters parameters)
        {
            Angle = parameters.Angle;
            AngleUp = parameters.AngleUp;
        }


        public float Angle
        {
            get { return PrettyAngle(angle.eulerAngles.y); }
            set { angle.localEulerAngles = Vector3.up * value; }
        }


        public float AngleUp
        {
            get { return PrettyAngle(angleUp.eulerAngles.x); }
            set
            {
                angleUp.localEulerAngles = Vector3.right * value;
            }
        }


        [SerializeField] protected Transform angle;
        [SerializeField] protected Transform angleUp;
    }
}