﻿using MTParameters;
using UnityEngine;

namespace MTLightNCamera
{
    public enum CameraMode { FixedTarget, MovingTarget }

    public class GameCamera : GameLightNCameraBase
    {
        [SerializeField] private Transform offset;
        [SerializeField] private Transform dolly;
        [SerializeField] private UnityEngine.Camera cam;

        private CameraMode targetMode;
        private bool isBusy;
        private ProcessDelegate processDelegate;
        private EndDelegate endDelegate;

        //To send camera somewhere and run delegate on End;
        private CameraParameters previousCameraParameters;
        private CameraParameters targetCameraParameters;
        private Vector3 previousPosition;
        private Vector3 targetPosition;
        //private Transform targetTransform;
        private WaySystem.WayChunkMovingPoint targetPoint;

        public override void CheckAndApplyParams(CameraParameters parameters)
        {
            base.CheckAndApplyParams(parameters);
            if (Dolly != parameters.Dolly)
                Dolly = parameters.Dolly;

            if (Fov != parameters.Fov)
                Fov = parameters.Fov;
        }

        public override void ApplyParams(CameraParameters parameters)
        {
            base.ApplyParams(parameters);
            Dolly = parameters.Dolly;
            Fov = parameters.Fov;
        }

        public float Dolly
        {
            get { return dolly.localPosition.z * -1f; }
            set { dolly.localPosition = Vector3.forward * -value; }
        }

        public float Fov
        {
            get { return cam.fieldOfView; }
            set { cam.fieldOfView = value; }
        }


        public UnityEngine.Camera Camera { get { return cam; } }

        public Vector3 GetScreenPoint(Vector3 point)
        {
            return Camera.WorldToScreenPoint(point);
        }

        #region public Methods
        public void MoveTo(Vector3 targetPosition, CameraParameters targetCameraParameters,
            EndDelegate endDelegate, ProcessDelegate processDelegate,
            float flySpeed, float angleSpeed)
        {
            if (isBusy)
                return;
            isBusy = true;

            targetMode = CameraMode.FixedTarget;
            Save();
            this.targetPosition = targetPosition;
            this.targetCameraParameters = targetCameraParameters;
            this.processDelegate = processDelegate;
            this.endDelegate = endDelegate;

            float distance = Vector3.Distance(transform.position, this.targetPosition);
            float flyTime = distance / flySpeed;

            float angle = targetCameraParameters.Angle - previousCameraParameters.Angle;
            float angleTime = Mathf.Abs(angle) / angleSpeed;
            float max = Mathf.Max(flyTime, angleTime);

            //Debug.LogFormat("FlyTime {0} AngleTime {1} time {2}", flyTime, angleTime, max);
            Launch(max);
        }

        public void ChaseTransform(WaySystem.WayChunkMovingPoint targetPoint, CameraParameters targetCamParams, 
            float flyTime, EndDelegate endDelegate)
        {
            if (isBusy)
                return;
            isBusy = true;

            Save();
            this.targetPoint = targetPoint;
            this.targetCameraParameters = targetCamParams;
            this.processDelegate = null;
            this.endDelegate = endDelegate;
            targetMode = CameraMode.MovingTarget;
            Launch(flyTime);
        }

      

        public void Save(MTTown.CamDock camDock)
        {
            Save();
            targetPosition = camDock.transform.position;
            targetCameraParameters = camDock.CameraParams;
        }


        public void LerpSaved(float t)
        {
            transform.position = Vector3.Lerp(previousPosition, targetPosition, t);
            Lerp(previousCameraParameters, targetCameraParameters, t);
            Debug.LogFormat("Dolly {0}/{1} {2}",
                previousCameraParameters.Dolly, targetCameraParameters.Dolly, Dolly);
        }


        public T RaycastComponent<T>(Vector2 screenPosition) where T : Component
        {
            RaycastHit hit;
            Ray ray = GameStarter.Instance.MainComponents.GameCamera.Camera.ScreenPointToRay(screenPosition);

            if (Physics.Raycast(ray, out hit))
            {
                T comp = hit.transform.GetComponent<T>();
                if (comp != null)
                    return comp;
            }

            return null;
        }

        public void Lerp(MTTown.CamDock a, MTTown.CamDock b, float t)
        {
            Lerp(a.transform.position, b.transform.position, t);
            Lerp(a.CameraParams, b.CameraParams, t);
        }

        public void Lerp(CameraParameters a, CameraParameters b, float t)
        {
            if (a.Angle != b.Angle)
                Angle = Mathf.Lerp(a.Angle, b.Angle, t);
            if (a.AngleUp != b.AngleUp)
                AngleUp = Mathf.Lerp(a.AngleUp, b.AngleUp, t);
            if (a.Dolly != b.Dolly)
                Dolly = Mathf.Lerp(a.Dolly, b.Dolly, t);
            if (a.Fov != b.Fov)
                Fov = Mathf.Lerp(a.Fov, b.Fov, t);
        }

        public void Lerp(Vector3 a, Vector3 b, float t)
        {
            this.transform.position = Vector3.Lerp(a, b, t);
        }

        public void Set(Vector3 position, CameraParameters camParams)
        {
            transform.position = position;
            CheckAndApplyParams(camParams);
        }
        #endregion

        #region Privates
        protected override void TimeChange()
        {
            base.TimeChange();
            float smoothTime = GameStarter.Instance.SmoothAnimCurve.Evaluate(TimeNormalized);
            if (isBusy)
            {
                Lerp(previousCameraParameters, targetCameraParameters, smoothTime);
                if (this.targetMode == CameraMode.FixedTarget)
                    Lerp(previousPosition, targetPosition, smoothTime);
                else if (this.targetMode == CameraMode.MovingTarget)
                    Lerp(previousPosition, targetPoint.Point.Position, smoothTime);
            }

            if (processDelegate != null)
            {
                processDelegate(smoothTime);
            }
        }


        protected override void End()
        {
            base.End();
            isBusy = false;

            if (endDelegate != null)
            {
                endDelegate();
            }
        }

        
        private void Save()
        {
            previousPosition = transform.position;
            previousCameraParameters = new CameraParameters("previous", Angle, AngleUp, Dolly, Fov);
        }


        private void OnDrawGizmos()
        {
            Gizmos.color = new Color(1.0f, 0f, 0f, 0.4f);
            Gizmos.DrawCube(transform.position, Vector3.one * 0.4f);
            Gizmos.DrawCube(transform.position, new Vector3(0.6f, 0.01f, 0.6f));
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(transform.position, 0.2f);
        }
        #endregion Methods
    }
}