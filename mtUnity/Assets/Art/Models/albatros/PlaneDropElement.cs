using Triggers;
using UnityEngine;
using UnityEngineInternal;
using WaySystem;

public class PlaneDropElement : TrainTrigger
{
    Vector3 startPosition;
    public static readonly float SPEED = 4;
    public static readonly float FALL_DISTANCE = 5f;
    public static readonly float SPEED_MULTIPLIER = 0.8f;


    public void Start()
    {
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y > startPosition.y - FALL_DISTANCE)
            transform.Translate(Vector3.down * SPEED * Time.deltaTime);
        else
            transform.position = Vector3.Lerp(
                transform.position,
                GameStarter.Instance.Train.transform.position,
                Time.deltaTime * GameStarter.Instance.Train.TrainParameters.Speed * SPEED_MULTIPLIER);
    }
}
