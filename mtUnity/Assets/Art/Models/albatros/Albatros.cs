using System.Collections.Generic;
using UnityEngine;
using WaySystem.PickUp;

public class Albatros : MTCore.RayCastTap
{
    [SerializeField] private Transform prop;
    [SerializeField] private float propSpeed;
    [SerializeField] private float speed;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private bool ignition;
    [SerializeField] private Transform bomb;
    [SerializeField] private VisibilityChecker visibilityChecker;
    [SerializeField] private AudioSource audioSource;

    private bool empty;
    private List<PickUp> elements;
    Quaternion off = Quaternion.LookRotation((Vector3.back + Vector3.up).normalized);


    public void Run()
    {
        ignition = true;
        audioSource.Play();
    }

    public void Init(List<PickUp> elements)
    {
        this.elements = elements;
        visibilityChecker.OnVisibilityChanged = VisibilityChanged;
    }

   public override void Tap()
   {
        base.Tap();
        if (elements.Count == 0)
        {
            Debug.LogWarningFormat("Albatros: No pickUp element for plane tap!");
            return;
        }
        PickUp rf = elements[0];
        elements.RemoveAt(0);
        if (!empty)
        {
            PickUp clone = GameObject.Instantiate<PickUp>(rf);
            clone.transform.position = bomb.position;
            clone.gameObject.AddComponent<PlaneDropElement>();
        }
        empty = elements.Count == 0;
    }

    private void VisibilityChanged(bool newValue)
    {
        if (!newValue && ignition)
        {
            TimerBeh tb = this.gameObject.AddComponent<TimerBeh>();
            tb.Launch(7);
            tb.OnEnd = () => { GameObject.Destroy(this.gameObject); };
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!ignition)
            return;

        prop.Rotate(0, 0, propSpeed * Time.deltaTime);
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
        if (empty)
            transform.rotation = Quaternion.Lerp(transform.rotation, off, rotationSpeed * Time.deltaTime);
    }
}
