using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

namespace WaySystem.Additions
{
    public class ElPole : UberBehaviour, IAddition
    {
        public Transform WireDock { get { return wireDock; } }
        [SerializeField] private Transform wireDock;
        [SerializeField] private Transform wire5;

        public void DockTo(Transform other)
        {
            this.transform.position = other.position;
            this.transform.rotation = other.rotation;
        }

        public void DockTo(WayChunk chunk)
        {
            this.transform.position = chunk.Transform.position;
            this.transform.rotation = chunk.Transform.rotation;
        }

        public void LinkWire(ElPole other)
        {
            float length = Vector3.Distance(this.wireDock.position, other.wireDock.position);
            length = length / 5f;
            Vector3 offset = other.wireDock.position - this.wireDock.position;
            Quaternion rotation = Quaternion.LookRotation(offset);
            this.wire5.position = this.wireDock.position;
            this.wire5.rotation = rotation;
            this.wire5.localScale = new Vector3(1, 1, length);
        }
        public void Last()
        {
            Destroy(this.wire5.gameObject);
        }
    }

    public class PolesLine
    {
        private ElPole refObject;
        private List<ElPole> list;
        private Transform root;

        public PolesLine(ElPole source, Transform root)
        {
            this.refObject = source;
            this.root = root;
            this.list = new List<ElPole>();
        }

        public void Clone(WayChunk target)
        {
            Clone().DockTo(target);
        }

        public void Clone(Transform target)
        {
            Clone().DockTo(target);
        }


        private ElPole Clone()
        {
            ElPole clone = GameObject.Instantiate(refObject, root);
            this.list.Add(clone);
            return clone;
        }

        public void Close()
        {
            for (int i = 0; i < list.Count - 1; i++)
            {
                ElPole current = list[i];
                ElPole next = list[i + 1];
                current.LinkWire(next);
            }
            list[list.Count - 1].Last();
        }
    }
}
