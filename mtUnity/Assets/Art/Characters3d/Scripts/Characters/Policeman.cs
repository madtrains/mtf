using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTCharacters
{
    public class Policeman : CharacterLogic
    {
        public bool Busy 
        { 
            get { return _busy; } 
            set 
            { 
                _busy = value;
                if (_busy)
                {
                    BrakeChangeCollection(AnimCollection.Collection.Policeman); 
                }
                    
                else
                {
                    BrakeChangeCollection(AnimCollection.Collection.Idle);
                }
            }
        }

        private bool _busy;
    }
}

