using UnityEngine;
using UnityEngine.AI;

namespace MTCharacters
{
    public class TownPassenger : CharacterLogic
    {
        public Color Color { get { return debugColor; } set { debugColor = value; } }
        public bool InExpressMode { get { return expressMode; } set { expressMode = value; } }
        [SerializeField] protected NavMeshAgent agent;
        [SerializeField] protected TargetPoint target;
        [SerializeField] protected Color debugColor;
        private bool isWalking;
        private bool expressMode;

        public bool Teleport(Vector3 position, Quaternion quaternion)
        {
            NavMeshHit hit;
            if (NavMesh.SamplePosition(position, out hit, 5, NavMesh.AllAreas))
            {
                Activate(gameObject, true);
                transform.position = hit.position;
                transform.rotation = quaternion;
                return true;
            }
            else
            {
                Debug.LogErrorFormat("Can not spawn character!");
                return false;
            }
        }

        public void TurnOff()
        {
            Activate(gameObject, false);
        }

        public void GoTo(TargetPoint point)
        {
            if (agent == null)
            {
                Debug.LogWarningFormat("{0} Passenger has null in navmesh agent! {1}", this.name, this.transform.position);
                return;
            }
            if (agent.isOnNavMesh)
            {
                target = point;
                agent.SetDestination(point.transform.position);
                agent.isStopped = false;
                isWalking = true;
                BackToIdle();
                AnimState = AnimState.Walk;
                BrakeAnimatorPlay();
            }
            else
            {
                Debug.LogErrorFormat(
                    "Character is not on NavMesh {0}",
                    gameObject.name);
            }
        }

        public void ClearTarget()
        {
            if (target != null)
                target.Clear();
        }

        public void BreakToCustomer()
        {
            BrakeChangeCollection(AnimCollection.Collection.Customer);
        }

        public void BreakToTalk()
        {
            BrakeChangeCollection(AnimCollection.Collection.Talk);
        }

        public void BreakToImpatience()
        {
            BrakeChangeCollection(AnimCollection.Collection.ImpatientPassenger);
        }

        public void BackToIdle()
        {
            currentCollection = AnimCollection.Collection.Idle;
        }

        protected void Going()
        {
            if (agent.velocity.magnitude > 0)
            {
                float speed = Mathf.InverseLerp(0, 2, agent.velocity.magnitude);
                graphics.Animator.SetFloat(hSpeed, speed);
            }
            else
            {
                if (!agent.pathPending && agent.path.corners.Length == 1)
                {
                    agent.isStopped = true;
                    isWalking = false;
                    AnimState = AnimState = AnimState.Basic;
                    BrakeAnimatorPlay();
                    target.Reached(this);
                }
            }
        }

        protected virtual void Update()
        {
            //debugStates = string.Format("state {0}, index {1}", animator.GetInteger(hState), animator.GetInteger(hIndex));            
            if (isWalking)
            {
                Going();
            }
            else
            {
                if (target != null)
                    transform.rotation = Quaternion.Slerp(transform.rotation, target.transform.rotation, Time.deltaTime * 3f);
            }
        }

        protected void OnDrawGizmos()
        {
            Gizmos.color = debugColor;
            Gizmos.DrawWireCube(transform.position, Vector3.one * 0.5f);
            if (agent.path == null || agent.path.corners.Length == 0)
                return;
            if (agent.path.corners.Length > 1)
            {
                for (int i = 1; i < agent.path.corners.Length; i++)
                {
                    int prev = i - 1;
                    Gizmos.DrawLine(agent.path.corners[prev], agent.path.corners[i]);
                    Gizmos.DrawSphere(agent.path.corners[i], 0.15f);
                }
            }
        }
    }
}