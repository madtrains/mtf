using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTCharacters
{
    public class IndexedAnim : MonoBehaviour
    {
        public int Index { get { return index; } }

        [SerializeField] private int index;
    }
}

