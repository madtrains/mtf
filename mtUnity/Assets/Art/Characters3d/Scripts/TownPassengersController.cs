using MTTown.Characters;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using WaySystem.Collection;

namespace MTCharacters
{
    public class TownPassengersController : UberBehaviour
    {
        [SerializeField] private TownPassenger townPassenger;
        [SerializeField] private CharacterLogic erich;
        [SerializeField] private CharacterLogic marleen;
        [SerializeField] private Policeman[] policemen;
        [SerializeField] private Seller seller;
        [SerializeField] private AlterFritz alterFritz;
        [SerializeField] private GrosserFrantz grosserFranz;
        [SerializeField] private PlatformPoint[] points;
        [SerializeField] private ExitPoint[] ePoints;
        [SerializeField] private Color[] colors;
        [SerializeField] private Color expressColor;

        [SerializeField] private int platformPassengersCount;
        public int ExpressPassengersCount { get { return expessPassengersCount; } }
        [SerializeField] private int expessPassengersCount;

        public int PassengersPullCount { get { return passengersPullCount; } }
        [SerializeField] private int passengersPullCount;
        /// <summary>
        /// точки первоначальной позиции персонажей
        /// </summary>
        [SerializeField] private Marker[] prePoints;
        [SerializeField] private Dilemma leavePlatformDilemma;
        [SerializeField] private Dilemma takeExpress;
        [SerializeField] private Transform tKarree;

        private CharactersGraphicsPull charactersGraphicsPull;
        private TownPassengersPull platformPassengers;
        private TownPassengersPull expressPassengers;
        private ExitPoints expressGetIn;
        private PlatformPoints platformPoints;
        private ExitPoints exitPoints;
        private ColorPull colorPull;

        public void Run()
        {
            platformPassengers.Iterate((passenger)=>
            {
                TargetPoint townPoint = GetNextOccupation();
                townPoint.Assign(passenger);
            });
        }

        public void Init(CharacterFactory.CharacterFactory characterFactory)
        {
            CharacterGraphics[] graphicsArray = new CharacterGraphics[passengersPullCount];
            for (int i = 0; i < passengersPullCount; i++)
            {
                CharacterGraphics graphics =
                    characterFactory.CreateRandom(UberBehaviour.RandomBool,
                        (container) => { });
                graphicsArray[i] = graphics;
            }
            charactersGraphicsPull =
                new CharactersGraphicsPull(tKarree, graphicsArray);
            /*
            
            */
            erich.AssignGraphics(characterFactory.CreateMan((c) => { },
                new CharacterFactory.OverrideName(), //hair color
                new CharacterFactory.OverrideName("Erich"), //character
                new CharacterFactory.OverrideName(), //dress color
                new CharacterFactory.OverrideHeight(192f),
                new CharacterFactory.OverrideAddition(), //hat
                new CharacterFactory.OverrideAddition(), //hairs
                new CharacterFactory.OverrideAddition(), //facial hairs
                new CharacterFactory.OverrideAddition() //smoker
            ));
            erich.Init(characterFactory.MainAnimCollection, AnimCollection.Collection.Talk);

            marleen.AssignGraphics(characterFactory.CreateWoman ((c) => { },
                new CharacterFactory.OverrideName(), //hair color
                new CharacterFactory.OverrideName("Marleen"), //character
                new CharacterFactory.OverrideName(), //dress color
                new CharacterFactory.OverrideHeight(172f),
                new CharacterFactory.OverrideAddition("Krankenschwester", true) //hat
            ));
            marleen.Init(characterFactory.MainAnimCollection, AnimCollection.Collection.Talk);
            foreach(Policeman policeman in policemen)
            {
                policeman.AssignGraphics(characterFactory.CreateMan((c) => { },
                new CharacterFactory.OverrideName(), //hair color
                new CharacterFactory.OverrideName("Hans"), //character
                new CharacterFactory.OverrideName(), //dress color
                new CharacterFactory.OverrideHeight(),
                new CharacterFactory.OverrideAddition("Tschako", true), //hat
                new CharacterFactory.OverrideAddition(), //hairs
                new CharacterFactory.OverrideAddition(), //facial hairs
                new CharacterFactory.OverrideAddition() //smoker
            ));
                policeman.Init(characterFactory.MainAnimCollection, AnimCollection.Collection.Policeman);
            }
            seller.AssignGraphics(characterFactory.CreateWoman((c) => { },
                new CharacterFactory.OverrideName(), //hair color
                new CharacterFactory.OverrideName("ElsaMadel"), //character
                new CharacterFactory.OverrideName(), //dress color
                new CharacterFactory.OverrideHeight(),
                new CharacterFactory.OverrideAddition("Corona", true) //hat
            ));
            seller.Init(characterFactory.MainAnimCollection, AnimCollection.Collection.Seller);
            CharacterGraphics fritzGraphics =
                characterFactory.CreateMan((c) => { },
                new CharacterFactory.OverrideName("grey"), //hair color
                new CharacterFactory.OverrideName("AlterFritz"), //character
                new CharacterFactory.OverrideName(), //dress color
                new CharacterFactory.OverrideHeight(188f),
                new CharacterFactory.OverrideAddition(), //hat
                new CharacterFactory.OverrideAddition("Drei", true), //hairs
                new CharacterFactory.OverrideAddition("LangBart", true), //facial hairs
                new CharacterFactory.OverrideAddition("", false));//smoker
            alterFritz.AssignGraphics(fritzGraphics);
            alterFritz.Init(characterFactory.MainAnimCollection, AnimCollection.Collection.IdlePatient);
            alterFritz.AssignBubble(GameStarter.Instance.UIRoot.Town.Bubble, fritzGraphics.Head);
            CharacterGraphics grosserFranzGraphics = characterFactory.CreateMan((c) => { },
                new CharacterFactory.OverrideName("brown"), //hair color
                new CharacterFactory.OverrideName("GrosserFranz"), //character
                new CharacterFactory.OverrideName(), //dress color
                new CharacterFactory.OverrideHeight(196f),
                new CharacterFactory.OverrideAddition("Hat", true), //hat
                new CharacterFactory.OverrideAddition("Ein", true), //hairs
                new CharacterFactory.OverrideAddition("Schnauzbart", true), //facial hairs
                new CharacterFactory.OverrideAddition("Pipe", true));
            grosserFranz.AssignGraphics(grosserFranzGraphics); //smoker
            grosserFranz.Init(characterFactory.MainAnimCollection, AnimCollection.Collection.IdlePatient);
            grosserFranz.AssignBubble(GameStarter.Instance.UIRoot.Town.Bubble, grosserFranzGraphics.Head);

            if (platformPassengersCount > (points.Length + 1))
                Debug.LogErrorFormat("To many passengers {0} for {1} platform points. Add points to prefab", platformPassengersCount, points.Length);
            colorPull = new ColorPull(colors);
    
            platformPoints = new PlatformPoints();
            foreach (PlatformPoint pp in points)
            {
                pp.Init();
                pp.OnDone += OnPlatformPointDone;
                platformPoints.Add(pp);
            }

            exitPoints = new ExitPoints();
            foreach (ExitPoint exitPoint in ePoints)
            {
                exitPoint.Init();
                exitPoint.OnReached += OnExitPointReached;
                exitPoints.Add(exitPoint);
            }

            platformPassengers = new TownPassengersPull();
            for (int i = 0; i < platformPassengersCount; i++)
            {
                TownPassenger tp = 
                    ClonePassenger(characterFactory.MainAnimCollection, i,
                    "Passenger_", false);
                platformPassengers.Add(tp);
                tp.transform.position = prePoints[i].transform.position;
                tp.InExpressMode = false;
                Activate(tp.gameObject, true);
                tp.Color = colorPull.Next;
            }

            expressPassengers = new TownPassengersPull();
            for (int i = 0; i < expessPassengersCount; i++)
            {
                TownPassenger ep = 
                    ClonePassenger(characterFactory.MainAnimCollection, i,
                    "ExpressPassenger_", true);
                expressPassengers.Add(ep);
                Activate(ep.gameObject, false);
                ep.Color = expressColor;
            }

            if (prePoints.Length < platformPassengersCount)
                Debug.LogErrorFormat("Not enough town points {0} for {1}",
                    prePoints.Length, platformPassengersCount); 
        }


        private TownPassenger ClonePassenger(AnimCollection animCollection, int i, string name, bool expressMode)
        {
            TownPassenger townPassenger = Instantiate<TownPassenger>(this.townPassenger, this.townPassenger.transform.parent);
            townPassenger.gameObject.name = new StringBuilder().Append(name).Append(i).ToString();
            townPassenger.AssignGraphics(charactersGraphicsPull.Next);
            townPassenger.Init(animCollection, AnimCollection.Collection.Idle);
            townPassenger.InExpressMode = expressMode;
            return townPassenger;
        }

        public void LinkFakeTrain(FakeTrain fakeTrain)
        {
            fakeTrain.OnPassengerOut += OnExpressPassengerGetOut;
            fakeTrain.OnArrive += OnExpressArrival;
            expressGetIn = new ExitPoints();
            foreach (ExitPoint exitPoint in fakeTrain.GetInPoints)
            {
                exitPoint.Init();
                exitPoint.OnReached += OnExpressGetInPointReached;
                expressGetIn.Add(exitPoint);
            }
        }

        private TargetPoint GetNextOccupation()
        {
            if (leavePlatformDilemma.PositiveDesicion)
            {
                return exitPoints.Next;
            }
            else
            {
                return platformPoints.Next;
            }
        }

        //platform passengers
        private void OnPlatformPointDone(PlatformPoint point, TownPassenger passenger)
        {
            GetNextOccupation().Assign(passenger);
            point.Clear();
        }

        private void OnExitPointReached(TargetPoint targetPoint, TownPassenger passenger)
        {
            ChangeSkin(passenger);
            if (passenger.InExpressMode)
            {
                passenger.TurnOff();
            }
            else
            {
                GetNextOccupation().Assign(passenger);
            }
        }

        private void OnExpressGetInPointReached(TargetPoint ep, TownPassenger passenger)
        {
            TargetPoint exitPoint = exitPoints.Next;
            passenger.Teleport(exitPoint.transform.position, exitPoint.transform.rotation);
            ChangeSkin(passenger);
            GetNextOccupation().Assign(passenger);
        }


        //express passenger full cycle
        private void OnExpressPassengerGetOut(Vector3 door, Quaternion quaternion)
        {
            TownPassenger ep = expressPassengers.Next;
            ep.Teleport(door, quaternion);
            ep.GoTo(GetArrayRandomElement<TargetPoint>(ePoints));
        }

        private void OnExpressArrival(TargetPoint[] points)
        {
            expressPassengers.Shuffle();
            platformPassengers.Iterate((TownPassenger passenger) =>
            {
                if (takeExpress.PositiveDesicion)
                {
                    TargetPoint expressDoor = expressGetIn.Next;
                    passenger.ClearTarget();
                    expressDoor.Assign(passenger);
                }
            });
        }

        private void ChangeSkin(TownPassenger passenger)
        {
            CharacterGraphics old = passenger.Graphics;
            passenger.AssignGraphics(charactersGraphicsPull.Next);
            charactersGraphicsPull.Shelf(old);
        }
    }

    [System.Serializable]
    public class Dilemma
    {
        [SerializeField] [Range(0f, 1f)]private float weight;
        public bool Decide()
        {
            if (Random.Range(0f, 1f) < weight)
                return true;
            return false;
        }

        public bool PositiveDesicion
        {
            get { return Decide(); }
        }
    }

    [System.Serializable]
    public class CharacterOrder
    {
        public bool Man { get { return man; } }
        [SerializeField] private bool man;

        public bool Name { get { return name; } }
        [SerializeField] private bool name;

        public float Height { get { return height; } }
        [SerializeField] private float height;
    }

    public class CharactersGraphicsPull : Pull<CharacterGraphics>
    {
        public CharactersGraphicsPull(Transform dock, CharacterGraphics[] characters) : base()
        {
            this.dock = dock;
            pairs = new Dictionary<CharacterGraphics, Vector3>();
            occupation = new Dictionary<CharacterGraphics, bool>();
            Vector3[] karree = Karree.Create(Vector3.zero, characters.Length, 1.3f);
            for (int i = 0; i < characters.Length; i++)
            {
                Add(characters[i]);
                pairs.Add(characters[i], karree[i]);
                occupation.Add(characters[i], false);
                characters[i].transform.SetParent(dock, false);
                characters[i].transform.localPosition = karree[i];
            }
        }
        private Transform dock;
        private Dictionary<CharacterGraphics, Vector3> pairs;
        private Dictionary<CharacterGraphics, bool> occupation;

        public override CharacterGraphics Get()
        {
            CharacterGraphics receiver = GetCycle();
            if (occupation[receiver])
                return Get();
            else
            {
                occupation[receiver] = true;
                return receiver;
            }
        }

        public void Shelf(CharacterGraphics reciver)
        {
            reciver.transform.SetParent(dock);
            reciver.transform.localPosition = pairs[reciver];
            occupation[reciver] = false;
        }
    }

    public class TownPassengersPull : Pull<TownPassenger>
    {
        public TownPassengersPull() : base() { }
        public override TownPassenger Get()
        {
            return base.GetRandom();
        }
    }

    public class PlatformPoints : Pull<PlatformPoint>
    {
        public PlatformPoints() : base() { }

        public override PlatformPoint Get()
        {
            List<PlatformPoint> unoccupiedPoints = new List<PlatformPoint>();
            foreach(PlatformPoint pp in list)
            {
                if (pp.OccupiedState == PlatformPoint.State.Free)
                    unoccupiedPoints.Add(pp);
            }
            if (unoccupiedPoints.Count == 0)
            {
                Debug.LogErrorFormat("No unoccupied points left");
            }
            return unoccupiedPoints[Random.Range(0, unoccupiedPoints.Count)];
        }
    }

    public class ExitPoints : Pull<TargetPoint>
    {
        public ExitPoints() : base() { }
        public override TargetPoint Get()
        {
            return GetRandom();
        }
    }

    public class ColorPull : Pull<Color>
    {
        public ColorPull(Color[] colors) : base()
        {
            this.list = new List<Color>(colors);
        }
        public override Color Get()
        {
            return base.GetCycle();
        }
    }
}
