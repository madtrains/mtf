using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTCharacters
{
    public class PolicemanPoint : PlatformPoint
    {
        [SerializeField] private Policeman policeman;

        protected override void ChangeStateAdd()
        {
            base.ChangeStateAdd();
            switch(_state)
            {
                case (State.Occupied):
                {
                    policeman.Busy = true;
                    passenger.BreakToTalk();
                    break;
                }
                default:
                {
                    policeman.Busy = false;
                    break;
                }
            }
        }
    }
}

