using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTCharacters
{
    public class PlatformPoint : TargetPoint
    {
        public UnityEngine.Events.UnityAction<PlatformPoint, TownPassenger> OnDone;

        [SerializeField] protected MinMax timer;
        protected TimerBeh timerBeh;

        public enum State { Free, Waiting, Occupied }

        public UnityEngine.Events.UnityAction<State> OnChangeState;

        public State OccupiedState { get { return _state; } }
        protected State _state;

        public override void Init()
        {
            base.Init();
            timerBeh = gameObject.AddComponent<TimerBeh>();
            timerBeh.OnEnd = End;
        }

        public override void Assign(TownPassenger passenger)
        {
            base.Assign(passenger);
            ChangeState(State.Waiting);
        }
        

        public override void Reached(TownPassenger passenger)
        {
            base.Reached(passenger);
            timerBeh.Launch(timer.Random);
            passenger.AnimState = AnimState.Basic;
            ChangeState(State.Occupied);
        }

        protected void End()
        {
            if (_state == State.Occupied)
                Done();
        }

        private void Done()
        {
            OnDone?.Invoke(this, passenger);
        }

        protected virtual void ChangeState(State newState)
        {
            if (this._state != newState)
            {
                this._state = newState;
                ChangeStateAdd();
                OnChangeState?.Invoke(newState);
            }
        }

        protected virtual void ChangeStateAdd()
        {

        }

        public override void Clear()
        {
            base.Clear();
            ChangeState(State.Free);
        }

        protected virtual void OnDrawGizmos()
        {
            Color myColor = Color.green;
            switch (_state)
            {
                case State.Waiting:
                    {
                        myColor = Color.yellow;
                        break;
                    }
                case State.Occupied:
                    {
                        myColor = Color.red;
                        break;
                    }
            }

            Gizmos.color = myColor;
            Gizmos.DrawSphere(transform.position, 0.52f);
            Gizmos.DrawLine(transform.position, transform.position + transform.TransformDirection(Vector3.forward));
        }
    }
}

