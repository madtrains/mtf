using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTCharacters
{
    public class DebateClubPoint : PlatformPoint
    {
        [SerializeField] DebateClubPoint[] otherPoints;


        protected override void ChangeStateAdd()
        {
            if (_state == State.Waiting)
                return;

            bool toTalk = false;
            
            foreach (DebateClubPoint otherPoint in otherPoints)
            {
                if (otherPoint.OccupiedState == State.Occupied)
                {
                    toTalk = true;
                    break;
                }
            }

            if (_state == State.Occupied && toTalk)
            {
                Talk();
            }

            ProcessOthers(toTalk);
        }

        public void Talk()
        {
            if (passenger.CurrentCollection != AnimCollection.Collection.Talk)
                passenger.BreakToTalk();
        }

        public void ShutUp()
        {
            passenger.BackToIdle();
            passenger.BrakeAnimatorPlay();
        }

        public void ProcessOthers(bool talk)
        {
            foreach (DebateClubPoint otherPoint in otherPoints)
            {
                if (otherPoint.OccupiedState == State.Occupied)
                {
                    if (talk)
                        otherPoint.Talk();
                    else
                        otherPoint.ShutUp();
                }
            }
        }
    }
}


