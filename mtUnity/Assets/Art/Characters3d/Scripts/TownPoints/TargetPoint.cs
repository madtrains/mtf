using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTCharacters
{
    public class TargetPoint : UberBehaviour
    {
        protected TownPassenger passenger;
        public UnityEngine.Events.UnityAction<TargetPoint, TownPassenger> OnReached;

        public virtual void Init()
        {

        }

        public virtual void Assign(TownPassenger passenger)
        {
            this.passenger = passenger;
            this.passenger.GoTo(this);
        }

        public virtual void Reached(TownPassenger passenger)
        {
            OnReached?.Invoke(this, passenger);
        }


        public virtual void Clear()
        {
            this.passenger = null;
        }
    }
}

