using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTCharacters
{
    public class ClockPoint : PlatformPoint
    {

        protected override void ChangeStateAdd()
        {
            base.ChangeStateAdd();
            switch(_state)
            {
                case (State.Occupied):
                { 
                    passenger.BreakToImpatience();
                    break;
                }
                default:
                {
                    break;
                }
            }
        }
    }
}

