using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTCharacters
{
    public class OverhearPoint : PlatformPoint
    {

        protected override void ChangeStateAdd()
        {
            base.ChangeStateAdd();
            switch(_state)
            {
                case (State.Occupied):
                { 
                    passenger.BreakToTalk();
                    break;
                }
                default:
                {
                    break;
                }
            }
        }
    }
}

