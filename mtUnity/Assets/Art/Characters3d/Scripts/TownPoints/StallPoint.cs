using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTCharacters
{
    public class StallPoint : PlatformPoint
    {
        [SerializeField] private Seller seller;

        protected override void ChangeStateAdd()
        {
            base.ChangeStateAdd();
            switch(_state)
            {
                case (State.Occupied):
                {
                    seller.Busy = true;
                    passenger.BreakToCustomer();
                    break;
                }
                default:
                {
                    seller.Busy = false;
                    break;
                }
            }
        }
    }
}

