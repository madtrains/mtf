using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTCharacters
{
    public class CharacterGraphics : MonoBehaviour
    {
        public Animator Animator { get { return animator; } set { animator = value; } }
        [SerializeField] protected Animator animator;

        public Transform Head { get { return head; }  }
        [SerializeField] protected Transform head;

        public UnityEngine.Events.UnityAction OnEventAnimPlay;
        public UnityEngine.Events.UnityAction OnEventAnimEnd;
        public void EventAnimPlay()
        {
            OnEventAnimPlay?.Invoke();
        }

        public void EventAnimEnd()
        {
            OnEventAnimEnd?.Invoke();
        }
    }
}

