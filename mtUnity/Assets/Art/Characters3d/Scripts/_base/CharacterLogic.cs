using UnityEngine;

namespace MTCharacters
{
    public enum AnimState { Basic, Walk }

    public class CharacterLogic : UberBehaviour
    {
        public AnimCollection.Collection CurrentCollection { get { return currentCollection; } }
        public CharacterGraphics Graphics { get { return graphics; } }

        public static readonly int hIndex = Animator.StringToHash("index");
        public static int hSpeed = Animator.StringToHash("speed");
        public static int hState = Animator.StringToHash("state");
        public static int hOffset = Animator.StringToHash("offset");
        public static int hBrake = Animator.StringToHash("brake");

        public UnityEngine.Events.UnityAction<AnimState> OnChangeAnimState;

        [SerializeField] protected CharacterGraphics graphics;
        [SerializeField] protected AnimCollection globalCollection;
        protected AnimState _animstate;
        protected AnimCollection.Collection currentCollection;
        protected bool graphicsAssigned;

        public AnimState AnimState { get { return _animstate; } set { ChangeMainAnimState(value); } }
        public bool IsVisibleAndActive { get { return GetVisibleAndActive(); } }

        public virtual void AssignGraphics(CharacterGraphics graphics)
        {
            this.graphics = graphics;
            this.graphics.transform.SetParent(transform, true);
            this.graphics.transform.localPosition = Vector3.zero;
            this.graphics.transform.localRotation = Quaternion.identity;
            this.graphics.OnEventAnimPlay = EventAnimPlay;
            this.graphics.OnEventAnimEnd = EventAnimEnd;
            this.graphicsAssigned = true;
        }

        public virtual void Init(
            AnimCollection globalCollection, 
            AnimCollection.Collection currentCollection)
        {
            this.globalCollection = globalCollection;
            this.currentCollection = currentCollection;
            RandomizeAnimatorOffset();
            ChangeAnim();
        }

        public virtual void Start()
        {

        }

        public virtual void EventAnimPlay()
        {
            ChangeAnim();
        }

        public virtual void EventAnimEnd()
        {

        }

        protected virtual void ChangeAnim()
        {
            if (!IsVisibleAndActive)
                return;
            RandomizeAnimatorOffset();
            int index = globalCollection.GetRandomIndex(currentCollection);
            graphics.Animator.SetInteger(hIndex, index);
        }

        protected virtual bool GetVisibleAndActive()
        {
            return graphicsAssigned;
        }

        protected virtual void BrakeChangeCollection(AnimCollection.Collection newCollection)
        {
            if (!IsVisibleAndActive)
                return;
            currentCollection = newCollection;
            BrakeAnimatorPlay();
            ChangeAnim();
        }

        public void BrakeAnimatorPlay()
        {
            if (!IsVisibleAndActive)
                return;

            if (graphics == null)
            {
                Debug.LogErrorFormat("No graphics assigned {0}", gameObject.name);
                return;
            }
                
            if (graphics.Animator == null)
            {
                Debug.LogErrorFormat("No Animator assigned {0}", gameObject.name);
                return;
            }
                
            graphics.Animator.SetTrigger(hBrake);
        }

        public virtual void RandomizeAnimatorOffset()
        {
            if (!IsVisibleAndActive)
                return;
            graphics.Animator.SetFloat(hOffset, UnityEngine.Random.Range(0f, 1f));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newState">Basic or Walk</param>
        protected virtual void ChangeMainAnimState(AnimState newState)
        {
            if (!IsVisibleAndActive)
                return;

            if (newState == _animstate)
                return;

            _animstate = newState;
            graphics.Animator.SetInteger(hState, (int)_animstate);
            BrakeAnimatorPlay();
            ChangeAnim();
            OnChangeAnimState?.Invoke(_animstate);
        }



    }
}

