using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class VisibilityChecker : MonoBehaviour
{
    public UnityEngine.Events.UnityAction<bool> OnVisibilityChanged;
    private bool isVisible;

    protected virtual void OnBecameInvisible()
    {
        isVisible = false;
        OnVisibilityChanged?.Invoke(isVisible);
    }

    protected virtual void OnBecameVisible()
    {
        isVisible = true;
        OnVisibilityChanged?.Invoke(isVisible);
    }
}
