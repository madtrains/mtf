using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTCharacters
{
    public class AnimCollection : UberBehaviour
    {
        public enum Collection { 
            Idle, Talk, Overhear, Customer, Seller, ImpatientPassenger, 
            Policeman, IdlePatient }
        public AnimList[] lists;

        public void Init()
        {
            foreach (AnimList aList in lists)
            {
                aList.Init();
            }
        }

        public int GetRandomIndex(Collection collection)
        {
            int index = (int)collection;
            return lists[index].GetIndex();
        }
    }

    [System.Serializable]
    public class AnimList
    {
        [SerializeField] private string name;
        [SerializeField] private WeightedAnimation[] animations;
        protected WeightList<WeightedAnimation> weightedList;

        public void Init()
        {
            weightedList = new WeightList<WeightedAnimation>();
            for (int i = 0; i < animations.Length; i++)
            {
                weightedList.Add(animations[i]);
            }
        }

        public int GetIndex()
        {
            return weightedList.GetRandomElement().anim.Index;
        }
    }


    [System.Serializable]
    public class WeightedAnimation : WeightedElement
    {
        public IndexedAnim anim;
    }
}

