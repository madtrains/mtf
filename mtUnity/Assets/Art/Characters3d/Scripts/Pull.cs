using System.Collections.Generic;
using UnityEngine;

namespace MTCharacters
{
    /// <summary>
    /// ������ � ����������� ������� ����� - ������, ����������� �����
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Pull<T>
    {
        public delegate void IterateMethod(T t);
        public Pull()
        {
            list = new List<T>();
            i = 0;
        }

        public T Next { get { return Get(); } }

        protected List<T> list;
        protected int i = 0;

        public virtual void Add(T item)
        {
            list.Add(item);
        }

        public virtual T Get()
        {
            return list[0];
        }

        protected virtual T GetCycle()
        {
            T result = list[i];
            i += 1;
            if (i >= list.Count)
                i = 0;
            return result;
        }

        protected virtual T GetRandom()
        {
            i = Random.Range(0, list.Count);
            return list[i];
        }

        public void Shuffle()
        {
            System.Random rng = new System.Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
            i = Random.Range(0, list.Count);
        }

        public void Iterate(IterateMethod method)
        { 
            foreach (T el in list)
            {
                method(el);
            }
        }
    }
}
