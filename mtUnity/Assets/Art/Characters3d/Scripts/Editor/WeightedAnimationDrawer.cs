using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace MTCharacters
{
	/*
	[CustomPropertyDrawer(typeof(PreAssetElement))]
	class PreAssetElementDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			Rect main = new Rect(position);
			main = EditorGUI.IndentedRect(main);
			int indent = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 0;

			int numberW = 25;
			int resourceW = 75;
			int mWidth = (int)main.width - (resourceW + numberW);
			int cellSize = mWidth / 2;

			Rect r1 = new Rect(main.x, main.y, numberW, main.height); //id
			Rect r2 = new Rect(r1.max.x, main.y, cellSize, main.height); //name
			Rect r3 = new Rect(r2.max.x, position.y, resourceW, main.height); //dropbox
			Rect r4 = new Rect(r3.max.x, position.y, cellSize, main.height); //object

			string[] spl = property.displayName.Split(' '); // taking index from string such as Element 0 or Element 1
			int index = -1;
			int.TryParse(spl[1], out index);


			SerializedProperty obj = property.FindPropertyRelative("obj");
			SerializedProperty assetType = property.FindPropertyRelative("assetType");

			UnityEngine.Object uObject = obj.objectReferenceValue;

			GUI.Label(r1, index.ToString());

			if (uObject != null)
			{
				GUI.Label(r2, "\"" + uObject.name + "\"");
			}
			else
			{
				GUI.Label(r2, "...");
			}

			EditorGUI.PropertyField(r3, assetType, GUIContent.none, false);
			EditorGUI.PropertyField(r4, obj, GUIContent.none, false);
			EditorGUI.indentLevel = indent;
		}
	*/
	[CustomPropertyDrawer(typeof(WeightedAnimation))]
	public class WeightedAnimationDrawer : PropertyDrawer
	{
		public static readonly Color ErrorColor = new Color(1f, 0.2f, 0f, 0.1f);
		public static readonly Color OkColor = new Color(0.2f, 1f, 0f, 0.01f);

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			Rect main = new Rect(position);
			main = EditorGUI.IndentedRect(main);
			int indent = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 0;

			int fixedW = 30;
			int cellSize = (int)((main.width - fixedW) / 2) ;

			Rect r1 = new Rect(main.x, main.y, fixedW, main.height); //weight
			Rect r2 = new Rect(r1.max.x, main.y, cellSize, main.height); //name
			Rect r3 = new Rect(r2.max.x, main.y, cellSize, main.height); //field


			SerializedProperty weight = property.FindPropertyRelative("weight");
			SerializedProperty anim = property.FindPropertyRelative("anim");
			UnityEngine.Object uObject = anim.objectReferenceValue;

			string lableString = "No anim!";
			if (uObject != null)
			{
				lableString = uObject.name.ToString();
			}
			else
            {
				EditorGUI.DrawRect(r2, ErrorColor);
			}
			EditorGUI.PropertyField(r1, weight, GUIContent.none, false);
			if (weight.floatValue == 0)
				EditorGUI.DrawRect(r1, ErrorColor);
			else
				EditorGUI.DrawRect(r1, OkColor);
			GUI.Label(r2, lableString);
			EditorGUI.PropertyField(r3, anim, GUIContent.none, false);
			
		}
	}
}

