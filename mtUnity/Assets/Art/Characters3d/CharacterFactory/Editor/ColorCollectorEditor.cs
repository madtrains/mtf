﻿using UnityEditor;
using UnityEngine;

namespace WaySystem.Cargos.Passengers
{
    [CustomEditor(typeof(ColorCollector))]
    public class ColorCollectorEditor : UberEditor
    {
        public Renderer renderer;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            renderer = EditorGUILayout.ObjectField(renderer, typeof(Renderer), true) as Renderer;
            if (GUILayout.Button("Collect"))
            {
                ColorCollector cc = (ColorCollector)target;
                Color[] colors = new Color[renderer.materials.Length];
                for (int i = 0; i < renderer.materials.Length; i++)
                {
                    colors[i] = renderer.materials[i].color;
                }
                cc.Set(colors);
            }
        }
    }
}