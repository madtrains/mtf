﻿using UnityEngine;

namespace MTCharacters.CharacterFactory
{
    [System.Serializable]
    public class MeshElement : WeightedElement
    {
        public MeshContainer MeshContainer { get { return meshContainer; } }
        [SerializeField] private MeshContainer meshContainer;

        public void Instantiate(Transform target, Color color)
        {
            MeshContainer mc = 
                GameObject.Instantiate<MeshContainer>(meshContainer);
            mc.ApplyColor(color);
            mc.MeshTransform.SetParent(target, true);
            GameObject.Destroy(mc.gameObject);
        }

        public void Instantiate(Transform target, Color[] colors)
        {
            MeshContainer mc =
                GameObject.Instantiate<MeshContainer>(meshContainer);
            mc.ApplyColors(colors);
            mc.MeshTransform.SetParent(target, true);
            GameObject.Destroy(mc.gameObject);
        }

        public void Instantiate(Transform target)
        {
            MeshContainer mc =
                GameObject.Instantiate<MeshContainer>(meshContainer);
            mc.MeshTransform.SetParent(target, true);
            GameObject.Destroy(mc.gameObject);
        }
    }
}