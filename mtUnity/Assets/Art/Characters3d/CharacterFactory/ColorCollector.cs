﻿using UnityEngine;

namespace WaySystem.Cargos.Passengers
{
    public class ColorCollector : UberBehaviour
    {
        [SerializeField] private Color[] colors;

        public void Set(Color[] colors) 
        { 
            this.colors = colors;
        }
    }
}