﻿using UnityEngine;

namespace MTCharacters.CharacterFactory
{
    [System.Serializable]
    public class RenderElement : WeightedElement
    {
        public Renderer Rend { get { return rend; } }
        [SerializeField] private Renderer rend;



    }
}