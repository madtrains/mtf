﻿using MTCharacters.CharacterFactory.Kreator;
using System.Collections.Generic;
using UnityEngine;

namespace MTCharacters.CharacterFactory
{
    public class SoldierContainer : CharacterContainer
    {

        [SerializeField] private Settings.SingleAddition<Renderer> pistol;
        [SerializeField] private Settings.SingleAddition<Renderer> rifle;

        public override void Init(Color hairColor, Color[] dress, Color[] hat)
        {
            base.Init(hairColor, dress, hat);
            if (rifle.Off)
                Destroy(rifle.Value.gameObject);
            if (pistol.Off)
                Destroy(pistol.Value.gameObject);
        }
    }
}