﻿using NUnit.Framework;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace MTCharacters.CharacterFactory
{
    [CustomEditor(typeof(CharacterContainer))]
    public class CharacterContainerEditor : UberEditor
    {
        private int index;
        CharacterContainer characterContainer;
        public Color[] colors;

        public override void OnInspectorGUI()
        {
            characterContainer = (CharacterContainer)target;
            base.OnInspectorGUI();
            if (GUILayout.Button("Set"))
            {
                ColorVariation cv = characterContainer.ColorVariations[index];
                characterContainer.PaintRendererShared(characterContainer.Dress, cv.Colors);
                index += 1;
                if (index == characterContainer.ColorVariations.Length)
                {
                    index = 0;
                }
            }
            if (GUILayout.Button("Gather"))
            {
                this.colors = new Color[characterContainer.Dress.sharedMaterials.Length];
                for (int i = 0; i < characterContainer.Dress.sharedMaterials.Length; i++)
                {
                    this.colors[i] = characterContainer.Dress.sharedMaterials[i].color;
                }
                List<ColorVariation> list = new List<ColorVariation>(characterContainer.ColorVariations);
                ColorVariation colorVariation  = new ColorVariation();
                colorVariation.Fill(this.colors);
                list.Add(colorVariation);
                characterContainer.ColorVariations = list.ToArray();
            }
        }
    }

    [CustomEditor(typeof(FemaleCharacterContainer))]
    public class FemaleCharacterContainerEditor : CharacterContainerEditor
    {

    }

    [CustomEditor(typeof(SoldierContainer))]
    public class SoldierContainerEditor : CharacterContainerEditor
    {

    }
}