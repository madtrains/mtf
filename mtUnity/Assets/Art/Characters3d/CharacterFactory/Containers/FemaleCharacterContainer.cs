﻿using System.Collections.Generic;
using UnityEngine;

namespace MTCharacters.CharacterFactory
{
    public class FemaleCharacterContainer : CharacterContainer
    {
        public SkinnedMeshRenderer[] SkinnedHairs { get { return skinnedHairs; } }
        [SerializeField] private SkinnedMeshRenderer[] skinnedHairs;

        public override void Init(Color hairColor, Color[] dress, Color[] hat)
        {
            base.Init(hairColor, dress, hat);
            int usedHairs  = Random.Range(0, skinnedHairs.Length);
            for (int i = 0; i < skinnedHairs.Length; i++)
            {
                if (i == usedHairs)
                {
                    PaintRenderer(skinnedHairs[i], hairColor);
                }
                else
                {
                    Activate(skinnedHairs[i].gameObject, false);
                    Destroy(skinnedHairs[i].gameObject);
                }
            }
        }
        /*
        public Renderer[] SetGetRenderers(List<Renderer> renderers)
        {
            renderers.Add(Dress);
            renderers.Add(Body);
            this.renderers = renderers.ToArray();
            return this.renderers;
        }
        


        //[SerializeField] private Transform pelvis;


        //[SerializeField] private Renderer[] renderers;
        [SerializeField] private SkinnedMeshRenderer[] skinnedHairs;
        
        [SerializeField] private CompleteCollection<ColorVariation> colors;
        */
    }
}