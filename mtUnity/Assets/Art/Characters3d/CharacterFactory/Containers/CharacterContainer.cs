﻿using UnityEngine;

namespace MTCharacters.CharacterFactory
{
    public class CharacterContainer : UberBehaviour
    {
        public CharacterGraphics CharacterGraphics { get { return characterGraphics; } }
        [SerializeField] protected CharacterGraphics characterGraphics;

        public SkinnedMeshRenderer Dress { get { return dress; } }
        [SerializeField] protected SkinnedMeshRenderer dress;

        public SkinnedMeshRenderer Body { get { return body; } }
        [SerializeField] protected SkinnedMeshRenderer body;

        public ColorVariation[] ColorVariations { get { return colorVariations; } set { colorVariations = value; } }
        [SerializeField] private ColorVariation[] colorVariations;


        public virtual void Init(Color hairColor, Color[] dress, Color[] hat)
        {
            PaintRenderer(this.dress, dress);
        }

        public void PaintRendererShared(Renderer target, Color[] colors)
        {
            for (int i = 0; i < Mathf.Min(target.sharedMaterials.Length, colors.Length); i++)
            {
                Material mat = target.sharedMaterials[i];
                mat.color = colors[i];
            }
        }

        public void PaintRenderer(Renderer target, Color[] colors)
        {
            for (int i = 0; i < Mathf.Min(target.materials.Length, colors.Length); i++)
            {
                Material mat = target.materials[i];
                mat.color = colors[i];
            }
        }

        protected void PaintRenderer(Renderer target, Color color)
        {
            for (int i = 0; i < target.materials.Length; i++)
            {
                Material mat = target.materials[i];
                mat.color = color;
            }
        }


        /*
        //public SkinnedMeshRenderer[] SkinnedHairs { get { return skinnedHairs; } }
        //public Renderer[] Renderers { get { return renderers; } }

        
        public Renderer[] SetGetRenderers(List<Renderer> renderers)
        {
            renderers.Add(Dress);
            renderers.Add(Body);
            this.renderers = renderers.ToArray();
            return this.renderers;
        }
        


        //[SerializeField] private Transform pelvis;


        //[SerializeField] private Renderer[] renderers;
        [SerializeField] private SkinnedMeshRenderer[] skinnedHairs;
        
        [SerializeField] private CompleteCollection<ColorVariation> colors;
        */
    }
}