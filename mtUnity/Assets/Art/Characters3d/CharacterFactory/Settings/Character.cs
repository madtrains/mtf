﻿using UnityEngine;

namespace MTCharacters.CharacterFactory.Settings
{
    [System.Serializable]
    public class Character : WeightedElement
    {
        public CharacterContainer Container { get { return container; } }
        [SerializeField] CharacterContainer container;

        public Addition<MeshElement> Hats { get { return hats; } }
        [SerializeField] private Addition<MeshElement> hats;
    }
}