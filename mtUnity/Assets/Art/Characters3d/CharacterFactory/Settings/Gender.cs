﻿using UnityEngine;
using UnityEngine.TextCore.Text;

namespace MTCharacters.CharacterFactory.Settings
{
    [System.Serializable]
    public class Gender
    {
        public MinMax Height { get { return height; } }
        [SerializeField] private MinMax height;

        public Character[] Characters { get { return characters; } }
        [SerializeField] private Character[] characters;
    }


    [System.Serializable]
    public class Men : Gender
    {
        public Addition<MeshElement> Hairs { get { return hairs; } }
        [SerializeField] private Addition<MeshElement> hairs;

        public Addition<MeshElement> FacialHairs { get { return facialHairs; } }
        [SerializeField] private Addition<MeshElement> facialHairs;

        public Addition<MeshElement> Smoker { get { return smoker; } }
        [SerializeField] private Addition<MeshElement> smoker;
    }

    [System.Serializable]
    public class Women : Gender
    {

    }
}