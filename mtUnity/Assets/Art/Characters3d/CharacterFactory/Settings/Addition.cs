﻿using System.ComponentModel;
using UnityEngine;
using UnityEngine.TextCore.Text;

namespace MTCharacters.CharacterFactory.Settings
{
    [System.Serializable]
    public class Addition<T> where T : WeightedElement
    {
        public T[] Array { get { return array; } }

        public bool Off { get { return !On; } }

        public bool On
        {
            get
            {
                if (probability == 0)
                    return false;
                float randomValue = Random.Range(0.01f, 1f);
                return probability >= randomValue;
            }
        }

        [Tooltip("0: Always Disabled, 1: Always Enabled")]
        [SerializeField][Range(0f, 1f)] protected float probability;
        [SerializeField] protected T[] array;
    }

    [System.Serializable]
    public class SingleAddition<T> where T : UnityEngine.Component
    {
        public T Value { get { return value; } }

        public bool Off { get { return !On; } }

        public bool On
        {
            get
            {
                if (probability == 0)
                    return false;
                float randomValue = Random.Range(0.01f, 1f);
                return probability >= randomValue;
            }
        }

        [Tooltip("0: Always Disabled, 1: Always Enabled")]
        [SerializeField][Range(0f, 1f)] protected float probability;
        [SerializeField] protected T value;
    }
}