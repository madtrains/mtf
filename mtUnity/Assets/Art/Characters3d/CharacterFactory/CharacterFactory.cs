﻿using UnityEngine;
using WaySystem.Cargos;

namespace MTCharacters.CharacterFactory
{
    public class CharacterFactory
    {
        public delegate void ProcessContainer(CharacterContainer container);
        public AnimCollection MainAnimCollection { get { return animCollection; } }

        public CharacterFactory(PassengersSO passengersSO)
        {
            this.passengersSO = passengersSO;
            this.hairColors = new WeightedCollection<HairColor>(passengersSO.HairColors);
            this.men = new Kreator.Men(passengersSO.Men);
            this.women = new Kreator.Women(passengersSO.Women);
            this.animCollection =
                GameObject.Instantiate<AnimCollection>(passengersSO.MainAnimCollection);
            animCollection.Init();
        }

        public delegate void ProcessCreatedPassenger(Passenger passenger);
        private PassengersSO passengersSO;

        private WeightedCollection<HairColor> hairColors;
        private Kreator.Men men;
        private Kreator.Women women;
        private AnimCollection animCollection;

        public CharacterGraphics CreateRandom(bool man, ProcessContainer process)
        {
            OverrideName oColor = new OverrideName();
            OverrideName oName = new OverrideName();
            OverrideName oDressColor = new OverrideName();
            OverrideHeight oHeight = new OverrideHeight();
            OverrideAddition oHats = new OverrideAddition();
            OverrideAddition oHairs = new OverrideAddition();
            OverrideAddition oFacialHairs = new OverrideAddition();
            OverrideAddition oSmoker = new OverrideAddition();

            Color hairColor = hairColors.Get(oColor).Color;
            CharacterContainer container;

            if (man)
            {
                container = men.Kreate(hairColor, oName, oDressColor, oHeight,
                    oHats, oHairs, oFacialHairs, oSmoker);
            }

            else
            {
                container = women.Kreate(hairColor, oName, oDressColor, oHeight,
                    oHats);
            }

            process(container);
            CharacterGraphics result = container.CharacterGraphics;
            GameObject.Destroy(container);
            return result;
        }

        public CharacterGraphics CreateMan(
            ProcessContainer process,
            OverrideName oColor, 
            OverrideName oName,
            OverrideName oDressColor,
            OverrideHeight oHeight,
            OverrideAddition oHats,
            OverrideAddition oHairs,
            OverrideAddition oFacialHairs,
            OverrideAddition oSmoker
        )
        {
            Color hairColor = hairColors.Get(oColor).Color;
            CharacterContainer container = 
                men.Kreate(
                    hairColor, oName, oDressColor, oHeight,
                    oHats, oHairs, oFacialHairs, oSmoker);
            process(container);
            CharacterGraphics result = container.CharacterGraphics;
            GameObject.Destroy(container);
            return result;
        }

        public CharacterGraphics CreateWoman(
            ProcessContainer process,
            OverrideName oColor,
            OverrideName oName,
            OverrideName oDressColor,
            OverrideHeight oHeight,
            OverrideAddition oHats
        )
        {
            Color hairColor = hairColors.Get(oColor).Color;
            CharacterContainer container =
                women.Kreate(
                    hairColor, oName, oDressColor, oHeight,
                    oHats);
            process(container);
            CharacterGraphics result = container.CharacterGraphics;
            GameObject.Destroy(container);
            return result;
        }
    }
}
