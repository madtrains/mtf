﻿using UnityEngine;

namespace MTCharacters.CharacterFactory
{
    public class OverrideRequest<T>
    {
        public bool overrideIsOn;
        public T value;

        public OverrideRequest()
        {
            this.overrideIsOn = false;
            this.value = default(T);
        }

        public OverrideRequest(T value)
        {
            this.overrideIsOn = true;
            this.value = value;
        }
    }

    public class OverrideName : OverrideRequest<string>
    {
        public OverrideName() : base() { }
        public OverrideName(string name) : base(name) { }
    }

    public class OverrideHeight : OverrideRequest<float>
    {
        public OverrideHeight() : base() { }
        public OverrideHeight(float height) : base(height) { }
    }

    public class OverrideAddition : OverrideName
    {
        public OverrideAddition() : base() 
        {
            this.additionEnabled = false;
        }

        public OverrideAddition(string name, bool additionEnabled) : base(name)
        {
            this.additionEnabled = additionEnabled;
        }
        public bool additionEnabled;
    }
}