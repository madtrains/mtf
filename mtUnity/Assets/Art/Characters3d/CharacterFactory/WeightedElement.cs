﻿using UnityEngine;

namespace MTCharacters.CharacterFactory
{
    [System.Serializable]
    public class WeightedElement
    {
        public string Name { get { return name; } }
        public float Weight { get { return weight; } }

        [SerializeField] protected string name;
        [SerializeField] protected float weight;
    }
}