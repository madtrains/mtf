﻿using UnityEngine;

namespace MTCharacters.CharacterFactory
{
    [System.Serializable]
    public class HairColor : WeightedElement
    {
        public Color Color { get { return color; } }
        [SerializeField] private Color color;
    }

    [System.Serializable]
    public class ColorVariation : WeightedElement
    {
        public Color[] Colors { get { return colors; } }
        public Color[] Hat { get { return hat; } }

        [SerializeField] private Color[] colors;
        [SerializeField] private Color[] hat;

        public void Fill(Color[] colors)
        {
            this.colors = colors;
            this.hat = new Color[1] { Color.white};
        }
    }
}