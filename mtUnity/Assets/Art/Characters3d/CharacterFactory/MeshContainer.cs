﻿using UnityEngine;
using static UnityEngine.GraphicsBuffer;

namespace MTCharacters.CharacterFactory
{
    public class MeshContainer : UberBehaviour
    {
        public Renderer Renderer { get { return rend; } }
        public Transform MeshTransform { get { return meshTransform; } }

        [SerializeField] Renderer rend;
        [SerializeField] Transform meshTransform;
        [SerializeField] bool noColoring;
        [SerializeField] int[] overrideIndexes;

        public void ApplyColor(Color color)
        {
            rend.material.color = color;
        }

        public void ApplyColors(Color[] colors)
        {
            if (noColoring)
                return;
            if (overrideIndexes.Length == 0)
            {
                for (int i = 0; i < Mathf.Min(rend.materials.Length, colors.Length); i++)
                {
                    Material mat = rend.materials[i];
                    mat.color = colors[i];
                }
            }
            else
            {
                if (colors.Length < overrideIndexes.Length)
                {
                    Debug.LogErrorFormat("Not enough colors to apply {0}", this.name);
                }
                for (int i = 0; i < overrideIndexes.Length; i++)
                {
                    int rendMaterialsIndex = overrideIndexes[i];
                    if (rendMaterialsIndex >= rend.materials.Length)
                    {
                        Debug.LogErrorFormat("Color Apply error. Invalid index {0} {1}", 
                            rendMaterialsIndex, this.name);
                    }
                    
                    Material mat = rend.materials[rendMaterialsIndex];
                    mat.color = colors[i];
                }
            }
            
        }
    }

    /*
     * public void Test()
    {
        //UberCharacter uc = DefineObjAs<GameObject>().GetComponent<UberCharacter>();
        //GameObject newCharacter = uc.Clone(UberBehaviour.RandomBool);
        //newCharacter.transform.position = Vector3.right * offset;

        Transform newRootBone = SearchBoneByName(rootBone, smr.rootBone.name);
        Dictionary<string, Transform> dct = new Dictionary<string, Transform>();
        AddToDict(newRootBone, ref dct);
        List<Transform> list = new List<Transform>();
        foreach (Transform bone in smr.bones)
        {
            list.Add(dct[bone.name]);
        }
        smr.bones = list.ToArray();
        //offset += step;
    }

    private Transform SearchBoneByName(Transform rootBone, string name)
    {
        if (rootBone.name == name)
            return rootBone;
        for (int i = 0; i < rootBone.childCount; i++)
        {
            Transform child = rootBone.GetChild(i);
            return SearchBoneByName(child, name);
        }
        return null;
    }

    private void AddToDict(Transform rootBone, ref Dictionary<string, Transform> dct)
    {
        dct[rootBone.name] = rootBone;
        for (int i = 0; i < rootBone.childCount; i++)
        {
            Transform child = rootBone.GetChild(i);
            AddToDict(child, ref dct);
        }
    }
     * */
}