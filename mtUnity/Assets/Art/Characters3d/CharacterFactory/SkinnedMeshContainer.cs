﻿using System.Collections.Generic;
using UnityEngine;

namespace MTCharacters.CharacterFactory
{
    public class SkinnedMeshContainer : UberBehaviour
    {
        public SkinnedMeshRenderer SkinnedMeshRenderer { get { return skinnedMeshRenderer; } }
        public Transform MeshTransform { get { return meshTransform; } }

        [SerializeField] SkinnedMeshRenderer skinnedMeshRenderer;
        [SerializeField] Transform meshTransform;

        public void Retarget(Transform rootBone, Transform objectRoot)
        {
            Transform newRootBone = SearchBoneByName(rootBone, skinnedMeshRenderer.rootBone.name);
            Dictionary<string, Transform> dct = new Dictionary<string, Transform>();
            AddToDict(newRootBone, ref dct);
            List<Transform> list = new List<Transform>();
            foreach (Transform bone in skinnedMeshRenderer.bones)
            {
                list.Add(dct[bone.name]);
            }
            skinnedMeshRenderer.bones = list.ToArray();
            skinnedMeshRenderer.rootBone = newRootBone;
            meshTransform.SetParent(objectRoot);
        }

        private Transform SearchBoneByName(Transform rootBone, string name)
        {
            if (rootBone.name == name)
                return rootBone;
            for (int i = 0; i < rootBone.childCount; i++)
            {
                Transform child = rootBone.GetChild(i);
                return SearchBoneByName(child, name);
            }
            return null;
        }

        private void AddToDict(Transform rootBone, ref Dictionary<string, Transform> dct)
        {
            dct[rootBone.name] = rootBone;
            for (int i = 0; i < rootBone.childCount; i++)
            {
                Transform child = rootBone.GetChild(i);
                AddToDict(child, ref dct);
            }
        }
    }

    /*
     * public void Test()
    {
        //UberCharacter uc = DefineObjAs<GameObject>().GetComponent<UberCharacter>();
        //GameObject newCharacter = uc.Clone(UberBehaviour.RandomBool);
        //newCharacter.transform.position = Vector3.right * offset;

        Transform newRootBone = SearchBoneByName(rootBone, smr.rootBone.name);
        Dictionary<string, Transform> dct = new Dictionary<string, Transform>();
        AddToDict(newRootBone, ref dct);
        List<Transform> list = new List<Transform>();
        foreach (Transform bone in smr.bones)
        {
            list.Add(dct[bone.name]);
        }
        smr.bones = list.ToArray();
        //offset += step;
    }

    private Transform SearchBoneByName(Transform rootBone, string name)
    {
        if (rootBone.name == name)
            return rootBone;
        for (int i = 0; i < rootBone.childCount; i++)
        {
            Transform child = rootBone.GetChild(i);
            return SearchBoneByName(child, name);
        }
        return null;
    }

    private void AddToDict(Transform rootBone, ref Dictionary<string, Transform> dct)
    {
        dct[rootBone.name] = rootBone;
        for (int i = 0; i < rootBone.childCount; i++)
        {
            Transform child = rootBone.GetChild(i);
            AddToDict(child, ref dct);
        }
    }
     * */
}