﻿using UnityEngine;

namespace MTCharacters.CharacterFactory
{
    [CreateAssetMenu(menuName = "PassengersSO")]
    public class PassengersSO : ScriptableObject
    {
        public HairColor[] HairColors { get { return hairColors; } }
        public AnimCollection MainAnimCollection { get { return mainAnimCollection; } } 

        [SerializeField] HairColor[] hairColors;

        [SerializeField] AnimCollection mainAnimCollection;

        public Settings.Men Men { get { return men; } }
        [SerializeField] Settings.Men men;

        public Settings.Women Women { get { return women; } }
        [SerializeField] Settings.Women women;
    }
}