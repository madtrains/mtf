﻿using System.Collections.Generic;
using Unity.Properties;
using UnityEngine;
using static UnityEngine.Networking.UnityWebRequest;

namespace MTCharacters.CharacterFactory.Kreator
{
    [System.Serializable]
    public class Gender
    {
        public static readonly float HEIGHT = 180f;

        protected MinMax heightScale;
        protected WeightedCollection<Character> charactersCollection;
        protected Dictionary<string, Character> characterDictionary;

        public Gender(Settings.Gender gender)
        {
            characterDictionary = new Dictionary<string, Character>();
            heightScale = new MinMax(gender.Height.Min / HEIGHT, gender.Height.Max / HEIGHT);
            Character[] characters = new Character[gender.Characters.Length];
            for (int i = 0; i < gender.Characters.Length; i++)
            {
                Settings.Character settingsCharacter = gender.Characters[i];
                Character character = new Character(settingsCharacter);
                characters[i] = character;
                characterDictionary[settingsCharacter.Name] = character;
            }
            charactersCollection = new WeightedCollection<Character>(characters);
        }

        
        protected virtual CharacterContainer CreateContainer(
            Color hairColor,
            OverrideName characterName, 
            OverrideName colorVariation,
            OverrideAddition hat)
        {
            Character character = charactersCollection.Get(characterName);
            CharacterContainer result = character.Create(hairColor, colorVariation, hat);
            return result;
        }

        protected virtual void Scale(CharacterContainer characterContainer, OverrideHeight overrideHeight)
        {
            float scale = overrideHeight.overrideIsOn ?
                   overrideHeight.value / HEIGHT
                   :
                   heightScale.Random;

            characterContainer.transform.localScale = Vector3.one * scale;
        }

    }

    [System.Serializable]
    public class Men : Gender
    {
        private Kreator.Addition<MeshElement> hairs;
        private Kreator.Addition<MeshElement> facialHairs;
        private Kreator.Addition<MeshElement> smoker;

        public Men(Settings.Gender gender) : base(gender)
        {
            Settings.Men settingsMen = gender as Settings.Men;
            this.hairs = new Kreator.Addition<MeshElement>(settingsMen.Hairs);
            this.facialHairs = new Kreator.Addition<MeshElement>(settingsMen.FacialHairs);
            this.smoker = new Kreator.Addition<MeshElement>(settingsMen.Smoker);
        }

        public CharacterContainer Kreate(
            Color hairColor,
            OverrideName characterName,
            OverrideName colorVariation,
            OverrideHeight heightOverride,
            OverrideAddition hatsOverride,
            OverrideAddition hairsOverride,
            OverrideAddition facialHairsOverride,
            OverrideAddition smokerOverride)
        {
            CharacterContainer characterContainer = 
                CreateContainer(hairColor, characterName, colorVariation, hatsOverride);
            bool hairsAreOn = hairsOverride.overrideIsOn ? hairsOverride.additionEnabled : hairs.On;
            if (hairsAreOn)
            {
                MeshElement currentHairs = hairs.Get(hairsOverride);
                currentHairs.Instantiate(characterContainer.CharacterGraphics.Head, hairColor);
            }
            bool facialHairsAreOn = facialHairsOverride.overrideIsOn ? facialHairsOverride.additionEnabled : facialHairs.On;
            if (facialHairsAreOn)
            {
                MeshElement me = facialHairs.Get(facialHairsOverride);
                me.Instantiate(characterContainer.CharacterGraphics.Head, hairColor);
            }

            bool smokerIsOn = smokerOverride.overrideIsOn ? smokerOverride.additionEnabled : smoker.On;
            if (smokerIsOn)
            {
                MeshElement me = smoker.Get(smokerOverride);
                me.Instantiate(characterContainer.CharacterGraphics.Head);
            }
            Scale(characterContainer, heightOverride);
            return characterContainer;
        }
    }

    [System.Serializable]
    public class Women : Gender
    {
        public Women(Settings.Gender gender) : base(gender)
        {
            Settings.Women settingsWomen = gender as Settings.Women;
        }

        public CharacterContainer Kreate(
            Color hairColor,
            OverrideName characterName,
            OverrideName colorVariation,
            OverrideHeight heightOverride,
            OverrideAddition hatsOverride)
        {
            CharacterContainer characterContainer = 
                CreateContainer(hairColor, characterName, colorVariation, hatsOverride);
            Scale(characterContainer, heightOverride);
            return characterContainer;
        }

        /*
        public override CharacterContainer Clone(Color hairColor)
        {
            CharacterContainer characterContainer = base.Clone(hairColor);
            return characterContainer;
        }
        */
    }
}