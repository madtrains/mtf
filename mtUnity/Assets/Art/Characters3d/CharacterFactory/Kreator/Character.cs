﻿using System.Text;
using UnityEngine;

namespace MTCharacters.CharacterFactory.Kreator
{
    [System.Serializable]
    public class Character : WeightedElement
    {
        WeightedCollection<ColorVariation> colorVariations;
        Addition<MeshElement> hats;
        CharacterContainer container;

        public Character(Settings.Character character)
        {
            this.name = character.Name;
            this.weight = character.Weight;
            this.container = character.Container;
            this.colorVariations = 
                new WeightedCollection<ColorVariation>(character.Container.ColorVariations);
            this.hats =
                new Addition<MeshElement>(character.Hats);
        }

        public CharacterContainer Create(
            Color hairColor, 
            OverrideName colorVariation,
            OverrideAddition hat)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(name);
            stringBuilder.Append("_");

            CharacterContainer result =
                GameObject.Instantiate<CharacterContainer>(container);
            ColorVariation dressVariation = colorVariations.Get(colorVariation);
            stringBuilder.Append(dressVariation.Name);
            result.Init(hairColor, dressVariation.Colors, dressVariation.Hat);

            bool hatIsOn = hat.overrideIsOn ? hat.additionEnabled : hats.On;
            if (hatIsOn)
            {
                MeshElement hatMeshElement = hats.Get(hat);
                hatMeshElement.Instantiate(result.CharacterGraphics.Head, dressVariation.Hat);
                stringBuilder.Append("_");
                stringBuilder.Append(hatMeshElement.Name);
            }
            
            result.gameObject.name = stringBuilder.ToString();
            return result;
        }
    }
}