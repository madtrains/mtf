﻿namespace MTCharacters.CharacterFactory.Kreator
{
    [System.Serializable]
    public class Addition<T> where T : WeightedElement
    {
        public bool On { get { return addition.On; } }
        public bool Off { get { return addition.Off; } }

        protected WeightedCollection<T> collection;
        protected Settings.Addition<T> addition;

        public Addition(Settings.Addition<T> addition)
        {
            this.addition = addition;
            this.collection = new WeightedCollection<T>(addition.Array);
        }

        public T Get(OverrideName overrideName)
        {
            return collection.Get(overrideName);
        }
    }
}