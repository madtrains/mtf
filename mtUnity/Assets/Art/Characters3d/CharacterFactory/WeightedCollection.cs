﻿using System.Collections.Generic;
using UnityEngine;

namespace MTCharacters.CharacterFactory
{
    [System.Serializable]
    public class CompleteCollection<T> where T : WeightedElement
    {
        [SerializeField] protected T[] elements;

        public WeightedCollection<T> Collection { get { CheckInit(); return collection; } }
        

        protected WeightedCollection<T> collection;
        protected bool inited;
        public void CheckInit()
        {
            if (!inited)
            {
                inited = true;
                collection = new WeightedCollection<T>(elements);
            }
        }
    }

    public enum RandomMode { Any, NoRepeatLast, Unique }

    


    public class WeightedCollection<T> where T : WeightedElement
    {
        protected int LastIndex
        {
            get
            {
                if (usedIndexes.Count > 0)
                {
                    return usedIndexes[usedIndexes.Count - 1];
                }
                return -1;
            }
        }

        protected float totalWeight;
        protected List<int> usedIndexes;
        protected List<LinkElement<T>> linkElements;
        protected Dictionary<string, LinkElement<T>> namesDict;

        public WeightedCollection(T[] array)
        {
            this.totalWeight = 0f;
            this.linkElements = new List<LinkElement<T>>();
            this.usedIndexes = new List<int>();
            this.namesDict = new Dictionary<string, LinkElement<T>>();

            for (int i = 0; i < array.Length; i++)
            {
                T arrayElement = array[i];
                float normalizedWeight = totalWeight + arrayElement.Weight;
                LinkElement<T> newLinkElement =
                    new LinkElement<T>(normalizedWeight, i, arrayElement);
                this.totalWeight = normalizedWeight;
                this.linkElements.Add(newLinkElement);
                if (this.namesDict.ContainsKey(arrayElement.Name))
                {
                    Debug.LogErrorFormat(
                        "Names Dict already contains name {0}",
                        arrayElement.Name);
                }
                this.namesDict.Add(arrayElement.Name, newLinkElement);
            }
            linkElements.Sort((x, y) => x.NormalizedWeight.CompareTo(y.NormalizedWeight));
        }

        public T Get(OverrideName overrideByName)
        {
            if (overrideByName.overrideIsOn)
            {
                return GetByName(overrideByName.value);
            }
            else
            {
                return GetRandom(RandomMode.Any);
            }
        }


        public T GetRandom(RandomMode mode)
        {
            float random = UnityEngine.Random.Range(0, totalWeight);
            for (int i = 0; i < linkElements.Count; i++)
            {
                LinkElement<T> linkElement = linkElements[i];
                if (random <= linkElement.NormalizedWeight)
                {
                    int index = linkElement.Index;
                    switch (mode)
                    {
                        case (RandomMode.NoRepeatLast):
                        {
                            if (index == LastIndex && linkElements.Count > 1)
                            {
                                return GetRandom(mode);
                            }
                            else
                            {
                                usedIndexes.Add(index);
                                return linkElement.Element;
                            }
                        }
                        default:
                        {
                            usedIndexes.Add(index);
                            return linkElement.Element;
                        }
                    }
                }
            }
            usedIndexes.Add(0);
            return linkElements[0].Element;
        }

        public T GetByName(string name)
        {
            LinkElement<T> result = this.namesDict[name];
            usedIndexes.Add(result.Index);
            return result.Element;
        }
    }

    //рандомно мы выбираем этот объект, их можно перемешивать, но они сохраняют индексы списка
    //реальных объектов
    public class LinkElement<T> where T : WeightedElement
    {
        public LinkElement(float normalizedWeight, int index, T element)
        {
            this.normalizedWeight = normalizedWeight;
            this.index = index;
            this.element = element;
        }
        

        public float NormalizedWeight { get { return normalizedWeight; } }
        private float normalizedWeight;
        public int Index { get { return index; } }
        private int index;
        public T Element { get { return element; } }
        private T element;
    }
}