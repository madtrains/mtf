﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Text;

namespace SFD
{
    [CustomEditor(typeof(SplitFlapDisplay))]
    public class SplitFlapDisplayEditor : UberEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.BeginHorizontal();
            SplitFlapDisplay splitFlapDisplay = target as SplitFlapDisplay;

            //source = EditorGUILayout.ObjectField(source, typeof(Object), true);
            
                
            if (GUILayout.Button("Load/Assign Icons"))
            {
                int count = System.Enum.GetValues(typeof(Texture)).Length;
                Texture2D[] array = new Texture2D[count];
                for (int i = 0; i < System.Enum.GetValues(typeof(Texture)).Length; i++)
                {
                    Texture t = (Texture)i;
                    Texture2D tex = GetIcon(t.ToString());
                    array[i] = tex;
                }
                (target as SplitFlapDisplay).DebugApplyTextures(array);
            }
            if (GUILayout.Button("Test Town Results"))
            {
                List<ResultLineValues> resultLineValues = new List<ResultLineValues>();
                
                resultLineValues.Add(new ResultLineValues(1, new MinMax(1, 10000).RandomInt, new MinMax(10, 1000).RandomInt));
                resultLineValues.Add(new ResultLineValues(2, new MinMax(1, 10000).RandomInt, new MinMax(10, 1000).RandomInt));
                resultLineValues.Add(new ResultLineValues(0, new MinMax(1, 10000).RandomInt, new MinMax(10, 1000).RandomInt));
                resultLineValues.Add(new ResultLineValues(9, new MinMax(1, 10000).RandomInt, new MinMax(10, 1000).RandomInt));
                resultLineValues.Add(new ResultLineValues(8, new MinMax(1, 10000).RandomInt, new MinMax(10, 1000).RandomInt));
                splitFlapDisplay.ShowFlipperResultsInTown(resultLineValues, () => { Debug.Log("Test is Done"); });
            }
            EditorGUILayout.EndHorizontal();
        }


        private Texture2D GetIcon(string name)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Assets/Art/SplitFlapDisplay/icons/");
            sb.Append("sfd_");
            sb.Append(name);
            sb.Append(".tga");
            Texture2D texture = AssetDatabase.LoadAssetAtPath<Texture2D>(sb.ToString());
            return texture;
        }
    }
}