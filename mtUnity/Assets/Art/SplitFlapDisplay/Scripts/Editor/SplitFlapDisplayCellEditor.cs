﻿using UnityEngine;
using UnityEditor;

namespace SFD
{
    [CustomEditor(typeof(Cell))]
    public class SplitFlapDisplayCellEditor : UberEditor
    {
        //private List targetIcon;
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.BeginHorizontal();
            Cell cell = target as Cell;
            if (GUILayout.Button("Test Switch"))
            {
                cell.SwitchRandomly();
            }

            EditorGUILayout.EndHorizontal();
        }
    }
}