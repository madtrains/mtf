﻿using MTCore;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;


namespace SFD
{
	public delegate void CellClick(Cell cell);
    public delegate void SFDDelegate();

    public class SplitFlapDisplay : UberBehaviour
    {
        public delegate void ProcessCell(Cell cell);
        public delegate bool IsButtonBlocked();

        public ClickBLockManager ClickBLockManager { get { return clickBLockManager; } }    

        public MTTown.CamDock CamDock { get { return camDocks[0]; } }
        public MTTown.CamDock CamDockFlipper { get { return camDocks[1]; } }
        public MTTown.CamDock CamDockStation { get { return camDocks[2]; } }
        public MTTown.CamDock CamDockExit { get { return camDocks[3]; } }

        public MinMax SwitchCount { get { return switchCount; } }
        public MinMax CellSpeeds {  get { return cellSpeeds; }  }
        public int MinSpeedCellsNumber { get { return minSpeedCellsNumber; } }
        public float LoopSoundPitch { get { return loopSoundPitch; } }

        public void SetReloadButtonBlock(IsButtonBlocked blocked)
        {
            isReloadButtonBlockedDelegate = blocked;
        }


        public UnityEngine.Events.UnityAction<Trip, int> OnTripSelected;
        public UnityEngine.Events.UnityAction OnAllCellsReady;


        public Texture2D Error { get { return errorIcon; } }

        public Cell GetCell(int x, int y)
        {
            return cells[x, y];
        }

        [SerializeField] Texture2D[] textures;
        [SerializeField] private Material sideA;
        [SerializeField] private Material sideB;
        [SerializeField] private Texture2D errorIcon;
        [SerializeField] private MTTown.CamDock[] camDocks;
        [SerializeField] private TimerBeh timer;
        [SerializeField] private Cell sourceCell;
        [SerializeField] private Transform basement;
        [SerializeField] private Transform[] corners;
        //[SerializeField] private Transform siding;
        //[SerializeField] private MeshFilter frame;
        private Cell[] allCells;
        private Cell[,] cells;
        private int activeCells;
        private Dictionary<Cell, Trip> tripCells;
        private IsButtonBlocked isReloadButtonBlockedDelegate;



        public int Y { get { return cells.GetLength(1); } }
        //[SerializeField] private Renderer dbg;
        //[SerializeField] private Renderer dbg2;

        private ClickBLockManager clickBLockManager;

        [SerializeField] private MinMax switchCount;
        [SerializeField] private MinMax cellSpeeds;
        [SerializeField] private int minSpeedCellsNumber;
        [SerializeField][Range(0.5f, 1f)] private float loopSoundPitch;

        public void Init(int h, int v)
        {
            allCells = new Cell[h * v];
            for (int i = 0; i < allCells.Length; i++)
            {
                StringBuilder sb = new StringBuilder();
                Cell currentCell =
                    GameObject.Instantiate<Cell>(this.sourceCell, this.sourceCell.transform.parent);
                sb.Append("cell_");
                sb.Append(i);
                currentCell.name = sb.ToString();
                currentCell.Init(this);

                currentCell.OnSwitchCalled += () => {
                    activeCells++;
                };
                currentCell.OnReady += (texture) => {
                    activeCells--;
                    if (activeCells == 0)
                    {
                        OnAllCellsReady?.Invoke();
                    }
                };
                allCells[i] = currentCell;
            }

            Activate(sourceCell.gameObject, false);

            clickBLockManager = new ClickBLockManager();
        }

        public void Assemble(MTParameters.SFD.Assemble assemble)
        {
            Assemble(assemble.Size.X, assemble.Size.Y, assemble.Height);
            this.switchCount = assemble.SwitchCount;
            this.cellSpeeds = assemble.CellSpeeds;
        }

        /// <summary>
        /// сборка табло с заданным размером. Количество ячеек не должно превышать заданные на старте
        /// </summary>
        /// <param name="h">Количество ячеек по горизонтали</param>
        /// <param name="v">Количество ячеек по вертикали</param>
        /// <param name="basementOffset">высота нижнего края табло</param>
        public void Assemble(int h, int v, float basementOffset)
        {
            cells = new Cell[h, v];
            activeCells = 0;

            int i = 0;
            //горизонталь
            for (int x = 0; x < h; x++)
            {
                //вертикаль
                for (int y = 0; y < v; y++)
                {
                    Cell currentCell = allCells[i];
                    Activate(currentCell.gameObject, true);
                    currentCell.transform.localPosition = new Vector3(0, y * Cell.SIDE * -1f, x * Cell.SIDE * -1f);
                    cells[x, y] = currentCell;
                    currentCell.AssebmleAt(x, y);
                    i++;
                }
            }

            for(;i<allCells.Length;i++) 
            {
                Activate(allCells[i].gameObject, false);
            }

            float xOffset = -0.33f; //сдвиг на нас, на зрителя
            //this.siding.localScale = new Vector3(1f, v, h);
            float horizontalOffset = ((h * Cell.SIDE) / 2f) - (Cell.SIDE / 2f);
            float verticalOffet = (v * Cell.SIDE) - (Cell.SIDE / 2f) + basementOffset;

            this.sourceCell.transform.parent.localPosition = new Vector3(xOffset, verticalOffet, horizontalOffset);
            //this.siding.localPosition = new Vector3(xOffset, basementOffset, 0);

            
            float boneH = (h * Cell.SIDE) - (Cell.SIDE / 2f);
            float boneV = (v * Cell.SIDE) - (Cell.SIDE / 2f);

            corners[0].transform.localPosition = new Vector3(corners[0].transform.localPosition.x, -boneV, corners[0].transform.localPosition.z);
            corners[2].transform.localPosition = new Vector3(corners[2].transform.localPosition.x, corners[2].transform.localPosition.y, -boneH);
            corners[3].transform.localPosition = new Vector3(corners[3].transform.localPosition.x, -boneV, -boneH);

            camDocks[0].transform.localPosition = 
                new Vector3(
                    0, 
                    -(((v * Cell.SIDE) / 2f) - (Cell.SIDE / 2f)), 
                    -horizontalOffset);

            basement.localPosition = Vector3.up * basementOffset;
            basement.localScale = new Vector3(1f, basementOffset, 1f);
            CloseAll(true);
        }

        public void SwtichDisplayToTripsMode(bool immediately)
        {
            GameStarter.Instance.SplitFlapDisplay.Switch(
                GameStarter.GameParameters.SFDParameters.ReloadButton.X,
                GameStarter.GameParameters.SFDParameters.ReloadButton.Y, SFD.Texture.reload, true);
            cells[
                GameStarter.GameParameters.SFDParameters.ReloadButton.X,
                GameStarter.GameParameters.SFDParameters.ReloadButton.Y].SetClick(
                (cell) =>
                {
                    if(!isReloadButtonBlockedDelegate())
                    {
                        List<Trip> trips = GenerateTrips();
                        ShowTripsLines(trips, false);
                        cell.PlayButtonAnimation();
                    }
                });

            List<Trip> trips = GenerateTrips();
            ShowTripsLines(trips, immediately);
        }

        public bool IsBusy(int x, int y)
        {
            return cells[x, y].IsFlipping;
        }

        public void Dock(Transform root, MTParameters.SFD.Assemble assemble)
        {
            this.transform.position = root.transform.position;
            this.transform.rotation = root.transform.rotation;
            this.transform.localScale = root.transform.localScale;
            this.Assemble(assemble);
        }

        public void SetCameraDock(MTParameters.CameraParameters cameraParameters)
        {
            this.camDocks[0].CameraParams = cameraParameters;
        }

        #region Switch
        public void ShowStation(int lineIndex, List<Texture> textureLine, int resourceID)
        {
            textureLine.Add(Texture.colon);
            textureLine.Add(GetTextureByResourceID(resourceID));
            Switch(lineIndex, false, textureLine.ToArray());
        }

        public void ShowFlipperResultsLine(int lineIndex, int resourceID, int value)
        {
            List<Texture> result = new List<Texture>();
            result.Add(GetTextureByResourceID(resourceID));
            result.Add(Texture.colon);
            result.AddRange(GetTexturesByInt(value));
            Switch(lineIndex, false, result.ToArray());
        }

        public void ShowFlipperResultsInTown(List<ResultLineValues> resultLines, SFDDelegate sfdDelegate)
        {
            //ProcessAll((cell) => { cell.SaveState(); });
            EventChain.Chain chain = new EventChain.Chain();

            chain.Add(new SFDStep(() =>
            {
                CloseAll(false);
            }));
            chain.Add(new PauseStep(this.timer, GameStarter.GameParameters.SFDParameters.TownResultsPause));

            for (int i = 0; i < resultLines.Count; i++)
            {
                ResultLineValues resultLine = resultLines[i];
                chain.Add(new SFDStep(() =>
                {
                    //первая строка
                    Switch(
                        GameStarter.GameParameters.SFDParameters.TownResultsCorner.X,
                        GameStarter.GameParameters.SFDParameters.TownResultsCorner.Y,
                        GetTextureByResourceID(resultLine.resourceID),
                        false);

                    Switch(
                        GameStarter.GameParameters.SFDParameters.TownResultsCorner.X + 1,
                        GameStarter.GameParameters.SFDParameters.TownResultsCorner.Y,
                        Texture.colon,
                        false);
                    int lengthX = cells.GetLength(0);

                    Texture[] valuesTex = GetTexturesByInt(resultLine.value);
                    int startX = GameStarter.GameParameters.SFDParameters.TownResultsCorner.X + 2;
                    valuesTex = FillWithEmpty(valuesTex, lengthX - (startX + valuesTex.Length));
                    int j = startX;
                    foreach(Texture fTex in valuesTex)
                    {
                        Switch(
                            j,
                            GameStarter.GameParameters.SFDParameters.TownResultsCorner.Y,
                            fTex,
                            false);
                        j++;
                    }
                    


                    //вторая полоса
                    Switch(
                        GameStarter.GameParameters.SFDParameters.TownResultsCorner.X + 1,
                        GameStarter.GameParameters.SFDParameters.TownResultsCorner.Y + 1,
                        Texture.plus,
                        false);

                    valuesTex = GetTexturesByInt(resultLine.plusValue);
                    valuesTex = FillWithEmpty(valuesTex, lengthX - (startX + valuesTex.Length));
                    j = startX;
                    foreach (Texture fTex in valuesTex)
                    {
                        Switch(
                            j,
                            GameStarter.GameParameters.SFDParameters.TownResultsCorner.Y + 1,
                            fTex,
                            false);
                        j++;
                    }
                }));
                chain.Add(new PauseStep(this.timer, GameStarter.GameParameters.SFDParameters.TownResultsPause));
            }
            chain.Add(new SFDStep(() =>
            {
                sfdDelegate.Invoke();
            }));
            chain.Run();
        }

        public void ShowTripsLines(List<MTCore.Trip> trips, bool switchImmediately)
        {
            for (int i = 0; i < trips.Count; i++)
            {
                int x = GameStarter.GameParameters.SFDParameters.TripsCorner.X;
                int y = i + GameStarter.GameParameters.SFDParameters.TripsCorner.Y;
                
                bool selected = i == 0;
                Trip trip = trips[i];

                GameStarter.Instance.SplitFlapDisplay.Switch(x, y, (SFD.Texture)(i + 1), switchImmediately);
                x++;

                GameStarter.Instance.SplitFlapDisplay.Switch(x, y, SFD.Texture.colon, switchImmediately);
                x++;

                GameStarter.Instance.SplitFlapDisplay.Switch(x, y, GameStarter.Instance.SplitFlapDisplay.GetTextureOfSize((int)trip.Size), switchImmediately);
                x++;

                GameStarter.Instance.SplitFlapDisplay.Switch(x, y, SFD.Texture.colon, switchImmediately);
                x++;

                //основной ресурс
                GameStarter.Instance.SplitFlapDisplay.Switch(x, y,
                    GameStarter.Instance.SplitFlapDisplay.GetTextureByResourceID(trip.Resourses[0]),
                    switchImmediately);
                x++;

                //второй ресурс
                Texture secondResource = trip.Resourses.Count > 1 ?
                    GameStarter.Instance.SplitFlapDisplay.GetTextureByResourceID(trip.Resourses[1]) :
                    Texture.dash;
                GameStarter.Instance.SplitFlapDisplay.Switch(x, y,
                    secondResource, switchImmediately);
                x++;

                //третий ресурс
                Texture thirdResource = trip.Resourses.Count > 2 ?
                    GameStarter.Instance.SplitFlapDisplay.GetTextureByResourceID(trip.Resourses[2]) :
                    Texture.dash;
                GameStarter.Instance.SplitFlapDisplay.Switch(x, y,
                    thirdResource, switchImmediately);
                x++;

                GameStarter.Instance.SplitFlapDisplay.Switch(x, y, selected ? SFD.Texture.selected : SFD.Texture.unselected, switchImmediately);
                GetCell(x, y).SetClick(
                    (cell) =>
                    {
                        bool isBusy = false;
                        for (int j = GameStarter.GameParameters.SFDParameters.TripsCorner.Y; j < GameStarter.GameParameters.SFDParameters.TripsCorner.Y + trips.Count; j++)
                        {
                            if (IsBusy(x, j))
                            {
                                isBusy = true;
                                break;
                            }
                        }
                        if (!isBusy && !ClickBLockManager.IsBlocked)
                        {
                            int index = cell.Y - GameStarter.GameParameters.SFDParameters.TripsCorner.Y;
                            OnTripSelected?.Invoke(trip, index);
                            SelectDeselect(
                                x, 
                                GameStarter.GameParameters.SFDParameters.TripsCorner.Y, 
                                GameStarter.GameParameters.SFDParameters.TripsCorner.Y + trips.Count, 
                                index);
                            cell.PlayButtonAnimation();
                        }
                    });
            }
        }

        public void Switch(int lineIndex, bool immediately, params Texture[] textures)
        {
            if (lineIndex >= this.cells.GetLength(1))
            {
                Debug.LogErrorFormat("Not enough lines to show line: {0} ", lineIndex);
                return;
            }
            for (int i = 0; i < textures.Length; i++)
            {
                if (i >= this.cells.GetLength(0))
                {
                    Debug.LogErrorFormat("SFD Line is too short to show horizontal cell: {0}", i);
                    return;
                }
                this.cells[i, lineIndex].SwitchTo(textures[i], immediately); 
            }
        }

        public void Switch(int x, int y, Texture texture, bool immediately)
        {
            this.cells[x, y].SwitchTo(texture, immediately);
            this.cells[x, y].OnTap = null;
        }

        /// <summary>
        /// Закрывает все окошки пустой текстурой
        /// </summary>
        /// <param name="immediately"> Мгновенное переключение, без анимации </param>
        public void CloseAll(bool immediately)
        {
            ProcessAll((cell) => {
                cell.SwitchTo(Texture.empty, immediately); 
            });
        } 

        public void Delay(float time, SFDDelegate command)
        {
            this.timer.OnEnd = () => 
            {
                command?.Invoke();
                this.timer.OnEnd = null;
            };
            this.timer.Launch(time);
        }

        private void SelectDeselect(int x, int yStart, int yEnd, int selectIndex)
        {
            int targetY = selectIndex + yStart;
            for (int i = yStart; i < yEnd; i++)
            {
                Texture tex = (i == targetY) ? Texture.selected : Texture.unselected;
                cells[x, i].SwitchTo(tex, false);
            }
        }

        private List<Trip> GenerateTrips()
        {
            List<Trip> trips =
                GameStarter.Instance.TownDataManager.WaysDispatcher.BuildTripsForTown(GameStarter.GameParameters.SFDParameters.TownTripLines);
            return trips;
        }
        #endregion

        #region GetTextures
        public Texture GetTextureRandom()
        {
            int count = System.Enum.GetValues(typeof(Texture)).Length;
            int randomIndex = UnityEngine.Random.Range(0, count);
            Texture tex = (Texture)randomIndex;
            return tex;
        }

        public Texture[] GetTexturesByInt(int value)
        {
            int[] digits = IntToDigitsArray(value);
            Texture[] result = new Texture[digits.Length];
            for (int i = 0; i < digits.Length; i++)
            {
                result[i] = (Texture)digits[i];
            }
            return result;
        }

        public Texture GetTextureByResourceID(int resourceID)
        {
            Texture result = resourceID >= 0 ? (Texture)(resourceID + 10) : Texture.dash;
            return result;
        }

        public Texture[] GetTexturesByResourceDict(Dictionary<int, int> resources)
        {
            List<Texture> textures = new List<Texture>();
            foreach (KeyValuePair<int, int> kvp in resources)
            {
                textures.Add(GetTextureByResourceID(kvp.Key));
            }
            return textures.ToArray();
        }

        public Texture GetTextureOfSize(int size)
        {
            return (Texture)size + 27;
        }

        public Texture2D GetTexture2D(Texture name)
        {
            return textures[(int)name];
        }
        #endregion

        #region Utilities
        private int[] IntToDigitsArray(int value)
        {
            //Debug.Log(value);
            if (value == 0)
                return new int[] { 0 };

            int digitCount = (int)System.Math.Log10(value) + 1;
            int[] result = new int[digitCount];
            int left = value;
            for (int i = digitCount, j = 0; i > 0; i--, j++)
            {
                int power = i - 1;
                int round = (int)Mathf.Pow(10, power);//10 100 100 e.t.c
                int digit = Mathf.FloorToInt(left / round);
                result[j] = digit;
                left = left - (digit * round);
            }
            return result;
        }

        private Texture[] FillWithEmpty(Texture[] textures, int count)
        {
            if (count <= 0)
                return textures;
            Texture[] result = new Texture[textures.Length + count];
            for(int i =0; i < textures.Length; i++)
            {
                result[i] = textures[i];
            }
            for (int i = textures.Length; i < textures.Length + count; i++)
            {
                result[i] = Texture.empty;
            }
            return result;
        }

        //для кнопки инспектора в редакторе
        public void DebugApplyTextures(Texture2D[] tex)
        {
            textures = tex;
        }

        private void ProcessAll(ProcessCell processCell)
        {
            if (cells == null || cells.Length == 0)
                return;
            for (int i = 0; i < cells.GetLength(0); i++)
            {
                for (int j = 0; j < cells.GetLength(1); j++)
                {
                    processCell(cells[i, j]);
                }
            }
        }
        #endregion

        #region nestedClasses
        private class SFDStep : EventChain.Step
        {
            public SFDStep(EventChain.StepDelegate del) : base(del) { }

            public override void Run()
            {
                base.Run();
                Done();
            }
        }

        private class PauseStep : EventChain.Step
        {
            public PauseStep(TimerBeh timer, float pause)
            {
                this.pause = pause;
                this.timer = timer;
            }
            private float pause;
            private TimerBeh timer;

            public override void Run()
            {
                timer.OnEnd = () => { Done(); };
                timer.Launch(pause);
            }
        }
        #endregion
    }

    

    public class ResultLineValues
    {
        public ResultLineValues(int resourceID, int value, int plusValue)
        {
            this.resourceID = resourceID;
            this.value = value;
            this.plusValue = plusValue;
        }

        public int resourceID;
        public int value;
        public int plusValue;
    }

    [System.Serializable]
    public class ClickBLockManager
    {
        public bool IsBlocked
        {
            get
            {
                if (isTown)
                {
                    return !sfdPoint || goCounterPopupOpened || !isTown;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool IsTown { set { isTown = value; } }
        public bool GoCounterPopupOpened { set { goCounterPopupOpened = value; } }
        public bool SFDPoint { set { sfdPoint = value; } }

        private bool isTown;
        private bool goCounterPopupOpened;
        private bool sfdPoint;
    }
}