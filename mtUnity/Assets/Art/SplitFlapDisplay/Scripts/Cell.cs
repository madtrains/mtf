﻿using MTTown;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SFD
{
    public class Cell : MTCore.RayCastTap
    {
        public static readonly float SIDE = 2.25f;
        public static readonly int hFlip = Animator.StringToHash("flip");
        public static readonly int hFlipSpeed = Animator.StringToHash("flipSpeed");
        public static readonly int hButton = Animator.StringToHash("button");

        public Texture Current { get { return this.current; } }

        public UnityEngine.Events.UnityAction OnSwitchCalled;
        public UnityEngine.Events.UnityAction<Texture> OnReady;
        public UnityEngine.Events.UnityAction OnFlip;

        public void PlayButtonAnimation()
        {
            animator.SetTrigger(hButton);
        }

        public void SetClick(CellClick cellClick)
        {
            this.OnTap = () =>
            {
                cellClick.Invoke(this);
            };
        }
        public void SetClickCheckBlock(CellClick cellClick)
        {
            this.OnTap = () =>
            {
                cellClick.Invoke(this);
            };
        }

        public bool IsFlipping { get { return isFlipping; } }

        //public Line line;
        public int X { get { return x; } }
        public int Y { get { return y; } }

        [SerializeField] private Renderer render;
        [SerializeField] private Material a;
        [SerializeField] private Material b;
        [SerializeField] private Animator animator;
        [SerializeField] private AudioSource loop;
        //[SerializeField] private MTSound.MultiSource hit;
        private Texture current;
        private Texture next;
        private List<Texture> switchList;
        private bool playSingleSound;
        private SplitFlapDisplay sfd;
        private int x;
        private int y;
        private Texture saved;

        public void SaveState()
        {
            saved = this.current;
        }

        public void SwitchToSavedState(bool immediately)
        {
            SwitchTo(saved, immediately);
        }

        private bool isFlipping;
        private int totalCount = System.Enum.GetValues(typeof(Texture)).Length;

        public void Init(SplitFlapDisplay sfd)
        {
            this.sfd = sfd;
            loop.pitch = sfd.LoopSoundPitch;
            
            animator.SetFloat(hFlipSpeed, sfd.CellSpeeds.Min);
            //hit.InitCloneByFirstElement(3);

            //поиск материалов по именам, чтобы иметь далее ссылку на каждую сторону
            //незивестно как при импорте раскидает материалы
            for (int i = 0; i < render.materials.Length; i++)
            {
                Material m = render.materials[i];
                if (m.name.Contains("SideA"))
                {
                    a = m;
                }

                else if (m.name.Contains("SideB"))
                {
                    b = m;
                }
            }
            this.current = Texture.empty;
        }

        public void AssebmleAt(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        /// <summary>
        /// все остальные комманды на переключение тоже приходят сюда
        /// </summary>
        /// <param name="target"></param>
        /// <param name="immediately"></param>
        public void SwitchTo(Texture target, bool immediately=false)
        {
            OnSwitchCalled?.Invoke();
            //уже готово
            if (target == current)
            {
                OnReady?.Invoke(current);
                return;
            }

            if (!immediately)
                Switch(target);
            else
            {
                SetImmediately(target);
            }
        }

        public void SwitchRandomly()
        {
            Texture tex = sfd.GetTextureRandom();
            //Debug.Log(tex);
            SwitchTo(tex);
        }

        /// <summary>
        /// вызывается аним эвентом
        /// </summary>
        public void OnFlipped()
        {
            OnSwitchEndCheck();
            OnFlip?.Invoke(); 
        }

        private void Switch(Texture target)
        {
            List<Texture> forward = SwitchListForward(target);
            List<Texture> backward = SwitchListBackward(target);
            switchList = forward.Count < backward.Count ? forward : backward;
            int count = sfd.SwitchCount.RandomInt;
            while (switchList.Count > count)
            {
                switchList.RemoveAt(Random.Range(0, switchList.Count - 1));
            }
            SwitchByList();
        }

        private void SetImmediately(Texture texture)
        {
            int count = System.Enum.GetNames(typeof(Texture)).Length;
            int index = (int)texture;
            int nextIndex = index + 1;
            if (nextIndex >= count)
                nextIndex = 0;

            current = texture;
            next = (Texture)nextIndex;

            a.mainTexture = sfd.GetTexture2D(current);
            b.mainTexture = sfd.GetTexture2D(next);

            isFlipping = false;
            OnReady?.Invoke(current);
        }

        private void SwitchByList()
        {
            int switchesLeft = switchList.Count;

            float speed = sfd.CellSpeeds.Min;
            if (switchesLeft > sfd.MinSpeedCellsNumber)
            {
                float t = Mathf.InverseLerp(sfd.MinSpeedCellsNumber, totalCount, switchesLeft);
                speed = sfd.CellSpeeds.Lerp(t);
                
                if (!loop.isPlaying)
                    loop.Play();
                playSingleSound = false;
            }
            else
            {
                if (loop.isPlaying)
                    loop.Stop();
                playSingleSound = true;
            }

            animator.SetFloat(hFlipSpeed, speed);
            SwitchStep(switchList.First());
        }

        private void SwitchStep(Texture next)
        {
            //change target texure
            b.mainTexture = sfd.GetTexture2D(next);
            this.next = next;

            //run animation
            animator.SetBool(hFlip, true);
            isFlipping = true;
        }

        /// <summary>
        /// выполняется по аниим эвенту, не напрямую
        /// </summary>
        private void OnSwitchEndCheck()
        {
            current = next;
            a.mainTexture = sfd .GetTexture2D(current);
            animator.SetBool(hFlip, false);
            switchList.RemoveAt(0);
            if (playSingleSound)
            {
                //hit.Play();
            }

            if (switchList.Count > 0)
                SwitchByList();
            else
            {
                isFlipping = false;
                OnReady?.Invoke(current);
            }
        }

        private List<Texture> SwitchListForward(Texture target)
        {
            int attempts = 0;
            List<Texture> result = new List<Texture>();
            Texture next = Next(current);
            result.Add(next);
            while (result.Last() != target)
            {
                result.Add(Next(result.Last()));
                attempts += 1;
                if (attempts > (System.Enum.GetValues(typeof(Texture)).Length * 2))
                {
                    Debug.LogErrorFormat("too many attempts {0}", target);
                    break;
                }
            }
            return result;
        }

        private List<Texture> SwitchListBackward(Texture target)
        {
            int attempts = 0;
            List<Texture> result = new List<Texture>();
            Texture previous = Previous(current);
            result.Add(previous);
            while (result.Last() != target)
            {
                result.Add(Previous(result.Last()));
                attempts += 1;
                if (attempts > (System.Enum.GetValues(typeof(Texture)).Length * 2))
                {
                    Debug.LogErrorFormat("too many attempts {0}", target);
                    break;
                }
            }
            return result;
        }

        private Texture Next(Texture current)
        {
            int nextIndex = (int)current + 1;
            if (nextIndex >= totalCount)
                nextIndex = 0;
            return (Texture)nextIndex;
        }

        private Texture Previous(Texture current)
        {
            int previousIndex = (int)current -1;
            if (previousIndex < 0)
                previousIndex = totalCount - 1;
            return (Texture)previousIndex;
        }
    }
}