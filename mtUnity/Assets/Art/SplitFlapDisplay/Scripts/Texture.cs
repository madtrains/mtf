﻿using MTTown;
using UnityEngine.Events;

namespace SFD
{
    public enum Texture
    {
        zero, one, two, three, four, five, six, seven, eight, nine,
        coin, passenger, mail, oil, iron, wood, fish, freight, ticket, cogCoin,
        x, dash, colon,
        europe, egypt,
        plus, minus,
        small, medium, large,
        selected, unselected, reload, reload_disabled,
        empty
    }
}