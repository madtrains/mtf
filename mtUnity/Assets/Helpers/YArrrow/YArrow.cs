using UnityEngine;
using WaySystem;


public class YArrow : UberBehaviour
{
    public static readonly int hSpeed = Animator.StringToHash("speed");

    public bool ArrowVisible { set { Activate(arrow, value); } }
    public float T { set { t = value; } }
    public float Speed { set { animator.SetFloat(hSpeed, value); } }

    public MinMax Heights { set { heights = value; } }

    [SerializeField] private Animator animator;
    [SerializeField] private Transform point;
    [SerializeField] private GameObject arrow;
    [SerializeField] MinMax heights;
    [SerializeField] private Transform slider;
    [SerializeField] private float t;

    public void ReplacePointer(GameObject newPointer)
    {
        if (arrow.transform.childCount > 0)
        {
            Destroy(arrow.transform.GetChild(0).gameObject);
            GameObject np = Instantiate(newPointer, arrow.transform);
            np.transform.localPosition = Vector3.zero;
            np.transform.localRotation = Quaternion.identity;
            Activate(np, true);
        }
    }



    public void PointAt(Vector3 position, MinMax heights)
    {
        this.heights = heights;
        PointAt(position);   
    }

    public void PointAt(Vector3 position)
    {
        this.transform.position = position;
        this.transform.rotation = Quaternion.identity;
        Activate(this.gameObject, true);
    }

    public void PointAt(Point point, MinMax heights)
    {
        this.transform.position = point.Position;
        this.transform.rotation = point.Rotation;
        this.heights = heights;
        Activate(this.gameObject, true);
    }

    public void Close()
    {
        Activate(this.gameObject, false);
    }

    private void Update()
    {
        slider.localPosition = Vector3.up * heights.Lerp(t);   
    }
}
