using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WaySystem;

namespace MTTown.OLD_TrainConstructor
{
    public class YCoin : UberBehaviour
    {
        public static readonly int hSpeed = Animator.StringToHash("speed");

        public bool CoinVisible { set { Activate(coin, value); } }

        public float Speed { set { animator.SetFloat(hSpeed, value); } }

        public float Height { set { SetHeight(value); } }

        [SerializeField] private Animator animator;
        [SerializeField] private GameObject coin;


        private void SetHeight(float value)
        {
            coin.transform.localPosition = Vector3.up * value;
        }
    }
}