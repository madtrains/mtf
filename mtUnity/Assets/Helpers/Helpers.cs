﻿using UnityEngine;


public class Helpers : UberBehaviour
{
	public void OnStart()
	{
		Activate(yArrow.gameObject, false);
        Activate(yGear.gameObject, false);
        Activate(yCoin.gameObject, false);
        Activate(coinPointer, false);
    }

	public YArrow Arrow { get { return yArrow; } }
    [SerializeField] private YArrow yArrow;

    public YArrow Gear { get { return yGear; } }
    [SerializeField] private YArrow yGear;

    public YArrow Coin { get { return yCoin; } }
    [SerializeField] private YArrow yCoin;

    [SerializeField] public GameObject CoinPointer { get { return coinPointer; } } 
    [SerializeField] private GameObject coinPointer;

}
