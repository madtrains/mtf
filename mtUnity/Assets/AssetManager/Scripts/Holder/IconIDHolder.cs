﻿using System.Collections.Generic;
using UnityEngine;

namespace MTUi
{
    [CreateAssetMenu(menuName = "Icon ID Holder")]
    public class IconIDHolder : ScriptableObject
    {
        public bool NeedsValidation { get { return needsValidation; } }
        protected bool needsValidation;

        [SerializeField] private IconID[] icons;
        private Dictionary<int, Sprite> dct;

        public Sprite Get(int id)
        {
            if (dct == null) 
            {
                dct = new Dictionary<int, Sprite>();
                for (int i = 0; i < icons.Length; i++) 
                {
                    dct[icons[i].ID] = icons[i].Sprite;
                }
            }

            if (dct.ContainsKey(id))
                return dct[id];
            return dct[-1];
        }

        protected virtual void OnValidate()
        {
            for (int i = 0; i < icons.Length; i++)
            {
                var c = icons[i];
                c.DuplicateID = false;
                c.DuplicateSprite = false;
            }

            for (int i = 0; i < icons.Length; i++)
            {
                var c = icons[i];
                for (int j = i + 1; j < icons.Length; j++)
                {
                    var n = icons[j];
                    if (c.ID == n.ID)
                    {
                        c.DuplicateID = true;
                        n.DuplicateID = true;
                    }

                    if (c.Sprite == n.Sprite)
                    {
                        c.DuplicateSprite = true;
                        n.DuplicateSprite = true;
                    }
                }
            }
        }
    }
}