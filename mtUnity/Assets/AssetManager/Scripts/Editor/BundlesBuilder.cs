﻿using MTBundles;
using MTParameters;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

public class BundlesBuilder
{
	[MenuItem("MT/Bundles/Delete Bundles #&Q")]
	public static void DeleteBundlesCommand()
	{
		if (EditorUtility.DisplayDialog("Process Parameters", "Delete Bundles?", "Delete", "Cancel"))
		{
			DeleteBundles();
		}
	}
	public static void DeleteBundles()
	{
		DirectoryInfo di = Directory.GetParent(Application.dataPath);
		string bundles = Path.Combine(di.ToString(), "bundles");
		string storage = Path.Combine(di.ToString(), "MT_STORAGE", "MT_BUNDLES_CACHE");
		FileUtil.DeleteFileOrDirectory(bundles);
		FileUtil.DeleteFileOrDirectory(storage);
	}

	[MenuItem("MT/Player Settings/Delete Progress")]
	public static void DeleteSaves()
	{
		if (EditorUtility.DisplayDialog("Process Parameters", "Delete Saves?", "Delete", "Cancel"))
		{
			DirectoryInfo di = Directory.GetParent(Application.dataPath);
			string storage = Path.Combine(di.ToString(), "MT_STORAGE", "MT_PLAYER");
			FileUtil.DeleteFileOrDirectory(storage);
			return;
		}
	}


	//[MenuItem("MT/Bundles/Build Android")]
	static void BuildAllBundlesAndroid()
	{
		BuildBundles("android");
	}


	//[MenuItem("MT/Bundles/Build Editor")]
	static void BuildAllBundlesEditor()
	{
		BuildBundles("editor");
	}

	[MenuItem("MT/Bundles/Build")]
	public static void BuildBundles()
	{
		var platform = GetCurrentPlatform();
		BuildBundles(platform);
	}
		public static void BuildBundles(string platform)
	{
		Debug.Log("Try build bundles for  " + platform);

		string bundleFullPath = BundlesServerManager.GetBundleFullPath(platform);

		Debug.Log("Start build bundles for  " + platform);
		AssetBundleManifest manifest = BuildPipeline.BuildAssetBundles(bundleFullPath, BuildAssetBundleOptions.None, GetTarget(platform));
		Debug.Log("Finish build bundles for  " + platform);
	}
	public static AssetBundleManifest GetManifest(string bundlePath)
	{
		AssetBundle.UnloadAllAssetBundles(true);
		var manifestBundle = AssetBundle.LoadFromFile(Path.Combine(Directory.GetParent(Application.dataPath).FullName, bundlePath, InBuildParameters.InBuildParametersInstance.BundlePrefix));

		if (manifestBundle == null)
		{
			Debug.LogError("Bundles manifest is not loaded");
			throw new System.Exception("Bundles manifest is not loaded");
		}
		var manifest = manifestBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
		if (manifest == null)
		{
			Debug.LogError("Bundle manifest is null");
			throw new System.Exception("Bundle manifest is null");
		}
		return manifest;
	}

	[MenuItem("MT/Bundles/Save To Cache")]
	public static async Task SaveBundlesToCacheAsync()
	{
		try
		{
			string platform = GetCurrentPlatform();
			Debug.Log($"Send bundles to cache for platform: {platform}");

			string bundleFullPath = BundlesServerManager.GetBundleFullPath(platform);

			var manifest = GetManifest(bundleFullPath);

			var bundles = manifest.GetAllAssetBundles();
			var bsm = new BundlesServerManager();

			var index = 1;
			foreach (var b in bundles.ToList())
			{
				Debug.Log($"Save bundle {index++}/{bundles.Count()} - {b}");

				EditorUtility.ClearProgressBar();
				EditorUtility.DisplayProgressBar("Preparing bundles", $"Send bundle {index}/{bundles.Count()} - {b}", index / bundles.Length );
				await bsm.SendToCacheAsync(bundleFullPath, b);

			}
			EditorUtility.ClearProgressBar();
			EditorUtility.DisplayProgressBar("Preparing bundles", $"Save bundles manifest file.", 1f);

			Debug.Log($"Save bundles manifest file.");

			await bsm.SendToCacheAsync(bundleFullPath, InBuildParameters.InBuildParametersInstance.BundlePrefix);

			EditorUtility.ClearProgressBar();
			Debug.Log($"Complete!");
		}
		finally
		{
			AssetBundle.UnloadAllAssetBundles(true);
		}
	}

	//[MenuItem("MT/Bundles/Send To Server")]
	public static async Task SendToServerBundlesAsync()
	{
		try
		{
			string platform = GetCurrentPlatform();
			Debug.Log($"Send bundles to server for platform: {platform}");

			string bundleFullPath = BundlesServerManager.GetBundleFullPath(platform);

			var manifest = GetManifest(bundleFullPath);

			var bsm = new BundlesServerManager();
			await bsm.SendToServerAsync(bundleFullPath, manifest, platform);
		}
		finally
		{
			AssetBundle.UnloadAllAssetBundles(true);
		}
	}
	private static BuildTarget GetTarget(string platform)
	{
		BuildTarget target = BuildTarget.NoTarget;
		switch (platform)
		{
			case "android":
				target = BuildTarget.Android;
				break;
			case "editor":
				target = BuildTarget.StandaloneWindows64;
				break;
		}
		return target;
	}
	/// <summary>
	/// Возвращает текущую платформу Unity в строковом формате.
	/// </summary>
	public static string GetCurrentPlatform()
	{
		switch (EditorUserBuildSettings.activeBuildTarget)
		{
			case BuildTarget.StandaloneWindows:
			case BuildTarget.StandaloneWindows64:
				return "editor";
			case BuildTarget.Android:
				return "android";
			case BuildTarget.iOS:
				return "ios";
			case BuildTarget.WebGL:
				return "webgl";
			default:
				throw new System.NotSupportedException($"Unsupported platform: {EditorUserBuildSettings.activeBuildTarget}");
		}
	}
}