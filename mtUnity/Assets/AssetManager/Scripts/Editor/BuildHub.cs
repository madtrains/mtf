﻿using MTParameters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using System.Text.RegularExpressions;


namespace Assets.AssetManager.Scripts.Editor
{
	[CustomEditor(typeof(BuildHub))]
	public class BuildHub : UnityEditor.Editor
	{
		public BuildHub()
		{
		}
		[MenuItem("MT/_BIG_RED_BUTTON")]
		public static async Task BigRedButton()
		{
			var completeWithSuccess = false;
			try
			{
				await ParametersProcessor.ProcessParamsCompele();
				await PrepareServerData();
				ParametersProcessor.GenerateEnumFromResources();
				GenerateEnumFromBundle();
				completeWithSuccess = true;
			}
			finally
			{
				EditorUtility.ClearProgressBar();
				if(completeWithSuccess ) {
					Debug.Log("All actions completed succesfully");
				}
				else
				{
					Debug.LogError("BIG BADABUM!!!!");
				}
			}
		}

		[MenuItem("MT/Server/_Prepare data")]
		public static async Task PrepareServerData()
		{
			var t = DateTime.Now;

			Debug.Log("Start!!!");

			try
			{
				// (1/3) Prepare assets
				EditorUtility.ClearProgressBar();
				EditorUtility.DisplayProgressBar("Preparing Data (1/4)", "Preparing assets...", 0.10f);
				Debug.Log("(1/3) Prepare assets...");

				// (2/3) Prepare game params
				EditorUtility.ClearProgressBar();
				EditorUtility.DisplayProgressBar("Preparing Data (2/5)", "Preparing game params...", 0.20f);
				Debug.Log("(2/5) Prepare game params...");

				await ParametersProcessor.ProcessParameters(false);

				EditorUtility.ClearProgressBar();
				EditorUtility.DisplayProgressBar("Preparing Data (3/5)", "Clear bundles...", 0.30f);
				Debug.Log("(3/5) Clear bundles...");

				BundlesBuilder.DeleteBundles();

				EditorUtility.ClearProgressBar();
				EditorUtility.DisplayProgressBar("Preparing Data (4/5)", "Preparing bundles...", 0.50f);
				Debug.Log("(3/5) Build bundles...");

				BundlesBuilder.BuildBundles();

				EditorUtility.ClearProgressBar();
				EditorUtility.DisplayProgressBar("Preparing Data (5/5)", "Send bundles to server...", 0.60f);
				Debug.Log("(5/5) Save bundles...");

				await BundlesBuilder.SendToServerBundlesAsync();

				Debug.Log($"Done! Finished in {(int)(DateTime.Now - t).TotalSeconds} seconds.");
			}
			catch (System.Exception e)
			{
				Debug.Log("Failed to prepare data...");
				Debug.LogError(e);
			}
			finally
			{
				EditorUtility.ClearProgressBar();
			}
		}
		[MenuItem("MT/BuildHub/_Prepare data #&W")]
		public static async Task PrepareData()
		{
			var t = DateTime.Now;

			Debug.Log("Start!!!");

			try
			{
				// (1/3) Prepare assets
				EditorUtility.ClearProgressBar();
				EditorUtility.DisplayProgressBar("Preparing Data (1/5)", "Preparing assets...", 0.10f);
				Debug.Log("(1/3) Prepare assets...");
				List<string> result = new List<string>();
				string bundlesFolder = "Assets/AssetManager/Bundles";
				ProcessBundleFolderRecursively(bundlesFolder, bundlesFolder.Length + 1, result);

				// (2/3) Prepare game params
				EditorUtility.ClearProgressBar();
				EditorUtility.DisplayProgressBar("Preparing Data (2/5)", "Preparing game params...", 0.33f);
				Debug.Log("(2/5) Prepare game params...");
				await ParametersProcessor.ProcessParameters(false);

				EditorUtility.ClearProgressBar();
				EditorUtility.DisplayProgressBar("Preparing Data (3/5)", "Clear bundles...", 0.66f);
				Debug.Log("(3/5) Clear bundles...");
				BundlesBuilder.DeleteBundles();

				EditorUtility.ClearProgressBar();
				EditorUtility.DisplayProgressBar("Preparing Data (4/5)", "Preparing bundles...", 0.66f);
				Debug.Log("(4/5) Build bundles...");
				BundlesBuilder.BuildBundles();

				EditorUtility.ClearProgressBar();
				EditorUtility.DisplayProgressBar("Preparing Data (5/5)", "Save bundles to cache...", 0.99f);
				Debug.Log("(5/5) Save bundles...");
				await BundlesBuilder.SaveBundlesToCacheAsync();

				Debug.Log($"Done! Finished in {(int)(DateTime.Now - t).TotalSeconds} seconds.");
			}
			catch (System.Exception e)
			{
				Debug.Log("Failed to prepare data...");
				Debug.LogError(e);
			}
			finally
			{
				EditorUtility.ClearProgressBar();
			}
		}

		[MenuItem("MT/BuildHub/_Build_Bundles_Enum")]
		private static void GenerateEnumFromBundle()
		{
			try
			{
				string enumName = "MTAssetBundles";

				// Путь для сохранения сгенерированного enum
				string enumFilePath = Path.Combine(Application.dataPath, "Scripts", $"{enumName}.cs");

				// Убедимся, что директория существует
				string directory = Path.GetDirectoryName(enumFilePath);
				if (!Directory.Exists(directory))
				{
					Directory.CreateDirectory(directory);
				}

				// Начинаем формирование enum
				StringBuilder enumBuilder = new StringBuilder();
				enumBuilder.AppendLine("public enum " + enumName);
				enumBuilder.AppendLine("{");

				string platform = BundlesBuilder.GetCurrentPlatform();
				string bundleFullPath = MTBundles.BundlesServerManager.GetBundleFullPath(platform);

				var manifest = BundlesBuilder.GetManifest(bundleFullPath);

				var bundles = manifest.GetAllAssetBundles();

				var index = 0;
				foreach (var b in bundles.ToList())
				{
					var bundleName = $"{b}".Replace("/", "_").Replace("\\", "_");
					enumBuilder.AppendLine($"    {bundleName} = {index}, // \"{b}\"");

					EditorUtility.ClearProgressBar();
					EditorUtility.DisplayProgressBar("Add to enum", $"Bundle {index}/{bundles.Count()} - {b}", index / bundles.Length);
					Debug.Log($"Add bundle to enum {index++}/{bundles.Count()} - {b}");
				}

				enumBuilder.AppendLine("}");

				// Записываем enum в файл
				File.WriteAllText(enumFilePath, enumBuilder.ToString());
				Debug.Log($"Enum {enumName} generated at: {enumFilePath}");

				// Обновляем ассеты в проекте
				AssetDatabase.Refresh();
			}
			finally { EditorUtility.ClearProgressBar(); }
		}


		private static void ProcessBundlesFolder2(string path)
		{
			string end = System.IO.Path.GetDirectoryName(path);
			string[] split = end.Split('\\');
			string last = split[split.Length - 1];
			string last2 = split[split.Length - 2];
			StringBuilder st = new StringBuilder();
			st.Append(last2);
			st.Append("/");
			st.Append(last);
			st.Append("/");
		}





		private static void ProcessBundleFolderRecursively(string path, int removeCount, List<string> fileInfoList)
		{
			Regex regex = new Regex(@"\.meta");

			// Получаем все файлы в текущем каталоге
			string[] files = Directory.GetFiles(path);
			foreach (string file in files)
			{
				// Добавляем информацию о каждом файле (например, полный путь)
				string correctPath = file.Replace('\\', '/');
				correctPath = correctPath.Remove(0, removeCount);

				if (!regex.IsMatch(correctPath))
				{
					string directory = System.IO.Path.GetDirectoryName(correctPath);
					directory = directory.Replace('\\', '/');

					string name = Path.GetFileNameWithoutExtension(correctPath);
					string result = directory + "/" + name;
					result = result.ToLower();
					Debug.LogWarningFormat(result);
					AssetImporter assetImporter = AssetImporter.GetAtPath(file);
					assetImporter.SetAssetBundleNameAndVariant(result, "");
					fileInfoList.Add(correctPath);
				}
			}

			// Получаем все подкаталоги в текущем каталоге
			string[] directories = Directory.GetDirectories(path);
			foreach (var directory in directories)
			{
				// Рекурсивно обходим каждый подкаталог
				ProcessBundleFolderRecursively(directory, removeCount, fileInfoList);
			}
		}
	}
}
