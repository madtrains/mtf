﻿using MT_ServerDataManager;
using MTParameters;
using MTCore;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace MTBundles
{
    public class BundlesServerManager
	{
		ServerDataManager _serverManager;
		public BundlesServerManager()
		{
			var serverUrl = InBuildParameters.InBuildParametersInstance.ServerUrl;
			_serverManager = new ServerDataManager(serverUrl);
		}
		public async Task SendToServerAsync(string folder, AssetBundleManifest manifest, string platform = "")
		{
			Debug.Log($"Send bundles to server!");
			Debug.Log($"Platform: {platform}");

			if (!await _serverManager.IsServerAvailable())
			{
				Debug.LogWarning("Server is not available");
				return;
			}
			Debug.Log($"Start!");
			var serverPath = GetBundleFullPath(platform);

			Debug.Log($"Bundles server path : {serverPath}");
			var bundles = manifest.GetAllAssetBundles();
			var index = 1;
			foreach (var b in bundles.ToList())
			{
				EditorUtility.ClearProgressBar();
				EditorUtility.DisplayProgressBar("Preparing bundles", $"Send bundle {index++}/{bundles.Count()} - {b}", 1/ bundles.Length*index);

				Debug.Log($"Send bundle {index++}/{bundles.Count()} - {b}");
				await SendToServerAsync(folder, b, platform);
			}
			EditorUtility.ClearProgressBar();
			EditorUtility.DisplayProgressBar("Preparing bundles", $"Save bundles manifest file.", 1f);

			Debug.Log($"Send bundles manifest file.");
			//отправляем сам манифест на сервер
			await SendToServerAsync(folder, InBuildParameters.InBuildParametersInstance.BundlePrefix, platform);

			EditorUtility.ClearProgressBar();
			Debug.Log($"Complete!");
		}
		public async Task SendToServerAsync(string folder, string name, string platform)
		{
			var bundlePath = Path.Combine(folder, name);
			var bundleManifestPath = $"{bundlePath}.manifest";

			var bundleManifest = File.ReadAllBytes(bundleManifestPath);
			var bundle = File.ReadAllBytes(bundlePath);

			var serverPath = GetBundleServerPath(platform);
			var bundleName = $"{serverPath}/{name}".Replace("/", "_").Replace("\\", "_");

			await PostBundleAsync(bundleName, bundle, bundleManifest);

			Debug.Log($"Full server name : {bundleName}");
		}

		public async Task SendToCacheAsync(string folder, string name)
		{
			var bundlePath = Path.Combine(folder, name);
			var bundleManifestPath = $"{bundlePath}.manifest";

			var bundleManifest = File.ReadAllBytes(bundleManifestPath);
			var bundle = File.ReadAllBytes(bundlePath);

			var bundleName = $"{name}".Replace("/", "_").Replace("\\", "_");

			await ResourceLoader.SaveBundleToLocalStorage(bundleName, bundle, bundleManifest);

			Debug.Log($"Full cache name : {bundleName}");
		}

		public async Task SendParamsToServerAsync(TextAsset asset, TextAsset assetManifest, byte[] paramsData, byte[] manifestData)
		{
			if (!await _serverManager.IsServerAvailable())
			{
				Debug.LogWarning("Server is not available");
				return;
			}
			try
			{
				Debug.Log($"Send {asset.name} to server");

				var serverPath = GetBundleServerPath();
				var paramsPath = $"{serverPath}/{asset.name}.json";
				var paramsManifestPath = $"{serverPath}/{assetManifest.name}.txt";

				Debug.Log($"Send {asset.name} to server");
				await PostBundleAsync(paramsPath.Replace("/", "_"), paramsData);
				Debug.Log($"Send {assetManifest.name} to server");
				await PostBundleAsync(paramsManifestPath.Replace("/", "_"), manifestData);
				Debug.Log($"Complete!!!");
			}
			catch(Exception ex)
			{
				Debug.Log($"Sending failed with exception: {ex}");
			}
		}
		private async Task PostBundleAsync(string name, byte[] bundle, byte[] manifest)
		{
			try
			{
				await _serverManager.PostBundle(bundle, name);
				await _serverManager.PostBundle(manifest, name + ".manifest");
			}
			catch (Exception ex)
			{
				Debug.LogError(ex.Message);
			}

		}
		private async Task PostBundleAsync(string name, byte[] data)
		{
			try
			{
				await _serverManager.PostBundle(data, name);
			}
			catch (Exception ex)
			{
				Debug.LogError(ex.Message);
			}

		}
		public static string GetBundleServerPath(string platform = "params")
		{
			InBuildParameters inBuildParameters = Resources.Load<InBuildParameters>("InBuildParameters");
			var bundleServerPath = $"{inBuildParameters.BuildType}/{platform}/{inBuildParameters.BundlePrefix}";
			return bundleServerPath;
		}
		public static string GetBundleFullPath(string platform)
		{
			InBuildParameters inBuildParameters = Resources.Load<InBuildParameters>("InBuildParameters");
			string bundleFullPath = inBuildParameters.BundleFolder + "/" + inBuildParameters.BuildType + "/" + platform + "/" + inBuildParameters.BundlePrefix;
			Directory.CreateDirectory(bundleFullPath);
			return bundleFullPath;
		}

	}
}

