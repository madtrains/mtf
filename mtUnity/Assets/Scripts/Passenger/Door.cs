using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : UberBehaviour
{
    public bool IsOccupied
    {
        get { return persons > 0; }
    }  

    [SerializeField] private Transform axis;
    [SerializeField] private float speed;
    [SerializeField] private float angleOpen;
    [SerializeField] private float angleClosed;
    private int persons;

    private Quaternion closed;
    private Quaternion opened;


    private void Start()
    {
        closed = Quaternion.Euler(Vector3.up * angleClosed);
        opened = Quaternion.Euler(Vector3.up * angleOpen);
    }

    private void OnTriggerEnter(Collider other)
    {
        persons++;
    }


    private void OnTriggerExit(Collider other)
    {
        persons--;
    }

    private void Update()
    {
        // opening
        if (IsOccupied)
        {
            axis.localRotation = Quaternion.Slerp(axis.localRotation, opened, Time.deltaTime * speed);
        }
        // closing
        else
        {
            axis.localRotation = Quaternion.Slerp(axis.localRotation, closed, Time.deltaTime * speed);
        }
    }
} 
