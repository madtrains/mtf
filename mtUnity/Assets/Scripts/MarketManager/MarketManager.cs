﻿using MTParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using static MTTown.MTMarket;

namespace MTTown
{
	public static class MTMarket
	{
		public enum MarketCurrency
		{
			MTC = 0,
			MTT = 8,
			MTG = 9
		}
		public enum MarketType
		{
			Resources,
			Trains,
			Coaches,
			Exschange
		}
	}
	public class MarketManager
	{
		private Dictionary<int, Dictionary<int, Dictionary<int, MarketInfo>>> CurrencyMarkets;

		private GameParameters _gp;
		private MarketManager() { }
		private MarketManager(GameParameters parameters)
		{
			_gp = parameters;

			CurrencyMarkets = new Dictionary<int, Dictionary<int, Dictionary<int, MarketInfo>>>();

			var coinMarkets = new Dictionary<int, Dictionary<int, MarketInfo>>();
			coinMarkets.Add((int)MarketType.Resources, new Dictionary<int, MarketInfo>());
			coinMarkets.Add((int)MarketType.Trains, new Dictionary<int, MarketInfo>());
			coinMarkets.Add((int)MarketType.Coaches, new Dictionary<int, MarketInfo>());
			coinMarkets.Add((int)MarketType.Exschange, new Dictionary<int, MarketInfo>());

			CurrencyMarkets.Add((int)MarketCurrency.MTC, coinMarkets);

			var cogMarkets = new Dictionary<int, Dictionary<int, MarketInfo>>();
			cogMarkets.Add((int)MarketType.Trains, new Dictionary<int, MarketInfo>());
			cogMarkets.Add((int)MarketType.Coaches, new Dictionary<int, MarketInfo>());

			CurrencyMarkets.Add((int)MarketCurrency.MTG, cogMarkets);

			FillupMarket();
		}

		public static MarketManager Init(GameParameters parameters)
		{
			var mm = new MarketManager(parameters);
			return mm;
		}
		private void FillupMarket()
		{
			FillupMarketResources();
			FillupMarketWithTrains();
			FillupMarketWithCoaches();
		}
		private void FillupMarketResources()
		{
			//заменить на данные из гугл таблиц...cоздать их cначала
			var Passenger = new MarketInfo()
			{
				Id = 1,
				MinAmount = 50,
				Price = 3000
			};

			var Mail = new MarketInfo()
			{
				Id = 2,
				MinAmount = 30,
				Price = 300,
				StrictPrice = true
			};

			var Wood = new MarketInfo()
			{
				Id = 5,
				MinAmount = 20,
				Price = 500
			};

			var currencyMarket = CurrencyMarkets[(int)MarketCurrency.MTC];
			var market = currencyMarket[(int)MarketType.Resources];

			market.Add(Passenger.Id, Passenger);
			market.Add(Mail.Id, Mail);
			market.Add(Wood.Id, Wood);
		}

		private void FillupMarketWithTrains()
		{
			var currencyMarket = CurrencyMarkets[(int)MarketCurrency.MTC];
			var market = currencyMarket[(int)MarketType.Trains];

			//заменить на данные из гугл таблиц...cоздать их cначала
			var marketInfo = _gp.Trains.ToList().Select(t => new MarketInfo()
			{
				Id = t.ID,
				MinAmount = 1,
				Price = t.Price
			});

			foreach (var info in marketInfo)
			{
				market.Add(info.Id, info);
			}

			//заменить на данные из гугл таблиц...cоздать их cначала
			marketInfo = _gp.Trains.ToList().Select(t => new MarketInfo()
			{
				Id = t.ID,
				MinAmount = 1,
				Price = t.CogCoinsPrice
			});

			currencyMarket = CurrencyMarkets[(int)MarketCurrency.MTG];
			market = currencyMarket[(int)MarketType.Trains];

			foreach (var info in marketInfo)
			{
				market.Add(info.Id, info);
			}
		}
		private void FillupMarketWithCoaches()
		{
			var currencyMarket = CurrencyMarkets[(int)MarketCurrency.MTC];
			var market = currencyMarket[(int)MarketType.Coaches];

			var marketInfo = _gp.Coaches.ToList().Select(t => new MarketInfo()
			{
				Id = t.ID,
				MinAmount = 1,
				Price = t.Price
			});

			foreach (var info in marketInfo)
			{
				market.Add(info.Id, info);
			}

			currencyMarket = CurrencyMarkets[(int)MarketCurrency.MTG];
			market = currencyMarket[(int)MarketType.Coaches];

			marketInfo = _gp.Coaches.ToList().Select(t => new MarketInfo()
			{
				Id = t.ID,
				MinAmount = 1,
				Price = t.CogCoinsPrice
			});

			foreach (var info in marketInfo)
			{
				market.Add(info.Id, info);
			}
		}

		public Dictionary<int, MarketInfo> GetMarket(MarketType marketType, MarketCurrency currency = MarketCurrency.MTC)
		{
			var currencyMarket = CurrencyMarkets[(int)currency];
			return currencyMarket[(int)marketType];
		}
		public bool TryGetResourceInfo(int resourceId, out MarketInfo result)
		{
			return TryGetMarketInfo(MarketType.Resources, resourceId, out result);
		}
		public bool TryGetTrainInfo(int resourceId, MarketCurrency currency, out MarketInfo result)
		{
			return TryGetMarketInfo(MarketType.Trains, resourceId, out result, currency);
		}
		public bool TryGetCoachInfo(int resourceId, MarketCurrency currency, out MarketInfo result)
		{
			return TryGetMarketInfo(MarketType.Coaches, resourceId, out result, currency);
		}
		public bool TryGetExchangeInfo(int resourceId, out MarketInfo result)
		{
			return TryGetMarketInfo(MarketType.Exschange, resourceId, out result);
		}
		public bool TryGetMarketInfo(MarketType marketType, int resourceId, out MarketInfo result, MarketCurrency currency = MarketCurrency.MTC)
		{
			var market = GetMarket(marketType, currency);
			if (market.ContainsKey(resourceId))
			{
				result = market[resourceId];
				return true;
			}
			result = null;
			return false;
		}
	}
	public class MarketInfo
	{
		public int Id;
		public int MinAmount;
		public int Price;
		private MarketCurrency MarketCurrency = MarketCurrency.MTC;
		public bool StrictPrice;
		public int Currency { get { return (int)MarketCurrency; } }
		public MarketInfo() { }
		public int GetPrice() => StrictPrice ? Price : (int)MathF.Ceiling(Price);
	}
}
