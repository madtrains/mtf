﻿using MTPlayer;
using UnityEngine;

namespace MTTown
{
	internal class MTEvent_First_Dialog: MTEvent
	{
		private PlayerManager _pm;

		public MTEvent_First_Dialog() : base() {
			_pm = GameStarter.Instance.PlayerManager;
		}
		override public bool PrepareCondition()
		{
			var result = !_pm.IsEventCompleted(GameStarter.GlobalEvents.FirstMeeting);
			return result;
		}
		override public bool ExecuteCondition() {
			return true; 
		}
		override public void EventBody()
		{
			var t = GameStarter.Instance.TownManager.Town as TownForest;

			t.AlterFritzPost.AlterFritz.Dialogs.GreetengsDialog(() =>

			{
				_pm.CompleteEvent(GameStarter.GlobalEvents.FirstMeeting);
			});
		}

		public override void EventAfter(System.Action callBack = null)
		{
			//temp to run event every time
			_pm.ClearEvent(GameStarter.GlobalEvents.FirstMeeting);
			base.EventAfter(callBack);
		}
	}
}