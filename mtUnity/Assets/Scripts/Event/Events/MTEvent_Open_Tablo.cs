﻿using MTTown.PointsOfInterest;
using UnityEngine;
using UnityEngine.Events;
using static MTCore.GamePlayManager;

namespace MTTown
{
	public class MTEvent_Open_Tablo : MTEvent
	{
		override public void EventBody()
		{
			try
			{
				//make depo icon visible
				GameStarter.Instance.UIRoot.Town.TownPointsSwitcher.SetButtonActive(Tag.SFDisplay, true);

				// Создаём делегат
				UnityAction sFDisplayHint = null;

				// Определяем логику подписки и отписки
				sFDisplayHint = () =>
				{
					MTStaticHints.TabloHint(() =>
					{
						// Отписка внутри коллбэка после завершения действий
						GameStarter.Instance.UIRoot.Town.OnShown -= sFDisplayHint;
						GameStarter.Instance.GamePlayManager.SaveProgress(ProgressMarks.TabloTownIcon);
					});
				};

				// Подписка на событие
				GameStarter.Instance.UIRoot.Town.OnShown += sFDisplayHint;
			}
			finally
			{
				Debug.LogWarning($"Execute event {this.GetType().Name}");
			}
		}
	}
}