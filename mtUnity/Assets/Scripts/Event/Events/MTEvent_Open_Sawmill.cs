﻿using MTTown.PointsOfInterest;
using UnityEngine;
using UnityEngine.Events;
using static MTCore.GamePlayManager;

namespace MTTown
{
	public class MTEvent_Open_Sawmill : MTEvent
	{
		override public void EventBody()
		{
			try
			{
				//make sawmill icon visible
				GameStarter.Instance.UIRoot.Town.TownPointsSwitcher.SetButtonActive(Tag.LumberMill, true);

				// Создаём делегат
				UnityAction sawMillHint = null;

				// Определяем логику подписки и отписки
				sawMillHint = () =>
				{
					MTStaticHints.SawMillHint(() =>
					{
						// Отписка внутри коллбэка после завершения действий
						GameStarter.Instance.UIRoot.Town.OnShown -= sawMillHint;
						GameStarter.Instance.GamePlayManager.SaveProgress(ProgressMarks.SawMillIcon);
					});
				};

				// Подписка на событие
				GameStarter.Instance.UIRoot.Town.OnShown += sawMillHint;
			}
			finally
			{
				Debug.LogWarning($"Execute event {this.GetType().Name}");
			}
		}
	}
}