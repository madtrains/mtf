﻿using MTCore;
using MTPlayer;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MTTown
{
    internal class MTEvent_Tutor_Task : MTEvent
	{
		public MTEvent_Tutor_Task() : base()
		{
		}
		override public void EventBody()
		{
			try
			{
				var gpm = GameStarter.Instance.GamePlayManager;

				var tutorTasks = gpm.GetCapibaras("Any").Where(c => c.Features.Contains("tutor"));

				foreach (var tt in tutorTasks)
				{
					gpm.CapibaraReadyToWork(tt);
					Debug.LogError($"capibara added {JsonUtility.ToJson(tt)}");
				}
				Debug.LogError($"capibaras added {tutorTasks.Count()}");
			}
			finally { 
				Debug.LogWarning($"Execute event {this.GetType().Name}");
			}
		}
		public override void EventAfter(System.Action callBack = null)
		{
			//temp to run event every time
			base.EventAfter(callBack);
		}
	}
}