﻿using MTTown.PointsOfInterest;
using UnityEngine;
using UnityEngine.Events;
using static MTCore.GamePlayManager;

namespace MTTown
{
	public class MTEvent_Open_Depo : MTEvent
	{
		override public void EventBody()
		{
			try
			{
				//make depo icon visible
				GameStarter.Instance.UIRoot.Town.TownPointsSwitcher.SetButtonActive(Tag.Depot, true);

				// Создаём делегат
				UnityAction depoHint = null;

				// Определяем логику подписки и отписки
				depoHint = () =>
				{
					MTStaticHints.DepoHint(() =>
					{
						// Отписка внутри коллбэка после завершения действий
						GameStarter.Instance.UIRoot.Town.OnShown -= depoHint;
						GameStarter.Instance.GamePlayManager.SaveProgress(ProgressMarks.DepoIcon);
					});
				};

				// Подписка на событие
				GameStarter.Instance.UIRoot.Town.OnShown += depoHint;
			}
			finally
			{
				Debug.LogWarning($"Execute event {this.GetType().Name}");
			}
		}
	}
}