﻿using Assets.Scripts.Dialog.Dialogs.Characters;
using MTPlayer;
using MTTown.Characters;
using MTTown.PointsOfInterest;
using UnityEngine;

namespace MTTown
{
	internal class MTEvent_Clear_Dialogs_And_Events : MTEvent
	{
		override public void EventBody()
		{
			var pm = GameStarter.Instance.PlayerManager;
			var t = GameStarter.Instance.TownManager.Town as TownForest;

			t.AlterFritzPost.AlterFritz.Dialogs.ClearCompletedDialogs();
			pm.ClearEvent(GameStarter.GlobalEvents.FirstMeeting);

			t.LumberMill.GrosserFrantz.Dialogs.ClearCompletedDialogs();
			t.LumberMill.GrosserFrantz.Dialogs.ClearDialog(MTDCharacterDialogs.BasicDialogs.Greeting.ToString());

			Debug.LogWarning("Clear events");
		}
	}
}