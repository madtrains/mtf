﻿using MTPlayer;
using System;
using UnityEngine;

namespace MTTown
{
    internal class MTEvent_Daily_Task : MTEvent
	{
		private PlayerManager _pm;
		private DateTime _lastUpdate;
		private int _updatePeriod = 2;
		public MTEvent_Daily_Task() : base()
		{
			_pm = GameStarter.Instance.PlayerManager;
			DateTime.TryParse(_pm.Data.GetString(GameStarter.GlobalValues.Update_Daily_Date_Time.ToString()), out _lastUpdate);
		}
		override public bool ExecuteCondition()
		{
			var readyToUpdate = (DateTime.Now - _lastUpdate) > TimeSpan.FromMinutes(_updatePeriod);
			//DateTime.TryParse(_pm.Data.GetString(GameStarter.GlobalValues.Update_Daily_Date_Time.ToString()), out var update_Daily_Date_Time);
			//DateTime.TryParse(_pm.Data.GetString(GameStarter.GlobalValues.Last_Daily_Update_Date_Time.ToString()), out var last_Daily_Update_Date_Time);
			return readyToUpdate;
		}
		override public void EventBody()
		{
			try
			{
				var gpm = GameStarter.Instance.GamePlayManager;

				var dailyCapibaras = gpm.GetCapibaras("daily", "weekly");
				foreach (var capibara in dailyCapibaras)
				{
					gpm.CapibaraReadyToWork(capibara);
				}
			}
			finally { 
				_lastUpdate = DateTime.Now;
				_pm.Data.SetString(GameStarter.GlobalValues.Last_Daily_Update_Date_Time.ToString(), _lastUpdate.ToString("G"));
				Debug.LogWarning($"Execute event {this.GetType().Name}");
			}
		}
	}
}