﻿using UnityEngine;

namespace MTTown
{
	public class MTEvent
	{
		public bool keepAlive;
		virtual public bool ExecuteCondition() { return true; }
		virtual public bool PrepareCondition() { return true; }
		virtual public void Prepare() { }
		virtual public void Execute() {
			var readyToExecute = ExecuteCondition();

			if (readyToExecute)
			{
				EventBefore();
				EventBody();
				EventAfter();
			}
		}
		virtual public void EventBefore()
		{
		}
		virtual public void EventBody()
		{
		}
		virtual public void EventAfter(System.Action callBack = null)
		{
			callBack?.Invoke();
		}
	}
}