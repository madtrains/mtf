﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MTTown
{
	public class EventManager
	{
		private List<MTEvent> GameEvents;
		private EventManager()
		{
			GameEvents = new List<MTEvent>();
		}
		public static EventManager Init()
		{
			var mte = new EventManager();

			//mte.Proceed(new MTEvent_Clear_Dialogs_And_Events());
			//mte.Proceed(new MTEvent_First_Dialog());
			mte.ProcessEvent(new MTEvent_Daily_Task(), true);
			mte.ProcessEvent(new MTEvent_Tutor_Task());

			return mte;
		}

		public void RunEvents()
		{
			//Debug.LogWarning($"Run events - {GameEvents.Count}");
			var eventsToRun = GameEvents.ToList();
			foreach (var eventMember in eventsToRun)
			{
				eventMember.Execute();
				if (!eventMember.keepAlive && GameEvents.Contains(eventMember))
				{
					GameEvents.Remove(eventMember);
					Debug.LogError($"Remove {eventMember.GetType().Name} from events list");
				}
			}
		}

		public void ProcessEvent(MTEvent globalEvent, bool keepAlive = false)
		{
			var ready = globalEvent.PrepareCondition();
			if (ready)
			{
				globalEvent.keepAlive = keepAlive;
				globalEvent.Prepare();
				GameEvents.Add(globalEvent);

				Debug.LogError($"Add {globalEvent.GetType().Name} to events list");
			}
		}

		public void ProcessEvent(string eventName, bool keepAlive = false)
		{
			try
			{
				// Расширенный поиск типа
				Type eventType = Type.GetType(eventName) ??
								 AppDomain.CurrentDomain.GetAssemblies()
								 .SelectMany(a => a.GetTypes())
								 .FirstOrDefault(t => t.Name == eventName);

				if (eventType == null)
				{
					Debug.LogError($"Type '{eventName}' not found.");
					return;
				}

				// Проверка наследования
				if (!typeof(MTEvent).IsAssignableFrom(eventType))
				{
					Debug.LogError($"Type '{eventName}' is not a subclass of MTEvent.");
					return;
				}

				// Создание экземпляра
				var gameEvent = Activator.CreateInstance(eventType) as MTEvent;
				if (gameEvent == null)
				{
					Debug.LogError($"Failed to instantiate type '{eventName}'. Ensure it has a default constructor.");
					return;
				}

				Debug.Log($"Event of type '{eventName}' created successfully.");

				// Обработка события
				ProcessEvent(gameEvent, keepAlive);
			}
			catch (Exception ex)
			{
				Debug.LogError($"Error creating or processing event '{eventName}': {ex.Message}");
			}
		}

	}
}