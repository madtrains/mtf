﻿using MTUi;
using Trains;
using UnityEngine;
using WaySystem;
using static Trains.Train;

namespace TutorialFlipper
{
    namespace Steps
    {
        public delegate void StepDelegate();

        [System.Serializable]
        public class Step
        {
            public UnityEngine.Events.UnityAction OnDone;
            protected Step next;

            public Step()
            {
            }

            public virtual void Run() { }

            public virtual void Done() { Next(); OnDone?.Invoke(); }

            public void Next()
            {
                if (next != null)
                    next.Run();
            }

            public void LinkTo(Step target)
            {
                target.next = this;
            }
        }

        [System.Serializable]
        public class ReturnCamera : Step
        {
            [SerializeField] protected float speed;

            public ReturnCamera(float speed)
            {
                this.speed = speed;
            }

            public override void Run()
            {
                base.Run();
                GameStarter.Instance.MainComponents.GameCamera.MoveTo(
                    GameStarter.Instance.Train.transform.position,
                    GameStarter.GameParameters.Cameras[6],
                    () =>
                    {
                        Done();
                    },
                    (value) => { }, speed, speed);
            }
        }

        [System.Serializable]
        public class Dialog : Step
        {

            [SerializeField] protected string text;
            [SerializeField] protected bool once;
            public Dialog(string text, bool once)
            {
                this.text = text;
                this.once = once;
            }

            protected void ShowDialog()
            {
                if (once)
                {
                    string okString = GameStarter.Instance.TranslationManager.GetTableString("Dialog.Ok");
                    GameStarter.Instance.UIRoot.Dialog.Show(
                        text, GameCharacter.Fritz,
                        new MTUi.Town.DialogButton[] {new MTUi.Town.DialogButton(
                            okString,
                            () =>
                            {
                                GameStarter.Instance.UIRoot.Dialog.Hide();
                                Done();
                            }
                        )});
                }
                else
                {
                    GameStarter.Instance.UIRoot.Dialog.Show(
                        text, GameCharacter.Fritz,
                        new MTUi.Town.DialogButton[] {new MTUi.Town.DialogButton(
                            "...",
                            () =>
                            {
                                Done();
                            }
                        )});
                }
            }

            public override void Run()
            {
                base.Run();
                ShowDialog();
            }
        }

        [System.Serializable]
        public class DialogAndArrow : Dialog
        {
            [SerializeField] protected Helpers helpers;
            [SerializeField] protected Point point;
            [SerializeField] protected MinMax heights;

            public DialogAndArrow(string text, bool once,
                Helpers flipperTutorial, Point point, float minHeight, float maxHeight) 
                : base(text, once)
            {
                this.helpers = flipperTutorial;
                this.point = point;
                this.heights = new MinMax(minHeight, maxHeight);
            }

            public override void Run()
            {
                helpers.Arrow.PointAt(point, heights);
                base.Run();
            }

            public override void Done()
            {
                helpers.Arrow.ArrowVisible = false;
                base.Done();
            }
        }

        [System.Serializable]
        public class CameraFlyTo : Step
        {
            [SerializeField] protected Vector3 cameraTarget;
            [SerializeField] protected MTParameters.CameraParameters cameraParameters;
            [SerializeField] protected float speed;

            public CameraFlyTo(
                Vector3 cameraTarget, MTParameters.CameraParameters cameraParameters, float speed)
            {
                this.cameraTarget = cameraTarget;
                this.cameraParameters = cameraParameters;
                this.speed = speed;
            }

            public override void Run()
            {
                base.Run();
                GameStarter.Instance.MainComponents.GameCamera.MoveTo(cameraTarget, cameraParameters,
                    () =>
                    {
                        Done();
                    },
                    (value) => { }, speed, speed);
            }
        }

        [System.Serializable]
        public class Delegate : Step
        {
            protected StepDelegate stepDelegate;
            public Delegate(
                StepDelegate stepDelegate)
            {
                this.stepDelegate = stepDelegate;
            }

            public override void Run()
            {
                base.Run();
                this.stepDelegate += () => { Done(); };
                this.stepDelegate();
            }
        }

        [System.Serializable]
        public class SetFLipperScreenVisibility : Step
        {
            bool mode;

            public SetFLipperScreenVisibility(bool mode)
            {
                this.mode = mode;
            }

            public override void Run()
            {
                base.Run();
                if (mode)
                {
                    GameStarter.Instance.UIRoot.Flipper.Show();
                }
                else
                {
                    GameStarter.Instance.UIRoot.Flipper.Hide();
                }
                Done();
            }
        }

        [System.Serializable]
        public class TrainStop : Step
        {

            public TrainStop()
            {
                
            }

            public override void Run()
            {
                base.Run();
                GameStarter.Instance.Train.Stop();
                Done();
            }
        }

        [System.Serializable]
        public class TrainGo : Step
        {
            EngineSpeedState state;
            bool isMainCameraControlledByTrain;

            public TrainGo(Train.EngineSpeedState state, bool isMainCameraControlledByTrain)
            {
                this.state = state;
                this.isMainCameraControlledByTrain = isMainCameraControlledByTrain;
            }

            public override void Run()
            {
                base.Run();
                GameStarter.Instance.Train.Go(state, isMainCameraControlledByTrain);
                Done();
            }
        }

        [System.Serializable]
        public class UIHighlight : Step
        {
            private string text;
            private float transparency;
            private RectTransform target;
            private Popup.Placing placing;

            public UIHighlight(
                string text, float transparency, RectTransform target, Popup.Placing placing)
            {
                this.text = text;
                this.transparency = transparency;
                this.target = target;
                this.placing = placing;
            }

            public override void Run()
            {
                base.Run();
                GameStarter.Instance.UIRoot.Popup.ShowTutorialCircleOkButton(target, text, transparency, () =>
                {
                    Done();
                },
                placing);
            }
        }
    }
}