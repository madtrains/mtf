﻿using Trains;
using TutorialFlipper.Steps;
using UnityEngine;

namespace TutorialFlipper
{
    [System.Serializable]
    public class Tutorial
    {
        public UnityEngine.Events.UnityAction OnDone;

        public Tutorial()
        {
            steps = new OberList<Step>();
        }

        [SerializeField] private OberList<Step> steps;

        public void Launch()
        {
            steps.First.Run();
            steps.Last.OnDone += OnDone;
        }

        public void AddStep(Step newStep)
        {
            if (steps == null)
                steps = new OberList<Step>();
            else
            {
                if (steps.Count > 0)
                    newStep.LinkTo(steps.Last);
            }
            steps.Add(newStep);
        }
    }
}