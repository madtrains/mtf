﻿using Trains;
using System.Collections.Generic;
using UnityEngine;

namespace Triggers
{
    /// <summary>
    /// Триггер для поезда
    /// </summary>
    public class TrainTrigger : Trigger
    {
        public UnityEngine.Events.UnityAction<Train> OnTrain;
        public UnityEngine.Events.UnityAction<Coach> OnCoach;
        protected override void OnRigidBodyEnterTrigger(Rigidbody rgbd)
        {
            base.OnRigidBodyEnterTrigger(rgbd);
            Train train = rgbd.gameObject.GetComponent<Train>();
            if (train != null)
            {
                OnTrainEntersTrigger(train);
                if (OnTrain != null)
                    OnTrain(train);
            }

            Coach coach = rgbd.gameObject.GetComponent<Coach>();
            if (coach != null)
            {
                OnCoachEntersTrigger(coach);
                if (OnCoach != null)
                    OnCoach(coach);
            }
        }


        protected virtual void OnTrainEntersTrigger(Train train)
        {

        }


        protected virtual void OnCoachEntersTrigger(Coach coach)
        {

        }
    }
}