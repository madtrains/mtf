using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Semaphore2Section : UberBehaviour
{

    [SerializeField] private ContentHolders lights;

    public void Green()
    {
        lights.Activate(2);
    }

    public void Red()
    {
        lights.Activate(1);
    }

    public void Off()
    {
        lights.Activate(0);
    }
}
