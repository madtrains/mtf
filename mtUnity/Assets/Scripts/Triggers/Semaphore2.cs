﻿using Trains;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Triggers
{
    public class Semaphore2 : UberBehaviour
    {
        [SerializeField] private Semaphore2Section[] sections;
        [SerializeField] private Transform[] docks;
        

        public void Init(int sectionsNumber, int trainCanTake)
        {
            List<Semaphore2Section> lst = new List<Semaphore2Section>(sections);
            for (int i = 1; i < sectionsNumber; i++)
            {
                lst.Add(Clone(docks[i]));
            }
            this.sections = lst.ToArray();
        }


        private Semaphore2Section Clone(Transform target)
        {
            Semaphore2Section clone = GameObject.Instantiate<Semaphore2Section>(sections[0]);
            clone.transform.SetParent(target, false);
            return clone;
        }


        public void SetImmediate(int value)
        {
            for (int i = 0; i < sections.Length; i++)
            {
                Semaphore2Section current = sections[i];
                if (i < value)
                    current.Green();
                else
                    current.Red();
            }
        }


        public void Run(int lightsNumber)
        {
            var t = TimerBeh.CreateTimer();
            t.SelfDestructable = true;
            
            t.OnStep += (int step) =>
            {
                //Debug.LogWarningFormat("Semaphore Step {0}", v);
                if (sections.Length > step)
                {
                    if (lightsNumber > step)
                        sections[step].Green();
                    else
                        sections[step].Red();
                }
            };
            t.Launch(0.2f, 3);
        }


        public void Reset()
        {
            for (int i = 0; i < sections.Length; i++)
            {
                Semaphore2Section current = sections[i];
                current.Off();
             }
        }
    }
}