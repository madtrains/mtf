﻿using Trains;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WaySystem;


namespace Triggers
{
    public class Semaphore : TrainTrigger
    {

        [SerializeField] private MTUi.ContentSwitcher switcher;


        public WaySystem.WayChunk WayChunk { get { return wayChunk; } }

        public float Offset  { get { return offset; } }
        //[SerializeField] private
        private bool on;
        private WaySystem.WayChunk wayChunk;
        private float offset;
        /// <summary>
        /// индекс пути рестарта группы путей (холостой путь)
        /// </summary>
        private int wayIndex;

        public void Init(WayChunk wayChunk, float offset)
        {
            this.wayChunk = wayChunk;
            this.offset = offset;
        }

        public void Init(WayChunk wayChunk, float offset, Vector3 semaphorePosition)
        {
            Init(wayChunk, offset);
            this.transform.position = semaphorePosition;
        }

        protected override void OnTrainEntersTrigger(Train train)
        {
            base.OnTrainEntersTrigger(train);
            //семафор срабатыывает только один раз
            if (!on)
            {
                on = true;
                switcher.Activate(1); //зелёный свет семафора
                GameStarter.Instance.UIRoot.Flipper.CheckPoint(); //показываем в интерфейсе
                train.SaveSemaphore(this);//передаём в поезд текущий семафор
            }
        }
    }
}