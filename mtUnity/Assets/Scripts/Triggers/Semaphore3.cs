﻿using Trains;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MTUi;

namespace WaySystem.Additions
{
    public class Semaphore3 : ContentSwitcher
    {
        public enum Light { Off, Red, Yellow, Green, Blue, White }

        public void Activate (Light light)
        {
            this.Activate((int)light);
        }
    }
}