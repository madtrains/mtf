﻿using Trains;
using TutorialFlipper;
using TutorialFlipper.Steps;

namespace Triggers
{
    public class TrainTutorialTrigger : TrainTrigger
    {
        
        private Tutorial tutorial;
        private bool done;


        public void AssignTutor(Tutorial tutor)
        {
            this.tutorial = tutor;
        }

        public void AddStep(Step step)
        {
            this.tutorial.AddStep(step);
        }

        protected override void OnTrainEntersTrigger(Train train)
        {
            base.OnTrainEntersTrigger(train);
            if (!done)
            {
                done = true;
                tutorial.Launch();
            }
        }
    }
}