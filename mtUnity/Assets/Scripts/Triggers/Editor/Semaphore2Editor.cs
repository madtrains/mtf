﻿using Trains;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


namespace Triggers
{
    [CustomEditor(typeof(Semaphore2))]
    public class Semaphore2Editor : UberEditor
    {


        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            Semaphore2 semaphore2 = target as Semaphore2;

            if (GUILayout.Button("Off"))
            {
                semaphore2.SetImmediate(0);
            }

            if (GUILayout.Button("One"))
            {
                semaphore2.SetImmediate(1);
            }

            if (GUILayout.Button("Two"))
            {
                semaphore2.SetImmediate(2);
            }

            if (GUILayout.Button("Three"))
            {
                semaphore2.SetImmediate(3);
            }

            if (GUILayout.Button("One Anim"))
            {
                semaphore2.Run(3);
            }
        }
    }
}