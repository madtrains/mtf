﻿
using UnityEditor;

namespace Triggers
{
    [CustomEditor(typeof(WaySystem.PickUp.PickUp))]
    public class RRPickUpEditor : UberEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            /*
            PickUp rrPickUp = target as PickUp;
            if (rrPickUp.Index >= MTParameters.GameParametersViewer.GetParameters().PickUps.Length)
            {
                EditorGUILayout.HelpBox(string.Format("NO PickUp present for index {0}", rrPickUp.Index),
                    MessageType.Error);
            }

            else
            {
                TranslationManager translationManager = new TranslationManager(MTParameters.GameParametersViewer.GetParameters());
                string name = translationManager.GetTableString(MTParameters.GameParametersViewer.GetParameters().PickUps[rrPickUp.Index].TranslationKey);
                EditorGUILayout.HelpBox(string.Format("PickUp : {0} : {1}", rrPickUp.Index, name),
                    MessageType.Info);
            }
            */
        }
    }
}