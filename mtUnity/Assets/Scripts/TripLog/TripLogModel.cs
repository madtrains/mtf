using Trains;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;


public class TripLogModel 
{
	public string GetHeaders()
	{
       var logFields = this.GetType().GetFields(BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance).Where(prop => prop.IsDefined(typeof(LogMember), false)).ToList();
       StringBuilder headers = new StringBuilder();

        foreach (var field in logFields)
        {
            LogMember attr = (LogMember)field.GetCustomAttribute(typeof(LogMember));
            headers.Append(attr.header); headers.Append("\t");
        }

        return headers.ToString();
	}
    /// <summary>
    /// ������ ������
    /// </summary>
    [LogMember(header: "Size")]
    public MTCore.TripSize size;

    /// <summary>
    /// ����� ����� ������� � ��������
    /// </summary>
    [LogMember(header: "Trip Time")]
    public int tripTime;

    /// <summary>
    /// ��� ������, ��� ���� ����������� �� ������
    /// </summary>
    [LogMember(header: "All Coins On Level")]
    public int allCoinsOnLevel;

    /// <summary>
    /// ��� ������, ��� ���� �������, ������� ����������
    /// </summary>
    [LogMember(header: "All Coins Gathered")]
    public int allCoinsGathered;

    /// <summary>
    /// ������� ���������� �����
    /// </summary>
    [LogMember(header: "Coins Lost")]
    public int coinsLost;

    /// <summary>
    /// ������� ������ �������� �������
    /// </summary>
    [LogMember(header: "Coins Gathered")]
    public int coinsGathered;

    /// <summary>
    /// ������� ���������� �������
    /// </summary>
    [LogMember(header: "Resources")]
    public int resourceGathered;

    /// <summary>
    /// ����� ���������� �������
    /// </summary>
    [LogMember(header: "Deaths")]
    public int deaths;

    /// <summary>
    /// ����� ���������� ������, ������� �������� � ������ ������
    /// </summary>
    [LogMember(header: "Hits")]
    public int hits;

    /// <summary>
    /// ����� ���������� ��������� �� ������
    /// </summary>
    [LogMember(header: "All Gears on Level")]
    public int allGearsOnLevel;

    /// <summary>
    /// ���������� ��������� �� ������ ���������, ������� ��, ��� �� ����� ��������� �����, ��� ��� ����� ���� �����
    /// </summary>
    [LogMember(header: "Gears Gathered")]
    public int gearsCollected;

    /// <summary>
    /// ���������� ��������� �� ������ ���������, ������� ��, ��� �� ����� ��������� �����, ��� ��� ����� ���� �����
    /// </summary>
    [LogMember(header: "Train Index")]
    public int trainIndex;

    /// <summary>
    /// ������ �������
    /// </summary>
    [LogMember(header: "Coaches Number\tCoach1\tCoach2\tCoach3")]
    public int[] coaches;

    public System.DateTime timeStart;
    public System.DateTime timeEnd;

    public override string ToString()
    {
        StringBuilder st = new StringBuilder();
        st.Append(size); st.Append("\t");
        st.Append(tripTime); st.Append("\t");
        st.Append(allCoinsOnLevel); st.Append("\t");
        st.Append(allCoinsGathered); st.Append("\t");
        st.Append(coinsLost); st.Append("\t");
        st.Append(coinsGathered); st.Append("\t");
        st.Append(resourceGathered); st.Append("\t");
        st.Append(deaths); st.Append("\t");
        st.Append(hits); st.Append("\t");
        st.Append(allGearsOnLevel); st.Append("\t");
        st.Append(gearsCollected); st.Append("\t");
        st.Append(trainIndex);  st.Append("\t");
        st.Append(coaches.Length); st.Append("\t");
        foreach (int i in coaches)
        {
            st.Append(i); st.Append("\t");
        }
        return st.ToString();
    }
}
[AttributeUsage(AttributeTargets.Field)]
public class LogMember : Attribute
{
    private string _header;
    public string header => _header;
    public LogMember(string header)
    {
        _header = header;
    }
}