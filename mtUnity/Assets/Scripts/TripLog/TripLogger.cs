[System.Serializable]
public class TripLogger
{
	public TripLogModel Data;

	public TripLogger()
	{
		Data = new TripLogModel();
	}

	public void Save()
	{
		var th = GameStarter.Instance.PlayerManager.TripHistory;
		if (th.Length == 0)
		{
			th.AddData(Data.GetHeaders());
		}
		th.AddData(Data.ToString());
		th.SAVE();
	}

	public TripLogger(WaySystem.WayRoot wayRoot, int trainIndex, params int[] coaches)
	{
		Data = new TripLogModel();
		Data.tripTime = 0;
		Data.allCoinsGathered = 0;
		Data.coinsLost = 0;
		Data.coinsGathered = 0;
		Data.resourceGathered = 0;
		Data.deaths = 0;
		Data.hits = 0;
		Data.gearsCollected = 0;
		Data.trainIndex = trainIndex;
		Data.coaches = coaches;
		Data.timeStart = System.DateTime.Now;
	}

	public void AddCoin()
	{
		Data.allCoinsGathered += 1;
	}

	public void AddGearRepair()
	{
		Data.gearsCollected += 1;
	}

	public void AddHit()
	{
		Data.hits += 1;
	}

	public void AddDeath(int lostCoinsNumber)
	{
		Data.deaths += 1;
		Data.coinsLost += lostCoinsNumber;
	}

	public void CloseTrip(int coinsGathered, int resourceGathered)
	{
		Data.timeEnd = System.DateTime.Now;
		Data.coinsGathered = coinsGathered;
		Data.resourceGathered = resourceGathered;
		Data.tripTime = (int)(Data.timeEnd - Data.timeStart).TotalSeconds;
		GameStarter.Instance.PlayerManager.applyTripResults(Data);

		Save();
	}
}