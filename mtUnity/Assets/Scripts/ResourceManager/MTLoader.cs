﻿using MT_ServerDataManager;
using MTParameters;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Pool;

namespace MTCore
{
	public class MTLoader
	{
		private const int DELAY_TIME = 100;

		public MTLoader()
		{

		}

		internal static string GetCachePath()
		{

			var path = Path.Combine(InBuildParameters.InBuildParametersInstance.LocalStoragePath, "MT_STORAGE", "MT_BUNDLES_CACHE", InBuildParameters.InBuildParametersInstance.BundlePrefix);

			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}
			return path;
		}
		internal static void CacheText(string fileName, string data)
		{
			var cachDir = GetCachePath();
			var cacheFilePath = Path.Combine(cachDir, $"{fileName}");
			File.WriteAllText(cacheFilePath, data);
		}
		internal static async Task CacheBundle(string fileName, byte[] data)
		{
			var cacheFilePath = GetBundleCacheFullPath(fileName);
			Debug.LogError($"cacheFilePath - {cacheFilePath}");
			await File.WriteAllBytesAsync(cacheFilePath, data);
		}
		internal bool IsBundleCached(string fileName)
		{
			var cacheFilePath = GetBundleCacheFullPath(fileName);
			return File.Exists(cacheFilePath);
		}
		internal Hash128 GetBundleHash(string fileName)
		{
			var manifestName = fileName + ".manifest";
			var cacheManifestFilePath = GetBundleCacheFullPath(manifestName);
			if (File.Exists(cacheManifestFilePath))
			{
				var matifest = File.ReadAllText(cacheManifestFilePath); ;
				//получаем hash
				var hashRow = matifest.Split("\n".ToCharArray())[5];
				var hash = Hash128.Parse(hashRow.Split(':')[1].Trim());
				return hash;
			}
			return default;
		}

		internal static string GetBundleCacheFullPath(string fileName)
		{
			var cachDir = GetCachePath();
			var bundleName = GetBundleCacheName(fileName);
			var cacheFilePath = Path.Combine(cachDir, bundleName);
			return cacheFilePath;
		}

		internal static string GetBundleCacheName(string fileName)
		{
			return $"{fileName}".Replace("/", "_").Replace("\\", "_").ToLower();
		}
		internal static string GetTextFromCache(string fileName)
		{
			var cachDir = GetCachePath();
			var cacheFilePath = Path.Combine(cachDir, $"{fileName}");
			if (File.Exists(cacheFilePath))
			{
				return File.ReadAllText(cacheFilePath);
			}
			return null;
		}
		internal async Task GetStringAsync(string url, System.Action<string> callBack)
		{
			callBack(await GetStringAsync(url));
		}
		internal async Task<string> GetStringAsync(string url)
		{
			using (var hc = new HttpClient())
			{
				try
				{
					var response = await hc.GetStringAsync(url);
					Debug.LogFormat("Data loaded: {0}", url);
					return response;
				}
				catch (Exception ex)
				{
					Debug.LogError(url);
					Debug.LogError(ex);
				}
			}
			return String.Empty;
		}
		internal static string GetBundleRequestUrl(string bundleName, bool isGp = false)
		{
			string url =
			//"https://localhost:7005"+
			InBuildParameters.InBuildParametersInstance.ServerUrl +
			MT_ServerDataManager.ServerDataManager.ENDPOINT_BUNDLE +
			GetBundleServerName(bundleName, isGp);
			return url;
		}
		internal static string GetBundleServerName(string bundleName, bool isGp = false)
		{
			var platform = "/params";

			if (!isGp)
			{

#if UNITY_ANDROID
				platform = "/android";
#elif UNITY_EDITOR
				platform = "/editor";
#elif UNITY_IPHONE
		 platform = "/iphone";
#else
#endif
			}
			string name = "/" +
			$"{InBuildParameters.InBuildParametersInstance.BuildType}{platform}/{InBuildParameters.InBuildParametersInstance.BundlePrefix}/{bundleName}".Replace("/", "_").Replace("\\", "_");
			return name;
		}
		internal static bool IsServerAvailableAsync
		{
			get
			{
				if (InBuildParameters.InBuildParametersInstance.Offline)
				{
					Debug.Log($"Offline mode enabled");
				}
				else if (IsInternetAvailable)
				{
					Debug.Log($"Internet is Available");

					ServerDataManager sm = new MT_ServerDataManager.ServerDataManager(InBuildParameters.InBuildParametersInstance.ServerUrl);

					Task<bool> task = Task.Run<bool>(async () => await sm.IsServerAvailable());
					if (task.Result)
					{
						Debug.Log($"Server is Available");
						return true;
					}
					else
					{
						Debug.LogWarning($"Server is not Available");
					}
				}
				else
				{
					Debug.LogWarning($"Internet is not Available");
				}
				return false;
			}
		}
		internal static bool IsInternetAvailable
		{
			get
			{
				NetworkReachability reachability = Application.internetReachability;

				if (reachability == NetworkReachability.NotReachable)
				{
					return false;
				}

				else if (reachability == NetworkReachability.ReachableViaCarrierDataNetwork)
				{
					return true;
				}

				else if (reachability == NetworkReachability.ReachableViaLocalAreaNetwork)
				{
					return true;
				}

				return false;
			}
		}
		internal static async Task WaitForUpdate(int miliseconds = DELAY_TIME)
		{
			await Task.Delay(miliseconds);
		}
	}
}
