﻿using MTParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace MTCore
{
	public class ParamsLoader : MTLoader
	{
		public bool UpdateResources = false;
		public async Task<T> LoadParamAsync<T>(TextAsset asset, TextAsset assetManifest, UnityEngine.Events.UnityAction<T> callBack = null)
		{
			T param = await LoadParamAsync<T>(asset, assetManifest);
			if (callBack != null)
				callBack(param);
			return param;
		}
		public async Task<T> LoadParamAsync<T>(TextAsset asset, TextAsset assetManifest)
		{
			var parameters = JsonUtility.FromJson<T>(asset.text);
			var manifest = JsonUtility.FromJson<JsonManifest>(assetManifest.text);

			Debug.Log($"Params Loader: Got {asset.name} from build v:{manifest.Version}");

			string cachedParametersManifestString = GetTextFromCache($"{assetManifest.name}.txt");

			if (!string.IsNullOrEmpty(cachedParametersManifestString))
			{
				JsonManifest cachedParametersManifest = JsonUtility.FromJson<JsonManifest>(cachedParametersManifestString);
				if (cachedParametersManifest.IsFresher(manifest))
				{
					manifest = cachedParametersManifest;
					string cachedGameParametersString = GetTextFromCache($"{asset.name}.json");
					T cachedParameters = JsonUtility.FromJson<T>(cachedGameParametersString);
					parameters = cachedParameters;
					Debug.Log($"Params Loader: Cache {asset.name} v:{manifest.Version}");
					UpdateResources = true;
				}
			}

			if (InBuildParameters.InBuildParametersInstance.Offline)
			{
				Debug.Log($"Offline mode enabled");
			}
			else if (IsServerAvailableAsync)
			{
				Debug.Log($"Params Loader: Checking {asset.name} server version");

				try
				{
					string url = GetBundleRequestUrl($"{assetManifest.name}.txt", true);
					await GetStringAsync(url, async (text) =>
					{
						if (string.IsNullOrEmpty(text))
						{
							Debug.LogErrorFormat("Params Loader: No text got in manifest");
							return;
						}

						JsonManifest manifestInBundle = JsonUtility.FromJson<JsonManifest>(text);

						if (manifestInBundle.IsFresher(manifest))
						{
							Debug.LogFormat("Params Loader: Server Version higher then local {0} > {1}", manifestInBundle.Version, manifest.Version);
							string paramUrl = GetBundleRequestUrl($"{asset.name}.json", true);
							manifest = manifestInBundle;
							await GetStringAsync(paramUrl, (paramText) =>
							{
								Debug.Log("Params Loader: Caching");
								CacheText($"{asset.name}.json", paramText);
								CacheText($"{assetManifest.name}.txt", JsonUtility.ToJson(manifest));

								T serverParameters = JsonUtility.FromJson<T>(paramText);
							});
							UpdateResources = true;
						}
						else
						{
							Debug.LogFormat("Params Loader: Version on server: {0} in build/cache: {1}\nNo need to download it from server ",
								manifestInBundle.Version, manifest.Version);
							return;
						}
					});
				}
				catch (Exception ex)
				{
					Debug.Log($"Some error: {ex.Message}");
				}
			}
			return parameters;
		}
	}
}
