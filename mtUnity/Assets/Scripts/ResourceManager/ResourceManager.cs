﻿using MTParameters;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace MTCore
{
    public class ResourceManager
	{
		public GameParameters GameParameters { get; private set; }

		private ParamsLoader ParamsLoader { get; set; }
		private ResourceLoader ResourceLoader { get; set; }

		public static bool IsInternerAvailable
		{
			get
			{
				return ResourceLoader.IsInternetAvailable;
			}
		}

		public static bool IsServerAvailable
		{
			get { return ResourceLoader.IsServerAvailableAsync; }
		}


		public bool WithLogs;
		public ResourceManager()
		{
			ParamsLoader = new ParamsLoader();
			ResourceLoader = new ResourceLoader();
		}

		public static async Task<ResourceManager> Init(InBuildParameters InBuildParameters)
		{
			ResourceManager rm = new ResourceManager();

			//await rm.LoadGameParameters(InBuildParameters);
			//await rm.LoadAssetManager(InBuildParameters);
			rm.ResourceLoader.ForceUpdate = InBuildParameters.ForceUpdate;
			rm.ResourceLoader.UpdateResources = rm.ParamsLoader.UpdateResources;
			await rm.LoadBundles(InBuildParameters);
			await rm.LoadParamsAndAssetsFromBundles();
			return rm;
		}

		public async Task LoadParamsAndAssetsFromBundles()
		{
			UnityEngine.Object obj = await LoadByBundleName(MTAssetBundles.game_parameters);
            ParametersHolder paramsHolder = obj as ParametersHolder;
			if (paramsHolder != null)
			{
				Debug.LogWarning("Params holder loaded");
				if (paramsHolder.gameParameters != null)
				{
					Debug.LogWarning("Params loaded");
					GameParameters = paramsHolder.gameParameters;
					GameParameters.LoadPostProcess();
				}
			}
		}

		public async Task LoadBundles(InBuildParameters InBuildParameters)
		{
			if (!IsServerAvailable)
				return;

			Debug.LogError("Check server bundles...");

			var bundlesListManifestName = InBuildParameters.BundlePrefix;

			//добавить проверку манифеста
			await ResourceLoader.LoadBundleFromServer(bundlesListManifestName);


			var bundle = await ResourceLoader.LoadAssetBundleFromServerWithCache(bundlesListManifestName);
			if (bundle == null)
			{
				Debug.LogError("Bundles manifest is not loaded");
				return;
			}
			var manifest = bundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
			if (manifest == null)
			{
				Debug.LogError("Bundle manifest is null");
				return;
			}
			// Получение списка всех Asset Bundles
			string[] allAssetBundles = manifest.GetAllAssetBundles();

			var bundlesToLoad = new List<string>();

			foreach (string assetBundleName in allAssetBundles)
			{
				if (ResourceLoader.IsBundleCached(assetBundleName))
				{
					Hash128 serverHash = manifest.GetAssetBundleHash(assetBundleName);
					Debug.Log($"Already in cache - {assetBundleName}");
					Hash128 bunbleHash = ResourceLoader.GetBundleHash(assetBundleName);
					if (serverHash.CompareTo(bunbleHash) == 0)
						continue;
					Debug.Log($"Reload - {assetBundleName}");
				}
				bundlesToLoad.Add(assetBundleName);
			}
			if (bundlesToLoad.Any())
			{
				var index = 1;
				GameStarter.Instance.UIRoot.Splash.SetBundles();
				foreach (var bundleName in bundlesToLoad)
				{
					GameStarter.Instance.UIRoot.Splash.SetBundlesLoad(index, bundlesToLoad.Count);
					Debug.Log($"Bundle {index++}/{allAssetBundles.Length}");

					//загружаем бандл новым методом
					await ResourceLoader.LoadBundleFromServer(bundleName);
				}
			}
			Debug.Log("Asset Bundles Ready!!");

			// Не забыть выгрузить AssetBundle после использования
			bundle.Unload(false);
			ResourceLoader.bundlesManifest = manifest;
		}

		public async Task<T> InstantiateByBundleName<T>(MTAssetBundles bundleName) where T : Component
		{
			UnityEngine.Object loaded = await LoadByBundleName(bundleName);
            UnityEngine.Object instantiatedObj = GameObject.Instantiate(loaded);
			GameObject go = instantiatedObj as GameObject;
			T result = go.GetComponent<T>();
			return result;
		}

        public async Task<UnityEngine.Object> LoadByBundleName(MTAssetBundles bundleName)
		{
			string path = bundleName.ToString();
			return await LoadByBundleName(path);
        }

        public async Task<UnityEngine.Object> LoadByBundleName(string bundleName)
		{
            UnityEngine.Object obj = await ResourceLoader.LoadObjectAsync<UnityEngine.Object>(bundleName);
            if(obj == null)
            {
                Debug.LogErrorFormat("Can not load Object by path: {0}", bundleName);
            }
            else
            {
                Debug.LogWarningFormat("Bundle Loaded by path: {0}", bundleName);
            }
            return obj;
        }
	}
}