﻿using MTParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using UnityEngine;

namespace MTCore
{
    public class ResourceLoader : MTLoader
	{
		public bool UpdateResources = false;
		public bool ForceUpdate = false;

		public AssetBundleManifest bundlesManifest;
		public ResourceLoader() : base()
		{
		}
		public async Task<T> LoadObjectAsync<T>(string path) where T : UnityEngine.Object
		{
			var bundle = await LoadAssetBundleFromServerWithCache(path);
			string[] assetNames = bundle.GetAllAssetNames();
			Debug.Log($"Assets in bundle: {string.Join(", ", assetNames)}");
			T asset = bundle.LoadAsset<T>(assetNames[0]); ;
			return asset;
		}


		internal async Task<AssetBundle> LoadAssetBundleFromServerWithCache(string el)
		{
			var loadedBundle = await LoadBundleFromCache(el);
			if (loadedBundle != null)
			{
				return loadedBundle;
			}
			if (!InBuildParameters.InBuildParametersInstance.Offline)
			{
				var isSuccees = await LoadBundleFromServer(el);
				if (isSuccees)
				{
					return await LoadBundleFromCache(el);
				}
			}
			Debug.LogWarning($"Failed to load {el} from server!");
			return null;
		}

		public async Task<bool> LoadBundleFromServer(string assetBundleName)
		{
			using (var wc = new WebClient())
			{
				try
				{
					var url = GetBundleRequestUrl(assetBundleName);
					var data = await wc.DownloadDataTaskAsync(new Uri(url));

					Debug.LogWarning($"Loaded from server - {assetBundleName}");

					var manifestUrl = GetBundleRequestUrl(assetBundleName + ".manifest");
					var manifestData = await wc.DownloadDataTaskAsync(new Uri(manifestUrl));

					Debug.LogWarning($"Manifest loaded from server - {assetBundleName}");

					await SaveBundleToLocalStorage(assetBundleName, data, manifestData);
					return true;
				}
				catch (Exception ex)
				{
					Debug.LogError(ex);
					Debug.LogError(GetBundleRequestUrl(assetBundleName));
					return false;
				}
			}
		}

		private async Task<AssetBundle> LoadBundleFromCache(string el)
		{
			var bundleName = GetBundleCacheName(el);
			var loadedBundles = AssetBundle.GetAllLoadedAssetBundles();
			var loadedBundle = loadedBundles.FirstOrDefault(b => b.name == el.ToLower());

			if (loadedBundle != null)
			{
				//Debug.LogError($"Loaded bundle name - {loadedBundle.name}");
				return loadedBundle;
			}

			var bundlePath = GetBundleCacheFullPath(bundleName);
			var bundle = AssetBundle.LoadFromFile(bundlePath);

			if (bundle != null)
			{
				//Debug.LogError($"Loaded from cache - {bundle.name}");
				return bundle;
			}

			Debug.LogError("Bundle not loaded");
			return null;
		}

		public static async Task SaveBundleToLocalStorage(string bundleName, byte[] bundleBytes, byte[] manifestBytes)
		{
			await CacheBundle(bundleName, bundleBytes);
			await CacheBundle(bundleName + ".manifest", manifestBytes);
			//Debug.LogError($"Saved to cache - {bundleName}");
		}
	}
}