﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace MTCore
{
	class CustomJsonConverter
	{
		public static string ToJson(Dictionary<string, string> dictionary)
		{
			List<StringStringDictionary> dictionaryItemsList = new List<StringStringDictionary>();
			foreach (KeyValuePair<string, string> kvp in dictionary)
			{
				dictionaryItemsList.Add(new StringStringDictionary() { key = kvp.Key, value = kvp.Value });
			}
			StringStringDictionaryArray dictionaryArray = new StringStringDictionaryArray() { items = dictionaryItemsList.ToArray() };
			return JsonUtility.ToJson(dictionaryArray);
		}
		public static Dictionary<string, string> FromJson(string json)
		{
			var loadedData = JsonUtility.FromJson<StringStringDictionaryArray>(json);
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			for (int i = 0; i < loadedData.items.Length; i++)
			{
				dictionary.Add(loadedData.items[i].key, loadedData.items[i].value);
			}
			return dictionary;
		}

		[Serializable]
		private class StringStringDictionaryArray
		{
			public StringStringDictionary[] items;
		}

		[Serializable]
		private class StringStringDictionary
		{
			public string key;
			public string value;
		}
	}
}
