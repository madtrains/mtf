﻿using Trains;
using UnityEngine;
using WaySystem;

namespace MTRails
{
    public enum TrainCollisionType { NoEffect, HitPoint, Deadly, HornLastingTrigger}

    [RequireComponent(typeof(Rigidbody))]
    public class RRCollision : UberBehaviour
    {
        public const float HORN_MULTIPLIER = 6f;
        public UnityEngine.Events.UnityAction OnTrainCollision;
        public UnityEngine.Events.UnityAction OnHornTriggerStay;
        public UnityEngine.Events.UnityAction<Horn> OnHornEntersTrigger;


        public bool On { get { return !disabled; } }

        public bool Off { get { return disabled; } }
        public Color Color { set { gizmoColor = value; } }
        public float Height { get { return height; } }

        

        
        public Rigidbody Rigid { get { return rigid; } }
        public TrainCollisionType CollisionType { get { return trainCollisionType; } }
        public float Multiplier { get { return multiplier; } }

        public Transform PointerTransform { get { return pointerTransform; } }
        public Transform VisualTransform { get { return visualTransform; } }
        

        [SerializeField] protected Rigidbody rigid;
        [SerializeField] protected TrainCollisionType trainCollisionType;
        [SerializeField] protected float multiplier;

        [SerializeField] protected Transform pointerTransform;
        [SerializeField] protected Transform visualTransform;
        [SerializeField] protected float height;
        private bool disabled;
        

        protected Color gizmoColor = Color.green;

        public Point ColliderCenter
        {
            get
            {
                Collider collider = GetComponentInChildren<Collider>();
                if (collider != null)
                {
                    BoxCollider boxCollider = collider as BoxCollider;
                    if (boxCollider != null)
                    {
                        Vector3 position = boxCollider.transform.localToWorldMatrix.MultiplyPoint(boxCollider.center);
                        return new Point(position, transform.rotation);
                    }
                    else
                    {
                        return new Point(transform.position, transform.rotation);
                    }
                }
                else
                {
                    return new Point(transform.position, transform.rotation);
                }
            }
        }

        public float CollisionHeight
        {
            get
            {
                Collider collider = GetComponentInChildren<Collider>();
                if (collider != null)
                {
                    BoxCollider boxCollider = collider as BoxCollider;
                    if (boxCollider != null)
                    {
                        return boxCollider.size.y;
                    }
                }
                return 2f;
            }
        }

        protected virtual void Start()
        {
            Init();
        }

        protected virtual void Init()
        {
            if (rigid == null)
            {
                rigid = gameObject.AddComponent<Rigidbody>();
            }

            rigid.useGravity = false;
            //rigid.constraints = RigidbodyConstraints.FreezeAll;
            Collider[] colliders = gameObject.GetComponentsInChildren<Collider>(false);
            foreach (Collider collider in colliders)
            {
                collider.isTrigger = trainCollisionType == TrainCollisionType.HornLastingTrigger ? true : false;
            }
        }

        public void TrainCollision(Collision collision, float speed)
        {
            switch (CollisionType)
            {
                case (TrainCollisionType.HitPoint):
                {
                    if (GameStarter.Instance.DebugManager.DebugSettings.DebugDraw)
                    {
                        MTDebug.DebugManager.BoxColliderCube(collision.collider as BoxCollider, On ? new Color(1f, 0.2f, 0f, 0.3f) : new Color(1f, 1f, 0f, 0.3f), false);
                    }
                    disabled = true;
                    Push(collision, speed);
                    break;
                }

                case (TrainCollisionType.NoEffect):
                {
                    disabled = true;
                    Push(collision, speed);
                    break;
                }
            }
            OnTrainCollision?.Invoke();
        }

        public void HornTtiggerEnter(Horn horn)
        {
            OnHornEntersTrigger?.Invoke(horn);
        }

        public void HornCollision(Collision collision)
        {
            switch (CollisionType)
            {
                case (TrainCollisionType.HitPoint):
                {
                    disabled = true;
                    GameStarter.Instance.Train.SoundHornHit();
                    Push(collision, HORN_MULTIPLIER);
                    break;
                }

                case (TrainCollisionType.NoEffect):
                {
                    disabled = true;
                    GameStarter.Instance.Train.SoundHornHit();
                    Push(collision, HORN_MULTIPLIER);
                    break;
                }
            }
        }

        public void HorStay()
        {
            OnHornTriggerStay?.Invoke();
        }

        public void Push(Collision collision, float speed)
        {
            float cMult = (speed * GameStarter.GameParameters.FlipperParameters.CollisionImpulseMutiplier) / collision.contacts.Length;
            rigid.useGravity = true;
            rigid.constraints = RigidbodyConstraints.None;
            this.transform.parent = null;

            for (int i = 0; i < collision.contacts.Length; i++)
            {
                rigid.AddForceAtPosition(collision.contacts[i].normal * cMult * -1f, collision.contacts[i].point, ForceMode.Impulse);
                rigid.AddTorque(collision.contacts[i].normal);
                if (GameStarter.Instance.DebugManager.DebugSettings.DebugDraw)
                {
                    MTDebug.DebugManager.Arrow(collision.contacts[i].point, collision.contacts[i].normal, Color.red, cMult * -1f, true);
                }
            }
            //rigid.mass = 1000f;
        }


        protected virtual void OnDrawGizmos()
        {
            foreach (BoxCollider bc in GetComponentsInChildren<BoxCollider>())
            {
                if (!disabled)
                    DrawBoxCollider(bc, gizmoColor.r, gizmoColor.g, gizmoColor.b, 0.30f);
            }
        }


        protected void OnBecameInvisible()
        {
            if (disabled)
                GameObject.Destroy(this.gameObject);
        }
    }
}