using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Environment
{
    public class Lamp : LightingDevice
    {
        [SerializeField] private ContentHolders statesSwitch;

        protected override void Main()
        {
            base.Main();
            int index = UberBehaviour.BoolToInt(_on);
            statesSwitch.Activate(index);
        }
    }
}