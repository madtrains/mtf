using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Environment
{
    public class LightingDevice : UberBehaviour
    {
        public bool On { get { return _on; } set { Set(value); } }
        protected bool _on;

        protected void Set(bool value)
        {
            if (_on == value)
                return;
            _on = value;
            Main();
        }

        protected virtual void Main()
        {

        }
    }
}