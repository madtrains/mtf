﻿using MTCharacters.CharacterFactory;
using MTParameters;
using MTTown.PointsOfInterest;
using System;
using Unity.AI.Navigation;
using UnityEngine;
using WaySystem;
using WaySystem.Cargos;
using static Trains.Train;

namespace MTTown
{
    public class TownForest : TownBase
	{
        [SerializeField] private WayChunkMeta expressStart;
        [SerializeField] private MTCharacters.TownPassengersController passengers;
        [SerializeField] private FakeTrain fakeTrain;
        [SerializeField] private NavMeshSurface navMesh;


        public AlterFritzPost AlterFritzPost { get { return alterFritzPost; } }
        [SerializeField] private AlterFritzPost alterFritzPost;

        public LumberMill LumberMill { get { return lumberMill; } }
        [SerializeField] private LumberMill lumberMill;
        [SerializeField] private UberColorizer[] colorizers;

        [SerializeField] private StayingInTown stayingInTown;
        [SerializeField] private FromFlipper fromFlipper;
        [SerializeField] private WoodUnload woodUnload;
        
        private TownEnter currentEnter;


        public override void Init(
            Town townParameters, 
            PassengersSO passengersSO,
            bool isExitFromFlipper)
        {
            base.Init(townParameters, passengersSO, isExitFromFlipper);
			Func<Tag, bool> checkVisibility = (Tag tag) => GameStarter.Instance.GamePlayManager.CheckProgressForObject<Tag>(tag);
            GameStarter.Instance.UIRoot.Town.TownPointsSwitcher.SetButtonActive(Tag.AlterFritz, checkVisibility(Tag.AlterFritz));
            GameStarter.Instance.UIRoot.Town.TownPointsSwitcher.SetButtonActive(Tag.Coupler, checkVisibility(Tag.Coupler));
            GameStarter.Instance.UIRoot.Town.TownPointsSwitcher.SetButtonActive(Tag.SFDisplay, checkVisibility(Tag.SFDisplay));
            GameStarter.Instance.UIRoot.Town.TownPointsSwitcher.SetButtonActive(Tag.Depot, checkVisibility(Tag.Depot));
            GameStarter.Instance.UIRoot.Town.TownPointsSwitcher.SetButtonActive(Tag.LumberMill, checkVisibility(Tag.LumberMill));
            GameStarter.Instance.UIRoot.Town.TownPointsSwitcher.SetButtonActive(Tag.Express, checkVisibility(Tag.Express));

            wayRoot.Prepare();
            fakeTrain.Place(expressStart.Chunk);
            fakeTrain.PassengersCount = passengers.ExpressPassengersCount;
            GameStarter.Instance.UIRoot.Splash.OnCommandHide = () =>
            {
                Run();
            };

            currentEnter = isExitFromFlipper ? fromFlipper : stayingInTown;

            //тут можно выставить условие для катсцены дерева или ещё чего - то
            if(isExitFromFlipper && GameStarter.Instance.Train.IsWoodLoaded) 
            {
                currentEnter = woodUnload;
            }
            
            
            currentEnter.Prepare(townName, pointsManager);
            foreach(UberColorizer uc in this.colorizers)
            {
                uc.SetRandomColor();
            }
		}

        protected override void SetCharactersAndPassengers()
        {
            base.SetCharactersAndPassengers();
            passengers.Init(characterFactory);
            passengers.LinkFakeTrain(fakeTrain);
        }

        private void Run()
        {
            passengers.Run();
            music.Play();
            PlayAmbient();
            fakeTrain.Launch();
            currentEnter.Run();
            GameStarter.Instance.EventManager.RunEvents();
        }
    }

    [System.Serializable]
    public class StayingInTown : TownEnter
    {
        public override void Prepare(string townName, PointsManager pointsManager)
        {
            base.Prepare(townName, pointsManager);
            this.pointsManager.SwitchImmediateTo(PointsOfInterest.Tag.InTownStart, true);
            //ставим поезд на пути, передаём скорости
            GameStarter.Instance.Train.PrepareForTown(startChunk.WayChunk, startOffset);
        }

        public override void Run()
        {
            base.Run();
            GameStarter.Instance.UIRoot.Town.Show(townName);
            GameStarter.Instance.Train.Stop();
        }
    }

    [System.Serializable]
    public class FromFlipper : TownEnter
    {
        [SerializeField] private float enterSpeed;
        [SerializeField] private float breakingSpeed;
        [SerializeField] private float distance;
        [SerializeField] private float threshold;

        public override void Prepare(string townName, PointsManager pointsManager)
        {
            base.Prepare(townName, pointsManager);
            this.pointsManager.SwitchImmediateTo(Tag.ShortArrival, true);

            GameStarter.Instance.Train.PrepareForTown(startChunk.WayChunk, startOffset, enterSpeed, breakingSpeed, distance, threshold,
                null, null, null);
        }

        public override void Run()
        {
            base.Run();
            GameStarter.Instance.UIRoot.Town.Show(this.townName);
            GameStarter.Instance.Train.StartEngine();
            GameStarter.Instance.Train.Go(EngineSpeedState.SpeedControlBySaved, false);
            GameStarter.Instance.SplitFlapDisplay.SwtichDisplayToTripsMode(false);
        }
    }

    [System.Serializable]
    public class WoodUnload : TownEnter
    {
        [SerializeField] private CamDock start;
        [SerializeField] private CamDock lumberMill;
        [SerializeField] private CamDock end;
        
        [SerializeField] private float enterSpeed;
        [SerializeField] private float breakingSpeed;
        [SerializeField] private float distance;
        [SerializeField] private float threshold;
        [SerializeField] private CargoBunch cargoBunch;
        [SerializeField] [Range(0f, 1f)]private float camSubT;

        private int fullSpeedArriveDistance = 65;
        private int breakDistance = 25;
        private int breakDistance2 = 30;
        private float speed2 = 8;

        public override void Prepare(string townName, PointsManager pointsManager)
        {
            base.Prepare(townName, pointsManager);
            cargoBunch.gameObject.SetActive(true);
            cargoBunch.Prepare();
            start.SetCamera();
            GameStarter.Instance.Train.TownEnter(startChunk.WayChunk, startOffset, enterSpeed, fullSpeedArriveDistance,
            (t) =>
            {
                //float camT = Mathf.InverseLerp(0, camSubT, t);
                float camT = GameStarter.Instance.SmoothAnimCurve.Evaluate(t);
                GameStarter.Instance.MainComponents.GameCamera.Lerp(start, lumberMill, camT);
            },
            () =>
            { 
                Step2(); 
            });
        }

        public override void Run()
        {
            base.Run();
            GameStarter.Instance.Train.StartEngine();
            GameStarter.Instance.Train.Go(EngineSpeedState.SpeedControlBySaved, false);
        }

        private void Step2()
        {
            GameStarter.Instance.Train.TownUnload(Trains.LOADS_ENUM.WOOD, this.cargoBunch, () => { UnloadDone(); });
            GameStarter.Instance.Train.TownMove(breakDistance, null, () => { Step3(); }, speed2);
        }

        private void Step3()
        {
            //var c = pointsManager.GetPoint(Tag.InTownStart).
            GameStarter.Instance.Train.TownBreak(breakDistance2, 1, 0.8f, (distance) =>
            {
                float camT = GameStarter.Instance.SmoothAnimCurve.Evaluate(distance);
                GameStarter.Instance.MainComponents.GameCamera.Lerp(lumberMill, end, camT);
            },
            () =>
            {
                End();
            },
            null);
        }

        private void End()
        {
            GameStarter.Instance.UIRoot.Town.Show(this.townName);
        }

        private void UnloadDone()
        {

        }
    }

    [System.Serializable]
    public class WayChunkMeta
    {
        public WayChunk Chunk { get { return chunkBehaviour.WayChunk; } }
        [SerializeField] protected WayChunkBehaviour chunkBehaviour;
    }
}