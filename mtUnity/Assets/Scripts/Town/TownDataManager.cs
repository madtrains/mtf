﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using MTPlayer;
using MTUi.Town;
using UnityEngine;
using static MTTown.TripsDispatcher;

namespace MTTown
{
	public class TownDataManager
	{
		public Action OnTownEnter;
		private List<InTownStructure> _structures { get; set; }
		public TripsDispatcher WaysDispatcher { get; private set; }
		private TownDataManager()
		{
			WaysDispatcher = TripsDispatcher.Init();
			OnTownEnter += () =>
			{
				//достаем структуры...перенести в менеджер города
				_structures = new List<InTownStructure>
			{
				new InTownStructure
				{
					Id = 1,
					Name = "Railway station",
					Need = new int[]{1},
					Has = new int[]{0},
				},
				new InTownStructure
				{
					Id = 2,
					Name = "Lamber mill",
					Need = new int[]{5},
					Has = new int[]{0}
				}
			};
				WaysDispatcher.OnTownEnter?.Invoke();
			};
		}
		public static TownDataManager Init()
		{
			return new TownDataManager();
		}
		public List<InTownStructure> GetInTownStructures()
		{
			return _structures;
		}
		public void GetData()
		{
			var bindingFlags = System.Reflection.BindingFlags.Public;
			List<FieldInfo> myFields = this.GetType().GetFields().Where(value => value.GetValue(this).ToString() != null).ToList();

			//PlayerManager.Do(() =>
			//{
			//	for (int i = 0; i < myFields.Count; i++)
			//	{
			//		var value = PlayerManager.Data.GetString($"{Name}_{myFields[i].Name}");
			//		if (value != null)
			//		{
			//			myFields[i].SetValue(this, value);
			//			Debug.LogError($"{Name}_{myFields[i].Name} - {value}");
			//		}
			//	}
			//}, false);
		}
		public void SetData()
		{
			var bindingFlags = System.Reflection.BindingFlags.Public;
			List<FieldInfo> myFields = this.GetType().GetFields().Where(value => value.GetValue(this).ToString() != null).ToList();

			//PlayerManager.Do(() =>
			//{
			//	for (int i = 0; i < myFields.Count; i++)
			//	{
			//		PlayerManager.Data.SetString($"{Name}_{myFields[i].Name}", myFields[i].GetValue(this).ToString());
			//		Debug.LogError($"{Name}_{myFields[i].Name} - {myFields[i].GetValue(this).ToString()}");
			//	}
			//});
		}
	}
	public class InTownStructure
	{
		public int Id;
		public string Name;
		public int[] Need;
		public int[] Has;
		public bool Active;
	}
}
