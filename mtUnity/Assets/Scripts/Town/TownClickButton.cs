﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTTown
{
    public class TownClickButton : MTCore.RayCastTap
    {
        public static readonly int hOn = Animator.StringToHash("on");

        public bool waitForRelease;
        public UnityEngine.Events.UnityAction OnReleased;

        protected bool clicked;
        [SerializeField] private Animator animator;


        public override void Tap()
        {
            if (waitForRelease && clicked)
                return;
            animator.SetTrigger(hOn);
            clicked = true;
            base.Tap();
        }


        public void OnEventReleased()
        {
            clicked = false;
            if (OnReleased != null)
                OnReleased();
        }
    }
}