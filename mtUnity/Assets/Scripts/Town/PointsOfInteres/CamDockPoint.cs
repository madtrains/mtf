using UnityEngine;
using UnityEngine.Events;

namespace MTTown.PointsOfInterest
{
    /// <summary>
    /// ����� � �����  CamDock
    /// </summary>
    [System.Serializable]
    public class CamDockPoint : Point
    {
        public CamDock CamDock { get { return GetCamDock(); }  set { SetCamDock(value); } }
        [SerializeField] protected CamDock camDock;

        public override void Open()
        {
            base.Open();
            CamDock.MoveCameraToMe(
                //onEnd
                () =>
                {
                    Opened();

                },
                //process, do nothing
                (float time) =>
                {
                    OnSwitching?.Invoke(time);
                });
        }

        public override void OpenImmediately()
        {
            base.OpenImmediately();
            CamDock.SetCamera();
        }

        protected virtual CamDock GetCamDock()
        {
            return camDock;
        }

        protected virtual void SetCamDock(CamDock camDock) 
        {
            this.camDock = camDock;
        }

        /*
        */
    }
}