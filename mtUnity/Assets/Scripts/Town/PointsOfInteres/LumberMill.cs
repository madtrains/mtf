using MTTown.Characters;
using UnityEngine;

namespace MTTown.PointsOfInterest
{
    /// <summary>
    /// ����� � �����  CamDock
    /// </summary>
    [System.Serializable]
    public class LumberMill : CamDockPoint
    {
        public GrosserFrantz GrosserFrantz { get { return grosserFrantzCharacter; } }
        [SerializeField] private GrosserFrantz grosserFrantzCharacter;

        protected override void Opened()
        {
            base.Opened();
            grosserFrantzCharacter.ShowUpdateBubble();
        }

        public override void Close()
        {
            base.Close();
            grosserFrantzCharacter.HideBubble();
        }
    }
}