using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MTTown.PointsOfInterest
{
    [System.Serializable]
    public class PointsManager
    {
        public UnityAction<Tag> OnPointCalled;
        public UnityAction<Tag> OnPointClosed;
        public UnityAction<Tag> OnPointOpened;
        public UnityAction<float> OnSwitching;

        public Tag Point { get { return currentPoint; } } 

        public Point GetPoint(Tag tag)
        {
            return pointsDct[tag];
        }

        [SerializeField] private MTTown.PointsOfInterest.Point[] points;
        private Dictionary<Tag, Point> pointsDct;
        private Tag currentPoint;
        private bool isSwitching;

       
        public void Init()
        {
            pointsDct = new Dictionary<Tag, Point>();
            foreach (MTTown.PointsOfInterest.Point point in points)
            {
                pointsDct[point.Tag] = point;
                point.OnOpen += (point) => 
                {
					GameStarter.Instance.EventManager.RunEvents();
					OnPointOpened?.Invoke(point); 
                    currentPoint = point;
                    isSwitching = false;
                    GameStarter.Instance.UIRoot.Town.TownPointsSwitcher.SwitchEnd(point);
                    
                };
                point.OnClose += (point) => { OnPointClosed?.Invoke(point); };
                point.OnSwitching += (t) => 
                {
					OnSwitching?.Invoke(t);
                    GameStarter.Instance.UIRoot.Town.TownPointsSwitcher.Switch(
                        currentPoint, point.Tag, t);
                };
            }
        }

        public void SwitchTo(Tag point)
        {
            if (!CheckSwitchTo(point))
                return;
            GameStarter.Instance.MainComponents.SoundManager.Shared.Play(MTSound.SharedNames.ButtonSoft);
            isSwitching = true;
            GameStarter.Instance.UIRoot.Town.TownPointsSwitcher.SwitchStart(point);
            OnPointCalled?.Invoke(point);
            pointsDct[currentPoint].Close();
            pointsDct[point].Open();
            SwitchPointUpdateDataCollections(point);
		}
        public void SwitchPointUpdateDataCollections(Tag point)
        {
			GameStarter.Instance.GamePlayManager.UpdateCollection(MTCore.MTDataCollection.Switch_town_points_number, 1);
			GameStarter.Instance.GamePlayManager.UpdateCollection((int)MTCore.MTDataCollection.Visit_town_point_overview + (int)point, 1);
            GameStarter.Instance.GamePlayManager.RunCheckers();
		}
		public void SwitchImmediateTo(Tag point, bool noCheck=false) 
        {
            if (!CheckSwitchTo(point) && !noCheck)
                return;
            OnPointCalled?.Invoke(point);
            pointsDct[currentPoint].Close();
            pointsDct[point].OpenImmediately();
			//camDocks[point].SetCamera();
			SwitchPointUpdateDataCollections(point);
		}

		private bool CheckSwitchTo(Tag point) 
        {
            if (point == currentPoint)
                return false;

            if (isSwitching)
                return false;

            if (!pointsDct.ContainsKey(point))
            {
                Debug.LogErrorFormat("Point {0} was not prepared yet. Check your town prefab for this point", point);
                return false;
            }
            return true;
        }
    }
}