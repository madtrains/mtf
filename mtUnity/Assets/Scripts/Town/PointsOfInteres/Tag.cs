namespace MTTown.PointsOfInterest
{
    public enum Tag
    {
        //Shared
        Overview,
        ShortArrival,
        Center,
        Coupler,
        Depot,
        Express,
        SFDisplay,
        InTownStart,
        //Forest
        LumberMill,
        AlterFritz
    }
}