using MTTown.Characters;
using UnityEngine;

namespace MTTown.PointsOfInterest
{
    /// <summary>
    /// ����� � �����  CamDock
    /// </summary>
    [System.Serializable]
    public class AlterFritzPost : CamDockPoint
    {
        public AlterFritz AlterFritz { get { return alterFritzCharacter; } }
        [SerializeField] private AlterFritz alterFritzCharacter;
        protected override void Opened()
        {
            base.Opened();
            //GameStarter.Instance.UIRoot.Town.Bubble.Character = this.grosserFrantz;
            //GameStarter.Instance.UIRoot.Town.BubbleFrantz.UpdateBubble();


            alterFritzCharacter.ShowUpdateBubble();
        }

        public override void Close()
        {
            base.Close();
            alterFritzCharacter.HideBubble();
			alterFritzCharacter.ShowUpdateBubble();
		}
	}
}