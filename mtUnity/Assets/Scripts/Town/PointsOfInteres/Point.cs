using UnityEngine;
using UnityEngine.Events;

namespace MTTown.PointsOfInterest
{
    /// <summary>
    /// ������� ����� ������������� �� ������ ����� � ����������� CamDock  
    /// ��� ������� ������������� ����� ���� �������
    /// </summary>
    [System.Serializable]
    public class Point : UberBehaviour
    {
        public UnityAction<Tag> OnOpen;
        public UnityAction<Tag> OnClose;
        public UnityAction<float> OnSwitching;
        protected bool opened;

        public Tag Tag { get { return myTag; } }
        [SerializeField] protected Tag myTag;

        /// <summary>
        /// ������ �� ������������ �������� ����� � ��������
        /// </summary>
        public virtual void OpenImmediately()
        {
            Opened();
        }

        /// <summary>
        /// ������ �� �������� �����
        /// </summary>
        public virtual void Open()
        {

        }

        /// <summary>
        /// ������ �� �������� � ��������
        /// </summary>
        public virtual void Close() 
        {
            opened = false;
            OnClose?.Invoke(myTag);
        }

        /// <summary>
        /// �� �������� �����
        /// </summary>
        protected virtual void Opened()
        {
            opened = true;
            OnOpen?.Invoke(myTag);
        }
    }
}