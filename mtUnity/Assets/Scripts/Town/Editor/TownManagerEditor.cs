﻿using UnityEngine;
using UnityEditor;

namespace MTTown
{
    [CustomEditor(typeof(TownBase))]
    public class TownManagerEditor : Editor
    {
        protected virtual void OnSceneGUI()
        {
            TownBase tm = target as TownBase;

            //DrawPoint(tm.In, Color.yellow);
            //DrawPoint(tm.Depot, Color.yellow);
            //DrawPoint(tm.Out, Color.yellow);
        }


        private void DrawPoint(Vector3 pos, Color color)
        {
            Handles.color = color;
            Handles.SphereHandleCap(0, pos, Quaternion.identity, 1f, EventType.Repaint);
        }
    }
}