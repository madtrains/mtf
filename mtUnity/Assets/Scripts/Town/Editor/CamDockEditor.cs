﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace MTTown
{
    [CustomEditor(typeof(CamDock))]
    public class CamDockEditor : UberEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            //EditorGUILayout.BeginHorizontal();
            MTParameters.GameParameters gp = MTParameters.GameParametersViewer.GetParameters();
            if (gp == null)
            {

                EditorGUILayout.HelpBox("No Game Parameters Found", MessageType.Error);
                return;
            }

            //EditorGUILayout.EndHorizontal();

            CamDock camDock = target as CamDock;


            if (GUILayout.Button("Set Camera"))
            {
                MTLightNCamera.GameCamera gc = FindObjectOfType<MTLightNCamera.GameCamera>();
                if (gc == null)
                {
                    Debug.LogErrorFormat("Can not Find object of type {0}", "MTLightNCamera.GameCamera");
                    return;
                }

                gc.transform.position = camDock.transform.position;
                gc.CheckAndApplyParams(camDock.CameraParams);
            }
        }
    }
}