﻿using MTCharacters.CharacterFactory;
using MTCore;
using MTTown.PointsOfInterest;
using MTUi;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using WaySystem;

namespace MTTown
{
    public enum TrainArrivalMode { ShortArrival, InTown, LongArrival, WoodCutScene }

	public abstract class TownBase : UberBehaviour
	{
		public UnityAction OnInited;

		public Tag CurrentPoint { get { return pointsManager.Point; } }

		[SerializeField] protected TrainMaintenance.Depo depo;
		[SerializeField] protected TrainMaintenance.Coupler coupler;

		//public PointsManager PointsManager { get { return pointsManager; } }
		[SerializeField] protected PointsManager pointsManager;
		[SerializeField] private AudioSource[] ambientSounds;

		public SFDPoint SFDPoint { get { return sFDPoint; } }
		[SerializeField] private SFDPoint sFDPoint;

		[SerializeField] protected TownBuilding depot;
		[SerializeField] protected TownBuilding resourceBuilding;

		[SerializeField] protected Transform splitFlapDisplayRoot;
		[SerializeField] protected WayRoot wayRoot;
		[SerializeField] protected AudioSource music;
		[SerializeField] private TownLight townLight;


		protected int _townId;
		protected string townName;
		protected bool townLoadedAsFLipperExit;
		protected bool townReady;
		protected CharacterFactory characterFactory;
		protected bool isInFlyMode;
		public MTParameters.Town TownParameters { get {return townParameters; } }
        protected MTParameters.Town townParameters;

        #region Set/Init
        /// <summary>
        /// подготовка города к открытию
        /// </summary>
        public virtual void Init(
			MTParameters.Town townParameters,
			PassengersSO passengersSO,
			bool isExitFromFlipper)
		{
            this.townParameters = townParameters;
            _townId = townParameters.ID;
			GameStarter.Instance.MainComponents.SoundManager.Music = music;
			this.characterFactory = new CharacterFactory(passengersSO);

			townName = GameStarter.Instance.TranslationManager.GetTableString(townParameters.TranslationKey);
			this.townLoadedAsFLipperExit = isExitFromFlipper;
			townReady = false;
			this.pointsManager.Init();
			this.pointsManager.OnPointOpened += (newPoint) =>
			{
				GameStarter.Instance.SplitFlapDisplay.ClickBLockManager.SFDPoint =
					newPoint == PointsOfInterest.Tag.SFDisplay || newPoint == PointsOfInterest.Tag.ShortArrival;
			};

            depo.Init(townParameters.DepotIDs);
			coupler.Init();

			//SFD
			GameStarter.Instance.SplitFlapDisplay.Dock(
				splitFlapDisplayRoot,
				GameStarter.GameParameters.SFDParameters.TownAssemble);
			GameStarter.Instance.SplitFlapDisplay.OnTripSelected = (trip, tripIndex) =>
			{
                GameStarter.Instance.TownDataManager.WaysDispatcher.SetCurrentWay(trip);
                GameStarter.Instance.UIRoot.Town.SetTrainDisplay(trip.Resourses);
				Debug.LogWarningFormat("Current trip resources: {0} {1}", trip.Resourses[0], trip.Size);
            };

			SetCharactersAndPassengers();
			//main menu
			GameStarter.Instance.UIRoot.Town.OnPlayButton = async () => 
			{
				await OnPlayButton(); 
			};
			GameStarter.Instance.UIRoot.Town.TownPointsSwitcher.OnSwitchButton = (point) =>
			{
				this.pointsManager.SwitchTo(point);
			};

			GameStarter.Instance.PlayerManager.OnChangeResources?.Invoke();
			GameStarter.Instance.UIRoot.Splash.SetLoaded(true);

			(this.pointsManager.GetPoint(Tag.SFDisplay) as CamDockPoint).CamDock = GameStarter.Instance.SplitFlapDisplay.CamDock;
            GameStarter.Instance.SplitFlapDisplay.SwtichDisplayToTripsMode(true);
            GameStarter.Instance.MainComponents.GameLight.Set(townLight);
			foreach(AudioSource audioSource in ambientSounds)
			{
				audioSource.outputAudioMixerGroup = GameStarter.Instance.MainComponents.SoundManager.AssignMixer(MTSound.SoundType.Ambient);
			}
			GameStarter.Instance.GamePlayManager.RunCheckers();
        }

		public void PlayAmbient()
		{
            foreach (AudioSource audioSource in ambientSounds)
            {
				audioSource.Play();
            }
        }

        public void SFD2Results(List<SFD.ResultLineValues> results)
        {
            GameStarter.Instance.SplitFlapDisplay.ShowFlipperResultsInTown(
                results, () =>
                {
                    GameStarter.Instance.SplitFlapDisplay.SwtichDisplayToTripsMode(false);
                });
        }

        public virtual void Close()
		{
			GameStarter.Instance.UIRoot.Town.Reset();
            GameStarter.Instance.MainComponents.Helpers.Arrow.Close();
		}

        protected virtual void SetCharactersAndPassengers()
		{

		}
		

        #endregion
        public int GetTownId()
        {
			return _townId;
		}

		public virtual void UpdateInteractiveCharacters()
		{
			
		}


		protected async Task OnPlayButton()
		{
            MTCore.Trip currentTrip = GameStarter.Instance.TownDataManager.WaysDispatcher.GetCurrentTrip();
			GameStarter.Instance.UIRoot.Town.Hide();
            await GameStarter.Instance.FlipperManager.LoadFlipperByTrip(currentTrip);
			Close();
		}
	}

	[System.Serializable]
    public class TownLight
    {
        public Color Color { get { return color; }}
		public Color Ambient { get { return ambient; } }
		public Color Shadows { get { return shadows; } }
        public float ShadowsStrength { get { return shadowsStrength; } }
        public float Intensity { get { return intensity; }}
        public float Angle { get { return angle; }}
        public float AngleUp { get { return angleUp; }}

        [SerializeField] private Color color;
		[SerializeField] private Color ambient;
		[SerializeField] private Color shadows;
        [SerializeField] private float shadowsStrength;
        [SerializeField] private float intensity;
        [SerializeField] protected float angle;
        [SerializeField] protected float angleUp;
	}
}
