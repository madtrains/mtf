using MTCharacters.CharacterFactory;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace MTTown
{
    public class TownManager
    {
        public MTParameters.Town TownParameters { get { return _townParameters; } }
        public TownBase Town;

        private MTParameters.Town _townParameters;
		public PassengersSO passengersSO;
		public async static Task<TownManager> Init()
        {
            TownManager tm = new TownManager();
            await tm.LoadPassengersSO();
			return tm;
        }

        public TownManager(){}

        public async Task<TownBase> LoadTown(
            MTParameters.Town townParameters,
            bool isExitFromFlipper)
        {
            _townParameters = townParameters;

            //������ ���������� �� ������ ��� ������
            await GameStarter.Instance.TrainManager.Pull.LoadAllTownLocosNCoaches(townParameters);

            //���� ������ �� ����, ������. ��������� ��� ������ �������� ������
            if (!GameStarter.Instance.TrainManager.WasCreated)
            {
                await GameStarter.Instance.TrainManager.CreateTrainFromDraft();
            }

            //������ ���� ���������� ��� ������������
            int[] myTrains = GameStarter.Instance.PlayerManager.GetTrains();
            foreach (int i in myTrains)
            {
                await GameStarter.Instance.TrainManager.Pull.GetLoadLocomotive(i);
            }

            //������ ���� ������ ��� ������������
            List<MTPlayer.CoachDepoInfo> myCoaches = GameStarter.Instance.PlayerManager.GetCoachesList();
            foreach (MTPlayer.CoachDepoInfo coachDepoInfo in myCoaches)
            {
                await GameStarter.Instance.TrainManager.Pull.GetLoadCoach(coachDepoInfo.Id);
            }

            //������ ������ ������
            Town = await GetTown(townParameters.MainBundle);


			GameStarter.Instance.PlayerManager.SetCurrentLevel(townParameters.ID);
            GameStarter.Instance.TownDataManager.OnTownEnter();

            //GameStarter.Instance.SplitFlapDisplay.Assemble(GameStarter.GameParameters.SFDParameters.TownAssemble);
            GameStarter.Instance.SplitFlapDisplay.SetCameraDock(GameStarter.GameParameters.SFDParameters.TownSFDCamera);
            //���������� ������ � ������
            Town.Init(townParameters, passengersSO, isExitFromFlipper);
            return Town;
        }

        //������ ������ ������
        private async Task<MTTown.TownBase> GetTown(string bundleName)
        {
            UnityEngine.Object env = await GameStarter.Instance.ResourceManager.LoadByBundleName(bundleName);
			var obj = GameObject.Instantiate(env);
			GameObject go = obj as GameObject;
			var town = go.GetComponent<MTTown.TownBase>();
            return town;
        }

        private async Task LoadPassengersSO()
        {
            UnityEngine.Object unityEngineObj =
                await GameStarter.Instance.ResourceManager.LoadByBundleName(MTAssetBundles.holders_passengers);
            if (unityEngineObj == null)
            {
                Debug.LogErrorFormat("Passengers SO is not loaded, id:{0}", MTAssetBundles.holders_passengers);
                return;
            }

			passengersSO = unityEngineObj as PassengersSO;
        }
    }
}