﻿using MTCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using static MTTown.TripsDispatcher;
using UnityEngine;


namespace MTTown
{
    public partial class TripsDispatcher
	{
		public Action OnTownEnter;
		private Trip _currentTrip { get; set; }
		private List<Trip> _trips { get; set; }
		private MTParameters.Town _town { get; set; }
		private TripsDispatcher()
		{
			OnTownEnter += () =>
			{
				_town = GameStarter.Instance.TownManager.TownParameters;
				BuildTaskTrips();
			};
		}
		public static TripsDispatcher Init()
		{
			return new TripsDispatcher();
		}

		public void SetCurrentWay(Trip way)
		{
			_currentTrip = way;
		}

		public Trip GetCurrentTrip()
		{
			return _currentTrip;
		}

		public List<Trip> GetTrips()
		{
			return _trips;
		}

		public void BuildTaskTrips()
		{
			var result = new List<Trip>();

			//убираем повторы по ресурсам
			
   //         foreach (var task in GameStarter.Instance.PlayerManager.GetUserIngameTasks())
			//{
			//	result.Add(BuildTrip(task));
			//}
			//var tasks = GameStarter.Instance.GamePlayManager.GetTasksToStart();
			//foreach (var task in tasks)
			//{
			//	Debug.LogError($"Task - {task.Title} - {task.Description}");
			//}

			//Debug.LogError($"Ways - {result.Count}");
			_trips = result;
		}

		public List<Trip> BuildTripsForTown(int count)
		{
            //генерим маршруты по задачам
            List<int> tasksUniqueResources =
				GameStarter.Instance.PlayerManager.GetUserIngameTasks().Select(x => x.ResourseType).Distinct().ToList();

			//костыль убирающий почту
			if (tasksUniqueResources.Contains(2))
				tasksUniqueResources.Remove(2);

            List<Trip> result = new List<Trip>();
			for (int i = 0; i < Math.Min(tasksUniqueResources.Count, count); i++)
			{
				int resource = tasksUniqueResources[i];
				result.Add(BuildTrip(resource, TripType.Local, _town.ID));
			}

            //если маршрутов меньше чем заказано для города, генерим случайные
            if (result.Count < count)
			{
				for (int i = result.Count; i < count; i++)
				{
					result.Add(BuildRandomTrip());
				}
			}

            SetCurrentWay(result[0]);
            return result;
        }

		public Trip BuildRandomTrip()
		{
			//var tasks = GameStarter.Instance.PlayerManager.GetUserIngameTasks();
			//var randomTask = tasks.Count>0 ? tasks[UnityEngine.Random.Range(0, tasks.Count)] : GameStarter.Instance.TownDataManager.WaysDispatcher.GetDefaultTask();
			var randomTask =  GameStarter.Instance.TownDataManager.WaysDispatcher.GetDefaultTask();
			return BuildTrip(randomTask);
		}
		public Trip BuildTripToNeighbor()
		{
			//var tasks = GameStarter.Instance.PlayerManager.GetUserIngameTasks();
			//var randomTask = tasks.Count>0 ? tasks[UnityEngine.Random.Range(0, tasks.Count)] : GameStarter.Instance.TownDataManager.WaysDispatcher.GetDefaultTask();
			var neighborTask = GameStarter.Instance.TownDataManager.WaysDispatcher.GetNeighborsTask();
			return BuildTrip(neighborTask);
		}

		public Trip BuildTrip(InGameTask task)
		{
			return BuildTrip(task.ResourseType, task.Type, task.Town);
		}

        public Trip BuildTrip(int mainResource, TripType tripType, int to)
		{
            var from = _town.ID;
            int size = UnityEngine.Random.Range(0, 3);
            List<int> resources = new List<int>() { mainResource };
            if (GameStarter.GameParameters.TownTrips.SecondResource)
            {
                while (resources.Count < 2)
                {
                    int random2 =
                        GameStarter.GameParameters.TownTrips.RandomResource;
                    if (!resources.Contains(random2))
                        resources.Add(random2);
                }
            }

            if (GameStarter.GameParameters.TownTrips.ThirdResource)
            {
                while (resources.Count < 3)
                {
                    int random3 =
                        GameStarter.GameParameters.TownTrips.RandomResource;
                    if (!resources.Contains(random3))
                        resources.Add(random3);
                }
            }

            var result = new Trip(tripType, (TripSize)size, resources, from, to);
            return result;
        }
        /*
		public void FakeRide()
		{
			if (_currentTrip == null)
			{
				Debug.Log($"Choose way!");
				return;
			}

			var majResourceAmount = 0;
			var minResourceAmount = 0;

			switch (_currentTrip.Size)
			{
				case TripSize.S:
					majResourceAmount = 200;
					minResourceAmount = 50;
					break;
				case TripSize.M:
					majResourceAmount = 400;
					minResourceAmount = 100;
					break;
				case TripSize.L:
					majResourceAmount = 600;
					minResourceAmount = 200;
					break;
				default:
					majResourceAmount = 1;
					minResourceAmount = 1;
					break;
			}

			GameStarter.Instance.PlayerManager.ResourceIncreaseAmount(_currentTrip.Resourses[0].Item1, majResourceAmount);
			GameStarter.Instance.PlayerManager.ResourceIncreaseAmount(_currentTrip.Resourses[1].Item1, minResourceAmount);

			var text = "You cant get another task";

			if (_currentTrip.Resourses[0] == _currentTrip.Resourses[1])
			{
				text = $"Earn: {_currentTrip.Resourses[0]} - {majResourceAmount + minResourceAmount}";
			}
			else
			{
				text = $"Earn: {_currentTrip.Resourses[0]} - {majResourceAmount}, {_currentTrip.Resourses[1]} - {minResourceAmount}";
			}

			GameStarter.Instance.UIRoot.Dialog.Show(text);

			BuildTrips();

			_currentTrip = null;
		}
		*/

        public InGameTask GetDefaultTask()
		{
			var townStructures = _town.Buildings;
			var building = GameStarter.GameParameters.TownBuildings.FirstOrDefault(b => townStructures.Any(s => s == b.ID) && b.Resources.Any(r => r != -1));
			return GetDefaultTask(_town.ID, 0);
		}

		public InGameTask GetNeighborsTask()
		{
			var neighbours = _town.Neighbours;
			var index = UnityEngine.Random.Range(0, _town.Neighbours.Length);
			return GetDefaultTask(neighbours[index], 0);
		}

		public static InGameTask MakeTask(int town, int owner, TripType type, int amount, int resource, int prize, int prizeType = 0)
		{
			var task = new InGameTask();

			task.Town = town;
			task.Owner = owner;
			task.Type = type;
			task.ResourseAmount = amount;
			task.ResourseType = resource;
			task.PrizeAmount = prize;
			task.PrizeType = prizeType;

			return task;
		}

		public static InGameTask MakeTask(int town, int owner, int amount, int resource, int prize, int prizeType = 0)
		{
			return MakeTask(town, owner, TripType.Local, amount, resource, prize, prizeType);
		}

		public static InGameTask GetRandomTask()
		{
			var town = UnityEngine.Random.Range(0, GameStarter.GameParameters.Towns.Length);
			var owner = 0;
			var wayType = UnityEngine.Random.Range(0, TripType.GetValues(typeof(TripType)).Cast<int>().Max());
			var amount = UnityEngine.Random.Range(5, 10) * 100;
			var resourse = UnityEngine.Random.Range(0, GameStarter.GameParameters.Resources.Length);
			var prize = amount * 3;

			var task = TripsDispatcher.MakeTask(town, owner, (TripType)wayType, amount, resourse, prize);
			return task;
		}

		public static InGameTask GetTaskFor(int town, int resourse, int amount = 50, int multilier = 4, int prize = 0, GameCharacter character = GameCharacter.Fritz)
		{
			var wayType = TripType.Local;
			prize = prize == 0 ? amount * multilier : prize ;
			var task = TripsDispatcher.MakeTask(town, (int)character, wayType, amount, resourse, prize);
			return task;
		}

		public static InGameTask GetDefaultTask(int town, int owner)
		{
			var wayType = TripType.Local;
			var amount = 100;
			var resourse = GameStarter.GameParameters.TownTrips.RandomResource;
			var prize = amount * 3;
			var task = TripsDispatcher.MakeTask(town, owner, (TripType)wayType, amount, resourse, prize);
			return task;
		}

		public static InGameTask GetTaskFor(int town, InTownStructure structure)
		{
			var wayType = UnityEngine.Random.Range(0, TripType.GetValues(typeof(TripType)).Cast<int>().Max());
			var amount = UnityEngine.Random.Range(5, 10) * 100;
			var resource = structure.Need[UnityEngine.Random.Range(0, structure.Need.Length)];
			var prize = amount * 3;
			var task = TripsDispatcher.MakeTask(town, structure.Id, (TripType)wayType, amount, resource, prize);
			return task;
		}

		public static InGameTask GetTaskFor(int town, MTParameters.TownBuilding structure)
		{
			var wayType = UnityEngine.Random.Range(0, TripType.GetValues(typeof(TripType)).Cast<int>().Max());
			var amount = 100;
			var resource = structure.Resources[0];
			var prize = amount * 5;
			var task = TripsDispatcher.MakeTask(town, structure.ID, (TripType)wayType, amount, resource, prize);
			return task;
		}

		public static int GetRandomResource(params int[] resourses)
		{
			var index = UnityEngine.Random.Range(0, resourses.Length);
			return resourses[index];
		}

		public static int GetDefaultResource()
		{
			return 1;
		}

		public class InGameTask
		{
			public string Id;
			public int Town;
			public int Owner;
			public TripType Type;
			public int ResourseType;
			public int ResourseAmount;
			public bool Active;
			public int PrizeAmount;
			public int PrizeType;

			public InGameTask()
			{
				Id = Guid.NewGuid().ToString();
				Active = true;
			}
			public InGameTask(string id)
			{
				Id = id;
				Active = true;
			}
		}
	}

}
