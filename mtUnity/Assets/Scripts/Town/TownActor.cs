﻿using UnityEngine;

namespace MTTown
{
    public class TownActor : MTCore.RayCastTap
    {
        public CamDock CameraDock { get { return cameraDock; } }

        [SerializeField] private CamDock cameraDock;
    }
}