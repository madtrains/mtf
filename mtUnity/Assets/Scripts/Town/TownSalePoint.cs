using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTTown
{
	public class TownSalePoint : TownActor
	{
		public bool SaleEnabled { get { return saleEnabled; } }
		[SerializeField] protected bool saleEnabled = false;
		[SerializeField] protected GameObject coin;
		[SerializeField] protected Trains.Visual visual;
		private Func<bool> _dynamicCondition;

		public virtual void Set(Trains.Visual visual, bool saleEnabled, Func<bool> dynamicCondition)
		{
			if (visual != null)
				Destroy(visual);

			this.visual = visual;
			visual.transform.SetParent(this.transform, false);
			visual.transform.localPosition = Vector3.zero;
			Activate(coin, saleEnabled && dynamicCondition());
			this.saleEnabled = saleEnabled;
			_dynamicCondition = dynamicCondition;

			if (saleEnabled)
				StartCoroutine(CheckForSale());
		}
		public virtual void Withdraw()
		{
			Activate(coin, false);
			this.saleEnabled = false;
		}
		public IEnumerator CheckForSale()
		{
			while (saleEnabled)
			{
				if (_dynamicCondition())
				{
					Activate(coin, true);
					this.saleEnabled = true;
				}
				yield return new WaitForSeconds(5f);
			}
		}
	}
}