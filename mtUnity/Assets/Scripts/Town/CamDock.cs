﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTTown
{
    public class CamDock : UberBehaviour
    {
        [SerializeField] private MTParameters.CameraParameters cameraParams;
        
        public MTParameters.CameraParameters CameraParams 
        { 
            get 
            {
                return cameraParams;
            }

            set
            {
                cameraParams = value;
            }
        }


        //private EndDelegate endDelegate;

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, 0.4f);
        }


        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, 0.4f);
        }


        public void SetCamera()
        {
            GameStarter.Instance.MainComponents.GameCamera.Set(transform.position,
                CameraParams);
        }


        public void MoveCameraToMe(EndDelegate endDelegate, ProcessDelegate processDelegate)
        {
            MoveCameraToMe(
                endDelegate,
                processDelegate,
                GameStarter.GameParameters.TownParameters.CameraFlySpeed,
                GameStarter.GameParameters.TownParameters.CameraAngleSpeed);
        }

        public void MoveCameraToMe(EndDelegate endDelegate, ProcessDelegate processDelegate, float flySpeed, float angleSpeed)
        {
            GameStarter.Instance.MainComponents.GameCamera.MoveTo(transform.position,
                CameraParams, endDelegate, processDelegate,
                flySpeed, angleSpeed);
        }
    }
}