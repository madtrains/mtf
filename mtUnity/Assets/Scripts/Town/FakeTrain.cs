using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WaySystem;

public class FakeTrain : UberBehaviour
{
    public MTCharacters.ExitPoint[] GetInPoints { get { return points; } }
    public int PassengersCount { get { return passengersNumber; } set { passengersNumber = value; } }
    public enum State { AtStation, WayOut, Outside, WayToStation }
    public float speed;
    public UnityEngine.Events.UnityAction<Vector3, Quaternion> OnPassengerOut;
    public UnityEngine.Events.UnityAction<MTCharacters.TargetPoint[]> OnArrive;
    [SerializeField] private float maxSpeed;
    [SerializeField] private float minSpeed;
    [SerializeField] private Transform loco;
    [SerializeField] private Transform wagen;
    [SerializeField] private float wagenOffset;
    [SerializeField] private AnimationCurve curve;
    [SerializeField] private MinMax limits;
    [SerializeField] private Transform[] doors;
    [SerializeField] private TimerBeh timer;
    [SerializeField] private MinMax outTime;
    [SerializeField] private MinMax passengerInterval;
    [SerializeField] private MTCharacters.ExitPoint[] points;
    [SerializeField] private AudioSource engineSound;
    private int passengersNumber;
    private int cPassenger;

    private WayChunkMovingPoint movingPoint;
    private WayChunkMovingPoint movingPoint2;
    private bool inited;
    private WayChunk startChunk;
    private State CState
    {
        get { return _state; }
        set
        {
            if (_state != value)
            {
                _state = value;
                if (value == State.AtStation)
                {
                    Station();
                }

                else if (value == State.Outside)
                {
                    Away();
                    engineSound.Stop();
                }
                    
                else if (value == State.WayOut)
                {
                    speed = 1f;
                }
                    
                else if (value == State.WayToStation)
                {
                    speed = -1f;
                    engineSound.Play();
                }
                    
            }
        }
    }

    private State _state;

    private float positionSpeed;
    private float lerpT;


    public void Launch()
    {
        timer.Launch(outTime.Random);
    }

    public void Place(WayChunk wayChunk)
    {
        movingPoint = new WayChunkMovingPoint();
        movingPoint2 = new WayChunkMovingPoint();
        startChunk = wayChunk;
        movingPoint.Place(wayChunk, limits.Min);
        movingPoint2.PlaceOffsetedFromOther(movingPoint, wagenOffset);
        inited = true;
        timer.OnEnd = () => { CState = State.WayOut; };
        UpdateTransforms();
    }

    public void Move()
    {
        lerpT = Mathf.InverseLerp(limits.Min, limits.Max, movingPoint.OffsetGlobal); ;
        float ev = curve.Evaluate(lerpT);
        positionSpeed = Mathf.Lerp(minSpeed, maxSpeed, ev);
        this.movingPoint.Move(speed * Time.deltaTime * positionSpeed);
        movingPoint2.PlaceOffsetedFromOther(movingPoint, wagenOffset);
        UpdateTransforms();
    }

    private void UpdateTransforms()
    {
        loco.transform.position = movingPoint.Point.Position;
        loco.transform.rotation = movingPoint.Point.Rotation;
        wagen.transform.position = movingPoint2.Point.Position;
        wagen.transform.rotation = movingPoint2.Point.Rotation;
    }

    protected virtual void Update()
    {
        if (!inited)
            return;

        if (CState == State.WayOut || CState == State.WayToStation)
        {
            Move();
            if (movingPoint.OffsetGlobal >= limits.Max && CState == State.WayOut)
            {
                CState = State.Outside;
                Stop();
            }

            else if (movingPoint.OffsetGlobal <= limits.Min && CState == State.WayToStation)
            {
                CState = State.AtStation;
                Stop();
            }
        }   
    }

    private void Station()
    {
        Stop();
        SpawnPassenger();
        OnArrive?.Invoke(points);
    }

    private void Away()
    {
        Stop();
        timer.Launch(outTime.Random);
        timer.OnEnd = () => 
        { 
            CState = State.WayToStation;
        };
    }

    private void Stop()
    {
        speed = 0;
    }

    private void SpawnPassenger()
    {
        Transform door = GetArrayRandomElement<Transform>(doors);
        OnPassengerOut?.Invoke(door.position, door.rotation);
        cPassenger += 1;
        if (cPassenger < passengersNumber)
        {
            timer.Launch(passengerInterval.Random);
            timer.OnEnd = () => { SpawnPassenger(); };
        }
        else
        {
            CState = State.WayOut;
            cPassenger = 0;
        }
    }

    private void OnDrawGizmos()
    {
        if (this.movingPoint == null)
            return;
        Gizmos.DrawCube(this.movingPoint.Point.Position, Vector3.one);
    }
}
