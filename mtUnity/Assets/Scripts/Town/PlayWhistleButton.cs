using UnityEngine;

namespace MTUi
{
	public class PlayWhistleButton : UberButton
	{
		public enum PlayButtonModes { Enabled, Overweight }
		[SerializeField] private ContentHolders modes;


        public override void Init()
        {
            base.Init();
			Set(PlayButtonModes.Enabled);
        }

        public void Set(PlayButtonModes mode)
		{
			int intMode = (int)mode;
			modes.Activate(intMode);
			this.buttonComponent.interactable = intMode == 0;
		}
	}
		
}
