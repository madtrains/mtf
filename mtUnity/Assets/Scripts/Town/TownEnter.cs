﻿using MTCharacters.CharacterFactory;
using MTCore;
using MTTown.PointsOfInterest;
using MTUi;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using WaySystem;

namespace MTTown
{


	[System.Serializable]
	public class TownEnter
	{
        //public WayChunk StartChunk { get { return startChunk.WayChunk; } }

        [SerializeField] protected WayChunkBehaviour startChunk;
        [SerializeField] protected float startOffset;
		protected string townName;
		protected PointsManager pointsManager;

        public virtual void Prepare(string townName, PointsManager pointsManager)
		{
			this.townName = townName;
			this.pointsManager = pointsManager;
		}


		public virtual void Run()
		{

		}
	}
}
