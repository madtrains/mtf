﻿using System.Collections.Generic;

namespace MTCore
{
    public class Trip
	{
		public Trip(TripType type, TripSize size, List<int> resourses, int from, int to)
		{
			TripType = type;
			Size = size;
			Resourses = resourses;
			StartPoint = from;
			EndPoint = to;
		}

		public TripType TripType { get; set; }
		public TripSize Size { get; set; }
		public int StartPoint { get; set; }
		public int EndPoint { get; set; }

		/// <summary>
		/// Ресурсы <idРесурса , где нулевой ресурс - основной
		/// </summary>
		public List<int> Resourses { get; set; }
	}

	public enum TripType
	{
		Local,
		External
	}

	public enum TripSize : ushort
	{
		S,
		M,
		L,
		XL
	}
}
