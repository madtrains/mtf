﻿using MTUi;
using System.Collections.Generic;
using System.Linq;
using Trains;
using UnityEngine;

namespace MTTown.TrainMaintenance
{
    [System.Serializable]
    public class Depo : TrainMaintenance
    {
        public DepoDock[] DepoDockTransforms { get { return depoTransforms; } }

        public DepoDock GetDock(int ID)
        {
            return docksByIds[ID];
        }

        [SerializeField] private DepoDock[] depoTransforms;

        private Dictionary<int, DepoDock> docksByIds;
        private MinMax heights = new MinMax(3.8f, 8f);
        private MinMax heightsLocked = new MinMax(5f, 8f);
        private DepoSwipeBlocker swipeBlocker;


        public void Init(int[] ids)
        {
            base.Init();
            swipeBlocker = new DepoSwipeBlocker();
            cogUnlocker.OnChangeBuisinessState += (value) => { swipeBlocker.isGearsFlying = value; };
            GameStarter.Instance.UIRoot.Town.OnAuxActive = (value) =>{ swipeBlocker.isAuxUiPanelOpened = value; };
            docksByIds = new Dictionary<int, DepoDock>();
            int dockIndex = 0;
            foreach (MTParameters.Coach coachParameters in GameStarter.GameParameters.Coaches)
            {
                if (ids.Contains<int>(coachParameters.ID))
                {
                    Visual coachVisual =
                        GameObject.Instantiate<Visual>(GameStarter.Instance.TrainManager.Pull.GetCoach(coachParameters.ID));
                    UberBehaviour.Activate(coachVisual.gameObject, true);
                    coachVisual.Colorize(GameStarter.GameParameters.Customization.GetColor(0));
                    coachVisual.gameObject.name = coachParameters.Name + "_depot";

                    MTPlayer.CoachDepoInfo coachDepoInfo =
                        GameStarter.Instance.PlayerManager.GetCoachesList().FirstOrDefault(
                            info => info.Id == coachParameters.ID);
                    depoTransforms[dockIndex].Init(this, coachParameters, coachDepoInfo, coachVisual);
                    docksByIds[coachParameters.ID] = depoTransforms[dockIndex];
                    dockIndex++;
                }
            }

            foreach (MTParameters.Train loco in GameStarter.GameParameters.Trains)
            {
                if (ids.Contains<int>(loco.ID))
                {
                    Visual locoVisual =
                        GameObject.Instantiate<Visual>(GameStarter.Instance.TrainManager.Pull.GetLocomotive(loco.ID));
                    UberBehaviour.Activate(locoVisual.gameObject, true);
                    locoVisual.gameObject.name = loco.Name + "_depot";
                    locoVisual.StopEngine();
                    depoTransforms[dockIndex].Init(this, loco, locoVisual);
                    docksByIds[loco.ID] = depoTransforms[dockIndex];
                    dockIndex++;
                }
            }
        }

        protected override void Opened()
        {
            base.Opened();
            depoTransforms[currentIndex].SetAsTarget();
            GameStarter.Instance.MainComponents.Helpers.Arrow.PointAt(
                depoTransforms[currentIndex].transform.position, new MinMax(6f, 8f));
            GameStarter.Instance.UIRoot.Town.OnSwipe = Swipe;
        }

        public override void Close()
        {
            base.Close();
            GameStarter.Instance.MainComponents.Helpers.Arrow.Close();
            GameStarter.Instance.UIRoot.Town.TrainMaintenance.Hide();
            GameStarter.Instance.UIRoot.Town.OnSwipe = null;
            //GameStarter.Instance.UIRoot.Town.TrainMaintenance.Aux.Hide();
        }

        protected override void Swipe(SwipeDirection swipeDirection)
        {
            base.Swipe(swipeDirection);
            if (swipeDirection == SwipeDirection.Left)
            {
                if (currentIndex > 0 && !swipeBlocker.IsChangeTargetBlocked)
                {
                    swipeBlocker.isCameraFlying = true;
                    depoTransforms[currentIndex - 1].CamDock.MoveCameraToMe(
                    () =>
                    {
                        currentIndex--;
                        GameStarter.Instance.MainComponents.Helpers.Arrow.PointAt(
                            depoTransforms[currentIndex].transform.position,
                            depoTransforms[currentIndex].IsLocked ?
                            heightsLocked : heights);
                        depoTransforms[currentIndex].SetAsTarget();
                        swipeBlocker.isCameraFlying = false;

                    }, null);
                }
            }

            else if (swipeDirection == SwipeDirection.Right)
            {
                if (currentIndex < (depoTransforms.Length - 1) && !swipeBlocker.IsChangeTargetBlocked)
                {
                    swipeBlocker.isCameraFlying = true;
                    depoTransforms[currentIndex + 1].CamDock.MoveCameraToMe(
                    () =>
                    {
                        currentIndex++;
                        GameStarter.Instance.MainComponents.Helpers.Arrow.PointAt(
                            depoTransforms[currentIndex].transform.position,
                            depoTransforms[currentIndex].IsLocked ?
                            heightsLocked : heights);
                        depoTransforms[currentIndex].SetAsTarget();
                        swipeBlocker.isCameraFlying = false;
                    }, null);
                }
            }
        }
    }

    public class DepoSwipeBlocker : MaintenanceSwipeBlocker
    {
        public bool isGearsFlying;
        public bool isAuxUiPanelOpened;

        protected override bool GetChangeTargetBlocked()
        {
            if (isCameraFlying || isGearsFlying || isAuxUiPanelOpened)
                return true;
            return false;
        }
    }
}
