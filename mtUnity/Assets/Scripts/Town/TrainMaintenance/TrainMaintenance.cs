﻿using MTUi;
using UnityEngine;

namespace MTTown.TrainMaintenance
{
    [System.Serializable]
    public class TrainMaintenance : MTTown.PointsOfInterest.CamDockPoint
    {
        protected int currentIndex;
        public CogUnlocker CogUnlocker { get { return cogUnlocker; } }
        protected CogUnlocker cogUnlocker;

        public override void Open()
        {
            base.Open();
            currentIndex = 0;
        }

        protected override void Opened()
        {
            base.Opened();
            GameStarter.Instance.UIRoot.Town.TrainMaintenance.Show();
            GameStarter.Instance.MainComponents.SoundManager.Shared.Play(MTSound.SharedNames.MaintenanceOpen);
        }

        public override void Close()
        {
            base.Close();
            GameStarter.Instance.UIRoot.Town.TrainMaintenance.Hide();
            GameStarter.Instance.MainComponents.SoundManager.Shared.Play(MTSound.SharedNames.MaintenanceClose);
        }

        public virtual void Init()
        {
            cogUnlocker = new CogUnlocker();
        }

        protected virtual void Swipe(SwipeDirection driection)
        {

        }

        protected virtual void HoldTap(float time)
        {

        }
    }

    public class CogUnlocker
    {
        public delegate void OnUnlock();
        public UnityEngine.Events.UnityAction<bool> OnChangeBuisinessState;
        //public UnityEngine.Events.UnityAction<CogInfo> OnDone;

        public CogUnlocker()
        {
            
        }

        private int Remain { get { return cogInfo.TargetAmount - cogInfo.Amount; } }
        private int PlayerCogs { get { return GameStarter.Instance.PlayerManager.ResourceGetAmount(9); } }

        private MTPlayer.CogInfo cogInfo;
        private MTUi.Town.TrainMaintenance.CogUnlockerUI cogUnlockerUI;
        /// <summary>
        /// осталось чтобы докачать до конца
        /// </summary>
        private int remain;
        private int playerCogs;
        private int gearWeight = 1;
        private OnUnlock onUnlock;

        public void Assign(
            MTPlayer.CogInfo cogInfo,
            OnUnlock onUnlock,
            MTUi.Town.TrainMaintenance.CogUnlockerUI cogUnlockerUI)
        {
            this.cogInfo = cogInfo;
            this.cogUnlockerUI = cogUnlockerUI;
            cogUnlockerUI.Assign(GameStarter.Instance.UIRoot.Town.GearStream, cogInfo.Id);
            cogUnlockerUI.OnGearCreated = OnGearCreated;
            cogUnlockerUI.OnGearEnd = OnGearEnd;
            cogUnlockerUI.OnBurstend = OnBurstEnd;

            cogUnlockerUI.OnPressed = UiButtonPressRelease;
            cogUnlockerUI.UpdateInfo(this.cogInfo.Amount, this.cogInfo.TargetAmount);
            this.onUnlock = onUnlock;
            remain = Remain;
        }

        private void Calculate()
        {
            playerCogs = PlayerCogs;
            float cap = (float)Mathf.Min(playerCogs, remain);
            gearWeight = Mathf.FloorToInt(cap / 30f);
            if (gearWeight == 0)
                gearWeight = 1;
            int limit = Mathf.Min(remain, playerCogs);
            gearWeight = gearWeight > limit ? limit : gearWeight;
        }

        private void OnGearCreated(FloatingSprite floatingSprite)
        {
            floatingSprite.IntValue = gearWeight;
            remain -= gearWeight;
            playerCogs -= gearWeight;
            //cogUnlockerUI.temp1.text = remain.ToString();
            //cogUnlockerUI.temp2.text = playerCogs.ToString();
            //OnChangeValue?.Invoke(playerCogs);
            //Debug.LogFormat("Created gear {0} {1}", gearWeight, toTransfer);
            if (remain == 0 || playerCogs == 0)
            {
                cogUnlockerUI.Generate(false);
                return;
            }
            int limit = Mathf.Min(remain, playerCogs);
            gearWeight = gearWeight > limit ? limit : gearWeight;
            OnChangeBuisinessState?.Invoke(true);
            GameStarter.Instance.UIRoot.Town.Scores.CogCoins = playerCogs;
        }

        private void OnGearEnd(FloatingSprite floatingSprite)
        {
            this.cogInfo.Amount += floatingSprite.IntValue;

            cogUnlockerUI.UpdateInfo(this.cogInfo.Amount, this.cogInfo.TargetAmount);
        }

        private void OnBurstEnd()
        {
            //cogInfo.Amount =
            
            int difference = PlayerCogs - playerCogs;
            bool isDone = cogInfo.Amount == cogInfo.TargetAmount;
            if (isDone) 
            {
                cogInfo.IsDone = true;
            }
            GameStarter.Instance.PlayerManager.SetCogInfo(cogInfo);
            GameStarter.Instance.PlayerManager.TryReduceResource(9, difference);
            OnChangeBuisinessState?.Invoke(false);
            if (isDone)
            {
                //OnDone?.Invoke(cogInfo);
                onUnlock();
            }
        }

        private void UiButtonPressRelease(bool value)
        {
            if (value)
            {
                if (Remain > 0 && PlayerCogs > 0)
                {
                    Calculate();
                    cogUnlockerUI.Generate(true);
                }
            }
            else
            {
                cogUnlockerUI.Generate(false);
            }
        }
    }

    public class MaintenanceSwipeBlocker
    {
        public bool IsChangeTargetBlocked { get { return GetChangeTargetBlocked(); } }

        public bool isCameraFlying;

        protected virtual bool GetChangeTargetBlocked()
        {
            return false;
        }
    }
}