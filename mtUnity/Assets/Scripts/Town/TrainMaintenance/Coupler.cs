﻿using MTUi;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MTTown.TrainMaintenance
{
    [System.Serializable]
    public class Coupler : TrainMaintenance
    {
        [SerializeField] private Vector3 offset;
        [SerializeField] private float length;
        [SerializeField] private float holdTime = 1f;
        private Train train;
        private CouplerSwipeBlocker swipeBlocker;
        

        public override void Init()
        {
            base.Init();
            train = new Train(this);
            train.Add(new Cart(GameObject.Instantiate<CamDock>(camDock, camDock.transform.parent)));
            for (int i = 0; i < GameStarter.GameParameters.CoachesNumber; i++)
            {
                train.Add(new Cart(GameObject.Instantiate<CamDock>(camDock, camDock.transform.parent)));
            }
            GameStarter.Instance.UIRoot.Town.TrainMaintenance.Aux.Colorise.OnColorClick = 
                (index, color)=> { ColorClicked(index, color); };
            swipeBlocker = new CouplerSwipeBlocker();
        }


        public override void Open()
        {
            ProcessTrain();
            GameStarter.Instance.MainComponents.Helpers.Arrow.Heights = new MinMax(4.5f, 8f);
            GameStarter.Instance.UIRoot.Town.OnSwipe = Swipe;
            GameStarter.Instance.UIRoot.Town.OnTapHold = HoldTap;
            base.Open();
        }

        public override void Close()
        {
            base.Close();
            GameStarter.Instance.MainComponents.Helpers.Arrow.Close();
            GameStarter.Instance.UIRoot.Town.TrainMaintenance.Aux.Hide();
            GameStarter.Instance.UIRoot.Town.OnSwipe = null;
            GameStarter.Instance.UIRoot.Town.OnTapHold = null;
        }

        protected override void Opened()
        {
            base.Opened();
            GameStarter.Instance.MainComponents.Helpers.Arrow.ArrowVisible = true;
            GameStarter.Instance.UIRoot.Town.TrainMaintenance.SetAsPurchased();
            UpdateCurrentIndex();
        }

        protected override CamDock GetCamDock()
        {
            return train[0].camDock;
        }

        protected override void Swipe(SwipeDirection direction)
        {
            base.Swipe(direction);
            switch (direction)
            {
                case SwipeDirection.Left:
                {
                    TryMoveTowardsTail();
                    break;
                }
                case SwipeDirection.Right:
                {
                    TryMoveTowardsHead();
                    break;
                }
                case SwipeDirection.Up:
                {
                    Swap(true);
                    break;
                }
                case SwipeDirection.Down:
                {
                    Swap(false);
                    break;
                }
            }
        }

        protected override void HoldTap(float time)
        {
            base.HoldTap(time);
            Remove();
        }

        private void UpdateCurrentIndex()
        {
            //Debug.LogFormat("Coupler current index: {0}", currentIndex);
            Cart cart = train[currentIndex];
            GameStarter.Instance.MainComponents.Helpers.Arrow.PointAt(cart.point);
            if (cart.elType == ElType.Locomotive)
            {
                GameStarter.Instance.UIRoot.Town.TrainMaintenance.SetLocomotiveInfo(
                    GameStarter.Instance.TrainManager.Train.TrainParameters);
            }
            else if (cart.elType == ElType.Coach)
            {
                GameStarter.Instance.UIRoot.Town.TrainMaintenance.SetCoachInfo(
                    cart.absoluteIndex,  cart.Coach.Parameters);
                GameStarter.Instance.UIRoot.Town.TrainMaintenance.SetCoachForCoupler(
                    cart.absoluteIndex, cart.Coach.Parameters);
            }
        }

        private void Swap(bool reversed)
        {
            if (currentIndex == 0)
            {
                GameStarter.Instance.TrainManager.SwapLoco(reversed);
            }
            //вагон
            else
            {
                if (train[currentIndex].elType == ElType.Coach)
                {
                    //0 это локомотив, а нам нужно считать вагоны в своём массиве от 0
                    int coachIndex = currentIndex - 1;
                    GameStarter.Instance.TrainManager.SwapCoach(coachIndex, reversed);
                }
                else if (train[currentIndex].elType == ElType.Empty)
                {
                    GameStarter.Instance.TrainManager.AddCoach();
                }
            }
            ProcessTrain();
            UpdateCurrentIndex();
            //GameStarter.Instance.TrainManager.SwapLoco
        }

        private void Remove()
        {
            if (train[currentIndex].elType != ElType.Coach)
                return;
            int coachIndex = currentIndex - 1;
            bool removed = GameStarter.Instance.TrainManager.Remove(coachIndex);
            if (removed)
            {
                ProcessTrain();
                UpdateCurrentIndex();
            }
        }

        private void TryMoveTowardsTail()
        {
            if (currentIndex == train.Count - 1)
                return;
            if (train[currentIndex].elType == ElType.Empty)
                return;
            if (swipeBlocker.IsChangeTargetBlocked)
                return;

            swipeBlocker.isCameraFlying = true;
            int nextIndex = currentIndex + 1;
            if (train[nextIndex].elType == ElType.Indispensable)
                nextIndex += 1;
            train[nextIndex].camDock.MoveCameraToMe(() =>
            {
                currentIndex = nextIndex;

                UpdateCurrentIndex();
                swipeBlocker.isCameraFlying = false;
            },
            (t) => { });
        }

        private void TryMoveTowardsHead()
        {
            //стоим на локомотиве
            if (currentIndex == 0)
                return;

            if (swipeBlocker.IsChangeTargetBlocked)
                return;

            int nextIndex = currentIndex - 1;
            if (train[nextIndex].elType == ElType.Indispensable)
                nextIndex -= 1;
            /*
            if (nextIndex == 0 && GameStarter.Instance.UIRoot.Town.TrainMaintenance.Aux.IsBlocking)
                return;
            */

            swipeBlocker.isCameraFlying = true;
            train[nextIndex].camDock.MoveCameraToMe(() =>
            {
                currentIndex = nextIndex;
                UpdateCurrentIndex();
                swipeBlocker.isCameraFlying = false;
            },
            (t) => { });
        }

        private void ProcessTrain()
        {
            train[0].elType = ElType.Locomotive;
            train[0].camDock.transform.position =
                GameStarter.Instance.TrainManager.Train.transform.position + offset;
            train[0].point = GameStarter.Instance.TrainManager.Train.transform.position;
            train[0].absoluteIndex = 0;
            int absIndex = 1;
            for (int coachIndex = 0; coachIndex < GameStarter.Instance.TrainManager.Train.Coaches.Count; coachIndex++)
            {
                Trains.Coach coach = GameStarter.Instance.TrainManager.Train.Coaches[coachIndex];
                bool indispensable = coach.Parameters == null;
                int coachCorrectedIndex =
                    GameStarter.Instance.TrainManager.Train.TrainParameters.HasIndispensableCoach ?
                    coachIndex - 1 : coachIndex;
                train[absIndex].absoluteIndex = absIndex;
                train[absIndex].coachAbsoluteIndex = coachIndex;
                train[absIndex].coachRelativeIndex = coachCorrectedIndex;
                train[absIndex].elType = indispensable ? ElType.Indispensable : ElType.Coach;
                train[absIndex].point = coach.transform.position;
                train[absIndex].camDock.transform.position =
                    train[absIndex].point +
                    coach.transform.TransformDirection(offset);
                absIndex++;
            }

            //не осталось ни пустых ни залоченых позиций
            if (absIndex == train.Count)
            {
                ProcessDebug();
                return;
            }

            //пустая позиция
            Transform lastCoach = 
                GameStarter.Instance.TrainManager.Train.Coaches.Last<Trains.Coach>().transform;
            train[absIndex].elType = ElType.Empty;
            train[absIndex].point =
                lastCoach.position +
                lastCoach.TransformDirection(Vector3.forward * length * -1f);
            train[absIndex].camDock.transform.position =
                    train[absIndex].point +
                    lastCoach.TransformDirection(offset);
            absIndex++;
            //залоченые
            for (; absIndex < train.Count; absIndex++)
            {
                train[absIndex].elType = ElType.Locked;
            }

            ProcessDebug();
        }
        private void ProcessDebug()
        {
            return;
            StringBuilder debug = new StringBuilder();
            for (int a = 0; a < train.Count; a++)
            {
                debug.Append("I: ");
                debug.Append(a);
                debug.Append(" Type: ");
                debug.Append(train[a].elType);
                debug.Append("\n");
            }
            Debug.LogFormat(debug.ToString());
        }

        private void ColorClicked(int colorIndex, Color color)
        {
            Cart cart = train[currentIndex];
            if (cart.elType == ElType.Coach)
            {
                GameStarter.Instance.TrainManager.ColorizeCoach(
                    cart.coachAbsoluteIndex, 
                    cart.coachRelativeIndex, 
                    colorIndex, color);
            }
        }

        private class Train : List<Cart>
        {
            private Coupler coupler;

            public Train(Coupler coupler) : base()
            {
                this.coupler = coupler;
            }
        }


        [System.Serializable]
        private class Cart
        {
            public ElType elType;
            public CamDock camDock;
            /// <summary>
            /// индекс абсолютного смещения, где 0 - локомотив
            /// </summary>
            public int absoluteIndex;
            /// <summary>
            /// индекс смещения вагонов (включая тендер), где 0 - первый вагон или тендер
            /// </summary>
            public int coachAbsoluteIndex;

            /// <summary>
            /// индекс смещения вагонов (исключая тендер, если он есть), где 0 - только первый вагон
            /// </summary>
            public int coachRelativeIndex;

            public Vector3 point;

            public Trains.Coach Coach 
            {
                get
                {
                    return GameStarter.Instance.TrainManager.UsedCoaches[coachAbsoluteIndex];
                }
            }

            public Cart(CamDock camDock)
            {
                this.elType = ElType.Locked;
                this.camDock = camDock;
            } 
        }
    }

    public class CouplerSwipeBlocker : MaintenanceSwipeBlocker
    {
        protected override bool GetChangeTargetBlocked()
        {
            if (isCameraFlying)
                return true;
            return false;
        }
    }
}
