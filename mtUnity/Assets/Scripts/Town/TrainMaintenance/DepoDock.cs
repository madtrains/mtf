﻿using MTPlayer;
using MTUi;
using Trains;
using UnityEngine;


namespace MTTown.TrainMaintenance
{
    public class DepoDock : UberBehaviour
	{
        public static int ONE_TYPE_COACHES_COUNT = 4;
        public enum Progress { CogLock, Unlocked, IsBought}

        public CamDock CamDock { get { return camDock; } }
        public MTParameters.Train TrainParameters { get { return TrainParameters; } }
        public MTParameters.Coach CoachParams { get { return coachParameters; } }

        private Depo depo;
        private ElType elType;
        private MTParameters.Train trainParams;
        private MTParameters.Coach coachParameters;
        private CoachDepoInfo coachDepoInfo;
        private YArrow lockPointer;
        [SerializeField] private CamDock camDock;
        [SerializeField] private int priceCoins;
        [SerializeField] private int priceCogs;

        public int ID
        {
            get
            {
                return elType == ElType.Locomotive ?
                    trainParams.ID :
                    coachParameters.ID;
            }
        }

        public bool IsBought 
        { 
            get 
            {
                return elType == ElType.Locomotive ?
                    GameStarter.Instance.TrainManager.IsLocomotiveBought(trainParams.ID) :
                    GameStarter.Instance.TrainManager.IsCoachBought(coachParameters.ID); 
            } 
        }

        public bool CanBuy 
        { 
            get 
            {
                return elType == ElType.Locomotive ?
                    GameStarter.Instance.TrainManager.CanBuyTrain(trainParams.ID) :
                    GameStarter.Instance.TrainManager.CanBuyCoach(coachParameters.ID);
            } 
        }

        public CogInfo CogInfoUnlockBuy
        {
            get
            {
                return GameStarter.Instance.PlayerManager.GetCogInfo(ID) ??
                    GameStarter.Instance.PlayerManager.SetCogInfo(
                        new CogInfo() { Id = ID, TargetAmount = priceCogs });
            }
        }

        public bool IsLocked
        {
            get
            {
                return !CogInfoUnlockBuy.IsDone;
            }
        }



        public void Init(Depo depo, MTParameters.Train train, Visual visual)
		{
            this.depo = depo;
			this.elType = ElType.Locomotive;
			this.trainParams = train;
            ProcessVisual(visual);

            int locoID = train.ID;
            priceCoins = GameStarter.Instance.MarketManager.TryGetTrainInfo(
                    locoID, 
                    MTTown.MTMarket.MarketCurrency.MTC, 
                    out var info) 
                        ? info.Price : 99999;

            priceCogs = GameStarter.Instance.MarketManager.TryGetTrainInfo(
                    locoID, 
                    MTTown.MTMarket.MarketCurrency.MTG,
                    out var infoCogs) 
                        ? infoCogs.Price : 88888;
        }

		public void Init(Depo depo, MTParameters.Coach coachParameters, CoachDepoInfo coachDepoInfo, Visual visual)
		{
            this.depo = depo;
            this.elType = ElType.Coach;
            this.coachParameters = coachParameters;
            this.coachDepoInfo = coachDepoInfo;
			ProcessVisual(visual);

            int coachID = coachParameters.ID;
            priceCoins = GameStarter.Instance.MarketManager.TryGetCoachInfo(
                    coachID,
                    MTTown.MTMarket.MarketCurrency.MTC,
                    out var info)
                        ? info.Price : 77777;

            priceCogs = GameStarter.Instance.MarketManager.TryGetCoachInfo(
                    coachID,
                    MTTown.MTMarket.MarketCurrency.MTG,
                    out var infoCogs)
                        ? infoCogs.Price : 66666;
        }

		private void ProcessVisual(Visual visual)
		{
            visual.transform.SetParent(this.transform);
            visual.transform.localPosition = Vector3.zero;
            visual.transform.localRotation = Quaternion.identity;
        }

        public void SetAsTarget()
        {
            CheckCreateLockPointer();
            if (elType == ElType.Locomotive)
            {
                GameStarter.Instance.UIRoot.Town.TrainMaintenance.SetLocomotiveInfo(trainParams);
                GameStarter.Instance.UIRoot.Town.TrainMaintenance.OnButtonBuyClicked = () =>
                {
                    GameStarter.Instance.TrainManager.BuyLocomotive(trainParams);
                    GameStarter.Instance.UIRoot.Town.TrainMaintenance.SetAsPurchased();
                    Destroy(lockPointer.gameObject);
                };
            }
            //coach
            else
            {
                GameStarter.Instance.UIRoot.Town.TrainMaintenance.SetCoachInfo(0, coachParameters);
                ProcessCoachDepoInfo();

                GameStarter.Instance.UIRoot.Town.TrainMaintenance.OnButtonBuyClicked = () =>
                {
                    GameStarter.Instance.TrainManager.BuyCoach(coachParameters);
                    GameStarter.Instance.UIRoot.Town.TrainMaintenance.SetAsPurchased();
                    ProcessCoachDepoInfo();
                    Destroy(lockPointer.gameObject);
                };
            }

            if (IsBought)
            {
                GameStarter.Instance.UIRoot.Town.TrainMaintenance.SetAsPurchased();
            }
            else
            {
                if (CogInfoUnlockBuy.IsDone)
                {
                    GameStarter.Instance.UIRoot.Town.TrainMaintenance.SetAsUnlocked(CanBuy);
                }
                else
                {
                    GameStarter.Instance.UIRoot.Town.TrainMaintenance.SetAsLocked();
                    depo.CogUnlocker.Assign(CogInfoUnlockBuy,
                        () =>
                        {
                            GameStarter.Instance.UIRoot.Town.TrainMaintenance.SetAsUnlocked(CanBuy);
                            lockPointer.ReplacePointer(
                                GameStarter.Instance.MainComponents.Helpers.CoinPointer);
                        },
                    GameStarter.Instance.UIRoot.Town.TrainMaintenance.CogUnlockerUI);
                    GameStarter.Instance.UIRoot.Town.TrainMaintenance.CogUnlockerUI.From.ShowResource(9);
                    GameStarter.Instance.UIRoot.Town.TrainMaintenance.CogUnlockerUI.From.ValueAndCross = priceCogs;
                    GameStarter.Instance.UIRoot.Town.TrainMaintenance.CogUnlockerUI.From.Stars = 0;
                    GameStarter.Instance.UIRoot.Town.TrainMaintenance.CogUnlockerUI.To.ShowResource(0);
                    GameStarter.Instance.UIRoot.Town.TrainMaintenance.CogUnlockerUI.To.ValueAndCross = priceCoins;
                    GameStarter.Instance.UIRoot.Town.TrainMaintenance.CogUnlockerUI.To.Stars = 0;
                }
            }
        }

        private void ProcessCoachDepoInfo()
        {
            coachDepoInfo =
                    GameStarter.Instance.TrainManager.Account.GetCoachDepoInfo(coachParameters.ID);
            if (coachDepoInfo == null)
                return;
            CogInfo coachNumberCogInfo =
                GameStarter.Instance.PlayerManager.GetCogInfo(coachDepoInfo.NumberCog);
            
            CLickDelegate numberUpgradeClick = null;

            //объекта прокачки вагона не было создано
            if (coachNumberCogInfo == null)
            {
                //создаём с нуля объект прокачки
                coachNumberCogInfo = GameStarter.Instance.PlayerManager.SetCogInfo(
                        new CogInfo()
                        { 
                            Id = coachDepoInfo.NumberCog,
                            TargetAmount = coachParameters.CogCoinsPrice
                        });
            }
            if (coachDepoInfo.Number != ONE_TYPE_COACHES_COUNT)
            {
                CogUnlocker.OnUnlock onUnlock = () =>
                {
                    coachDepoInfo.Number = coachDepoInfo.Number + 1;
                    coachDepoInfo.NumberLevel = coachDepoInfo.NumberLevel + 1;
                    GameStarter.Instance.PlayerManager.SaveCoach(coachDepoInfo);
                    GameStarter.Instance.UIRoot.Town.TrainMaintenance.Aux.Hide();
                    coachNumberCogInfo.IsDone = false;
                    coachNumberCogInfo.Amount = 0;
                    GameStarter.Instance.PlayerManager.SetCogInfo(coachNumberCogInfo);
                    ProcessCoachDepoInfo();
                };
                numberUpgradeClick = () =>
                {
                    depo.CogUnlocker.Assign(
                        coachNumberCogInfo,
                        onUnlock,
                        GameStarter.Instance.UIRoot.Town.TrainMaintenance.Aux.CogUnlocker);
                    GameStarter.Instance.UIRoot.Town.TrainMaintenance.Aux.CogUnlocker.From.Activate(MTUi.Town.TrainMaintenance.Attributes.Coaches);
                    GameStarter.Instance.UIRoot.Town.TrainMaintenance.Aux.CogUnlocker.From.ValueAndCross = coachDepoInfo.Number;
                    GameStarter.Instance.UIRoot.Town.TrainMaintenance.Aux.CogUnlocker.From.Stars = coachDepoInfo.NumberLevel;

                    GameStarter.Instance.UIRoot.Town.TrainMaintenance.Aux.CogUnlocker.To.Activate(MTUi.Town.TrainMaintenance.Attributes.Coaches);
                    GameStarter.Instance.UIRoot.Town.TrainMaintenance.Aux.CogUnlocker.To.ValueAndCross = coachDepoInfo.Number + 1;
                    GameStarter.Instance.UIRoot.Town.TrainMaintenance.Aux.CogUnlocker.To.Stars = coachDepoInfo.NumberLevel + 1;
                };
            }
            else
            {
                numberUpgradeClick = null;
            }
            GameStarter.Instance.UIRoot.Town.TrainMaintenance.SetCoachForDepo(coachParameters, coachDepoInfo, numberUpgradeClick);  
        }

        private void CheckCreateLockPointer()
        {
            if (lockPointer == null)
            {
                if (IsBought)
                    return;
                YArrow target = CogInfoUnlockBuy.IsDone ?
                GameStarter.Instance.MainComponents.Helpers.Coin :
                GameStarter.Instance.MainComponents.Helpers.Gear;
                lockPointer =
                    GameObject.Instantiate<YArrow>(target, this.transform);
                Activate(lockPointer.gameObject, true);
                lockPointer.transform.localPosition = Vector3.zero;
                lockPointer.transform.localRotation = Quaternion.identity;
                lockPointer.Heights = new MinMax(4.7f, 5f);
            }
        }
	}
}
