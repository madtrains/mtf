using Assets.Scripts.Dialog.Dialogs.Characters;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTTown.Characters
{
	public class GrosserFrantz : InteractiveCharacter
	{
		private void Awake()
		{
			Dialogs = new FrantzDialogs(GameStarter.Instance.TownManager.TownParameters.ID);
			TaskResourcesList = new List<int>() { 5 };
		}
	}
}