using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTTown.Characters
{
    public class TownCharacter : UberBehaviour
    {
        public virtual MTDCharacterDialogs Dialogs { get; set; }
        public virtual void EventChangeRandomAnim()
        {

        }    
    }
}