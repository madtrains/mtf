using Assets.Scripts.Dialog.Dialogs.Characters;
using System.Collections.Generic;

namespace MTTown.Characters
{
    public class AlterFritz : InteractiveCharacter
    {
		private void Awake()
		{
			Dialogs = new FritzDialogs (GameStarter.Instance.TownManager.TownParameters.ID);
			TaskResourcesList = new List<int>() { 1,2 };
		}
	}
}