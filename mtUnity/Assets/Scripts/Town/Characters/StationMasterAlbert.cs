using Assets.Scripts.Dialog.Dialogs.Characters;
using System.Collections.Generic;

namespace MTTown.Characters
{
    public class StationMasterAlbert : InteractiveCharacter
    {
		private void Awake()
		{
			Dialogs = new AlbertDialogs (GameStarter.Instance.TownManager.TownParameters.ID);
			TaskResourcesList = new List<int>() { 1,2 };
		}
	}
}