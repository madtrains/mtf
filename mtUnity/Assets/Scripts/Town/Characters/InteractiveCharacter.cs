using MTUi;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MTTown.Characters
{
    public class InteractiveCharacter : MTCharacters.CharacterLogic
	{
		public MTDCharacterDialogs Dialogs { get; set; }
		public GameCharacter Character { get { return character; } set { character = value; } }
		public Transform BubbleDock { get { return bubbleDock; } }
		public Vector2 Offset { get { return offset; } set { offset = value; } }

		public List<int> TaskResourcesList { get; set; }

		[SerializeField] protected Transform bubbleDock;
		[SerializeField] protected Vector2 offset;
		[SerializeField] protected GameCharacter character;
		[SerializeField] protected MTUi.Bubble uiBubble;

		public void ShowUpdateBubble()
		{
			(bool hasDialog, bool hasTask) state = GameStarter.Instance.GamePlayManager.GetCharacterState(character);

			SetBubbleClickDelegate(() =>
			{
				GameStarter.Instance.GamePlayManager.StartDialog(this, () => { ShowUpdateBubble(); });
			});

			ShowUiBubbleWithState(state.hasDialog, state.hasTask);
		}

		public void HideBubble()
		{
			uiBubble.Hide();
		}

		public void AssignBubble(Bubble bubble, Transform bubbleDock)
		{
			this.uiBubble = bubble;
			this.bubbleDock = bubbleDock;
		}

		protected void ShowUiBubbleWithState(bool hasTask, bool hasDialog)
		{
			uiBubble.Character = this;
			
			if (hasDialog || hasTask)
			{
                uiBubble.Show();
                ChangeState(hasDialog, hasTask);
			}
			else
			{
				uiBubble.Hide();
			}	
		}

		protected void ShowIdleNoTaskBubble()
		{
			uiBubble.Character = this;
			uiBubble.Show();
			uiBubble.ChangeState(Bubble.StateValue.Idle);
		}

		protected void SetBubbleClickDelegate(CLickDelegate disposableDelegate)
		{
			this.uiBubble.OnClick = () => { disposableDelegate(); this.uiBubble.OnClick = null; };
		}

		protected void ChangeState(bool haveDialog, bool haveTask = false)
		{
			if (haveDialog)
			{
				if (haveDialog)
				{
					uiBubble.ChangeState(Bubble.StateValue.Task);//�������� ������ � Ready
				}
				else
				{
					uiBubble.ChangeState(Bubble.StateValue.Wait);
				}
			}
			else if (haveTask)
			{
				
				uiBubble.ChangeState(Bubble.StateValue.Ready);
			}
			else
			{
				uiBubble.ChangeState(Bubble.StateValue.Idle);
			}
		}

		/*
        protected virtual void Click()
        {
            if (disposableDelegate != null)
            {
                disposableDelegate();
                //disposableDelegate = null;
            }
        }
        */


	}
}

