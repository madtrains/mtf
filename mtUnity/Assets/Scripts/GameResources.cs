
public enum GameResources
{
    Coin,
    Passenger,
    Mail,
    Oil,
    Iron,
    Wood,
    Fish,
    Freight
}

