﻿using System.Threading.Tasks;
using UnityEngine;

public class MainComponentsManager : UberBehaviour
{
    public TimerManager TimerManager { get { return timerManager; } }
    public MTLightNCamera.GameCamera GameCamera { get { return gameCamera; } }
    public MTLightNCamera.GameLight GameLight { get { return gameLight; } }
    public MTSound.SoundManager SoundManager { get { return soundManager; } }
    public Helpers Helpers { get { return helpers; } }

    [SerializeField] private MTLightNCamera.GameCamera gameCamera;
    [SerializeField] private MTLightNCamera.GameLight gameLight;
    [SerializeField] private MTSound.SoundManager soundManager;
    [SerializeField] private Helpers helpers;

    private TimerManager timerManager;

    public async Task Init()
    {
        timerManager = gameObject.AddComponent<TimerManager>();
        timerManager.Init();
        MTSound.SoundsShared soundsShared =
            await GameStarter.Instance.ResourceManager.InstantiateByBundleName<MTSound.SoundsShared>(MTAssetBundles.sound_soundsshared);
        soundsShared.Init();
		soundsShared.transform.SetParent(this.transform, false);
        soundManager.Shared = soundsShared;
    }
}