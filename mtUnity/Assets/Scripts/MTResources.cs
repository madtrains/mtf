public enum MTResources
{
    Coin = 0, // TranslationKey: Resource.Coin, IsCargo: False, ShowInUICargoCounters: False, ResultsOrder: 8
    Passenger = 1, // TranslationKey: Resource.Passenger, IsCargo: True, ShowInUICargoCounters: True, ResultsOrder: 0
    Mail = 2, // TranslationKey: Resource.Mail, IsCargo: True, ShowInUICargoCounters: True, ResultsOrder: 1
    Oil = 3, // TranslationKey: Resource.Oil, IsCargo: True, ShowInUICargoCounters: True, ResultsOrder: 2
    Gem = 4, // TranslationKey: Resource.Gem, IsCargo: True, ShowInUICargoCounters: False, ResultsOrder: 3
    Wood = 5, // TranslationKey: Resource.Wood, IsCargo: True, ShowInUICargoCounters: True, ResultsOrder: 4
    Fish = 6, // TranslationKey: Resource.Fish, IsCargo: True, ShowInUICargoCounters: False, ResultsOrder: 5
    Fracht = 7, // TranslationKey: Resource.Freight, IsCargo: True, ShowInUICargoCounters: True, ResultsOrder: 6
    Ticket = 8, // TranslationKey: Resource.Ticket, IsCargo: False, ShowInUICargoCounters: False, ResultsOrder: 9
    CogCoin = 9, // TranslationKey: Resource.CogCoin, IsCargo: False, ShowInUICargoCounters: False, ResultsOrder: 7
}
