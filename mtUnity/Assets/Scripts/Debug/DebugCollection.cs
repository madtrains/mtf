﻿using UnityEngine;

namespace MTDebug
{
    [CreateAssetMenu(menuName = "Debug Collection")]
    public class DebugCollection : ScriptableObject
    {
        public Mesh Arrow { get { return arrow; } }
        public GameObject Axis { get { return axis; } }
        public Mesh Cube { get { return cube; } }
        public Mesh Octagon { get { return octagon; } }
        public Mesh Drop { get { return drop; } }

        public Material DebugUnlit { get { return debugUnlit; } }
        public Material DebugUnlitTransparent { get { return debugUnlitTransparent; } }


        [SerializeField] private Mesh arrow;
        [SerializeField] private GameObject axis;
        [SerializeField] private Mesh octagon;
        [SerializeField] private Mesh drop;
        [SerializeField] private Mesh cube;
        
        [SerializeField] private Material debugUnlit;
        [SerializeField] private Material debugUnlitTransparent;
    }
}
