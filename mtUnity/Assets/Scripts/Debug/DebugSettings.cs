﻿using MTData;

namespace MTDebug
{
    [SaveableProperties(name: "MT_DebugSettings.mts", folder: "MT_DEBUG", storage: "MT_STORAGE")]
	public class DebugSettings : SaveableDictionary
	{
		//поля с простыми типами можно прям так создавать
		[SaveableField] public bool isActive = false;

        public DebugSettings(string localStoragePath) : base(localStoragePath)
		{

		}

		public void Activate()
		{
			isActive = true;
            SetInt("gameMode", (int)GameStarter.GameMode.StartWithDebugScreen);
            SAVE();
        }

		public GameStarter.GameMode GameMode
		{
			get
			{
                int result = GetInt("gameMode");
                return (GameStarter.GameMode)result;
            }

			set
			{
                SetInt("gameMode", (int)value);
				SAVE();
            }
		}


		public MTCore.TripSize TestFlipperSize
        {
            get
            {
                int result = GetInt("testFlipperSize");
                return (MTCore.TripSize)result;
            }
            set
			{
                SetInt("testFlipperSize", (int)value);
				SAVE();
            }
		}

        public bool DebugDraw
        {
            get
            {
                bool result = GetBool("debugDraw");
                return result;
            }
            set
            {
                SetBool("debugDraw", value);
                SAVE();
            }
        }

        public bool DebugChunksAtStart
        {
            get
            {
                bool result = GetBool("debugChunksAtStart");
                return result;
            }
            set
            {
                SetBool("debugChunksAtStart", value);
                SAVE();
            }
        }

        public bool FlipperAdditionalInfo
        {
            get
            {
                bool result = GetBool("flipperAdditionalInfo");
                return result;
            }
            set
            {
                SetBool("flipperAdditionalInfo", value);
                SAVE();
            }
        }
    }
}
