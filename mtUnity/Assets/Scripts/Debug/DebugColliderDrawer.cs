using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Must;

namespace MTDebug
{
    public class DebugColliderDrawer : MonoBehaviour
    {
        private Dictionary<BoxCollider, Transform> links;
        private void Start()
        {
            links = new Dictionary<BoxCollider, Transform>();
        }

        private void FixedUpdate()
        {
            BoxCollider[] boxColliders = FindObjectsByType<BoxCollider>(FindObjectsSortMode.None);        
            foreach (BoxCollider boxCollider in boxColliders)
            {
                if (!links.ContainsKey(boxCollider) || links[boxCollider] == null)
                {
                    Transform cube = CreateCube(boxCollider);
                    links[boxCollider] = cube;
                    
                }
                Matrix4x4 m = boxCollider.transform.localToWorldMatrix;
                links[boxCollider].position = m.MultiplyPoint(boxCollider.center);
                links[boxCollider].rotation = m.rotation;
            }
        }

        private Transform CreateCube(BoxCollider boxCollider)
        {
            Transform cube = MTDebug.DebugManager.CreatePrimitive(
                PrimitiveType.Cube,
                DebugColorCollection.RandomColor, 0.7f,
                boxCollider.transform.position, boxCollider.size, boxCollider.transform.rotation, true, boxCollider.name + "_col");
            return cube;
        }
    }
}

