﻿using MTDebug;
using System.Collections.Generic;
using UnityEngine;


namespace MTDebug
{
    public class DebugManager
    {
        private DebugManager() { }
        public static DebugManager Init() {
			Instance = Instance ?? new DebugManager();
            Instance.DebugSettings = new DebugSettings(GameStarter.InBuildParameters.LocalStoragePath);
			Instance.DebugSettings.LOAD();
			return Instance;
		}
        public static DebugManager Instance { get; private set; }

		public DebugSettings DebugSettings;

		public void SaveLocalSettings()
		{
			DebugSettings.SAVE();
		}

		/// <summary>
		/// Creates arrow of defined color and size
		/// </summary>
		/// <param name="position">Position of arrow</param>
		/// <param name="direction">Direction of arrow</param>
		/// <param name="color">Arrows color</param>
		/// <param name="size">Arrows scale</param>
		/// <param name="sefDestruct">After 5 seconds go will be destroyed</param>
		/// <returns></returns>
		public static GameObject Arrow(Vector3 position, Vector3 direction,
            Color color, float size, bool sefDestruct)
        {
            Quaternion rotation = Quaternion.LookRotation(direction);
            GameObject go = new GameObject("_arrow");
            go.transform.position = position;
            go.transform.rotation = rotation;
            go.transform.localScale = Vector3.one * size;
            MeshFilter mf = go.AddComponent<MeshFilter>();
            mf.mesh = Collection.Arrow;
            MeshRenderer mr = go.AddComponent<MeshRenderer>();
            mr.material = Collection.DebugUnlit;
            //mr.material.color = color;

            if (sefDestruct)
            {
                TimerBeh tb = go.AddComponent<TimerBeh>();
                tb.SelfDestructable = true;
                tb.Launch(12f);
            }
            return go;
        }

        public static GameObject Arrow(Vector3 position, Vector3 direction, float size)
        {
            return Arrow(position, direction, Color.white, size, false);
        }

        public static GameObject Arrow(WaySystem.Point point, float size, Transform parent)
        {
            GameObject go = Arrow(point.Position, point.Vector(), Color.white, size, false);
            if (parent != null)
                go.transform.SetParent(parent, true);
            return go;
        }

        public static GameObject Axis(float size)
        {
            GameObject go = GameObject.Instantiate(Collection.Axis);
            go.transform.localScale = Vector3.one * size;
            go.name = "axis";
            return go;
        }

        public static GameObject Axis(Vector3 position, Quaternion rotation, float size, Transform parent)
        {
            GameObject axis = Axis(size);
            axis.transform.SetParent(parent, true);
            axis.transform.position = position;
            axis.transform.rotation = rotation;
            return axis;
        }

        public static GameObject Axis(WaySystem.Point point, float size, Transform parent)
        {
            return Axis(point.Position, point.Rotation, size, parent);
        }

        public static Transform CreatePrimitive(
            Color color, string name, 
            WaySystem.Point target, 
            float uniformScale = 0.7f, float transparency = 0.7f, 
            PrimitiveType primitiveType = PrimitiveType.Sphere)
        {
            return CreatePrimitive(
                primitiveType, color, transparency,
                target.Position, Vector3.one * uniformScale, target.Rotation, true, name);
        }


        public static Transform CreatePrimitive(
            PrimitiveType primitiveType, Color color, float transparency,
            WaySystem.Point target, Vector3 size, bool destroyCollider, string name)
        {
            return CreatePrimitive(
                primitiveType, color, transparency, 
                target.Position, size, target.Rotation, destroyCollider, name);
        }

        public static Transform CreatePrimitive(
            PrimitiveType primitiveType, Color color, float transparency,
            Vector3 position, Vector3 size, Quaternion rotation, bool destroyCollider, string name)
        {
            Color transparent = new Color(color.r, color.g, color.b, transparency);
            GameObject go = GameObject.CreatePrimitive(primitiveType);
            go.transform.position = position;
            go.transform.rotation = rotation;
            go.transform.localScale = size;
            go.gameObject.name = name;
            Material mt = Collection.DebugUnlitTransparent;
            Material copy = new Material(mt);
            go.GetComponent<Renderer>().sharedMaterial = copy;
            copy.color = transparent;
            //go.GetComponent<Renderer>().sharedMaterial.color = transparent;
            if (destroyCollider)
                GameObject.Destroy(go.GetComponent<Collider>());
            return go.transform;
        }

        public static GameObject BoxColliderCube(BoxCollider collider, Color color, bool sefDestruct)
        {
            if (collider == null)
            {
                UnityEngine.Debug.LogError("Can not draw box collider. Collider is null");
                return null;
            }

            GameObject go = new GameObject(collider.gameObject.name + "_debugCube");

            Matrix4x4 m = collider.transform.localToWorldMatrix;
            Vector3 position = m.MultiplyPoint(collider.center);
            Quaternion rotation = UberBehaviour.ExtractRotation(m);
            Vector3 scale = collider.size;
            go.transform.position = position;
            go.transform.rotation = rotation;
            go.transform.localScale = scale;

            MeshFilter mf = go.AddComponent<MeshFilter>();
            mf.mesh = Collection.Cube;

            MeshRenderer mr = go.AddComponent<MeshRenderer>();
            mr.material = Collection.DebugUnlitTransparent;
            mr.material.color = color;

            return go;
        }


        public static DebugCollection Collection
        {
            get
            {
                if (_collection == null)
                {
                    _collection = Resources.Load<DebugCollection>("DebugCollection");
                }

                return _collection;
            }
        }

        private static DebugCollection _collection;
    }

    public static class DebugColorCollection
    {
        public static readonly Color Maroon = new Color(0.5f, 0.0f, 0.0f);
        public static readonly Color DarkRed = new Color(0.54296875f, 0.0f, 0.0f);
        public static readonly Color Brown = new Color(0.64453125f, 0.1640625f, 0.1640625f);
        public static readonly Color Firebrick = new Color(0.6953125f, 0.1328125f, 0.1328125f);
        public static readonly Color Crimson = new Color(0.859375f, 0.078125f, 0.234375f);
        public static readonly Color Red = new Color(0.99609375f, 0.0f, 0.0f);
        public static readonly Color Tomato = new Color(0.99609375f, 0.38671875f, 0.27734375f);
        public static readonly Color Namecoral = new Color(0.99609375f, 0.49609375f, 0.3125f);
        public static readonly Color IndianRed = new Color(0.80078125f, 0.359375f, 0.359375f);
        public static readonly Color LightCoral = new Color(0.9375f, 0.5f, 0.5f);
        public static readonly Color DarkSalmon = new Color(0.91015625f, 0.5859375f, 0.4765625f);
        public static readonly Color Salmon = new Color(0.9765625f, 0.5f, 0.4453125f);
        public static readonly Color LightSalmon = new Color(0.99609375f, 0.625f, 0.4765625f);
        public static readonly Color OrangeRed = new Color(0.99609375f, 0.26953125f, 0.0f);
        public static readonly Color DarkOrange = new Color(0.99609375f, 0.546875f, 0.0f);
        public static readonly Color Orange = new Color(0.99609375f, 0.64453125f, 0.0f);
        public static readonly Color Gold = new Color(0.99609375f, 0.83984375f, 0.0f);
        public static readonly Color DarkGoldenRod = new Color(0.71875f, 0.5234375f, 0.04296875f);
        public static readonly Color GoldenRod = new Color(0.8515625f, 0.64453125f, 0.125f);
        public static readonly Color PaleGoldenRod = new Color(0.9296875f, 0.90625f, 0.6640625f);
        public static readonly Color DarkKhaki = new Color(0.73828125f, 0.71484375f, 0.41796875f);
        public static readonly Color Khaki = new Color(0.9375f, 0.8984375f, 0.546875f);
        public static readonly Color Olive = new Color(0.5f, 0.5f, 0.0f);
        public static readonly Color Yellow = new Color(0.99609375f, 0.99609375f, 0.0f);
        public static readonly Color YellowGreen = new Color(0.6015625f, 0.80078125f, 0.1953125f);
        public static readonly Color DarkOliveGreen = new Color(0.33203125f, 0.41796875f, 0.18359375f);
        public static readonly Color OliveDrab = new Color(0.41796875f, 0.5546875f, 0.13671875f);
        public static readonly Color LawnGreen = new Color(0.484375f, 0.984375f, 0.0f);
        public static readonly Color ChartReuse = new Color(0.49609375f, 0.99609375f, 0.0f);
        public static readonly Color GreenYellow = new Color(0.67578125f, 0.99609375f, 0.18359375f);
        public static readonly Color DarkGreen = new Color(0.0f, 0.390625f, 0.0f);
        public static readonly Color Green = new Color(0.0f, 0.5f, 0.0f);
        public static readonly Color ForestGreen = new Color(0.1328125f, 0.54296875f, 0.1328125f);
        public static readonly Color Lime = new Color(0.0f, 0.99609375f, 0.0f);
        public static readonly Color LimeGreen = new Color(0.1953125f, 0.80078125f, 0.1953125f);
        public static readonly Color LightGreen = new Color(0.5625f, 0.9296875f, 0.5625f);
        public static readonly Color PaleGreen = new Color(0.59375f, 0.98046875f, 0.59375f);
        public static readonly Color DarkSeaGreen = new Color(0.55859375f, 0.734375f, 0.55859375f);
        public static readonly Color MediumSpringGreen = new Color(0.0f, 0.9765625f, 0.6015625f);
        public static readonly Color SpringGreen = new Color(0.0f, 0.99609375f, 0.49609375f);
        public static readonly Color SeaGreen = new Color(0.1796875f, 0.54296875f, 0.33984375f);
        public static readonly Color MediumAquaMarine = new Color(0.3984375f, 0.80078125f, 0.6640625f);
        public static readonly Color MediumSeaGreen = new Color(0.234375f, 0.69921875f, 0.44140625f);
        public static readonly Color LightSeaGreen = new Color(0.125f, 0.6953125f, 0.6640625f);
        public static readonly Color DarkSlateGray = new Color(0.18359375f, 0.30859375f, 0.30859375f);
        public static readonly Color Teal = new Color(0.0f, 0.5f, 0.5f);
        public static readonly Color DarkCyan = new Color(0.0f, 0.54296875f, 0.54296875f);
        public static readonly Color Aqua = new Color(0.0f, 0.99609375f, 0.99609375f);
        public static readonly Color Cyan = new Color(0.0f, 0.99609375f, 0.99609375f);
        public static readonly Color LightCyan = new Color(0.875f, 0.99609375f, 0.99609375f);
        public static readonly Color DarkTurquoise = new Color(0.0f, 0.8046875f, 0.81640625f);
        public static readonly Color Turquoise = new Color(0.25f, 0.875f, 0.8125f);
        public static readonly Color MediumTurquoise = new Color(0.28125f, 0.81640625f, 0.796875f);
        public static readonly Color PaleTurquoise = new Color(0.68359375f, 0.9296875f, 0.9296875f);
        public static readonly Color AquaMarine = new Color(0.49609375f, 0.99609375f, 0.828125f);
        public static readonly Color PowderBlue = new Color(0.6875f, 0.875f, 0.8984375f);
        public static readonly Color CadetBlue = new Color(0.37109375f, 0.6171875f, 0.625f);
        public static readonly Color SteelBlue = new Color(0.2734375f, 0.5078125f, 0.703125f);
        public static readonly Color CornFlowerBlue = new Color(0.390625f, 0.58203125f, 0.92578125f);
        public static readonly Color DeepSkyBlue = new Color(0.0f, 0.74609375f, 0.99609375f);
        public static readonly Color DodgerBlue = new Color(0.1171875f, 0.5625f, 0.99609375f);
        public static readonly Color LightBlue = new Color(0.67578125f, 0.84375f, 0.8984375f);
        public static readonly Color SkyBlue = new Color(0.52734375f, 0.8046875f, 0.91796875f);
        public static readonly Color LightSkyBlue = new Color(0.52734375f, 0.8046875f, 0.9765625f);
        public static readonly Color MidnightBlue = new Color(0.09765625f, 0.09765625f, 0.4375f);
        public static readonly Color Navy = new Color(0.0f, 0.0f, 0.5f);
        public static readonly Color DarkBlue = new Color(0.0f, 0.0f, 0.54296875f);
        public static readonly Color MediumBlue = new Color(0.0f, 0.0f, 0.80078125f);
        public static readonly Color Blue = new Color(0.0f, 0.0f, 0.99609375f);
        public static readonly Color RoyalBlue = new Color(0.25390625f, 0.41015625f, 0.87890625f);
        public static readonly Color BlueViolet = new Color(0.5390625f, 0.16796875f, 0.8828125f);
        public static readonly Color Indigo = new Color(0.29296875f, 0.0f, 0.5078125f);
        public static readonly Color DarkSlateBlue = new Color(0.28125f, 0.23828125f, 0.54296875f);
        public static readonly Color SlateBlue = new Color(0.4140625f, 0.3515625f, 0.80078125f);
        public static readonly Color MediumSlateBlue = new Color(0.48046875f, 0.40625f, 0.9296875f);
        public static readonly Color MediumPurple = new Color(0.57421875f, 0.4375f, 0.85546875f);
        public static readonly Color DarkMagenta = new Color(0.54296875f, 0.0f, 0.54296875f);
        public static readonly Color DarkViolet = new Color(0.578125f, 0.0f, 0.82421875f);
        public static readonly Color DarkOrchid = new Color(0.59765625f, 0.1953125f, 0.796875f);
        public static readonly Color MediumOrchid = new Color(0.7265625f, 0.33203125f, 0.82421875f);
        public static readonly Color Purple = new Color(0.5f, 0.0f, 0.5f);
        public static readonly Color Thistle = new Color(0.84375f, 0.74609375f, 0.84375f);
        public static readonly Color Plum = new Color(0.86328125f, 0.625f, 0.86328125f);
        public static readonly Color Violet = new Color(0.9296875f, 0.5078125f, 0.9296875f);
        public static readonly Color Magenta = new Color(0.99609375f, 0.0f, 0.99609375f);
        public static readonly Color Orchid = new Color(0.8515625f, 0.4375f, 0.8359375f);
        public static readonly Color MediumVioletRed = new Color(0.77734375f, 0.08203125f, 0.51953125f);
        public static readonly Color PaleVioletRed = new Color(0.85546875f, 0.4375f, 0.57421875f);
        public static readonly Color DeepPink = new Color(0.99609375f, 0.078125f, 0.57421875f);
        public static readonly Color HotPink = new Color(0.99609375f, 0.41015625f, 0.703125f);
        public static readonly Color LightPink = new Color(0.99609375f, 0.7109375f, 0.75390625f);
        public static readonly Color Pink = new Color(0.99609375f, 0.75f, 0.79296875f);
        public static readonly Color AntiqueWhite = new Color(0.9765625f, 0.91796875f, 0.83984375f);
        public static readonly Color Beige = new Color(0.95703125f, 0.95703125f, 0.859375f);
        public static readonly Color Bisque = new Color(0.99609375f, 0.890625f, 0.765625f);
        public static readonly Color BlanchedAlmond = new Color(0.99609375f, 0.91796875f, 0.80078125f);
        public static readonly Color Wheat = new Color(0.95703125f, 0.8671875f, 0.69921875f);
        public static readonly Color CornSilk = new Color(0.99609375f, 0.96875f, 0.859375f);
        public static readonly Color LemonChiffon = new Color(0.99609375f, 0.9765625f, 0.80078125f);
        public static readonly Color LightGoldenRodYellow = new Color(0.9765625f, 0.9765625f, 0.8203125f);
        public static readonly Color LightYellow = new Color(0.99609375f, 0.99609375f, 0.875f);
        public static readonly Color SaddleBrown = new Color(0.54296875f, 0.26953125f, 0.07421875f);
        public static readonly Color Sienna = new Color(0.625f, 0.3203125f, 0.17578125f);
        public static readonly Color Chocolate = new Color(0.8203125f, 0.41015625f, 0.1171875f);
        public static readonly Color Peru = new Color(0.80078125f, 0.51953125f, 0.24609375f);
        public static readonly Color SandyBrown = new Color(0.953125f, 0.640625f, 0.375f);
        public static readonly Color BurlyWood = new Color(0.8671875f, 0.71875f, 0.52734375f);
        public static readonly Color Tan = new Color(0.8203125f, 0.703125f, 0.546875f);
        public static readonly Color RosyBrown = new Color(0.734375f, 0.55859375f, 0.55859375f);
        public static readonly Color Moccasin = new Color(0.99609375f, 0.890625f, 0.70703125f);
        public static readonly Color NavajoWhite = new Color(0.99609375f, 0.8671875f, 0.67578125f);
        public static readonly Color PeachPuff = new Color(0.99609375f, 0.8515625f, 0.72265625f);
        public static readonly Color MistyRose = new Color(0.99609375f, 0.890625f, 0.87890625f);
        public static readonly Color LavenderBlush = new Color(0.99609375f, 0.9375f, 0.95703125f);
        public static readonly Color Linen = new Color(0.9765625f, 0.9375f, 0.8984375f);
        public static readonly Color OldLace = new Color(0.98828125f, 0.95703125f, 0.8984375f);
        public static readonly Color PapayaWhip = new Color(0.99609375f, 0.93359375f, 0.83203125f);
        public static readonly Color SeaShell = new Color(0.99609375f, 0.95703125f, 0.9296875f);
        public static readonly Color MintCream = new Color(0.95703125f, 0.99609375f, 0.9765625f);
        public static readonly Color SlateGray = new Color(0.4375f, 0.5f, 0.5625f);
        public static readonly Color LightSlateGray = new Color(0.46484375f, 0.53125f, 0.59765625f);
        public static readonly Color LightTeelBlue = new Color(0.6875f, 0.765625f, 0.8671875f);
        public static readonly Color Lavender = new Color(0.8984375f, 0.8984375f, 0.9765625f);
        public static readonly Color FloralWhite = new Color(0.99609375f, 0.9765625f, 0.9375f);
        public static readonly Color AliceBlue = new Color(0.9375f, 0.96875f, 0.99609375f);
        public static readonly Color GhostWhite = new Color(0.96875f, 0.96875f, 0.99609375f);
        public static readonly Color Honeydew = new Color(0.9375f, 0.99609375f, 0.9375f);
        public static readonly Color Ivory = new Color(0.99609375f, 0.99609375f, 0.9375f);
        public static readonly Color Azure = new Color(0.9375f, 0.99609375f, 0.99609375f);
        public static readonly Color Snow = new Color(0.99609375f, 0.9765625f, 0.9765625f);
        public static readonly Color Black = new Color(0.0f, 0.0f, 0.0f);
        public static readonly Color DimGrey = new Color(0.41015625f, 0.41015625f, 0.41015625f);
        public static readonly Color Grey = new Color(0.5f, 0.5f, 0.5f);
        public static readonly Color DarkGrey = new Color(0.66015625f, 0.66015625f, 0.66015625f);
        public static readonly Color Silver = new Color(0.75f, 0.75f, 0.75f);
        public static readonly Color LightGrey = new Color(0.82421875f, 0.82421875f, 0.82421875f);
        public static readonly Color Gainsboro = new Color(0.859375f, 0.859375f, 0.859375f);
        public static readonly Color WhiteSmoke = new Color(0.95703125f, 0.95703125f, 0.95703125f);
        public static readonly Color White = new Color(0.99609375f, 0.99609375f, 0.99609375f);

        private static List<Color> Colors = new List<Color>();

        public static Color RandomColor 
        { 
            get 
            {
                if (Colors == null)
                    Colors = new List<Color>();
                if (Colors.Count == 0)
                {
                    var props = typeof(DebugColorCollection).GetFields();
                    foreach (var prop in props)
                    {
                        var value = prop.GetValue(null);
                        Colors.Add((Color)value);
                    }
                }
                return Colors[UnityEngine.Random.Range(0, Colors.Count)];
            }
        }
    }
}



