﻿using System.Collections.Generic;

namespace TutorialFlipper
{
    public enum TutorialElements { FlipperBasic, FirstRide }

    public class TutorialManager
    {
		public static TutorialManager Init()
		{
			return new TutorialManager();
		}

        public bool IsElementDone (TutorialElements element)
        {
            return this.elements[element];
        }

        private List<string> stringElements;
        private Dictionary<TutorialElements, bool> elements;

        private TutorialManager()
		{
            //грузим из профиля
            stringElements = 
                GameStarter.Instance.PlayerManager.Data.GetList<string>(
                    GameStarter.GlobalValues.Tutorial_Elements.ToString());

            if (stringElements == null)
                stringElements = new List<string>();

            elements = new Dictionary<TutorialElements, bool>();
            foreach (TutorialElements tp in System.Enum.GetValues(typeof(TutorialElements)))
            {
                elements.Add(tp, stringElements.Contains(tp.ToString()));
            }
        }

        public void Save(TutorialElements tp)
        {
            stringElements.Add(tp.ToString());
            GameStarter.Instance.PlayerManager.SetTutorialElementDone(stringElements);
        }
	}
}