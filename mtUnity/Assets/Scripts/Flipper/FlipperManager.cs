﻿using MTCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using WaySystem;
using WaySystem.Collection;

namespace MTTown
{
    public class FlipperManager
	{
		private delegate WaySystem.WayRootFlipper Kreate(WaySystem.Kreator.Kreator kreator);

		public static FlipperManager Init()
		{
			return new FlipperManager();
		}


		public async Task LoadFlipperByTrip(Trip trip)
		{
			//показваем сплешъ
			GameStarter.Instance.UIRoot.Splash.Show();
            //прежде всего грузим поезд - без поезда флиппер бесполезен
            if(!GameStarter.Instance.TrainManager.WasCreated)
            {
                await GameStarter.Instance.TrainManager.CreateTrainFromDraft();
            }

			GameStarter.Instance.Train.OnFlipperExit = async (results) => { await ExitFlipper(results, trip.EndPoint); };

			GameStarter.Instance.Train.OnFlipperRelaunch = async () => { await ContinueFlipper(); }; 

			await GameStarter.SceneManager.LoadScene("empty", async ()=>
			{
				WayRootFlipper wayRoot = await CreateWayRootFlipper(trip);
				Go(wayRoot);
                AssetBundle.UnloadAllAssetBundles(false);
            });
		}

		private void Go(WayRootFlipper wayRoot)
		{
			//камера
			wayRoot.CamDock.SetCamera();

			GameStarter.Instance.SplitFlapDisplay.transform.position = Vector3.up * 5000f;
			GameStarter.Instance.SplitFlapDisplay.ClickBLockManager.IsTown = false;
			GameStarter.Instance.Train.SetForFlipper(wayRoot);

			GameStarter.Instance.UIRoot.Town.Reset();
			GameStarter.Instance.UIRoot.Flipper.Prepare(wayRoot);

			GameStarter.Instance.UIRoot.Splash.SetLoaded(true);

			GameStarter.Instance.UIRoot.Splash.OnCommandHide = () =>
			{
				GameStarter.Instance.Train.StartEngine();
                GameStarter.Instance.Train.Go(Trains.Train.EngineSpeedState.SavedConstant, false, 1f);
				GameStarter.Instance.MainComponents.SoundManager.Music.Play();
				GameStarter.Instance.UIRoot.Splash.OnCommandHide = null;
                GameStarter.Instance.UIRoot.Flipper.Show();
            };
			GameStarter.Instance.GamePlayManager.RunCheckers();
		}


		//загрузка коллекции
		private async Task<WaySystem.Collection.Collection> LoadCollection()
		{
			//Загрузка объекта-коллекции чанков и настроек
			UnityEngine.Object railsObj =
				await GameStarter.Instance.ResourceManager.LoadByBundleName(MTAssetBundles.holders_collection);
			if (railsObj == null)
			{
				Debug.LogError("Rails storage is not loaded");
				return null;
			}

			WaySystem.Collection.Collection collection = railsObj as WaySystem.Collection.Collection;
			return collection;
		}

		private async Task<WayRootFlipper> CreateWayRootFlipper(Trip trip)
		{
            Collection collection = await LoadCollection();

            MTParameters.Kreator.Kreator kreatorParameters =
                GameStarter.GameParameters.Kreator[(int)trip.Size];

            MTParameters.Flipper.TripContent tripContent =
                GameStarter.GameParameters.GetRandomTripContent(trip.Size);

            WaySystem.Kreator.Kreator kreator = 
				new WaySystem.Kreator.Kreator(
					collection,
					GameStarter.Instance.TownManager.passengersSO,
					kreatorParameters,
					GameStarter.GameParameters.FlipperParameters,
					GameStarter.Instance.Train.CeilIntLength);

            WayRootFlipper wayRoot =
                         kreator.CreateFlipper(tripContent, trip.Resourses);
			return wayRoot;
        }

        private async Task ExitFlipper(Dictionary<int, int> results, int exitTownIndex)
		{
            GameStarter.Instance.UIRoot.Splash.Show();
            GameStarter.Instance.SplitFlapDisplay.CloseAll(true);
			

            MTParameters.Town townParameters = GameStarter.GameParameters.Towns[exitTownIndex];
            await GameStarter.SceneManager.LoadScene("empty", async () =>
            {
                _ = await GameStarter.Instance.TownManager.LoadTown(townParameters, true);
                AssetBundle.UnloadAllAssetBundles(false);
            });
        }

		private async Task ContinueFlipper()
		{
            //показваем сплешъ
            GameStarter.Instance.UIRoot.Splash.Show();

            await GameStarter.SceneManager.LoadScene("empty", async () =>
            {
                MTCore.Trip trip = GameStarter.Instance.TownDataManager.WaysDispatcher.GetCurrentTrip();
                WayRootFlipper wayRoot = await CreateWayRootFlipper(trip);
                Go(wayRoot);
                AssetBundle.UnloadAllAssetBundles(false);
            });
        }
	}
}