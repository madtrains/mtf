﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Trains
{


	[CustomEditor(typeof(Visual))]
	public class VisualEditor : Editor
	{
		public void OnSceneGUI()
		{
			Visual visual = target as Trains.Visual;
			Handles.Label(visual.transform.position, visual.name);
			Vector3 end = Vector3.forward * visual.RearOffset * -1;
			DrawLine(Color.yellow, end, visual.transform);

			Vector3 forward = Vector3.forward * visual.FrontOffset;
			DrawLine(Color.yellow, forward, visual.transform);

			Vector3 half = Vector3.forward * (visual.Length / 2f);
			DrawLine(Color.red, half, visual.transform);
			DrawLine(Color.red, half * -1f, visual.transform);
		}


		private void DrawLine(Color color, Vector3 vector, Transform tr)
		{
			Vector3 v = tr.TransformPoint(vector);
			Handles.color = color;
			Handles.SphereHandleCap(0,
				v,
				Quaternion.identity,
				0.2f,
				EventType.Repaint);

			Handles.ArrowHandleCap(0,
					v,
					tr.rotation * Quaternion.Euler(-90f, 0f, 0f),
					1.5f,
					EventType.Repaint);

		}
	}
}