﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(Trains.Coach))]
public class CoachEditor : Editor
{
	public void OnSceneGUI()
	{
		Trains.Coach coach = target as Trains.Coach;
	}
}


[CustomEditor(typeof(Trains.Train))]
public class TrainEditor : Editor
{
	public void OnSceneGUI()
	{
		Trains.Train train = target as Trains.Train;
	}
}