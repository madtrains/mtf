﻿using MTPlayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Trains
{
    public class TrainManager
	{
		public static List<MTPlayer.CoachInfo> StartCoaches 
		{
			get
			{
                List<MTPlayer.CoachInfo> coaches = new List<MTPlayer.CoachInfo>() 
				{ 
					new MTPlayer.CoachInfo() 
					{ 
							Id = 1000, 
							ColorId = 0 
					} 
				};
				return coaches;
            }
		}

		public static MTPlayer.TrainInfo StartLocomotive
		{
			get
			{
				TrainInfo result = new MTPlayer.TrainInfo()
				{
					Id = 0
				};
				return result;
            }
		}

        public TrainAccount Account { get { return account; } }
		public TrainPull Pull { get { return trainPull; } }

		private TrainAccount account;
        private TrainPull trainPull;


		private bool wasCreated;
        private Train train;
		/// <summary>
		/// все возможные в игре вагоны (объект, под который встаёт Visual), 
		/// количество определяется в параметрах
		/// </summary>
		private List<Coach> allPossibleCoaches;  //все возможные в игре вагоны
        /// <summary>
        /// все используемые на данный момент вагоны
        /// </summary>
        private List<Coach> usedCoaches; //используемые поездом вагоны

        private TrainManager()
        {
            train = CreateTrainGO();
            allPossibleCoaches = new List<Coach>();
            for (int i = 0; i < GameStarter.GameParameters.CoachesNumber; i++)
            {
                allPossibleCoaches.Add(CreateCoachGO(i + 1));
            }

            train.Init();
			this.account = new TrainAccount();
            this.trainPull = new TrainPull();
        }

        public bool WasCreated { get { return wasCreated; } }

        public Train Train { get { return train; } }

        /// <summary>
        /// все используемые поездом вагоны, включая тендер
        /// </summary>
        public List<Coach> UsedCoaches { get { return usedCoaches; } }

        public int LocomotiveID { get { return train.TrainParameters.ID; } }
        #region Player Data
        
        public bool IsLocomotiveBought(int id)
		{
			int[] trains = GameStarter.Instance.PlayerManager.GetTrains();
			return trains.Contains(id);
			//int[] coaches = GameStarter.Instance.PlayerManager.GetActiveCoaches();
		}

        public bool IsCoachBought(int id)
        {
            return account.Coaches.Any(c => c.Id == id);
        }

        public bool CanBuyTrain(int id)
		{
			if (GameStarter.Instance.MarketManager.TryGetTrainInfo(id, MTTown.MTMarket.MarketCurrency.MTC, out var info))
			{
				int playerPaymentAbility = GameStarter.Instance.PlayerManager.ResourceGetAmount(info.Currency);
				return playerPaymentAbility >= info.Price;
			}
			return false;
		}

		public bool CanBuyCoach(int id)
		{
			if (GameStarter.Instance.MarketManager.TryGetCoachInfo(id, MTTown.MTMarket.MarketCurrency.MTC, out var info))
			{
				int playerPaymentAbility = GameStarter.Instance.PlayerManager.ResourceGetAmount(info.Currency);
				return playerPaymentAbility >= info.Price;
			}
			return false;
		}	
		#endregion

		#region Coupling
		public void SwapLoco(bool reversed)
		{
			int newLocoID = CycleIDs(account.ActiveLocomotive.Id, account.Locomotives, reversed);
            TrainInfo newTrainInfo = new TrainInfo() { Id = newLocoID };
            CreateTrain(newTrainInfo, account.ActiveCoaches, true);
		}

        /// <summary>
        /// меняет вагон в указанной позиции на следующий оставшийся из пула
        /// </summary>
        /// <param name="coachIndex"></param>
        /// <param name="reversed"></param>
        public void SwapCoach(int coachIndex, bool reversed)
        {
            List<MTPlayer.CoachInfo> newCoaches = account.ActiveCoaches;
            if (train.TrainParameters.HasIndispensableCoach)
                coachIndex -= 1;

            var inactiveCoaches = account.GetInactiveCoaches();
            var notNullCoaches = inactiveCoaches.Where(n=>n.Value>0).Select(k=>k.Key).ToList();
            if (notNullCoaches.Count == 0) 
                return;

            int newCoachID = CycleIDs(
                                account.ActiveCoaches[coachIndex].Id,
                                notNullCoaches,
                                reversed);

            newCoaches[coachIndex] = new CoachInfo() { Id = newCoachID, ColorId = 0 };
            CreateTrain(account.ActiveLocomotive, newCoaches, true);
        }

        //добавляет вагон с хвоста из сохранённых вагонов
        public void AddCoach()
		{
            List<MTPlayer.CoachInfo> newCoaches = account.ActiveCoaches;
            var inactiveCoaches = account.GetInactiveCoaches();
            var notNullCoaches = inactiveCoaches.Where(n => n.Value > 0).Select(k => k.Key).ToList();
            if (notNullCoaches.Count <= 0)
                return;
            int newCoachID = notNullCoaches[0]; 
            newCoaches.Add(new CoachInfo() { Id = newCoachID, ColorId = 0 });
            CreateTrain(account.ActiveLocomotive, newCoaches, true);
        }

		public void ColorizeCoach(int coachAbsIndex, int coachRelativeIndex, int colorIndex, Color color)
		{
            Coach coach = this.usedCoaches[coachAbsIndex];
			coach.Visual.Colorize(color);
            account.ColorizeAndSaveCoach(coachRelativeIndex, colorIndex);
            Debug.LogFormat(
                "Coach: {0} set color: [{1}]. Saved to CoachInfo with Id: {2}", 
                this.usedCoaches[coachAbsIndex].Parameters.Name, 
                colorIndex, account.ActiveCoaches[coachRelativeIndex].Id);
		}

		/// <summary>
		/// Удаляет вагон на указанной позиции где 0 - первый вагон (включая тендер)
		/// </summary>
		/// <param name="coachIndex">индекс вагона</param>
		/// <param name="preventLastCoachRemoval">сохранить один вагон</param>
		/// <returns>true если удаление было успешно</returns>
		public bool Remove(int coachIndex, bool preventLastCoachRemoval=true)
		{
			if (train.TrainParameters.HasIndispensableCoach)
				coachIndex -= 1;

			if (preventLastCoachRemoval)
			{
				//с тендером минимальное количество вагонов - 2
				if (train.TrainParameters.HasIndispensableCoach)
				{
					if (train.Coaches.Count == 2)
						return false;
				}
                //без тендера должен остаться один вагон минимальное количество вагонов - 1
                else
                {
                    if (train.Coaches.Count == 1)
                        return false;
                }
			}
            List<MTPlayer.CoachInfo> newCoaches = account.ActiveCoaches;
            newCoaches.RemoveAt(coachIndex);
            CreateTrain(account.ActiveLocomotive, newCoaches, true);
			return true;
		}
		#endregion

		#region Init
		public static async Task<TrainManager> Init()
		{
			var tm = new TrainManager();
			await tm.LoadTrainAuxBundles();
			return tm;
		}

		/// <summary>
		/// Вызывается один раз, при запуске игры
		/// </summary>
		
		#endregion

		#region Get/Load
		private async Task LoadTrainAuxBundles()
		{
            MTSound.SoundsTrain soundsTrain =
                await GameStarter.Instance.ResourceManager.InstantiateByBundleName<MTSound.SoundsTrain>(MTAssetBundles.sound_soundstrain);
            soundsTrain.Init();
			GameStarter.Instance.MainComponents.SoundManager.Train = soundsTrain;

			Horn horn =
				await GameStarter.Instance.ResourceManager.InstantiateByBundleName<Horn>(MTAssetBundles.misc_horn);
			train.AssignBaseComponents(soundsTrain, horn);
		}
        #endregion

        #region CreateTrain
        /// <summary>
        /// Создание поезда с нуля
        /// </summary>
        /// <returns></returns>
        public async Task CreateTrainFromDraft()
        {
            TrainInfo playerActiveTrain = GameStarter.Instance.PlayerManager.GetActiveTrain();
			//int trainId = playerActiveTrain?.Id ?? 0;
			TrainInfo trainInfo = playerActiveTrain != null ? playerActiveTrain : StartLocomotive;
            List<MTPlayer.CoachInfo> playerActiveCoaches = GameStarter.Instance.PlayerManager.GetActiveCoaches();
            List<MTPlayer.CoachInfo> coaches = playerActiveCoaches == null ?
                StartCoaches : playerActiveCoaches;
            //var coachIds = coaches == null ? new int[] { 1000 } : coaches.Select(c => c.Id).ToArray();
            await CreateDownloadTrain(trainInfo, coaches, false);
        }

        private async Task CreateDownloadTrain(MTPlayer.TrainInfo trainInfo, List<MTPlayer.CoachInfo> coachInfos, bool refreshPosition)
        {
			await trainPull.GetLoadLocomotive(trainInfo.Id);
			foreach(MTPlayer.CoachInfo coachInfo in coachInfos)
			{
				await trainPull.GetLoadCoach(coachInfo.Id);
			}
            CreateTrain(trainInfo, coachInfos, refreshPosition);
        }

        private void CreateTrain(MTPlayer.TrainInfo trainInfo, List<MTPlayer.CoachInfo> coachInfos, bool refreshPosition, bool saveToAccount=true)
		{
			train.DeleteVisual();
			Visual trainVisual = trainPull.GetLocomotive(trainInfo.Id);
			train.AssignParentVisual(trainVisual, true);
			train.TrainParameters = trainPull.GetTrainParameters(trainInfo.Id);
			float offset = trainVisual.Length / 2f;

			usedCoaches = new List<Coach>();
			for (int i = 0, a = 0; i < GameStarter.GameParameters.CoachesNumber; i++)
			{
				float half = 0f;
				Coach currentCoach = allPossibleCoaches[i];
				currentCoach.DeleteVisual();

				//тендер
				if (i == 0 && train.TrainParameters.HasIndispensableCoach)
				{
					Visual indispensableVisual = trainPull.GetIndispensable(trainInfo.Id);
                    currentCoach.ActiveSelf = true;
					currentCoach.Init(train, null, indispensableVisual);
					half = indispensableVisual.Length / 2f;
					offset += half;
					currentCoach.LocomotiveOffset = offset;
					offset += half;
					usedCoaches.Add(currentCoach);
				}

				//любой другой вагон, что активен
				else if (coachInfos.Count > a)
				{
					CoachInfo coachInfo = coachInfos[a];
					MTParameters.Coach coachParameters = trainPull.GetСoachParameters(coachInfo.Id);
					Visual coachVisual = trainPull.GetCoach(coachInfo.Id);
					currentCoach.ActiveSelf = true;
					Visual cloned = 
                        currentCoach.Init(train, coachParameters, coachVisual);
                    cloned.Colorize(GameStarter.GameParameters.Customization.GetColor(coachInfo.ColorId));
					half = coachVisual.Length / 2f;
					offset += half;
					currentCoach.LocomotiveOffset = offset;
					offset += half;
					usedCoaches.Add(currentCoach);
					a += 1;
				}
				//неактивные вагоны, им удаляем визуал
				else
				{
					currentCoach.ActiveSelf = false;
					currentCoach.DeleteVisual();
				}
			}

			train.Coaches = usedCoaches;

			if (refreshPosition)
				train.RefreshCoachesPositionsByHead();
            wasCreated = true;

            if (!saveToAccount)
                return;
			train.RecalculateCoaches();
            account.SaveActiveTrain(trainInfo, coachInfos);
            //GameStarter.Instance.PlayerManager.SetActiveTrain(Train.TrainParameters.ID);
            //GameStarter.Instance.PlayerManager.SetActiveCoaches(CoachesIDs);
        }
        #endregion

        #region IDs, Collections



        private int CycleIDs(int currentID, List<int> pull, bool reversed)
        {
            if (pull.Count == 1)
                return pull[0];

            int index = pull.IndexOf(currentID);
            if (index < 0)
                return pull[0];

            //прямой порядок
            if (!reversed)
            {
                int next = index + 1;
                if (next < pull.Count)
                    return pull[next];
                else
                    return pull[0];
            }
            //обратный порядок
            else
            {
                int next = index - 1;
                if (next >= 0)
                    return pull[next];
                else
                    return pull[pull.Count - 1];
            }
        }
        #endregion

        #region Buy

        public void BuyLocomotive(MTParameters.Train trainParameters)
		{
			if(GameStarter.Instance.MarketManager.TryGetTrainInfo(trainParameters.ID, MTTown.MTMarket.MarketCurrency.MTC, out var info))
			{
				if(GameStarter.Instance.PlayerManager.TryReduceResource(info.Currency, info.Price))
				{
					GameStarter.Instance.PlayerManager.AddTrain(trainParameters.ID);
                    account.UpdateProfile();
                }
			}
		}

		public void BuyCoach(MTParameters.Coach coachParameters)
		{
			if (GameStarter.Instance.MarketManager.TryGetCoachInfo(coachParameters.ID, MTTown.MTMarket.MarketCurrency.MTC, out var info))
			{
				if (GameStarter.Instance.PlayerManager.TryReduceResource(info.Currency, info.Price))
				{
					GameStarter.Instance.PlayerManager.SaveCoach(coachParameters);
                    account.UpdateProfile();
                }
			}
		}
        #endregion

        #region GameObjects
        private Train CreateTrainGO()
        {
            GameObject go = new GameObject("TheTrain");
            Train trainComponent = go.AddComponent<Train>();
            trainComponent.CreateMovingPoint();

            go.transform.parent = GameStarter.Instance.transform;
            go.transform.position = Vector3.forward;

            #region Debug
            GameObject tap1 = new GameObject("tap1");
            tap1.transform.parent = GameStarter.Instance.transform;
            GameObject tap2 = new GameObject("tap2");
            tap2.transform.parent = GameStarter.Instance.transform;
            #endregion

            trainComponent.CreateGameObjects();
            return trainComponent;
        }

        private Coach CreateCoachGO(int number)
        {
            StringBuilder name = new StringBuilder();
            name.Append("Coach_");
            name.Append(number);
            GameObject go = new GameObject(name.ToString());
            Coach coachComponent = go.AddComponent<Coach>();
            coachComponent.CreateGameObjects();
            coachComponent.CreateMovingPoint();
            go.SetActive(false);
            go.transform.parent = GameStarter.Instance.transform;
            go.transform.position = Vector3.forward * number * -5f;
            return coachComponent;
        }
        #endregion
    }

	
    public class TrainPull
	{
		public TrainPull()
		{
            visualRoot = new GameObject("Visuals").transform;
            visualRoot.SetParent(GameStarter.Instance.transform, true);
            visualRoot.transform.position = Vector3.up * 5000f;

            locomotives = new Dictionary<int, Visual>();
			indispensables = new Dictionary<int, Visual>();
			coaches = new Dictionary<int, Visual>();

			trainsParameters = new Dictionary<int, MTParameters.Train>();
			coachesParameters = new Dictionary<int, MTParameters.Coach>();

            for (int i = 0; i < GameStarter.GameParameters.Trains.Length; i++)
            {
                MTParameters.Train trainParameters = GameStarter.GameParameters.Trains[i];
                trainsParameters.Add(trainParameters.ID, trainParameters);
            }
            for (int i = 0; i < GameStarter.GameParameters.Coaches.Length; i++)
            {
                MTParameters.Coach coachParameters = GameStarter.GameParameters.Coaches[i];
                coachesParameters.Add(coachParameters.ID, coachParameters);
            }
        }

        private Dictionary<int, Visual> locomotives;
        private Dictionary<int, Visual> indispensables;
        private Dictionary<int, Visual> coaches;

        private Dictionary<int, MTParameters.Train> trainsParameters;
        private Dictionary<int, MTParameters.Coach> coachesParameters;
        private Transform visualRoot;

        /// <summary>
        /// запрос на скачанную модель локомотива, без проверки
        /// </summary>
        public Visual GetLocomotive(int id)
        {
            return locomotives[id];
        }

		public Visual GetIndispensable(int id)
		{
			return indispensables[id];
		}

        /// <summary>
        /// запрос на скачанную модель вагона, без проверки
        /// </summary>
        public Visual GetCoach(int id)
        {
            return coaches[id];
        }

        /// <summary>
        /// грузим только те локомотивы и вагоны, что пробиты для этого города
        /// </summary>
        /// <returns></returns>
        public async Task LoadAllTownLocosNCoaches(MTParameters.Town townParams)
        {
            foreach (MTParameters.Train loco in GameStarter.GameParameters.Trains)
            {
                if (townParams.DepotIDs.Contains<int>(loco.ID))
                {
                    await GetLoadLocomotive(loco.ID);
                }
            }

            foreach (MTParameters.Coach coach in GameStarter.GameParameters.Coaches)
            {
                if (townParams.DepotIDs.Contains<int>(coach.ID))
                {
                    await GetLoadCoach(coach.ID);
                }
            }
        }

        public async Task<Visual> GetLoadLocomotive(int id)
        {
            if (locomotives.ContainsKey(id))
            {
                return locomotives[id];
            }
            else
            {
                Visual locoVisual =
                    await LoadVisual(trainsParameters[id].MainBundle);
                locomotives.Add(id, locoVisual);

                //тендер или спаренный локомотив
                if (trainsParameters[id].HasIndispensableCoach)
                {
                    Visual indispensableVisual =
                        await LoadVisual(trainsParameters[id].IndispensableCoach);
                    indispensables.Add(id, indispensableVisual);
                }
                return locoVisual;
            }
        }

        public async Task<Visual> GetLoadCoach(int id)
        {
            if (coaches.ContainsKey(id))
            {
                return coaches[id];
            }
            else
            {
                Visual coachVisual =
                    await LoadVisual(coachesParameters[id].MainBundle);
                coaches.Add(id, coachVisual);
                return coachVisual;
            }
        }

            
        /// <summary>
        /// парамсы поездов
        /// </summary>
        public MTParameters.Train GetTrainParameters(int id)
        {
            return trainsParameters[id];
        }

        /// <summary>
        /// парамсы вагонов
        /// </summary>
        public MTParameters.Coach GetСoachParameters(int id)
        {
            return coachesParameters[id];
        }


        private async Task<Visual> LoadVisual(string prefabBundleName)
        {
            UnityEngine.Object obj = await GameStarter.Instance.ResourceManager.LoadByBundleName(prefabBundleName);
            GameObject go = obj as GameObject;
            GameObject instantiatedGO = GameObject.Instantiate(go, Vector3.up * 5000, Quaternion.identity);
            Visual visual = instantiatedGO.GetComponent<Visual>();
            visual.transform.SetParent(visualRoot, false);
            UberBehaviour.Activate(visual.gameObject, false);
            return visual;
        }
    }

    public class TrainAccount
    {
        public TrainInfo ActiveLocomotive { get { return activeLocomotive; } }
        public List<MTPlayer.CoachInfo> ActiveCoaches { get { return activeCoaches; } }
        public List<int> Locomotives { get { return locomotives; } }
        public List<CoachDepoInfo> Coaches { get { return coaches; } }

        public TrainAccount()
        {
            GetData();
            GetActiveData();
        }

        /// <summary>
        /// все купленные игроком локомотивы
        /// </summary>
        private List<int> locomotives;

        /// <summary>
        /// текущий локомотив
        /// </summary>
        private TrainInfo activeLocomotive;

        /// <summary>
        /// все вагоны купленые игроком
        /// </summary>
        private List<MTPlayer.CoachDepoInfo> coaches;
        private Dictionary<int, MTPlayer.CoachDepoInfo> coachesDict;
        /// <summary>
        /// id, number
        /// </summary>
        private Dictionary<int, int> inactiveCoaches;

        /// <summary>
        /// текущий состав
        /// </summary>
        private List<MTPlayer.CoachInfo> activeCoaches;

        public void UpdateProfile()
        {
            GetData();
        }

        public List<int> ActiveCoachesIDs
        {
            get
            {
                return activeCoaches.Select(sl => sl.Id).ToList() ;
            }
        }

        public List<int> CoachesIDs
        {
            get
            {
                return coaches.Select(sl => sl.Id).ToList();
            }
        }

        public Dictionary<int, int> GetInactiveCoaches()
        {
            
            inactiveCoaches = new Dictionary<int, int>();
            if (coaches == null || activeCoaches == null)
                return inactiveCoaches;

            foreach (CoachDepoInfo coach in coaches)
            {
                inactiveCoaches.Add(coach.Id, coach.Number);
            }
            foreach (CoachInfo activeCoach in activeCoaches)
            {
                inactiveCoaches[activeCoach.Id] -= 1;
            }
            return inactiveCoaches;
        }

        public void SaveActiveTrain(TrainInfo activeLocomotive, List<MTPlayer.CoachInfo> activeCoaches)
		{
			this.activeLocomotive = activeLocomotive;
			this.activeCoaches = activeCoaches;
			GameStarter.Instance.PlayerManager.SetActiveTrain(activeLocomotive.Id);
			GameStarter.Instance.PlayerManager.SetActiveCoaches(activeCoaches.ToArray());
        }

        public void ColorizeAndSaveCoach(int index, int colorIndex)
        {
            activeCoaches[index].ColorId = colorIndex;
            GameStarter.Instance.PlayerManager.SetActiveCoaches(activeCoaches.ToArray());
        }

        public CoachDepoInfo GetCoachDepoInfo(int id)
        {
            return coachesDict.ContainsKey(id) ? coachesDict[id] : null;
        }

        private void GetData()
        {
            locomotives = GameStarter.Instance.PlayerManager.GetTrains()?.ToList()?? new List<int>();
            coaches = GameStarter.Instance.PlayerManager.GetCoachesList();
            coachesDict = new Dictionary<int, CoachDepoInfo>();
            foreach(CoachDepoInfo coachDepoInfo in coaches)
            {
                coachesDict.Add(coachDepoInfo.Id, coachDepoInfo);
            }
        }

        private void GetActiveData()
        {
            activeLocomotive = GameStarter.Instance.PlayerManager.GetActiveTrain();
            activeCoaches = GameStarter.Instance.PlayerManager.GetActiveCoaches();
            GetInactiveCoaches();
        }
    }
}
