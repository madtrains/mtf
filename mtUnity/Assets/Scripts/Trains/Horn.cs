﻿using MTCore;
using MTRails;
using UnityEngine;
using WaySystem.Cargos;

namespace Trains
{
    public class Horn : UberBehaviour
    {
        public UnityEngine.Events.UnityAction<RRCollision, Collision> OnRRCollision;
        public UnityEngine.Events.UnityAction<CargoGenerators> OnCargoGenerators;
        public UnityEngine.Events.UnityAction<RRCollision> OnRRTriggerEnter;
        public UnityEngine.Events.UnityAction<RRCollision> OnRRTriggerExit;

        public bool On { get { return _on; } }
        [SerializeField] private ParticleSystem particles;
        [SerializeField] private GameObject colider;
        [SerializeField] private AudioSource horn;
        [SerializeField] private AudioSource hornEmpty;
        [SerializeField] private AudioSource noHorn;


        private bool _on;

        private void Awake()
        {
            this.colider.SetActive(false);
        }

        public void Switch(bool value)
        {
            UberBehaviour.Activate(colider, value);
            _on = value;
            if (value)
            {
                horn.Play();
                particles.Play();
				GameStarter.Instance.GamePlayManager.UpdateCollection(MTDataCollection.Horn_number);
			}
			else
            {
                horn.Stop();
                particles.Stop();
            }
        }

        public void Impossible()
        {
            noHorn.Play();
        }

        public void DuringActionCanceled()
        {
            hornEmpty.Play();
        }

        private void OnCollisionEnter(Collision collision)
        {
            RRCollision rrCollision = collision.rigidbody.GetComponent<RRCollision>();
            if (rrCollision != null)
            {
                OnRRCollision?.Invoke(rrCollision, collision);
            }
        }

        private void OnTriggerStay(Collider other)
        {
            CargoGenerators cargoGenerators = other.GetComponent<CargoGenerators>();
            if (cargoGenerators != null)
            {
                OnCargoGenerators?.Invoke(cargoGenerators);
            }
        }


        protected virtual void OnTriggerEnter(Collider other)
        {
            RRCollision rrCollision = other.GetComponent<RRCollision>();
            if (rrCollision != null)
            {
                rrCollision.Color = Color.cyan;
                OnRRTriggerEnter?.Invoke(rrCollision);
                if (rrCollision.CollisionType == TrainCollisionType.HornLastingTrigger)
                    rrCollision.OnHornEntersTrigger(this);
            }
        }

        protected virtual void OnTriggerExit(Collider other)
        {
            RRCollision rrCollision = other.GetComponent<RRCollision>();
            if (rrCollision != null)
            {
                rrCollision.Color = Color.blue;
                OnRRTriggerExit?.Invoke(rrCollision);
            }
        }
    }
}
