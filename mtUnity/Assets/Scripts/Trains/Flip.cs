﻿using System.Collections.Generic;
using UnityEngine;
using WaySystem;

namespace Trains
{
    public class Flip : TimerBeh
    {
        [SerializeField] private AnimationCurve animCurve;
        public enum Direction { Here, There }

        [SerializeField] protected TrainFLipingPack train;
        [SerializeField] protected List<CoachFlipingPack> coaches;
        [SerializeField] protected List<ChunkFlipingPack> chunks;

        protected Direction direction;
        private float halfTime;
        private float fullTime;
        private float penaltyMultiplier;

        public float PenaltyMultiplier { set { this.penaltyMultiplier = value; } }

        public void Init(AnimationCurve animCurve, float halfTime, float fullTime)
        {
            this.animCurve = animCurve;
            this.halfTime = halfTime;
            this.fullTime = fullTime;
        }

        public void AddTrain(Visual trainVisual, Direction direction)
        {
            this.train = new TrainFLipingPack(trainVisual, direction);
            coaches = new List<CoachFlipingPack>();
            chunks = new List<ChunkFlipingPack>();
        }

        public void AddCoach(Visual coachVisual)
        {
            coaches.Add(new CoachFlipingPack(coachVisual));
        }

        public void AddChunk(WaySystem.WayChunk chunk)
        {
            chunks.Add(new ChunkFlipingPack(chunk, train.Step));
        }

        public void RunDouble()
        {
            train.Double();
            /*
            foreach (var pack in packs)
            {
                pack.Double();
            }
            */

            float left = fullTime - (halfTime * TimeNormalized);
            Launch(left);
        }

        

        public override void Launch(float lifeTime)
        {
            float complete = lifeTime * penaltyMultiplier;
            base.Launch(complete);
            GameStarter.Instance.UIRoot.Flipper.DebugTable.Set(
                8,
                "Flip: ",
                System.Math.Round(lifeTime, 2),
                " * ",
                System.Math.Round(penaltyMultiplier, 2),
                " = ",
                System.Math.Round(complete, 3));
        }

        protected override void TimeChange()
        {
            base.TimeChange();
            float evaluated = animCurve.Evaluate(TimeNormalized);
            Job(evaluated);
        }

        private void Job(float t)
        {
            train.Lerp(t);
            foreach(CoachFlipingPack coach in coaches)
            {
                coach.UpdateByTrain(train.Angle);
            }
            foreach (ChunkFlipingPack chunk in chunks)
            {
                chunk.Lerp(t);
            }
        }
    }

    [System.Serializable]
    public class ChunkFlipingPack : MainFlipingPack
    {
        public ChunkFlipingPack(WayChunk chunk, float step) : base(chunk.FlipTransform)
        {
            this.currentStep = step;
            to = from + currentStep;
            to = UberBehaviour.RoundWithStep(to, 180f);
        }
    }


    [System.Serializable]
    public class TrainFLipingPack : MainFlipingPack
    { 
        public Flip.Direction Direction { get { return direction; } }
        private Flip.Direction direction;

        public TrainFLipingPack(Visual visual, Flip.Direction direction) : base(visual.transform)
        {
            this.direction = direction;
            int prettyAngle = UberBehaviour.TransformToPrettyAngle(angle);
            float abs = Mathf.Abs(prettyAngle);

            //наверху
            if (prettyAngle <= 0 && abs <= 90)
            {
                //к нам
                if (direction == Flip.Direction.Here)
                    currentStep = -STEP;
                //от нас
                else
                    currentStep =  STEP;
            }
            //внизу
            else
            {
                //к нам
                if (direction == Flip.Direction.Here)
                    currentStep = STEP;
                //от нас
                else
                    currentStep = -STEP;
            }
            to = from + currentStep;
            to = UberBehaviour.RoundWithStep(to, 180f);
            //Debug.LogWarningFormat("from: {0} to:{1} direction:{2} abs:{3}", from, to, direction, abs);
        }
    }

    [System.Serializable]
    public class MainFlipingPack : BaseFlipingPack
    {
        public float Step { get { return currentStep; } }
        protected float currentStep;

        public MainFlipingPack(Transform transform) : base(transform)
        {
            this.from = this.angle;
        }

        /// <summary>
        /// Дополнить углы вращения до полного оборота
        /// </summary>
        public void Double()
        {
            from = angle;
            to = to + currentStep;
        }

        public void Lerp(float t)
        {
            angle = Mathf.Lerp(this.from, to, t);
            transform.localEulerAngles = Vector3.forward * angle;
        }
    }

    [System.Serializable]
    public class CoachFlipingPack : BaseFlipingPack
    {

        public CoachFlipingPack(Visual visual) : base(visual.transform)
        {

        }

        public void UpdateByTrain(float angle)
        {
            this.angle = angle;
            Set();
        }
    }

    [System.Serializable]
    public class BaseFlipingPack
    {
        public static readonly float STEP = 180f;
        public float Angle { get { return angle; } }

        protected Transform transform;
        protected float angle;
        protected float from;
        protected float to;

        public BaseFlipingPack(Transform transform)
        {
            this.transform = transform;
            this.angle = transform.localEulerAngles.z;
        }

        protected void Set()
        {
            transform.localEulerAngles = Vector3.forward * angle;
        }

        protected float SimplifyAngle(float angle)
        {
            float turns = angle / 360f;
            if (Mathf.Abs(turns) > 0)
            {
                turns = Mathf.Round(turns);
                float result = angle - (turns * 360);
                return result;
            }
            else
                return angle;
        }
    }
}