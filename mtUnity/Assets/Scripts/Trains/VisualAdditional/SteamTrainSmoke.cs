﻿using UnityEngine;

namespace Trains.Speeds
{
    public class SteamTrainSmoke : UberBehaviour
    {
        [SerializeField] protected ParticleSystem ps;
        [SerializeField] protected MinMax accelerationEmission;
        [SerializeField] protected int idleValue;


        public void ReceiveAcceleration(float value)
        {
            var emission = ps.emission;
            float rate = accelerationEmission.Lerp(value);
            emission.rateOverTime = rate;
        }


        public  void IdleRun()
        {
            var emission = ps.emission;
            emission.rateOverTime = idleValue;
        }

        public void On()
        {
            var emission = ps.emission;
            emission.enabled = true;
        }

        

        public void Off()
        {
            var emission = ps.emission;
            emission.enabled = false;
        }


    }
}