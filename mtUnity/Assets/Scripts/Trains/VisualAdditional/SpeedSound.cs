﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MTRails;

namespace Trains.Speeds
{
    public class SpeedSound : UberBehaviour
    {
        [SerializeField] protected AudioSource moveSound;
        [SerializeField] protected MinMax pitch;
        [SerializeField] protected MinMax speed;

        public void Lerp(float speedValue)
        {
            float t = this.speed.InverseLerp(speedValue);
            moveSound.pitch = pitch.Lerp(t);
        }

        public void OnOff(bool value)
        {
            if (value)
                moveSound.Play();
            else
                moveSound.Stop();
        }
    }
}