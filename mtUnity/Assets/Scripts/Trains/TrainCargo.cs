﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MTRails;
using System;
using System.Text;
using System.Threading.Tasks;

namespace Trains
{
    [System.Serializable]
    public class TrainCargo
    {
        public int ResourceID { get { return resourceID; } }
        public int NumberOfCoaches { get { return numberOfCoaches; } }

        private int resourceID;
        private int numberOfCoaches;

        public TrainCargo (int resourceID, int numberOfCoaches)
        {
            this.resourceID = resourceID;
            this.numberOfCoaches = numberOfCoaches;
        }
    }
}