﻿using System.Collections.Generic;
using UnityEngine;
using WaySystem;

namespace Trains
{
    public partial class Train
    {
        #region Debug Commands and Draw
        private void DebugToggleGodMode(bool value)
        {
            _debugGodMode = value;
        }

        private void DebugRestoreHitPoints()
        {
            this.hitPointsStates.Add();
            GameStarter.Instance.UIRoot.Flipper.ActiveHitpoints = hitPointsStates.hitPoints;
        }

        private void DebugDecreaseSpeedMultiplier()
        {
            speedControlBlock.Multiplier -= 0.1f;
            Debug.Log(speedControlBlock.Multiplier);
        }

        private void DebugZeroSpeedMultipier()
        {
            speedControlBlock.Multiplier = 0;
            Debug.Log(speedControlBlock.Multiplier);
        }

        private void DebugIncreaseSpeed()
        {
            speedControlBlock.Multiplier += 0.1f;
            Debug.Log(speedControlBlock.Multiplier);
        }

        private void DebugHorn(bool value)
        {
            HoldTap(value);
        }

        private void DebugSlower(bool value)
        {
            speedControlBlock.Multiplier -= 0.1f;
            Debug.LogFormat("Multiplier: {0}", speedControlBlock.Multiplier);
        }

        private void DebugNormal(bool value)
        {
            if (Time.timeScale == 1.0f)
                Time.timeScale = 4f;
            else
                Time.timeScale = 1.0f;
            Debug.LogFormat("TimeScale: {0}", Time.timeScale);
        }

        private void DebugFaster(bool value)
        {
            this.speedControlBlock.Multiplier += 0.1f;
            Debug.LogFormat("Multiplier: {0}", speedControlBlock.Multiplier);
        }

        private void DebugJump(bool value)
        {
            /*
            float newOffset = this.wayMovingPoint.Offset + 10f;
            this.wayMovingPoint.SetOffsetAndCalculate(newOffset);
            */
            List<WayTeleport> teleports = new List<WayTeleport>(FindObjectsOfType<WayTeleport>());
            teleports.Sort((x, y) => x.transform.position.z.CompareTo(y.transform.position.z));
            foreach(WayTeleport teleport in  teleports)
            {
                if (this.transform.position.z < teleport.transform.position.z)
                {
                    this.Place(teleport.Chunk, 0);
                    teleport.OnTeleport();
                    return;
                }
            }
            
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(transform.position, 0.2f);

            Gizmos.color = Color.green;
            if (wayMovingPoint != null && wayMovingPoint.Point != null)
            {
                Gizmos.DrawSphere(wayMovingPoint.Point.Position, 0.45f);
            }


            foreach (var bc in transform.GetComponentsInChildren<BoxCollider>())
            {
                DrawBoxCollider(bc, 0.85f, 1f, 0f, 0.2f);
            }

        }

        /// <summary>
        /// Set color
        /// </summary>
        /// <param name="chunk"></param>
        /// <param name="coeff"></param>
        /// <param name="color"></param>
        private void SetMat(WaySystem.WayChunk wayChunk, float coeff, Color color)
        {
            if (wayChunk.Transform == null)
            {
                Debug.LogErrorFormat("No Linked transform in chunk {0} {1}", wayChunk.Transform.position);
                return;
            }

            List<Renderer> rends = new List<Renderer>();
            Renderer myRender = wayChunk.Transform.GetComponent<Renderer>();
            if (myRender != null)
            {
                rends.Add(myRender);
            }
            Renderer[] childrenRenderers = wayChunk.Transform.GetComponentsInChildren<Renderer>();
            foreach (Renderer childRenderer in childrenRenderers)
            {
                if (childRenderer != null)
                {
                    rends.Add(childRenderer);
                }
            }


            foreach (Renderer r in rends)
            {
                Material mat = r.material;
                if (mat != null)
                {
                    mat.SetFloat("_Coeff", coeff);
                    mat.SetColor("_Color", color);
                }
            }
        }
        #endregion
        private void CreateDebugParamsChange()
        {
			/*
			float speed = TrainParameters.Speed;
			float flipTime = TrainParameters.FlipTime;

			GameStarter.Instance.DebugScreen.Controls.CreateDebugSlider((float value) =>
			{
				trainParameters.ChangeSpeeds(value, trainParameters.FlipTime);
			},
			MTUi.DebugIcons.train, 1f, 70f, speed, 1, "slider_speed" );


			GameStarter.Instance.DebugScreen.Controls.CreateDebugSlider((float value) =>
			{
				trainParameters.ChangeSpeeds(trainParameters.Speed, value);
			},
			MTUi.DebugIcons.train, 0.1f, 0.9f, flipTime, 2, "slider_flipTime");
			*/
		}
    }
}