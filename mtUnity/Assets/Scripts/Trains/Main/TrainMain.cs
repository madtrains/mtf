﻿using MTCore;
using System.Collections.Generic;
using UnityEngine;
using WaySystem;


namespace Trains
{
    public partial class Train
	{
		public delegate void ProcessVisual(Visual visual);

		#region Awake UI Inition
		private void Awake()
		{
			MTUi.Root.OnUIInited += InitUi;
			MTUi.Root.OnUIInited += InitDebug;
		}

        private void InitUi()
		{
		
			//воскрешение с потерей моеток
			GameStarter.Instance.UIRoot.Flipper.OnResurrect_Continue_Coins += () =>
			{
				Resurrect(true);
			};

			//воскрешение без потери монеток, но с потерей билетов
			GameStarter.Instance.UIRoot.Flipper.OnResurrect_Continue_Tickets += () =>
			{
				if (GameStarter.Instance.PlayerManager.TryReduceTickets(out int tickets))
				{
					Resurrect(false);
				}
			};

			GameStarter.Instance.UIRoot.Flipper.OnResurrect_Return += () =>
			{
				Resurrect(true);
				ArriveFlipper();
			};

			GameStarter.Instance.UIRoot.Flipper.OnTap = Tap;
            GameStarter.Instance.UIRoot.Flipper.OnPress = Press;
            GameStarter.Instance.UIRoot.Flipper.OnDoubleTap = DoubleTap;
			GameStarter.Instance.UIRoot.Flipper.OnTapHold = HoldTap;
		}

		private void InitDebug()
		{
			GameStarter.Instance.DebugScreen.OnDebugGodMode = DebugToggleGodMode;
			GameStarter.Instance.DebugScreen.OnDebugButtonRestorePoint = DebugRestoreHitPoints;
			GameStarter.Instance.DebugScreen.OnDebugButtonHorn = DebugHorn;
			GameStarter.Instance.DebugScreen.OnDebugButtonSlower = DebugSlower;
			GameStarter.Instance.DebugScreen.OnDebugButtonNormal = DebugNormal;
			GameStarter.Instance.DebugScreen.OnDebugButtonFaster = DebugFaster;
			GameStarter.Instance.DebugScreen.OnDebugButtonA = DebugJump;
		}
        #endregion

		#region Init/Place
		public override void CreateGameObjects()
		{
			base.CreateGameObjects();
			if (fliper == null)
			{
				fliper = gameObject.AddComponent<Flip>();
				fliper.OnEnd += FlipEnd;
			}
        }

        public override void Init()
        {
            base.Init();
            coaches = new List<Coach>();
            //стейты
            statesList = new List<States>();
            mainStates = new MainStates();
            additionalStates = new AdditionalStates();
            statesList.Add(mainStates);
            statesList.Add(additionalStates);
            hitPointsStates = new HitPointsStates();
            statesList.Add(hitPointsStates);
            distanceTasks = new List<DistanceTask>();
            timeTasks = new List<TimeTask>();
			//blocks
            this.blocks = new List<TrainControllBlock>();
            stoffControlBlock = new StoffControlBlock(this);
            blocks.Add(stoffControlBlock);
            chunksControlBlock = new ChunksControlBlock(this);
            blocks.Add(chunksControlBlock);
            speedControlBlock = new SpeedControlBlock(this);
            blocks.Add(speedControlBlock);
            cargoControlBlock = new CargoControlBlock(this);
            blocks.Add(cargoControlBlock);
			cameraControlBlock = new CameraControlBlock(this);
			blocks.Add(cameraControlBlock);
        }

        public override void CreateMovingPoint()
		{
			base.CreateMovingPoint();
			this.wayMovingPoint.IsEnableForEventTriggering = true;
        }

		public void AssignBaseComponents(MTSound.SoundsTrain sounds, Horn horn)
        {
            horn.transform.SetParent(this.transform);
            sounds.transform.SetParent(GameStarter.Instance.transform);
			this.stoffControlBlock.Horn = horn;
			horn.OnRRCollision = this.OnHornCollision;
            horn.OnRRTriggerEnter = this.OnHornTriggerEnter;
            horn.OnCargoGenerators = this.OnHornCargoGenerators;
            horn.OnRRTriggerExit = this.OnHornTriggerExit;
            //this.horn = GameStarter.Instance.InstantiateDebugLink<Horn>(17);

        }
        #endregion

        #region Go/Die/Resurrect
		private void DieByCollision(Collision collision)
		{
            mainStates.mainState = MainState.Falling;

            Vector3 localForce =
                Vector3.Lerp(
                    visual.transform.up,
                    transform.forward,
                    TrainShared.TrainPhysForce.Mix);
            Vector3 forcePoint = transform.position + (transform.forward * visual.FrontOffset) + (transform.up * 0.2f);
			/*
            PushRigidBody(this.Rigid,
                localForce * Speed * TrainShared.TrainPhysForce.Multiplier, forcePoint, ForceMode.VelocityChange);
			*/

            foreach (Coach coach in coaches)
            {
                PushRigidBody(coach.Rigid,
                    localForce, coach.transform.position);
            }

            GameStarter.Instance.UIRoot.Flipper.ShowRestart();
			tripLogger.AddDeath(cargoControlBlock.Coins);
			return;
		}

		private void FallToAbyss()
		{
            mainStates.mainState = MainState.Falling;

			Vector3 localForce =
				transform.forward * 1f;

            PushRigidBody(this.Rigid,
                localForce * Speed, transform.position + Vector3.up * 2f, ForceMode.VelocityChange);
			
			//MTDebug.DebugManager.Arrow(transform.position, localForce * Speed, 6f);
			
            foreach (Coach coach in coaches)
            {
                PushRigidBody(coach.Rigid,
                    localForce * Speed, transform.position + Vector3.up * 2f, ForceMode.VelocityChange);
            }

            GameStarter.Instance.UIRoot.Flipper.ShowRestart();
            tripLogger.AddDeath(cargoControlBlock.Coins);
            return;
        }
		#endregion

		#region Sounds
		public void SoundHornHit()
		{
			Sounds.HornHit();
		}
		#endregion

		#region public Set For Flipper/Town
		public void SetForFlipper(WaySystem.WayRootFlipper wayRoot)
		{
			this.ResetToDefaults();
			fliper.Init(GameStarter.GameParameters.TrainShared.FlipCurve,
					trainParameters.FlipTime,
					trainParameters.FlipTime * GameStarter.GameParameters.TrainShared.DoubleFlipMultiplier);

			//визуальная часть гудка
			stoffControlBlock.Horn.transform.SetParent(visual.transform);
            stoffControlBlock.Horn.transform.localPosition = new Vector3(0, visual.DebugBoxCollider.size[1] / 2f, visual.FrontOffset);

			//выставление значений на начало уровня
			mainStates.levelType = LevelType.Flipper;
			speedControlBlock.SetConstants(
				TrainShared.StartSpeed,
				trainParameters.Speed,
				trainParameters.SpeedIncrease);
			speedControlBlock.EngineSpeedState = EngineSpeedState.SavedConstant;

			hitPointsStates.hitPoints = TrainParameters.HitPoints;
			hitPointsStates.fullHitPoints = TrainParameters.HitPoints;
			hitPointsStates.OnHitPointsChange = ChangeHitPoints;

			GameStarter.Instance.UIRoot.Flipper.Prepare(TrainParameters);
			int coachIndex = 0;
			this.cargoControlBlock.InitForFlipper();
			foreach (Coach coach in coaches)
			{
				if (coach.Parameters == null)
					continue;

                int index = coachIndex;
                
				coachIndex++;
			}
			//MinMax absSpeed = trainParameters.SpeedAbsolute(TrainShared);
			GameStarter.Instance.UIRoot.Flipper.Reset();
			Place(wayRoot);
			this.tripLogger = new TripLogger(
				wayRoot,
				GameStarter.Instance.TrainManager.Train.TrainParameters.ID,
				GameStarter.Instance.TrainManager.Account.ActiveCoachesIDs.ToArray());

			wayRoot.ProcessStationManagers((stationManager) =>
			{
				ProcessStationManager(stationManager);
            });
		}

		public void ReSetForFlipper(WaySystem.WayRoot wayRoot)
        {
			ResetToDefaults();
			Place(wayRoot);
            GameStarter.Instance.UIRoot.Flipper.Reset();
		}


		/// <summary>
		/// Выставляем въезд в город, едем с постоянной скоростью указанное расстояние
		/// </summary>
        public void TownEnter(WayChunk chunk, float offset,
            float speed,
            float fullDistance,
            UnityEngine.Events.UnityAction<float> onProcess,
            UnityEngine.Events.UnityAction onDone)
        {
            PrepareForTown(chunk, offset);
            speedControlBlock.SetConstants(
                speed,
                speed,
                trainParameters.SpeedIncrease);

            speedControlBlock.SpeedFullControlOn(speed, speed);
			speedControlBlock.SpeedControl(0);

            CreateDistanceTask(fullDistance,
                (distance) =>
                {
                    onProcess?.Invoke(distance);
                },
                () =>
                {
                    onDone?.Invoke();
                });
        }

        public void TownMove(
			float fullDistance,
            UnityEngine.Events.UnityAction<float> onProcess,
            UnityEngine.Events.UnityAction onDone,
			float changeSpeed = 0)
        {
			if (changeSpeed > 0)
			{
                speedControlBlock.SpeedFullControlOn(changeSpeed);
            }

            CreateDistanceTask(fullDistance,
                (distance) =>
                {
                    onProcess?.Invoke(distance);
                    speedControlBlock.SpeedControl(distance);
                },
                () =>
                {
                    onDone?.Invoke();
                    if(changeSpeed > 0)
                    {
                        speedControlBlock.SpeedFullControlOn(changeSpeed, changeSpeed);
                    }
                });
        }

        public void TownBreak(float fullDistance,
			float minimalSpeed, float arrivalThresholdDistance,
            UnityEngine.Events.UnityAction<float> onProcess,
            UnityEngine.Events.UnityAction onThreshold,
            UnityEngine.Events.UnityAction onDone)
        {
            speedControlBlock.SpeedFullControlOn(minimalSpeed);
            bool arrived = false;
            CreateDistanceTask(fullDistance,
                (distance) =>
                {
                    onProcess?.Invoke(distance);
                    if(!arrived && distance > arrivalThresholdDistance)
                    {
                        arrived = true;
                        onThreshold?.Invoke();
                    }
                    speedControlBlock.SpeedControl(distance);
                },
                () =>
                {
                    onDone?.Invoke();
                    Stop();
                });
        }

        public void PrepareForTown(WayChunk chunk, float offset)
        {
            ResetToDefaults();
            mainStates.levelType = LevelType.Town;
            mainStates.mainState = MainState.Idle;

            stoffControlBlock.Horn.transform.SetParent(null);
            stoffControlBlock.Horn.transform.position = Vector3.up * 1000;

            Place(chunk, offset);
        }

        /// <summary>
        /// Готовим поезд к въезду в город
        /// </summary>
        /// <param name="chunk">стартовый чанк</param>
        /// <param name="offset">оффсет от стартового чанка</param>
        /// <param name="enterSpeed">скорость въезда</param>
        /// <param name="lowSpeed">скорость торможения</param>
        /// <param name="fullDistance">полная дистанция до остановки поезда</param>
        /// <param name="arrivalThresholdDistance">дистанция после которой запустятся эвенты прибытия</param>
        /// <param name="action">эент прибытия</param>
        public void PrepareForTown(WayChunk chunk, float offset,
			float enterSpeed, float lowSpeed, 
			float fullDistance, float arrivalThresholdDistance,
			UnityEngine.Events.UnityAction<float> onProcess,
            UnityEngine.Events.UnityAction onThreshold,
            UnityEngine.Events.UnityAction onDone)
        {
			PrepareForTown(chunk, offset);
            speedControlBlock.SetConstants(
                lowSpeed,
                enterSpeed,
                trainParameters.SpeedIncrease);

            speedControlBlock.SpeedFullControlOn(lowSpeed, enterSpeed);

            bool arrived = false;
            CreateDistanceTask(fullDistance,
                (distance) =>
                {
					onProcess?.Invoke(distance);
                    if(!arrived && distance > arrivalThresholdDistance)
                    {
                        arrived = true;
                        onThreshold?.Invoke();
                    }
                    speedControlBlock.SpeedControl(distance);
                },
                () =>
                {
					onDone?.Invoke();
                    Stop();
                });
        }
        #endregion

        protected override void AddSetRigidbody()
		{
			base.AddSetRigidbody();
			//rigid.interpolation = RigidbodyInterpolation.Extrapolate;
			//rigid.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
		}

		private void Move(float speed)
		{
			this.wayMovingPoint.Move(speed);
            this.UpdateTransform();
            this.visual.Move(
				null,
                speedControlBlock.Speed, speedControlBlock.EngineSpeed,
				speedControlBlock.LowMainSpeedsT);
            Visual previous = this.visual;
			if (coaches.Count != 0)
			{
				foreach (Coach coach in coaches)
				{
                    coach.OffsetFromOther(wayMovingPoint, -coach.LocomotiveOffset);
                    coach.UpdateTransform();
                    coach.Visual.Move(
						previous, 
						speedControlBlock.Speed, speedControlBlock.EngineSpeed,
						speedControlBlock.LowMainSpeedsT);
                    previous = coach.Visual;
				}
			}
        }

		private bool FlipCheck()
        {
			if (mainStates.levelType != LevelType.Flipper)
				return false;

			if (mainStates.mainState != MainState.Move)
				return false;

			if (!chunksControlBlock.FlippableRails)
				return false;

			return true;
		}

		private void Press()
		{
            if (cargoControlBlock.PlatformAssigned)
			{
                cargoControlBlock.Tap();
            }
        }

		private void Tap()
		{
			//обычная ситуация, не платформа
			if (!cargoControlBlock.PlatformAssigned)
			{
                if (!FlipCheck())
                    return;

                Flip();
                return;
            }
		}

		private void DoubleTap()
        {
			if (!FlipCheck())
				return;

			if (!chunksControlBlock.DoubleFlipEnabled)
				return;

			if (mainStates.moveSubState == MoveSubState.Default)
			{
				Flip();
				DoubleFlip();
				mainStates.moveSubState = MoveSubState.DoubleFlipping;
				return;
			}
			else if (mainStates.moveSubState == MoveSubState.Flipping)
			{
				DoubleFlip();
				mainStates.moveSubState = MoveSubState.DoubleFlipping;
				return;
			}
		}

		private void Flip()
        {
			mainStates.moveSubState = MoveSubState.Flipping;
			Trains.Flip.Direction direction = (Flip.Direction)UnityEngine.Random.Range(0, 2);
			if (chunksControlBlock.FlipDirections == WayChunkFlipDirections.HereOnly)
				direction = Trains.Flip.Direction.Here;
			else if(chunksControlBlock.FlipDirections == WayChunkFlipDirections.ThereOnly)
                direction = Trains.Flip.Direction.There;
            fliper.AddTrain(this.visual, direction);

			foreach (Coach coach in coaches)
			{
				fliper.AddCoach(coach.Visual);
			}

			foreach (WaySystem.WayChunk chunk in chunksControlBlock.FlipChunks)
			{
				fliper.AddChunk(chunk);
			} 
            fliper.Launch(TrainParameters.FlipTime);
			Sounds.Flip();
			GameStarter.Instance.GamePlayManager.UpdateCollection(MTDataCollection.Flips_number);
		}

		private void DoubleFlip()
        {
			fliper.RunDouble();
		}	

		private void FlipEnd()
		{
			mainStates.moveSubState = MoveSubState.Default;
			stoffControlBlock.AddByFlip();
			if (mainStates.moveSubState != MoveSubState.Default)
			{
				Debug.LogErrorFormat("Strange Situation on Flip Endd!");
			}
		}

		private void Hit()
		{
			//MTDebug.Debug.BoxColliderCube(Visual.DebugBoxCollider, new Color(0.75f, 1.0f, 0f, 0.25f), false);
			hitPointsStates.Hit();
			speedControlBlock.Hit();
			cameraControlBlock.Hit();

			tripLogger.AddHit();
			//Debug.Break();
		}
		
		private void HoldTap(bool value)
        {
			if (mainStates.levelType == LevelType.Flipper && mainStates.mainState == MainState.Move)
            {
				if (value)
					stoffControlBlock.Press();
				else
                    stoffControlBlock.Release();
			}
        }

		private void RelaunchFlipper()
		{
            Stop();

			OnFlipperRelaunch?.Invoke();
        }

        private void ArriveFlipper()
        {
            mainStates.levelType = LevelType.None;
			Dictionary<int, int> resources = cargoControlBlock.LeaveFlipperAndExitToTown();
            Stop();
            tripLogger.CloseTrip(
				cargoControlBlock.Coins, 
				cargoControlBlock.GetCurrentRoundResourceGathered(1)); //пассажиры, логгер принимает лишь один ресурс :-(
			OnFlipperExit?.Invoke(resources);
        }

        private void ResetToDefaults()
        {
			//cброс всех значений до "заводских", обнуление
			foreach (States states in statesList)
			{
				states.ResetToDefaults();
			}
			foreach (TrainControllBlock trainControllBlock in blocks)
				trainControllBlock.Reset();
			rigid.useGravity = false;
			rigid.constraints = RigidbodyConstraints.FreezeAll;
			foreach (Coach coach in coaches)
            {
				coach.Rigid.useGravity = false;
				coach.Rigid.constraints = RigidbodyConstraints.FreezeAll;
			}
			ProcessVisuals(
				(visual) => {
					visual.transform.localPosition = Vector3.zero;
					visual.transform.localRotation = Quaternion.identity;
				});
		}

        private void ProcessVisuals(ProcessVisual processVisual)
        {
            processVisual(visual);
            foreach(Coach coach in coaches)
            {
                processVisual(coach.Visual);
            }
        }
    }
}
