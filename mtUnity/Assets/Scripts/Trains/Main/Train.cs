﻿using MTSound;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Trains
{
    public partial class Train : TrainCoachProto
    {
        public MTParameters.TrainShared TrainShared { get { return GameStarter.GameParameters.TrainShared; } }
        public SoundsTrain Sounds { get { return GameStarter.Instance.MainComponents.SoundManager.Train; } }
        public MTParameters.Train TrainParameters 
        { 
            get { return trainParameters; }
            set { trainParameters = value; }
        }

        public List<Coach> Coaches { get { return coaches; } set { AssignCoaches(value); } }

        public float Weight { get { return cargoControlBlock.Weight; } }

        public float TractionRatio { get { return cargoControlBlock.TractionRatio; } }

        /// <value>Property <c>Angle</c> represents the train's angle. If Angle > 0, train goes down.</value>
        public float Angle
        {
            get
            {
                float angle = Mathf.Abs(transform.eulerAngles.x);
                //going up
                if (angle > 180)
                {
                    angle = 360 - angle;
                    angle = angle * -1f;
                }
                //going down
                return angle;
            }
        }

        public bool Immortal
        {
            get
            {
                if (mainStates.moveSubState == MoveSubState.Flipping ||
                    mainStates.moveSubState == MoveSubState.DoubleFlipping ||
                    stoffControlBlock.Horn.On)
                        return true;
                return false;
            }
        }

        public float Length
        {
            get
            {
                float result = visual.Length / 2f;
                result += coaches.Last().LocomotiveOffset + coaches.Last().Visual.Length / 2f;
                return result;
            }
        }

        public int CeilIntLength
        {
            get { return Mathf.CeilToInt(Length); }
        }

        public float HeadLength
        {
            get { return visual.Length + coaches.First().Visual.Length;}
        }

        public float HeadOffset
        {
            get { return visual.Length / 2f + coaches.First().Visual.Length; }
        }

        public float Speed { get { return speedControlBlock.Speed; } }

        //train-стейты
        private Train.MainStates mainStates;
        private Train.AdditionalStates additionalStates;
        private Train.HitPointsStates hitPointsStates;
        private Train.ChunksControlBlock chunksControlBlock;
        private Train.StoffControlBlock stoffControlBlock;
        private Train.SpeedControlBlock speedControlBlock;
        private Train.CargoControlBlock cargoControlBlock;
        private Train.CameraControlBlock cameraControlBlock;
        private List<Train.States> statesList;
        private List<Train.TrainControllBlock> blocks;


        private MTParameters.Train trainParameters;
        private List<Coach> coaches;
        private Flip fliper;
        private TripLogger tripLogger;

        //debug
        private bool _debugGodMode;

        private void AssignCoaches(List<Coach> coaches)
        {
            this.coaches = coaches;
            foreach (Coach coach in this.coaches)
            {
                coach.OnTrigger = CoachContactTrigger;
            }
        }
    }
}