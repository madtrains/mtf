﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MTRails;
using System;
using System.Text;
using System.Threading.Tasks;

namespace Trains
{
    public partial class Train
    {
        [System.Serializable]
        public class HitPointsStates : States
        {
            public UnityEngine.Events.UnityAction OnDeath;
            public UnityEngine.Events.UnityAction<int> OnHitPointsChange;
            public int hitPoints;
            public int fullHitPoints;


            public override void Resurrect()
            {
                this.hitPoints = fullHitPoints;
                if (OnHitPointsChange != null)
                    OnHitPointsChange(hitPoints);
            }

            public void Hit()
            {
                hitPoints -= 1;
                
                if (hitPoints <= 0)
                {
                    hitPoints = 0;

                    if (OnDeath != null)
                        OnDeath();
                }

                if (OnHitPointsChange != null)
                    OnHitPointsChange(hitPoints);
            }


            public void Add()
            {
                hitPoints += 1;
                hitPoints = Mathf.Clamp(hitPoints, 0, fullHitPoints);
            }
        }
    }
}