﻿
namespace Trains
{

    public partial class Train
    {
        [System.Serializable]
        public class AdditionalStates : States
        {
            /// <summary>
            /// Переключатель выезда у табло сработал
            /// </summary>
            public bool SFDSwitcherClicked;
            public bool SFDSCameraReady;


            public override void ResetToDefaults()
            {
                base.ResetToDefaults();
                this.SFDSwitcherClicked = false;
                this.SFDSCameraReady = false;
            }
        }
    }
}