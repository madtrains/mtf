﻿
using UnityEditor;

namespace Trains
{
    public enum LevelType { None, Flipper, Town }
    public enum MainState { Idle, Move, Falling }
    public enum MoveSubState { Default, Flipping, DoubleFlipping}
    

    public partial class Train
    {
        [System.Serializable]
        public class MainStates : States
        {
            /// <summary>
            /// Тип локации
            /// </summary>
            public LevelType levelType;

            /// <summary>
            /// стоим/едем/падаем
            /// </summary>
            public MainState mainState;

            public MoveSubState moveSubState;

            /// <summary>
            /// сохранённое положение на путях
            /// </summary>
            public Triggers.Semaphore semaphore;


            public override void ResetToDefaults()
            {
                this.mainState = MainState.Idle;
                this.moveSubState = MoveSubState.Default;
            }

            public override void Resurrect()
            {
                this.mainState = MainState.Move;
                this.moveSubState = MoveSubState.Default;
            }

            public void Save(Triggers.Semaphore semaphore)
            {
                this.semaphore = semaphore;
            }
        }
    }
}