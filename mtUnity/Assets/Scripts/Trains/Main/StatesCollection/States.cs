﻿
namespace Trains
{
    public partial class Train
    {
        [System.Serializable]
        public class States
        {
            /// <summary>
            /// вызывается при каждом заезде на флиппер/ въезде в город / повторном выезде на флиппер
            /// </summary>
            public virtual void ResetToDefaults()
            {
                
            }

            /// <summary>
            /// сохранение параметров для воскрешения
            /// </summary>
            public virtual void Save()
            {

            }

            /// <summary>
            /// воскрешение во флиппере
            /// </summary>
            public virtual void Resurrect()
            {

            }
        }
    }
}
