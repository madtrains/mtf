﻿using MTRails;
using UnityEngine;
using WaySystem.PickUp;

namespace Trains
{
    public partial class Train
    {
        private float catchGeneratedCargoPause;
        private void OnCollisionEnter(Collision collision)
        {
            if (mainStates.levelType != LevelType.Flipper)
                return;

            if (mainStates.mainState == MainState.Falling)
                return;

            if (_debugGodMode)
                return;

            RRCollision rrCollision = collision.rigidbody.GetComponent<RRCollision>();
            if (rrCollision != null)
            {
                switch (rrCollision.CollisionType)
                {
                    case (TrainCollisionType.HitPoint):
                    {
                        if (Immortal || rrCollision.Off)
                        {
                            Sounds.HitNoPoint();
                            break;
                        }

                        Hit();

                        if (hitPointsStates.hitPoints <= 0)
                        {
                            DieByCollision(collision);
                            Sounds.Death();
                        }
                        else
                        {
                            Sounds.Hit();
                        }
                        break;
                    }
                    case (TrainCollisionType.Deadly):
                    {
                        Sounds.Death();
                        ProcessVisuals((visual) => { visual.Death(); });
                        DieByCollision(collision);
                        break;
                    }
                }
                rrCollision.TrainCollision(collision, Speed);
            }
        }

        private void OnHornCollision(RRCollision rrCollision, Collision collision)
        {
            rrCollision.HornCollision(collision);
        }

        private void OnHornTriggerEnter(RRCollision rrCollision)
        {
           
        }

        private void OnHornCargoGenerators(WaySystem.Cargos.CargoGenerators cargoGenerators)
        {
            if (catchGeneratedCargoPause < GameStarter.GameParameters.FlipperParameters.HornCargoLoadDelay)
            {
                catchGeneratedCargoPause += GameStarter.GameParameters.FlipperParameters.HornCargoLoadStep;
            }
            else 
            {
                catchGeneratedCargoPause = 0f;
                cargoControlBlock.CatchGeneratedCargo(cargoGenerators);
            }
        }

        private void OnHornTriggerExit(RRCollision rrCollision)
        {

        }

        public void SaveSemaphore(
            Triggers.Semaphore semaphore)
        {
            mainStates.Save(semaphore);
            Sounds.Semaphore(true);
        }

        private void ChangeHitPoints(int hitpoins)
        {
            GameStarter.Instance.UIRoot.Flipper.ActiveHitpoints = hitpoins;
        }

        private void CoachContactTrigger(Rigidbody rigidbody)
        {
            if (rigidbody == null || rigidbody.transform.parent == null)
            {
                return;
            }
            PickUp pickUp = rigidbody.transform.parent.GetComponent<PickUp>();
            if (pickUp == null)
            {
                return;
            }
            Pick(pickUp);
        }

        protected override void OnTriggerEnter(Collider other)
        {
            if (mainStates.mainState != MainState.Move)
                return;

            Rigidbody rigidbody = other.attachedRigidbody;
            if (rigidbody == null || rigidbody.transform.parent == null) 
            { 
                return;
            }

            PickUp pickUp = rigidbody.transform.parent.GetComponent<PickUp>();
            if (pickUp != null)
            {
                Pick(pickUp);
                return;
            }

            AbyssEdge abyssEdge = rigidbody.transform.GetComponent<AbyssEdge>();
            if (abyssEdge != null)
            {
                FallToAbyss();
                return;
            }
        }

        /// <summary>
        /// обработка подбирашек
        /// </summary>
        /// <param name="pickUp"></param>
        private void Pick(PickUp pickUp)
        {
            cargoControlBlock.PickUp(pickUp.PickUpType);
            Sounds.PickUp(pickUp.PickUpType);
            switch (pickUp.PickUpType)
            {
                case (Type.GearRepair):
                {
                    this.hitPointsStates.Add();
                    GameStarter.Instance.UIRoot.Flipper.ActiveHitpoints = hitPointsStates.hitPoints;
                    break;
                }
                case (Type.Ticket):
                {
                    GameStarter.Instance.UIRoot.Flipper.Ticket();
                    break;
                }

                case (Type.CoinSilver):
                {
                    GameStarter.Instance.UIRoot.Flipper.GiftCoin(10);
                    break;
                }

                case (Type.CoinGold):
                {
                    GameStarter.Instance.UIRoot.Flipper.GiftCoin(100);
                    break;
                }

                default :    
                {
                    //GameStarter.Instance.UIRoot.Flipper.Ticket();
                    break;
                }
            }
            pickUp.Off(this);
        }
    }
}