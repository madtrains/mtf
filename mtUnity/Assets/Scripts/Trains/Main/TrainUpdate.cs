﻿using UnityEngine;

namespace Trains
{
    public partial class Train
    {
        private enum ChunkPlace { Behind, Under, Horn, Flip, InFront }

        private void UpdateSoundMove()
        {
            this.Sounds.UpdateMoveSounds(speedControlBlock.Speed, speedControlBlock.EngineSpeed, speedControlBlock.LowMainSpeedsT);
        }

        private void Update()
        {
            if (mainStates == null || mainStates.levelType == LevelType.None)
                return;

            if (distanceTasks.Count > 0)
            {
                for (int i = 0; i < distanceTasks.Count; i++)
                {
                    var dc = distanceTasks[i];
                    dc.Move(wayMovingPoint.OffsetGlobal);
                }
            }

            if (this.timeTasks.Count > 0)
            {
                for (int i = 0; i < timeTasks.Count; i++)
                {
                    var dc = timeTasks[i];
                    dc.Update();
                }
            }

            if (mainStates.levelType == LevelType.Flipper)
            {
                UpdateInFlipper();
            }

            else if (mainStates.levelType == LevelType.Town)
            {
                UpdateInTown();
            }

            if (mainStates.mainState != MainState.Move)
                return;
            
            this.UpdateTransform();
            if (coaches.Count != 0)
            {
                foreach (Coach coach in coaches)
                {
                    coach.UpdateTransform();
                }
            }
            UpdateSoundMove();
        }


        private void LateUpdate()
        {
            cameraControlBlock.Update();
            //UpdateCamera();
        }


        private void PlaceCameraImmediate(Vector3 position, MTParameters.CameraParameters camParameters)
        {
            GameStarter.Instance.MainComponents.GameCamera.transform.position = position;
            GameStarter.Instance.MainComponents.GameCamera.CheckAndApplyParams(camParameters);
        }


        private void UpdateInFlipper()
        {
            chunksControlBlock.Update();
            speedControlBlock.Update();
            stoffControlBlock.Update();

            if (mainStates.mainState == MainState.Move)
            {
                //передача смещения
                Move(Time.deltaTime * speedControlBlock.Speed);
                wayRootFlipper.SetTrainZ(this.transform.position.z);
            }
        }

        private void UpdateInTown()
        {
            speedControlBlock.Update();

            if (mainStates.mainState == MainState.Move)
            {
                Move(Time.deltaTime * speedControlBlock.Speed);
                if (OnTownMove != null)
                    OnTownMove(wayMovingPoint.OffsetGlobal);
            }
        }
        
    }
}