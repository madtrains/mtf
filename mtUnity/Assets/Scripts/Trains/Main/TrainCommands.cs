﻿using MTTown;
using System.Collections.Generic;
using UnityEngine;
using WaySystem;
using WaySystem.Additions;
using WaySystem.Cargos;

namespace Trains
{
    public partial class Train
    {
        public UnityEngine.Events.UnityAction<float> OnTownMove;
        public UnityEngine.Events.UnityAction OnTownArrival;
        public UnityEngine.Events.UnityAction OnTrainResurrection;
        public UnityEngine.Events.UnityAction OnTrainStopped;

        public UnityEngine.Events.UnityAction<Dictionary<int, int>> OnFlipperExit;
        public UnityEngine.Events.UnityAction OnFlipperRelaunch;
        public UnityEngine.Events.UnityAction OnCameraFocusedOnSFD;
        public UnityEngine.Events.UnityAction OnSFDStopReached;
        private List<DistanceTask> distanceTasks;
        private List<TimeTask> timeTasks;
        private WayRoot wayRoot;
        private WayRootFlipper wayRootFlipper;

        public bool IsWoodLoaded { get { return cargoControlBlock.IsWoodLoaded; } }


        public void Go(
            EngineSpeedState engineSpeedState = EngineSpeedState.Acceleration,
            bool isMainCameraControlledByTrain = true,
            float additionalCamT = 0f)
        {
            speedControlBlock.EngineSpeedState = engineSpeedState;
            cameraControlBlock.IsCameraControlledByTrain = isMainCameraControlledByTrain;
            cameraControlBlock.Main2Add = additionalCamT;
            Go();
        }

        private void Go()
        {
            mainStates.mainState = MainState.Move;
            ProcessVisuals((visual) => { visual.GO(); });
        }

        public void Stop()
        {
            mainStates.mainState = MainState.Idle;
            speedControlBlock.Reset();
            cameraControlBlock.Reset();
            speedControlBlock.Stop();
            ProcessVisuals((visual) => { visual.Stop(); });
            OnTrainStopped?.Invoke();
        }

        public void StartEngine()
        {
            ProcessVisuals((visual) => { visual.StartEngine(); });
        }

        public void StopEngine()
        {
            ProcessVisuals((visual) => { visual.StopEngine(); });
        }

        public void Place(WayRoot wayRoot)
        {
            this.wayRoot = wayRoot;
            if(wayRoot is WayRootFlipper) { this.wayRootFlipper = wayRoot as WayRootFlipper; }
            Place(wayRoot.FirstChunk, wayRoot.TrainStartOffset);
        }

        public override void Place(WayChunk wayChunk, float locoOffset)
        {
            base.Place(wayChunk, locoOffset);

            if(coaches.Count != 0)
            {
                foreach(Coach coach in coaches)
                {
                    coach.OffsetFromOther(this.wayMovingPoint, -coach.LocomotiveOffset);
                    coach.UpdateTransform();
                }
            }
        }

        public void Resurrect(bool setCoinsToZero)
        {
            foreach(States states in statesList)
            {
                states.Resurrect();
            }
            foreach(TrainControllBlock trainControllBlock in blocks)
                trainControllBlock.Resurrect();

            rigid.useGravity = false;
            rigid.constraints = RigidbodyConstraints.FreezeAll;
            foreach(Coach coach in coaches)
            {
                coach.Rigid.useGravity = false;
                coach.Rigid.constraints = RigidbodyConstraints.FreezeAll;
            }
            ProcessVisuals(
                (visual) =>
                {
                    visual.transform.localPosition = Vector3.zero;
                    visual.transform.localRotation = Quaternion.identity;
                });

            if(mainStates.semaphore == null)
            {
                UnityEngine.Debug.LogError("Semaphore was not saved, can not ressurect train!");
                return;
            }

            Place(mainStates.semaphore.WayChunk, mainStates.semaphore.Offset);
            if(setCoinsToZero)
                cargoControlBlock.ZeroCoins();
            OnTrainResurrection?.Invoke();
            this.stoffControlBlock.Horn.transform.localPosition = new Vector3(0, visual.DebugBoxCollider.size[1] / 2f, visual.FrontOffset);
            this.stoffControlBlock.Horn.transform.localRotation = Quaternion.identity;
            Go();
        }

        public void RefreshCoachesPositionsByHead()
        {
            if(coaches.Count != 0)
            {
                foreach(Coach coach in coaches)
                {
                    coach.Place(wayMovingPoint.WayChunk, wayMovingPoint.Offset - coach.LocomotiveOffset);
                }
            }
        }

        public void ProcessEnter(WaySystem.Enter enter)
        {
            cameraControlBlock.IsCameraControlledByTrain = true;
            cameraControlBlock.AddCamera(enter.CamDock);
            cameraControlBlock.Main2Add = 1f;
            var dc = CreateDistanceTask(
                enter.CameraEventLength, (t) =>
                {
                    float lerpValue = 1f - t;
                    cameraControlBlock.Main2Add = lerpValue;
                },
                () =>
                {
                    cameraControlBlock.Main2Add = 0f;
                    speedControlBlock.EngineSpeedState = EngineSpeedState.Acceleration;
                });
        }

        public void Command_FullThrottle()
        {
            speedControlBlock.EngineSpeedState = EngineSpeedState.Acceleration;
        }

      

        
        /// <summary>
        /// Дать поезду подписаться на все эвенты менеджера станции
        /// </summary>
        /// <param name="stationManager"></param>
        public void ProcessStationManager(StationManager stationManager)
        {
            //первое торможение, и включение табло
            stationManager.OnStationBreaking += () =>
            {
                this.cargoControlBlock.StationArrive(stationManager);
                //включаем табло
                stationManager.Show();
                cameraControlBlock.SideCamera =
                    GameStarter.Instance.SplitFlapDisplay.CamDockStation.CameraParams;
                speedControlBlock.SpeedControllOn(
                    stationManager.StationParameters.BreakingLine.Speed);

                ProcessVisuals((visual) => { visual.Breaking(true); });
                var dc = CreateDistanceTask(stationManager.StationParameters.BreakingLine.ChunksCount,
                    (t) =>
                    {
                        cameraControlBlock.Speed2Side = t;
                        speedControlBlock.SpeedControl(t);
                    },
                    () =>
                    {
                        speedControlBlock.SpeedControlOff(EngineSpeedState.SavedConstant);
                        ProcessVisuals((visual) => { visual.Breaking(false); });
                    });
            };
            
            stationManager.OnWayToPlatforms += (platform) =>
            {
                bool canDoIt = false;
                if(platform != null && !platform.Empty)
                {
                    canDoIt = this.cargoControlBlock.Active(platform.CargoResourceID);
                }
                if(canDoIt)
                {
                    float wayToPlatform = platform.AdditionalInfo.PreviousLineLength + platform.AdditionalInfo.OnLinePositionOffset;
                    cameraControlBlock.AddCamera(platform.CamDock);
                    speedControlBlock.SpeedControllOn(stationManager.StationParameters.PlatformSpeed);

                    CreateDistanceTask(wayToPlatform - stationManager.StationParameters.GreenLightOffset,
                    (t) =>
                    {
                        speedControlBlock.SpeedControl(t);
                        cameraControlBlock.Speed2Side = 1 - t;
                        cameraControlBlock.Main2Add = t;
                    },
                    () =>
                    {
                        //прибыли на погрузку, включили зелёный, едем вдоль платформы
                        float distance = platform.BoardingLength + stationManager.StationParameters.RedLightOffset + stationManager.StationParameters.GreenLightOffset;
                        cargoControlBlock.PlatformOnGreenLight(platform);
                        CreateDistanceTask(distance,
                            (t2) =>
                            {

                            },
                            () =>
                            {
                                //пора уезжать со станции
                                cargoControlBlock.PlatformOffRedLight(platform);
                                speedControlBlock.EngineSpeedState = EngineSpeedState.Acceleration;
                                CreateDistanceTask(stationManager.StationParameters.LeaveLength,
                                (t3) =>
                                {
                                    cameraControlBlock.Main2Add = 1 - t3;

                                },
                                () => { }
                                );
                            });
                    });
                    //ProcessPlatformAsTarget(stationManager, platform);
                }
                speedControlBlock.EngineSpeedState = EngineSpeedState.Acceleration;
                ProcessVisuals((visual) => { visual.Pass(); });
                Sounds.Skip();
                CreateDistanceTask(stationManager.StationParameters.LeaveLength,
                (tPass1) =>
                {
                    cameraControlBlock.Speed2Side = 1 - tPass1;
                },
                () =>
                {
                    speedControlBlock.EngineSpeedState = EngineSpeedState.Acceleration;
                }
                );
            };
            /*
            stationManager.OnArrive += (platform) =>
            {
                //float trainLength = Length - visual.Length / 2f;

                //если мы не можем собирать этот ресурс, то должны проехать мимо
                bool canDoIt = this.cargoControlBlock.IsResourceActive(platform.Cargo.ResourceID);
                if (canDoIt) 
                { 
                    ProcessPlatformAsTarget(stationManager, platform);
                }
                else
                { 
                    ProcessPlatformAsPass(stationManager, platform); 
                }
            };
            */
        }

        public void ProcessExitManager(ExitManager exitManager)
        {
            exitManager.OnBreaking += (distance) =>
            {
                speedControlBlock.SpeedControllOn(
                    GameStarter.GameParameters.TrainShared.ExiSfdSpeed);
                cameraControlBlock.Main2Add = 0;
                cameraControlBlock.AddCamera(GameStarter.Instance.SplitFlapDisplay.CamDockExit);
                bool shown = false;
                ProcessVisuals((visual) => { visual.Breaking(true); });
                CreateDistanceTask(distance,
                    (t) =>
                    {
                        //ещё не доехав до самого конца поезд начинает высадку пассажиров, если они есть
                        if(!shown && t > 0.85f)
                        {
                            shown = true;
                            StopAlomost(exitManager);
                        }
                        speedControlBlock.SpeedControl(t);
                        cameraControlBlock.Main2Add = t;
                    },
                    () =>
                    {
                        exitManager.ExitChainSteps.ResourcesToShowOnSFD = cargoControlBlock.AllResourcesGathered;
                        Stop();
                        exitManager.ExitChainSteps.IsTrainStopped = true;
                    });
            };

            exitManager.OnTrainDepart += () =>
            {
                Go(EngineSpeedState.SpeedControlBySaved, false);
                speedControlBlock.SpeedControllOn(exitManager.FlipperParameters.ExitConstructor.LeaveSpeed);
                ProcessVisuals((visual) => { visual.StartEngine(); });
                exitManager.LeaveCam.MoveCameraToMe(
                    () => { }, (t) =>
                    {
                        speedControlBlock.SpeedControl(t);
                    },
                    exitManager.FlipperParameters.ExitConstructor.LeaveCamFlySpeed,
                    exitManager.FlipperParameters.ExitConstructor.LeaveCamFlySpeed);
            };

            exitManager.OnFlipperExitToTown += () =>
            {
                ArriveFlipper();
            };

            exitManager.OnFlipperRelaunch += () =>
            {
                RelaunchFlipper();
            };
        }

        public bool TownUnload(LOADS_ENUM load, CargoBunch bunch, UnityEngine.Events.UnityAction OnUnloadDone)
        {
            switch (load)
            {
                case LOADS_ENUM.WOOD:
                {
                    bool result = cargoControlBlock.IsWoodLoaded;
                    RecursiveTimerAction woodUnload = new RecursiveTimerAction(
                        GameStarter.GameParameters.FlipperParameters.UnloadPause,
                        //check
                        () => { return cargoControlBlock.IsWoodLoaded; },
                        //action - refill
                        () => { cargoControlBlock.WoodUnload(bunch); });

                    woodUnload.OnDone = () => { OnUnloadDone?.Invoke(); };
                    woodUnload.Run();
                    return result;
                }

                default : return false;
            }
        }

        private void StopAlomost(ExitManager exitManager)
        {
            cargoControlBlock.ExitArrive(exitManager);
            RecursiveTimerAction passengersUnload = new RecursiveTimerAction(
                GameStarter.GameParameters.FlipperParameters.UnboardPause,
                //check
                ()=> { return cargoControlBlock.IsPassengersLoaded; },
                //action - unload
                ()=> { cargoControlBlock.ExitUnloadPassenger(exitManager.PlatformExit.Passengers);});

            RecursiveTimerAction freightRefill = new RecursiveTimerAction(
                GameStarter.GameParameters.FlipperParameters.UnloadPause,
                //check
                () => { return cargoControlBlock.IsFreightPlaceLeft; },
                //action - refill
                () => { cargoControlBlock.RefillLoadFreight(exitManager.PlatformExit.FreightReloader); });

            passengersUnload.OnDone = () => 
            { 
                freightRefill.Run();  //связка со следующим эвентом
            }; 
            freightRefill.OnDone = () => 
            {
                //табло можно использовать roundResources для показа только текущего раунда
                Dictionary<int, int> roundResources = cargoControlBlock.CloseFlipperRound();
                exitManager.ExitChainSteps.IsTrainUnload = true;  //условие завершение высадки / погрузки
            }; 
            passengersUnload.Run();
        }

        private void ProcessPlatformAsPass(StationManager stationManager, Platform platform)
        {
            float length = 4f;
                //platform.AdditionalInfo.ArrivalLength;
                //(trainLength - stationManager.StationParameters.PlatformLoadOffset);
                //platform.BoardingLength + stationManager.StationParameters.PlatformLoadOffset;
            //начинаем разгон
            speedControlBlock.SpeedControlOff(EngineSpeedState.Acceleration);
            //даём гудок
            CreateDistanceTask(
                length,
                (t) =>
                {
                    cameraControlBlock.Speed2Side = 1 - t;

                },
                ()=>
                {
                    ProcessVisuals((visual) => { visual.Pass(); });
                    Sounds.Skip();
                }
            );

        }

        private DistanceTask CreateDistanceTask(
            float distance,
            DistanceTask.OnProcess onMove,
            DistanceTask.OnDone onDone)
        {
            DistanceTask task =
                new DistanceTask(
                    wayMovingPoint.OffsetGlobal,
                    distance,
                    onMove,
                    onDone);
            distanceTasks.Add(task);
            task.SetOnRemove(() => distanceTasks.Remove(task));
            return task;
        }

        private void CreateDelayedDistanceTask(
            float delayDistance, float distance,
            DistanceTask.OnProcess onMove,
            DistanceTask.OnDone onDone)
        {
            DistanceTask delayTask = CreateDistanceTask(delayDistance, (t) => { }, () =>
            {
                DistanceTask mainTask = CreateDistanceTask(distance, onMove, onDone);
            });
        }



        private class RecursiveTimerAction
        {
            public delegate bool CheckDelegate();
            public delegate void ActionDelegate();
            
            public UnityEngine.Events.UnityAction OnDone { set => onDone = value; }

            private MinMax pause;            
            private CheckDelegate check;
            private ActionDelegate action;
            private UnityEngine.Events.UnityAction onDone;

            public RecursiveTimerAction(MinMax pause, CheckDelegate check, ActionDelegate action)
            {
                this.pause = pause;
                this.check = check;
                this.action = action;
            }

            public void Run()
            {
                if(Check())
                {
                    Action();
                }
                else
                {
                    Done();
                }
            }

            private void Action()
            {
                this.action();
                Relaunch();
            }

            private void Done()
            {
                onDone?.Invoke();
            }

            private void Relaunch()
            {
                if(Check())
                {
                    float t = pause.Random;
                    Debug.LogWarningFormat("Pause {0}", t);
                    GameStarter.Instance.MainComponents.TimerManager.CreateSimpleTimer(t).OnEnd += () =>
                    {
                        Debug.LogWarningFormat("Timer is End {0}", t);
                        Action();
                    };
                }
                else
                {
                    Done();
                }
            }

            protected virtual bool Check()
            {
                return this.check();
            }
        }

        private class LerpTask
        {
            public delegate void OnProcess(float t);
            public delegate void OnDone();

            protected float start;
            protected float end;
            protected OnProcess onProcess;
            protected OnDone onDone;
            protected OnDone onRemove;
            

            public LerpTask(float start, float end, OnProcess onProcess, OnDone onDone)
            {
                this.start = start;
                this.end = end;
                this.onProcess = onProcess;
                this.onDone = onDone;
            }

            public void SetOnRemove(OnDone onRemove)
            {
                this.onRemove = onRemove;
            }

            protected void Check(float t)
            {
                onProcess(t);
                if (t >= 1.0)
                {
                    onDone();
                    onRemove();
                }
            }
        }

        private class DistanceTask : LerpTask
        {

            public DistanceTask(float currentPosition, float length, OnProcess onProcess, OnDone onDone):
                base(currentPosition, currentPosition+length, onProcess, onDone)
            {

            }

            public void Move(float currentPosition)
            {
                float result = Mathf.InverseLerp(start, end, currentPosition);
                float clamped = Mathf.Clamp(result, 0f, 1f);
                Check(clamped);
            }
        }

        private class TimeTask : LerpTask
        {
            public TimeTask(float duration, OnProcess onProcess, OnDone onDone) :
                base(Time.time, Time.time + duration, onProcess, onDone)
            {
                
            }

            public void Update()
            {
                float result = Mathf.InverseLerp(start, end, Time.time);
                float clamped = Mathf.Clamp(result, 0f, 1f);
                Check(clamped);
            }
        }
    }
}