﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MTRails;

namespace Trains
{
    public class VisualRubber : Visual
    {
        [SerializeField] private Transform joint;
        public override void Move(Visual previous, float absoluteSpeed, float engineSpeed, float acceleration)
        {
            base.Move(previous, absoluteSpeed, engineSpeed, acceleration);
            joint.position = previous.Anchor.position;
            joint.rotation = previous.Anchor.rotation;
        }
    }
}