﻿using Trains.Speeds;
using UnityEngine;

namespace Trains
{
    public class SteamTrainVisual : VisualTrain
    {
        [SerializeField] private SteamTrainSmoke smoke;
        [SerializeField] private float wheelRadius;

        public override void Move(Visual previous, float absoluteSpeed, float engineSpeed, float acceleration)
        {
            base.Move(previous, absoluteSpeed, engineSpeed, acceleration);
            smoke.ReceiveAcceleration(acceleration);
            float circleLength = 2f * Mathf.PI * wheelRadius;
            float speed = absoluteSpeed / circleLength;
            animator.SetFloat(HashSpeed, speed);
        }

        public override void Stop()
        {
            base.Stop();
            smoke.IdleRun();
            animator.SetFloat(HashSpeed, 0);
        }

        public override void StopEngine()
        {
            base.StopEngine();
            smoke.Off();
        }

        public override void StartEngine()
        {
            base.StartEngine();
            smoke.On();
        }
    }
}