﻿using UnityEngine;

namespace Trains
{
    public class Visual : UberColorizer
    {
        public static int HashSpeed = Animator.StringToHash("speed");
        public static int HashAcceleration = Animator.StringToHash("acceleration");

        public Transform[] Sockets =>  sockets;
        public Transform Anchor { get { return anchor; } }

        [SerializeField] protected Transform anchor;
        [SerializeField] protected Transform[] sockets;


        public Visual Copy
        {
            get
            {
                Visual clone = GameObject.Instantiate<Visual>(this, this.transform.parent);
                clone.name = this.name;
                return clone;
            }
        }


        public virtual void GO()
        {
            
        }

        public virtual void Stop()
        {
            
        }

        public virtual void Move(Visual previous, float absoluteSpeed, float engineSpeed, float acceleration)
        {

        }

        public virtual void SlowBreaking()
        {

        }

        public virtual void Breaking(bool status)
        {

        }

        public virtual void StartEngine()
        {

        }

        public virtual void StopEngine()
        {

        }

        public virtual void Hit()
        {

        }

        public virtual void Death()
        {
            /*
            foreach (Speeds.Speed speed in speeds)
            {
                speed.Death();
            }
            */
        }

        public virtual void Pass()
        {

        }


        public float FrontOffset { get { return frontOffset; } }
        public float RearOffset { get { return rearOffset; } }
        public float Length { get { return length; } }
        public BoxCollider DebugBoxCollider { get { return debugBoxCollider; } }

        [Tooltip("Offset from pivot to front bounding point. Most actual for train ")]
        [SerializeField] protected float frontOffset;

        [Tooltip("Offset from pivot to rear bounding point, most actual for last coach")]
        [SerializeField] protected float rearOffset;

        [Tooltip("Length to find offset from previous coach or train for placing it on way")]
        [SerializeField] protected float length;

        [SerializeField] protected Animator animator;
        [SerializeField] protected BoxCollider debugBoxCollider;
    }
}