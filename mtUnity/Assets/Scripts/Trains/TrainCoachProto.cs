﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MTRails;
using WaySystem;
using System;

namespace Trains
{
    public class TrainCoachProto : UberBehaviour
    {
        public Action<Rigidbody> OnTrigger;
        public WaySystem.WayChunkMovingPoint WayMovingPoint { get {return wayMovingPoint; } }
        public Rigidbody Rigid { get { return rigid; } }
        public Visual Visual { get { return visual; } }
        

        [SerializeField] protected Rigidbody rigid;
        protected Visual visual;
        protected WaySystem.WayChunkMovingPoint wayMovingPoint;

        public virtual void CreateMovingPoint()
        {
            this.wayMovingPoint = new WaySystem.WayChunkMovingPoint();
        }

        public virtual void Place(WayChunk wayChunk, float offset)
        {
            this.wayMovingPoint.Place(wayChunk, offset);
            UpdateTransform();
        }

        public void OffsetFromOther(
            WaySystem.WayChunkMovingPoint other, float offset)
        {
            this.wayMovingPoint.PlaceOffsetedFromOther(other, offset);
        }

        public virtual void UpdateTransform()
        {
            this.transform.position = wayMovingPoint.Point.Position;
            this.transform.rotation = wayMovingPoint.Point.Rotation;
        }

        public virtual void UpdateLerpTransform(float speed)
        {
            this.transform.position = Vector3.Lerp(this.transform.position,  wayMovingPoint.Point.Position, speed);
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation,  wayMovingPoint.Point.Rotation, speed);
        }

        public virtual void CreateGameObjects()
        {
            AddSetRigidbody();
        }

        public virtual void Init()
        {

        }

        public static void PushRigidBody(
            Rigidbody rigid, Vector3 force, Vector3 point, ForceMode mode = ForceMode.Impulse)
        {
            rigid.isKinematic = false;
            rigid.useGravity = true;
            rigid.constraints = RigidbodyConstraints.None;
            rigid.AddForceAtPosition(force, point, mode);
        }


        ///<summary>
        ///Назначение Визуала, можно копию, дабы не удалять загруженный объект
        ///<param name="visual">
        ///Визуал
        ///</param>
        ///<param name="cloneVisual">
        ///Назначать копию
        ///</param>
        ////</summary>
        public virtual Visual AssignParentVisual(Visual visual, bool cloneVisual)
        {
            if (cloneVisual)
            {
                this.visual = visual.Copy;
            }
            else
            {
                this.visual = visual;
            }

            this.visual.transform.SetParent(this.transform, false);
            this.visual.transform.localPosition = Vector3.zero;
            this.visual.transform.rotation = Quaternion.identity;
            UberBehaviour.Activate(this.visual.gameObject, true);
            return this.visual;
        }


        public virtual Visual ExtractUnparentVisual()
        {
            if (this.visual != null)
            {
                Visual saved = this.visual;
                this.visual.transform.parent = null;
                this.visual = null;
                return saved;
            }

            return null;
        }


        public virtual void DeleteVisual()
        {
            if (this.visual != null)
            {
                Destroy(this.visual.gameObject);
            }
        }


        protected virtual void AddSetRigidbody()
        {
            rigid = gameObject.AddComponent<Rigidbody>();
            rigid.isKinematic = false;
            rigid.useGravity = false;
            rigid.constraints = RigidbodyConstraints.None;
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
            OnTrigger?.Invoke(other.attachedRigidbody);
        }
    }
}