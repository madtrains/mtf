﻿using MTSound;
using Trains.Speeds;
using UnityEngine;

namespace Trains
{
    public class VisualTrain :  Visual
    {
        [SerializeField] protected SpeedSound engineSound;
        [SerializeField] protected AudioSource breakingAudioSource;
        [SerializeField] protected AudioSource startAudioSource;

        public SoundsTrain Sounds { get { return GameStarter.Instance.MainComponents.SoundManager.Train; } }

        public override void Move(Visual previous, float absoluteSpeed, float engineSpeed, float acceleration)
        {
            base.Move(previous, absoluteSpeed, engineSpeed, acceleration);
            engineSound.Lerp(engineSpeed);
        }

        public override void GO()
        {
            base.GO();
            engineSound.OnOff(true);
        }

        public override void Death()
        {
            base.Death();
            engineSound.OnOff(false);
        }

        public override void Stop()
        {
            base.Stop();
            engineSound.OnOff(false);
        }

        public override void SlowBreaking()
        {
            base.SlowBreaking();
            //breakingAudioSource.Play();
        }

        public override void Breaking(bool status)
        {
            base.Breaking(status);
            engineSound.OnOff(!status);
            if (status)
                Sounds.BreakPress();
            else
                Sounds.BreakRelease();
        }


        public override void StartEngine()
        {
            base.StartEngine();
            startAudioSource.Play();
        }



    }
}