﻿using MTUi.Flipper;
using System.Collections.Generic;
using WaySystem.Cargos;

namespace Trains.Load
{
    public class MailLoad : CoachLoad
    {
        public MailLoad(Coach coach) : base(coach, (int)LOADS_ENUM.MAIL)
        {
            
        }

        protected override bool CheckIfCanLoad()
        {
            return true;
        }
    }

    public class MailBunch : CoachLoadBunch<MailLoad>
    {
        public override void UpdateUI()
        {
            base.UpdateUI();
            GameStarter.Instance.UIRoot.Flipper.CoachCounterTrigger(4f);
            GameStarter.Instance.UIRoot.Flipper.CoachCounter.SetStartValues((int)LOADS_ENUM.MAIL, CoachCounter.Mode.Unlimited);
            GameStarter.Instance.UIRoot.Flipper.CoachCounter.UpdateValue(Loaded);
        }

        public MailBunch(List<Coach> coaches)
        {
            List<MailLoad> loads = new List<MailLoad>();
            foreach(Coach coach in coaches)
            {
                loads.Add(new MailLoad(coach));
            }
            this.coaches = loads;
            this.coaches.ForEach((coach) => { coach.OnLoadUnload += OnLoadUnload; });
            this.coaches.ForEach((coach) => { coach.OnLoad += OnLoad; });
            this.coaches.ForEach((coach) => { coach.OnUnload += OnUnload; });
        }

        public override void ReceiveCargo(Cargo cargo)
        {
            if (Active) 
            {
                base.ReceiveCargo(cargo);
                this.coaches.ForEach(coach => { coach.CheckAndUnload(cargo.LoadUnload(Mode.InTrainLoad)); });
            }
        }

        protected override void OnLoad()
        {
            base.OnLoad();
            OnCountResult?.Invoke((this.ResourceID, 1));
        }
    }
}
