﻿using System.Collections.Generic;
using WaySystem.Cargos;

namespace Trains.Load
{
    public class PassengerLoad : AdvancedLoad
    {
        
        public PassengerLoad(Coach coach, int resourceID, int capacity) : base(coach, resourceID, capacity)
        {
            
        }
    }

    public class PassengerBunch: AdvancedBunch<PassengerLoad>
    {
        public PassengerBunch(List<Coach> coaches)
        {
            List<PassengerLoad> loads = new List<PassengerLoad>();
            foreach(Coach coach in coaches)
            {
                loads.Add(new PassengerLoad(coach, (int)LOADS_ENUM.PASSENGERS, coach.Parameters.Capacity));
            }
            this.coaches = loads;
            this.coaches.ForEach((coach) => { coach.OnLoadUnload += OnLoadUnload;});
            this.coaches.ForEach((coach) => { coach.OnLoad += OnLoad; });
            this.coaches.ForEach((coach) => { coach.OnUnload += OnUnload; });
        }

        public PassengerBunch(params PassengerLoad[] loads) : base(loads) { }

        public override void PlatformTap(Cargo cargo)
        {
            base.PlatformTap(cargo);
            if(UnloadLimit > 0)
            {
                UnloadCargo(cargo);
            }
            else
            {
                ReceiveCargo(cargo);
            }
        }


        protected override void OnUnload()
        {
            base.OnUnload();
            OnCountResult?.Invoke((this.ResourceID, 1));
        }


    }
}
