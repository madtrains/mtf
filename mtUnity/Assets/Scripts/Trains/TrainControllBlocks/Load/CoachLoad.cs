﻿using System.Collections.Generic;
using System.Linq;
using WaySystem.Additions;
using WaySystem.Cargos;

namespace Trains.Load
{
    public interface ICoachLoadBunch
    {
        public bool Active { get; }

        public void PlatformTap(Cargo cargo);

        public void ReceiveCargo(Cargo cargo);

        public void PlatformApproach(Platform platform);
    }
    /// <summary>
    /// груз в ОДНОМ вагоне, ОДИН вагон в представлении груза. Из таких элментов составляются Bunch. Прямой связи вагон=груз быть не может, так как один вагон может возить несколько типов грузов (люди, почта)
    /// </summary>
    public class CoachLoad
    {
        public UnityEngine.Events.UnityAction OnLoadUnload;
        public UnityEngine.Events.UnityAction OnLoad;
        public UnityEngine.Events.UnityAction OnUnload;

        /// <summary>
        /// ссылка на вагон
        /// </summary>
        protected Coach coach;
        protected int resourceID;
        protected int loaded;

        public CoachLoad(Coach coach, int resourceID)
        {
            this.coach = coach;
            this.resourceID = resourceID;
            this.loaded = 0;
        }

        public int ResourceID { get { return resourceID; } }

        public int Loaded { get { return loaded; } }

        public bool CanLoad { get { return CheckIfCanLoad(); } }

        public bool CanUnload { get { return CheckIfCanUnload(); } }

        public void CheckAndLoad(CargoElement cargoElement)
        {
            if(CanLoad)
            {
                Load(cargoElement);
            }
        }

        public void CheckAndUnload(CargoElement cargoElement)
        {
            if(CanUnload)
            {
                Unload(cargoElement);
            }
        }

        /// <summary>
        /// перезарядка без отображения в интерфейсе
        /// </summary>
        public virtual void RefillSilently()
        {
            //loaded = capacity;
        }

        public virtual void Reset()
        {

        }

        protected virtual void Load(CargoElement cargoElement)
        {
            loaded++;
            OnLoadUnload?.Invoke();
            OnLoad?.Invoke();
            RunElement(cargoElement);
        }

        protected virtual void Unload(CargoElement cargoElement)
        {
            loaded--;
            OnLoadUnload?.Invoke();
            OnUnload?.Invoke();
            RunElement(cargoElement);
        }

        protected virtual bool CheckIfCanLoad()
        {
            return false;
        }

        protected virtual bool CheckIfCanUnload()
        {
            return false;
        }

        protected virtual void RunElement(CargoElement cargoElement)
        {
            cargoElement.Run(
                this.coach.Visual.transform,
                GameStarter.GameParameters.Cargo.ArcHeight,
                0, //задержка всегда нулевая, но вынесена на всякий случай
                GameStarter.GameParameters.Cargo.FlyTime.Random,
                () => { }, true, false);
        }
    }

    public class CoachLoadBunch<T> : ICoachLoadBunch where T : CoachLoad
    {
        public UnityEngine.Events.UnityAction<(int id, int loadedValue)> OnCountResult;
        public delegate void ProcessCoachLoad(T cargoElement);

        public UnityEngine.Events.UnityAction<(int id, int loadedValue)> OnResourceLoadedUnloaded;

        public CoachLoadBunch(params T[] coaches)
        {
            this.coaches = new List<T>();
            foreach(T coach in coaches)
            {
                this.coaches.Add(coach);
                coach.OnLoadUnload += OnLoadUnload;
                coach.OnLoad += OnLoad;
                coach.OnUnload += OnUnload;
            }
        }

        protected List<T> coaches;

        /// <summary>
        /// На старте мы создаём связки для всех грузов в игре, а активна ли она на этом составе? есть ли хоть один вагон для этого типа груза? проверяется тут
        /// </summary>
        public bool Active { get { return GetActive(); } }

        /// <summary>
        /// сколько единиц груза сейчас в составе?
        /// </summary>
        public int Loaded => this.coaches.Sum(coach => coach.Loaded);

        /// <summary>
        /// Может ли состав принять груз?
        /// </summary>
        public bool CanLoad => this.coaches.Any(coach => coach.CanLoad);

        /// <summary>
        /// Может ли состав выгрузить свой груз?
        /// </summary>
        public bool CanUnload => this.coaches.Any(coach => coach.CanUnload);


        public int ResourceID => this.coaches[0].ResourceID;


        /// <summary>
        /// вызывается вагонами при изменении груза в них
        /// </summary>
        protected virtual void OnLoadUnload()
        {
            OnResourceLoadedUnloaded?.Invoke((ResourceID, Loaded));
            UpdateUI();
        }

        protected virtual void OnLoad()
        {

        }

        protected virtual void OnUnload()
        {

        }

        protected void Process(ProcessCoachLoad delegat)
        {
            foreach(T coach in this.coaches)
            {
                delegat(coach);
            }
        }

        /// <summary>
        ///вызывается на изменения значения груза для отображение в интерфейсе
        /// </summary>
        public virtual void UpdateUI()
        {

        }

        public virtual void PlatformApproach(Platform platform)
        {

        }

        public virtual void PlatformGreenLight(Platform platform)
        {

        }

        public virtual void PlatformRedLight(Platform platform)
        {

        }

        /// <summary>
        /// на платформе стоит груз, был тап
        public virtual void PlatformTap(Cargo cargo)
        {

        }


        /// <summary>
        /// принять груз
        /// </summary>
        /// <param name="cargo">объект-груз обрабатывающий погрузку в вагон</param>
        public virtual void ReceiveCargo(Cargo cargo)
        {
            if (Active)
            {
                this.coaches.ForEach(coach => { coach.CheckAndLoad(cargo.LoadUnload(Mode.InTrainLoad)); });
            }
        }

        /// <summary>
        /// выгрузить груз из вагона
        /// </summary>
        /// <param name="cargo">объект принимающий выгружаемый из вагона груз</param>
        public virtual void UnloadCargo(Cargo cargo)
        {
            if(Active)
            {
                this.coaches.ForEach(coach => { coach.CheckAndUnload(cargo.LoadUnload(Mode.OutOfTrainUnload)); });
            }
        }

        /// <summary>
        /// используется для вагонов которые загружаются "на базе" (фрахт)
        /// </summary>
        public virtual void RefillSilently()
        {
            this.coaches.ForEach(coach => { coach.RefillSilently(); });
        }

        /// <summary>
        /// используется для вагонов которые надо обнулять перед выездом  (дерево)
        /// </summary>
        public virtual void Reset()
        {
            this.coaches.ForEach(coach => { coach.Reset(); });
        }

        /// <summary>
        /// этот метод определяет активна ли эта связка вагонов или нет, ведь на старте мы создаём связки для всех грузов в игре, а активна ли она на этом составе определяется тут
        /// </summary>
        /// <returns></returns>
        protected virtual bool GetActive()
        {
            return this.coaches.Count > 0;
        }
    }
}
