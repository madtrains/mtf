﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WaySystem.Cargos;

namespace Trains.Load
{
    public class WoodLoad : AdvancedLoad
    {
        public WoodLoad(Coach coach, int resourceID, int capacity) : base(coach, resourceID, capacity) {}

        private int currentIndex = 0;
        private List<CargoElement> loadedLogs;

        protected override void Unload(CargoElement cargoElement)
        {
            OnLoadUnload?.Invoke();
            OnUnload?.Invoke();
            loaded--;
            unloadLimit--;
            CargoElement coachLog = loadedLogs.Last();
            coachLog.BaseTransform = coachLog.transform.parent;
            coachLog.transform.localPosition = Vector3.zero;
            coachLog.transform.localRotation = Quaternion.identity;
            coachLog.Run(cargoElement.BaseTransform,
                GameStarter.GameParameters.Cargo.ArcHeight,
                0,
                GameStarter.GameParameters.Cargo.FlyTime.Random, () =>
                {
                    coachLog.transform.SetParent(cargoElement.BaseTransform, false);
                    coachLog.transform.localPosition = Vector3.zero;
                    coachLog.transform.localRotation = Quaternion.identity;
                },
            false, true);
            loadedLogs.RemoveAt(loadedLogs.Count - 1);
        }

        protected override void RunElement(CargoElement cargoElement)
        {
            Transform socket = this.coach.Visual.Sockets[currentIndex];
            cargoElement.Run(
                socket,
                GameStarter.GameParameters.Cargo.ArcHeight,
                0, //задержка всегда нулевая, но вынесена на всякий случай
                GameStarter.GameParameters.Cargo.FlyTime.Random,
                () => 
                {
                    cargoElement.transform.SetParent(socket, false);
                    cargoElement.transform.localPosition = Vector3.zero;
                    cargoElement.transform.localRotation = Quaternion.identity;
                }, 
                false, true);
            currentIndex++;
            loadedLogs.Add(cargoElement);
        }

        public override void Reset()
        {
            base.Reset();
            this.currentIndex = 0;
            this.loadedLogs = new List<CargoElement>();
        }
    }

    public class WoodBunch : AdvancedBunch<WoodLoad>
    {
        public WoodBunch(List<Coach> coaches)
        {
            List<WoodLoad> loads = new List<WoodLoad>();
            foreach(Coach coach in coaches)
            {
                loads.Add(new WoodLoad(coach, (int)LOADS_ENUM.WOOD, coach.Parameters.Capacity));
            }
            this.coaches = loads;
            this.coaches.ForEach((coach) => { coach.OnLoadUnload += OnLoadUnload; });
            this.coaches.ForEach((coach) => { coach.OnLoad += OnLoad; });
            this.coaches.ForEach((coach) => { coach.OnUnload += OnUnload; });
        }

        public override void PlatformTap(Cargo cargo)
        {
            base.PlatformTap(cargo);
            if (CanLoad)
            {
                ReceiveCargo(cargo);
            }
        }

        protected override void OnLoad()
        {
            base.OnLoad();
            OnCountResult?.Invoke((this.ResourceID, 1));
        }
    }
}
