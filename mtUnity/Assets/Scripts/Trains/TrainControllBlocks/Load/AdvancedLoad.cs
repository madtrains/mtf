﻿using MTUi.Flipper;
using System.Linq;
using UnityEngine;
using WaySystem.Additions;
using WaySystem.Cargos;

namespace Trains.Load
{
    public class AdvancedLoad : CoachLoad
    {
        public AdvancedLoad(Coach coach, int resourceID, int capacity) : base(coach, resourceID)
        {
            this.capacity = capacity;
            this.unloadLimit = 0;
        }

        public int Capacity => capacity;

        public int UnloadLimit => unloadLimit;

        public void SetUnloadLimit(float perentage)
        {
            this.unloadLimit = Mathf.FloorToInt(this.loaded * perentage);
        }

        public void AddUnloadLimit()
        {
            if(this.unloadLimit < this.loaded)
                this.unloadLimit += 1;
        }

        public void ResetUnloadLimit()
        {
            this.unloadLimit = 0;
        }

        protected override bool CheckIfCanLoad()
        {
            return this.loaded < capacity;
        }

        protected override bool CheckIfCanUnload()
        {
            return loaded > 0 &&  unloadLimit > 0;
        }

        public override void RefillSilently()
        {
            base.RefillSilently();
            this.loaded = capacity;
        }

        protected override void Unload(CargoElement cargoElement)
        {
            base.Unload(cargoElement);
            this.unloadLimit -= 1;
        }

        protected int capacity;
        protected int unloadLimit;
    }

    public class AdvancedBunch<T> : CoachLoadBunch<T> where T : AdvancedLoad
    {
        public int Capacity => this.coaches.Sum(coach => coach.Capacity);
        public int UnloadLimit => this.coaches.Sum(coach => coach.UnloadLimit);

        public AdvancedBunch(params T[] loads) : base(loads) 
        {
            
        }


        public override void UpdateUI()
        {
            base.UpdateUI();
            GameStarter.Instance.UIRoot.Flipper.CoachCounterTrigger(10f);
            GameStarter.Instance.UIRoot.Flipper.CoachCounter.SetStartValues(this.ResourceID, CoachCounter.Mode.CapacityLoadUnload, Capacity);
            GameStarter.Instance.UIRoot.Flipper.CoachCounter.UpdateValueAndSlider(this.Loaded, UnloadLimit);
        }

        public void SetUnloadLimit(float percentage, int absoluteLimit)
        {
            this.coaches.ForEach(coach => { coach.ResetUnloadLimit();});
            int limit = Mathf.Clamp(Mathf.FloorToInt(Loaded * percentage), 0, absoluteLimit);
            while(limit > 0)
            {
                this.coaches.ForEach(coach => { coach.AddUnloadLimit(); limit -= 1; });
            }
        }

        public override void PlatformApproach(Platform platform)
        {
            base.PlatformApproach(platform);
            if (Active)
            {
                SetUnloadLimit(platform.UnloadPercentage, platform.Cargo.UnloadPlacesCount);
            }
        }

        
    }
}
