﻿using System.Collections.Generic;
using WaySystem.Cargos;

namespace Trains.Load
{
    public class FreightLoad : AdvancedLoad
    {
        public FreightLoad(Coach coach, int resourceID, int capacity) : base(coach, resourceID, capacity)
        {
            
        }
    }

    public class FreightBunch: AdvancedBunch<FreightLoad>
    {
        public FreightBunch(List<Coach> coaches)
        {
            List<FreightLoad> loads = new List<FreightLoad>();
            foreach(Coach coach in coaches)
            {
                loads.Add(new FreightLoad(coach, (int)LOADS_ENUM.FREIGHT, coach.Parameters.Capacity));
            }
            this.coaches = loads;
            this.coaches.ForEach((coach) => { coach.OnLoadUnload += OnLoadUnload; });
            this.coaches.ForEach((coach) => { coach.OnLoad += OnLoad; });
            this.coaches.ForEach((coach) => { coach.OnUnload += OnUnload; });
        }

        public override void PlatformTap(Cargo cargo)
        {
            base.PlatformTap(cargo);
            if (CanUnload)
            {
                UnloadCargo(cargo);
            }
        }

        protected override void OnUnload()
        {
            base.OnUnload();
            OnCountResult?.Invoke((this.ResourceID, 1));
        }
    }
}
