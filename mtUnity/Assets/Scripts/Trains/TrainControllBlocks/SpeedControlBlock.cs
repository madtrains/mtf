﻿using MTParameters;
using System.Text;
using UnityEngine;
using UnityEngine.Rendering;

namespace Trains
{
    public partial class Train
    {
        public enum EngineSpeedState 
        {
            SavedConstant, 
            Acceleration,
            SpeedControlBySaved
        }

        [System.Serializable]
        private class SpeedControlBlock : TrainControllBlock
        {
            public SpeedControlBlock(Train train) : base(train) { }

            public EngineSpeedState EngineSpeedState { get { return speedState; } set { speedState = value; } }
            /// <summary>
            /// стейт скорости поезда, находится ли он в состоянии покоя или 
            /// </summary>
            private EngineSpeedState speedState;
            

            private float savedEngineSpeed;
            //"вшитые" параметры

            private float lowSpeedValue;
            private float mainSpeedValue;
            private float maxSpeedValue;

            /// <summary>
            /// множитель акселерации, берётся из парамсов
            /// </summary>
            private float increase;

            private float controlSpeedT;
            private float controlSpeedTargetValue;

            public float LowMainSpeedsT { get { return Mathf.InverseLerp(lowSpeedValue, mainSpeedValue, engineSpeed); } }


            public float EngineSpeed { get { return engineSpeed; } }
            /// <summary>
            /// чистая скорость поезда, без учёта последующих модификаторов угла, бафов и прочего
            /// </summary>
            private float engineSpeed;

            /// <summary>
            /// дополнительный множитель для дебага
            /// </summary>
            private float multiplier = 1f;
            public float Multiplier { get { return multiplier; } set { multiplier = value; } }

            /// <summary>
            /// финальная скорость поезда
            /// </summary>
            private float speed;
            public float Speed { get { return speed; } }

            private float AngleMultiplier
            {
                get
                {
                    float angle = train.Angle;
                    GameStarter.Instance.UIRoot.Flipper.DebugTable.Set(3, "Ang: ", System.Math.Round(angle));
                    //up
                    if (angle < 0)
                    {
                        float absAngle = Mathf.Abs(angle);
                        float speedAngleUpModifier = 
                            GameStarter.GameParameters.TrainShared.SpeedAngleUpModifier.GetLerpedValue(absAngle, 1f);
                        float angleUpPenalty = train.TrainShared.OverweightPenalty.GetAngleMultiplier(absAngle);
                        float result = speedAngleUpModifier * angleUpPenalty;
                        GameStarter.Instance.UIRoot.Flipper.DebugTable.Set(4, "UP: ", System.Math.Round(speedAngleUpModifier, 2),  
                            " * ", System.Math.Round(angleUpPenalty, 2), " = ", System.Math.Round(result, 2));
                        return
                            result;
                    }
                    //down
                    else if (angle > 0)
                    {
                        float speedAngleDownModifier = 
                            GameStarter.GameParameters.TrainShared.SpeedAngleDownModifier.GetLerpedValue(angle, 1f);
                        GameStarter.Instance.UIRoot.Flipper.DebugTable.Set(4, "Speed Down: ", System.Math.Round(speedAngleDownModifier, 2));
                        return
                            speedAngleDownModifier;
                            //1f + (1f - train.TrainShared.OverweightPenalty.GetAngleMultiplier(angle));
                    }
                    GameStarter.Instance.UIRoot.Flipper.DebugTable.Set(4, "Flat");
                    return 1f;
                }
            }


            public override void Update()
            {
                if (train.mainStates.mainState != MainState.Move)
                {
                    return;
                }

                switch (speedState)
                {
                    case (EngineSpeedState.SavedConstant):
                    {
                        engineSpeed = savedEngineSpeed;
                        break;
                    }
                    case (EngineSpeedState.Acceleration):
                    {
                        //набираем скорость до своей максималки из параметров
                        if (engineSpeed < mainSpeedValue)
                        {
                                engineSpeed += Time.deltaTime * increase *
                                        train.TrainShared.OverweightPenalty.SpeedUpMultiplier *
                                        AngleMultiplier;
                                GameStarter.Instance.UIRoot.Flipper.DebugTable.Set(6, "Main Speed UP: ", System.Math.Round(train.TrainShared.OverweightPenalty.SpeedUpMultiplier, 3));
                        }
                        //набираем скорость до абсолютной максималки (40 м/с)
                        else
                        {
                            //где мы сейчас
                            float inverseT =
                                    Mathf.InverseLerp(
                                        mainSpeedValue,
                                        maxSpeedValue,
                                        engineSpeed);

                            inverseT = 1f - inverseT;
                            float overSpeed = inverseT * train.TrainShared.OverSpeedFactor;
                            GameStarter.Instance.UIRoot.Flipper.DebugTable.Set(6, "Over Speed T: ", System.Math.Round(inverseT, 3));
                            GameStarter.Instance.UIRoot.Flipper.DebugTable.Set(7, "Over Speed UP: ", System.Math.Round(overSpeed, 3));

                                engineSpeed += Time.deltaTime * overSpeed *
                                        train.TrainShared.OverweightPenalty.SpeedUpMultiplier *
                                        AngleMultiplier;
                                //Debug.Log(train.TrainShared.SpeedIncrease.Evaluate(inverseT));
                                engineSpeed = Mathf.Clamp(engineSpeed, lowSpeedValue, maxSpeedValue);
                        }
                        savedEngineSpeed = engineSpeed;
                        break;
                    }
                    case (EngineSpeedState.SpeedControlBySaved):
                    {
                        engineSpeed = Mathf.Lerp(
                            savedEngineSpeed,
                            controlSpeedTargetValue,
                            controlSpeedT);
                        break;
                    }
                }
                


                ////
                //применяем модификаторы скорости
                speed = engineSpeed * multiplier * AngleMultiplier;

                StringBuilder stringBuilder = new StringBuilder();
                GameStarter.Instance.UIRoot.Flipper.DebugTable.Set(0, "Eng: ", System.Math.Round(engineSpeed, 2));
                GameStarter.Instance.UIRoot.Flipper.DebugTable.Set(1, 
                    "Low / Main :", 
                    System.Math.Round(lowSpeedValue, 2),
                    " / ",
                    System.Math.Round(mainSpeedValue, 2),
                    "| ",
                    System.Math.Round(maxSpeedValue, 2));
                GameStarter.Instance.UIRoot.Flipper.DebugTable.Set(2, "State: ", speedState);
                GameStarter.Instance.UIRoot.Flipper.DebugTable.Print();
                //передаём на спидометр значение скорости в метрах в секунду
                GameStarter.Instance.UIRoot.Flipper.ReceiveAbsoluteSpeed(speed);
                //Debug.LogWarningFormat("engine {0:0.0} {1} {2:0.00}", engineSpeed, speedState, acceleration);
            }

            public void Stop()
            {
                GameStarter.Instance.UIRoot.Flipper.ReceiveAbsoluteSpeed(speed);
            }

            public override void Resurrect()
            {
                base.Resurrect();
                this.speedState = EngineSpeedState.Acceleration;
                this.controlSpeedT = 0;
                this.savedEngineSpeed = lowSpeedValue;
                this.engineSpeed = lowSpeedValue;
            }

            public override void Reset()
            {
                base.Reset();
                this.speedState = EngineSpeedState.SavedConstant;
                this.controlSpeedT = 0f;
                this.savedEngineSpeed = 0f;
                this.engineSpeed = 0f;
                this.multiplier = 1f;
                this.speed = 0f;
            }

            public void SpeedControllOn(float targetSpeed)
            {
                speedState = EngineSpeedState.SpeedControlBySaved;
                savedEngineSpeed = engineSpeed;
                controlSpeedTargetValue = targetSpeed * train.TrainShared.OverweightPenalty.AbsMultiplier;
                controlSpeedT = 0f;
            }

            public void SpeedFullControlOn(float targetSpeed, float savedEngineSpeed = 0)
            {
                speedState = EngineSpeedState.SpeedControlBySaved;
                if (savedEngineSpeed != 0)
                {
                    this.savedEngineSpeed = savedEngineSpeed;
                }
                controlSpeedTargetValue = targetSpeed;
                controlSpeedT = 0f;
            }

            public void SpeedControl(float t)
            {
                this.controlSpeedT = t;
            }

            public void SpeedControlOff(EngineSpeedState mode)
            {
                this.savedEngineSpeed = engineSpeed;
                speedState = mode;
            }

            public override void Hit()
            {
                base.Hit();
                speedState = EngineSpeedState.Acceleration;
                engineSpeed = lowSpeedValue;
            }

            public void SetConstants(
                float lowSpeed, float mainSpeedValue, float increase)
            {
                train.TrainShared.OverweightPenalty.SetRatio(train.TractionRatio);
                train.fliper.PenaltyMultiplier = train.TrainShared.OverweightPenalty.FlipMultiplier;
                this.lowSpeedValue = lowSpeed  * train.TrainShared.OverweightPenalty.AbsMultiplier; 
                this.mainSpeedValue = mainSpeedValue * train.TrainShared.OverweightPenalty.AbsMultiplier;
                this.increase = increase;
                this.savedEngineSpeed = lowSpeedValue;
                maxSpeedValue = train.TrainShared.MaxSpeed * train.TrainShared.OverweightPenalty.AbsMultiplier;
                GameStarter.Instance.UIRoot.Flipper.DebugTable.Set(
                    5, 
                    "Ratio: ", System.Math.Round(train.TractionRatio, 2), 
                    " OwT: ", System.Math.Round(train.TrainShared.OverweightPenalty.T, 2),
                    " Weight: ", System.Math.Round(train.Weight, 2), 
                    " Power: ", train.TrainParameters.TractionPower );
            }
        }
    }
}