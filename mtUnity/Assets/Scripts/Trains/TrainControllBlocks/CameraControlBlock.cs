﻿using WaySystem;
using UnityEngine;
using MTParameters;
using MTTown;
using static UnityEngine.Networking.UnityWebRequest;
using UnityEditor;

namespace Trains
{
    public partial class Train
    {
        private class CameraControlBlock : TrainControllBlock
        {
            public CameraControlBlock(Train train) : base(train)
            {
                cameraMovingPoint = new WaySystem.WayChunkMovingPoint();
                speedA = GameStarter.GameParameters.Cameras[6];
                speedB = GameStarter.GameParameters.Cameras[5];
                sideCam = new CameraParameters();
                additionalCam = new CameraParameters();
                additionalTarget = new Vector3();
                resultParameters = new CameraParameters();
            }

            /// <summary>
            /// Параметры камеры при минимальной и максимальной скоростях
            /// </summary>
            private CameraParameters speedA;
            private CameraParameters speedB;
            private CameraParameters sideCam;
            private CameraParameters additionalCam;
            private Vector3 additionalTarget;
            private CameraParameters resultParameters;
            private float speedT;
            private float speed2SideT;
            private float main2AddT;
            private float cameraPointOffset;


            private bool isMainCameraControlledByTrain;

            public bool IsCameraControlledByTrain
            {
                get { return isMainCameraControlledByTrain; }
                set { isMainCameraControlledByTrain = value; }
            }

            public float Speed2Side { set { speed2SideT = value; } }
            public float Main2Add { set { main2AddT = value; } }

            public CameraParameters SideCamera { set { this.sideCam = value; } }

            public void AddCamera(CameraParameters parameters, Vector3 target)
            {
                this.additionalCam = parameters;
                this.additionalTarget = target;
            }

            public void AddCamera(CamDock dock)
            {
                AddCamera(dock.CameraParams, dock.transform.position);
            }

            /// <summary>
            /// цель камеры поезда
            /// </summary>
            private WaySystem.WayChunkMovingPoint cameraMovingPoint;

            /// <summary>
            /// оффсет от позиции поезда по углу наклона (едем горизонтально/ вниз/ под наклоном)
            /// </summary>
            public float angleOffset;

            /// <summary>
            /// камера медленно возвращается после удара поезда
            /// </summary>
            private bool isInReturnMode;

            public override void Update()
            {
                 if (!isMainCameraControlledByTrain)
                    return;

                if (train.mainStates.levelType == LevelType.Flipper)
                {
                    if (train.mainStates.mainState != MainState.Falling)
                    {
                        cameraPointOffset = 0;
                        //В дефолтном режиме камеры камера интерполируется в зависимости от
                        //ускорения
                        if (!isInReturnMode)
                        {
                            speedT = train.speedControlBlock.LowMainSpeedsT;
                        }
                        //при ударе, камера должна сначала мягко догнать
                        //текущее значение интерполяции от ускорения
                        else
                        {
                            speedT = Mathf.Lerp(
                                speedT,
                                train.speedControlBlock.LowMainSpeedsT,
                                Time.deltaTime * GameStarter.GameParameters.TrainShared.CameraReturnSpeed);

                            //выходим из режима мягкого возврата, догнали целевые значения
                            if (speedT - train.speedControlBlock.LowMainSpeedsT <= 0.05)
                            {
                                isInReturnMode = false;
                                speedT = train.speedControlBlock.LowMainSpeedsT;
                            }
                        }

                        //скорость
                        resultParameters.Lerp(
                            speedA, speedB,
                            speedT,
                                ref resultParameters);

                        //доп камера поезда
                        resultParameters.Lerp(
                            resultParameters, sideCam,
                            speed2SideT,
                            ref resultParameters);

                        //доп камера
                        resultParameters.Lerp(
                            resultParameters, additionalCam,
                            main2AddT,
                            ref resultParameters);

                        cameraPointOffset =
                            Mathf.Lerp(
                                GameStarter.GameParameters.TrainShared.CameraLowSpeedOffset,
                                GameStarter.GameParameters.TrainShared.CameraHighSpeedOffset,
                                speedT);

                        angleOffset = 0;
                        //если едем вниз, видим дальше
                        if (train.Angle > 0)
                        {
                            angleOffset =
                                GameStarter.GameParameters.TrainShared.CameraAngleOffset.GetLerpedValue(
                                    Mathf.Abs(train.Angle), 0f);
                        }
                        cameraPointOffset += angleOffset;

                        cameraMovingPoint.PlaceOffsetedFromOther(
                            train.wayMovingPoint, cameraPointOffset);

                        //желаемая финальная позиция камеры
                        Vector3 myTarget =
                            Vector3.Lerp(
                                cameraMovingPoint.Point.Position,
                                additionalTarget,
                                main2AddT);

                        //Догоняем эту позицию, пытаемся смягчить движение камеры
                        GameStarter.Instance.MainComponents.GameCamera.transform.position =
                            Vector3.Lerp(
                                GameStarter.Instance.MainComponents.GameCamera.transform.position,
                                myTarget,
                                Time.deltaTime * GameStarter.GameParameters.TrainShared.CameraMoveSpeed);

                        //применение параметров
                        GameStarter.Instance.MainComponents.GameCamera.ApplyParams(
                            resultParameters);
                    }
                }
                else
                {
                    //update camera in town...
                }
            }

            public void PlaceCameraImmediate(Vector3 position, MTParameters.CameraParameters camParameters)
            {
                GameStarter.Instance.MainComponents.GameCamera.transform.position = position;
                GameStarter.Instance.MainComponents.GameCamera.CheckAndApplyParams(camParameters);
            }

            public override void Reset()
            {
                base.Reset();
                isMainCameraControlledByTrain = false;
                speed2SideT = 0;
                speedT = 0f;
            }

            public override void Resurrect()
            {
                base.Resurrect();
                cameraMovingPoint.Place(train.mainStates.semaphore.WayChunk, train.mainStates.semaphore.Offset);
                speedT = 0;
                PlaceCameraImmediate(cameraMovingPoint.Point.Position, speedA);
            }

            public override void Hit()
            {
                base.Hit();
                isInReturnMode = true;
            }
            /*
            private class LinearInteroplation<T>
            {
                public LinearInteroplation(T result)
                {
                    this.result = result;
                }

                protected float t;
                protected T result;

                public T ValueA { get { return GetA(); } set { SetA(value); } }

                public T ValueB { get { return GetB(); } set { SetB(value); } }

                public float Factor { get { return t; } set { Lerp(t); } }

                public T Value { get { return result; } }

                public virtual void Reset()
                {
                    this.t = 0f;
                }

                private void Lerp(float t)
                {
                    this.t = t;
                    LerpProcess(ValueA, ValueB, t, ref result);
                }

                protected virtual void SetA(T value) { }

                protected virtual void SetB(T value) { }

                protected virtual T GetA() { return default; }

                protected virtual T GetB() { return default; }


                protected virtual void LerpProcess(T valueA, T valueB, float t, ref T r)
                {

                }
            }


            private class LerpCameraParameters : LinearInteroplation<CameraParameters>
            {
                public LerpCameraParameters(CameraParameters result) : base(result) {}
            }

            private class TrainCameraParameters : LinearInteroplation<CameraParameters>

            private class PCamera
            {
                public CameraParameters Parameters { get { return GetParameters(); } };
                public Vector3 Target { get { return GetTarget(); } }

                public virtual CameraParameters GetParameters()
                {
                    return null;
                }

                public virtual Vector3 GetTarget()
                {
                    return Vector3.zero;
                }
            }

            private class TrainFollowCameras : PCamera
            {
                public TrainFollowCameras()
                {
                    speedA = new CameraParameters();
                    speedB = new CameraParameters();
                    side = new CameraParameters();
                    result = new CameraParameters();
                }

                public void AssignSpeedCameras(CameraParameters speedA, CameraParameters speedB)
                {
                    this.speedA = speedA;
                    this.speedB = speedB;
                }

                public float SpeedA2SpeedB { get { return speedT; } set { speedT = value; } }
                public float Speed2Side { get { return speedToSideT; } set { speedToSideT = value; } }


                public CameraParameters Side { set { side = value; } }

                private CameraParameters speedA;
                private CameraParameters speedB;
                private CameraParameters side;
                private CameraParameters result;

                private float speedT;
                private float speedToSideT;

                public override CameraParameters GetParameters()
                {
                    result.Lerp(speedA, speedB, speedT, ref result);
                    result.Lerp(result, side, speedToSideT, ref result);
                    return result;
                }

                public override Vector3 GetTarget()
                {
                    return base.GetTarget();
                }
            }
            */

        }
    }
}