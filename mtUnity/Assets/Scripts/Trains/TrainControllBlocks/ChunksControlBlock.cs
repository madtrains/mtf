﻿using JetBrains.Annotations;
using System.Collections.Generic;
using UnityEngine;
using UnityEngineInternal;
using WaySystem;

namespace Trains
{
    public partial class Train
    {
        private class ChunksControlBlock : TrainControllBlock
        {
            public static readonly int BEHIND_TRAIN_LENGTH = 20;
            public static readonly int FLIP_CHUNKS_PROCESS_LENGTH = 30; //расстояние на проверку чанков на флип
            public static readonly int ADDITIONAL_FRONT_LENGTH = 30; //расстоние от последнего чанка на флип на проверки

            //public UnityEngine.Events.UnityAction<StoffActionType, StoffActionType> OnChageStoffActionType;
            public bool FlippableRails { get { return flippableRails; } }
            public WaySystem.StoffActionType StoffActionType { get { return stoffActionType; } }
            public WaySystem.WayChunkFlipDirections FlipDirections { get { return flipDirections; } }
            public OberList<WaySystem.WayChunk> FlipChunks { get { return flipChunks; } }
            public bool DoubleFlipEnabled { get { return doubleFlipEnabled; } } 

            private bool flippableRails;
            private WaySystem.WayChunkFlipDirections flipDirections;
            private StoffActionType stoffActionType;
            private OberList<WaySystem.WayChunk> flipChunks;
            private List<StoffActionType> stoffActionTypes;
            private WaySystem.WayChunkScoutPoint scoutPoint;
            private bool doubleFlipEnabled;


            public ChunksControlBlock(Train train) : base(train)
            { 
                this.scoutPoint = new WayChunkScoutPoint();
            }

            public void Place(WayChunk wayChunk, float locoOffset)
            {
                this.scoutPoint.Place(wayChunk, locoOffset);
            }

            public override void Update()
            {
                base.Update();
                flippableRails = true;
                flipDirections = WaySystem.WayChunkFlipDirections.DefaultBothSides;
                flipChunks = new OberList<WaySystem.WayChunk>();
                scoutPoint.Off();

                ///назад от переднего края включительно
                scoutPoint.PlaceOffsetedFromOther(train.wayMovingPoint, (train.visual.Length / 2f));
                stoffActionTypes = new List<StoffActionType>();
                ProcessUnderLocoChunk(scoutPoint.WayChunk);
                scoutPoint.On((waychunk) =>
                {
                    ProcessUnderLocoChunk(scoutPoint.WayChunk);
                }, -train.visual.Length);

                StoffActionType target = stoffActionType;
                bool uniquePresent =
                    stoffActionTypes.Count > 0;

                if (!uniquePresent)
                {
                    if (stoffActionType != StoffActionType.DefaultHorn)
                        ChangeActionType(StoffActionType.DefaultHorn);
                }
                else
                {
                    foreach(StoffActionType targetStoffActionType in stoffActionTypes)
                    {
                        if (targetStoffActionType != this.stoffActionType)
                        {
                            ChangeActionType(targetStoffActionType);
                            break;
                        }
                    }
                }

                scoutPoint.On((waychunk) =>
                {
                    ProcessUnderTrainChunk(waychunk);
                }, (train.Length - train.visual.Length - 0.2f) * -1f);
                scoutPoint.On((waychunk) =>
                {
                    ProcessBehindChunk(waychunk);
                }, BEHIND_TRAIN_LENGTH * -1f);

                scoutPoint.On((waychunk) =>
                {
                    ProcessCleanUpChunk(waychunk);
                }, -3f);

                ///вперёд от переднего края
                scoutPoint.Off();
                scoutPoint.PlaceOffsetedFromOther(train.wayMovingPoint, train.visual.Length / 2f);
                //flip
                bool flipBreak = false;
                scoutPoint.On((waychunk) =>
                {
                    ProcessEveryChunk(waychunk);
                    //SetMat(waychunk, 0.3f, Color.blue);
                    if (flippableRails)
                    {
                        if (waychunk.FlipType == WaySystem.WayChunkFlipType.Always && !flipBreak)
                        {
                            flipChunks.Add(waychunk);
                        }
                        else
                        {
                            flipBreak = true;
                        }
                    }
                }, FLIP_CHUNKS_PROCESS_LENGTH);

                scoutPoint.On((waychunk) =>
                {
                    ProcessEveryChunk(waychunk);
                    //SetMat(waychunk, 0.3f, Color.yellow);
                }, ADDITIONAL_FRONT_LENGTH);
                scoutPoint.Off();

                GameStarter.Instance.UIRoot.Flipper.SetInfo(
                    flippableRails, doubleFlipEnabled,
                    stoffActionType == WaySystem.StoffActionType.Nothing);
                //Debug.LogWarningFormat("Flip: {0}", FlipDirections);
            }

            private void ProcessUnderLocoChunk(WaySystem.WayChunk wayChunk)
            {
                if (wayChunk.ActionType != StoffActionType.DefaultHorn)
                {
                    stoffActionTypes.Add(wayChunk.ActionType);
                }
                ProcessUnderTrainChunk(wayChunk);
            }   

            private void ChangeActionType(StoffActionType to)
            {
                StoffActionType previous = stoffActionType;
                stoffActionType = to;

                if (stoffActionType != StoffActionType.DefaultHorn)
                {
                    train.stoffControlBlock.BreakHorn();
                }

                //Debug.LogWarningFormat("Change from {0} to {1}", previous, to);
            }

            private void ProcessUnderTrainChunk(WaySystem.WayChunk chunk)
            {
                //SetMat(chunk, 0.2f, Color.green);
                if (flippableRails)
                {
                    if (chunk.FlipType == WaySystem.WayChunkFlipType.Never)
                    {
                        flippableRails = false;
                        return;
                    }
                    flipChunks.Add(chunk);

                    if (flipDirections == WaySystem.WayChunkFlipDirections.DefaultBothSides)
                    {
                        if (chunk.FlipDirections != WayChunkFlipDirections.DefaultBothSides)
                            flipDirections = chunk.FlipDirections;
                    }
                }
                doubleFlipEnabled = flipDirections == WayChunkFlipDirections.DefaultBothSides;
                ProcessEveryChunk(chunk);
            }

            private void ProcessBehindChunk(WaySystem.WayChunk wayChunk)
            {
                //SetMat(wayChunk, 0.2f, Color.yellow);
                ProcessEveryChunk(wayChunk);
            }

            private void ProcessEveryChunk(WaySystem.WayChunk wayChunk)
            {
            }

            private void ProcessCleanUpChunk(WaySystem.WayChunk wayChunk)
            {
                //SetMat(wayChunk, 0.5f, Color.red);
            }

            public override void Reset()
            {
                base.Reset();
            }
        }
    }
}
