﻿using MTCore;
using WaySystem;

namespace Trains
{
    public partial class Train
    {
        private class StoffControlBlock : TrainControllBlock
        {
            public StoffControlBlock(Train train) : base(train) { }
            public Horn Horn {get { return horn; } set { horn = value; } }

            public bool Locked { set { _stoffIsLocked = value; } }

            public bool Refillable { set { _isRefillable = value; } }

            private bool _stoffIsLocked;
            private bool stoffTargetSwitchMode;
            private bool _isOn;
            private bool _isRefillable;
            private float _stoff = 1f;
            private float _counter;
            private Horn horn;

            public void Press()
            {
                if (_stoffIsLocked)
                    return;

                stoffTargetSwitchMode = true;
                if (IsPossible)
                {
                    GameStarter.Instance.GamePlayManager.UpdateCollection(MTDataCollection.Horn_time);
					MainSwitcher = true;
                }
                else
                {
                    //ложный вызов, не могём
                    Impossible();
                }
            }

            public void Release()
            {
                stoffTargetSwitchMode = false;
            }

            public override void Update()
            {
                base.Update();
                _isRefillable = train.mainStates.mainState == MainState.Move;
                //при запрете гудка выключаем его
                if (this._stoffIsLocked)
                {
                    MainSwitcher = false;
                    return;
                }

                if (_isOn)
                {
                    _stoff -= (GameStarter.GameParameters.TrainShared.SpeedDown * UnityEngine.Time.deltaTime);
                    _counter += UnityEngine.Time.deltaTime;
                    if (_counter >=
                        GameStarter.GameParameters.TrainShared.OneShotTime && !stoffTargetSwitchMode)
                    {
                        MainSwitcher = false;
                    }
                    if (_stoff <= 0)
                    {
                        //"топливо" закончилось при нажатой кнопке
                        _stoff = 0f;
                        MainSwitcher = false;
                        DuringActionCanceled();
                    }
                }
                else
                {
                    //пополнение
                    if (_isRefillable && _stoff < 1f)
                    {
                        _stoff += (GameStarter.GameParameters.TrainShared.SpeedRefill * UnityEngine.Time.deltaTime);
                        if (_stoff > 1)
                            _stoff = 1f;
                    }
                }
                MTUi.Flipper.Horn.HornColors hornColor = MTUi.Flipper.Horn.HornColors.Green;
                if (_isOn)
                    hornColor = MTUi.Flipper.Horn.HornColors.Yellow;
                if (!IsPossible)
                    hornColor = MTUi.Flipper.Horn.HornColors.Red;
                GameStarter.Instance.UIRoot.Flipper.Horn.Set(_stoff, hornColor);

            }

            public void AddByFlip()
            {
                if (_stoff <= 1f)
                    _stoff += GameStarter.GameParameters.TrainShared.FlipRefillValue;
            }

            private bool MainSwitcher
            {
                set
                {
                    if (_isOn != value)
                    {
                        _isOn = value;
                        switch(train.chunksControlBlock.StoffActionType)
                        {
                            case(StoffActionType.DefaultHorn):
                            {
                                horn.Switch(value);
                                break;
                            }

                            default:
                                break;
                        }
                        //включаем
                        if (_isOn)
                        {

                        }
                        //выключаем
                        else
                        {
                            _counter = 0f;
                        }
                    }
                }
            }

            public void BreakHorn()
            {
                if (horn.On)
                    horn.Switch(false);
            }

            private bool IsPossible
            {
                get
                {
                    if (_stoffIsLocked)
                        return false;
                    return _stoff > GameStarter.GameParameters.TrainShared.OneShotValue;
                }
            }

            private void Impossible()
            {
                if (train.chunksControlBlock.StoffActionType == StoffActionType.DefaultHorn)
                    horn.Impossible();
            }

            private void DuringActionCanceled()
            {
                if (train.chunksControlBlock.StoffActionType == StoffActionType.DefaultHorn)
                    horn.DuringActionCanceled();
            }

            public override void Resurrect()
            {
                base.Resurrect();
                _stoff = 1f;
                _counter = 0f;
            }
        }
    }
}