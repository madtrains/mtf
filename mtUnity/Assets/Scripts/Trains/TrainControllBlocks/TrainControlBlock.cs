﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MTRails;
using System;
using System.Text;
using System.Threading.Tasks;

namespace Trains
{
    public partial class Train
    {
        [System.Serializable]
        private class TrainControllBlock
        {
            public TrainControllBlock(Train train)
            {
                this.train = train;
            }

            protected Train train;

            public virtual void Update() {}

            public virtual void Resurrect() {}

            public virtual void Reset() {}

            public virtual void Hit() {}
        }
    }
}