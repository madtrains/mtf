﻿using System;
using System.Collections.Generic;
using System.Linq;
using Trains.Load;
using UnityEngine;
using WaySystem;
using WaySystem.Additions;
using WaySystem.Cargos;

namespace Trains
{
    public enum LOADS_ENUM
    {
        PASSENGERS = 1,
        MAIL = 2,
        WOOD = 5,
        FREIGHT = 7
    }

public partial class Train
{
        public void RecalculateCoaches()
        {
            cargoControlBlock.Recalculate();
        }

        private class CargoControlBlock : TrainControllBlock
        {
            public CargoControlBlock(Train train) : base(train)
            {
                Create();
                CreateResourcesCollected();
            }

            private bool platformAssigned;
            private Cargo cargo;

            private PassengerBunch passengerBunch;
            private MailBunch mailBunch;
            private FreightBunch freightBunch;
            private WoodBunch woodBunch;

            private Dictionary<int, ICoachLoadBunch> bunches;
            private Dictionary<int, int> currentRoundResources;
            private Dictionary<int, int> allResources;
            private Dictionary<WaySystem.PickUp.Type, int> roundPickUps;
            
            private int _rounds;
            private int _coins;
            private int _cogCoins;
            private int _gearRepair;
            private float _weight;
            private float _overWeight;
            private float _tractionRatio;

            public Dictionary<int, int> AllResourcesGathered { get { return allResources; } }

            /// <summary>
            /// обработка ресурсов в рамках собранных подбирашек
            /// </summary>
            /// <param name="pickUpType">тип подбирашки</param>
            public void PickUp(WaySystem.PickUp.Type pickUpType)
            {
                roundPickUps[pickUpType] += 1;
                switch(pickUpType) 
                {
                    case (WaySystem.PickUp.Type.Coin):
                    {
                        currentRoundResources[0] += 1;
                        //gui
                        GameStarter.Instance.UIRoot.Flipper.AddCoinEvent();
                        GameStarter.Instance.UIRoot.Flipper.Coins = currentRoundResources[0];
                        //train.tripLogger.AddCoin();
                        break;
                    }

                    case (WaySystem.PickUp.Type.CoinSilver):
                    {
                        currentRoundResources[0] += 10;
                        //gui
                        GameStarter.Instance.UIRoot.Flipper.AddCoinEvent();
                        GameStarter.Instance.UIRoot.Flipper.Coins = currentRoundResources[0];
                        break;
                    }

                    case (WaySystem.PickUp.Type.CoinGold):
                    {
                        currentRoundResources[0] += 100;
                        //gui
                        GameStarter.Instance.UIRoot.Flipper.AddCoinEvent();
                        GameStarter.Instance.UIRoot.Flipper.Coins = currentRoundResources[0];
                        break;
                    }

                    case (WaySystem.PickUp.Type.Ticket):
                    {
                        GameStarter.Instance.PlayerManager.AddTickets(1);
                        currentRoundResources[8] += 1;
                        break;
                    }

                    case (WaySystem.PickUp.Type.CogCoin):
                    {
                        currentRoundResources[9] += 1;
                        GameStarter.Instance.UIRoot.Flipper.CogCoins = currentRoundResources[9];
                        break;
                    }
                }
            }
            public bool IsPassengersLoaded { get => passengerBunch.Active && passengerBunch.Loaded > 0; }
            public bool IsFreightPlaceLeft { get => freightBunch.Active && freightBunch.CanLoad; }

            public bool IsWoodLoaded { get => woodBunch.Active && woodBunch.Loaded > 0; }

            public float Weight { get { return _weight; } }

            public float Overweight { get { return _overWeight; } }
            public float TractionRatio {  get { return _tractionRatio ; } }

            public int Coins
            {
                set
                {
                    currentRoundResources[0] = value;
                    _coins = value;
                }
                get { return _coins; }
            }

            public int CogCoins
            {
                set
                {
                    currentRoundResources[9] = value;
                    _cogCoins = value;
                }
                get { return _cogCoins; }
            }

            public int GearRepair
            {
                set
                {
                    _gearRepair = value;
                }
                get { return _gearRepair; }
            }

            public bool PlatformAssigned { get { return platformAssigned; } }

            public Dictionary<int, int> CloseFlipperRound()
            {
                _rounds += 1;
                GameStarter.Instance.UIRoot.ChangeRounds(_rounds);

                Dictionary<int, int> result = new Dictionary<int, int>(currentRoundResources);
                foreach(KeyValuePair<int, int> pair in result) 
                {
                    if(pair.Value > 0)
                    {
                        SaveResourcesLoadToProfile(pair.Key, pair.Value, false);
                        allResources[pair.Key] += currentRoundResources[pair.Key];
                        currentRoundResources[pair.Key] = 0;
                    }
                }
                Coins = 0;
                CogCoins = 0;
                return result;
            }

            public Dictionary<int, int> LeaveFlipperAndExitToTown()
            {
                _rounds = 0;
                GameStarter.Instance.UIRoot.ChangeRounds(_rounds);

                Dictionary<int, int> result = new Dictionary<int, int>(allResources);
                foreach(KeyValuePair<int, int> pair in result)
                {
                    if(pair.Value > 0)
                    {
                        SaveResourcesLoadToProfile(pair.Key, pair.Value, true);
                        allResources[pair.Key] = 0;
                    }
                }
                return result;
            }

            private void SaveResourcesLoadToProfile(int resourceID, int value, bool isExitToTown)
            {
                switch(resourceID)
                {
                    default:
                    {
                        //продолжаем следующий раунд
                        if (!isExitToTown)
                        {
                            GameStarter.Instance.PlayerManager.ResourceIncreaseAmount(resourceID, value);
                        }
                        break;
                    }

                    //дерево сейвится только при выезде в город
                    case (int)LOADS_ENUM.WOOD:
                    {
                        if(isExitToTown)
                        {
                            GameStarter.Instance.PlayerManager.ResourceIncreaseAmount(resourceID, value);
                        }
                        break;
                    }
                };
            }

            public int GetCurrentRoundResourceGathered(int resourceID)
            {
                return currentRoundResources[resourceID];
            }

            public int GetAllResourceGathered(int resourceID)
            {
                return allResources[resourceID];
            }

            public bool Active(int resourceID)
            {
                ICoachLoadBunch bunch = GetBunchByResourceID(resourceID);
                return bunch != null && bunch.Active;
            }

            //был тап, мы у платформы
            public void Tap()
            {
                ICoachLoadBunch coachLoadBunch = GetBunchByResourceID(cargo.ResourceID);
                if (coachLoadBunch != null)
                {
                    coachLoadBunch.PlatformTap(cargo);
                }
            }

            public void CatchGeneratedCargo(CargoGenerators cargoGenerators)
            {
                ICoachLoadBunch coachLoadBunch = GetBunchByResourceID(cargoGenerators.ResourceID);
                if(coachLoadBunch != null)
                {
                    coachLoadBunch.ReceiveCargo(cargoGenerators);
                }
            }

            public void StationArrive(StationManager stationManager)
            {
                GameStarter.GameParameters.CycleResourceIDs((resourceID) =>
                {
                    Platform platform = stationManager.GetPlatform(resourceID);
                    if (platform != null) 
                    {
                        ICoachLoadBunch coachLoadBunch = GetBunchByResourceID(platform.Cargo.ResourceID);
                        if (coachLoadBunch != null)
                        {
                            coachLoadBunch.PlatformApproach(platform);
                        }
                    }
                });
            }

            public void ExitUnloadPassenger(CargoPassengers plarformPassengers)
            {
                passengerBunch.UnloadCargo(plarformPassengers);
            }

            public void RefillLoadFreight(CargoGenerators cargoGenerators)
            {
                freightBunch.ReceiveCargo(cargoGenerators);
            }

            public void WoodUnload(CargoBunch cargoBunch)
            {
                woodBunch.SetUnloadLimit(1, woodBunch.Loaded);
                woodBunch.UnloadCargo(cargoBunch);
            }

            /// <summary>
            /// прибытие на станцию в конце уровня 
            /// </summary>
            /// <param name="exitManager">менеджер выхода</param>
            public void ExitArrive(ExitManager exitManager)
            {
                passengerBunch.SetUnloadLimit(1f, 200);
            }

            public void PlatformOnGreenLight(Platform platform)
            {
                platformAssigned = true;
                cargo = platform.Cargo;
                platform.IsOpened = true;
                train.Sounds.Semaphore(true);
                //int resourceID = platform.CargoResourceID;
            }

            public void PlatformOffRedLight(Platform platform)
            {
                this.platformAssigned = false;
                this.cargo = null;
                platform.IsOpened = false;
                train.Sounds.Semaphore(false);
            }

            /// <summary>
            /// Обнуляет счётчики собранного только в этом раунде, переводя в общую копилку собранного за все поездки этой сессии во флиппере
            /// </summary>
            

            public override void Reset()
            {
                base.Reset();
                CreateResourcesCollected();
                Coins = 0;
                CogCoins = 0;
                GearRepair = 0;
            }

            /// <summary>
            /// перед каждым заездом во флиппер из города или при запуске игры
            /// </summary>
            public void InitForFlipper()
            {
                Debug.LogWarningFormat("Cargo Init For flipper. Refill Freight, Reset Wood");
                freightBunch.RefillSilently();
                woodBunch.Reset();
            }

            /// <summary>
            /// вызывается при изменении количества или состава вагонов
            /// </summary>
            public void Recalculate()
            {
                Create();
            }

            public void ZeroCoins()
            {
                CogCoins = 0;
                GameStarter.Instance.UIRoot.Flipper.CogCoins = 0;
                Coins = 0;
                GameStarter.Instance.UIRoot.Flipper.Coins = 0;
            }

            private ICoachLoadBunch GetBunchByResourceID(int resourceID)
            {
                if(bunches.ContainsKey(resourceID))
                    return bunches[resourceID] ;

                return null;
            }
            
            private void Create()
            {
                bunches = new Dictionary<int, ICoachLoadBunch>();
                List<Coach> passengersCoaches = 
                    train.coaches.Where(coach => coach.Parameters != null && coach.Parameters.Resources.Contains((int)LOADS_ENUM.PASSENGERS)).ToList();
                passengerBunch = new PassengerBunch(passengersCoaches);
                passengerBunch.OnCountResult += AddRoundResources;
                bunches.Add((int)LOADS_ENUM.PASSENGERS, passengerBunch);


                List<Coach> mailCoaches =
                    train.coaches.Where(coach => coach.Parameters != null && coach.Parameters.Resources.Contains((int)LOADS_ENUM.MAIL)).ToList();
                mailBunch = new MailBunch(mailCoaches);
                mailBunch.OnCountResult += AddRoundResources;
                bunches.Add((int)LOADS_ENUM.MAIL, mailBunch);
             

                List<Coach> freightCoaches =
                    train.coaches.Where(coach => coach.Parameters != null && coach.Parameters.Resources.Contains((int)LOADS_ENUM.FREIGHT)).ToList();
                freightBunch = new FreightBunch(freightCoaches);
                freightBunch.OnCountResult += AddRoundResources;
                bunches.Add((int)LOADS_ENUM.FREIGHT, freightBunch);

                List<Coach> woodCoaches =
                    train.coaches.Where(coach => coach.Parameters != null && coach.Parameters.Resources.Contains((int)LOADS_ENUM.WOOD)).ToList();
                woodBunch = new WoodBunch(woodCoaches);
                woodBunch.OnCountResult += AddRoundResources;
                bunches.Add((int)LOADS_ENUM.WOOD, woodBunch);


                if (train.coaches.Count > 0 && this.train.TrainParameters != null)
                {
                    this._weight = 0f;
                    this._overWeight = 0f;
                    this._tractionRatio = 0f;
                    foreach (Coach coach in train.coaches)
                    {
                        if (coach.Parameters != null)
                        {
                            _weight += coach.Parameters.Weight;
                        }
                    }
                    this._tractionRatio = this._weight / this.train.TrainParameters.TractionPower;
                    GameStarter.Instance.UIRoot.Town.SetWeight(this._tractionRatio);
                    if (GameStarter.Instance.DebugManager.DebugSettings.isActive && GameStarter.Instance.DebugManager.DebugSettings.FlipperAdditionalInfo)
                    {
                        Debug.LogFormat("Weight: {0} (of {2}) Ratio: {1}", 
                            _weight, 
                            _tractionRatio, 
                            this.train.TrainParameters.TractionPower);
                    }
                }
            }

            private void AddRoundResources((int resourceID, int value) values)
            {
                currentRoundResources[values.resourceID] += values.value;
            }

            private void CreateResourcesCollected()
            {
                currentRoundResources = new Dictionary<int, int>();
                roundPickUps = new Dictionary<WaySystem.PickUp.Type, int>();
                allResources = new Dictionary<int, int>();
                GameStarter.GameParameters.CycleResourceIDs((resourceID) =>
                {
                    currentRoundResources[resourceID] = 0;
                    allResources[resourceID] = 0;
                });
                foreach (WaySystem.PickUp.Type tp in Enum.GetValues(typeof(WaySystem.PickUp.Type)))
                {
                    roundPickUps[tp] = 0;
                }
            }


            

            private delegate void CoachDelegate(Coach coach);
            #region Current SubClasses
            /// <summary>
            /// базовый класс для грузов со сложной логикой загрузки выгрузки - пассажиров и фрахта
            /// </summary>
            #endregion
        }
    }
}
