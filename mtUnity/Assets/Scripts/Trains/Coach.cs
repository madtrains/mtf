﻿namespace Trains
{
    public class Coach : TrainCoachProto
    {
        /// <summary>
        /// On Load event, <loaded, capacity, unloadLimit>
        /// </summary>
        //public UnityEngine.Events.UnityAction<int, int, int> OnLoadUndload;
        public UnityEngine.Events.UnityAction<int, int> OnResourceDone;

        private Train train;
        private float locomotiveOffset;
        private float tailOffset;
        private MTParameters.Coach parameters;

        public Train Train { get { return train; } }

        public MTParameters.Coach Parameters { get { return parameters; } }

        public float LocomotiveOffset
        {
            get { return locomotiveOffset; }
            set
            {
                locomotiveOffset = value;
                tailOffset = locomotiveOffset + this.Visual.RearOffset;
            }
        }

        public float TailOffset { get { return tailOffset; } }

        /// <summary>
        /// Инициализация вагона, связь с поездом, назначение параметров и клонирование модельки
        /// </summary>
        /// <param name="train"></param>
        /// <param name="parameters"></param>
        /// <param name="visual"></param>
        public Visual Init(Train train, MTParameters.Coach parameters, Visual visual)
        {
            this.train = train;
            this.parameters = parameters;
            return this.AssignParentVisual(visual, true);
        }
    }
}