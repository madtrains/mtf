﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System.Net.Http;
using System.Collections;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Reflection;
using System.IO;


[System.Serializable]
public partial class ProtoParameters
{
	public static T FromJson<T>(string jsonString) where T : ProtoParameters
	{
		return JsonUtility.FromJson<T>(jsonString);
	}


	public static bool AreEqual(ProtoParameters a, ProtoParameters b)
	{
		if (a.Hash == b.Hash)
			return true;

		return false;
	}


	public int Hash
	{
		get
		{
			return this.ToString().GetHashCode();
		}
	}


	public string Json
	{
		get
		{
			return this.ToJson();
		}
	}


	public override string ToString()
    {
        return this.ToJson();
    }


	protected virtual string ToJson()
	{
		string result = JsonUtility.ToJson(this, true);
		return result;
	}


	public virtual byte[] GetBytes()
	{
		string json = this.ToJson();
		byte[] bytes = Encoding.UTF8.GetBytes(json);
		return bytes;
	}


	public virtual void Write(string path)
	{
		byte[] bytesJson = this.GetBytes();
		File.WriteAllBytes(path, bytesJson);
	}
}
