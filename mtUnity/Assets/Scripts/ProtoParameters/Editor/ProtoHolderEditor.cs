﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Text;


public class ProtoHolderEditor : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI ();
		ProtoHolder holder = target as ProtoHolder;
		EditorGUILayout.HelpBox(string.Format("Version: {0}.{1}",
			holder.manifest.VersionMajor, holder.manifest.VersionMinor),
			MessageType.Info);

		EditorGUILayout.HelpBox(string.Format("Hash: {0}",
			holder.manifest.Hash),
			MessageType.Info);

		if (holder.NeedsValidation)
		{
			//_ = MTAssets.AssetEditor.ProcessHolderAsync();
			holder.EndValidation();
		}
	}
}
