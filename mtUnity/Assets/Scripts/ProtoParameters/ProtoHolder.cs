﻿using UnityEngine;


public class ProtoHolder : ScriptableObject
{
	public bool NeedsValidation { get { return needsValidation; } }
	[HideInInspector] public JsonManifest manifest;
	protected bool needsValidation;


	public JsonManifest UpdateManifestAndPlusVersion<T>(T parameters, string path) where T : ProtoParameters
	{
		int major = this.manifest.VersionMajor;
		int minor = this.manifest.VersionMinor;
		minor++;

		JsonManifest jsonManifest = new JsonManifest(parameters, major, minor);
		this.manifest = jsonManifest;
		jsonManifest.Write(path);
		return jsonManifest;
	}


	protected virtual void OnValidate()
	{
		if (!needsValidation)
			needsValidation = true;
	}

	public void EndValidation()
	{
		needsValidation = false;
	}
}