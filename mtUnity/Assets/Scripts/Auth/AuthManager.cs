﻿using UnityEngine;
using System.Threading.Tasks;
using Unity.Services.Authentication;
using Unity.Services.Core;

namespace MTCore
{
	public partial class AuthManager
	{
		public string Error;
		public string PlayerId;
		public string UnityToken;
		private AuthManager()
		{

		}
		public static async Task<AuthManager> Init()
		{
#if UNITY_ANDROID
			await InitAndroid();
#endif
			Debug.Log("Initialize UnityServices");
			await UnityServices.InitializeAsync();

			var aM = new AuthManager();

			AuthenticationService.Instance.SignedIn += () =>
			{
				aM.PlayerId = AuthenticationService.Instance.PlayerId;
				//Shows how to get a playerID
				Debug.Log($"PlayedID: {AuthenticationService.Instance.PlayerId}");

				aM.UnityToken = AuthenticationService.Instance.AccessToken;

				//Shows how to get an access token
				Debug.Log($"Access Token: {AuthenticationService.Instance.AccessToken}");

				const string successMessage = "Sign in succeeded!";
				Debug.Log(successMessage);
			};

			return new AuthManager();
		}

		public async Task SignIn()
		{
#if UNITY_ANDROID
			await InitAndroid();
#else
			await SignInCachedUserAsync();
#endif
		}

		//unity login
		static async Task SignInAnonymouslyAsync()
		{
			try
			{
				await AuthenticationService.Instance.SignInAnonymouslyAsync();
				Debug.Log("Sign in anonymously succeeded!");
			}
			catch (Unity.Services.Authentication.AuthenticationException ex)
			{
				// Compare error code to AuthenticationErrorCodes
				// Notify the player with the proper error message
				Debug.LogException(ex);
			}
			catch (RequestFailedException ex)
			{
				// Compare error code to CommonErrorCodes
				// Notify the player with the proper error message
				Debug.LogException(ex);
			}
		}
		static async Task SignInWithUnityAsync(string accessToken)
		{
			try
			{
				await AuthenticationService.Instance.SignInWithUnityAsync(accessToken);
				Debug.Log("SignIn is successful.");
			}
			catch (AuthenticationException ex)
			{
				// Compare error code to AuthenticationErrorCodes
				// Notify the player with the proper error message
				Debug.LogException(ex);
			}
			catch (RequestFailedException ex)
			{
				// Compare error code to CommonErrorCodes
				// Notify the player with the proper error message
				Debug.LogException(ex);
			}
		}
		static async Task LinkWithUnityAsync(string accessToken)
		{
			try
			{
				await AuthenticationService.Instance.LinkWithUnityAsync(accessToken);
				Debug.Log("Link is successful.");
			}
			catch (AuthenticationException ex) when (ex.ErrorCode == AuthenticationErrorCodes.AccountAlreadyLinked)
			{
				// Prompt the player with an error message.
				Debug.LogError("This user is already linked with another account. Log in instead.");
			}

			catch (AuthenticationException ex)
			{
				// Compare error code to AuthenticationErrorCodes
				// Notify the player with the proper error message
				Debug.LogException(ex);
			}
			catch (RequestFailedException ex)
			{
				// Compare error code to CommonErrorCodes
				// Notify the player with the proper error message
				Debug.LogException(ex);
			}
		}
		async Task SignInCachedUserAsync()
		{
			// Check if a cached player already exists by checking if the session token exists
			if (!AuthenticationService.Instance.SessionTokenExists)
			{
				// if not, then do nothing
				return;
			}

			// Sign in Anonymously
			// This call will sign in the cached player.
			try
			{
				await AuthenticationService.Instance.SignInAnonymouslyAsync();
				Debug.Log("Sign in anonymously succeeded!");

				// Shows how to get the playerID
				Debug.Log($"PlayerID: {AuthenticationService.Instance.PlayerId}");
			}
			catch (AuthenticationException ex)
			{
				// Compare error code to AuthenticationErrorCodes
				// Notify the player with the proper error message
				Debug.LogException(ex);
			}
			catch (RequestFailedException ex)
			{
				// Compare error code to CommonErrorCodes
				// Notify the player with the proper error message
				Debug.LogException(ex);
			}
		}
	}
}
