﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTGameResources
{
	public enum GameResourceType
	{
		Wood,
		Iron,
		Gold,
		Oil
	}
	public class GameResource
	{
		public GameResourceType Type { get; set; }
		public int Amount { get; set; }
		internal GameResource() { }
		public GameResource(GameResourceType type, int amount = 0)
		{
			Type = type;
			Amount = amount;
		}
	}
	public class Wood : GameResource
	{
		private Wood()
		{
			Type = GameResourceType.Wood;
		}
		public Wood(int amount = 0)
		{
			Type = GameResourceType.Wood;
			Amount = amount;
		}
	}
	public class Iron : GameResource
	{
		private Iron()
		{
			Type = GameResourceType.Iron;
		}
		public Iron(int amount = 0)
		{
			Type = GameResourceType.Iron;
			Amount = amount;
		}
	}
	public class Gold : GameResource
	{
		private Gold()
		{
			Type = GameResourceType.Gold;
		}
		public Gold(int amount = 0)
		{
			Type = GameResourceType.Gold;
			Amount = amount;
		}
	}
	public class Oil : GameResource
	{
		private Oil()
		{
			Type = GameResourceType.Oil;
		}
		public Oil(int amount = 0)
		{
			Type = GameResourceType.Oil;
			Amount = amount;
		}
	}
}
