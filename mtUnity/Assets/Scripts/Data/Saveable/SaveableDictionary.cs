﻿using MTData;
using MTCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace MTData
{
	[SaveableProperties(name: "data.mts")]
	public abstract class SaveableDictionary : SaveableObject<ObjectData<SaveableDictionary>>
	{
		[SaveableField]
		internal string dataSet;
		private Dictionary<string, string> _dataSet;
		public SaveableDictionary(string localStoragePath) : base(localStoragePath)
		{
			_dataSet = new Dictionary<string, string>();
		}
		internal override void BeforeSave()
		{
			dataSet = CustomJsonConverter.ToJson(_dataSet);
		}
		internal override void AfterLoad()
		{
			_dataSet = CustomJsonConverter.FromJson(dataSet);
		}
		internal override void ProcessDataBeforeSave() {
			DataToSave = new ObjectData<SaveableDictionary>(this);
		}
		internal override void ProcessDataAfterSaveLoad() {
			base.DataToSave.ApplyDataToObject(this);
		}

		public Dictionary<string, string> GetAll()
		{
			return _dataSet;
		}

		public void SetAll(Dictionary<string, string> dataset)
		{
			_dataSet = dataset;
		}
		public void SetString(string key, string value)
		{
			if (!_dataSet.TryGetValue(key, out string tValue))
				_dataSet.Add(key, value.ToString());
			else
				_dataSet[key] = value.ToString();
		}
		public string GetString(string key)
		{
			if (_dataSet.TryGetValue(key, out string tValue))
			{
				if (tValue != null)
					return tValue;
			}
			return null;
		}
		public void SetInt(string key, int value)
		{
			if (!_dataSet.TryGetValue(key, out string tValue))
				_dataSet.Add(key, value.ToString());
			else
				_dataSet[key] = value.ToString();
		}
		public int GetInt(string key)
		{
			if (_dataSet.TryGetValue(key, out string tValue))
			{
				if (tValue != null && int.TryParse(tValue, out var result))
					return result;
			}
			return 0;
		}
		public void SetBool(string key, bool value)
		{
			if (!_dataSet.TryGetValue(key, out string tValue))
				_dataSet.Add(key, value.ToString());
			else
				_dataSet[key] = value.ToString();
		}
		public bool GetBool(string key)
		{
			if (_dataSet.TryGetValue(key, out string tValue))
			{
				if (tValue != null && bool.TryParse(tValue, out var result))
					return result;
			}
			return false;
		}
		public void SetObject<T>(string key, T value)
		{
			if (!_dataSet.TryGetValue(key, out string tValue))
				_dataSet.Add(key, JsonUtility.ToJson(value));
			else
				_dataSet[key] = JsonUtility.ToJson(value);

		}
		public T GetObject<T>(string key)
		{
			if (_dataSet.TryGetValue(key, out string tValue))
			{
				if (!string.IsNullOrEmpty(tValue))
					return JsonUtility.FromJson<T>(tValue);
			}
			return default(T);
		}
		public bool TryGetObject<T>(string key)
		{
			if (_dataSet.TryGetValue(key, out string tValue))
				if (!string.IsNullOrEmpty(tValue))
					return JsonUtility.FromJson<T>(tValue) is T;
			return false;

		}
		public void SetList<T>(string key, List<T> value)
		{
			var tList = typeof(T) == typeof(string)? value as List<string> : value.Select(t => JsonUtility.ToJson(t)).ToList();
			var listString = string.Join(";", tList);

			if (!_dataSet.TryGetValue(key, out string tValue))
				_dataSet.Add(key, listString);
			else
				_dataSet[key] = listString;
		}
		public List<T> GetList<T>(string key)
		{
			if (_dataSet.TryGetValue(key, out string tValue))
			{
				if (!string.IsNullOrEmpty(tValue))
				{
					var value = tValue.Split(';').ToList();
					return typeof(T) == typeof(string)? value as List<T> : value.Select(t => JsonUtility.FromJson<T>(t)).ToList();
				}
			}
			return default;
		}
		public bool TryGetList<T>(string key)
		{
			if (_dataSet.TryGetValue(key, out string tValue))
				if (!string.IsNullOrEmpty(tValue))
				{
					var value = tValue.Split(',').Select(t => JsonUtility.FromJson<T>(t)).ToList();
					return value != null;
				}
			return false;

		}
	}
}
