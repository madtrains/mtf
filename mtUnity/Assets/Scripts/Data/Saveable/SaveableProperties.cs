﻿using System;

namespace MTData
{
	[AttributeUsage(AttributeTargets.Class)]
	public class SaveableProperties : Attribute
	{
		private SaveableFileType _fileType;
		private SaveablePATH _path;
		public SaveablePATH PATH => _path;
		public SaveableFileType FILE_TYPE => _fileType;

		public SaveableProperties(string name, string folder = null, string storage = null, SaveableFileType fileType = SaveableFileType.Binary)
		{
			_path = new SaveablePATH(name, folder, storage);
			_fileType = fileType;
		}
	}

}
