﻿using System;
using System.IO;
using UnityEngine;

namespace MTData
{
	[Serializable]
	public class SaveablePATH
	{
		private const string default_storage_name = "MT_STORAGE";
		private const string default_folder_name = "MT_SAVEABLE";
		[SerializeField]
		private string storage_name;
		[SerializeField]
		private string file_folder;
		[SerializeField]
		private string file_name;
		public virtual string PATH { get { return Path.Combine(storage_name, file_folder); } }
		public virtual string NAME { get { return file_name; } set { file_name = value; } }
		public virtual string FULL_PATH { get { return Path.Combine(PATH, NAME); } }

		public SaveablePATH(string name, string folder, string storage)
		{
			if (string.IsNullOrEmpty(storage))
				storage = default_storage_name;

			if (folder == null)
				folder = default_folder_name;

			storage_name = storage;
			file_folder = folder;
			file_name = name;
		}
	}

}
