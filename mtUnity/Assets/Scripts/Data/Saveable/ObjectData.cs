﻿using MTCore;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace MTData
{
	[Serializable]
	public class ObjectData<T> where T : ISaveable
	{
		public string TYPE = "none";
		public string DATA_FIELDS;

		public ObjectData(T dataObject)
		{
			TYPE = typeof(T).Name;
			//Debug.Log($"Start saving {TYPE}");

			var objectValues = new Dictionary<string, string>();
			var fields = dataObject.SAVEABLE_FIELDS;
			//Debug.Log($"Save {fields.Count} fields");

			foreach (var field in fields)
			{
				var fieldValue = field.GetValue(dataObject);
				objectValues.Add(field.Name, fieldValue.ToString());
				//Debug.Log($"{field.Name} : {fieldValue.ToString()}");
			}
			DATA_FIELDS = CustomJsonConverter.ToJson(objectValues);
		}

		public void ApplyDataToObject(T dataObject)
		{
			var objectValues = CustomJsonConverter.FromJson(DATA_FIELDS);
			var fields = dataObject.SAVEABLE_FIELDS;
			foreach (var field in fields)
			{
				field.SetValue(dataObject, Convert.ChangeType(objectValues[field.Name], field.FieldType));
			}
		}
	}

}
