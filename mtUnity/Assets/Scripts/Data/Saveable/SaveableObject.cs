﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;

namespace MTData
{
	public class SaveableObject<T> : DataStateMachine, ISaveable
	{
		internal T DataToSave;
		private string _localStoragePath = "/";
		private SaveablePATH _saveable_path { get; set; }
		private SaveableFileType _saveable_type { get; set; }
		private StorageDataHandler _dataHandler { get; set; }
		public SaveableFileType SAVEABLE_TYPE { get { return _saveable_type; } set { _saveable_type = value; } }
		public SaveablePATH SAVEABLE_PATH { get { return _saveable_path; } set { _saveable_path = value; } }
		public List<FieldInfo> SAVEABLE_FIELDS => this.GetType().GetFields(BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance).Where(prop => prop.IsDefined(typeof(SaveableField), false)).ToList();
		public SaveableObject(string localStoragePath)
		{
			_localStoragePath = localStoragePath;

			SaveableProperties saveablePAthAttribute = (SaveableProperties)Attribute.GetCustomAttribute(this.GetType(), typeof(SaveableProperties));

			if (saveablePAthAttribute != null)
			{
				SAVEABLE_PATH = saveablePAthAttribute.PATH;
				SAVEABLE_TYPE = saveablePAthAttribute.FILE_TYPE;
			}


		}
		internal virtual void BeforeSave() { }
		internal virtual void AfterSave() { }
		internal virtual void BeforeLoad() { }
		internal virtual void AfterLoad() { }
		internal virtual void OnSave()
		{
			string path = Path.Combine(_localStoragePath, SAVEABLE_PATH.PATH);

			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}

			SaveData();
		}
		internal virtual void OnLoad()
		{
			string path = Path.Combine(_localStoragePath, SAVEABLE_PATH.FULL_PATH);
			if (!File.Exists(path))
			{
				SAVE();
			}
			var data = LoadData();
			DataToSave = data;
		}

		public virtual void SAVE(System.Action callBack = null)
		{
			ChangeStateTo(DataState.Processing);

			BeforeSave();
			ProcessDataBeforeSave();
			OnSave();
			AfterSave();

			ChangeStateTo(DataState.Ready);

			if (callBack != null)
				callBack();
		}
		public virtual void LOAD(System.Action callBack = null)
		{
			ChangeStateTo(DataState.Processing);

			BeforeLoad();
			OnLoad();
			ProcessDataAfterSaveLoad();
			AfterLoad();

			ChangeStateTo(DataState.Ready);
			if (callBack != null)
				callBack();
		}

		private void SaveData()
		{

			var fullPath = Path.Combine(_localStoragePath, SAVEABLE_PATH.FULL_PATH);

			switch (SAVEABLE_TYPE)
			{
				case SaveableFileType.Binary:

					FileStream stream = new FileStream(fullPath, FileMode.Create);
					BinaryFormatter formatter = new BinaryFormatter();
					formatter.Serialize(stream, DataToSave);
					stream.Close();

					break;
				case SaveableFileType.Text:
					File.WriteAllText(fullPath, DataToSave.ToString());
					break;
				default:
					break;
			}
		}

		private T LoadData()
		{
			string path = Path.Combine(_localStoragePath, SAVEABLE_PATH.FULL_PATH);

			switch (SAVEABLE_TYPE)
			{
				case SaveableFileType.Binary:
					BinaryFormatter formatter = new BinaryFormatter();
					FileStream stream = new FileStream(path, FileMode.Open);
					T data = (T)formatter.Deserialize(stream);
					stream.Close();
					return data;
				case SaveableFileType.Text:
					return (T)(object)File.ReadAllText(path); ;
					;
				default:
					return default(T);
			}
		}

		internal virtual void ProcessDataBeforeSave() { }
		internal virtual void ProcessDataAfterSaveLoad() { }

	}

}
