﻿using System;
using System.IO;
using UnityEngine;

namespace MTData
{
	public enum SaveableFileType
	{
		Binary,
		Text
	}

}
