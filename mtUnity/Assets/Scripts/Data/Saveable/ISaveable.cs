﻿using System.Collections.Generic;
using System.Reflection;

namespace MTData
{
	public interface ISaveable
	{
		SaveablePATH SAVEABLE_PATH { get; set; }
		List<FieldInfo> SAVEABLE_FIELDS { get; }
		void SAVE(System.Action callBack);
		void LOAD(System.Action callBack);

	}

}
