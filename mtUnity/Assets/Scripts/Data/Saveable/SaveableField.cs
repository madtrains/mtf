﻿using System;

namespace MTData
{
	[AttributeUsage(AttributeTargets.Field)]
	public class SaveableField : Attribute
	{
	}

}
