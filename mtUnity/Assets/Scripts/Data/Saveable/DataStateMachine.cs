﻿using UnityEngine;

namespace MTData
{
	public class DataStateMachine {
		public enum DataState { Processing, Ready, Error }
		public DataState State { get { return state; } private set { state = value; } }
		private DataState state = DataState.Processing;
		internal void ChangeStateTo(DataState state)
		{
			if (State == DataState.Error)
			{
				Debug.LogError($"{this.GetType().Name} state is ERROR! Cant be changed to {state.ToString()}");
				return;
			}
			State = state;
			//Debug.Log($"{this.GetType().Name} state changed to {State.ToString()}");
		}

	}

}
