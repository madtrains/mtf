﻿using MTParameters;
using MTPlayer;
using MTCore;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace MTData
{
	public class DataHadler
	{
		public AppData Settings { get; private set; }
		public PlayerData Data { get; private set; }
		public TripHistoryData TripHistory { get; private set; }
		private DataSettings _dataSettings { get; set; }
		private DataHadler(InBuildParameters inBuildParameters)
		{
			_dataSettings = new DataSettings();

			Settings = new AppData(_dataSettings.DataPath);
			Data = new PlayerData(_dataSettings);
			TripHistory = new TripHistoryData(_dataSettings.DataPath);
		}

		public static DataHadler Init(InBuildParameters inBuildParameters)
		{
			return new DataHadler(inBuildParameters);
		}

		#region data storages

		#region local storage

		public T GetLocalData<T>(string path)
		{
			object result = new object();

			FileStream file = null;
			try
			{
				BinaryFormatter bf = new BinaryFormatter();
				file = File.Open(path, FileMode.Open);
				result = (T)bf.Deserialize(file);
				return (T)result;
			}
			catch (Exception ex)
			{
				if (ex != null)
				{
					Console.WriteLine(ex.Message);
				}
			}
			finally
			{
				if (file != null)
				{
					file.Close();
				}
			}
			return default;
		}

		private void PutLocalData<T>(string path, T data)
		{
			FileStream file = null;
			try
			{
				BinaryFormatter bf = new BinaryFormatter();
				file = File.Create(path);
				bf.Serialize(file, data);
			}
			catch (Exception ex)
			{
				if (ex != null)
				{
					Console.WriteLine(ex.Message);
				}
			}
			finally
			{
				if (file != null)
				{
					file.Close();
				}
			}
		}
		#endregion

		#endregion
	}

}
