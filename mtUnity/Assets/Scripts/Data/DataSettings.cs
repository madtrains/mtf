﻿using MTData;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace MTData
{
	public class DataSettings
	{
		public bool IsInternetForbiden = false;
		//private string _server_url = "https://localhost:44379/api";
		public string _Server_url = "https://thoth-core.ru/api";
		public string UserDataPath = "/data1.dat";
		public string AuthPath = "/auth4.dat";
		public string DataPath;
		public DataSettings()
		{
#if UNITY_EDITOR
			DataPath = Directory.GetParent(Application.dataPath).FullName;
#else
			DataPath = Application.persistentDataPath;                                
#endif
		}
	}
}
