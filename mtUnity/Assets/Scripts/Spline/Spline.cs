﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

namespace MTSpline
{
	public class SplineCubic
	{
		public float TotalLength { get { return totalLength; } }

		private  int numSegments;
		private int numberOfPoints;
		private CubicCoeffs[] cx;
		private CubicCoeffs[] cy;
		private CubicCoeffs[] cz;
		private float[][] segment;            // interval-segments lengths
		private float[] fullD;                // lengths from the beginning
		private float totalLength = 0.0f;
		private int hint = 0;
        public SplineCubic(RectTransform[] rects) : this(SplineCubic.RectsPositions(rects), 10)
        {
        }

        public SplineCubic(RectTransform[] rects, int numSegments) : this(SplineCubic.RectsPositions(rects), numSegments)
		{
        }

        /// <summary>
        /// Creates a cubic spline by Vector3 coordinates of 4 and more points
        /// </summary>
        public SplineCubic(Vector3[] pointCoordinates, int numSegments)
		{
			this.numSegments = numSegments;
			if (pointCoordinates.Length < 4)
			{
				Debug.LogError("Cubic spline can not be created by less than 4 points");
				return;
			}

			float[] x = new float[pointCoordinates.Length];
			float[] y = new float[pointCoordinates.Length];
			float[] z = new float[pointCoordinates.Length];
			for (int i = 0; i < pointCoordinates.Length; i++)
			{
				x[i] = pointCoordinates[i].x;
				y[i] = pointCoordinates[i].y;
				z[i] = pointCoordinates[i].z;
			}

			cx = CalcNaturalCubic(x);
			cy = CalcNaturalCubic(y);
			cz = CalcNaturalCubic(z);
			numberOfPoints = cx.Length;

			CalculateDistances();
			//Debug.Log (this.totalLength);
		}


		public float GetNearestPoint(Vector3 position, int steps, float threshold)
		{
			List<SpecPoint> spPoints = new List<SpecPoint>();

			float step = 1f / (float)steps;
			float currentT = 0f;
			for (int i = 0; i < steps; i++)
			{
				Vector3 currentPoint = GetPointNormalized(currentT);
				float currentDistance = Vector3.Distance(position, currentPoint);
				if (currentDistance <= threshold)
					return currentT;

				SpecPoint sp = new SpecPoint(currentT, currentDistance, currentPoint);
				spPoints.Add(sp);

				currentT += step;
			}


			spPoints.Sort(delegate (SpecPoint x, SpecPoint y)
			{
				return (x.distance.CompareTo(y.distance));
			});

			return spPoints[0].t;
		}


		public Vector3[] GetDrawLinePoints(int levelOfDetalisation)
		{
			Vector3[] result = new Vector3[levelOfDetalisation];
			float segmentNormalizedLength = 1 / (float)levelOfDetalisation;
			float currentSegment = 0f;
			for (int i = 0; i < levelOfDetalisation; i++)
			{
				if (i == 34)
				{
					var c = 1;
				}
				currentSegment = currentSegment + segmentNormalizedLength;
				currentSegment = Mathf.Clamp(currentSegment, 0f, 1f);
				Vector3 currentCoordinates = this.GetPointNormalized(currentSegment);
				result[i] = currentCoordinates;
			}

			return result;
		}


		public Vector3 GetPointAbsolute(float m)
		{
			if (m < 0.0f)
				return GetPoint(0, 0.0f);

			if (m >= (totalLength - Mathf.Epsilon))
				return GetPoint(numberOfPoints - 1, 1.0f);

			FindHint(m);

			return GetPoint(hint, GetT(m));
		}

		/// <summary>
		/// Get Point Position Normilized, where 0 is the start of the spline and 1 is the end
		/// </summary>
		public Vector3 GetPointNormalized(float t)
		{
			return GetPointAbsolute(Mathf.Lerp(0, totalLength, t));
		}


		public Vector3 GetPoint(int i, float p)
		{
			Vector3 v = new Vector3(cx[i].GetValue(p), cy[i].GetValue(p), cz[i].GetValue(p));
			return v;
		}


		private void CalculateDistances()
		{
			segment = new float[numberOfPoints][];
			fullD = new float[numberOfPoints + 1];
			fullD[0] = 0.0f;

			for (int i = 0; i < numberOfPoints; i++)
			{
				float s = 0.0f;
				segment[i] = new float[numSegments];
				for (int j = 0; j < numSegments; j++)
				{
					Vector3 v1 = GetPoint(i, (float)j / numSegments);
					Vector3 v2 = GetPoint(i, (float)(j + 1) / numSegments);
					float segmentLength = (v2 - v1).magnitude;
					segment[i][j] = segmentLength;
					s += segmentLength;
				}

				fullD[i + 1] = fullD[i] + s;
			}

			totalLength = fullD[numberOfPoints];
		}


		private static CubicCoeffs[] CalcNaturalCubic(float[] x)
		{
			int n = x.Length - 1;

			float[] gamma = new float[n + 1];
			float[] delta = new float[n + 1];
			float[] D = new float[n + 1];

			gamma[0] = 0.5f;

			for (int i = 1; i < n; i++)
			{
				gamma[i] = 1.0f / (4.0f - gamma[i - 1]);
			}

			gamma[n] = 1.0f / (2.0f - gamma[n - 1]);

			delta[0] = 3.0f * (x[1] - x[0]) * gamma[0];

			for (int i = 1; i < n; i++)
			{
				delta[i] = (3.0f * (x[i + 1] - x[i - 1]) - delta[i - 1]) * gamma[i];
			}

			delta[n] = (3.0f * (x[n] - x[n - 1]) - delta[n - 1]) * gamma[n];

			D[n] = delta[n];

			for (int i = n - 1; i >= 0; i--)
			{
				D[i] = delta[i] - gamma[i] * D[i + 1];
			}

			CubicCoeffs[] C = new CubicCoeffs[n];

			for (int i = 0; i < n; i++)
			{
				C[i] = new CubicCoeffs(
					(float)x[i],
					D[i],
					3.0f * (x[i + 1] - x[i]) - 2.0f * D[i] - D[i + 1],
					2.0f * (x[i] - x[i + 1]) + D[i] + D[i + 1]);
			}

			return C;
		}

		private static Vector3[] RectsPositions(RectTransform[] rects)
		{
            Vector3[] pointCoordinates = new Vector3[rects.Length];
            for (int i = 0; i < rects.Length; i++)
            {
                pointCoordinates[i] = rects[i].transform.position;
            }
			return pointCoordinates;
        }

		private float GetT(float m)
		{
			float dist = fullD[hint];
			float t = 0.0f;
			for (int i = 0; i < numSegments; i++)
			{
				dist += segment[hint][i];

				if (dist >= m)
				{
					t = (i + (m - dist + segment[hint][i]) / segment[hint][i]) / numSegments;
					break;
				}
			}

			return t;
		}


		private void FindHint(float m)
		{
			if ((m < fullD[hint]) || (m > fullD[hint + 1]))
			{
				for (int i = 1; i <= numberOfPoints; i++)
				{
					if (m < fullD[i])
					{
						hint = i - 1;
						break;
					}
				}
			}
		}


		private class SpecPoint
		{
			public float t;
			public Vector3 position;
			public float distance;

			public SpecPoint(float t, float distance, Vector3 position)
			{
				this.t = t;
				this.distance = distance;
				this.position = position;
			}
		}


		private class CubicCoeffs
		{
			private float a;
			private	float b;
			private float c;
			private float d;

			public CubicCoeffs(float a, float b, float c, float d)
			{
				this.a = a;
				this.b = b;
				this.c = c;
				this.d = d;
			}

			public float GetValue(float u)
			{
				return (((d * u) + c) * u + b) * u + a;
			}
		}
	}
}
    
