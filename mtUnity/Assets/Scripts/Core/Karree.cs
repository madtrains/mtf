using UnityEngine;

public class Karree
{
    /// <param name="corner">position of Karee corner</param>
    /// <param name="count">number of cells</param>
    /// <param name="step">distance between two cells</param>
    /// <returns></returns>
    public static Vector3[] Create(Vector3 corner, int count, float step = 2f)
    {
        //line
        if (count < 4)
        {
            Vector3[] result = new Vector3[count];
            for (int i = 0; i < count; i++)
            {
                Vector3 current = corner + (Vector3.right * step * i);
                result[i] = current;
            }
            return result;
        }
        //square
        else
        {
            int side = Mathf.CeilToInt(Mathf.Sqrt(count));
            Vector3[] result = new Vector3[count];
            int k = 0;
            for (int i = 0; i < side; i++)
            {
                for (int j = 0; j < side; j++)
                {
                    result[k] = corner + new Vector3(step * i, 0f, step * j);
                    k++;
                    if (k >= count)
                        return result;
                }
            }
            return result;
        }
    }
}
