﻿using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class WeightedElement
{
    public string Name { get { return name; } set { name = value; } }
    public float Weight { get { return weight; }  set { weight = value; } }

    public float NormalizedWeight { get { return normalizedWeight; } set { normalizedWeight = value; } }


    [SerializeField] protected string name;
    [SerializeField] protected float weight;
    [HideInInspector] [SerializeField] protected float normalizedWeight;
}


public class WeightList<T> : List<T> where T : WeightedElement
{
    public static WeightList<T> Create(T[] arr)
    {
        WeightList<T> lst = new WeightList<T>();
        foreach (T el in arr)
        {
            lst.Add(el);
        }
        return lst;
    }

    public static WeightList<T> Create(List<T> arr)
    {
        WeightList<T> lst = new WeightList<T>();
        foreach (T el in arr)
        {
            lst.Add(el);
        }
        return lst;
    }

    protected float totalWeight = 0;


    public new virtual void Add(T newElement)
    {
        newElement.NormalizedWeight = newElement.Weight + totalWeight;
        totalWeight += newElement.Weight;
        base.Add(newElement);
        this.Sort((x, y) => x.NormalizedWeight.CompareTo(y.NormalizedWeight));
    }


    public int GetRandomIndex()
    {
        float random = UnityEngine.Random.Range(0, totalWeight);

        for (int i = 0; i < this.Count; i++)
        {
            if (random <= this[i].NormalizedWeight)
            {
                return i;
            }
        }
        return -1;
    }

    public T GetRandomElement()
    {
        float random = UnityEngine.Random.Range(0, totalWeight);

        foreach (T el in this)
        {
            if (random <= el.NormalizedWeight)
            {
                return el;
            }
        }

        return null;
    }

    public T GetElementAt(int index)
    {
        return this[index];
    }
}


public class WeightedCollectionElement : WeightedElement
{

    public int Index { get { return index; } set { index = value; } }
    public bool Unique { get { return unique; } }

    [SerializeField] protected bool unique;
    
    [HideInInspector][SerializeField] protected int index;
    
}

[System.Serializable]
public class WeightedCollection<T> where T : WeightedCollectionElement
{
    [SerializeField] private T[] collection;
    [HideInInspector] private bool inited;
    [HideInInspector] private List<T> elements;
    [HideInInspector] private List<int> used;
    [HideInInspector] private float totalWeight;

    public void Init()
    {
        if (inited)
            return;

        inited = true;
        if (collection == null || collection.Length == 0)
            Debug.LogErrorFormat("Collection is null");

        totalWeight = 0f;
        elements = new List<T>();
        used = new List<int>();

        for (int i = 0; i < collection.Length; i++)
        {
            T element = collection[i];
            AddElement(element);
        }
        elements.Sort((x, y) => x.NormalizedWeight.CompareTo(y.NormalizedWeight));
    }

    protected virtual void AddElement(T element)
    {
        this.elements.Add(element);
        element.NormalizedWeight = element.Weight + this.totalWeight;
        this.totalWeight += element.Weight;
    }

    public T CheckGet()
    {
        Init();
        return Get();
    }

    public virtual T Get()
    {
        float random = UnityEngine.Random.Range(0, totalWeight);

        foreach (T el in elements)
        {
            if (random <= el.NormalizedWeight)
            {
                if (el.Index == used[used.Count - 1] || (el.Unique && used.Contains(el.Index)))
                    return Get();
                used.Add(el.Index);
                return el;
            }
        }

        return null;
    }
}