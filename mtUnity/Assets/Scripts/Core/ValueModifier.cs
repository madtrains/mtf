﻿using UnityEngine;

[System.Serializable]
public class ValueModifier : PropertyAttribute
{
    public float Value { get { return value; } }
    [SerializeField] protected float value;

    public float Modifier { get { return modifier; } }
    [SerializeField] protected float modifier;

    public virtual float GetLerpedValue(float value)
    {
        return GetLerpedValue(value, 0f);
    }

    /// <summary>
    /// Finds lerped value by reference value.
    /// </summary>
    /// <param name="value">reference value</param>
    /// <param name="lerpFromValue">start value fo start lerp of result value from. 0 or 1, if multiplier is needed</param>
    /// <returns>lerped float value</returns>
    public virtual float GetLerpedValue(float value, float lerpFromValue)
    {
        float t = Mathf.InverseLerp(0, this.value, value);
        float result = Mathf.Lerp(lerpFromValue, this.modifier, t);
        return result;
    }
}

[System.Serializable]
public class FromToProperty : PropertyAttribute
{
    public float From { get { return from; } }
    public float To { get { return to; } }

    [SerializeField] protected float from;
    [SerializeField] protected float to;


    public float InverseLerp(float value)
    {
        return Mathf.InverseLerp(from, to, value);
    }

    public float Lerp(float value)
    {
        return Mathf.Lerp(from, to, value);
    }
}

[System.Serializable]
public class FromToModifier : FromToProperty
{
    public float Modifier { get { return modifier; } }

    [SerializeField] protected float modifier;
}