﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace MTCore
{
	public class SceneManager
	{
		private CancellationTokenSource cts;
		private static SceneManager instance;
		private System.Action sceneCallback;
		public static SceneManager Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new SceneManager();
					UnityEngine.SceneManagement.SceneManager.activeSceneChanged += OnSceneWasLoaded;
				}

				return instance;
			}
		}

		public async void LoadScene(int sceneIndex, System.Action callback = null)
		{
			var scene = UnityEngine.SceneManagement.SceneManager.GetSceneAt(sceneIndex);
			await LoadScene(scene.name, callback);
		}

		//Methods
		//Try Catch async tas 
		public async Task LoadScene(string sceneName, System.Action callback = null)
		{
			Debug.LogFormat("Starting to load scene {0}", sceneName);
			if (cts == null)
			{
				cts = new CancellationTokenSource();
				try
				{
					await PerformSceneLoading(cts.Token, sceneName);
					sceneCallback = callback;
					Debug.LogFormat("Scene {0} is loaded", sceneName);
				}
				catch (OperationCanceledException ex)
				{
					if (ex.CancellationToken == cts.Token)
					{
						//Perform operation after cancelling
						Debug.LogWarning("Loading Scene cancelled");
					}
				}
				finally
				{
					cts.Cancel();
					cts = null;
				}
			}
			else
			{
				//Cancel Previous token
				cts.Cancel();
				cts = null;
			}
		}
		//Actual Scene loading
		private async Task PerformSceneLoading(CancellationToken token, string sceneName)
		{
			token.ThrowIfCancellationRequested();
			if (token.IsCancellationRequested)
				return;

			AsyncOperation asyncOperation = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName);
			asyncOperation.allowSceneActivation = false;

			while (asyncOperation.isDone)
			{
				token.ThrowIfCancellationRequested();
				if (token.IsCancellationRequested)
					return;
				//if (asyncOperation.progress > 0.9f)
			}
			asyncOperation.allowSceneActivation = true;

			//asyncOperation.allowSceneActivation = true;
			token.ThrowIfCancellationRequested();
			if (token.IsCancellationRequested)
				return;

		}

		private static void OnSceneWasLoaded(UnityEngine.SceneManagement.Scene from, UnityEngine.SceneManagement.Scene to)
		{
			var callback = SceneManager.Instance.sceneCallback;

			if (callback != null)
				callback();
		}
	}
}
