﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UberColorizer : UberBehaviour
{
    [SerializeField] protected Renderer[] renderers;

    public void Colorize(Color color)
    {
        if (renderers == null || renderers.Length == 0) { return; }
        foreach (Renderer rend in renderers)
        {
            rend.material.color = color;
        }
    }

    public void SetRandomColor()
    {
        Colorize(MTParameters.Customization.RandomNamedColor);
    }

    
}