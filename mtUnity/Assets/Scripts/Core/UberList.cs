﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UberList<T> : List<T>
{
    public static UberList<T> Combine(UberList<T> a, UberList<T> b)
    {
        UberList<T> result = new UberList<T>();
        result.Append(a);
        result.Append(b);
        return result;
    }


    public UberList(T[] array) : base(array)
    { 
        foreach (T el in array)
        {
            base.Add(el);
        }
    }


    public UberList() : base()
    {
    }


    public static bool IsNullOrEmpty(UberList<T> list)
    {
        if (list == null || list.Count == 0)
            return true;

        return false;
    }


    public bool Empty
    {
        get
        {
            if (base.Count == 0)
                return true;
            return false;
        }
    }


    public T First
    {
        get
        {
            if (base.Count > 0)
                return base[0];

            return default;
        }
    }


    public T Last
    {
        get
        {
            if (base.Count > 0)
                return base[LastIndex];

            return default;
        }
    }


    public int LastIndex
    {
        get
        {
            return base.Count - 1;
        }
    }


    public void Append(List<T> other)
    {
        foreach(T el in other)
        {
            base.Add(el);
        }
    }

    public void AddUnique(T element)
    {
        if (this.Contains(element))
            return;

        base.Add(element);
    }


    public void PopLast()
    {
        if(base.Count > 0)
        {
            base.RemoveAt(LastIndex);
        }
    }
}