﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : UberBehaviour
{
    public void Init()
    {
        Rigidbody rigidbody = GetComponent<Rigidbody>();
        if (rigidbody == null)
        {
            rigidbody = gameObject.AddComponent<Rigidbody>();
        }
        rigidbody.useGravity = false;
        rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        Collider[] colliders = gameObject.GetComponentsInChildren<Collider>(false);
        foreach (Collider collider in colliders)
        {
            collider.isTrigger = true;
        }
    }


    protected virtual void OnTriggerEnter(Collider other)
    {
        Rigidbody rgbd = other.attachedRigidbody;
        if (rgbd == null)
            return;
        OnRigidBodyEnterTrigger(rgbd);
    }


    protected virtual void OnRigidBodyEnterTrigger(Rigidbody rgbd)
    {

    }
}