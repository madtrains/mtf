﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerBeh : UberBehaviour
{
    public static TimerBeh CreateTimer()
    {
        GameObject go = new GameObject("_timer");
        TimerBeh timer = go.AddComponent<TimerBeh>();
        return timer;
    }

    public bool SelfDestructable { get { return selfDestructable; } set { selfDestructable = value; } }

    public System.Action OnEnd;
	public System.Action<float> OnTimeChanged;
    public System.Action<int> OnStep;

	protected bool isActive = false;
    protected bool selfDestructable;

    //all modes
    protected float time = 0f;
	protected float speed = 1f;
	protected float lifeTime = 1f;

    //step mode
    protected int steps = 1; //used only in steps mode
    protected float stepTime;
    protected int currentStep;
    protected float currentStepTime;


    /// <value>Property <c>TimeNormalized</c> Returns time of lifetime normalized from 0 to 1</value>
    public float TimeNormalized
	{
		get
		{
			return time / lifeTime;
		}
	}


    /// <summary>
    /// Just Stops the timer, OnEnd will NOT be called
    /// </summary>
    public void Break()
    {
        isActive = false;
    }
    

    /// <summary>
    /// Re launchs timer with same parameters, that were created on Launch
    /// </summary>
    public void ReLaunch()
    {
        this.time = 0;
        isActive = true;
    }


    #region Launch
    /// <summary>
    /// Launches Timer for defined lifeTime. When defined as lifeTime number of seconds will pass, OnEnd will be called
    /// </summary>
    /// <param name="lifeTime">LifeTime in seconds of the Timer</param>
    public virtual void Launch(float lifeTime)
	{
        Zero();
        this.speed = 1f / lifeTime;
        this.lifeTime = 1f;
        this.isActive = true;
	}


    /// <summary>
    /// Launches Timer for defined lifeTime. During the defined number of seconds, defined number of steps will be made
    /// On every step OnStep will be called
    /// When defined number of seconds as lifeTime will pass, OnEnd will be called
    /// </summary>
    /// <param name="lifeTime">LifeTime in seconds of the Timer</param>
    /// /// <param name="steps">Defined number of steps</param>
    public virtual void Launch(float lifeTime, int steps)
    {
        Zero();
        this.speed = 1f / lifeTime;
        this.lifeTime = lifeTime;
        this.steps = steps;
        this.stepTime = 1f / (float)(this.steps);
        this.currentStep = 0;
        this.isActive = true;
    }


    /// <summary>
    /// Launches Timer for defined lifetime with defined speed
    /// </summary>
    /// <param name="lifeTime">LifeTime in seconds of the Timer</param>
    /// <param name="speed">Abstract Speed of the Timer</param>
    public virtual void Launch(float lifeTime, float speed)
    {
        Zero();
        this.speed = speed;
        this.lifeTime = lifeTime;
        this.isActive = true;
    }
    #endregion


    #region Insides
    protected void Zero()
    {
        time = 0f;
        speed = 0f;
        lifeTime = 0f;
        steps = 0;
        stepTime = 0f;
        currentStep = 0;
        currentStepTime = 0;
    }


    protected virtual void Update()
    {
		if (!isActive)
			return;

        //if steps mode active
        if (steps > 0)
        {
            if (currentStep < steps)
            {
                currentStepTime += (Time.deltaTime * speed);
                if (currentStepTime >= stepTime)
                {
                    if (OnStep != null)
                    {
                        OnStep(currentStep);
                    }

                    //TimeChange()
                    currentStep++;
                    currentStepTime = 0f;
                }
            }

            else
            {
                //time = 1f;
                End();
            }
        }

        //default mode
        else
        {
            time += (Time.deltaTime * speed);
            if (time >= lifeTime)
            {
                time = lifeTime;
                TimeChange();
                End();
                return;
            }

            TimeChange();
        }
    }


    protected virtual void TimeChange()
    {
        if (OnTimeChanged != null)
            OnTimeChanged(TimeNormalized);
    }


    protected virtual void End()
    {
        isActive = false;

        //last step was not made, make it forcibly
        if (TimeNormalized < 1)
        {
            time = lifeTime;
            TimeChange();
        }

        time = 0f;
        if (OnEnd != null)
            OnEnd();

        if (selfDestructable)
        {
            Destroy(this.gameObject);
        }
    }
    #endregion
}