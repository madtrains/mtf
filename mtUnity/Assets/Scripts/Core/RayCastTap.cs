namespace MTCore
{
    public class RayCastTap :  UberBehaviour
    {
        public UnityEngine.Events.UnityAction OnTap;
        public virtual void Tap()
        {
            OnTap?.Invoke();
        }
    }
}