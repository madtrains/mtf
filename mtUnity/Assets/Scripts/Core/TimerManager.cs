﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimerManager : UberBehaviour
{
    private List<TimerBase> timers;
    private List<TimerBase> toRemove;

    public void Init()
    {
        timers = new List<TimerBase>();
        toRemove = new List<TimerBase>();
    }


    public TimerBase CreateSimpleTimer(float lifeTime)
    {
        SimpleTimer st = new SimpleTimer(lifeTime);
        timers.Add(st);
        st.OnEnd += () =>
        {
            toRemove.Add(st);
        };
        return st;
    }


    private void Update()
    {
        if (timers!= null && timers.Count > 0)
        {
            foreach (TimerBase timer in timers.ToArray())
            {
                timer.Update();
            }

            if (toRemove.Count > 0)
            {
                timers.Remove(toRemove[0]);
                toRemove.RemoveAt(0);
            }
        }
    }
}


public class TimerBase
{
    public UnityAction OnEnd;
    public UnityAction<float> OnChange;
    public UnityAction<int> OnStep;


    protected float time = 0f;

    public virtual void Update()
    {

    }
}


public class SimpleTimer : TimerBase
{
    private float speed = 1f;
    private float lifeTime = 1f;


    public SimpleTimer(float lifeTime)
    {
        this.time = 0f;
        this.speed = 1f / lifeTime;
        this.lifeTime = 1f;
    }


    /// <value>Property <c>TimeNormalized</c> Returns time of lifetime normalized from 0 to 1</value>
    public float TimeNormalized
    {
        get
        {
            return time / lifeTime;
        }
    }


    public override void Update()
    {
        base.Update();
        time += (Time.deltaTime * speed);
        if (time >= lifeTime)
        {
            time = lifeTime;
            End();
            return;
        }

        TimeChange();
    }


    private void TimeChange()
    {
        if (OnChange != null)
            OnChange(TimeNormalized);
    }


    protected virtual void End()
    {
        if (OnChange != null)
            OnChange(1f);


        if (OnEnd != null)
            OnEnd();
    }


    protected void Zero()
    {
        time = 0f;
        speed = 0f;
        lifeTime = 0f;
    }
}