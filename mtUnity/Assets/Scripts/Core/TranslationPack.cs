using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TranslationPack
{
    public LanguagePack LanguagePack { get { return languagePacks[currentLanguage]; } }

    public void SetLanguage(int index)
    {
        currentLanguage = index;
    }

    private int currentLanguage;

    [SerializeField] LanguagePack[] languagePacks;
}


[System.Serializable]
public class LanguagePack
{
    public string name;
    public Font font;
}
