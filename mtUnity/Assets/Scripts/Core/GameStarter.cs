﻿using MTCore;
using MTDebug;
using MTParameters;
using MTPlayer;
using MTTown;
using MTUi;
using MTUi.Town;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Trains;
using TutorialFlipper;
using UnityEngine;

public class GameStarter : MonoBehaviour
{
	#region GLOBALS
	public enum GlobalValues
	{
		Level_active,
		User_tasks,
		User_resourses,
		Active_Train,
		Active_Coaches,
		Current_Language,
		Trains,
		Coaches,
		Game_Acsess_List,
		User_tickets,
		Tutorial_Elements,
		Cogs,
		Global_Events,
		Update_Daily_Date_Time,
		Last_Daily_Update_Date_Time,
		Trains_List,
		Coaches_List,
		User_Task_List,
		Zoo,
		Timers,
		Progress_Marks
	}

	public enum GlobalEvents
	{
		FirstMeeting
	}

	public enum GameAccsessType
	{
		Common,
		Custom,
		Passenger,
		Wood,
		Mail
	}
	#endregion

	public enum GameMode
	{
		Normal,
		DebugEnabled,
		StartWithDebugScreen
	}

	public static GameStarter Instance { get; private set; }

	public static MTCore.SceneManager SceneManager { get { return MTCore.SceneManager.Instance; } }
	public static InBuildParameters InBuildParameters { get { return InBuildParameters.InBuildParametersInstance; } }
	public static GameParameters GameParameters { get { return GameStarter.Instance.ResourceManager.GameParameters; } }

	public ResourceManager ResourceManager { get; private set; }
	public PlayerManager PlayerManager { get; private set; }
	public TownDataManager TownDataManager { get; private set; }
	public TownManager TownManager { get; set; }
	public FlipperManager FlipperManager { get; set; }
	public TrainManager TrainManager { get; set; }
	public MarketManager MarketManager { get; set; }
	public DialogManager DialogManager { get; set; }
	public TranslationManager TranslationManager { get; set; }
	public TutorialManager TutorialManager { get; set; }
	public EventManager EventManager { get; set; }
	public AuthManager AuthManager { get; set; }
	public GamePlayManager GamePlayManager { get; set; }
	public MTUi.Root UIRoot { get { return uiRoot; } }
	public Trains.Train Train { get { return TrainManager.Train; } }
	public MainComponentsManager MainComponents { get { return mainComponents; } }
	public MTUi.DebugScreen.DebugScreen DebugScreen { get { return debugScreen; } }

	public TranslationPack TranslationPack { get { return translationPack; } }
	public AnimationCurve SmoothAnimCurve { get { return smoothCurve; } }
	public DebugManager DebugManager { get; set; }

	public GameMode Gamemode { get { return gameMode; } }
	[SerializeField] private GameMode gameMode;

	[SerializeField] private MTUi.Root uiRoot;
	[SerializeField] private MainComponentsManager mainComponents;
	[SerializeField] private MTUi.DebugScreen.DebugScreen debugScreen;

	public int BackGroundIndex { get { return backGroundIndex; } set { backGroundIndex = value; } }
	[SerializeField] private int backGroundIndex = -1;
	[SerializeField] private AnimationCurve smoothCurve;
	[SerializeField] private TranslationPack translationPack;

	[SerializeField][Range(0, 120)] private int targetFPS;

	public int PauseOnFps { get { return pauseOnFps; } }
	[SerializeField][Range(0, 120)] private int pauseOnFps;

	public SFD.SplitFlapDisplay SplitFlapDisplay { get { return splitFlapDisplay; } }
	[SerializeField] private SFD.SplitFlapDisplay splitFlapDisplay;
	[SerializeField] private int sfdH;
	[SerializeField] private int sfdV;

	async Task Awake()
	{
		AssignInstance();

		//оставляем проверку что в релизе гейм мод нормал, далее переписываем из настроек
		if (InBuildParameters.BuildType == BuildType.Production)
		{
			gameMode = GameMode.Normal;
		}

		//инициализация и использование сохраняемых локально дебаг сетингов
		DebugManager = DebugManager.Init();
		if (DebugManager.DebugSettings != null)
		{
			if (DebugManager.DebugSettings.isActive)
			{
				gameMode = (GameMode)DebugManager.DebugSettings.GameMode;
				Debug.LogWarningFormat("Game Mode foaded from debug settings: {0}", gameMode);
			}
			else
			{
				Debug.LogWarningFormat("DebugManager Settings are inactive");
			}
		}
		else
		{
			Debug.LogWarningFormat("DebugManager Settings are null");
		}

		if (targetFPS > 0)
			Application.targetFrameRate = targetFPS;

		if (gameMode != GameMode.Normal)
		{
			Application.logMessageReceivedThreaded += HandleLog; //subscribe log
			UberBehaviour.Activate(debugScreen.gameObject, true);
		}
		else
		{
			GameObject.Destroy(debugScreen.gameObject);
		}
		MTUi.Root.OnUIInited += async () => { await OnUIInitAsync(); };
		AuthManager = await AuthManager.Init(); ;
	}

	public void AssignInstance()
	{
		Instance = this;
	}

	private async Task Start()
	{
		UIRoot.Splash.ScaleBkg();

		Debug.LogWarningFormat("Starting Game: Step {0}: {1}", 1, "Init Main Components");

		DontDestroyOnLoad(this);
		if (gameMode != GameMode.Normal)
			debugScreen.Init();

		await AuthManager.SignIn();

		//START MAIN FLOW
		Debug.LogWarningFormat("Starting Game: Step {0}: {1}", 2, "Load Game Parameters and Init Managers");
		await InitManagers();
		await LoadDisplay();
        await mainComponents.Init();

        Debug.LogWarningFormat("Starting Game: Step {0}: {1}", 3, "Download/Init UI");
		await UIRoot.DownloadAndInit();
		UIRoot.PostCreation();

		Debug.LogWarningFormat("Starting Game: Step {0}: {1}", 4, "UI Inited");
		GameStarter.Instance.PlayerManager.OnChangeResources?.Invoke();
		GameStarter.Instance.UIRoot.InitResourcesOrder();
		GameStarter.Instance.PlayerManager.OnChangeTicketsAmount?.Invoke(GameStarter.Instance.PlayerManager.GetTickets());
		mainComponents.Helpers.OnStart();
		Debug.LogWarningFormat("Starting Game: Step {0}: {1}", 5, "Init Completed");
	}

	private void HandleLog(string condition, string stackTrace, LogType type)
	{
		debugScreen.Console.CreateMessage(condition, type, stackTrace);
	}

	private async Task InitManagers()
	{
		//await this.preLoader.Load();
		ResourceManager = await ResourceManager.Init(InBuildParameters);//тут загрузятся парамсы
		Debug.LogWarningFormat("ResourceManager:Initiated");
		PlayerManager = await PlayerManager.Init(InBuildParameters);
		Debug.LogWarningFormat("PlayerManager:Initiated");
		TranslationManager = TranslationManager.Init(GameParameters);
		Debug.LogWarningFormat("TranslationManager:Initiated");
		MarketManager = MarketManager.Init(GameParameters);
		Debug.LogWarningFormat("MarketManager:Initiated");
		TownDataManager = TownDataManager.Init();
		Debug.LogWarningFormat("TownDataManager:Initiated");
		TrainManager = await TrainManager.Init();
		Debug.LogWarningFormat("TrainManager:Initiated");
		FlipperManager = FlipperManager.Init();
		Debug.LogWarningFormat("FlipperManager:Initiated");
		DialogManager = DialogManager.Init();
		Debug.LogWarningFormat("DialogManager:Initiated");
		TownManager = await TownManager.Init();
		Debug.LogWarningFormat("TownManager:Initiated");
		TutorialManager = TutorialManager.Init();
		Debug.LogWarningFormat("TutorialManager:Initiated");
		GamePlayManager = GamePlayManager.Init();
		Debug.LogWarningFormat("GamePlayManager:Initiated");
		EventManager = EventManager.Init();
		Debug.LogWarningFormat("EventManager:Initiated");
		Root.OnUIInited += () =>
		{
			PlayerManager.OnChangeResources += UIRoot.UpdateResources;
			PlayerManager.OnChangeTicketsAmount += UIRoot.UpdateTickets;

			Root.OnChangeLanguage += (langIndex) =>
			{
				this.translationPack.SetLanguage(langIndex);
				TranslationManager.Language newLang = (TranslationManager.Language)langIndex;
				PlayerManager.SetCurrentLanguage(newLang);
				TranslationManager.SetCurrentLanguage(newLang);
			};
			uiRoot.Splash.Init();
			Root.OnChangeLanguage.Invoke(PlayerManager.GetCurrentLanguage());
		};
	}

	private async Task OnUIInitAsync()
	{
		Debug.LogWarningFormat("Starting Game: Step {0} {1}", "Async", "UI Inited");

		//старт через дебаговый экран
		if (gameMode == GameMode.StartWithDebugScreen)
		{
			CreateStartDebugButtons();
		}
		//обычный cтарт игры
		else
		{
			await StartGame();
		}
	}

	private void CreateStartDebugButtons()
	{
		GameStarter.Instance.DebugScreen.Controls.CreateButton(
			MTUi.DebugIcons.germany, "Start Game",
		async () =>
		{
			CloseDebugScreenDeleteControlls();
			await StartGame();
		});

		//самый главный дропбокс с типом старта игры
		GameStarter.Instance.DebugScreen.Controls.CreatedDropBox(
			MTUi.DebugIcons.bug,
			Enum.GetValues(typeof(GameMode)).Cast<GameMode>().Select(v => v.ToString()).ToArray(),
			(vl) =>
			{
				DebugManager.DebugSettings.GameMode = (GameMode)vl;
			},
			(int)DebugManager.DebugSettings.GameMode);

		GameStarter.Instance.DebugScreen.Controls.CreateButton(
			MTUi.DebugIcons.coin, "Give me Money!",
		() =>
		{
			GameStarter.Instance.PlayerManager.ResourceIncreaseAmount(0, 100000);
			GameStarter.Instance.PlayerManager.ResourceIncreaseAmount(9, 100000);
		});

		//размер дебагового пути сохраняется в сеттинги
		GameStarter.Instance.DebugScreen.Controls.CreatedDropBox(
			MTUi.DebugIcons.warning,
			Enum.GetValues(typeof(TripSize)).Cast<TripSize>().Select(v => v.ToString()).ToArray(),
			(vl) =>
			{
				DebugManager.DebugSettings.TestFlipperSize = (TripSize)vl;
			},
			(int)DebugManager.DebugSettings.TestFlipperSize);


		//кнопка лаунча сразу во флиппер
		GameStarter.Instance.DebugScreen.Controls.CreateButton(
			MTUi.DebugIcons.finish, "Flipper IDs: 1,5,7",
			async () =>
			{
				CloseDebugScreenDeleteControlls();
				//Поездка
				Trip trip = new Trip(
					TripType.Local,
					DebugManager.DebugSettings.TestFlipperSize,
					new List<int>()
					{
						1, //пассажиры 
						5, //дерево
						7 //фрахт
					},
					GameStarter.GameParameters.Towns[0].ID,
					GameStarter.GameParameters.Towns[0].ID);
				await FlipperManager.LoadFlipperByTrip(trip);
			});

		GameStarter.Instance.DebugScreen.Controls.CreateButton(
			MTUi.DebugIcons.trainSign, "Start Town (Long)",
			async () =>
			{
				CloseDebugScreenDeleteControlls();
				await StartGame();
			});

		//Дебаговая отрисовка
		GameStarter.Instance.DebugScreen.Controls.CrteateButtonToggle("Debug Draw",
			(value) => { DebugManager.DebugSettings.DebugDraw = value; },
			DebugManager.DebugSettings.DebugDraw);

        //Дебаговая секвенция в начале уровня
        GameStarter.Instance.DebugScreen.Controls.CrteateButtonToggle("Debug Chunks at start",
            (value) => { DebugManager.DebugSettings.DebugChunksAtStart = value; },
            DebugManager.DebugSettings.DebugChunksAtStart);

        //Дебаговая секвенция в начале уровня
        GameStarter.Instance.DebugScreen.Controls.CrteateButtonToggle("Show Additional info in Flipper",
            (value) => { DebugManager.DebugSettings.FlipperAdditionalInfo = value; },
            DebugManager.DebugSettings.FlipperAdditionalInfo);


        GameStarter.Instance.DebugScreen.Bkg = true;
		GameStarter.Instance.DebugScreen.LadyBug(true);
	}

	private async Task StartGame()
	{
		GameStarter.Instance.UIRoot.Splash.Show();

		int level = PlayerManager.GetCurrentLevel();
		Debug.LogFormat("Loading town {0}", level);
		MTParameters.Town town = GameStarter.GameParameters.Towns[level];

		// грузим сцену города
		//int sceneId = town.SceneBundleID;
		//var sceneName = await GameStarter.Instance.ResourceManager.GetSceneByIdAsync(sceneId);
		await GameStarter.SceneManager.LoadScene("empty", async () =>
		{
			_ = await TownManager.LoadTown(town, false);
			AssetBundle.UnloadAllAssetBundles(false);
		});
	}

	private void CloseDebugScreenDeleteControlls()
	{
		DebugScreen.Bkg = false;
		DebugScreen.LadyBug(false);
		DebugScreen.Controls.DeleteAll();

        GameStarter.Instance.DebugScreen.Controls.CreateButton(
            MTUi.DebugIcons.info, "Test Dialog",
        () =>
        {
            GameStarter.Instance.UIRoot.Dialog.Show(
				"Die tolle Capybara!", 
				GameCharacter.Franz, 
				new MTUi.Town.DialogButton("Jawohl, Herr", () => 
				{ 
					Debug.Log("Gut gemacht!");

					GameStarter.Instance.UIRoot.Dialog.Set(
						"Such nach diese Sheiß, und schnell!",
						GameCharacter.Franz,
						null, //кнопки
						new MTUi.Town.DialogShieldInfo[2] { new DialogShieldInfo("Holz: 10", 5), new DialogShieldInfo("Fahrgäste: 50", 1) },  //запросы
						new MTUi.Town.DialogShieldInfo[2] { new DialogShieldInfo("Geld: 100", 0), new DialogShieldInfo("Fahrkarte: 2", 8) }); //награда
                }));
        });
    }

	protected async Task LoadDisplay()
	{
		UnityEngine.Object splitLoaded = await GameStarter.Instance.ResourceManager.LoadByBundleName(MTAssetBundles.misc_splitflapdisplay);
		UnityEngine.Object splitObj = GameObject.Instantiate(splitLoaded);
		GameObject splitGo = splitObj as GameObject;
		splitFlapDisplay = splitGo.GetComponent<SFD.SplitFlapDisplay>();
		splitFlapDisplay.transform.SetParent(transform, false);
		splitFlapDisplay.transform.localPosition = Vector3.up * 5050;
		splitFlapDisplay.transform.localRotation = Quaternion.identity;
		splitFlapDisplay.transform.localScale = Vector3.one;
		splitFlapDisplay.Init(sfdH, sfdV);
		//splitFlapDisplay.Assemble(6, 4, 2f);
	}
}