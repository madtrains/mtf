﻿using UnityEngine;

[System.Serializable]
public class MinMax : PropertyAttribute
{
    public static float PlusMinusValue(float value, float range)
    {
        MinMax mm = new MinMax(-range, range);
        float randV = mm.Random;
        float result = value + randV;
        return result;
    }


    [SerializeField] private float min;
    [SerializeField] private float max;

    public MinMax(float min, float max)
    {
        this.min = Mathf.Min(min, max);
        this.max = Mathf.Max(min, max);
    }


    public float Min
    {
        get
        {
            return min;
        }
    }
    

    public float Max
    {
        get
        {
            return max;
        }
    }


    public int MinInt
    {
        get
        {
            return (int)min;
        }
    }

    public int MaxInt
    {
        get
        {
            return (int)max;
        }
    }


    public float Random
    {
        get
        {
            return UnityEngine.Random.Range(min, max);
        }
    }


    public int RandomInt
    {
        get
        {
            return UnityEngine.Random.Range(MinInt, MaxInt);
        }
    }

    public float Difference
    {
        get
        {
            return max - min;
        }
    }

    public int DifferrenceInt
    {
        get
        {
            return Mathf.RoundToInt(Difference);
        }
    }

    public float Clamp(float value)
    {
        return Mathf.Clamp(value, min, max);
    }


    public float Lerp(float t)
    {
        return Mathf.Lerp(min, max, t);
    }


	public int LerpInt(float t)
	{
		float value = Mathf.Lerp(min, max, t);
		return Mathf.RoundToInt(value);
	}


	public float LerpReverse(float t)
	{
		return Mathf.Lerp(max, min, t);
	}


	public float InverseLerp(float value)
	{
		float t = Mathf.InverseLerp(min, max, value);
		return t;
	}


	public bool BelongsInclusive(float value)
    {
        if (value >= min && value <= max)
            return true;

        return false;
    }

    public bool Contains(float value)
    {
        if (value >= min && value < max)
            return true;

        return false;
    }

    public override string ToString()
    {
        return string.Format("Min: {0}, Max: {1}", Min, Max);
    }

    public Vector2 Vector2
    {
        get { return new Vector2(min, max); }
    }
}