﻿using MTUi;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class  UberBehaviour : MonoBehaviour
{
	public delegate void ProcessDelegate(float time);
	public delegate void EndDelegate();

	#region static
	public static int Cycle(int current, int max)
	{
		int value = current;
		value += 1;
		if (value >= max)
			value = 0;
		return value;
	}

	public static void DeleteAllChildren(Transform transform)
	{
		int childs = transform.childCount;
		for (int i = childs - 1; i > 0; i--)
		{
			GameObject.Destroy(transform.GetChild(i).gameObject);
		}
	}

	public static T GetArrayRandomElement<T>(T[] array)
	{
		int randomIndex = GetArrayRandomIndex<T>(array);
		return array[randomIndex];
	}

	public static int GetArrayRandomIndex<T>(T[] array)
	{
		int randomIndex = UnityEngine.Random.Range(0, array.Length);
		return randomIndex;
	}

	public static int GetArrayRandomIndex<T>(List<T> lst)
	{
		int randomIndex = UnityEngine.Random.Range(0, lst.Count);
		return randomIndex;
	}


	public static void ShuffleList<T>(List<T> list)
	{
		System.Random rng = new System.Random();
		int n = list.Count;
		while (n > 1)
		{
			n--;
			int k = rng.Next(n + 1);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}

	public static T CycleArray<T>(T[] array, T element)
	{
		for(int i = 0; i < array.Length; i++)
		{
			T cEl = array[i];
			if (cEl.Equals(element))
			{
				if (i == array.Length - 1)
				{
					return array[0];
				}
				else
				{
                    return array[i+1];
                }
			}
        }
		return array[0];
	}

	public static bool RandomBool
    {
		get
        {
			float v = Random.value;
			if (v > 0.5)
			{
				return true;
			}

			return false;
		}
	}

	public static void ApplyMatrix(Matrix4x4 matrix, Transform target)
    {
		target.localScale = ExtractScale(matrix);
		target.rotation = ExtractRotation(matrix);
		target.position = ExtractPosition(matrix);
	}

	public static Quaternion ExtractRotation(Matrix4x4 matrix)
	{
		Vector3 forward;
		forward.x = matrix.m02;
		forward.y = matrix.m12;
		forward.z = matrix.m22;

		Vector3 upwards;
		upwards.x = matrix.m01;
		upwards.y = matrix.m11;
		upwards.z = matrix.m21;

		return Quaternion.LookRotation(forward, upwards);
	}

	public static Vector3 ExtractPosition(Matrix4x4 matrix)
	{
		Vector3 position;
		position.x = matrix.m03;
		position.y = matrix.m13;
		position.z = matrix.m23;
		return position;
	}

	public static Vector3 ExtractScale(Matrix4x4 matrix)
	{
		Vector3 scale;
		scale.x = new Vector4(matrix.m00, matrix.m10, matrix.m20, matrix.m30).magnitude;
		scale.y = new Vector4(matrix.m01, matrix.m11, matrix.m21, matrix.m31).magnitude;
		scale.z = new Vector4(matrix.m02, matrix.m12, matrix.m22, matrix.m32).magnitude;
		return scale;
	}

	public static int RoundWithStep(float value, float step)
    {
		float rounded = Mathf.Round(value / step);
		rounded = rounded * step;
		int result = Mathf.RoundToInt(rounded);
		return result;
	}

	protected static private System.Exception NotImplementedException(string v)
	{
		throw new System.NotImplementedException(v);
	}


	/// <summary>
	/// Picks randomly defined number of elements from target array and returns the new array
	/// </summary>
	/// <param name="inputArray">Target Array, that will be used, not changed</param>
	/// <param name="numberOfElements">Length of target array</param>
	/// <returns>Array  of elements</returns>
	public static T[] GetElementsRandomly<T>(T[] inputArray, int numberOfElements)
	{
		List<T> downList = new List<T>(inputArray);
		List<T> upList = new List<T>();
		for (int i = 0; i < inputArray.Length; i++)
		{
			int index = Random.Range(0, downList.Count);
			T element = downList[index];
			upList.Add(element);
			downList.Remove(element);
			if (downList.Count == 0 || upList.Count == numberOfElements)
				break;
		}
		return upList.ToArray();
	}


	/// <summary>
	/// Finds or instantiates(in case of null find result), prefab of specified type
	/// </summary>
	/// <param name="t">A reference variable to check it for null</param>
	/// <param name="path">Resource path of prefab to instantiate</param>
	/// <param name="parent">Transform that shell be the parent of the new prefab. Value can be null</param>
	/// <returns>Component of instantiated or founded prefab</returns>
	public static T FindOrInstantiate<T>(ref T t, string path, Transform parent) where T : Component
	{
		if (t == null)
		{
			t = FindObjectOfType<T>();
			if (t == null)
			{
				T loaded = Resources.Load<T>(path);
				if (loaded == null)
					Debug.LogErrorFormat("Can not load objet at path {0}", path);
				t = Instantiate<T>(loaded);
				if (parent != null)
					t.transform.SetParent(parent, false);
			}
		}
		return t;
	}


	public static void Activate(GameObject target, bool state)
	{
		if (target.activeSelf != state)
			target.SetActive(state);
	}


	public static bool IsOdd(int a)
    {
		return !IsEven(a);
    }


	public static bool IsEven(int a)
	{
		return ((float)a % 2) == 0;
	}


	public static bool IntToBool(int value)
	{
		return value == 0 ? false : true;
	}


	public static int BoolToInt(bool value)
	{
		return value == true ? 1 : 0;
	}


	public static Transform[] GetChildren(Transform transform)
	{
		int childCount = transform.childCount;
		if (childCount == 0)
			return null;

		List<Transform> list = new List<Transform>();
		for (int i = 0; i < childCount; i++)
		{
			Transform tr = transform.GetChild(i);
			list.Add(tr);
		}

		return list.ToArray();
	}


	public static OberList<T> GetChildren<T>(Transform transform)
	{
		OberList<T> lst = new OberList<T>();
		if (transform == null)
			return lst;
		Transform[] children = GetChildren(transform);
		if (children == null || children.Length == 0)
			return lst;

		foreach (Transform child in GetChildren(transform))
        {
			T tComp = child.GetComponent<T>();
			if (tComp != null)
            {
				lst.Add(tComp);
            }
        }

		return lst;
	}


	public static void GetAllDescendents<T>(Transform transform, ref List<T> lst) where T : Component
	{
		if (transform.childCount > 0)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
				Transform child = transform.GetChild(i);
				T tComp = child.GetComponent<T>();
				if (tComp != null)
				{
					lst.Add(tComp);
					//Debug.LogFormat("Add {0}", tComp.name);
				}
				GetAllDescendents<T>(child, ref lst);
			}
        }
	}


	public static bool Equal(float a, float b, float infelicity)
	{
		return Mathf.Abs(a - b) < infelicity;
	}
	#endregion


	public Transform LastChild
	{
		get
		{
			if (transform.childCount > 0)
			{
				int index = transform.childCount - 1;
				return transform.GetChild(index);
			}
			return null;
		}
	}


	public Vector3 RoundVector90(Vector3 input)
	{
		return new Vector3
			(
			Mathf.Round(input.x / 90) * 90,
			Mathf.Round(input.y / 90) * 90,
			Mathf.Round(input.z / 90) * 90
			);
	}


	public void SelfDestroy()
	{
		Destroy(gameObject);
	}


	public bool ActiveSelf
	{
		get
		{
			return this.gameObject.activeSelf;
		}

		set
		{
			Activate(this.gameObject, value);
		}
	}


	public int PrettyAngle(float inputAngle)
	{
		return TransformToPrettyAngle(inputAngle);
	}

	public static int TransformToPrettyAngle(float inputAngle)
	{
        float floatValue = inputAngle;
        if (floatValue > 180)
        {
            floatValue = floatValue - 360;
        }

        int intValue = (int)floatValue;
        return intValue;
    }


	public List<T> GetComponentsRecursively<T>() where T : Component
	{
		List<T> result = new List<T>();
		ProcessChildrenComponentsRecursive(transform, ref result);
		return result;
	}


	private void ProcessChildrenComponentsRecursive<T>(Transform tr, ref List<T> collection) where T : Component
	{
		List<Transform> result = new List<Transform>();
        int childCount = tr.childCount;
		
		if (childCount > 0)
        {
            for (int i = 0; i < childCount; i++)
            {
				Transform currentChild = tr.GetChild(i);
				T currentT = currentChild.GetComponent<T>();
				if (currentT != null)
					collection.Add(currentT);

				ProcessChildrenComponentsRecursive<T>(currentChild, ref collection);
            }
        }
	}


	protected virtual void DrawBoxCollider(BoxCollider bc, float r, float g, float b, float alpha)
    {
		Gizmos.color = new Color(r, g, b, alpha);
		Gizmos.matrix = bc.transform.localToWorldMatrix;
		Gizmos.DrawCube(bc.center, bc.size);
		Gizmos.color = new Color(r, g, b);
		Gizmos.DrawWireCube(bc.center, bc.size);
	}


	public static Bounds Combine(List<Bounds> lst)
    {
		Bounds a = lst[0];
		if (lst.Count > 1)
        {
            for (int i = 1; i < lst.Count; i++)
            {
				Bounds b = lst[i];
				a = Combine(a, b);
            }
        }

		return a;
    }


	public static Bounds Combine(Bounds a, Bounds b)
	{
		float xMin = a.min.x;
		if (b.min.x < a.min.x)
			xMin = b.min.x;

		float yMin = a.min.y;
		if (b.min.y < a.min.y)
			yMin = b.min.y;

		float zMin = a.min.z;
		if (b.min.z < a.min.z)
			zMin = b.min.z;

		float xMax = a.max.x;
		if (b.max.x > a.max.x)
			xMax = b.max.x;

		float yMax = a.max.y;
		if (b.max.y > a.max.y)
			yMax = b.max.y;

		float zMax = a.max.z;
		if (b.max.z > a.max.z)
			zMax = b.max.z;

		float xCenter = (xMin + xMax) / 2f;
		float YCenter = (yMin + yMax) / 2f;
		float ZCenter = (zMin + zMax) / 2f;
		Vector3 center = new Vector3(xCenter, YCenter, ZCenter);

		float xSize = xMax - xMin;
		float ySize = yMax - yMin;
		float zSize = zMax - zMin;
		Vector3 size = new Vector3(xSize, ySize, zSize);
		Bounds bounds = new Bounds(center, size);
		return bounds;
	}


	public static Bounds PositionsArrayToBounds(Vector3[] positions )
	{
		float xMin = System.Single.MaxValue;
		float xMax = System.Single.MinValue;

		float yMin = System.Single.MaxValue;
		float yMax = System.Single.MinValue;

		float zMin = System.Single.MaxValue;
		float zMax = System.Single.MinValue;

		for (int i = 0; i < positions.Length; i++)
		{
			Vector3 pos = positions[i];

			if (pos.x < xMin)
				xMin = pos.x;
			if (pos.x > xMax)
				xMax = pos.x;
			if (pos.y < yMin)
				yMin = pos.y;
			if (pos.y > yMax)
				yMax = pos.y;
			if (pos.z < zMin)
				zMin = pos.z;
			if (pos.z > zMax)
				zMax = pos.z;
		}
		float xCenter = (xMin + xMax) / 2f;
		float YCenter = (yMin + yMax) / 2f;
		float ZCenter = (zMin + zMax) / 2f;
		Vector3 center = new Vector3(xCenter, YCenter, ZCenter);

		float xSize = xMax - xMin;
		float ySize = yMax - yMin;
		float zSize = zMax - zMin;
		Vector3 size = new Vector3(xSize, ySize, zSize);
		Bounds bounds = new Bounds(center, size);
		return bounds;
	}


	public static Bounds GameObjecsMeshesToBounds(GameObject go)
    {
		List<Vector3> vertices = new List<Vector3>();

		MeshFilter[] mfs = go.GetComponentsInChildren<MeshFilter>();
        foreach (MeshFilter mf in mfs)
        {
			if (mf != null)
            {
				Matrix4x4 m4 = mf.transform.localToWorldMatrix;
                foreach (Vector3 v in mf.sharedMesh.vertices)
                {
					Vector3 offseted = m4.MultiplyPoint(v);
					vertices.Add(offseted);
                }
            }
        }
		return PositionsArrayToBounds(vertices.ToArray());
    }
}