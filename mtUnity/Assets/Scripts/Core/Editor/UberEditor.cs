﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class UberEditor : Editor
{
    [MenuItem("MT/Tools/Karee 1.5")]
    public static void PlaceInKaree15()
    {
        PlaceInKaree(1.5f);
    }

    [MenuItem("MT/Tools/Karee 5")]
    public static void PlaceInKaree5()
    {
        PlaceInKaree(5f);
    }

    public static void PlaceInKaree(float step)
    {
        GameObject[] gs = Selection.gameObjects;
        Vector3[] karee = Karree.Create(Vector3.zero, gs.Length, step);
        for (int i = 0; i < gs.Length; i++)
        {
            gs[i].transform.position = karee[i];
        }
    }


    void OnEnable()
    {
        SceneView.duringSceneGui += OnSceneGUI;
    }


    /// <summary>
    /// When we close, unsubscribe from the SceneView
    /// </summary>
    void OnDisable()
    {
        SceneView.duringSceneGui -= OnSceneGUI;
    }

    protected virtual void OnSceneGUI(SceneView sceneView)
    {

    }

    public readonly static int LABEL_WIDTH = 55;
    public enum UberSliderType { asFloat, asInt, asAngleInt}
    public class HiddenFloat
    {
        public int Int { get { return intValue; } }
        public float Float { get { return floatValue; } }

        private int intValue;
        private float floatValue;


        public HiddenFloat()
        {

        }


        public void Set(int value)
        {
            intValue = value;
            floatValue = value;
        }


        public void Set(float value)
        {
            floatValue = value;
            intValue = (int)value;
        }


        public void ConvertAngle(float value)
        {
            floatValue = value;
            if (floatValue > 180)
            {
                floatValue = floatValue - 360;
            }

            intValue = (int)floatValue;
        }
    }


   public static int PrettyAngle(float inputAngle)
   {
        float floatValue = inputAngle;
        if (floatValue > 180)
        {
            floatValue = floatValue - 360;
        }

        int intValue = (int)floatValue;
        return intValue;
   }


   public static float UberSlider(string label, float target, UberSliderType sliderType, int min, int max)
   {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label, GUILayout.Width(LABEL_WIDTH));
        
        if (sliderType != UberSliderType.asFloat)
        {
            HiddenFloat hf = new HiddenFloat();
            if (sliderType == UberSliderType.asAngleInt)
                hf.ConvertAngle(target);
            else if (sliderType == UberSliderType.asInt)
                hf.Set(target);

            hf.Set(EditorGUILayout.IntSlider(hf.Int, min, max));
            EditorGUILayout.EndHorizontal();
            return hf.Float;
        }
        else
        {
            float value = EditorGUILayout.Slider(target, min, max);
            EditorGUILayout.EndHorizontal();
            return value;
        }
   }


    protected GameObject[] InstantiatePrefabUnderJoints(Object obj,
        Transform[] targetArray)
    {
        List<GameObject> goList = new List<GameObject>();

        foreach(Transform target in targetArray)
        {
            Object prefab = PrefabUtility.InstantiatePrefab(obj);
            GameObject go = prefab as GameObject;
            go.transform.SetParent(target, false);
            goList.Add(go);
        }

        return goList.ToArray();
    }



    protected string Get()
    {
        return "";
    }


}