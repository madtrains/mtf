﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class UberPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
		Rect main = new Rect(position);
		main = EditorGUI.IndentedRect(main);
		int indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;

		Draw(position, property, label, main);
		EditorGUI.indentLevel = indent;
	}

	protected virtual int GetIndex(SerializedProperty property)
    {
		// taking index from string such as Element 0 or Element 1
		string[] spl = property.displayName.Split(' ');
		int index = -1;
		int.TryParse(spl[1], out index);
		return index;
	}


	protected virtual void Draw(Rect position, SerializedProperty property, 
		GUIContent label,
		Rect main)
    {

    }
}
