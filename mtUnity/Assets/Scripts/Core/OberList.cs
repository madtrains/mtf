﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngineInternal;

[System.Serializable]
public class OberList<T> : IEnumerator<T>
{
    public delegate void ProcessDelegate(T item);

    #region Statica
    public static OberList<T> Combine(OberList<T> a, OberList<T> b)
    {
        OberList<T> result = new OberList<T>();
        result.Append(a);
        result.Append(b);
        return result;
    }

    public static bool IsNullOrEmpty(OberList<T> lst)
    {
        if (lst == null)
            return true;
        if (lst.Count == 0)
            return true;
        return false;
    }


    public static List<T> GetRandomCopy(List<T> inputList, int number, float percentage = 0f)
    {
        List<T> newList = new List<T>();
        List<T> copy = new List<T>(inputList);
        for (int i = 0; i < number; i++)
        {
            if (copy.Count > 0)
            {
                Add(ref copy, ref newList, percentage);
            }
            else
            {
                return newList;
            }
            
        }
        return newList;
    }

    public static List<int> DistributeIndexesRandomly(int count)
    {
        List<int> indexes = new List<int>();
        for (int i = 0; i < count; i++)
        {
            indexes.Add(i);
        }

        List<int> result = new List<int>();
        while (result.Count < count)
        {
            int indexI = UnityEngine.Random.Range(0, indexes.Count);
            result.Add(indexes[indexI]);
            indexes.RemoveAt(indexI);
        }
        return result;
    }


    private static void Add(ref List<T> from, ref List<T> to, float startPercentage)
    {
        int count = from.Count;
        int startIndex = (int)System.Math.Floor(startPercentage * count);
        int index = UnityEngine.Random.Range(startIndex, from.Count);
        to.Add(from[index]);
        from.RemoveAt(index);
    }
    #endregion

    #region Insides
    protected int curIndex = -1;
    protected T curElement = default;
    [SerializeField] protected List<T> list;
    public List<T> List { get { return list; } set { list = value; } }


    public bool MoveNext()
    {
        if (++curIndex >= list.Count)
        {
            return false;
        }
        else
        {
            curElement = list[curIndex];
        }
        return true;
    }


    public IEnumerator<T> GetEnumerator()
    {
        return list.GetEnumerator();
    }


    public void Dispose() { }


    public void Reset() { curIndex = -1; }


    public T Current
    {
        get { return curElement; }
    }


    object IEnumerator.Current
    {
        get { return Current; }
    }


    public OberList()
    {
        this.list = new List<T>();
    }


    public OberList(List<T> list)
    {
        this.list = list;
    }


    public OberList(T[] array)
    {
        this.list = new List<T>();
        foreach (T el in array)
            this.list.Add(el);
    }


    public OberList<T> TrimBothSides(int number, int number2)
    {
        if (number + number2 >= this.Count)
            return this;

        List<T> newList = new List<T>();
        for (int i = number; i < this.Count - number2; i++)
        {
            newList.Add(this.list[i]);
        }

        return new OberList<T>(newList);
    }


    public OberList<T> Odd()
    {
        List<T> newList = new List<T>();
        for (int i = 0; i < this.Count; i++)
        {
            if (!UberBehaviour.IsEven(i))
                newList.Add(this.list[i]);
        }

        return new OberList<T>(newList);
    }
    #endregion

    #region Getters
    public int Count
    {
        get
        {
            if (this.List == null)
                return 0;
            return this.List.Count;
        }
    }


    public bool Empty
    {
        get
        {
            if (list == null)
                return true;

            if (list.Count == 0)
                return true;
            return false;
        }
    }


    public T Random
    {
        get
        {
            return list[RandomIndex];
        }
    }

    public int RandomIndex
    {
        get
        {
            int randomIndex = UnityEngine.Random.Range(0, list.Count);
            return randomIndex;
        }
    }


    public T First
    {
        get
        {
            if (list.Count > 0)
                return list[0];

            return default;
        }
    }


    public T Last
    {
        get
        {
            if (list.Count > 0)
                return list[LastIndex];

            return default;
        }
    }


    public int LastIndex
    {
        get
        {
            return list.Count - 1;
        }
    }
    #endregion

    #region Methods
    public T[] GetRandomList(int maximum)
    {
        List<T> copy = new List<T>(list);
        List<T> result = new List<T>();
        for (int i = 0; i < maximum; i++)
        {
            int randomIndex = UnityEngine.Random.Range(0, copy.Count);
            T element = copy[randomIndex];
            copy.RemoveAt(randomIndex);
            result.Add(element);
        }

        return result.ToArray();
    }


    public T[] GetRandomList()
    {
        int maximum = UnityEngine.Random.Range(0, list.Count);
        return GetRandomList(maximum);
    }


    public T[] GetRandomList(float percentage)
    {
        float floatMaximum = percentage * list.Count;
        int maximum = Mathf.RoundToInt(floatMaximum);
        return GetRandomList(maximum);
    }


    public void Mix()
    {
        this.list = new List<T>(GetRandomList(1f));
    }


    public void Append(List<T> other)
    {
        foreach (T el in other)
        {
            list.Add(el);
        }
    }


    public void Append(OberList<T> other)
    {
        foreach (T el in other)
        {
            list.Add(el);
        }
    }


    public void AddUnique(T element)
    {
        if (list.Contains(element))
            return;

        Add(element);
    }


    public void Add(T element)
    {
        this.list.Add(element);
    }


    public void AddAtTheBegining(T element)
    {
        this.list.Insert(0, element);
    }


    public void AddPenultimate(T element)
    {
        this.list.Insert(this.LastIndex, element);
    }

    public void PopLast()
    {
        if (list.Count > 0)
        {
            list.RemoveAt(LastIndex);
        }
    }

    public void PopFirst()
    {
        if (list.Count >= 0)
        {
            list.RemoveAt(0);
        }
    }


    public T GetRandomAndPop()
    {
        if (list.Count > 0)
        {
            int randomIndex = UnityEngine.Random.Range(0, list.Count);
            T element = list[randomIndex];
            list.RemoveAt(randomIndex);
            return element;
        }

        return default;
    }

    public void Iterate(ProcessDelegate processDelegate)
    {
        foreach ( T item in list)
        {
            processDelegate(item);
        }
    }

    public void IterateAllRandomly(ProcessDelegate processDelegate)
    {
        T[] copy = GetRandomList();
        foreach (T item in copy)
        {
            processDelegate(item);
        }
    }
    #endregion
}