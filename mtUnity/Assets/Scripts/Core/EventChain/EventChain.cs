using System.Collections.Generic;
using System.Linq;

namespace EventChain
{
    [System.Serializable]
    public class Chain : List<Step>
    {
        public UnityEngine.Events.UnityAction OnDone;

        public new void Add(Step step)
        {
            if (this.Count > 0)
            {
                step.LinkToPrevious(this.Last<Step>());
            }
            base.Add(step);
        }

        public void Run()
        {
            if (OnDone != null)
                this.Last<Step>().OnDone += OnDone;
            this.First<Step>().Run();
        }
    }
}

