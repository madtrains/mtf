namespace EventChain
{
    public delegate void StepDelegate();
    [System.Serializable]
    public class Step
    {
        public UnityEngine.Events.UnityAction OnDone;
   
        protected StepDelegate stepDelegate;
        protected Step next;

        public Step()
        {

        }

        public Step(StepDelegate stepDelegate)
        {
            this.stepDelegate = stepDelegate;
        }

        public virtual void Run()
        {
            stepDelegate?.Invoke();
        }

        public virtual void Done()
        { 
            CheckRunNext();
            OnDone?.Invoke();
        }

        public virtual void CheckRunNext()
        {
            if (next != null)
                next.Run();
        }

        public void LinkToPrevious(Step target)
        {
            target.next = this;
        }
    }

    public class ConditionStep : Step
    {
        public ConditionStep() : base() { }

        public ConditionStep(StepDelegate stepDelegate) : base(stepDelegate) { }

        public override void CheckRunNext()
        {
            if (AreConditionsAccomplished())
            {
                OnDone?.Invoke();
                base.CheckRunNext();
            }
        }

        protected virtual bool AreConditionsAccomplished()
        {
            return false;
        }
    }

    public class SingleConditionStep : ConditionStep
    {
        public SingleConditionStep() : base() { }

        public SingleConditionStep(StepDelegate stepDelegate) : base(stepDelegate) { }

        public bool �ondition { set { mainCondition = value; CheckRunNext(); } }

        protected bool mainCondition;

        protected override bool AreConditionsAccomplished()
        {
            return mainCondition;
        }
    }
}
