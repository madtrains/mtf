﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;


public class EditorUtilities
{
    /// <summary>
    /// Searchs for tables in target folder by current regex
    /// </summary>
    /// <param name="searchFolderPath">Folder to search. Mostly downloads folder</param>
    /// <param name="nameRegexPattern">Regex pattern to compare file names</param>
    public static string GetLatestFile(string searchFolderPath, string nameRegexPattern)
    {
        string latestFile = "";
        string[] files = Directory.GetFiles(searchFolderPath);
        Regex regex = new Regex(nameRegexPattern);
        DateTime changed = DateTime.Now;
        foreach (string currentPath in files)
        {
            MatchCollection matchCollection = regex.Matches(currentPath);
            if (matchCollection.Count > 0)
            {
                if (string.IsNullOrEmpty(latestFile))
                {
                    latestFile = currentPath;
                    changed = File.GetLastWriteTime(currentPath);
                }

                else
                {
                    DateTime currentDateTime = File.GetLastWriteTime(currentPath);
                    if (currentDateTime > changed)
                    {
                        latestFile = currentPath;
                        changed = currentDateTime;
                    }
                }
            }
        }
        return latestFile;
    }


    public static bool AreIdenticalByMD5(string absPath1, string absPath2)
    {
        string h1 = File.ReadAllText(absPath1);
        string h2 = File.ReadAllText(absPath2);
        h1 = EditorUtilities.GetMd5Hash(h1);
        h2 = EditorUtilities.GetMd5Hash(h2);
        bool identical = EditorUtilities.VerifyMd5Hash(h1, h2);
        return identical;
    }


    public static bool IsNewer(string absPath1, string absPath2)
    {
        if (!File.Exists(absPath1))
        {
            Debug.LogErrorFormat("File does not exist: \"{0}\"", absPath1);
            return false;
        }

        if (!File.Exists(absPath2))
        {
            Debug.LogErrorFormat("File does not exist: \"{0}\"", absPath2);
            return false;
        }

        DateTime changed1 = File.GetLastWriteTime(absPath1);
        DateTime changed2 = File.GetLastWriteTime(absPath2);
        return changed1 > changed2;
    }


    public static string RelativePathToAbsolute(string relativePath)
    {
        StringBuilder st = new StringBuilder();
        string path = Application.dataPath.Replace("/Assets", "");
        st.Append(path);
        st.Append("/");
        st.Append(relativePath);
        return st.ToString();
    }


    public static string AbsolutePathToRelative(string absolutePath)
	{
		if (absolutePath.StartsWith(Application.dataPath))
		{
			string relativepath = "Assets" + absolutePath.Substring(Application.dataPath.Length);
			return relativepath;
		}

		else
		{
			Debug.LogErrorFormat("Can not get relative path from {0}", absolutePath);
			return "";
		}
	}


    public static void ReplaceAssetByExtraFile(string sourceAbsolutePath, string destinationRelativePath)
    {
        string destinationAbsPath = RelativePathToAbsolute(destinationRelativePath);
        if (File.Exists(destinationAbsPath))
        {
            AssetDatabase.DeleteAsset(destinationRelativePath);
        }
        
        File.Copy(sourceAbsolutePath, destinationAbsPath);
        AssetDatabase.Refresh();
        AssetDatabase.ImportAsset(destinationRelativePath);
    }


	public static string GetMd5Hash(string input)
	{
		// Create a new instance of the MD5CryptoServiceProvider object.
		MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

		// Convert the input string to a byte array and compute the hash.
		byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

		// Create a new Stringbuilder to collect the bytes
		// and create a string.
		StringBuilder sBuilder = new StringBuilder();

		// Loop through each byte of the hashed data
		// and format each one as a hexadecimal string.
		for (int i = 0; i < data.Length; i++)
		{
			sBuilder.Append(data[i].ToString("x2"));
		}

		// Return the hexadecimal string.
		return sBuilder.ToString();
	}


	public static bool VerifyMd5Hash(string input, string hash)
	{
		// Hash the input.
		string hashOfInput = GetMd5Hash(input);

		// Create a StringComparer an compare the hashes.
		StringComparer comparer = StringComparer.OrdinalIgnoreCase;

		if (0 == comparer.Compare(hashOfInput, hash))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}