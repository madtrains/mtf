﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEditor;

public class MTTools
{
	[MenuItem("MT/Tools/Find And Instantiate Prefabs with Colliders")]
	public static void ProcessInstantiate()
	{
		Instantiate();
	}


	[MenuItem("MT/Tools/Find And Narrow Collider #P")]
	public static void FindAndNarrowCollider()
	{
		GameObject go = Selection.activeGameObject;
		if (go != null)
        {
			foreach (BoxCollider bx in GetComponents<BoxCollider>(go))
            {
				if (!bx.isTrigger)
                {
					//here
					if (bx.size.x > 1.2)
                    {
						bx.size = new Vector3(1.2f, bx.size.y, bx.size.z);
                    }
                }
            }
        }
	}


	public static void Instantiate()
	{
		int x = 0;
		foreach (GameObject go in GetGameObjectsWithColliders())
		{
			GameObject instantiated = PrefabUtility.InstantiatePrefab(go) as GameObject;
			instantiated.transform.position = Vector3.right * x;
			x += 15;
		}
	}


	public static GameObject[] GetGameObjectsWithColliders()
    {
		List<GameObject> gos = new List<GameObject>();
		foreach (string prefabPath in GetAllPrefabsPaths())
        {
			var obj = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(prefabPath);
			GameObject go = obj as GameObject;

			List<BoxCollider> colliders = new List<BoxCollider>();
			BoxCollider bx = go.GetComponent<BoxCollider>();
			if (bx != null)
				colliders.Add(bx);

			BoxCollider[] boxColliders = go.GetComponentsInChildren<BoxCollider>();
			foreach (BoxCollider boxCollider in boxColliders)
            {
				colliders.Add(boxCollider);
            }
			
			foreach (BoxCollider collider in colliders)
            {
				if (!collider.isTrigger)
                {
					gos.Add(go);
					break;
                }
            }
        }

		return gos.ToArray();
    }

	public static T[] GetComponents<T>(GameObject go) where T : Component
    {
		List<T> result = new List<T>();
		T component = go.GetComponent<T>();
		if (component != null)
			result.Add(component);

		T[] childrenComponents = go.GetComponentsInChildren<T>();
		foreach (T childComponent in childrenComponents)
		{
			if (!result.Contains(childComponent))
			{
				result.Add(childComponent);
			}
		}

		return result.ToArray();
	}


	public static string[] GetAllPrefabsPaths()
	{
		List<string> result = new List<string>();

		foreach (string path in AssetDatabase.GetAllAssetPaths())
		{
			if (path.Contains(".prefab"))
			{
				result.Add(path);
			}
		}
		return result.ToArray();
	}
}