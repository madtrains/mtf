﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

[CreateAssetMenu(menuName = "Editor Settings")]
public class EditorSettings : ScriptableObject
{
	public static EditorSettings Settings
	{
		get
		{
			return AssetDatabase.LoadAssetAtPath<EditorSettings>("Assets/EditorSettings.asset");
		}
	}

	public string Splitter => GetSplitter();
	public string Extension => GetExtension();
	public string FileId => fileId;
	public bool ConvertToComa => convertToComa;
	public string DownloadPath => downloadPath;
	public bool TSV => tsv;

	
	[SerializeField] private bool convertToComa;
	[SerializeField] private string downloadPath;
	[SerializeField] private bool tsv;

	[SerializeField] private string fileId;
	// Dictionary for sheet name and gid pairs
	[SerializeField] private List<SheetInfo> sheets = new List<SheetInfo>();

	public Dictionary<string, string> SheetsDictionary
	{
		get
		{
			var dictionary = new Dictionary<string, string>();
			foreach (var sheet in sheets)
			{
				dictionary[sheet.sheetName] = sheet.gid;
			}
			return dictionary;
		}
	}

	public string GetGidByName(string sheetName)
	{
		SheetsDictionary.TryGetValue(sheetName, out var gid);
		Debug.Log($"Sheet name: {sheetName}. Sheet gid :{gid}");
		return gid;
	}

	public bool IsCorrectPath
	{
		get
		{
			var path = Path.Combine(Application.dataPath, DownloadPath);
			if (string.IsNullOrEmpty(path)) return false;
			if (!Directory.Exists(path)) return false;

			var attr = File.GetAttributes(path);
			return attr.HasFlag(FileAttributes.Directory);
		}
	}

	private string GetSplitter() => tsv ? "\t" : ",";
	private string GetExtension() => tsv ? "tsv" : "csv";
}

[System.Serializable]
public class SheetInfo
{
	public string sheetName;
	public string gid;
}
