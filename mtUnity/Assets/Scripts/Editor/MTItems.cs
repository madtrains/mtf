﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEditor;

public class MTItems
{

	[MenuItem("MT/Select Game Parameters #&1")]
	public static void FindGameParameters()
	{
		var result = AssetDatabase.FindAssets("t:ParametersHolder");
		if (result != null)
		{
			string path = AssetDatabase.GUIDToAssetPath(result[0]);
			Debug.LogFormat("Found {0}", path);
			Selection.activeObject = AssetDatabase.LoadMainAssetAtPath(path);
		}

		else
			Debug.LogWarning("No Game Parameters objFound");
	}

	[MenuItem("MT/Select InBuild Parameters #&2")]
	public static void FindInBuildParameters()
	{
		var result = AssetDatabase.FindAssets("t:InBuildParameters");
		if (result != null)
		{
			string path = AssetDatabase.GUIDToAssetPath(result[0]);
			Debug.LogFormat("Found {0}", path);
			Selection.activeObject = AssetDatabase.LoadMainAssetAtPath(path);
		}

		else
			Debug.LogWarning("No InBuild Parameters objFound");
	}
}