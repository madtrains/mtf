﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Replacer : EditorWindow
{
    Object obj;
    int startIndex = 1;
    string newName;

    // Add menu named "My Window" to the Window menu
    [MenuItem("MT/Tools/Replacer_Renamer")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        Replacer window = (Replacer)EditorWindow.GetWindow(typeof(Replacer));
        window.Show();
    }

    private void OnGUI()
    {
        obj = EditorGUILayout.ObjectField(obj, typeof(Object), true);
        
        if (GUILayout.Button("Replace"))
        {
            foreach (GameObject selected in Selection.gameObjects)
            {
                Vector3 pos = selected.transform.position;
                Quaternion rot = selected.transform.rotation;
                Transform parent = selected.transform.parent;
                DestroyImmediate(selected);
                GameObject newGo = PrefabUtility.InstantiatePrefab(obj) as GameObject;
                newGo.transform.position = pos;
                newGo.transform.rotation = rot;
                newGo.transform.parent = parent;
            }
        }
        newName = EditorGUILayout.TextField(newName);
        startIndex = EditorGUILayout.IntField(startIndex);
        if (GUILayout.Button("Rename"))
        {
            int a = startIndex;
            foreach (GameObject selected in Selection.gameObjects)
            {
                int index = selected.transform.GetSiblingIndex();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(newName);
                sb.Append("_");
                sb.Append(a);
                selected.name = sb.ToString();
                a += 1;
            }
        }
    }
}
