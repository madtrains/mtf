﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class EditorFixMaterials
{
    [MenuItem("MT/Tools/Fix Editor Materials %#q")]
    public static void FixEditorMaterials()
    {
        Debug.Log("Fix Editor Materials");

        List<Material> materials = new List<Material>();
        var materialsGuids = AssetDatabase.FindAssets("t:Material");
        foreach (string guid in materialsGuids)
        {
            string path = AssetDatabase.GUIDToAssetPath(guid);
            Object obj = AssetDatabase.LoadAssetAtPath<Object>(path);
            Material mat = obj as Material;
            if (mat != null)
                materials.Add(mat);
        }

        Renderer[] renderers = GameObject.FindObjectsOfType<Renderer>();
        foreach (Renderer r in renderers)
        {
            if (r.materials.Length > 1)
            {

            }
            //one material - simple change
            else
            {
                string cleanName = r.material.name.Replace(" (Instance)", "");
                if (cleanName == "SideA" || cleanName == "SideB")
                {
                    Debug.LogWarningFormat("Skipping {0}", cleanName);
                }
                else
                {
                    foreach (Material mat in materials)
                    {
                        if (cleanName == mat.name)
                        {
                            bool overlay = r.material.HasProperty("_Color") && r.material.HasProperty("_Coeff");

                            if (overlay)
                            {
                                Color color = r.material.color;
                                float transp = r.material.GetFloat("_Coeff");
                                //replace
                                r.material = mat;
                                r.material.color = color;
                                r.material.SetFloat("_Coeff", transp);
                            }
                            else
                            {
                                r.material = mat;
                            }
                        }
                    }
                }
            }
        }
    }
    [MenuItem("MT/Tools/Remove for Debug")]
    public static void RemoveForDebug()
    {
        //RemoveAll<Triggers.PickUp>(true);
        //RemoveAll<MTRails.RRCollision>(true);
        //RemoveAll<Rigidbody>(false);
        //RemoveAll<Collider>(false);
        //SetAllMaterialsFlatRandom();
        Debug.LogWarningFormat("Job is done");
    }

    

    private static void SetAllMaterialsFlatRandom()
    {
        Renderer[] renderers = GameObject.FindObjectsOfType<Renderer>();
        foreach (Renderer r in renderers)
        {
            int count = r.materials.Length;
            List<Material> lst = new List<Material>();
            for (int i = 0; i < count; i++)
            {
                Material copy = new Material(MTDebug.DebugManager.Collection.DebugUnlit);
                copy.color = Random.ColorHSV();
                lst.Add(copy);
            }
            r.materials = lst.ToArray();
        }
    }
    private static void RemoveAll<T>(bool go) where T: Component
    {
        T[] all = GameObject.FindObjectsOfType<T>();
        for (int i = 0; i < all.Length; i++)
        {
            if (go)
                GameObject.Destroy(all[i].gameObject);
            else
                GameObject.Destroy(all[i]);
        }
    }
}


