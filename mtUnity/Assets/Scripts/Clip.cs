using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Clips
{
    [System.Serializable]
    public class Clip
    {
        public UnityEngine.Events.UnityAction<float> OnRun;
        public UnityEngine.Events.UnityAction OnEnd;
        public MTLightNCamera.GameCamera Camera { get { return GameStarter.Instance.MainComponents.GameCamera; } }
        protected bool active;
        protected float t;

        public void Launch()
        {
            if (active)
                return;
            active = true;
        }

        protected virtual void Run()
        {
            if (OnRun != null)
                OnRun(t);
        }

        public void End()
        {
            if (!active)
                return;

            active = false;
            this.t = 1f;
            if (OnRun != null)
                OnRun(t);
            if (OnEnd != null)
                OnEnd();
        }

        public void Update(float t)
        {
            if (!active)
                return;
            UpdateForcibly(t);
        }

        public void UpdateForcibly(float t)
        {
            this.t = t;

            if (t >= 1f)
            {
                End();
            }
            else
            {
                Run();
            }
        }

        public void Rewind()
        {
            this.active = false;
            this.t = 0f;
        }
    }
}

