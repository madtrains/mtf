﻿using MTData;

namespace MTPlayer
{
	[SaveableProperties(name: "MT_PlayerSettings.mts", folder: "MT_PLAYER", storage: "MT_STORAGE")]
	public class AppData : SaveableDictionary
	{
		public string Hash;
		[SaveableField]
		public string name = "user";
		[SaveableField]
		public string nick = "user";
		[SaveableField]
		public bool old = true;
		[SaveableField]
		public int age = 1;
		public AppData(string localStoragePath) : base(localStoragePath)
		{
		}

	}
}
