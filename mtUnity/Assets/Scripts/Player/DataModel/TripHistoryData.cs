﻿using MTData;
using MTCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MTPlayer
{
	[SaveableProperties(name: "MT_PlayerTripHistory.txt", folder: "MT_PLAYER", storage: "MT_STORAGE", fileType: SaveableFileType.Text)]
	public class TripHistoryData : SaveableObject<string>
	{
		public int Length => DataToSave.Length;
		public string Get()
		{
			return DataToSave;
		}
		public void AddData(string value)
		{
			var sb = new StringBuilder(DataToSave);
			sb.AppendLine(value);
			DataToSave = sb.ToString();
		}

		public TripHistoryData(string localStoragePath) : base(localStoragePath)
		{
			DataToSave = "";
		}
	}
}
