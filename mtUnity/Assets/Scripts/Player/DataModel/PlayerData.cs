﻿using MTData;

namespace MTPlayer
{
	[SaveableProperties(name: "MT_PlayerData.mts", folder:"MT_PLAYER", storage: "MT_STORAGE")]
	public class PlayerData : SaveableDictionary
	{
		private DataSettings _settings;
		public PlayerData(DataSettings settings) : base(settings.DataPath)
		{
			_settings = settings;
		}

		internal override void AfterSave()
		{
			//Debug.LogError($"User data version updated to - {Timestamp}");
		}
		internal override void BeforeLoad()
		{
			//Debug.LogError($"User dara version - {Timestamp}");
		}
	}
}
 