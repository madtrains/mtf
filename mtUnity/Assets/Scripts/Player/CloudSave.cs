﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.Services.CloudSave;
using UnityEngine;

namespace Assets.Scripts.Player
{
	public class CloudSave
	{
		private static CloudSave _instance;
		public static CloudSave Instance
		{
			get
			{
				_instance = _instance ?? new CloudSave();
				return _instance;
			}
		}
		private CloudSave()
		{
			LastUpdate = DateTime.Now;
		}

		public DateTime LastUpdate;
		public int Delay => 1; //in seconds 

		public void PeriodicalSave(Dictionary<string, string> dataset)
		{
			if ((DateTime.Now - LastUpdate).TotalMinutes > Delay)
			{
				SaveDataToCloud(dataset);
			}
		}

		public async Task<Dictionary<string, string>> LoadDataWithCheck(string localTimeStamp)
		{
			var data = await CloudSaveService.Instance.Data.Player.LoadAsync(new HashSet<string> { "Save_TimeStamp" });

			if (data.TryGetValue("Save_TimeStamp", out var serverTimeStamp))
			{
				if (DateTime.TryParse(serverTimeStamp.ToString(), out DateTime stamp) && DateTime.TryParse(localTimeStamp, out DateTime localStamp))

					if ((stamp - localStamp).TotalMilliseconds > 0)
					{
						var dataSet = await LoadDataFromCloud();
						return dataSet;
					}

			}
			return null;
		}
		private async void SaveDataToCloud(Dictionary<string, string> dataset)
		{
			LastUpdate = DateTime.Now;
			var stamp = LastUpdate.ToString("G");

			if (dataset.ContainsKey("Save_TimeStamp"))
			{
				dataset["Save_TimeStamp"] = stamp;
			}
			else
			{
				dataset.Add("Save_TimeStamp", stamp);
			}

			var cloudDataset = new Dictionary<string, object>();
			foreach (var item in dataset)
			{
				cloudDataset.Add(item.Key.Replace(".", "-"), item.Value);
			}
			await CloudSaveService.Instance.Data.Player.SaveAsync(cloudDataset);
			Debug.Log($"Cloud save. {dataset.Count} - items. Time - {stamp}");
		}
		private async Task<Dictionary<string, string>> LoadDataFromCloud()
		{
			var cloudDataset = await CloudSaveService.Instance.Data.Player.LoadAllAsync();

			var dataSet = new Dictionary<string, string>();
			foreach (var item in cloudDataset)
			{
				dataSet.Add(item.Key.Replace(".", "-"), item.Value.ToString());
			}
			return dataSet;
		}
	}
}
