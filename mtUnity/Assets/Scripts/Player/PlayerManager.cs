﻿using Assets.Scripts.Player;
using MTData;
using MTDialogs;
using MTParameters;
using MTCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using static GameStarter;
using static MTTown.TripsDispatcher;

namespace MTPlayer
{
	public class PlayerManager
	{
		private DataHadler DataHandler { get; set; }

		private List<InGameResourse> _playerResources { get; set; } // кеширование cпиcка реcурcов
		private List<GlobalEvents> _playerEvents { get; set; }
		private int _currentLevel { get; set; } // кеширование уровня
		public AppData Settings => DataHandler.Settings;
		public PlayerData Data => DataHandler.Data;
		public TripHistoryData TripHistory => DataHandler.TripHistory;
		private PlayerManager() { }
		private PlayerManager(InBuildParameters parameters)
		{
			DataHandler = DataHadler.Init(parameters);

			OnChangeResources += () =>
			{
				_playerResources = Data.GetList<InGameResourse>(GameStarter.GlobalValues.User_resourses.ToString());
			};
			OnDataLoaded -= () =>
			{
				OnChangeResources?.Invoke();
				_currentLevel = Data.GetInt(GameStarter.GlobalValues.Level_active.ToString());
				_playerEvents = Data.GetList<GlobalEvents>(GameStarter.GlobalValues.Global_Events.ToString());
			};
		}

		public async static Task<PlayerManager> Init(InBuildParameters parameters)
		{
			var pm = new PlayerManager(parameters);
			pm.LOAD();
			return pm;
		}

		public virtual void LOAD(System.Action callBack = null)
		{
			Settings.LOAD(() =>
			{
				Data.LOAD(() =>
				{
					callBack?.Invoke();
					OnDataLoaded?.Invoke();
				});
				TripHistory.LOAD();
			});
		}
		public virtual void SAVE(System.Action callBack = null)
		{
			Settings.SAVE(() =>
			{
				Data.SAVE(() =>
				{
					if (ResourceLoader.IsInternetAvailable)
					{
						CloudSave.Instance.PeriodicalSave(Data.GetAll());
					}
					else
					{
						Debug.Log($"CloudSave: offline!!!");
					}
					if (callBack != null) callBack();
				});
			});
			Debug.LogWarning("Saaaaaaave!!!");
		}

		//логика работы c данными игрока
		public void Do(System.Action actions, bool save = true)
		{
			actions();
			if (save)
				SAVE();
		}
		//cобытия
		public UnityAction OnChangeResources;
		public UnityAction OnDataLoaded;
		public UnityAction<int> OnChangeTicketsAmount;

		//Операции c шестеренками
		public List<CogInfo> GetCogInfos()
		{
			var cogInfos = Data.GetList<CogInfo>(GlobalValues.Cogs.ToString());
			return cogInfos ?? new List<CogInfo>();
		}
		public void SetCogInfos(List<CogInfo> cogInfos)
		{
			Do(() =>
			{
				Data.SetList(GlobalValues.Cogs.ToString(), cogInfos);
			});
		}

		public CogInfo GetCogInfo(int? objId)
		{
			var cogInfos = GetCogInfos();
			var info = cogInfos.FirstOrDefault(c => c.Id == objId);
			return info;
		}
		public CogInfo UpdateCogInfo(int objId, int value, System.Action onDone = null)
		{
			var cogInfos = GetCogInfos();
			var cogInfo = cogInfos.FirstOrDefault(c => c.Id == objId);

			cogInfo.Amount = cogInfo.TargetAmount - cogInfo.Amount > value ? cogInfo.Amount + value : cogInfo.TargetAmount;
			cogInfo.IsDone = cogInfo.TargetAmount == cogInfo.Amount;

			SetCogInfo(cogInfo);

			if (cogInfo.IsDone)
			{
				onDone?.Invoke();
			}

			return cogInfo;
		}
		public CogInfo SetCogInfo(CogInfo info)
		{
			var cogInfos = GetCogInfos();
			var cogInfo = cogInfos.FirstOrDefault(c => c.Id == info.Id);

			if (cogInfo == null)
			{
				cogInfos.Add(info);
			}
			else
			{
				var index = cogInfos.IndexOf(cogInfo);
				cogInfos[index] = info;
			}

			SetCogInfos(cogInfos);

			return info;
		}

		//Операции c реcурcами
		public bool TryReduceResource(int resource, int amount)
		{
			return TryReduceResource(resource, amount, out int value);
		}
		public bool TryReduceResource(int resource, int amount, out int value)
		{
			value = ResourceGetAmount(resource);
			if (value >= amount)
			{
				value -= amount;
				ResourceSetAmount(resource, value);
				return true;
			}
			Debug.LogWarning($"Amount to high. Has - {value}. Need - {amount}");
			return false;
		}
		public void ResourceIncreaseAmount(int resource, int amount)
		{
			var value = ResourceGetAmount(resource);
			ResourceSetAmount(resource, value + amount);
			GameStarter.Instance.GamePlayManager.UpdateCollection(resource, amount);
		}
		public int ResourceGetAmount(int resource)
		{
			var resources = GetUserResources();
			var amount = resources.FirstOrDefault(r => r.Id == resource)?.Amount;
			return amount ?? 0; ;
		}
		private void ResourceSetAmount(int resource, int amount)
		{
			var resources = Data.GetList<InGameResourse>(GameStarter.GlobalValues.User_resourses.ToString());

			if (resources == null)
				resources = new List<InGameResourse>();

			var currentResource = resources?.FirstOrDefault(r => r.Id == resource);

			if (currentResource != null)
			{
				currentResource.Amount = amount;
			}
			else
			{
				currentResource = new InGameResourse { Id = resource, Amount = amount };
				resources.Add(currentResource);
			}
			Do(() =>
			{
				Data.SetList<InGameResourse>(GameStarter.GlobalValues.User_resourses.ToString(), resources);
				OnChangeResources?.Invoke();
			});
		}
		public List<InGameResourse> GetUserResources()
		{
			return _playerResources ?? new List<InGameResourse>();
		}
		public List<InGameTask> GetUserIngameTasks()
		{
			var tasks = Data.GetList<InGameTask>(GameStarter.GlobalValues.User_tasks.ToString());
			return tasks ?? new List<InGameTask>();
		}
		public InGameTask GetUserIngameTask(int town, int resourse)
		{
			var tasks = GetUserIngameTasks();
			var task = tasks.FirstOrDefault(t => t.Town == town && t.ResourseType == resourse);
			
			return task == null ? tasks.FirstOrDefault() : task;
		}
		public InGameTask GetUserIngameTask(string id)
		{
			var tasks = GetUserIngameTasks();
			var task = tasks.FirstOrDefault(t => t.Id == id);
			return task;
		}
		//
		public void AddUserIngameTask(InGameTask task)
		{
			var tasks = GetUserIngameTasks();
			var oldTask = tasks.FirstOrDefault(t => t.Id == task.Id);
			if (oldTask != null)
			{
				tasks.Remove(oldTask);
			}
			tasks.Add(task);
			Do(() =>
			{
				Data.SetList(GlobalValues.User_tasks.ToString(), tasks);
			});
		}
		public void RemoveUserIngameTask(InGameTask task)
		{
			var tasks = GetUserIngameTasks();

			var result = tasks.Where(t => t.Id != null && t.Id != task.Id).ToList();
			Debug.Log($"Remove{task.Id}");
			Do(() =>
			{
				Data.SetList(GlobalValues.User_tasks.ToString(), result);
				
				GameStarter.Instance.TownManager.Town.UpdateInteractiveCharacters();
				GameStarter.Instance.TownDataManager.WaysDispatcher.BuildTaskTrips();
			});
			Debug.Log($"list {GetUserIngameTasks().Count()}");
		}
		public void CompleteUserIngameTask(InGameTask task)
		{
			if (CheckUserIngameTask(task))
			{
				Do(() =>
				{
					if (TryReduceResource(task.ResourseType, task.ResourseAmount))
					{
						ResourceIncreaseAmount(task.PrizeType, task.PrizeAmount);
						RemoveUserIngameTask(task);
						GameStarter.Instance.TownDataManager.WaysDispatcher.BuildTaskTrips();
					}
				});
				return;
			}
			throw new System.ArgumentException("Insufficient resource");
		}
		public bool? CheckUserIngameTask(int town, int resourse)
		{
			var tasks = GetUserIngameTasks();
			var task = tasks.FirstOrDefault(t => t.Town == town && t.ResourseType == resourse);
			if (task == null)
				return null;
			return task.ResourseAmount <= ResourceGetAmount(task.ResourseType);
		}
		public bool CheckUserIngameTask(InGameTask task)
		{
			return task.ResourseAmount <= ResourceGetAmount(task.ResourseType);
		}
		public void SetCurrentLevel(int id)
		{
			if (Data.GetInt(GameStarter.GlobalValues.Level_active.ToString()) != id)
				Do(() =>
				{
					Data.SetInt(GameStarter.GlobalValues.Level_active.ToString(), id);
				});
		}
		public int GetCurrentLevel()
		{
			return _currentLevel;
		}
		public bool TryGetTrain(int trainId)
		{
			var trains = GetTrains().ToList();
			return trains.Any() && trains.Contains(trainId);
		}
		public int[] GetTrains()
		{
			var trains = Data.GetString(GameStarter.GlobalValues.Trains.ToString());
			return string.IsNullOrEmpty(trains) ? new int[] { 0 } : trains.Split(',').Select(t => int.Parse(t.Trim())).ToArray();
		}
		public void AddTrain(int trainId)
		{
			Do(() =>
			{
				var trains = GetTrains().ToList();
				trains.Add(trainId);
				trains.OrderBy(t => t).Distinct().ToList();
				Data.SetString(GameStarter.GlobalValues.Trains.ToString(), string.Join(", ", trains));
			});
		}
		public TrainInfo GetActiveTrain()
		{
			var train = Data.GetObject<TrainInfo>(GameStarter.GlobalValues.Active_Train.ToString());
			return train;
		}
		public void SetActiveTrain(int trainId)
		{
			if (Data.GetObject<TrainInfo>(GameStarter.GlobalValues.Active_Train.ToString())?.Id != trainId)
				Do(() =>
				{
					var train = new TrainInfo() { Id = trainId };
					Data.SetObject(GameStarter.GlobalValues.Active_Train.ToString(), train);
				});
		}
		public void SetTutorialElementDone(List<string> elementsDone)
		{
			Do(() =>
			{
				Data.SetList<string>(GameStarter.GlobalValues.Tutorial_Elements.ToString(), elementsDone);
			});
		}
		public List<CoachDepoInfo> GetCoachesList()//список купленых вагонов
		{
			var coachesList = Data.GetList<CoachDepoInfo>(GameStarter.GlobalValues.Coaches_List.ToString());
			return coachesList ?? new List<CoachDepoInfo>() { new CoachDepoInfo() { Id = 1000 } };
		}
		public void SaveCoach(MTParameters.Coach coach)// заполнение данными при покупке вагона через данные в парамсах
		{
			SaveCoach(new CoachDepoInfo() { Id = coach.ID, Capacity = coach.Capacity });
		}
		public void SaveCoach(CoachDepoInfo coach)
		{
			Do(() =>
			{
				var coaches = GetCoachesList();
				bool found = false;
				for (int i = 0; i < coaches.Count; i++)
				{
					if (coaches[i].Id == coach.Id)
					{
						found = true;
						coaches[i] = coach;
						break;
					}
				}
				if (!found)
				{
					coaches.Add(coach);
				}

				Data.SetList(GameStarter.GlobalValues.Coaches_List.ToString(), coaches);
			});
		}
		public void SetActiveCoaches(params CoachInfo[] coaches)
		{
			Do(() =>
			{
				var coachesToSave = coaches.ToList();
				Data.SetList(GameStarter.GlobalValues.Active_Coaches.ToString(), coachesToSave);
			});
		}
		public List<CoachInfo> GetActiveCoaches()
		{
			var coaches = Data.GetList<CoachInfo>(GameStarter.GlobalValues.Active_Coaches.ToString());
			return coaches;
		}
		public void SetActiveCoaches(params int[] coaches) //проверить заменить использование если надо
		{
			SetActiveCoaches(coaches.Select(c => new CoachInfo() { Id = c }).ToArray());
		}
		public void SetCurrentLanguage(TranslationManager.Language languageName)
		{
			if (Data.GetInt(GameStarter.GlobalValues.Current_Language.ToString()) != (int)languageName)
				Do(() =>
				{
					Data.SetInt(GameStarter.GlobalValues.Current_Language.ToString(), (int)languageName);
					Debug.Log($"Current language - {languageName}");
				});
		}
		public void Complete(MTDialog dialog)
		{
			Do(() =>
			{
				Data.SetBool($"{dialog.GetType().Name}", true);
			});
		}
		public bool IsCompleted<T>()
		{
			var dialog = (T)Activator.CreateInstance(typeof(T));
			return Data.GetBool($"{dialog.GetType().Name}");
		}
		public int GetCurrentLanguage()
		{
			return Data.GetInt(GameStarter.GlobalValues.Current_Language.ToString());
		}
		public List<MTGameAccsess> GetGameAccessList()
		{

			var accessList = Data.GetList<MTGameAccsess>(GameStarter.GlobalValues.Game_Acsess_List.ToString());
			return accessList ?? new List<MTGameAccsess>();

		}
		public MTGameAccsess GetGameAccess(GameAccsessType gameAccsessType)
		{
			var gameAccessList = GetGameAccessList();
			var gameAccess = gameAccessList.FirstOrDefault(a => a.gameAccsessType == gameAccsessType);
			return gameAccess;
		}
		public void AddGameAccess(GameAccsessType gameAccsessType)
		{
			if (GetGameAccess(gameAccsessType) == null)
			{
				var gameAccessList = GetGameAccessList();
				gameAccessList.Add(new MTGameAccsess(gameAccsessType));
				Do(() =>
				{
					Data.SetList(GlobalValues.Game_Acsess_List.ToString(), gameAccessList);
				});
			}
		}
		public void UpdateGameAccess(MTGameAccsess gameAccsess)
		{
			var gameAccessList = GetGameAccessList();
			var listAccess = gameAccessList.FirstOrDefault(a => a.gameAccsessType == gameAccsess.gameAccsessType);

			if (listAccess != null)
			{
				listAccess.goal = gameAccsess.goal;
				listAccess.value = gameAccsess.value;
				listAccess.rate = gameAccsess.rate;

				Do(() =>
				{
					Data.SetList(GlobalValues.Game_Acsess_List.ToString(), gameAccessList);
				});
			}
		}
		public void UpdateGameAccessList(List<MTGameAccsess> gameAccessList)
		{
			if (gameAccessList != null)
			{
				Do(() =>
				{
					Data.SetList(GlobalValues.Game_Acsess_List.ToString(), gameAccessList);
				});
			}
		}
		public void applyTripResults(TripLogModel tripResult)
		{

			var userAccessList = GetGameAccessList();

			foreach (var access in userAccessList)
			{
				switch (access.gameAccsessType)
				{
					case GameAccsessType.Passenger:
						access.value += tripResult.resourceGathered;
						break;
					case GameAccsessType.Wood:
						access.value += tripResult.resourceGathered;
						break;
					default:
						break;
				}
				if (access.goal <= access.value)
				{
					access.value -= access.goal;
					var newGoal = Mathf.Floor(access.goal * access.rate);
					access.goal = (int)newGoal;
					access.level++;
				}
			}

			UpdateGameAccessList(userAccessList);
		}
		public List<string> GetCompletedDialogs(string prefix)
		{
			var dialogs = Data.GetList<string>(prefix);
			return dialogs ?? new List<string>();
		}
		public void UpdateCompletedDialogs(string prefix, List<string> completedDialogs)
		{
			if (completedDialogs != null)
			{
				Do(() =>
				{
					Data.SetList(prefix, completedDialogs);
				});
			}
		}
		public int GetTickets()
		{
			var tickets = Data.GetInt(GlobalValues.User_tickets.ToString());
			return tickets;
		}
		public bool TryReduceTickets(out int tickets)
		{
			tickets = GetTickets();
			if (tickets == 0)
				return false;
			tickets--;
			UpdateTickets(tickets);
			return true;
		}
		public int AddTickets(int ticketsToAdd)
		{
			var tickets = GetTickets();
			tickets += ticketsToAdd;
			UpdateTickets(tickets);
			return tickets;
		}
		public void UpdateTickets(int tickets)
		{
			Do(() =>
			{
				Data.SetInt(GlobalValues.User_tickets.ToString(), tickets);
				OnChangeTicketsAmount(Data.GetInt(GlobalValues.User_tickets.ToString()));
			});
		}
		public bool IsEventCompleted(GlobalEvents globalEvent)
		{
			var _pE = GetPlayerIvents();
			return _pE.Contains(globalEvent);
		}
		public void CompleteEvent(GlobalEvents globalEvent)
		{
			UpdateEvent(globalEvent);
		}
		public void ClearEvent(GlobalEvents globalEvent)
		{
			UpdateEvent(globalEvent, true);
		}
		private void UpdateEvent(GlobalEvents globalEvent, bool remove = false)
		{
			Do(() =>
			{
				var _pE = GetPlayerIvents();
				if (remove)
				{
					if (_pE.Contains(globalEvent))
						_pE.Remove(globalEvent);
				}
				else
				{
					_pE.Add(globalEvent);
				}

				Data.SetList<GlobalEvents>(GameStarter.GlobalValues.Global_Events.ToString(), _pE);
			});
		}
		public List<GlobalEvents> GetPlayerIvents()
		{
			return _playerEvents ?? new List<GlobalEvents>();
		}
	}

	public class InGameResourse
	{
		public int Id;
		public int Amount;
	}

	public class MTGameAccsess
	{
		public GameAccsessType gameAccsessType;
		public int level;
		public float rate;
		public int goal;
		public int value;

		public MTGameAccsess(GameAccsessType gameAccsessType)
		{
			this.gameAccsessType = gameAccsessType;
			level = 1;
			rate = 1.3f;
			goal = 100;
			value = 0;
		}
	}

	public class TrainInfo
	{
		public int Id;
	}

	public class CoachDepoInfo // модель для хранения прокачки
	{
		public int Id;
		/// <summary>
		/// количество вагонов этого типа у игрока
		/// </summary>
		public int Number = 1;
		public int Capacity = 1;

		/// <summary>
		/// когда прогресс достигает 100% увеличиваем уровень аттрибута на 1 (level) а значение на какую-то величину*/
		/// </summary>
		public int NumberLevel = 0;
		/// <summary>
		/// по этому id нужно получать cogInfo в котором хранится прогресс прокачки количества вагонов
		/// </summary>
		public int NumberCog => 10 * 1000 + Id;


		public int CapacityLevel = 0;
		/// <summary>
		/// по этому id нужно получать cogInfo в котором хранится прогресс прокачки вместимости вагонов этого типа
		/// </summary>
		public int CapacityCog => 11 * 1000 + Id;
	}

	public class CoachInfo // для хранения изменений в активных вагонах
	{
		public int Id;
		public int ColorId;
	}
	public class CogInfo
	{
		public int Id;
		public int Amount;
		public int TargetAmount;
		public bool IsDone;
	}
}
