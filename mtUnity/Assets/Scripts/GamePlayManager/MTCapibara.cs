﻿using MTParameters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace MTCore
{
	/// <summary>
	/// Represents a capybara character in the game with characteristics such as breed, features, targets, 
	/// and various status indicators (e.g., active, happy, sad). The capybara can have associated rewards, 
	/// priorities, and a birth date for tracking age.
	/// </summary>
	/// <remarks>
	///  <see cref="Targets"/>  <see cref="MTDataCollection"/> 
	/// </remarks>
	[System.Serializable]
	public class MTCapibara : NamedIDParameters
	{
		public string Id;
		public string Breed;

		public int Character;

		public string Features;
		public string Targets;
		public string Counters;
		public string Rewards;

		public string Rewarder;

		public int Priority;

		public bool IsActive;
		public bool IsHappy;

		public bool IsSad;
		public bool IsOld;

		public DateTime BirthDay;

		public bool WithTail => Targets.Contains("=>");
		public int Tail;
		public MTCapibara() : base(0, "capibara")
		{
			Breed = "common";
			Character = (int)GameCharacter.Fritz;

			Features = "single";

			Targets = string.Empty;
			Counters = string.Empty;
			Rewards = string.Empty;

			Rewarder = "0";

			Priority = 100;
			IsActive = true;
			IsSad = false;
			IsOld = false;

			BirthDay = DateTime.Now;
		}

		public static MTCapibara Create(string id, int character, string breed, string features, string targets, string rewards, string rewarder, int priority, bool isActive)
		{
			var capibara = new MTCapibara();
			capibara.Id = id;
			capibara.Character = character;
			capibara.Breed = breed;
			capibara.Features = features;
			capibara.Targets = targets;
			capibara.Rewards = rewards;
			capibara.Rewarder = rewarder;
			capibara.Priority = priority;
			capibara.IsActive = isActive;
			return capibara;
		}
		/// <summary>
		/// Capibara age in minutes
		/// </summary>
		public int GetAge(string t = "s")
		{
			var age = DateTime.Now.Subtract(BirthDay);
			switch (t)
			{
				case "d":
					return (int)age.TotalDays;
				case "h":
					return (int)age.TotalHours;
				case "m":
					return (int)age.TotalMinutes;
				case "s":
				default:
					return (int)age.TotalSeconds;
			}

		}

		public int GetTargetTailLength()
		{
			return WithTail ? Targets.Split("=>").Length - 1 : 0;
		}
		public List<(int, int)> GetTargets()
		{
			return GetParams(WithTail ? Targets.Split("=>")[Tail] : Targets);
		}
		public List<(int, int)> GetCounters()
		{
			if (Counters == string.Empty)
				return GetTargets();
			return GetParams(WithTail ? Counters.Split("=>")[Tail] : Counters);
		}

		public void UpdateCounters(List<string> resultCounters)
		{
			Counters = "";

			var targetTail = GetTargetTailLength();

			for (int i = 0; i <= targetTail; i++)
			{
				if (i == Tail)
				{
					Counters += string.Join('|', resultCounters);
				}
				else if (i < Tail)
				{
					Counters += string.Join('|', GetParams(Targets.Split("=>")[i]).Select(t => t.Item1 + ":0"));
				}
				if (i > Tail)
				{
					Counters += Targets.Split("=>")[i];
				}
				if (i != targetTail)
				{
					Counters += "=>";
				}
			}
		}
		public List<(int, int)> GetRewards()
		{
			return GetParams(Rewards);
		}

		public List<(string, string)> GetFeatures()
		{
			return GetStringParams(Features);
		}
		private List<(int, int)> GetParams(string paramsString)
		{
			List<(int, int)> result = new List<(int, int)>();

			var stringParams = GetStringParams(paramsString);
			foreach (var item in stringParams)
			{
				if (int.TryParse(item.Item1, out var paramKey))
				{
					if (int.TryParse(item.Item2, out var paramValue))
					{
						result.Add((paramKey, paramValue));
					}
				}
			}
			return result;
		}
		private List<(string, string)> GetStringParams(string paramsString)
		{
			var paramsList = paramsString.Split("|").ToList();
			var result = new List<(string, string)>();

			foreach (var item in paramsList)
			{
				var splits = item.Split(":");
				result.Add((splits[0], splits.Length > 1 ? splits[1] : ""));
			}
			return result;
		}

		public MTCapibara Clone()
		{
			return new MTCapibara
			{
				Id = this.Id,
				Breed = this.Breed,
				Character = this.Character,
				Features = this.Features,
				Targets = this.Targets,
				Counters = this.Counters,
				Rewards = this.Rewards,
				Rewarder = this.Rewarder,
				Priority = this.Priority,
				IsActive = this.IsActive,
				IsHappy = this.IsHappy,
				IsSad = this.IsSad,
				IsOld = this.IsOld,
				BirthDay = this.BirthDay,
				Tail = this.Tail,
			};
		}

		/// <summary>
		/// Вычисляет хэш от ключевых полей объекта.
		/// </summary>
		public string ComputeHash()
		{
			using (var sha256 = SHA256.Create())
			{
				var inputString = $"{Breed}|{Character}|{Features}|{Targets}|{Rewards}|{Rewarder}";
				var bytes = Encoding.UTF8.GetBytes(inputString);
				var hash = sha256.ComputeHash(bytes);
				return string.Concat(hash.Select(b => b.ToString("x2")));
			}
		}

		/// <summary>
		/// Проверяет, изменился ли объект по сравнению с другим.
		/// </summary>
		public bool HasChanged(MTCapibara other)
		{
			if (other == null) return true;
			return ComputeHash() != other.ComputeHash();
		}
	}
}
