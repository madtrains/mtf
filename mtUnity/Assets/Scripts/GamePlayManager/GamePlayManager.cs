﻿using MTTown.Characters;
using MTTown.PointsOfInterest;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.WebSockets;
using UnityEngine;
using UnityEngine.UIElements;
using static GameStarter;

namespace MTCore
{
	public class GamePlayManager
	{
		public enum ProgressMarks
		{
			ShowAll = 4242,
			FirstMeet,
			TabloTownIcon,
			TasksIcon,
			WenderMachineIcon,
			SawMillIcon,
			DepoIcon,
			ExpressIcon,
		}

		private bool _clearOnStart = false;
		private MTDataCollector DataCollector;
		private CapibaraService _capibaraService;
		public Dictionary<MTDataCollection, int> DataCollection => DataCollector.DataCollection;
		private GamePlayManager()
		{
			DataCollector = new MTDataCollector();
			_capibaraService = new CapibaraService(DataCollection);
		}
		public void UpdateCollection(int item, int number = 1)
		{
			try
			{
				UpdateCollection((MTDataCollection)item, number);
			}
			catch (Exception e)
			{
				Debug.LogException(e);
			}
		}
		public void UpdateCollection(MTDataCollection item, int number = 1)
		{
			Debug.Log(item.ToString());
			DataCollector.UpdateCollection(item, number);
		}
		public (bool hasDialog, bool hasTask) GetCharacterState(GameCharacter character)
		{
			var hasWaitingCapibaras = _capibaraService.HasWaitingCapibaras(character);
			return (false, hasWaitingCapibaras);
		}
		public void StartDialog(InteractiveCharacter character, Action callBack)
		{
			var charState = GetCharacterState(character.Character);

			if (charState.hasDialog)
			{
				//todo
			}
			else if (charState.hasTask)
			{
				var capibara = GetCapibara(character.Character);
				if (capibara != null)
				{
					character.Dialogs.ResourceTaskDialog(capibara, callBack);
				}
			}
		}
		public void RunCheckers()
		{
			Debug.Log("RunCheckers");
			_capibaraService.CheckCapibarasHealth();
			DataCollector.Clear();
			GameStarter.Instance.PlayerManager.SAVE();
		}
		public static GamePlayManager Init()
		{
			var gpm = new GamePlayManager();
			gpm.PrepareCapibaras();
			return gpm;
		}
		/// <summary>
		public MTCapibara GetCapibara(GameCharacter character, params string[] breeds)
		{
			return _capibaraService.GetCapibara(character, breeds);
		}
		public List<MTCapibara> GetCapibaras(params string[] breeds)
		{
			return _capibaraService.GetCapibaras(breeds);
		}
		public bool HasWaitingCapibaras(GameCharacter character, params string[] breeds)
		{
			var capibara = _capibaraService.GetCapibara(character, breeds);
			return capibara != null;
		}
		private void PrepareCapibaras()
		{
			if (_clearOnStart)
			{
				_capibaraService.UpdateZoo();
			}
			_capibaraService.GetCapibarasInZoo();
		}
		public void CapibaraReadyToWork(MTCapibara capibara)
		{
			RunCheckers();
			_capibaraService.AddToZoo(capibara);
		}
		public void CapibaraWellDone(MTCapibara capibara)
		{
			_capibaraService.MilkCapibara(capibara);
		}
		public List<MTCapibara> GetZoo()
		{
			return _capibaraService.GetZoo();
		}
		public void SaveProgress(ProgressMarks mark)
		{
			var playerManager = GameStarter.Instance.PlayerManager;
			var pm = playerManager.Data.GetList<string>(GlobalValues.Progress_Marks.ToString()) ?? new List<string>();
			if (!pm.Contains(mark.ToString()))
			{
				pm.Add(mark.ToString());
				playerManager.Do(() =>
				{
					playerManager.Data.SetList<string>(GlobalValues.Progress_Marks.ToString(), pm);
				});
			}
		}
		public bool CheckProgressForObject<T>(T obj)
		{
			var pm = GameStarter.Instance.PlayerManager.Data.GetList<string>(GlobalValues.Progress_Marks.ToString()) ?? new List<string>();
			if (pm.Contains(ProgressMarks.ShowAll.ToString()))
				return true;

			if (obj is Tag tag)
			{
				return tag switch
				{
					Tag.Overview or
					Tag.ShortArrival or
					Tag.Center or
					Tag.Coupler or
					Tag.SFDisplay or
					Tag.InTownStart or
					Tag.AlterFritz => true,

					Tag.Depot => pm.Contains(ProgressMarks.DepoIcon.ToString()),
					Tag.LumberMill => pm.Contains(ProgressMarks.SawMillIcon.ToString()),
					Tag.Express => pm.Contains(ProgressMarks.ExpressIcon.ToString()),

					_ => false
				};
			}
			return false;
		}

	}
}
