﻿namespace MTCore
{
	public enum MTDataCollection
	{
		Coin = 0, //все ресурсы что будут собираемые от 0 до 1000
		Passenger = 1,
		Mail = 2,
		Oil = 3,
		Iron = 4,
		Wood = 5,
		Fish = 6,
		Freight = 7,
		Ticket = 8,
		Cog = 9,
		Action = 1000,//Все экшены от 1000 до 1999
		Click_tablo = 1001,
		Start_flipper = 1002,
		Finish_flipper = 1003,
		Visit_town_point_overview = 1100,//посешени игровых локаций
		Visit_town_point_depot = 1101,
		Visit_town_point_short_arrival = 1102,
		Visit_town_point_center = 1103,
		Visit_town_point_coupler = 1104,
		Visit_town_point_express = 1105,
		Visit_town_point_sdfdisplay = 1106,
		Visit_town_point_intownstart = 1107,
		Visit_town_point_limbermill = 1108,
		Visit_town_point_alter_fritz = 1109,
		Statictics = 2000, // все статистики от 2000 до 2999
		Flips_number = 2001,
		Horn_number = 2002,
		Bumps_number = 2003,
		Lives_lost_number = 2004,
		Lives_get_number = 2005,
		Stations_passed_number = 2006,
		Tickets_get_number = 2007,
		Tickets_used_number = 2008,
		Coins_lost_number = 2009,
		Cogs_lost_number = 2010,
		Deadly_crushes_number = 2011,
		Spins_number = 2012,
		Double_spins_number = 2013,
		Switchers_used_number = 2014,
		Switch_town_points_number = 2015,
		Finish_daily_task_number = 2016,
		Timers = 3000, // все таймеры ( в секундах )
		Level_active_time = 3001,
		Level_upside_time = 3002,
		Level_downside_time = 3003,
		Top_speed_time = 3004,
		Horn_time = 3005,
		Spin_time = 3006,
	}
}
