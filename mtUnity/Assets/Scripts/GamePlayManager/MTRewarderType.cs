﻿namespace MTCore
{
	public enum MTRewarderType
	{
		Empty = 0,
		Message = 1,
		Resource = 2,
		Event = 3,
	}
}
