﻿using System.Collections.Generic;

namespace MTCore
{
	public class MTDataCollector
	{
		public Dictionary<MTDataCollection, int> DataCollection { get; }

		public void Clear()
		{
			DataCollection.Clear();
		}
		public void UpdateCollection(MTDataCollection item, int number = 1)
		{
			if (DataCollection.ContainsKey(item))
			{
				DataCollection[item] += number;
				return;
			}
			DataCollection.Add(item, number);
		}
		public MTDataCollector()
		{
			DataCollection = new Dictionary<MTDataCollection, int>();
		}
	}
}
