﻿using System;

namespace MTCore
{
	public class GamePlayTimer
	{
		public string Id;
		public string Type;
		public string Action;
		public string Value;
		public DateTime Time;
		public GamePlayTimer() { }
		public GamePlayTimer(string id, string type, string action)
		{
			Id = id;
			Type = type;
			Action = action;
			Time = DateTime.Now;
		}
		public void UpdateTime(DateTime? time = null)
		{
			Time = time ?? DateTime.Now;
		}
	}
}
