﻿using MTDialogs;
using MTTown;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace MTCore
{
	public static class MTRewarder
	{
		public static void Run(MTCapibara capibara, Action callback)
		{
			Debug.Log("Run rewarder");
			var rewarderType = GetRewarderType(capibara.Rewarder);

			switch (rewarderType)
			{
				case MTRewarderType.Empty:
					break;
				case MTRewarderType.Message:
					ProcessMessageType(capibara, callback);
					break;
				case MTRewarderType.Resource:
					callback?.Invoke();
					ProcessResourceType(capibara);
					break;
				case MTRewarderType.Event:
					ProcessEventType(capibara);
					callback?.Invoke();
					break;
				default:
					callback?.Invoke();
					break;
			}
		}

		private static MTRewarderType GetRewarderType(string taskRewarder)
		{
			try
			{
				var splitRewarder = taskRewarder.Split(':');

				var rewarder = (MTRewarderType)int.Parse(splitRewarder[0]);
				return rewarder;
			}
			catch (Exception e)
			{
				Debug.LogError($"Wrong rewarder: {taskRewarder}");
				return MTRewarderType.Empty;
			}
		}
		private static void ProcessMessageType(MTCapibara capibara, Action callback)
		{
			Debug.LogError($"Rewarder message: {capibara.Id}");

			var dialogKey = $"Capibara.{capibara.Id}.Result";
			var dialogText = GameStarter.Instance.TranslationManager.GetTableString(dialogKey);

			Debug.LogError($"Rewarder message: {capibara.Id}");

			var dialog = new MTDialog(dialogText, (GameCharacter)capibara.Character, () =>
			{
				callback.Invoke();
			});

			GameStarter.Instance.DialogManager.Show(dialog);
		}
		private static void ProcessEventType(MTCapibara capibara)
		{
			var splitRewarder = capibara.Rewarder.Split(':');
			string eventName = splitRewarder.Length > 1 ? splitRewarder[1] : "none";

			if (eventName == "none")
			{
				return;
			}
			try
			{
				GameStarter.Instance.EventManager.ProcessEvent(eventName);
				GameStarter.Instance.EventManager.RunEvents();
			}
			catch (Exception e)
			{
				Debug.LogError($"Rewarder event error: {e.Message}");
			}
			Debug.LogError($"Rewarder event: {eventName}");
		}
		private static void ProcessResourceType(MTCapibara capibara)
		{
			var rewards = capibara.GetRewards();

			foreach (var resource in rewards)
			{
				GameStarter.Instance.PlayerManager.ResourceIncreaseAmount(resource.Item1, resource.Item2);
			}
		}
	}
}
