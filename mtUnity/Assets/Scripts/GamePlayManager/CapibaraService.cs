﻿using MTTown.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GameStarter;
using UnityEngine;
using MTUi.TasksPopup;
using MTPlayer;
using UnityEngine.UIElements;
using MTTown;

namespace MTCore

{
	public class CapibaraService
	{
		private Dictionary<MTDataCollection, int> _dataCollection;

		private List<MTCapibara> _zoo;
		private List<GamePlayTimer> _timers;

		private string[] DefaultBreads = new string[] { "common", "flipper" };
		private Dictionary<string, int> _breedLimit = new Dictionary<string, int> {
			{ "common", 2 },
			{ "flipper", 1 },
			{ "daily", 3 },
			{ "weekly", 1 }
		};
		public List<MTCapibara> GetZoo(bool withHidden = false) { return withHidden ? _zoo : _zoo.Where(c => !c.Breed.Contains("hidden")).ToList(); }
		public CapibaraService(Dictionary<MTDataCollection, int> dataCollection)
		{
			_zoo = new List<MTCapibara>();
			_timers = new List<GamePlayTimer>();
			_dataCollection = dataCollection;

			Capibaras = new List<MTCapibara>();
			FillCapibaras();
		}

		public GamePlayTimer? GetTimer(MTCapibara capibara, string action)
		{
			return _timers.FirstOrDefault(t => t.Id == capibara.Id && t.Type == "capibara" && t.Action == action);
		}

		public void RemoveTimers(MTCapibara capibara)
		{
			var timersToRemove = _timers.Where(t => t.Id == capibara.Id && t.Type == "capibara").ToList(); ;
			foreach (var timer in timersToRemove)
			{
				_timers.Remove(timer);
			}
		}


		public List<MTCapibara> Capibaras;

		/// <summary>
		/// Возвращает капибару, связанную с указанным персонажем, с учетом приоритета.
		/// Если несколько капибар имеют одинаковый приоритет, выбирается случайная из них.
		/// </summary>
		/// <param name="character">Персонаж, связанный с капибарами.</param>
		/// <param name="breeds">Список пород для фильтрации. Если не указаны, используются породы по умолчанию.</param>
		/// <returns>Капибара с наивысшим приоритетом или случайная из капибар с одинаковым приоритетом.</returns>
		public MTCapibara GetCapibara(GameCharacter character, params string[] breeds)
		{
			// Получаем список капибар с фильтром по породам и персонажу
			var filteredBreeds = breeds.Length > 0 ? breeds : DefaultBreads;

			var characterCapibaras = GetCapibaras(filteredBreeds)
				.Where(c => c.Character == (int)character)
				.OrderBy(c => c.Priority)
				.ToList();

			Debug.Log($"Character capibaras: {characterCapibaras.Count}");

			var limitedCapibaras = characterCapibaras
				.GroupBy(c => c.Breed)
				.Where(group =>
				{
					// Получаем лимит для породы
					int breedLimit = _breedLimit.TryGetValue(group.Key, out var limit) ? limit : int.MaxValue;

					// Считаем уже активных капибар
					var alreadyActiveCount = _zoo.Count(z => z.Breed == group.Key);

					// Оставляем группу, если активных капибар меньше лимита
					return alreadyActiveCount < breedLimit;
				})
				.SelectMany(group => group) // Разворачиваем оставшиеся группы в единый список
				.ToList();

			Debug.Log($"Limited capibaras: {limitedCapibaras.Count}");

			// Если список пуст, возвращаем null
			if (limitedCapibaras.Count == 0)
			{
				//Debug.LogWarning("No capibaras found for the given criteria.");
				return null;
			}

			// Берем капибар с наивысшим приоритетом
			var topPriority = limitedCapibaras.First().Priority;
			var samePriorityCapibaras = limitedCapibaras.Where(c => c.Priority == topPriority).ToList();

			Debug.Log($"Same priority capibaras: {samePriorityCapibaras.Count}");
			// Если есть несколько капибар с одинаковым приоритетом, выбираем случайную
			if (samePriorityCapibaras.Count > 1)
			{
				int randomIndex = UnityEngine.Random.Range(0, samePriorityCapibaras.Count);
				Debug.Log($"Capibara index: {randomIndex}");
				return samePriorityCapibaras[randomIndex];
			}

			// Если только одна капибара с наивысшим приоритетом, возвращаем ее
			return limitedCapibaras.First();
		}

		public MTCapibara GetCapibara(string name)
		{
			var capibara = Capibaras.FirstOrDefault(c => c.Id == name);
			return (capibara != null && capibara.IsActive) ? capibara : null;
		}
		public List<MTCapibara> GetCapibaras(params string[] breeds)
		{
			Debug.LogWarning("GetCapibaras");

			// Используем переданные породы или значения по умолчанию
			breeds = breeds.Length > 0 ? breeds : DefaultBreads;

			// Получаем активных капибар выбранных пород
			var activeCapibaras = Capibaras
				.Where(c => c.IsActive && (breeds.Contains("Any") || breeds.Contains(c.Breed)))
				.OrderBy(t => t.Breed)
				.ToList();

			Debug.Log($"Active capibaras: {activeCapibaras.Count}");

			if (!activeCapibaras.Any())
			{
				return new List<MTCapibara>();
			}

			// Применяем дополнительные фильтры, если они есть
			var filteredCapibaras = FilterCapibarasFeatures(activeCapibaras).ToList();

			Debug.Log($"Filtered capibaras: {filteredCapibaras.Count}");

			return filteredCapibaras;
		}

		private List<MTCapibara> FilterCapibarasFeatures(List<MTCapibara> capibaras)
		{
			var result = FilterCapibarasWithBreadsConditions(capibaras);
			if (result.Any())
			{
				result = FilterCapibarasWithFeatures(result);
			}
			return result;
		}
		private List<MTCapibara> FilterCapibarasWithBreadsConditions(List<MTCapibara> capibaras)
		{
			var result = new List<MTCapibara>();
			foreach (var capibara in capibaras)
			{

				bool conditionAccepted = false;

				switch (capibara.Breed)
				{
					case "common":
						conditionAccepted = true;
						break;
					case "flipper":
						conditionAccepted = true;
						break;
					case "daily":
						conditionAccepted = CheckDailyCapibaraTimers(capibara);
						break;
					case "weekly":
						conditionAccepted = CheckDailyCapibaraTimers(capibara); //check
						break;
					case "hidden":
						conditionAccepted = true;
						break;
					case "event":
						conditionAccepted = true;
						break;
					default:
						break;
				}

				if (conditionAccepted)
				{
					result.Add(capibara);
				}
			}
			return result;
		}

		private bool CheckDailyCapibaraTimers(MTCapibara capibara)
		{
			var finishTimer = GetTimer(capibara, "finished");
			if (finishTimer != null)
			{
				var timerReady = (int)DateTime.Now.Subtract(finishTimer.Time).TotalSeconds > 60;
				if (timerReady)
				{
					RemoveTimers(capibara);
				}
				return timerReady;
			}

			return true;
		}
		private List<MTCapibara> FilterCapibarasWithFeatures(List<MTCapibara> capibaras)
		{
			var result = new List<MTCapibara>();
			foreach (var capibara in capibaras)
			{
				var features = capibara.GetFeatures();

				int acceptionRate = 0;
				foreach (var feature in features)
				{
					bool conditionAccepted = false;
					switch (feature.Item1)
					{
						case "single":
							acceptionRate--;
							conditionAccepted = _zoo.All(t => t.Id != capibara.Id);
							break;
						case "uniq":
							acceptionRate--;
							conditionAccepted = GetTimer(capibara, "finished") == null;
							Debug.LogWarning($"not finished - {capibara.Id} {conditionAccepted} {acceptionRate}");
							break;
						case "delay":
							acceptionRate--;
							var finishTimer = GetTimer(capibara, "finished");
							var delay = int.TryParse(feature.Item2, out var delayValue) ? delayValue : 0;
							var secondsPass = finishTimer != null ? (int)DateTime.Now.Subtract(finishTimer.Time).TotalSeconds : delay;
							var secondsLeft = delay - secondsPass;
							conditionAccepted = secondsLeft <= 0;
							Debug.LogWarning($"Capibara {capibara.ID} delayed. Delay: {delay}. Left: {(secondsLeft < 0 ? 0 : secondsLeft)} ");
							break;
						default:
							break;
					}
					acceptionRate += conditionAccepted ? 1 : 0;
				}

				if (acceptionRate >= 0)
				{
					result.Add(capibara);
				}
			}
			return result;
		}
		public bool HasWaitingCapibaras(GameCharacter character, params string[] breeds)
		{
			var capibara = GetCapibara(character, breeds);
			return capibara != null;
		}
		private void FillCapibaras()
		{
			FillGPCapibaras();
			//FillCapibarasWithTempData();
		}
		private void FillGPCapibaras()
		{
			var gpCapibaras = GameStarter.GameParameters.Capibaras;

			if (gpCapibaras != null && gpCapibaras.Any())
			{
				Capibaras.AddRange(gpCapibaras);
			}
		}
		private void FillCapibarasWithTempData()
		{
			var tm = GameStarter.Instance.TranslationManager;
			var capibaraStingTemplate = "Capibara.{0}.{1}";

			var capibara = new MTCapibara();
			capibara.Id = "C_1";
			capibara.Character = (int)GameCharacter.Fritz;

			capibara.Features = "single";

			capibara.Targets = "1:500";
			capibara.Rewards = "0:1500";

			capibara.Rewarder = "2";
			capibara.IsActive = true;

			Capibaras.Add(capibara);

			tm.AddDevString(string.Format(capibaraStingTemplate, capibara.Id, "Title"), "Вези меня вези...");
			tm.AddDevString(string.Format(capibaraStingTemplate, capibara.Id, "Description"), "Перевези пассажиров, сами ножками они не дойдут.");

			/************************************************************/
			capibara = new MTCapibara();
			capibara.Id = "C_2";
			capibara.Character = (int)GameCharacter.Franz;

			capibara.Features = "single";

			capibara.Targets = "5:1000";
			capibara.Rewards = "0:1000";

			capibara.Rewarder = "2";
			capibara.IsActive = true;

			Capibaras.Add(capibara);

			tm.AddDevString(string.Format(capibaraStingTemplate, capibara.Id, "Title"), "Только щепки летят...");
			tm.AddDevString(string.Format(capibaraStingTemplate, capibara.Id, "Description"), "Папа Карло расширяет производство. Ему нужно БОЛЬШЕ ЗОЛО... т.е. дерева.");

			/************************************************************/

			capibara = new MTCapibara();
			capibara.Id = "F_1";
			capibara.Character = (int)GameCharacter.Albert;

			capibara.Breed = "flipper";

			capibara.Features = "single|f:1";

			capibara.Targets = "2002:10";
			capibara.Rewards = "0:100";

			capibara.Rewarder = "2";

			Capibaras.Add(capibara);

			tm.AddDevString(string.Format(capibaraStingTemplate, capibara.Id, "Title"), "Дуть или не дуть, в чем тут вопрос? (в)");
			tm.AddDevString(string.Format(capibaraStingTemplate, capibara.Id, "Description"), "Используй гудок по назачению, пусть все тебя услышат.");

			/************************************************************/
			capibara = new MTCapibara();
			capibara.Id = "F_2";
			capibara.Character = (int)GameCharacter.Fritz;

			capibara.Breed = "flipper";
			capibara.Features = "single";

			capibara.Targets = "2001:50|f:1";
			capibara.Rewards = "0:200";

			capibara.Rewarder = "2";

			Capibaras.Add(capibara);

			tm.AddDevString(string.Format(capibaraStingTemplate, capibara.Id, "Title"), "Кручу верчу - бонус хочу (в)");
			tm.AddDevString(string.Format(capibaraStingTemplate, capibara.Id, "Description"), "Вертись как уж на сковородке. Я считаю.");

			/************************************************************/

			capibara = new MTCapibara();
			capibara.Id = "D_1";
			capibara.Character = (int)GameCharacter.Fritz;

			capibara.Breed = "daily";
			capibara.Features = "single";

			capibara.Targets = "1003:5";
			capibara.Rewards = "0:1000";

			capibara.Rewarder = "2";

			Capibaras.Add(capibara);

			tm.AddDevString(string.Format(capibaraStingTemplate, capibara.Id, "Title"), "Важен только результат (д)");
			tm.AddDevString(string.Format(capibaraStingTemplate, capibara.Id, "Description"), "Заверши нужное количество поездок, добравшись до конечной станции.");

			/************************************************************/

			capibara = new MTCapibara();
			capibara.Id = "D_2";
			capibara.Character = (int)GameCharacter.Fritz;

			capibara.Breed = "daily";
			capibara.Features = "single";

			capibara.Targets = "2015:5";
			capibara.Rewards = "0:1000";

			capibara.Rewarder = "2";

			Capibaras.Add(capibara);

			tm.AddDevString(string.Format(capibaraStingTemplate, capibara.Id, "Title"), "Ежедневный обход (д)");
			tm.AddDevString(string.Format(capibaraStingTemplate, capibara.Id, "Description"), "Осмотри локации в городе, посмотри кто чем занят, качество осмотра только на тебе.");

			/************************************************************/

			capibara = new MTCapibara();
			capibara.Id = "D_3";
			capibara.Character = (int)GameCharacter.Franz;

			capibara.Features = "single";

			capibara.Targets = "2:1000";
			capibara.Rewards = "0:1000";

			capibara.Rewarder = "2";
			capibara.IsActive = true;

			Capibaras.Add(capibara);

			tm.AddDevString(string.Format(capibaraStingTemplate, capibara.Id, "Title"), "Только не лижи марки...(д)");
			tm.AddDevString(string.Format(capibaraStingTemplate, capibara.Id, "Description"), "Печкин в отпуске вся почта только на тебе. Доставь письма адресатам.");

			/************************************************************/


			capibara = new MTCapibara();
			capibara.Id = "W_1";
			capibara.Character = (int)GameCharacter.Fritz;

			capibara.Breed = "weekly";
			capibara.Features = "single";

			capibara.Targets = "2016:10";
			capibara.Rewards = "0:1000";

			capibara.Rewarder = "2";

			Capibaras.Add(capibara);

			tm.AddDevString(string.Format(capibaraStingTemplate, capibara.Id, "Title"), "Эх раз еще раз, еще много... (н)");
			tm.AddDevString(string.Format(capibaraStingTemplate, capibara.Id, "Description"), "Как говорил один человек - Работать, работать и еще раз работать. Норму то никто не отменял.");

		}

		public void ProcessCapibaraFeatures(MTCapibara capibara)
		{
			var features = capibara.GetFeatures();

			foreach (var feature in features)
			{
				switch (feature.Item1)
				{
					case "f":
						var dk = MTDataCollection.Finish_flipper;

						if (_dataCollection.ContainsKey(dk))
						{
							var timerValue = _dataCollection[dk];
							var timer = GetTimer(capibara, "flippers_done");
							if (timer != null && int.TryParse(timer.Value, out var value))
							{
								timerValue += value;
							}
							UpdateCapibaraTimer(capibara, "flippers_done", timerValue.ToString());
						}
						break;
					default:
						break;
				}
			}
		}
		public void MakeCapibaraHappy(MTCapibara capibara)
		{
			var targets = capibara.GetTargets();
			var counters = capibara.GetCounters();

			var resultCounters = new List<string>();
			var complitions = 0;

			for (int i = 0; i < targets.Count(); i++)
			{
				var targetKey = targets[i].Item1;

				var counterValue = counters.Count() == targets.Count() ? counters[i].Item2 : 0;

				var dk = (MTDataCollection)targetKey;

				if (_dataCollection.ContainsKey(dk))
				{
					counterValue -= _dataCollection[dk];
				}

				resultCounters.Add($"{targetKey}:{(counterValue <= 0 ? 0 : counterValue)}");
				if (counterValue <= 0)
				{
					complitions++;
				}
			}

			capibara.UpdateCounters(resultCounters);

			var goalReached = complitions == targets.Count();

			capibara.IsHappy = capibara.Tail == capibara.GetTargetTailLength() && goalReached;

			if (!capibara.IsHappy && goalReached)
			{
				capibara.Tail++;
			}
			if (capibara.IsHappy)
			{
				var notifier = GameStarter.Instance.gameObject.GetComponentInChildren<CapibaraComplete>();
				notifier.ComplitionNotification(capibara);
			}
		}
		public void RetierCapibaras()
		{
			foreach (var capibara in _zoo)
			{
				var wildCapibara = Capibaras.FirstOrDefault(c => c.Id == capibara.Id);
				capibara.IsOld = wildCapibara == null || wildCapibara.HasChanged(capibara);

				if (capibara.IsOld && !capibara.IsHappy)
				{
					var notifier = GameStarter.Instance.gameObject.GetComponentInChildren<CapibaraComplete>();
					notifier.ComplitionNotification(capibara);
				}
			}
		}
		public void CheckCapibarasAge(MTCapibara capibara)
		{
			if (capibara.IsOld)
				return;

			if (capibara.Breed.Contains("daily") && capibara.BirthDay.DayOfWeek != DateTime.Now.DayOfWeek && capibara.GetAge() > 120)
			{
				capibara.IsOld = true;
				return;
			}

			if (capibara.Breed.Contains("weekly") && capibara.GetAge("d") > 7)
			{
				capibara.IsOld = true;
				return;
			}

			var features = capibara.GetFeatures();
			foreach (var feature in features)
			{
				switch (feature.Item1)
				{
					case "time_h":
						if (int.TryParse(feature.Item2, out int time_h) && capibara.GetAge() > 60 * time_h)
						{
							capibara.IsOld = true;
						}
						break;
					case "time":
						if (int.TryParse(feature.Item2, out int time) && capibara.GetAge() > time)
						{
							capibara.IsOld = true;
						}
						break;
					case "f":
						if (int.TryParse(feature.Item2, out int f_number))
						{
							var finishedFlippers = GetTimer(capibara, "flippers_done");
							if (finishedFlippers != null && int.TryParse(finishedFlippers.Value, out int value) && value >= f_number)
							{
								Debug.Log($"Check flippers_done: {value} < {f_number}");
								capibara.IsOld = true;
							}
						}
						break;
					default:
						break;
				}
			}
		}
		public void CheckCapibarasHealth()
		{
			Debug.Log($"Check Capibaras Health");
			var withChanges = false;

			var sadCapibaras = _zoo.Where(c => c.IsSad).ToList();
			foreach (var capibara in sadCapibaras)
			{
				FreeCapibara(capibara);
				withChanges = true;
			}

			var featuredCapibaras = _zoo.Where(c => c.Features != string.Empty).ToList();
			foreach (var capibara in featuredCapibaras)
			{
				ProcessCapibaraFeatures(capibara);
			}

			var notHappyCapibaras = _zoo.Where(c => !c.IsHappy).ToList();
			foreach (var capibara in notHappyCapibaras)
			{
				MakeCapibaraHappy(capibara);
			}

			var notOldCapibaras = _zoo.Where(c => !c.IsOld);
			foreach (var capibara in notOldCapibaras)
			{
				CheckCapibarasAge(capibara);
			}

			var oldCapibaras = _zoo.Where(c => c.IsOld).ToList();

			foreach (var capibara in oldCapibaras)
			{
				if (capibara.IsHappy)
				{
					MilkCapibara(capibara);
				}
				else
				{
					FreeCapibara(capibara);
				}
				withChanges = true;
			}

			var happyCappibaras = _zoo.Where(c => c.IsHappy).ToList();

			foreach (var capibara in happyCappibaras)
			{
				if ( capibara.Breed == "hidden" || capibara.Breed == "event")
				{
					MilkCapibara(capibara);
				}
			}

			UpdateZoo();
			if (withChanges)
				CheckCapibarasHealth();
		}
		public void GetCapibarasInZoo()
		{
			GetTimers();
			_zoo = GameStarter.Instance.PlayerManager.Data.GetList<MTCapibara>(GlobalValues.Zoo.ToString()) ?? new List<MTCapibara>();
			RetierCapibaras();
		}
		private void GetTimers()
		{
			_timers = GameStarter.Instance.PlayerManager.Data.GetList<GamePlayTimer>(GlobalValues.Timers.ToString()) ?? new List<GamePlayTimer>();
			foreach (var timer in _timers)
			{
				Debug.LogError($"{timer.Id} {timer.Type} {timer.Value} {timer.Time}");
			}
			Debug.LogError(_timers.Count.ToString());
		}
		public void UpdateZoo()
		{
			GameStarter.Instance.PlayerManager.Data.SetList(GlobalValues.Zoo.ToString(), _zoo);
			UpdateTimers();
			GameStarter.Instance.PlayerManager.SAVE();
		}
		private void UpdateTimers()
		{
			GameStarter.Instance.PlayerManager.Data.SetList(GlobalValues.Timers.ToString(), _timers);
		}
		private void UpdateCapibaraTimer(MTCapibara capibara, string action, string value = "")
		{
			if (capibara == null)
			{
				Debug.LogError("Capibara is null. Timer update aborted.");
				return;
			}

			var timer = GetTimer(capibara, action);
			if (timer == null)
			{
				timer = new GamePlayTimer(capibara.Id, "capibara", action);
				_timers.Add(timer);  // Ensure "Timers" and "timers" are consistently named
				Debug.Log($"Created new timer for Capibara {capibara.Id} with action '{action}'");
			}

			timer.Value = value;
			Debug.Log($"Updated timer for Capibara {capibara.Id} with action '{action}'. Value - {(string.IsNullOrEmpty(value) ? value : "None")}");

			timer.UpdateTime();
		}
		public void AddToZoo(MTCapibara capibara)
		{
			Debug.Log($"Capibara {capibara.Id} added to zoo!");
			var newCapibara = capibara.Clone() as MTCapibara;
			_zoo.Add(newCapibara);
			UpdateZoo();
			CheckCapibarasHealth();
		}
		public void FreeCapibara(MTCapibara capibara)
		{
			_zoo.Remove(capibara);
			//RemoveTimers(capibara);
			UpdateCapibaraTimer(capibara, "finished");
			Debug.Log($"Capibara {capibara.Id} removed!");
			if (capibara.Breed == "daily")
				GameStarter.Instance.GamePlayManager.UpdateCollection(MTDataCollection.Finish_daily_task_number, 1);
			if (capibara.IsHappy && capibara.Features.Contains("run"))
			{
				try
				{
					var c = capibara.GetFeatures().FirstOrDefault(f => f.Item1 == "run");
					var nextId = c.Item2;
					var nextCapibara = GetCapibaras("Any").FirstOrDefault(c => c.Id == nextId);
					if (nextCapibara != null)
					{
						AddToZoo(nextCapibara);
					}
					else
					{
						Debug.LogError($"Capibara to run {nextId} not found!");
					}
				}
				catch (Exception ex)
				{
					Debug.LogError($"Capibara {capibara.Id} cant be added!");
				}
			}
			UpdateZoo();
			CheckCapibarasHealth();
		}
		public void MilkCapibara(MTCapibara capibara)
		{
			Debug.Log($"Capibara {capibara.Id} milked!");
			MTRewarder.Run(capibara, () => FreeCapibara(capibara));
		}
	}
}
