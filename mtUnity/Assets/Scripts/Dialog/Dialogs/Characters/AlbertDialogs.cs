﻿using MTCore;
using MTTown;
using System;
using static Assets.Scripts.Dialog.Dialogs.Characters.FritzDialogs;

namespace Assets.Scripts.Dialog.Dialogs.Characters
{
	public class AlbertDialogs : MTDCharacterDialogs
	{
		public AlbertDialogs(int townId) :
			base(townId, GameCharacter.Albert)
		{ }
	}
}
