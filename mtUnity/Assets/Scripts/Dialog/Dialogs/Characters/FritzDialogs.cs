﻿using MTCore;
using MTTown;
using System;

namespace Assets.Scripts.Dialog.Dialogs.Characters
{
	public class FritzDialogs : MTDCharacterDialogs
	{
		public FritzDialogs(int townId) :
			base(townId, GameCharacter.Fritz)
		{ }

		public enum CaracterDialogs
		{
			TaskInfo,
			Depo,
			Tablo
		}
		public override void ClearCompletedDialogs()
		{
			ClearDialog(BasicDialogs.Greeting.ToString());
			ClearDialog(CaracterDialogs.TaskInfo.ToString());
			ClearDialog(CaracterDialogs.Depo.ToString());
			ClearDialog(CaracterDialogs.Tablo.ToString());
		}
		public override void GreetengsDialog(Action beforeClose = null)
		{
			Action _beforeClose = () =>
			{
				beforeClose?.Invoke();
				var t = GameStarter.Instance.TownManager.Town as TownForest;
				var d = t.LumberMill.GrosserFrantz.Dialogs;
				MTStaticHints.SawMillHint(() => d.GreetengsDialog());
			};
			base.GreetengsDialog(_beforeClose);
		}
		public override void ResourceTaskDialog(MTCapibara capibara, Action beforeClose = null)
		{
			Action _beforeClose = () =>
			{
				beforeClose?.Invoke();

				var dialogName = CaracterDialogs.TaskInfo.ToString();

				if (DialogCompleted(dialogName))
					return;

				MTStaticHints.ResourceHint(5 , () =>
				{
					CompleteDialog(dialogName);
					MTStaticHints.TaskListHint();
				});
			};
			base.ResourceTaskDialog(capibara, _beforeClose);
		}
		public void DepoDialog()
		{
			var dialogName = CaracterDialogs.Depo.ToString();

			if (DialogCompleted(dialogName))
				return;

			var dialogKey = GetDialogFullName(dialogName);

			Action _beforeClose = () =>
			{
				CompleteDialog(dialogName);
				MTStaticHints.TabloHint(() => TabloDialog());
			};

			base.ShowDialog(dialogKey, _beforeClose);
		}
		public void TabloDialog(Action beforeClose = null)
		{
			var dialogName = CaracterDialogs.Tablo.ToString();

			if (DialogCompleted(dialogName))
				return;

			var dialogKey = GetDialogFullName(dialogName);

			Action _beforeClose = () =>
			{
				CompleteDialog(dialogName);
				MTStaticHints.StationHint(() => ResourceDialog(1, true));
			};

			base.ShowDialog(dialogKey, _beforeClose);
		}
	}
}
