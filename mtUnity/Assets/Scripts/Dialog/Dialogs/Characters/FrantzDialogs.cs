﻿using Assets.Scripts.Dialog.Dialogs.Characters;
using MTPlayer;
using System;

namespace MTTown
{
    public class FrantzDialogs : MTDCharacterDialogs
	{
		public FrantzDialogs(int townId) :
			base(townId, GameCharacter.Franz)
		{ }
		public override void GreetengsDialog(Action beforeClose = null)
		{
			Action _beforeClose = () =>
			{
				beforeClose?.Invoke();
				var t = GameStarter.Instance.TownManager.Town as TownForest;
				var d = t.AlterFritzPost.AlterFritz.Dialogs as FritzDialogs;
				MTStaticHints.DepoHint(() => d.DepoDialog());
			};
			base.GreetengsDialog(_beforeClose);
		}
	}
}
