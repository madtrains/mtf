﻿namespace MTDialogs
{
	public class MTDStartTrip : MTDialog
	{
		//public override void FirstScreen()
		//{
		//	SetMessage(GameStarter.Instance.TranslationManager.GetTableString(33));
		//	SetButtons(ButtonsType.YesNo);
		//	Yes(() => Go());
		//	No(() => Finish());
		//}

		public override void Init()
		{
			var text = GameStarter.Instance.TranslationManager.GetTableString("Dialog.TrainReadyToDepart");
			SetMessage(text);
		}

		/*
		public void Go()
		{
			if (GameStarter.Instance.UIRoot.Town.OnTrainGo != null)
				GameStarter.Instance.UIRoot.Town.OnTrainGo();
			Finish();
		}
		*/
	}
}