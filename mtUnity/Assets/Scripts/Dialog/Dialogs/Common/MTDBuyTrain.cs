﻿using System.Text;

namespace MTDialogs
{
	/// <summary>
	/// trainId:int req
	/// price: int req
	/// </summary>
	public class MTDBuyTrain : MTDialog
	{
		private int _price;
		private int _trainId;
		public MTDBuyTrain(int trainId, int price)
		{
			_price = price;
			_trainId = trainId;
		}
		public override void Init()
		{
			StringBuilder text = new StringBuilder();
			text.Append(GameStarter.Instance.TranslationManager.GetTableString("Dialog.BuyThisTrain1"));
			text.Append(_price);
			text.Append(_trainId);
			text.Append(" ");
			text.Append(GameStarter.Instance.TranslationManager.GetTableString("Dialog.BuyThisTrain2"));
			SetMessage(text.ToString());
		}

		public override void CompleteDialog()
		{
			Finish();
		}
	}


}