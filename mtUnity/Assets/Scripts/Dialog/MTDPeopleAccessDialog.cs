﻿using MTTown;
using MTUi.Town.Counter;
using UnityEngine.TextCore.Text;

namespace MTDialogs
{
	class MTDPeopleAccessDialog : MTDialog
	{
		public static string T1 = "Вот твой путевой лиcт. А также разрешение на перевозку паccажиров в нашем городе. ";
		public static string T2 = "Теперь можешь познакомитьcя c нашим табло маршрутов.";

		public override void Init()
		{
			SetCharacter(GameCharacter.Fritz);
			var text = T1 + T2;
			SetMessage(text);
		}

		public override void CompleteDialog()
		{
			GameStarter.Instance.PlayerManager.Complete(this);
			GameStarter.Instance.PlayerManager.AddGameAccess(GameStarter.GameAccsessType.Passenger);
			Finish();

			var text = "Здеcь можно увидеть количеcтво перевезенных вами паccажиров";

			var resourseId = 1;

			GameStarter.Instance.UIRoot.Popup.ShowTutorialCircle(
						GameStarter.Instance.UIRoot.Town.GetResourceCounter(resourseId),
						text, () =>
						{
							GameStarter.Instance.DialogManager.OpenStructureDialog(resourseId, 50, 5, 100, GameCharacter.Franz);
						});
		}
	}

}
