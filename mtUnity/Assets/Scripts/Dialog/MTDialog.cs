﻿using MTUi;
using MTUi.Town;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MTDialogs
{
	public class MTDialog
	{
		public delegate void ButtonkDelegate();
		public delegate void ScreenDelegate();
		public System.Action OnFinish;
		internal Dictionary<string, object> _context;
		internal object[] _args;
		public List<DialogShield> _taskShields;
		public List<DialogShield> _rewardShields;

		private string _title;
		private string _message = "Default text";
		private GameCharacter _uiCharacter = GameCharacter.Fritz;
		internal enum ButtonsType
		{
			Ok,
			Yes,
			No,
			YesNo,
			More,
			Custom
		}
		private List<DialogButton> _buttons;
		public static int chunkSize = 150;
		public void ProcessText()
		{
			int chunk = _context.ContainsKey("chunk") ? (int)_context["chunk"] : 0;
			var chunkToDisplay = _message.Substring(chunk);

			if (chunk < _message.Length - chunkSize)
			{
				var dot = _message.Substring(chunk + chunkSize).IndexOf('.') + 1;
				chunkToDisplay = _message.Substring(chunk, chunkSize + dot);

				_context["chunk"] = chunk + chunkSize + dot;

				SetButtons((ButtonsType.More, () => ProcessText()));
			}
			else
			{
				SetButtons((ButtonsType.Ok, () => CompleteDialog()));
			}

			if (chunk == 0)
			{
				GameStarter.Instance.UIRoot.Dialog.Show(chunkToDisplay, _uiCharacter, _buttons.ToArray(), _taskShields?.Select(s => new DialogShieldInfo(s.Value.ToString(), s.Icon)).ToArray(), _rewardShields?.Select(s => new DialogShieldInfo(s.Value.ToString(), s.Icon)).ToArray());
				//SetShields();
			}
			else
			{
				GameStarter.Instance.UIRoot.Dialog.Set(chunkToDisplay, _uiCharacter, _buttons.ToArray());
			}
		}

		public MTDialog()
		{
			_context = new Dictionary<string, object>();
			_buttons = new List<DialogButton>();
			_rewardShields = new List<DialogShield>();
			_taskShields = new List<DialogShield>();
		}

		public MTDialog(string text, GameCharacter character, Action onFinish)
		{
			_context = new Dictionary<string, object>();
			_buttons = new List<DialogButton>();
			_rewardShields = new List<DialogShield>();
			_taskShields = new List<DialogShield>();
			_uiCharacter = character;
			_message = text;
			OnFinish = onFinish;
		}
		public MTDialog(params object[] args)
		{
			_args = args;
		}
		public virtual void Init()
		{
		}
		public void SetCharacter(GameCharacter character)
		{
			_uiCharacter = character;
		}
		internal void SetTitle(string title)
		{
			_title = title;
		}
		internal void SetMessage(string message)
		{
			_message = message;
		}
		internal void SetButtons(params ValueTuple<ButtonsType, CLickDelegate>[] buttons)
		{
			_buttons.Clear();
			foreach (var button in buttons)
			{
				switch (button.Item1)
				{
					case ButtonsType.Ok:
						_buttons.Add(new MTUi.Town.DialogButton("Ok", button.Item2));
						break;
					case ButtonsType.Yes:
						_buttons.Add(new MTUi.Town.DialogButton("Yes", button.Item2));
						break;
					case ButtonsType.No:
						_buttons.Add(new MTUi.Town.DialogButton("No", button.Item2));

						break;
					case ButtonsType.More:
						_buttons.Add(new MTUi.Town.DialogButton("...", button.Item2));
						break;
					case ButtonsType.Custom:
						var text = _context["customButtonText"].ToString();
						_buttons.Add(new MTUi.Town.DialogButton(text, button.Item2));
						break;
					default:
						break;
				}
			}
		}
		public void Start()
		{
			Init();
			ProcessText();
		}
		public void Finish()
		{
			if (OnFinish != null)
				OnFinish();
            GameStarter.Instance.UIRoot.Dialog.Hide();
        }
		public virtual void CompleteDialog()
		{
			Finish();
		}
		/*
		private void SetShields()
		{
			//пока предполагаем что шилда всегда 2;
			if (_shields.Count == 2)
			{
				var taskShield = _shields[0];
				var rewardShield = _shields[1];
				GameStarter.Instance.UIRoot.Dialog.SetShields(
					taskShield.Label, taskShield.Space, taskShield.Value, taskShield.Icon,
					rewardShield.Label, rewardShield.Space, rewardShield.Value, rewardShield.Icon);
			}
		}
		*/

		public MTDialog WithTaskShields(params DialogShield[] shields)
		{
			_taskShields.AddRange(shields);
			return this;
		}
		public MTDialog WithRewardShields(params DialogShield[] shields)
		{
			_rewardShields.AddRange(shields);
			return this;
		}
		public class DialogShield
		{
			public string Label;
			public int Value;
			public int Icon = 0;
			public int Space = 2;
		}
	}
}