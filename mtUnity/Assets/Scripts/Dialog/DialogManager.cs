﻿using Assets.Scripts.Dialog.Dialogs.Characters;
using MTDialogs;
using MTPlayer;
using System;
using System.Collections.Generic;
using System.Transactions;

namespace MTTown
{
	public partial class DialogManager
	{
		public Queue<MTDialog> DialogsQueue;

		private DialogManager()
		{
			DialogsQueue = new Queue<MTDialog>();
		}

		public static DialogManager Init()
		{
			return new DialogManager();
		}

		public T Show<T>(params object[] args) where T : MTDialog
		{
			T dialog = (T)Activator.CreateInstance(typeof(T), args);
			Show(dialog);
			return dialog;
		}
		public void Show(MTDialog dialog)
		{
			dialog.OnFinish += () => FinishDialog();
			DialogsQueue.Enqueue(dialog);
			if (DialogsQueue.Count == 1)
				Next();
		}

		private void FinishDialog()
		{
			if (DialogsQueue.Count > 0)
			{
				var dialog = DialogsQueue.Dequeue();
				dialog.OnFinish -= () => FinishDialog();
			}
			Next();
		}

		public void Next()
		{
			if (DialogsQueue.TryPeek(out var dialog))
			{
				dialog.Start();
			}
		}
		public bool OpenStructureDialog(int resource, int amount = 10, int multilier = 4, int prize = 0, GameCharacter character = GameCharacter.Fritz)
		{
			var result = false;

			var town = GameStarter.Instance.TownManager.TownParameters;

			var resourceName = GameStarter.Instance.TranslationManager.GetTableString(GameStarter.GameParameters.Resources[resource].TranslationKey);
			var currentTask = GameStarter.Instance.PlayerManager.GetUserIngameTask(town.ID, resource);
			var text = $"Message text";

			if (currentTask == null)
			{
				var newTask = TripsDispatcher.GetTaskFor(GameStarter.Instance.TownManager.TownParameters.ID, resource, amount, multilier, prize, character);

				text = $"For every {newTask.ResourseAmount} {resourceName} transported, I will pay you {newTask.PrizeAmount} gold.";
				text = $"Задание на перевозку {resourceName} получено";
				MTUi.Town.DialogButton buttonYes =
					new MTUi.Town.DialogButton("Ja!", () =>
					{
						GameStarter.Instance.PlayerManager.AddUserIngameTask(newTask);
						GameStarter.Instance.UIRoot.Dialog.Hide();
                    });
				GameStarter.Instance.UIRoot.Dialog.Show(text, character, buttonYes);
			}
			else if (GameStarter.Instance.PlayerManager.CheckUserIngameTask(currentTask))
			{
				text = "Отличная работа! Мне кажется ты уже можешь получить награду.";

				MTUi.Town.DialogButton buttonOk =
					new MTUi.Town.DialogButton("OK!", () =>
					{
                        GameStarter.Instance.UIRoot.Dialog.Hide();
                    });
				GameStarter.Instance.UIRoot.Dialog.Show(text, character, buttonOk);
			}
			else
			{
				text = "Work, work...";
				var tasksButton = GameStarter.Instance.UIRoot.Town.TasksPanel.ButtonTasks;

				GameStarter.Instance.UIRoot.Popup.ShowTutorialCircle(
							tasksButton.RectTransform,
							text, () => 
							{ 
								tasksButton.ButtonComponent.onClick.Invoke();
							} );
				/*
				MTUi.Town.DialogButton buttonOk =
					new MTUi.Town.DialogButton("OK!", () =>
					{
						GameStarter.Instance.UIRoot.TutorialCircle.Show(
			GameStarter.Instance.UIRoot.Town.SwitchPanel.GetButton(GameStarter.Instance.UIRoot.Town.SwitchPanel.tasksButton));

						GameStarter.Instance.UIRoot.Dialog.Hide();
					});
				GameStarter.Instance.UIRoot.Dialog.Show(text, character, buttonOk);
				*/
			}
			return result;
		}
	}
}