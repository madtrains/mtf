﻿namespace MTDialogs
{
	class MTDSawMillFirstDialog : MTDialog
	{
		public override void Init()
		{
			var text = GameStarter.Instance.TranslationManager.GetTableString("Dialog.Town1.Franz.Welcome");
			SetMessage(text);
		}

		public override void CompleteDialog()
		{
			GameStarter.Instance.PlayerManager.Complete(this);
			Finish();
		}
	}
}
