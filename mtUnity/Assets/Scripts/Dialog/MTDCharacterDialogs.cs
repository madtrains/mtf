﻿using MTCore;
using MTDialogs;
using MTPlayer;
using System;
using System.Collections.Generic;
using static MTDialogs.MTDialog;
using static UnityEngine.GraphicsBuffer;

namespace MTTown
{
	public class MTDCharacterDialogs
	{
		private PlayerManager PlayerManager => GameStarter.Instance.PlayerManager;
		private TranslationManager TranslationManager => GameStarter.Instance.TranslationManager;
		private MarketManager MarketManager => GameStarter.Instance.MarketManager;
		private DialogManager DialogManager => GameStarter.Instance.DialogManager;

		private bool _clearDialogsOnStart = false;

		private List<string> _comletedDialogs = new List<string>();
		internal string DialogPrefix => $"Dialog-Town{_townId}-{_character.ToString()}";

		private int _townId;
		private GameCharacter _character;

		public enum BasicDialogs
		{
			Greeting
		}
		public MTDCharacterDialogs(int townId, GameCharacter character)
		{
			_townId = townId;
			_character = character;

			if (!_clearDialogsOnStart)
				_comletedDialogs = PlayerManager.GetCompletedDialogs(DialogPrefix);
		}
		public virtual void GreetengsDialog(Action beforeClose = null)
		{
			var dialogName = BasicDialogs.Greeting.ToString();

			var dialogKey = GetDialogFullName(dialogName);

			if (DialogCompleted(dialogName))
				return;

			Action beforeCloseFull = () =>
			{
				CompleteDialog(dialogName);
				beforeClose?.Invoke();
			};

			ShowDialog(dialogKey, beforeCloseFull);
		}
		public void ResourceDialog(int resource, bool repeated = false, Action beforeClose = null)
		{
			var hasTasks = GameStarter.Instance.GamePlayManager.HasWaitingCapibaras(_character);

			if (hasTasks)
			{
				var task = GameStarter.Instance.GamePlayManager.GetCapibara(_character);

				if (task != null)
				{
					ResourceTaskDialog(task, beforeClose);
				}
				else
				{
					NoResourceTaskDialog(0, beforeClose);
				}
			}

			//var gameTask = PlayerManager.GetUserIngameTask(_townId, resource);
			//if (gameTask != null)
			//{
			//	//бла бла задание в процеccе
			//	if (PlayerManager.CheckUserIngameTask(gameTask))
			//	{
			//		TaskReady(resource, beforeClose);
			//	}
			//	else
			//	{
			//		TaskInProcess(resource, beforeClose);
			//	}
			//}
			//else if (repeated)
			//{
			//	ResourceTaskDialog(resource, beforeClose);
			//}
			//else
			//{
			//	NoResourceTaskDialog(resource, beforeClose);
			//}
		}
		public virtual void ResourceTaskDialog(MTCapibara task, Action beforeClose = null)
		{

			var dialogKey = $"Capibara.{task.Id}.Description";
			var dialogParams = GetParams(task);

			var targets = task.GetTargets();
			var rewards = task.GetRewards();

			var taskShields = new List<DialogShield>();

			var addLable = false;
			foreach (var target in targets)
			{
				var taskShield = new DialogShield()
				{
					Label = addLable ? "Task:" : "",
					Value = target.Item2,
					Icon = target.Item1
				};

				taskShields.Add(taskShield);
				addLable = false;
			}

			var rewardShields = new List<DialogShield>();
			addLable = false;
			foreach (var reward in rewards)
			{
				var rewardShield = new DialogShield()
				{
					Label = addLable ? "Reward:" : "",
					Value = reward.Item2,
					Icon = reward.Item1
				};
				rewardShields.Add(rewardShield);
				addLable = false;
			}


			ShowDialogWithShields(dialogKey, (taskShields, rewardShields), () =>
			{
				GameStarter.Instance.GamePlayManager.CapibaraReadyToWork(task);
				beforeClose?.Invoke();
			}, dialogParams);
		}

		public virtual void TaskInProcess(int resource, Action beforeClose = null)
		{
			var dialogKey = GetDialogFullName($"Resource-{resource}-Task-InProgress");

			var cogInfo = PlayerManager.GetCogInfo(1);

			if (cogInfo != null)
			{
				PlayerManager.UpdateCogInfo(cogInfo.Id, 1, () => { UnityEngine.Debug.LogError("DONE!!!!"); });
			}
			else
			{
				PlayerManager.SetCogInfo(new CogInfo { Id = 1, TargetAmount = 10 });
			}

			ShowDialog(dialogKey, beforeClose);
		}
		public virtual void TaskReady(int resource, Action beforeClose = null)
		{
			var dialogKey = GetDialogFullName($"Resource-{resource}-Task-Ready");

			Action beforeCloseFull = () =>
			{
				GameStarter.Instance.UIRoot.Tasks.Show(MTUi.TasksPopup.Mode.Tasks);
				beforeClose?.Invoke();
			};

			ShowDialog(dialogKey, beforeCloseFull);
		}
		public virtual void NoResourceTaskDialog(int resource, Action beforeClose = null)
		{
			var dialogKey = GetDialogFullName($"Resource-{resource}-Task-No");

			ShowDialog(dialogKey, beforeClose);
		}
		public virtual void NoTasksDialog(Action beforeClose = null)
		{
			var dialogKey = GetDialogFullName("NoTask");

			ShowDialog(dialogKey, beforeClose);
		}
		private (string, string)[] GetParams(MarketInfo resourceInfo)
		{
			var resourceName = TranslationManager.GetTableString($"Resource-{resourceInfo.Id}");
			var resourceParam = ("resourceName", resourceName);
			var amountParam = ("resourceAmount", resourceInfo.MinAmount.ToString());
			var rewardParam = ("resourceReward", resourceInfo.GetPrice().ToString());
			return new (string, string)[] { resourceParam, amountParam, rewardParam };
		}

		private (string, string)[] GetParams(MTCapibara task)
		{
			var targets = task.GetTargets();
			var rewards = task.GetRewards();

			var resourceName = TranslationManager.GetTableString($"Resource-{targets[0].Item1}");
			var resourceParam = ("resourceName", resourceName);
			var amountParam = ("resourceAmount", targets[0].Item2.ToString());
			var rewardParam = ("resourceReward", rewards[0].Item1.ToString());
			return new (string, string)[] { resourceParam, amountParam, rewardParam };
		}
		internal void ShowDialog(string dialogKey, Action beforeClose = null, params (string, string)[] dialogParams)
		{
			ShowDialogWithShields(dialogKey, (null,null), beforeClose, dialogParams);
		}
		internal void ShowDialogWithShields(string dialogKey, (List<DialogShield>, List<DialogShield>) shields, Action beforeClose = null, params (string, string)[] dialogParams)
		{
			var dialogText = dialogParams != null ? TranslationManager.GetTableString(dialogKey.Replace("-", "."), dialogParams) : TranslationManager.GetTableString(dialogKey);

			var dialog = new MTDialog(dialogText, _character, beforeClose);
			if (shields.Item1 != null)
				dialog.WithTaskShields(shields.Item1.ToArray());
			if (shields.Item2 != null)
				dialog.WithRewardShields(shields.Item2.ToArray());

			DialogManager.Show(dialog);
		}
		public string GetDialogFullName(string dialogName)
		{
			return $"{DialogPrefix}-{dialogName}";
		}
		public bool DialogCompleted(string dialogName)
		{
			return _comletedDialogs.Contains(dialogName);
		}
		public void CompleteDialog(string dialogName)
		{
			_comletedDialogs.Add(dialogName);
			PlayerManager.UpdateCompletedDialogs(DialogPrefix, _comletedDialogs);
		}
		public void ClearDialog(string dialogName)
		{
			if (_comletedDialogs.Contains(dialogName))
			{
				_comletedDialogs.Remove(dialogName);
				PlayerManager.UpdateCompletedDialogs(DialogPrefix, _comletedDialogs);
			}
		}
		public virtual void ClearCompletedDialogs() { }
	}
}
