﻿using MTTown;
using MTUi.Town.Counter;
using UnityEngine.TextCore.Text;

namespace MTDialogs
{
	class MTDMailAccessDialog : MTDialog
	{
		public static string T1 = "А еще нужно забирать почту.";
		public static string T2 = "Это очень важно";

		public override void Init()
		{
			SetCharacter(GameCharacter.Fritz);
			var text = T1 + T2;
			SetMessage(text);
		}

		public override void CompleteDialog()
		{
			GameStarter.Instance.PlayerManager.Complete(this);
			GameStarter.Instance.PlayerManager.AddGameAccess(GameStarter.GameAccsessType.Mail);
			Finish();

			var text = "Здеcь можно увидеть количеcтво доcтавленной вами почты";

			var resourseId = 2;

			GameStarter.Instance.UIRoot.Popup.ShowTutorialCircle(
						GameStarter.Instance.UIRoot.Town.GetResourceCounter(resourseId),
						text, () =>
						{
							GameStarter.Instance.DialogManager.OpenStructureDialog(resourseId, 50, 5, 100, GameCharacter.Franz);
						});
		}
	}
}
