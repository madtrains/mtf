﻿namespace MTDialogs
{
	class MTDFirstDialog : MTDialog
	{
		public override void Init()
		{
			//var text = GameStarter.Instance.TranslationManager.GetTableString("Dialog.Town1.Fritz.Welcome");
			var resourceName = GameStarter.Instance.TranslationManager.GetTableString("Resource.Wood");
			var text = GameStarter.Instance.TranslationManager.GetTableString("Dialog.Town1.test", ("recourceName", resourceName), ("resourceAmount", "100"));
			SetMessage(text);
		}
		public override void CompleteDialog()
		{
			GameStarter.Instance.PlayerManager.Complete(this);
			Finish();
		}
	}

}
