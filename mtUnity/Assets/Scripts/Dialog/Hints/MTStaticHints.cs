﻿using System;
using UnityEngine;

namespace MTTown
{
    public class MTStaticHints
	{
		internal static void ShowHint(string hintKey, RectTransform transform, Action beforeClose = null, params (string, string)[] hintParams)
		{
			var TranslationManager = GameStarter.Instance.TranslationManager;
			if (TranslationManager == null) { return; }

			var hintText = hintParams != null ? TranslationManager.GetTableString(hintKey, hintParams) : TranslationManager.GetTableString(hintKey);

			GameStarter.Instance.UIRoot.Popup.ShowTutorialCircle(
			transform,
			hintText, () => { beforeClose?.Invoke(); });
		}
		/*
		Dialog.Town0.Fritz.Look.Sawmill
		Dialog.Town0.Fritz.Look.Depo
		Dialog.Town0.Fritz.Look.Station
		Dialog.Town0.Fritz.Look.Tablo
		Dialog.Town0.Fritz.Look.TaskList
		Dialog.Town0.Fritz.Look.Resource
		*/
		public static void DepoHint(Action beforeClose = null)
		{
			var hintKey = "Dialog.Town0.Fritz.Look.Depo";

			var target =
				GameStarter.Instance.UIRoot.Town.TownPointsSwitcher.GetSwitchButton(PointsOfInterest.Tag.Coupler);

			ShowHint(hintKey, target.RectTransform, () =>
			{
				target.Button.onClick.Invoke();
				beforeClose?.Invoke();
			});
		}
		public static void SawMillHint(Action beforeClose = null)
		{
			var hintKey = "Dialog.Town0.Fritz.Look.Sawmill";
			var target =
			GameStarter.Instance.UIRoot.Town.TownPointsSwitcher.GetSwitchButton(PointsOfInterest.Tag.LumberMill);

			ShowHint(hintKey, target.RectTransform, () =>
			{
				target.Button.onClick.Invoke();
				beforeClose?.Invoke();
			});
		}
		public static void TabloHint( Action beforeClose = null)
		{
			var hintKey = "Dialog.Town0.Fritz.Look.Tablo";
			var target =
			GameStarter.Instance.UIRoot.Town.TownPointsSwitcher.GetSwitchButton(PointsOfInterest.Tag.SFDisplay);

			ShowHint(hintKey, target.RectTransform, () => {
				target.Button.onClick.Invoke();
				beforeClose?.Invoke();
			});
		}
		public static void StationHint(Action beforeClose = null)
		{
			var hintKey = "Dialog.Town0.Fritz.Look.Station";

			var target =
			GameStarter.Instance.UIRoot.Town.TownPointsSwitcher.GetSwitchButton(PointsOfInterest.Tag.AlterFritz);

			ShowHint(hintKey, target.RectTransform, () =>
			{
				target.Button.onClick.Invoke();
				beforeClose?.Invoke();
			});
		}
		public static void ResourceHint(int resource, Action beforeClose = null)
		{
			var hintKey = "Dialog.Town0.Fritz.Look.Resource";

			Action _beforeClose = () =>
			{
				beforeClose?.Invoke();
			};

			ShowHint(hintKey, GameStarter.Instance.UIRoot.Town.GetResourceCounter(resource), _beforeClose);
		}
		public static void TaskListHint()
		{
			var hintKey = "Dialog.Town0.Fritz.Look.TaskList";
			var target =
				GameStarter.Instance.UIRoot.Town.TasksPanel.ButtonTasks;
			ShowHint(hintKey, target.RectTransform, () => target.ButtonComponent.onClick.Invoke());
		}
	}
}
