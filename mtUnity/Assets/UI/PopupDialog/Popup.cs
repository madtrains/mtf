﻿using UnityEngine;
using UnityEngine.UI;

namespace MTUi
{
    public class Popup : UIElement
    {
        public enum Placing { LeftDown, RightDown }

        public UnityEngine.Events.UnityAction<bool> OnCloseResult;

        [System.ObsoleteAttribute("Use methods with Parameters instead. Can not show dialog without text and delegates")]
        public new void Show()
        {
            throw new System.NotSupportedException("Dialog was shown without any parameters");
        }

        [SerializeField] private UberButton yes;
        [SerializeField] private UberButton no;
        [SerializeField] private UberButton ok;
        [SerializeField] private UITextElement textElement;
        [SerializeField] private Text txt;
        [SerializeField] private GameObject sh;
        [SerializeField] private TutorialCircle tutorialCircle;

        [SerializeField] private RectTransform circle;
        [SerializeField] private RectTransform frame;
        [SerializeField] private ContentHolders placings;
        
        private CLickDelegate yesDelegate;
        private CLickDelegate noDelegate;
        private CLickDelegate okDelegate;

        public override void Init()
        {
            base.Init();
            ResetAll();
            yes.Init(() => { Hide(); yesDelegate(); GiveCloseResult(true); });
            no.Init(() => { Hide(); noDelegate(); GiveCloseResult(false); });
            ok.Init(() => { Hide(); okDelegate(); GiveCloseResult(true); });
            tutorialCircle.Init();
            tutorialCircle.OnCirclePressed = () => { Hide(); okDelegate(); GiveCloseResult(true); };
        }

        public void Show(string text, CLickDelegate ok)
        {
            ShowOneButtonPopup(text);
            okDelegate = ok;
        }

        public void Show(string text, CLickDelegate yes, CLickDelegate no)
        {
            ShowTwoButtonsPopup(text);
            yesDelegate = yes;
            noDelegate = no;
        }

        public void ShowTutorialCircle(RectTransform target, string text, CLickDelegate ok)
        {
            ShowNoButtonsTutorial(text, target);
            okDelegate = ok;
        }

        public void ShowTutorialCircle(RectTransform target, string text, CLickDelegate ok, Placing placing)
        {
            ShowTutorialCircle(target, text, ok);
            Place(placing);
        }

        public void ShowTutorialCircleOkButton(RectTransform target, string text, float transparency, CLickDelegate ok, Placing placing)
        {
            ResetAll();
            txt.text = text;
            Activate(yes.gameObject, false);
            Activate(no.gameObject, false);
            Activate(this.ok.gameObject, true);
            okDelegate += () => { tutorialCircle.ResetTransparency(); ok?.Invoke(); };
            Activate(sh, false);
            tutorialCircle.Show(target);
            tutorialCircle.Transparency = transparency;
            Place(placing);
            base.Show();
        }

        private void ShowTwoButtonsPopup(string translationKey)
        {
            ResetAll();
            txt.text = translationKey;
            Activate(yes.gameObject, true);
            Activate(no.gameObject, true);
            Activate(ok.gameObject, false);
            Activate(sh, true);
            tutorialCircle.Hide();
            base.Show();
        }

        private void ShowOneButtonPopup(string text)
        {
            ResetAll();
            txt.text = text;
            Activate(yes.gameObject, false);
            Activate(no.gameObject, false);
            Activate(ok.gameObject, true);
            Activate(sh, true);
            tutorialCircle.Hide();
            base.Show();
        }

        private void ShowNoButtonsTutorial(string text, RectTransform target)
        {
            ResetAll();
            txt.text = text;
            Activate(yes.gameObject, false);
            Activate(no.gameObject, false);
            Activate(ok.gameObject, false);
            Activate(sh, false);
            tutorialCircle.Show(target);
            base.Show();
        }

        private void ResetAll()
        {
            yesDelegate = () => { };
            noDelegate = () => { };
            okDelegate = () => { };
            Place(Placing.LeftDown);
        }

        private void GiveCloseResult(bool result)
        {
            if (OnCloseResult != null)
                OnCloseResult(result);
        }

        private void Place(Placing placing)
        {
            switch (placing)
            {
                case Placing.LeftDown:
                {
                    circle.anchorMin = Vector2.zero;
                    circle.anchorMax = Vector2.zero;
                    circle.pivot = Vector2.zero;
                    circle.anchoredPosition = Vector2.zero;
                    frame.anchorMin = Vector2.zero;
                    frame.anchorMax = Vector2.zero;
                    frame.pivot = Vector2.zero;
                    frame.anchoredPosition = Vector2.zero;
                    break;
                }
                case Placing.RightDown:
                {
                    circle.anchorMin = Vector2.right;
                    circle.anchorMax = Vector2.right;
                    circle.pivot = Vector2.right;
                    circle.anchoredPosition = Vector2.zero;
                    frame.anchorMin = Vector2.right;
                    frame.anchorMax = Vector2.right;
                    frame.pivot = Vector2.right;
                    frame.anchoredPosition = Vector2.zero;
                    break;
                }
            }
            int index = (int)placing;
            placings.Activate(index);
        }
    }
}