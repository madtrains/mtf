﻿using UnityEngine;
using UnityEngine.UI;

namespace MTUi
{
	public class SettingsPanel : AnimUIElement
	{
        [SerializeField] private UberButtonLanguage[] languageButtons;
        [SerializeField] private UberButton close;
        [SerializeField] private Image bkg;
        [SerializeField] private GameObject firstSliderIcon;
        [SerializeField] private GameObject secondSliderIcon;

        //public UberButtonToggleBool fx;
        //public UberButtonToggleBool music;

        public Color BKG { set { bkg.color = value; } }

        public override void Init()
        {
            base.Init();
            for (int i = 0; i < languageButtons.Length; i++)
            {
                UberButtonLanguage lb = languageButtons[i];
                lb.Index = i;
                lb.Init(() =>
                {
                    Root.OnChangeLanguage.Invoke(lb.Index);
                });
            }
            close.Init(() => { animator.SetBool(Hash_ON, false); });
            UberBehaviour.Activate(firstSliderIcon, true);
            UberBehaviour.Activate(secondSliderIcon, true);
        }

        protected override void ShowHide(bool state)
        {
            if(state) //show
            {
                Activate(this.gameObject, true);
                animator.SetBool(Hash_ON, true);
            }
            else //hide
            {
                animator.SetBool(Hash_ON, false);
            }
        }

        protected override bool GetVisibility()
        {
            return animator.GetBool(Hash_ON);
        }


        public void MusicVolume(float value)
        {
            GameStarter.Instance.MainComponents.SoundManager.ChangeMusicVolume(value);
        }

        public void SFXVolume(float value)
        {
            GameStarter.Instance.MainComponents.SoundManager.ChangeSFXVolume(value);
        }
    }
}