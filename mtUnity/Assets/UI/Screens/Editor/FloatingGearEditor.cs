﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace MTUi
{
    [CustomEditor(typeof(FloatingGear))]
    public class GearnEditor : UberEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            FloatingGear gear = target as FloatingGear;
            if (GUILayout.Button("Fall"))
            {
                gear.SetAnimate(true);
                FloatingSprite.DebugDraw(gear.DebugLineRenderer, gear.SplineDown);
            }

            if (GUILayout.Button("Raise"))
            {
                gear.SetAnimate(false);
                FloatingSprite.DebugDraw(gear.DebugLineRenderer, gear.SplineUp);
            }

            if (GUILayout.Button("CreateSpline"))
            {
                gear.CreateSplines();
                
            }
        }
    }
}