﻿using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.Splash
{
    public class SplashPopup : UIElement
	{  
		[SerializeField] private Slider slider;
        [SerializeField] private Image bkg;
        [SerializeField] private UberButton settingsButton;

        [SerializeField] private SettingsPanel settingsPanel;

        [SerializeField] private ContentHolders loaded;
        [SerializeField] private Sprite loading;
        [SerializeField] private Text bundlesText;
        [SerializeField] private Text versionText;
        [SerializeField] private Animator animator;
        private bool reLaunch;
        private int pressCount;

        public void SecretPress()
        {
            pressCount++;
            if (pressCount == 3)
            {
                GameStarter.Instance.DebugManager.DebugSettings.Activate();
                versionText.text = string.Format("DEBUG ACTIVATED: {0}", versionText.text);
            }
        }

        public void SetBundlesLoad(int loaded, int count)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(loaded);
            sb.Append(" / ");
            sb.Append(count);
            bundlesText.text = sb.ToString();
        }

        public void SetBundles()
        {
            this.loaded.Activate(2);
        }

        public void SetLoaded(bool loaded)
        {
            this.loaded.Activate(loaded);
            this.slider.value = 0f;
        }

        public override void Init()
        {
            base.Init();
            
            settingsButton.Init(
            () =>
            {
                settingsPanel.Show();
            });
            animator.SetBool(AnimUIElement.Hash_ON, true);
        }

        public void ScaleBkg()
		{
            float defaultRatio = 1920f / 1080f;
            float diff = GameStarter.Instance.UIRoot.Ratio / defaultRatio;
            /*
			GameStarter.Instance.DebugInfo.Set(string.Format("Width: {0} Height: {1} Ratio: {2}, diff {3}" ,
                Root.Width, Root.Height, Root.Ratio, diff), Color.yellow);
            */
			if (diff > 1.0f)
			{
                float scale = diff + 0.05f;
                bkg.rectTransform.localScale = Vector3.one * scale;
            }
        }

        protected override void ShowHide(bool state)
        {
            base.ShowHide(state);
            //Root.ClickTapDisabled = state;
            if (state) 
            {
                Root.Flipper.Block();
                SetLoaded(false);
                this.slider.value = 0f;
            }
            else
            {
                Root.Flipper.UnBlock();
            }
        }

        public void SliderPress()
        {
            Debug.LogWarningFormat("Slider Grabbed!");
        }

        public void SliderRelease()
        {
            Debug.LogWarningFormat("Slider Released!");
        }

		//эвент на движение слайдера
		public void ChangeValue()
		{
            //sliderInertia = slider.value;
			if (slider.value >= 1f)
			{
                Hide();
                //первый запуск
                if (!reLaunch)
                {
                    reLaunch = true;
                    bkg.sprite = loading;
                }
            }
		}
	}
}