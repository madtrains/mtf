﻿using UnityEngine;
using UnityEngine.UI;

namespace MTUi.Splash
{
    public class TextLine : UIElement
    {
		[SerializeField] private Text text;
		public string Text
		{
			set
			{
				text.text = value;
			}
		}
	}
}