﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTUi.Splash
{
	public class FlipableEl : UIElement
	{
		public bool Flipped
		{
			get { return _flipped; }
			set 
			{ 
				_flipped = value;
				Activate(@default, !value);
                Activate(flipped, value);
            }
		}

		private bool _flipped;

		[SerializeField] private GameObject @default;
        [SerializeField] private GameObject flipped;

	}
}