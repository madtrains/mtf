﻿using UnityEngine;

namespace MTUi.Splash
{
    public class MiniGame : UIElement
	{
        [SerializeField] private MiniGameEl refEl;
        [SerializeField] private MinMax spawnDelay;
        [SerializeField] private float speed;
        [SerializeField] private FlipableEl[] train;

        private float time;
        private float nextSpawn;

        private bool Fliped
        {
            get
            {
                return train[0].Flipped;
            }
            set
            {
                foreach (FlipableEl fl in train)
                {
                    fl.Flipped = value;
                }
            }
        }

        protected override void Start()
        {
            base.Start();
            nextSpawn = spawnDelay.Random;
        }

        private void Update()
        {
            time += Time.deltaTime;
            if (time > nextSpawn)
            {
                nextSpawn = spawnDelay.Random;
                time = 0;
                Spawn();
            }
        }

        private void Spawn()
        {
            MiniGameEl newEL = CloneUIElementByRef<MiniGameEl>(refEl, refEl.transform.parent, "el", true);
            newEL.Flipped = UberBehaviour.RandomBool;
            newEL.speed = speed;
            newEL.OnHit = OnHit;
        }

        private void OnHit(MiniGameEl mgEl, bool flipped)
        {
            this.Fliped = !flipped;
        }
    }
}