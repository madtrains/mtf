﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.Splash
{
	public class MiniGameEl : UIElement
	{
		[SerializeField] private Slider slider;
        [SerializeField] private FlipableEl flipableEl;
		[SerializeField] private float hitValue;
		[HideInInspector] public float speed;

		public UnityEngine.Events.UnityAction<MiniGameEl, bool> OnHit;


		public bool Flipped
		{
			get { return flipableEl.Flipped; }
			set
			{ 
                flipableEl.Flipped = value;
            }
		}


        public float Value
		{
			get { return slider.value; }
			set 
			{
				if (value <= hitValue)
				{
					Hit();
				}
				if (value <= 0)
				{
					value = 0;
                    SelfDestroy();
                }
					
				slider.value = value;
			}
		}


        private void Update()
        {
            Value -= Time.deltaTime * speed;
        }


		private void Hit()
		{
			OnHit?.Invoke(this, Flipped);
		}
    }
}