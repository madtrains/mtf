using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.Ads
{
    public class Ads : UIElement
    {
        public UnityEngine.Events.UnityAction OnAdsScreenClick;

        public override void Init()
        {
            base.Init();
            screenButton.onClick.AddListener(ButtonCommand);
        }

        private void ButtonCommand()
        {
            Hide();
            OnAdsScreenClick?.Invoke();
        }

        [SerializeField] private Button screenButton;
    }
}

