﻿using UnityEngine;
using UnityEngine.UI;
namespace MTUi.Town.Counter
{
    public class Counter : UIElement
    {
        public void SetResource(int resourceIndex)
        {
            resources.Activate(resourceIndex);
        }


        public int Value
        {
            set
            {
                counter.text = value.ToString();
            }
        }

        [SerializeField] private ContentHolders resources;
        [SerializeField] private Text counter;

    }
}