﻿using MTTown.PointsOfInterest;
using MTUi.Town.TrainMaintenance;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace MTUi.Town
{
    public class Town : AnimUIElement
	{
        public static readonly float NO_SWIPE = 200;
        public static readonly float HOLD_DURATION = 0.7f;

        public UnityAction OnPlayButton;
		public UnityAction OnTrainGo;
        public UnityAction OnTownNameShown;
		public UnityAction<SwipeDirection> OnSwipe;
        public UnityAction<float> OnTapHold;
        public UnityAction<bool> OnAuxActive;

		public static int hPointer = Animator.StringToHash("pointer");
        public static int hMerch = Animator.StringToHash("merch");
        public static int hTitle = Animator.StringToHash("title");

        public TrainDisplay.TrainDisplay TrainDisplay { get { return trainDisplay; } }
		public TasksPanel.TasksPanel TasksPanel { get { return tasksPanel; } }
		public ScorePanel.ScorePanel Scores { get { return scorePanel; } }
		public Bubble Bubble { get { return bubble; } }

        public TrainMaintenance.TrainMaintenance TrainMaintenance { get { return trainMaintenance; } }
        public TownPointsSwitcher.TownPointsSwitcher TownPointsSwitcher { get { return switcher; } }
		public SpriteStream GearStream { get { return gearStream; } }

		[SerializeField] private TrainMaintenance.TrainMaintenance trainMaintenance;
		[SerializeField] private TownPointsSwitcher.TownPointsSwitcher switcher;
        [SerializeField] private TasksPanel.TasksPanel tasksPanel;
		[SerializeField] private TrainDisplay.TrainDisplay trainDisplay;

        [SerializeField] private UITextElement townName;

        [SerializeField] private PlayWhistleButton playButton;
		[SerializeField] ScorePanel.ScorePanel scorePanel;
		[SerializeField] private RectTransform[] tapIgnore;
		[SerializeField] private ContentHolder forDialogHide;
		[SerializeField] private RectTransform rippleRef;
		[SerializeField] private RectTransform pointerRef;

		[SerializeField] private Bubble bubble;
		[SerializeField] Text ticketsCounter;
		[SerializeField] private GoBar goBar;
		[SerializeField] private Text ticketsText;
		[SerializeField] private SpriteStream gearStream;
		[SerializeField] private RectTransform[] gearStreamPoints;

        public int Tickets { set { ticketsText.text = value.ToString(); } }

		public override void Reset()
		{
			base.Reset();
			trainMaintenance.Reset();
			trainMaintenance.Hide();
		}

		#region Main & Overrides
		public override void Init()
		{
            gearStream.Init(new MTSpline.SplineCubic(gearStreamPoints));
            Root.OnChangeTickets += (value) => { Tickets = value; };
			playButton.Init(() => { goBar.Run(); });
			goBar.OnClick = () =>
			{
				//Debug.Log("Go GO GO!");
				OnPlayButton?.Invoke();
			};

			goBar.OnChangeVisibility = (isVisible) =>
			{
				GameStarter.Instance.SplitFlapDisplay.ClickBLockManager.GoCounterPopupOpened = isVisible;
            };

			switcher.Init();
			/*
			Root.Tasks.OnCommandHide += () =>
			{
				animator.SetBool(hMain, true);
				animator.SetBool(hMaintenance, maintenanceState);
            };
			*/
			bubble.Hide();
            base.Init();
        }

        public void Show(string townName)
        {
            OnCommandShow?.Invoke();
            Activate(this.gameObject, true);
            animator.SetTrigger(hTitle);
        }

        protected override bool GetVisibility()
        {
			return isShownByAnimation;
        }

        public void EventTitleShown()
        {
            animator.SetBool(Hash_ON, true);
            scorePanel.Show();
            tasksPanel.Show();

            Root.ClickTap.OnHold = MainTapHold;
            Root.ClickTap.OnRelease = MainTapRelease;
        }

        protected override void ShowHide(bool state)
        {
            base.ShowHide(state);
            if(state)
            {
                animator.SetBool(Hash_ON, true);
                scorePanel.Show();
                tasksPanel.Show();
                Root.ClickTap.OnHold = MainTapHold;
                Root.ClickTap.OnRelease = MainTapRelease;
            }
            else //hide
            {
                animator.SetBool(Hash_ON, false);
                scorePanel.Hide();
                tasksPanel.Hide();

                Root.ClickTap.OnHold = null;
                Root.ClickTap.OnRelease = null;
            }
        }

        public override void EventIsShownByAnimation()
        {
            base.EventIsShownByAnimation();
            Root.Tasks.OnHidden = Show;
            Root.Dialog.OnHidden = Show;
        }


        public RectTransform GetResourceCounter(int resource)
		{
			return scorePanel.RectT;
		}
		#endregion

		public void SetTrainDisplay(List<int> resources)
		{

		}

        

        public void SetWeight(float tractionRatio)
		{
			this.trainDisplay.SetWeight(tractionRatio);
			this.playButton.Set(
				tractionRatio > GameStarter.GameParameters.TrainShared.OverweightPenalty.MaxOverweight ?
					PlayWhistleButton.PlayButtonModes.Overweight : PlayWhistleButton.PlayButtonModes.Enabled);
		}

        private void MainTapRelease(Vector2 pressCoordinates, Vector2 releaseCoordinates, float time)
        {
            Vector2 offset = releaseCoordinates - pressCoordinates;
            if(offset.magnitude >= NO_SWIPE)
            {
                //horizontal
                if(Mathf.Abs(offset.x) > Mathf.Abs(offset.y))
                {
                    if(offset.x > 0)
                        OnSwipe?.Invoke(SwipeDirection.Left);
                    else
                        OnSwipe?.Invoke(SwipeDirection.Right);
                }
                else
                {
                    if(offset.y > 0)
                        OnSwipe?.Invoke(SwipeDirection.Down);
                    else
                        OnSwipe?.Invoke(SwipeDirection.Up);
                }
            }
        }
        private void MainTapHold(Vector2 currentCoordinates, float time)
        {
            if(time > HOLD_DURATION)
            {
				OnTapHold?.Invoke(time);
            }
        }


        #region TrainConstructor
        //пустое место в конструкторе
        public void SetEmpyInfo(int index, MTParameters.Train trainParameters)
		{
			trainMaintenance.Clear();
			trainMaintenance.SetInfoGroup(InfoGroup.Coach);
			trainMaintenance.SetCoachPosition(index + 1, GameStarter.GameParameters.CoachesNumber);
			/*
			//лимиты выключены на неопределённый срок
			TrainConstructor.DskInfo.DskInfo limitInfo = trainConstructor.GetLimitDskInfo(0);
			limitInfo.Activate(TrainConstructor.DskInfo.TrainCapabilities.Max);
			limitInfo.SetColor(TrainConstructor.DskInfo.CardColor.Red);
			limitInfo.Value(trainParameters.CoachesNumber, true);
			*/
			//кнопка купить должна быть выключена
			trainMaintenance.SetAsPurchased();
		}
		#endregion

		#region oldTOrefactor
		public RectTransform CloneRipple(Transform target)
		{
			RectTransform clone = GameObject.Instantiate<RectTransform>(rippleRef);
			clone.transform.SetParent(target, false);
			clone.transform.localPosition = Vector3.zero;
			Activate(clone.gameObject, true);
			return clone;
		}

		public RectTransform ClonePointer(Transform target)
		{
			RectTransform clone = GameObject.Instantiate<RectTransform>(pointerRef);
			clone.transform.SetParent(target, false);
			clone.localPosition = Vector3.zero;
			Activate(clone.gameObject, true);
			return clone;
		}
		#endregion

		#region Fly Through Town Points
		//приходит из города информация о том, что полёт началcя
		public void StartToPoint(Tag currentPoint, Tag newPoint)
		{
			//SwitchPanel.StartFly(currentPoint);
			////пришла команда на отлёт от точки поездов
			//if (currentPoint == MTTown.TownPointsOfInteres.TrainConstructor && newPoint != MTTown.TownPointsOfInteres.TrainConstructor)
			//{
			//	animator.SetBool(hTConstructor, false);
			//	receiveSwipe = false;

			//}
			////улетаем от дядюшки Фрица :-(
			//else if (currentPoint == MTTown.TownPointsOfInteres.AlterFritz)
			//{
			//	bubble.Hide();
			//}

			////улетаем от большого франца и его леcопилки :-(
			//else if (currentPoint == MTTown.TownPointsOfInteres.LumberMill)
			//{
			//	bubble.Hide();
			//}
		}

		//точка доcтигнута, приходит из города
		public void PointReceived(Tag newPoint)
		{
			//SwitchPanel.PointReceived(newPoint);
			//switch (newPoint)
			//{
			//	//прилетели в точку поездов
			//	case (MTTown.TownPointsOfInteres.TrainConstructor):
			//		{
			//			animator.SetBool(hTConstructor, true);
			//			receiveSwipe = true;
			//			break;
			//		}
			//	case (MTTown.TownPointsOfInteres.AlterFritz):
			//		{
			//			break;
			//		}

			//	case (MTTown.TownPointsOfInteres.LumberMill):
			//		{
			//			break;
			//		}

			//	default:
			//		{
			//			break;
			//		}
			//}
		}
		#endregion
	}
}