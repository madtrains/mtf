using UnityEngine;
using UnityEngine.UI;

namespace MTUi.TasksPopup
{
    public enum VendingElements { Tickets3, CogRainbow }

    public class Merch : TasksPopupCardBase
    {
        public UnityEngine.Events.UnityAction OnClick;
        [SerializeField] private ContentHolders buttons;
        [SerializeField] private ContentHolders vendingElements;
        [SerializeField] private UberButton button;

        public bool IsButtonActive { set { buttons.Activate(value); } }

        public VendingElements VendingElement { set { vendingElements.Activate((int)value); } }

        public override void Init()
        {
            base.Init();
            button.Init(() => { OnClick?.Invoke(); });
        }
    }
}

