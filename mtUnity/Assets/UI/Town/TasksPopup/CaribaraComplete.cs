﻿using MTCore;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.GraphicsBuffer;

namespace MTUi.TasksPopup
{
	public class CapibaraComplete : MonoBehaviour
	{
		[SerializeField] private GameObject canvas;

		[SerializeField] private TextMeshProUGUI titleText;        // Ссылка на ваш UI текстовый элемент
		[SerializeField] private TextMeshProUGUI displayText;        // Ссылка на ваш UI текстовый элемент

		[SerializeField] private float displayDuration = 2f; // Время показа каждого сообщения в секундах

		private Coroutine displayRoutine;
		private bool isActive;

		private List<MTCapibara> capibaras;
		private void Awake()
		{
			Init();
		}
		public void Init()
		{
			capibaras = new List<MTCapibara>();
			isActive = false;
		}

		public void ComplitionNotification(MTCapibara capibara)
		{
			capibaras.Add(capibara);
		}

		void Update()
		{
			if (isActive != canvas.activeSelf)
			{
				canvas.SetActive(isActive);
			}
			if (!isActive && capibaras.Any())
			{
				isActive = true;
				canvas.SetActive(true);
				displayRoutine = StartCoroutine(ShowMessages());
			}
		}
		private IEnumerator ShowMessages()
		{
			// Пока есть сообщения
			while (capibaras.Count > 0)
			{
				var capibara = capibaras[0];
				// Показываем первое сообщение
				titleText.text = capibara.IsHappy ? "Completed":"Failed";

				var titleKey = $"Capibara.{capibaras[0].Id}.Title";
				var title = GameStarter.Instance.TranslationManager.GetTableString(titleKey);

				displayText.text = title;
				Debug.Log($"Показ сообщения: {displayText.text}");

				// Ждём заданное время
				yield return new WaitForSeconds(displayDuration);

				// Удаляем показанное сообщение
				capibaras.RemoveAt(0);
			}

			// Когда сообщений больше нет
			isActive = false;
			// Можно здесь скрыть canvas, если хотите
			// canvas.SetActive(false);

			// Корутин завершится сам
		}

	}
}