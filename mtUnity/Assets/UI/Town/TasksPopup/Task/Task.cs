﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
namespace MTUi.TasksPopup
{
    public class Task : TasksPopupCardBase
    {
		public enum Breed { common, flipper, daily, weekly, monthly }

		public UnityEngine.Events.UnityAction<State> OnChangeState;
		public UnityEngine.Events.UnityAction OnReady;
		[SerializeField] private ContentHolders stateHolders;
		[SerializeField] private ContentHolders characters;

		[SerializeField] private UberButton acceptButton;
        [SerializeField] private UITextElement labelTaskTextEl;
        [SerializeField] private UITextElement labelRewardTextEl;

		[SerializeField] private UITextElement taskProcessCounter;
		[SerializeField] private Image taskIconIDImage;
		[SerializeField] private UITextElement rewardCounter;
		[SerializeField] private Image rewardIconIDImage;
		[SerializeField] private UITextElement rewardCounter2;
		[SerializeField] private Image rewardIconIDImage2;
		[SerializeField][Tooltip("Will be activated for reward2")] private GameObject[] reward2Objects;

		[SerializeField] ContentHolders taskType;
		
		private State _state;
		private int _taskCountV1;
		private int _taskCountV2;
		private GameCharacter _character;

		public string TaskBreed
		{
			set
			{
				taskType.Activate(value);
			}
		}

		public State State
		{
			get { return _state; }
			set
			{
				if (_state == value)
					return;

				_state = value;
				stateHolders.Activate((int)value);
				if (value == State.Ready)
					OnReady();

				if (OnChangeState != null)
					OnChangeState(_state);
			}
		}

		/// <summary>
		/// Текущее значение ресурса
		/// </summary>
		public int CountV1
		{
			get { return this._taskCountV1; }
			set
			{
				this._taskCountV1 = value;
				SetCountText();
			}
		}

		/// <summary>
		/// Целевое значени ресурса
		/// </summary>
		public int CountV2
		{
			get { return this._taskCountV2; }
			set
			{
				this._taskCountV2 = value;
				SetCountText();
			}
		}

		/// <summary>
		/// сумма  вознаграждения
		/// </summary>
		public int Reward
		{
			set
			{
				rewardCounter.TextField.text = value.ToString();
			}
		}

		/// <summary>
		/// индекс целевого ресурса
		/// </summary>
		public int TaskIconID
		{
			set
			{
				Sprite iconIDSprite = Root.GetIconID(value);
				Root.SetIconIDSpriteTo(taskIconIDImage, iconIDSprite);
			}
		}

		public int RewardIconID
		{
			set
			{
				Sprite iconIDSprite = Root.GetIconID(value);
				Root.SetIconIDSpriteTo(rewardIconIDImage, iconIDSprite);
			}
		}


		public int RewardIconID2
		{
			set
			{
				Sprite iconIDSprite = Root.GetIconID(value);
				Root.SetIconIDSpriteTo(rewardIconIDImage2, iconIDSprite);
			}
		}

		/// <summary>
		/// Персонаж, выдающий задание
		/// </summary>
		public GameCharacter Character
		{
			get { return _character; }
			set
			{
				_character = value;
				characters.Activate((int)_character);
			}
		}

        public override void Init()
		{
			base.Init();
			_state = State.NotAcceptable;
			acceptButton.Init(
				() =>
				{
					if (State == State.Acceptable)
						State = State.Ready;
				});

			labelTaskTextEl.Push();
            labelRewardTextEl.Push();
        }

		private void SetCountText()
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(this._taskCountV1);
			sb.Append("/");
			sb.Append(this._taskCountV2);
			taskProcessCounter.TextField.text = sb.ToString();
		}
	}
}