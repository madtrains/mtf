﻿using UnityEngine;
namespace MTUi.TasksPopup
{
    public enum ListPosition { First, Regular, Last }

    /// <summary>
    /// Кнопка заблокирована, Кнопка разблокирована, Задание выполнено
    /// </summary>
    public enum State { NotAcceptable, Acceptable, Ready }
    public class TasksPopupCardBase : UIElement
	{
		[SerializeField] protected ContentHolder bracketTop;
		[SerializeField] protected ContentHolder bracketBottom;
        [SerializeField] protected UITextElement txtTitle;
		[SerializeField] protected UITextElement txtMessage;


		public ListPosition ListPosition
		{
			set
			{
				switch (value)
				{
					case (ListPosition.First):
					{
						bracketTop.IsActive = false;
						bracketBottom.IsActive = true;
						break;
					}
					case (ListPosition.Last):
					{
						bracketTop.IsActive = true;
						bracketBottom.IsActive = false;
						break;
					}
					default:
						bracketTop.IsActive = true;
						bracketBottom.IsActive = true;
						break;
				}
			}
		}

		/// <summary>
		/// крупный заголовок карточки
		/// </summary>
		public void Title(string translationKey)
		{
			txtTitle.PushKey(translationKey, false);
		}

		/// <summary>
		/// текст задания карточки
		/// </summary>
		public void Message(string translationKey)
		{
            txtMessage.PushKey(translationKey, false);
        }
	}
}