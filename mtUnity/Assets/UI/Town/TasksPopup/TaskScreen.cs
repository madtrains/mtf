﻿using MTUi.ScorePanel;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace MTUi.TasksPopup
{
	public enum Mode { Tasks, Vending }

	public class TaskScreen : AnimUIElement
	{
		[SerializeField] private UberButton close;
		[SerializeField] private Task task;
		[SerializeField] private Merch merch;
		[SerializeField] private ContentHolders mode;
		[SerializeField] private Gift.Gift gift;

		private List<Task> tasks;
		private List<Merch> merchs;
		private Mode cMode;

		public void Show(Mode mode)
		{
			this.cMode = mode;
			this.mode.Activate((int)mode);
            ShowHide(true);
		}

        [System.ObsoleteAttribute("Use methods with Parameters instead. Can not show dialog without text and delegates")]
        public new void Show()
        {
            throw new System.NotSupportedException("Dialog was shown without any parameters");
        }

        public override void Init()
		{
			base.Init();
			base.Hide();
			tasks = new List<Task>();
			merchs = new List<Merch>();
			Activate(task.gameObject, false);
			Activate(merch.gameObject, false);
			close.Init(() =>
			{
				this.Hide();
			});
		}

		protected override void ShowHide(bool state)
		{
            //Root.ClickTapDisabled = state;
            if(state)
			{
                base.ShowHide(true);
                animator.SetBool(Hash_ON, true);
                switch(cMode)
                {
                    case (Mode.Tasks):
                    {
                        ProcessCapibaras();
                        break;
                    }
                    //пока тестово создаём две карточки
                    case (Mode.Vending):
                    {
                        ProcessVendingCards();
                        break;
                    }
                }
            }
			else
			{
                animator.SetBool(Hash_ON, false);
                ClearTasks();
                ClearMerchs();
            }
        }

        public Task CreateNewTask(string title)
		{
			Task result = UIElement.CloneUIElementByRef<Task>(task, task.transform.parent, title, true);
			result.Init();
			//result.OnReady += () =>{};
			tasks.Add(result);
			return result;
		}

		public Merch CreateNewMerch(string title)
		{
			Merch result = UIElement.CloneUIElementByRef<Merch>(merch, merch.transform.parent, title, true);
			result.Init();
			merchs.Add(result);
			return result;
		}

        private void ProcessCapibaras()
        {
            List<MTCore.MTCapibara> capibaras = GameStarter.Instance.GamePlayManager.GetZoo();
            foreach(MTCore.MTCapibara capibara in capibaras)
            {
                var targets = capibara.GetTargets();
                var counters = capibara.GetCounters();
                var rewards = capibara.GetRewards();

                Task uiTask = CreateNewTask("Task");
                uiTask.Character = (GameCharacter)capibara.Character;

                uiTask.Title($"Capibara.{capibara.Id}.Title");
                uiTask.Message($"Capibara.{capibara.Id}.Description");
                uiTask.TaskBreed = capibara.Breed;

                uiTask.CountV1 = targets[0].Item2 - counters[0].Item2;
                uiTask.CountV2 = targets[0].Item2;

                uiTask.Reward = rewards[0].Item2;
                uiTask.TaskIconID = targets[0].Item1;

                uiTask.ListPosition = ListPosition.Regular;
                uiTask.Init();

                //настройка задания
                if(capibara.IsHappy)
                {
                    uiTask.State = State.Acceptable;
                }

                uiTask.OnReady += () =>
                {
                    GameStarter.Instance.GamePlayManager.CapibaraWellDone(capibara);
                };
            }
        }

        private void ProcessVendingCards()
        {
            int TOTAL_COUNT = 2;
            for(int i = 0; i < TOTAL_COUNT; i++)
            {
                Merch merch = CreateNewMerch(string.Format("Merch {0}", i));
                StringBuilder title = new StringBuilder();
                title.Append("Town.TasksPopup.Vending.Title.");
                title.Append(i);
                merch.Title(title.ToString());

                StringBuilder message = new StringBuilder();
                message.Append("Town.TasksPopup.Vending.Message.");
                message.Append(i);
                merch.Message(message.ToString());

                merch.VendingElement = (VendingElements)i;

                ListPosition listPosition = ListPosition.Regular;
                if(i == 0)
                {
                    listPosition = ListPosition.First;
                }
                else if(i == TOTAL_COUNT - 1)
                {
                    listPosition = ListPosition.Last;
                }
                merch.ListPosition = listPosition;

                switch(i)
                {
                    case 0:
                    {
                        merch.IsButtonActive = true;
                        merch.OnClick += () =>
                        {
                            Root.Ads.Show();
                            Root.Ads.OnAdsScreenClick = () =>
                            {
                                gift.Play(Gift.Collection.Ticket, 3, true);
                                GameStarter.Instance.PlayerManager.AddTickets(3);
                            };
                        };
                        break;
                    }
                    case 1:
                    {
                        merch.IsButtonActive = false;
                        break;
                    }
                }
            }
        }

		private void ClearTasks()
		{
			if (tasks == null)
				return;

			foreach (var task in tasks)
			{
				task.SelfDestroy();
			}
			tasks = new List<Task>();
		}

		private void ClearMerchs()
		{
			if (merchs == null)
				return;

			foreach (var merch in merchs)
			{
				merch.SelfDestroy();
			}
			merchs = new List<Merch>();
		}
	}
}