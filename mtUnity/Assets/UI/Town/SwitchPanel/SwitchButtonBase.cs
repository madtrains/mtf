﻿using UnityEngine;
using UnityEngine.UI;

namespace MTUi.Town.TownPointsSwitcher
{
    public class SwitchButtonBase : UIElement
    {
        [SerializeField] protected GameObject[] rings;

        public virtual void HideRings()
        {
            foreach(GameObject ring in rings)
            {
                UberBehaviour.Activate(ring, false);
            }
        }
    }
}