﻿using UnityEngine;
using UnityEngine.UI;
using MTTown.PointsOfInterest;
using System.Collections.Generic;

namespace MTUi.Town.TownPointsSwitcher
{
    public class TownPointsSwitcher : UIElement
    {
        public UnityEngine.Events.UnityAction<Tag> OnSwitchButton;

        [SerializeField] private PointImage[] pointsImages;
        [SerializeField] private SwitchButton switchButtonRef;
        [SerializeField] private SwitchButtonBase switchButtonBaseRef;
        [SerializeField] private GameObject iconsRefPlane;
        [SerializeField] private RectTransform selector;

        private Vector2 selectorPosition;
        private Dictionary<Tag, SwitchButton> buttons;

        public override void Init()
        {
            base.Init();
            Activate(switchButtonRef.gameObject, false);
            Activate(switchButtonBaseRef.gameObject, false);

            buttons = new Dictionary<Tag, SwitchButton>();
            int a = 0;
            foreach (PointImage pImage in pointsImages)
            {
                SwitchButton switchButton = CloneUIElementByRef<SwitchButton>(
                    this.switchButtonRef,
                    switchButtonRef.transform.parent,
                    pImage.Points.ToString(), false);

                SwitchButtonBase switchButtonBase = 
                    CloneUIElementByRef<SwitchButtonBase>(
                        switchButtonBaseRef, switchButtonBaseRef.transform.parent, string.Format("{0}_base", pImage.Points), true);
                Button button = switchButton.Set(pImage.Image, switchButtonBase);

                button.onClick.AddListener(() =>
                {
                    OnSwitchButton?.Invoke(pImage.Points);
                    
                });
                buttons.Add(pImage.Points, switchButton);
                switchButton.SetActiveIcon(0);

                if (a==0)
                {
                    switchButton.HideRings();
                }
                a++;
            }

            Activate(iconsRefPlane.gameObject, false);
        }

        public SwitchButton GetSwitchButton(Tag target)
        {
            if (buttons.ContainsKey(target))
            {
                return buttons[target];
            }

            Debug.LogErrorFormat("No Switch button for target : {0}", target);
            return null;
        }

        public void SetButtonActive(Tag point, bool value)
        {
            buttons[point].SetActive(value);
        }

        public void SwitchStart(Tag pointA)
        {
            selectorPosition = selector.anchoredPosition;
            //) { buttons[pointA].SetActiveIcon(false); }
        }
        
        public void Switch(
                        Tag pointA,
                        Tag pointB, float t)
        {
            if (!buttons.ContainsKey(pointB))
                return;

            float th = 0.1f;

            if(buttons.ContainsKey(pointA))
            {
                float t1 = Mathf.InverseLerp(0.0f, 0.0f + th, t);
                buttons[pointA].SetActiveIcon(1-t1);
            }
            if(buttons.ContainsKey(pointB))
            {
                float t2 = Mathf.InverseLerp(1.0f - th, 1.0f, t);
                buttons[pointB].SetActiveIcon(t2);
            }


            selector.anchoredPosition = Vector2.Lerp(
                selectorPosition,
                buttons[pointB].RectTransform.anchoredPosition,
                t);
        }

        public void SwitchEnd(Tag pointB)
        {
            //if (!buttons.ContainsKey(pointB)) return;
            //buttons[pointB].SetActiveIcon(true);
        }
    }

    [System.Serializable]
    public class ButtonTemplate
    {
        
    }


    [System.Serializable]
    public class PointImage
    {
        public Tag Points { get { return points; } }
        [SerializeField] private Tag points;

        public Image Image { get { return image; } }
        [SerializeField] private Image image;
    }
}