﻿using UnityEngine;
using UnityEngine.UI;

namespace MTUi.Town.TownPointsSwitcher
{
    public class SwitchButton : SwitchButtonBase
    {
        public Button Button { get { return button; } }

        [SerializeField] private Transform iconDock;
        [SerializeField] private SwitchButtonBase shadow;
        private Image icon;
        private Button button;


        public void SetActiveIcon(float t)
        {
            icon.rectTransform.localScale = Vector3.Lerp(Vector3.one * 0.75f, Vector3.one * 1.15f, t);
        }

        public Button Set(Image icon, SwitchButtonBase shadow)
        {
            this.icon = icon;
            Vector2 half = new Vector2(0.5f, 0.5f);
            icon.transform.SetParent(this.iconDock, false);
            icon.rectTransform.anchorMin = half;
            icon.rectTransform.anchorMax = half;
            icon.rectTransform.anchoredPosition = Vector2.zero;
            GetComponent<Image>().raycastTarget = true;

            button = gameObject.AddComponent<Button>();
            button.interactable = true;
            this.shadow = shadow;
            return button;
        }

        public void SetActive(bool value) 
        {
            Activate(this.gameObject, value);
            Activate(this.shadow.gameObject, value);
        }

        public override void HideRings()
        {
            base.HideRings();
            shadow.HideRings();
        }
    }
}