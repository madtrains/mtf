﻿using UnityEngine;
using UnityEngine.UI;

namespace MTUi
{
    public class Bubble : UIElement
	{
        public enum StateValue
        {
            Idle,
            Task,
            Wait,
            Ready,
        }

        public Button Button { get { return button; } }

		public UnityEngine.Events.UnityAction OnClick;

		public MTTown.Characters.InteractiveCharacter Character
		{
			get { return _character; }
			set { _character = value; }
		}

		public void AddDisposableCommand(CLickDelegate @delegate)
		{
			button.onClick.AddListener(() => { DisposableCommand(@delegate); });
		}

		private void DisposableCommand(CLickDelegate @delegate)
		{
			@delegate();
			button.onClick.RemoveListener(() => { DisposableCommand(@delegate); });
		}

		private MTTown.Characters.InteractiveCharacter _character;

		[SerializeField] private GameObject content;
		[SerializeField] private RectTransform sizeController;
		[SerializeField] private Button button;
        private StateValue State;

        //выставляется в интерфейсе, поэтому 0 референсов
        public void ButtonCommand()
		{
			OnClick?.Invoke();
		}

		public void UpdateOnCharacter()
		{
			if (_character == null)
				return;

			Vector3 transformed = GameStarter.Instance.MainComponents.GameCamera.GetScreenPoint(_character.BubbleDock.position);
			this.transform.position = transformed;
			RectTransform.anchoredPosition = RectTransform.anchoredPosition + _character.Offset;
		}

		
		protected override void ShowHide(bool state)
		{
			Activate(this.content, state);
		}

		protected override bool GetVisibility()
		{
			return content.activeSelf;
		}

		private void Update()
		{
			if (!Visible)
				return;

			UpdateOnCharacter();
		}

		public void ChangeState(StateValue state)
		{
			State = state;
			//Debug.LogWarning(state.ToString());
			ChangeAppiarence();
		}
		private void ChangeAppiarence()
		{
			var text = gameObject.GetComponentInChildren<Text>();
			if (text != null)
			{
				text.lineSpacing = 0.4f;

				switch (State)
				{
					case StateValue.Idle:
						text.text = "..." + "\n";
                        text.fontSize = 90;
                        text.color = Color.yellow;
						break;
					case StateValue.Task:
						text.text = "?";
                        text.fontSize = 90;
                        text.color = Color.yellow;
						break;
					case StateValue.Wait:
						text.text = "!";
                        text.fontSize = 90;
                        text.color = Color.gray;
						break;
					case StateValue.Ready:
						text.text = "!";
						text.fontSize = 95;
						text.color = Color.yellow;
						break;

					default:
						break;
				}
			}
		}

	}
}