﻿using UnityEngine;

namespace MTUi.TasksPanel
{
    public class TasksPanel : AnimUIElement, ISharedUIElement<TasksPanel>
    {
        public UnityEngine.Events.UnityAction OnShowVending;
        public UnityEngine.Events.UnityAction OnShowTasks;

        public UberButton ButtonShop { get { return buttonShop; } }
        public UberButton ButtonTasks { get { return buttonTasks; } }

        [SerializeField] private UberButton buttonShop;
        [SerializeField] private UberButton buttonTasks;


        public override void Init()
        {
            ISharedUIElement<TasksPanel>.AddInstance(this);
            buttonShop.Init(() =>
            {
                Root.Tasks.Show(TasksPopup.Mode.Vending);
                OnShowVending?.Invoke();
            });

            buttonTasks.Init(() =>
            {
                Root.Tasks.Show(TasksPopup.Mode.Tasks);
                OnShowTasks?.Invoke();
            });
        }
    }
}