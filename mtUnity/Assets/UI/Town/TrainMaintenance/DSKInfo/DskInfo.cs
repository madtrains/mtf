﻿using UnityEngine;
using UnityEngine.UI;

namespace MTUi.Town.TrainMaintenance.Dsk
{

    public class DskInfo : Dsk
    {
        [SerializeField] protected Color[] colors;
        [SerializeField] private Text value;
        [SerializeField] private ContentHolders holders;
        [SerializeField] private DskStar[] stars;

        public void SetColor(CardColor color)
        {
            SetColor(colors[(int)color]);
        }

        public override void SetColor(Color color)
        {
            base.SetColor(color);
            foreach (DskStar star in stars)
            {
                star.Color = color;
            }
        }

        public int ValueAndCross
        {
            set
            {
                bool active = value > 0;
                this.value.text = value.ToString();
                UberBehaviour.Activate(this.value.gameObject, active);
            }
        }

        public void Activate(Attributes attribute)
        {
            int index = (int)attribute + 10;
            holders.Activate(index);
        }

        public void ShowResource(int resourceID)
        {
            holders.Activate(resourceID);
        }

        public int Stars
        {
            set
            {
                for (int i = 0; i < stars.Length; i++)
                {
                    UberBehaviour.Activate(stars[i].gameObject, value >= 0);
                    stars[i].Filled = value > i;
                }
            }
        }
    }
}