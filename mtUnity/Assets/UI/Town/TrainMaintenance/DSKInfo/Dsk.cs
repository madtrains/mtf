﻿using UnityEngine;
using UnityEngine.UI;
namespace MTUi.Town.TrainMaintenance.Dsk
{

    public enum CardColor { Blue, Rose, Yellow }
    public class Dsk : UberButton
    {
        
        [SerializeField] protected Graphic[] colorise;

        public virtual void SetColor(Color color)
        {
            foreach (Graphic im in colorise)
            {
                im.color = color;
            }
        }
    }
}