﻿using MTTown;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
namespace MTUi.Town.TrainMaintenance.Dsk
{
    public class DskColorButton : Dsk
    {
        public UnityAction<int, Color> OnClick;

        private int index;
        private Color color;

        public void Init(int index, Color color)
        {
            this.index = index;
            this.color = color;
            base.Init(() => { { OnClick?.Invoke(this.index, this.color); } });
            this.SetColor(color);
        }
    }
}