﻿using UnityEngine;
using UnityEngine.UI;
namespace MTUi.Town.TrainMaintenance.Dsk
{

    public class DskStar : UIElement
    {
        [SerializeField] protected Graphic otl;
        [SerializeField] protected Graphic fill;

        public bool Filled
        {
            set
            {
                Activate(otl.gameObject, !value);
                Activate(fill.gameObject, value);

            }
        }

        public Color Color
        {
            set
            {
                otl.color = value;
                fill.color = value;
            }
        }
    }
}