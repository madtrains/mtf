﻿using MTUi.Town.TrainMaintenance.Dsk;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace MTUi.Town.TrainMaintenance
{
    public enum AuxMode { Color, Cogs }

    

    //вторая квадратная карточка
    public class TrainMaintenanceAux : AnimUIElement
    {
        public UnityEngine.Events.UnityAction<AuxMode> OnButtonAuxOpen;
        public UnityEngine.Events.UnityAction OnButtonAuxClose;
        public UnityAction<int, Color> OnColorButtonClick;
        public UnityAction<bool> Colouring;


        public Colorization Colorise { get { return colorization; } }
        public CogUnlockerUI CogUnlocker { get { return cogUnlockerUI; } }

        [SerializeField] UberButton close;
        [SerializeField] ContentHolders modeSwitcher;
        [SerializeField] Colorization colorization;
        [SerializeField] CogUnlockerUI cogUnlockerUI;

        public void Show(AuxMode mode)
        {
            modeSwitcher.Activate((int)mode);
            base.Show();
        }

        public override void Init()
        {
            base.Init();
            colorization.Init();
            close.Init(() =>
            {
                this.Hide();
                //OnCloseButton?.Invoke();
            });
        }

        protected override void ShowHide(bool state)
        {
            base.ShowHide(state);
            if (!state) { Activate(close.gameObject, false); }
        }

        public override void EventIsShownByAnimation()
        {
            base.EventIsShownByAnimation();
            Activate(close.gameObject, true);
        }



        public override void EventIsHiddenByAnimation()
        {
            base.EventIsHiddenByAnimation();
        }

        /*
        aux.OnCloseButton = OnButtonAuxClose;

        this.trainMaintenance.OnButtonAuxOpen = (mode) =>
			{
                animator.SetBool(hAux, true);
				trainMaintenance.Aux.ActivateContent(mode);
				OnAuxActive?.Invoke(true);
                //Debug.LogFormat("mode: {0}", mode);
			};
			this.trainMaintenance.OnButtonAuxClose = () => { animator.SetBool(hAux, false); };



        OnButtonAuxOpen = (mode) =>
            {
                animator.SetBool(hAux, true);
                //trainMaintenance.Aux.ActivateContent(mode);
                //OnAuxActive?.Invoke(true);
                //Debug.LogFormat("mode: {0}", mode);
            };

        public void CloseAux()
        {
            OnButtonAuxClose?.Invoke();
            GameStarter.Instance.MainComponents.SoundManager.Shared.Play(MTSound.SharedNames.MaintenanceClose);
        }
        */


        [System.Serializable]
        public class Colorization
        {
            [SerializeField] DskColorButton colorButtonSrc;
            public UnityAction<int, Color> OnColorClick;

            public void Init()
            {
                for (int i = 0; i < GameStarter.GameParameters.Customization.Colors.Length; i++)
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append("Color_");
                    stringBuilder.Append(i);
                    Color currentColor =
                        GameStarter.GameParameters.Customization.Colors[i];
                    DskColorButton button = 
                        UIElement.CloneUIElementByRef<DskColorButton>(
                            colorButtonSrc, 
                            colorButtonSrc.transform.parent, 
                            stringBuilder.ToString(), true);
                    button.Init(i, currentColor);
                    button.OnClick =(index, color) => { OnColorClick?.Invoke(index, color); };
                }
                UberBehaviour.Activate(colorButtonSrc.gameObject, false);
            }
        }
    }
}