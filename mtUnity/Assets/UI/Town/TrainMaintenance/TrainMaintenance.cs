﻿using MTUi.Town.TrainMaintenance.Dsk;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.Town.TrainMaintenance
{
    public enum Attributes { Flip, Horn, DoubleFlip, Coaches, Paint }
    public enum CardColor { Blue, Red }
    public enum InfoGroup { Train, Coach }

    

    //большая квадратная карточка информации о локомотиве/вагоне и манипуляции с ним
    public class TrainMaintenance : AnimUIElement
    {
        public UnityEngine.Events.UnityAction OnButtonBuyClicked;

        public TrainMaintenanceAux Aux { get { return aux; } }
        public CogUnlockerUI CogUnlockerUI { get { return cogUnlockerUI; } }

        public int SpeedInfo { set { infoStrings[0].SetFormatValues(value); } }
        public int RollInfo { set { infoStrings[1].SetFormatValues(value); } }
        public int DurabilityInfo { set { infoStrings[2].SetFormatValues(value); } }
        public int TractionForceInfo { set { infoStrings[3].SetFormatValues(value); } }
        public int CoachWeight { set { infoStrings[4].SetFormatValues(value); } }

        public int LocomotiveCoachID { get { return this.locomotiveCoachID; } set { locomotiveCoachID = value; } }

        public string Type { set { type.PushKey(value); } }
        public string Model { set { model.PushKey(value); } }

        [SerializeField] private TrainMaintenanceAux aux;
        [SerializeField] private ContentHolders mainHolders;
        [SerializeField] private Dsk.DskInfo[] dskInfos;
        [SerializeField] private Dsk.DskInfo[] dskInfos2;
        [SerializeField] private UITextElement[] infoStrings;
        [SerializeField] private Text coachPosition;
        [SerializeField] private ContentHolders infoGroups;
        [SerializeField] private UITextElement type;
        [SerializeField] private UITextElement model;
        [SerializeField] private Text price;
        [SerializeField] private GameObject buyButtonActive;
        [SerializeField] private GameObject buyButtonInactive;
        [SerializeField] private ContentHolder buyPanel;
        [SerializeField] private CogUnlockerUI cogUnlockerUI;
        private int locomotiveCoachID;

        public override void Init()
        {
            UberButton buyButton = buyButtonActive.AddComponent<UberButton>();
            buyButton.Init(() => { OnButtonBuyClicked(); });
            
            base.Init();
        }

        protected override void ShowHide(bool state)
        {
            base.ShowHide(state);
            if (state)
            {

            }
            else
            {
                aux.Hide();
            }
        }

        public void Clear()
        {
            DisableDsks();
        }

        public override void Reset()
        {
            base.Reset();
            aux.Hide();
        }

        public void SetLocomotiveInfo(MTParameters.Train trainParameters)
        {
            Clear();
            Type = trainParameters.Type;
            Model = trainParameters.Model;
            SetInfoGroup(MTUi.Town.TrainMaintenance.InfoGroup.Train);
            LocomotiveCoachID = trainParameters.ID;

            SpeedInfo = Mathf.RoundToInt(trainParameters.Speed * 3.6f);
            float roll = (1.0f / trainParameters.FlipTime) * 10;
            RollInfo = Mathf.RoundToInt(roll);
            DurabilityInfo = trainParameters.HitPoints;
            TractionForceInfo = Mathf.RoundToInt(trainParameters.TractionPower);

            DskInfo info1 = GetActivateDskInfo(0);
            info1.Activate(Attributes.Flip);
            info1.Stars = 1;

            DskInfo info2 = GetActivateDskInfo(1);
            info2.Activate(Attributes.Horn);
            info2.Stars = 1;

            DskInfo info3 = GetActivateDskInfo(2);
            info3.Activate(Attributes.DoubleFlip);
            info3.Stars = 1;
            price.text = trainParameters.Price.ToString();
        }

        public void SetCoachInfo(int absoluteIndex, MTParameters.Coach coachParameters)
        {
            Clear();
            LocomotiveCoachID = coachParameters.ID;
            Type = coachParameters.Type;
            Model = coachParameters.Model;
            price.text = coachParameters.Price.ToString();
            SetInfoGroup(InfoGroup.Coach);
            SetCoachPosition(absoluteIndex, GameStarter.GameParameters.CoachesNumber);
            CoachWeight = Mathf.RoundToInt(coachParameters.Weight);

            for (int i = 0; i < coachParameters.Resources.Count; i++)
            {
                int resourceIndex = coachParameters.Resources[i];
                Dsk.DskInfo dskInfo = GetActivateDskInfo(i);
                
                dskInfo.ShowResource(resourceIndex);
                //ресурс
                if (resourceIndex != 2)
                {
                    dskInfo.ValueAndCross = coachParameters.Capacity;
                }
                //почта без ёмкости
                else
                {
                    dskInfo.ValueAndCross = -1;
                }
                dskInfo.Stars = 0;
            }
        }

        public void SetCoachForCoupler(int index, MTParameters.Coach coachParameters)
        {
            Dsk.DskInfo openColorSelectorButton = GetActivateDskInfo2(0);
            openColorSelectorButton.SetColor(Dsk.CardColor.Yellow);
            openColorSelectorButton.Init(() =>
            {
                aux.Show(AuxMode.Color);
                //OnButtonAuxOpen?.Invoke(AuxMode.Color);
            });
            openColorSelectorButton.Activate(Attributes.Paint);
        }

        public void SetCoachForDepo(
            MTParameters.Coach coachParameters, 
            MTPlayer.CoachDepoInfo coachDepoInfo, 
            CLickDelegate numberUpgradeClick)
        {
            Dsk.DskInfo openNumberUpgrade = GetActivateDskInfo2(0);
            openNumberUpgrade.Activate(Attributes.Coaches);
            openNumberUpgrade.Stars = coachDepoInfo.NumberLevel;
            openNumberUpgrade.ValueAndCross = coachDepoInfo.Number;

            if (numberUpgradeClick != null)
            {
                openNumberUpgrade.Interactable = true;
                openNumberUpgrade.Init(() =>
                {
                    numberUpgradeClick();
                    aux.Show(AuxMode.Cogs);
                    //OnButtonAuxOpen?.Invoke(AuxMode.Cogs);
                    //надо открыть текущую прокачку количества вагонов
                });
            }
            else
            {
                openNumberUpgrade.Interactable = false;
            }
        }

        public void SetInfoGroup(InfoGroup infoGroup)
        {
            infoGroups.Activate((int)infoGroup);
        }

        public void SetCoachPosition(int position, int coachesNumber)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(position);
            sb.Append("/");
            sb.Append(coachesNumber);
            coachPosition.text = sb.ToString();
        }

        public Dsk.DskInfo GetActivateDskInfo(int index)
        {
            UberBehaviour.Activate(dskInfos[index].gameObject, true);
            return dskInfos[index];
        }

        public Dsk.DskInfo GetActivateDskInfo2(int index)
        {
            UberBehaviour.Activate(dskInfos2[index].gameObject, true);
            return dskInfos2[index];
        }

        private void DisableDsks()
        {
            foreach (Dsk.DskInfo cp in dskInfos)
            {
                UberBehaviour.Activate(cp.gameObject, false);
                cp.ValueAndCross = -1;
                cp.Stars = -1;
            }

            foreach (Dsk.DskInfo lm in dskInfos2)
            {
                UberBehaviour.Activate(lm.gameObject, false);
                lm.ValueAndCross = -1;
                lm.Stars = -1;
            }
        }

        public void SetAsPurchased()
        {
            mainHolders.Activate(false);
            buyPanel.IsActive = false;
            UberBehaviour.Activate(buyButtonActive, false);
            UberBehaviour.Activate(buyButtonInactive, false);
        }

        public void SetAsUnlocked(bool canBuy)
        {
            //cogUnlocker.Set(Root.GetResource(9).Value, cogPrice, cogsTransfered);
            mainHolders.Activate(false);
            buyPanel.IsActive = true;
            UberBehaviour.Activate(buyButtonActive, canBuy);
            UberBehaviour.Activate(buyButtonInactive, !canBuy);
        }

        public void SetAsLocked()
        {
            mainHolders.Activate(true);
            buyPanel.IsActive = false;
            UberBehaviour.Activate(buyButtonActive, false);
            UberBehaviour.Activate(buyButtonInactive, false);
        }

        public override void EventIsShownByAnimation()
        {
            base.EventIsShownByAnimation();
        }

        public override void EventIsHiddenByAnimation()
        {
            base.EventIsHiddenByAnimation();
        }
    }
}