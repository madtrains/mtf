﻿using MTUi.Town.TrainMaintenance.Dsk;
using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.Town.TrainMaintenance
{
    public class CogUnlockerUI : AnimUIElement
    {
        public  DskInfo From { get { return from; } }
        public DskInfo To { get { return to; } }

        public Action<bool> OnPressed;
        public Action<FloatingSprite> OnGearCreated;
        public Action<FloatingSprite> OnGearEnd;
        public Action OnBurstend;

        [SerializeField] private Slider slider;
        [SerializeField] private Text percentText;
        [SerializeField] private Text tValueCurrent;
        [SerializeField] private Text tValueTarget;
        [SerializeField] private RectTransform bigGear;
        [SerializeField] private Text idText;
        [SerializeField] private DskInfo from;
        [SerializeField] private DskInfo to;
        [SerializeField] private GameObject fromTo;
        [SerializeField] private float blendStep;
        [SerializeField] private GearRotation[] rotations;
        [SerializeField] private float blend;
        private bool reversed;
        private SpriteStream gearStream;

        public static int hBlend = Animator.StringToHash("blend");

        public void Assign(SpriteStream gearStream, int id)
        {
            this.gearStream = gearStream;
            this.gearStream.OnElementCreated = this.GearCreated;
            this.gearStream.OnElementDone = this.GearDone;
            this.gearStream.OnBurstEnd = this.BurstEnd;
            idText.text = string.Format("ID: {0}", id);
        }

        public void Press(bool isPressed)
        {
            OnPressed?.Invoke(isPressed);
        }

        public void Generate(bool value)
        {
            if (value)
            {
                gearStream.StartGeneration();
            }
            else
            {
                gearStream.StopGeneration();
            }
        }

        public void UpdateInfo(int amount, int targetAmount)
        {
            float division = (float)amount / (float)targetAmount;
            int percents = Mathf.RoundToInt(division * 100);
            this.tValueCurrent.text = amount.ToString();
            Percents = percents;
            slider.value = percents;
            this.tValueCurrent.text = amount.ToString();
            this.tValueTarget.text = targetAmount.ToString();
        }

        private int Percents
        {
            set
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(value);
                sb.Append("%");
                percentText.text = sb.ToString();
            }
        }

        private void GearCreated(FloatingSpriteTimer sprite)
        {
            OnGearCreated?.Invoke(sprite);
            GameStarter.Instance.MainComponents.SoundManager.Shared.Play(MTSound.SharedNames.PopJar);
            //Debug.LogWarningFormat("Gear Created: w{0} l{1} to{2}", gearWeight, limit, toTransfer);
        }

        private void GearDone(FloatingSpriteTimer sprite)
        {
            OnGearEnd?.Invoke(sprite);
            foreach(var gr in rotations)
            {
                gr.Rotate();
                GameStarter.Instance.MainComponents.SoundManager.Shared.Play(MTSound.SharedNames.BigCog);
            }
            float step = reversed ? -blendStep : blendStep;
            blend += step;
            if (blend >= 1f)
            {
                reversed = true;
                float over = blend - 1f;
                blend = blend - over;
            }
            if (blend <= 0f)
            {
                reversed = false;
                float over = Mathf.Abs(blend);
                blend = blend + over;
            }
            animator.SetFloat(hBlend, blend);
            //GameStarter.Instance.MainComponents.SoundManager.Shared.Coin();
        }

        private void BurstEnd()
        {
            OnBurstend?.Invoke(); 
        }

        [System.Serializable]
        public class GearRotation
        {
            public void Rotate()
            {
                this.gear.Rotate(0f, 0f, angle);
            }

            [SerializeField] private Transform gear;
            [SerializeField] private float angle;
            
        }
    }
}