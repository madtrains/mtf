﻿using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.Town
{
    public class DialogShieldInfo
    {
        public DialogShieldInfo(string text)
        {
            this._text = text;
        }

        public DialogShieldInfo(string text, int iconID)
        {
            this._text = text;
            this._iconID = iconID;
        }

        public int IconID
        {
            get
            {
                return _iconID;
            }
        }

        public string Text
        {
            get
            {
                return _text;
            }
        }

        public string _text = string.Empty;
        private int _iconID = -1;
    }

    public class DialogShield : UIElement
    {
        public void Set(DialogShieldInfo info)
        {
            this.text.text = info.Text;
            if (info.IconID >= 0)
            {
                Sprite icon = GameStarter.Instance.UIRoot.GetIconID(info.IconID);
				GameStarter.Instance.UIRoot.SetIconIDSpriteTo(image, icon);
                Activate(image.gameObject, true);
            }
            Activate(this.gameObject, true);
        }

        [SerializeField] private Text text;
        [SerializeField] private Image image;

        public override void Init()
        {
            base.Init();
        }
    }
}