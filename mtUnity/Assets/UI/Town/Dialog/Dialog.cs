﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.Town
{
    public class Dialog : AnimUIElement
    {
        public static string Character2Key(GameCharacter character)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Character.");
            sb.Append((int)character);
            sb.Append(".");
            sb.Append(character);
            return sb.ToString();
        }

        public Text TextComponent { get { return mainText; } }

        [SerializeField] private Text mainText;
        [SerializeField] private UberButton cross;
        [SerializeField] private GameObject buttonHolder;
        [SerializeField] private UberButton[] uberButtons;
        [SerializeField] private ContentHolders characters;
        [SerializeField] private Text characterName;
        [SerializeField] private DialogShield taskShield;
        [SerializeField] private DialogShield rewardShield;

        private List<DialogShield> taskShields;
        private List<DialogShield> rewardShields;

        public GameCharacter Character
        {
            set
            {
                characters.Activate((int)value);
                characterName.text = GameStarter.Instance.TranslationManager.GetTableString(Character2Key(value));
            }
        }

        public void Show(string text)
        {
            Set(text, GameCharacter.Fritz);
            ShowHide(true);
        }

		public void Show(string text, GameCharacter character)
        {
            Set(text, character);
            ShowHide(true);
        }

        public void Show(string text, GameCharacter character, params DialogButton[] dialogButtons)
        {
            Set(text, character, dialogButtons);
            ShowHide(true);
        }

        public void Show(string text, GameCharacter character, DialogButton[] dialogButtons, DialogShieldInfo[] taskShieldsInfos, DialogShieldInfo[] rewardShieldsInfos)
        {
            Set(text, character, dialogButtons, taskShieldsInfos, rewardShieldsInfos);
            ShowHide(true);
        }

        public void Set(string text, GameCharacter character, params DialogButton[] dialogButtons)
        {
			Set(text, character, dialogButtons, null, null);
        }

        public void Set(string text, GameCharacter character, DialogButton[] dialogButtons, DialogShieldInfo[] taskShieldsInfos, DialogShieldInfo[] rewardShieldsInfos)
        { 
            mainText.text = text;
            Character = character;
            #region buttons
            //есть кнопки, крестик не нужен
            if (dialogButtons != null && dialogButtons.Length > 0)
            {
                Activate(buttonHolder.gameObject, true);
                Activate(cross.gameObject, false);
                for (int i = 0; i < uberButtons.Length; i++)
                {
                    UberButton ub = uberButtons[i];
                    if (i < dialogButtons.Length)
                    {
                        ub.ButtonLabel = dialogButtons[i].Name;
                        DialogButton db = dialogButtons[i];
                        ub.SetCommand(() =>
                        {
                            db.Delegate();
                        });
                        Activate(uberButtons[i].gameObject, true);
                    }
                    else
                    {
                        Activate(uberButtons[i].gameObject, false);
                    }
                }
            }
            //крестик
            else
            {
                Activate(buttonHolder.gameObject, false);
                Activate(cross.gameObject, true);
            }
            #endregion

            #region taskShields
            ClearShields(ref taskShields);
            if (taskShieldsInfos != null && taskShieldsInfos.Length > 0)
            {
                SetShields(taskShieldsInfos, taskShield, ref taskShields);
            }

            ClearShields(ref rewardShields);
            if (rewardShieldsInfos != null && rewardShieldsInfos.Length > 0)
            {
                SetShields(rewardShieldsInfos, rewardShield, ref rewardShields);
            }
            #endregion
        }

        [System.ObsoleteAttribute("Use methods with Parameters instead. Can not show dialog without text and delegates")]
        public new void Show()
        {
            throw new System.NotSupportedException("Dialog was shown without any parameters");
        }

        public override void Init()
        {
            base.Init();
            cross.Init(() => {Hide(); });
            for (int i = 0; i < uberButtons.Length; i++)
            {
                uberButtons[i].Init();
            }
        }

        private void ClearShields(ref List<DialogShield> collection)
        {
            if (collection != null && collection.Count > 0)
            {
                for (int i = 0; i < collection.Count; i++)
                {
                    if (collection[i].gameObject != null)
                    {
                        Destroy(collection[i].gameObject);
                    }
                }
            }

            collection = new List<DialogShield>();
        }

        private void SetShields(DialogShieldInfo[] infos, DialogShield sourceShield, ref List<DialogShield> shieldsCollection)
        {
            for (int i = 0; i < infos.Length; i++)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Shield_");
                sb.Append(i);
                DialogShieldInfo info = infos[i];
                DialogShield copyShield =
                    CloneUIElementByRef<DialogShield>(sourceShield, sourceShield.transform.parent, sb.ToString(), true);
                copyShield.Set(info);
                shieldsCollection.Add(copyShield);
            }

        }
    }


    public class DialogButton
    {
        public string Name { get { return name; } }
        public CLickDelegate Delegate { get {return delegat;} }


        public DialogButton(string name, CLickDelegate delegat)
        {
            this.name = name;
            this.delegat = delegat;
        }

        private string name;
        private CLickDelegate delegat;
    }
}