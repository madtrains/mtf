﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.TextCore.Text;
using UnityEngine.UI;



namespace MTUi
{
    [System.Serializable]
    public class CargosSwitcher
    {
        public int Current { get { return currentIndex; } }
        public void Set(Cargos cargo)
        {
            Set((int)cargo);
        }

        public void Set(int index)
        {
            currentIndex = index;
            foreach (KeyValuePair<int, GameObject> kvp in Linker)
            {
                UberBehaviour.Activate(kvp.Value, kvp.Key == index);
            }
        }

        [SerializeField] private GameObject cargo0;
        [SerializeField] private GameObject cargo1;
        [SerializeField] private GameObject cargo2;
        [SerializeField] private GameObject cargo3;
        [SerializeField] private GameObject cargo4;
        [SerializeField] private GameObject cargo5;
        [SerializeField] private GameObject cargo6;
        [SerializeField] private GameObject cargo7;

        private Dictionary<int, GameObject> Linker
        {
            get
            {
                if (_lnk == null)
                {
                    _lnk = new Dictionary<int, GameObject>();
                    _lnk.Add(0, cargo0);
                    _lnk.Add(1, cargo1);
                    _lnk.Add(2, cargo2);
                    _lnk.Add(3, cargo3);
                    _lnk.Add(4, cargo4);
                    _lnk.Add(5, cargo5);
                    _lnk.Add(6, cargo6);
                    _lnk.Add(7, cargo7);
                }
                return _lnk;
            }
        }
        private Dictionary<int, GameObject> _lnk;
        private int currentIndex;
    }
}