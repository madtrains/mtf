using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MTUi.Town
{
    public class ScTownRipple : UIElement
    {
        public void Clone()
        {
            ScTownRipple copy = Instantiate<ScTownRipple>(this, this.transform.parent);
            copy.transform.localPosition = Vector3.zero;
        }
    }
}