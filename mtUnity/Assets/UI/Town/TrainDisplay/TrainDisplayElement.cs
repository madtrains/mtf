﻿using UnityEngine;

namespace MTUi.Town.TrainDisplay
{
    public class TrainDisplayElement : UIElement
    {
        public void SetEnabled(bool value)
        {
            holders.Activate(BoolToInt(value));                
        }

        public CargosSwitcher CargosSwitcher { get { return cargosSwitcher; } }

        [SerializeField] private ContentHolders holders;
        [SerializeField] private CargosSwitcher cargosSwitcher;
    }
}