﻿using UnityEngine;
using UnityEngine.UI;

namespace MTUi.Town.TrainDisplay
{
    public class TrainDisplay : UIElement
    {
        public void SetWeight(float weight)
        {
            float v1 = Mathf.InverseLerp(0f, 1f, weight);
            this.weight.fillAmount = v1;
            float v2 = weight > 1f ?
                Mathf.InverseLerp(1f, GameStarter.GameParameters.TrainShared.OverweightPenalty.MaxOverweight, weight) :
                0f;
            this.overweight.fillAmount = v2;
        }

        [SerializeField] private Image weight;
        [SerializeField] private Image overweight;
    }
}
