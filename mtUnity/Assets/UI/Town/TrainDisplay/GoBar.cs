﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.GraphicsBuffer;

namespace MTUi.Town
{
    public class GoBar : UIElement
    {
        public UnityEngine.Events.UnityAction OnTimerEnd;
        public UnityEngine.Events.UnityAction OnClick;
        public UnityEngine.Events.UnityAction<bool> OnChangeVisibility;
        [SerializeField] private int time;
        [SerializeField] private Image bar;
        private TimerBeh timer;

        public void Run()
        {
            UberBehaviour.Activate(this.gameObject, true);
            OnChangeVisibility?.Invoke(true);
            timer = TimerBeh.CreateTimer();
            timer.OnTimeChanged = (time) => { OnTimer(); };
            timer.OnEnd = () => 
            {
                OnTimerEnd?.Invoke();
                Reset();
            };
            timer.Launch(time);
        }

        public void Reset()
        {
            GameObject.Destroy(timer.gameObject);
            UberBehaviour.Activate(this.gameObject, false);
            OnChangeVisibility?.Invoke(false);
            bar.fillAmount = 0;
        }

        public void Click()
        {
            OnClick?.Invoke();
            Reset();
        }

        private void OnTimer()
        {
            bar.fillAmount = timer.TimeNormalized;
        }
    }
}