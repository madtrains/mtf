﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.DebugScreen
{
    public class DebugLogMessageCard : UberButton
    {
        public LogMessage LogMessage { get { return logMessage; } }

        [SerializeField] private Image icon;
        [SerializeField] private Color warnigColor;
        [SerializeField] private Color errorColor;
        private LogMessage logMessage;

        private bool setSize;
        private int updateCount;


        public void Init(CLickDelegate click, LogMessage logMessage, Sprite info, Sprite warning, Sprite error)
        {
            Init(click);
            this.logMessage = logMessage;
            this.ButtonLabel = logMessage.Text;
            if (logMessage.LogType == LogType.Error || logMessage.LogType == LogType.Exception)
            {
                icon.sprite = error;
                ButtonLabelColor = errorColor;
            }

            else if (logMessage.LogType == LogType.Warning)
            {
                icon.sprite = warning;
                ButtonLabelColor = warnigColor;
            }

            else
            {
                icon.sprite = info;
            }
        }


        protected override void Update()
        {
            base.Update();
            if (setSize && updateCount < 2)
            {
                CalculateSize();
                updateCount++;
            }
            else
            {
                setSize = false;
            }
        }


        public void SetSizeByText()
        {
            setSize = true;
            updateCount = 0;
        }


        private void CalculateSize()
        {
            float height = buttonLabel.preferredHeight + 14f;
            if (height < 72)
                return;

            this.RectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
        }
    }
}