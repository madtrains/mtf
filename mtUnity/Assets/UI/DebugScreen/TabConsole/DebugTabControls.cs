﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace MTUi.DebugScreen
{
    public delegate void Command();
    public delegate void CommandToggle(bool value);
    public delegate void CommandFloat(float value);
    public delegate void CommandInt(int value);
    public delegate void DropBox(int value);

    public partial class DebugTabControls : DebugTab
    {
        [SerializeField] private Elements.Button buttonRef;
        [SerializeField] private Elements.ButtonToggle buttonToggleRef;
        [SerializeField] private Elements.DropBox dropBoxRef;
        [SerializeField] private Elements.Slider sliderRef;
        [SerializeField] private Elements.ResourceButton resourceButtonRef;

        private List<Elements.Element> elements;
        public override void Init(int myIndex, Color activeColor, Color inactiveColor, Color disabledColor)
        {
            base.Init(myIndex, activeColor, inactiveColor, disabledColor);
            elements = new List<Elements.Element>();
        }

        public void DeleteAll()
        {
            for (int i = 0; i < elements.Count; i++)
            {
                elements[i].Delete();
            }
            elements = new List<Elements.Element>();
        }

        public void Set(Elements.Button button, DebugIcons icon, string text)
        {
            Sprite currentSprite = GameStarter.Instance.DebugScreen.DebugSprite(icon);
            button.Sprite = currentSprite;
            button.Text = text;
        }

        public Elements.Button CreateButton(DebugIcons icon, string text, Command command)
        {
            Sprite currentSprite = GameStarter.Instance.DebugScreen.DebugSprite(icon);
            Elements.Button newButton =
                UIElement.CloneUIElementByRef<Elements.Button>(
                    buttonRef, buttonRef.transform.parent, "button", true);

            Set(newButton, icon, text);
            newButton.Buton.onClick.AddListener(()=> { command(); });
            elements.Add(newButton);
            return newButton;
        }

        public Elements.ButtonToggle CrteateButtonToggle(string text, CommandToggle command, bool value)
        {
            Elements.ButtonToggle newButton =
                UIElement.CloneUIElementByRef<Elements.ButtonToggle>(
                    buttonToggleRef, buttonRef.transform.parent, "buttonToggle", true);
            newButton.ToggleValue = value;
            Set(newButton, DebugIcons.bulb, text);
            newButton.onChange += (v) => { command(v); };
            elements.Add(newButton);
            return newButton;
        }

        public Elements.DropBox CreatedDropBox(DebugIcons icon, string[] elements, DropBox dropBox, int currentValue=0)
        {
            Elements.DropBox newEl = 
                UIElement.CloneUIElementByRef<Elements.DropBox>(
                    dropBoxRef, buttonRef.transform.parent, "dropBox", true);
            
            if (elements != null && elements.Length > 0)
            {
                for (int i = 0; i < elements.Length; i++)
                {
                    newEl.DropDown.options.Add(new Dropdown.OptionData(elements[i]));
                }
            }
            if (currentValue != 0)
            {
                newEl.DropDown.value = currentValue;
            }
            newEl.DropDown.onValueChanged.AddListener((vl) => { dropBox(vl); });
            newEl.Sprite = GameStarter.Instance.DebugScreen.DebugSprite(icon);
            this.elements.Add(newEl);
            return newEl;
        }

        public Elements.Slider CreateSlider(DebugIcons icon, string text, float value,
            float minValue, float maxValue, bool wholeNumbers, CommandFloat cmd)
        {
            Elements.Slider newEl =
                UIElement.CloneUIElementByRef<Elements.Slider>(
                    sliderRef, sliderRef.transform.parent, "slider", true);
            newEl.Sprite = GameStarter.Instance.DebugScreen.DebugSprite(icon);

            newEl.Text = text;
            newEl.UISlider.value = value;
            newEl.Number = (float)System.Math.Round(value, 3);
            newEl.UISlider.minValue = minValue;
            newEl.UISlider.maxValue = maxValue;
            newEl.UISlider.wholeNumbers = wholeNumbers;
            newEl.UISlider.onValueChanged.AddListener((vl) =>
                {
                    newEl.Number = (float) System.Math.Round(vl, 3);
                    cmd(vl);
                });
            this.elements.Add(newEl);
            return newEl;
        }

        public Elements.ResourceButton CreateResourceButton(
            DebugIcons icon, string text)
        {
            Elements.ResourceButton newEl =
                UIElement.CloneUIElementByRef<Elements.ResourceButton>(
                    resourceButtonRef, resourceButtonRef.transform.parent, "resourceButton", true);
            newEl.Init();
            newEl.Text = text;
            newEl.Sprite = GameStarter.Instance.DebugScreen.DebugSprite(icon);
            this.elements.Add(newEl);
            return newEl;
        }
    }
}