﻿using UnityEngine;
namespace MTUi.DebugScreen
{
    public partial class DebugTabInfos : DebugTab
    {
        [SerializeField] private Elements.Info iforRef;

        public Elements.Info CreateInfo(string text)
        {
            Elements.Info newEl =
                UIElement.CloneUIElementByRef<Elements.Info>(
                    iforRef, iforRef.transform.parent, "info", true);
            newEl.Text = text;
            return newEl;
        }

        public Elements.Info CreateInfo(DebugIcons icon, string text)
        {
            Elements.Info newEl = CreateInfo(text);
            Sprite currentSprite = GameStarter.Instance.DebugScreen.DebugSprite(icon);
            newEl.Sprite = currentSprite;
            return newEl;
        }
    }
}