﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace MTUi.DebugScreen
{
    public partial class DebugTabConsole : DebugTab
    {
        public UnityEngine.Events.UnityAction OnChangeFilter;


        [SerializeField] private DebugLogMessageCard messageCardRef;
        [SerializeField] private DebugScreenButton buttonClear;
        [SerializeField] private DebugScreenButton buttonFilterInfos;
        [SerializeField] private DebugScreenButton buttonFilterWarnings;
        [SerializeField] private DebugScreenButton buttonFilterErrors;
        private List<DebugLogMessageCard> messages;


        public override void Init(int myIndex, Color activeColor, Color inactiveColor, Color disabledColor)
        {
            base.Init(myIndex, activeColor, inactiveColor, disabledColor);
            buttonClear.Init(
            () =>
            {
                foreach (var c in messages)
                {
                    c.SelfDestroy();
                }
                messages = new List<DebugLogMessageCard>();
            }, inactiveColor, inactiveColor);

            buttonFilterInfos.Init(
            () =>
            {
                if (OnChangeFilter != null)
                    OnChangeFilter();
            }, inactiveColor, disabledColor);


            buttonFilterWarnings.Init(
            () =>
            {
                if (OnChangeFilter != null)
                    OnChangeFilter();
            }, inactiveColor, disabledColor);
            

            buttonFilterErrors.Init(
            () =>
            {
                if (OnChangeFilter != null)
                    OnChangeFilter();
            }, inactiveColor, disabledColor);

            buttonFilterInfos.SetStateImmediately(true);
            buttonFilterWarnings.SetStateImmediately(true);
            buttonFilterErrors.SetStateImmediately(true);
            OnChangeFilter = SetCards;
        }

        public void CreateMessage(string text, LogType logType, string stackTrace)
        {
            if (messages == null)
                messages = new List<DebugLogMessageCard>();

            LogMessage newMessage = new LogMessage(text, logType, stackTrace);

            bool enabled = buttonFilterInfos.Toggle;
            if (logType == LogType.Warning)
                enabled = buttonFilterWarnings.Toggle;
            else if (logType == LogType.Error || logType == LogType.Exception)
                enabled = buttonFilterErrors.Toggle;

            System.Text.StringBuilder st = new System.Text.StringBuilder();
            st.Append(logType);
            st.Append(messages.Count);

            DebugLogMessageCard newDebugLogMessageCard =
                UIElement.CloneUIElementByRef<DebugLogMessageCard>(
                    messageCardRef,
                    messageCardRef.transform.parent,
                    st.ToString(),
                    enabled);

            newDebugLogMessageCard.transform.SetSiblingIndex(0);
            newDebugLogMessageCard.Init(() =>
                {
                    GameStarter.Instance.DebugScreen.ShowPopup(stackTrace);
                },
                newMessage,
                GameStarter.Instance.DebugScreen.DebugSprite(DebugIcons.info),
                GameStarter.Instance.DebugScreen.DebugSprite(DebugIcons.warning),
                GameStarter.Instance.DebugScreen.DebugSprite(DebugIcons.error));

            

            messages.Add(newDebugLogMessageCard);
            newDebugLogMessageCard.SetSizeByText();
        }


        public void SetCards()
        {
            foreach (DebugLogMessageCard message in messages)
            {
                message.SetSizeByText();
                if (message.LogMessage.LogType == LogType.Log)
                    Activate(message.gameObject, buttonFilterInfos.Toggle);

                else if (message.LogMessage.LogType == LogType.Warning)
                    Activate(message.gameObject, buttonFilterWarnings.Toggle);

                else if (message.LogMessage.LogType == LogType.Error || message.LogMessage.LogType == LogType.Exception)
                    Activate(message.gameObject, buttonFilterErrors.Toggle);
            }
        }
    }
}