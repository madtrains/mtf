﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MTUi
{
    public enum DebugIcons
    {
        bug,
        button,
        info,
        warning,
        error,
        back,
        bulb,
        coin,
        earth,
        finish,
        ninja,
        on,
        off,
        ok,
        palette,
        pause,
        person,
        phone,
        plus,
        reload,
        scienceHat,
        scroll,
        text,
        train,
        coach,
        trainSign,
        tv,
        passenger,
        mail,
        oil,
        iron,
        wood,
        fish,
        spain,
        finland,
        france,
        uk,
        germany,
        poland,
        russia
    }
}