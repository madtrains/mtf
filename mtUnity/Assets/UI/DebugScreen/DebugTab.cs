﻿using UnityEngine;
namespace MTUi.DebugScreen
{
    public partial class DebugTab : UberBehaviour
    {
        public DebugScreenButton EnabeButton { get { return enabeButton; } }

        [SerializeField] ContentHolder hideOnStart;
        [SerializeField] ContentHolder tabAdditionalContent;
        [SerializeField] protected DebugScreenButton enabeButton;
        private Color activeColor;
        private Color inactiveColor;
        private Color disabledColor;

        protected virtual void Start()
        {
            hideOnStart.IsActive = false;
        }

        public virtual void Init(int myIndex, Color activeColor, Color inactiveColor, Color disabledColor)
        {
            enabeButton.Init(() =>
            {
                GameStarter.Instance.DebugScreen.SwitchTab(myIndex);
            },
            inactiveColor, inactiveColor);

            this.activeColor = activeColor;
            this.inactiveColor = inactiveColor;
            this.disabledColor = disabledColor;
        }


        public bool IsActiveTab
        {
            set
            {
                tabAdditionalContent.IsActive = value;
                Activate(gameObject, value);
                Color color = inactiveColor;
                if (value)
                    color = activeColor;
                enabeButton.Background.color = color;
            }
        }
    }
}