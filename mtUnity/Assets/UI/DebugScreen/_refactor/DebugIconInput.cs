﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi
{
    public class DebugIconInput : UIElement
    {
        [SerializeField] private Image icon;
        [SerializeField] private Text label;
        [SerializeField] private InputField inputField;


        public void Init(UnityEngine.Events.UnityAction<float> onValue, Sprite icon, string label)
        {
            this.icon.sprite = icon;
            this.label.text = label;
            inputField.onEndEdit.AddListener((v) =>
            {
                float floatValue = float.Parse(v);
                onValue(floatValue);
            });
        }
    }
}