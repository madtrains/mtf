﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi
{
    public class DebugIconDropdown : UIElement
    {
        [SerializeField] private Image icon;
        [SerializeField] private Dropdown dropdown;


        public void Init(Sprite icon, UnityEngine.Events.UnityAction<int> onChange, string[] names)
        {
            this.icon.sprite = icon;
            dropdown.onValueChanged.AddListener(onChange);
            for (int i = 0; i < names.Length; i++)
            {
                dropdown.options.Add(new Dropdown.OptionData(names[i]));
            }
        }
    }
}