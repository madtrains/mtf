﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi
{
    public class DebugIconButton : UberButton
    {
        [SerializeField] private Image icon;

        public void Init(CLickDelegate click, Sprite icon, string label)
        {
            Init(click);
            this.icon.sprite = icon;
            ButtonLabel = label;
        }
    }
}