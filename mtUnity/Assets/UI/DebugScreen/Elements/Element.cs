﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.DebugScreen.Elements
{
    public class Element : UIElement
    {
        [SerializeField] protected Image icon;
        public Sprite Sprite { set { icon.sprite = value; } }
        public Color Color { set { SetColor(value); } }

        public void Delete()
        {
            Destroy(gameObject);
        }
        protected virtual void SetColor(Color color)
        {
            icon.color = color;
        }
    }
}