﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.DebugScreen.Elements
{
    public class Slider : Element
    {
        [SerializeField] protected Text text;
        [SerializeField] protected Text textNumbers;
        [SerializeField] protected UnityEngine.UI.Slider slider;
        public int IntValue { get { return (int)slider.value; } set { slider.value = value; } }

        public string Text { set { this.text.text = value; } }
        public float Number {set { this.textNumbers.text = value.ToString(); } }
        public UnityEngine.UI.Slider UISlider { get { return slider; } }
    }
}