﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.DebugScreen.Elements
{
    public class ButtonToggle : Button
    {
        public UnityEngine.Events.UnityAction<bool> onChange;
        [SerializeField] protected Image bulbOn;
        [SerializeField] protected bool toggleValue;


        public void Toggle()
        {
            ToggleValue = !ToggleValue;
        }

        public bool ToggleValue 
        {
            get { return toggleValue; }
            set 
            {
                toggleValue = value;
                bulbOn.enabled = value;
                if (onChange != null)
                    onChange(toggleValue);
            }
        }
    }
}