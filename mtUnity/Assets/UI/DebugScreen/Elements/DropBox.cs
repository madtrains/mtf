﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.DebugScreen.Elements
{
    public class DropBox : Element
    {
        public Dropdown DropDown { get { return dropdown; } }

        [SerializeField] private Dropdown dropdown;
    }
}