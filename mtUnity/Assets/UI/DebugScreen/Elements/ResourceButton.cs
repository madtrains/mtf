﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.DebugScreen.Elements
{
    public class ResourceButton : Element
    {
        public UnityEngine.Events.UnityAction<int> OnChange;
        [SerializeField] protected Text text;
        [SerializeField] protected UnityEngine.UI.Button minus;
        [SerializeField] protected UnityEngine.UI.Button zero;
        [SerializeField] protected UnityEngine.UI.Button plus;
        [SerializeField] protected UnityEngine.UI.Button plus10;
        [SerializeField] protected UnityEngine.UI.Button plus100;

        public string Text { set { this.text.text = value; } }

        public override void Init()
        {
            base.Init();
            zero.onClick.AddListener(() => { OnChange?.Invoke(0); });
            minus.onClick.AddListener(() => { OnChange?.Invoke(-1); });
            plus.onClick.AddListener(() => { OnChange?.Invoke(1); });
            plus10.onClick.AddListener(() => { OnChange?.Invoke(10); });
            plus100.onClick.AddListener(() => { OnChange?.Invoke(100); });
        }
    }
}