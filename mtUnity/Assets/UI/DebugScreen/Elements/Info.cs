﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.DebugScreen.Elements
{
    public class Info : Element
    {
        public string Text { get { return text.text; } set { text.text = value; } }
        public bool Toggle { set { Activate(toggle.gameObject, value); } }

        protected override void SetColor(Color color)
        {
            base.SetColor(color);
            toggle.color = color;
        }

        public void Set(string text, Color color, bool toggle=true)
        {
            Text = text;
            Color = color;
            Toggle = toggle;
        }

        [SerializeField] protected Text text;
        [SerializeField] protected Image toggle;
    }
}