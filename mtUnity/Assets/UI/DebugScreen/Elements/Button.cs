﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.DebugScreen.Elements
{
    public class Button : Element
    {
        [SerializeField] protected Text text;
        [SerializeField] protected UnityEngine.UI.Button button;

        public string Text { set { this.text.text = value; } }
        public UnityEngine.UI.Button Buton { get { return button; } }
    }
}