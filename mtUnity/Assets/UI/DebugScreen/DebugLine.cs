﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.DebugScreen
{
    public class DebugLine : UIElement
    {
        public string Logo { set { logo.text = value; } }
        public string Text { set { text.text = value; } }
        public float FLoatRoundedValue { set { text.text = System.Math.Round(value, 2).ToString(); } }
        public int IntValue { set { text.text = value.ToString(); } }
            
        [SerializeField] private Text logo;
        [SerializeField] private Text text;

        
    }
}