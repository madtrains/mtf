using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.DebugScreen
{
    public class DebugScreenButton : UberButton
    {
        public Image Background { get { return background; } }

        [SerializeField] protected Image background;
        protected Color colorActive;
        protected Color colorInactive;


        public Color BackgroundColor
        {
            set
            {
                if (background == null)
                {
                    Debug.LogErrorFormat("Can not set background color for object {0} Background Image is null", gameObject.name);
                    return;
                }
                background.color = value;
            }
        }


        public override void Init()
        {
            base.Init();
            if (background == null)
            {
                background = GetComponent<Image>();
            }   
        }


        public void Init(CLickDelegate @delegate, Color colorActive, Color colorInactive)
        {
            Init(@delegate);
            this.colorActive = colorActive;
            this.colorInactive = colorInactive;
        }


        public void SetStateImmediately(bool value)
        {
            Toggle = value;
            ApplyColorOnState();
        }


        private void ApplyColorOnState()
        {
            if (toggle)
                BackgroundColor = colorActive;
            else
                BackgroundColor = colorInactive;
        }


        protected override void ToggleCommand()
        {
            base.ToggleCommand();
            ApplyColorOnState();
        }
    }
}