﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MTUi.DebugScreen
{
    [System.Serializable]
    public class DebugIcon
    {
        public Sprite Icon { get { return sprite; } }

        [SerializeField] private Sprite sprite;
    }
}
