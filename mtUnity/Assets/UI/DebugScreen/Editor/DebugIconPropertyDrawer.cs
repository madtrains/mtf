﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace MTUi.DebugScreen
{
    [CustomPropertyDrawer(typeof(DebugIcon))]
    class DebugIconPropertyDrawer : UberPropertyDrawer
    {
        protected override void Draw(Rect position, SerializedProperty property, GUIContent label, Rect main)
        {
            base.Draw(position, property, label, main);
            int index = GetIndex(property);
            var current  = (DebugIcons)index;
            float labelWidth = main.width / 3f;
            Rect r1 = new Rect(main.x, main.y, (int)labelWidth, main.height); //name
            Rect r2 = new Rect(r1.max.x, main.y, (int)labelWidth * 2, main.height); //field
            GUI.Label(r1, current.ToString());
            SerializedProperty obj = property.FindPropertyRelative("sprite");
            //UnityEngine.Object uObject = obj.objectReferenceValue;
            EditorGUI.PropertyField(r2, obj, GUIContent.none, false);
        }
    }
}