﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace MTUi.DebugScreen
{
    public partial class DebugScreen : UberBehaviour
    {
        #region Actions
        public UnityEngine.Events.UnityAction<bool> OnDebugGodMode;
        public UnityEngine.Events.UnityAction OnDebugButtonRestorePoint;
        public UnityEngine.Events.UnityAction<bool> OnDebugButtonHorn;
        public UnityEngine.Events.UnityAction<bool> OnDebugButtonSlower;
        public UnityEngine.Events.UnityAction<bool> OnDebugButtonNormal;
        public UnityEngine.Events.UnityAction<bool> OnDebugButtonFaster;
        public UnityEngine.Events.UnityAction<bool> OnDebugButtonA;
        #endregion

        #region Get/Set
        public bool Bkg { set { Activate(bkg, value); } }
        public DebugTabConsole Console { get { return tabConsole; } }
        public DebugTabControls Controls { get { return tabControls; } }
        public DebugTabInfos Infos { get { return tabInfos; } }
        #endregion

        #region Fields
        [SerializeField] private DebugScreenButton dockButton;
        [SerializeField] private Slider transparencySlider;
        [SerializeField] private Color activeColor;
        [SerializeField] private Color inactiveColor;
        [SerializeField] private Color disabledColor;
        [SerializeField] private DebugTabConsole tabConsole;
        [SerializeField] private DebugTabControls tabControls;
        [SerializeField] private DebugTabInfos tabInfos;
        [SerializeField] private DebugIcon[] icons;
        [SerializeField] private ContentHolders mainHolders;
        [SerializeField] private DebugScreenButton popUpButton;
        [SerializeField] private Text popUpText;
        [SerializeField] private GameObject popUp;
        [SerializeField] private Text fpsText;
        [SerializeField] private GameObject godModeIndicator;
        [SerializeField] private CanvasGroup tabCanvasGroup;
        [SerializeField] private RectTransform[] dockPanels;
        [SerializeField] private Vector2 dockUp;
        [SerializeField] private Vector2 dockDown;
        [SerializeField] private GameObject bkg;
        [SerializeField] private DebugScreenButton exit;

        [SerializeField] private RectTransform mainRect;
        [SerializeField] private RectTransform[] mainDocks;


        private List<DebugTab> tabs;
        private bool enabledToggle;
        private bool dockToggle;
        private float fpsDeltaTime;
        private bool godModeValue;
        private int dockIndex;
        #endregion

        #region Publics
        public void DockStep()
        {
            dockIndex += 1;
            if (dockIndex == mainDocks.Length)
                dockIndex = 0;
            mainRect.transform.SetParent(mainDocks[dockIndex].transform, false);
            mainRect.anchoredPosition = Vector2.zero;
        }


        public Sprite DebugSprite(DebugIcons di)
        {
            int index = (int)di;
            return icons[index].Icon;
        }

        public void ShowPopup(string text)
        {
            Activate(popUp, true);
            popUpText.text = text;
        }

        public void Init()
        {
            exit.Init(() => { Application.Quit();});
            mainRect.transform.SetParent(mainDocks[dockIndex].transform, false);
            mainRect.anchoredPosition = Vector2.zero;
            popUpButton.Init(
            () =>
            {
                Activate(popUp, false);
            });

                dockButton.Init(
                () =>
                {
                    dockToggle = !dockToggle;
                    //hide
                    if (dockToggle)
                    {
                        foreach (var dp in dockPanels)
                            dp.anchoredPosition = dockDown;
                    }
                    //show
                    else
                    {
                        foreach (var dp in dockPanels)
                            dp.anchoredPosition = dockUp;
                    }
                },
                inactiveColor, inactiveColor);

                transparencySlider.onValueChanged.AddListener(ChangeTransparency);
                tabs = new List<DebugTab>();
                tabs.Add(tabConsole);
                tabs.Add(tabControls);
                tabs.Add(tabInfos);
                for (int i = 0; i < tabs.Count; i++)
                {
                    tabs[i].Init(i, activeColor, inactiveColor, disabledColor);
                    tabs[i].IsActiveTab = (i == 1);
                }
        }

        public void SwitchTab(int index)
        {
            for (int i = 0; i < tabs.Count; i++)
            {
                bool isActiveTab = i == index;
                tabs[i].IsActiveTab = isActiveTab;
            }
        }

        public void LadybugToggle()
        {
            enabledToggle = !enabledToggle;
            LadyBug(enabledToggle);
        }

        public void LadyBug(bool value)
        {
            if (value)
            {
                mainHolders.ActivateAll();
                SetCards();
            }

            else
            {
                mainHolders.DeactivateAll();
            }
        }

        public void LadyBugOpen(int tab)
        {
            LadyBug(true);
            this.SwitchTab(tab);
        }
        #endregion

        #region Privates
        private void Update()
        {
            fpsDeltaTime += (Time.deltaTime - fpsDeltaTime) * 0.1f;
            float msec = fpsDeltaTime * 1000.0f;
            float fps = 1.0f / fpsDeltaTime;
            fps = Mathf.Round(fps);
            //string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
            if (fps < GameStarter.Instance.PauseOnFps)
                Debug.Break();
            fpsText.text = fps.ToString();
            if (Input.GetButtonDown("DebugButtonHorn"))
            {
                if (OnDebugButtonHorn != null)
                    OnDebugButtonHorn(true);
            }

            if (Input.GetButtonUp("DebugButtonHorn"))
            {
                if (OnDebugButtonHorn != null)
                    OnDebugButtonHorn(false);
            }

            if (Input.GetButtonDown("GodMode"))
            {
                godModeValue = !godModeValue;
                Activate(godModeIndicator, godModeValue);
                if (OnDebugGodMode != null)
                    OnDebugGodMode(godModeValue);
            }

            if (Input.GetButtonDown("DebugButtonRestorePoint"))
            {
                if (OnDebugButtonRestorePoint != null)
                    OnDebugButtonRestorePoint();
            }

            if (Input.GetButtonDown("DebugButtonSlower"))
            {
                if (OnDebugButtonSlower != null)
                    OnDebugButtonSlower(true);
            }

            if (Input.GetButtonUp("DebugButtonNormal"))
            {
                if (OnDebugButtonNormal != null)
                    OnDebugButtonNormal(false);
            }

            if (Input.GetButtonUp("DebugButtonFaster"))
            {
                if (OnDebugButtonFaster != null)
                    OnDebugButtonFaster(false);
            }

            if (Input.GetButtonDown("DebugButtonA"))
            {
                if (OnDebugButtonA != null)
                    OnDebugButtonA(false);
            }
        }

        private void SetCards()
        {
            tabConsole.SetCards();
        }     
        
        private void SetGodMode(bool godMode)
        {
            Activate(this.godModeIndicator, godMode);
        }

        private void ChangeTransparency(float value)
        {
            tabCanvasGroup.alpha = value;
        }
        #endregion
    }


    [System.Serializable]
    public class LogMessage
    {
        public LogMessage(string text, LogType logType, string stackTrace)
        {
            this.text = text;
            this.logType = logType;
            this.stackTrace = stackTrace;
        }


        public string Text { get { return text; } }
        public LogType LogType { get { return logType; } }
        public string StackTrace { get { return stackTrace; } }

        private string text;
        private LogType logType;
        private string stackTrace;
    }
}