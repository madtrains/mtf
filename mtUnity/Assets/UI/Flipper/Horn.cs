﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
namespace MTUi.Flipper
{
    public class Horn : UIElement
    {
        public enum HornColors { Green, Yellow, Red };
        [SerializeField] private Image bg;
        [SerializeField] private Image cover;
        [SerializeField] private float offset;
        [SerializeField] private Color[] colors;
        //[SerializeField] private ContentHolders switcher;

        private float value;
        private float savedOffset;


        public override void Init()
        {
            base.Init();
            savedOffset = cover.rectTransform.anchoredPosition.x;
        }

        public void Resize(int newWidth)
        {
            offset = newWidth;
            this.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, newWidth);
        }

        public void Set(float value, HornColors color)
        {
            Value = value;
            int intValue = (int)color;
            bg.color = colors[intValue];
            //switcher.Activate(intValue);
        }

        public Color Color { set { bg.color = value; } }
        public float Value
        {
            get { return value; }
            private set
            {
                this.value = value;
                float x = Mathf.Lerp(savedOffset, offset, value);
                cover.rectTransform.anchoredPosition = 
                    new Vector2(x, cover.rectTransform.anchoredPosition.y);
            }
        }
    }
}