﻿using UnityEngine;
using UnityEngine.UI;
namespace MTUi.Flipper
{
    public class Speedometer : UIElement
    {
        public void SetSpeed(float speed)
        {
            float kmph = speed * 3.6f;
            speedValue.text = string.Format("{0:0}", kmph);
            float t = Mathf.InverseLerp(0, GameStarter.GameParameters.TrainShared.MaxSpeed, speed);
            if (reverseAnglesOrder)
                t = 1f - t;
            float angle = angles.Lerp(t);
            arrow.localEulerAngles = Vector3.forward * angle;
        }

        [SerializeField] private MinMax angles;
        [SerializeField] private bool reverseAnglesOrder;
        [SerializeField] private float speedMax;
        [SerializeField] private RectTransform arrow;
        [SerializeField] private Text speedValue;
    }
}