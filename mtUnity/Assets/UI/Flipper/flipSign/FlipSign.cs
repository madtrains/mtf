﻿using UnityEngine;
namespace MTUi.Flipper
{
    public class FlipSign : UIElement
    {
        public enum Colors { Red, Yellow, Green, Off }
        [SerializeField] private ContentHolders holders;

        //[SerializeField] private Animator animator;
        //public readonly int hEnabled = Animator.StringToHash("enabled");

        public Colors Color
        { 
            set 
            {
                int intValue = (int)value;
                //animator.SetBool(hEnabled, intValue > 0);
                holders.Activate(intValue);
            }
        }
    }
}