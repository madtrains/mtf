﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
namespace MTUi.Flipper
{
    public class CogsHitPointsCounter : UIElement
    {
        public UnityAction<int> OnResize;

        [SerializeField] private HorizontalLayoutGroup _layoutGroup;
        [SerializeField] private Cog[] cogs;
        [SerializeField] private GameObject gearDockDown;

        public override void Init()
        {
            base.Init();
            foreach(Cog cog in cogs)
            {
                cog.Init();
            }
        }

        protected override void Start()
        {
            base.Start();
            LayoutRebuilder.ForceRebuildLayoutImmediate(RectTransform);
            if (cogs != null)
            {
                foreach (Cog cog in cogs)
                {
                    cog.CreateSplines(gearDockDown);
                    //LayoutRebuilder.ForceRebuildLayoutImmediate(cog.RectTransform);
                    //Debug.Log(cog.transform.position);
                }
            }
            OnResize?.Invoke(Mathf.RoundToInt(this.RectTransform.sizeDelta.x));
        }

        public void SetActiveCogs(int count)
        {
            for(int i = 0; i < cogs.Length; i ++)
            {
                bool active = i < count;
                cogs[i].IsActive(active);
            }
        }

        public void AssignCogs(int count, GameObject gearDockDown)
        {
            for (int i = 0; i < cogs.Length; i++)
            {
                Activate(cogs[i].gameObject, i < count);
            }
            
            switch(count)
            {
                case 2:
                {
                    _layoutGroup.spacing = 66;
                    _layoutGroup.padding = new RectOffset(67, 67, 0, 0);
                    break;
                }
                case 3:
                {
                    _layoutGroup.spacing = 36;
                    _layoutGroup.padding = new RectOffset(36, 36, 0, 0);
                    break;
                }
                case 4:
                {
                    _layoutGroup.spacing = 24;
                    _layoutGroup.padding = new RectOffset(24, 24, 0, 0);
                    break;
                }
                default:
                {
                    _layoutGroup.spacing = 24;
                    _layoutGroup.padding = new RectOffset(12, 12, 0, 0);
                    break;
                }
            }

            LayoutRebuilder.ForceRebuildLayoutImmediate(Root.Flipper.RectTransform);
        }
    }
}