﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
namespace MTUi.Flipper
{
    public class Counter : UIElement
    {
        //public int Coins { set { coins.text = value.ToString(); } }
        public int CogCoins { set { cogCoins.text = value.ToString(); } }

        //[SerializeField] private Text coins;
        [SerializeField] private Text cogCoins;
        [SerializeField] private CoachCounter coachCounter;
        [SerializeField] private RectTransform shadowsDock;
        List<CoachCounter> coachCounters;
        List<RectTransform> shadows;

        public CoachCounter AddCoachCounter()
        {
            if (coachCounters == null) { coachCounters = new List<CoachCounter>(); }
            if (shadows ==  null) { shadows = new List<RectTransform>(); }

            CoachCounter result =
                CloneUIElementByRef<CoachCounter>(coachCounter, coachCounter.transform.parent, "coachCounter", true);
            
            coachCounters.Add(result);
            return result;
        }

        public void ResetDelete()
        {
            //Coins = 0;
            CogCoins = 0;
            if (coachCounters == null || shadows == null)
            {
                return;
            }

            foreach (CoachCounter coachCounter in coachCounters)
            {
                Destroy(coachCounter.gameObject);
            }
            coachCounters = new List<CoachCounter>();

            foreach (RectTransform shadow in shadows)
            {
                Destroy(shadow.gameObject);
            }
            shadows.Clear();
        }

        public override void Init()
        {
            base.Init();
        }
    }
}