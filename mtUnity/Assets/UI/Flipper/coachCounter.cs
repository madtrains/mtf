﻿using System.Text;
using UnityEngine;
using UnityEngine.UI;
namespace MTUi.Flipper
{
    public class CoachCounter : UIElement
    {
        public enum Mode { Capacity, CapacityLoadUnload, Unlimited }

        [SerializeField] private Text text;
        [SerializeField] private ContentSwitcher resourceSwitcher;
        [SerializeField] private RectTransform cap;
        [SerializeField] private RectMask2D maskYellow;
        [SerializeField] private RectMask2D maskRed;
        private int capacity;

        public void SetStartValues(int resourceID, Mode mode, int capacity=0)
        {
            float full = maskYellow.rectTransform.sizeDelta.x;

            switch (mode)
            {
                case Mode.Capacity:
                {
                    this.capacity = capacity;
                    Activate(maskYellow.gameObject, true);
                    Activate(maskRed.gameObject, false);
                    Activate(cap.gameObject, false);
                    SetYellow(0);
                    SetText(0, " / ", capacity);
                    break;
                }

                case Mode.CapacityLoadUnload:
                {
                    this.capacity = capacity;

                    Activate(maskYellow.gameObject, true);
                    Activate(maskRed.gameObject, true);
                    Activate(cap.gameObject, false);
                    SetYellow(0);
                    SetRed(0, 0);
                    SetText(0, " / ", capacity);
                    break;
                }

                case Mode.Unlimited:
                {
                    Activate(maskYellow.gameObject, false);
                    Activate(maskRed.gameObject, false);
                    Activate(cap.gameObject, true);
                    SetText(0);
                    break;
                }
            }

            this.resourceSwitcher.Activate(resourceID);
        }

        public void UpdateValueAndSlider(int loaded)
        {
            SetYellow(loaded);
            SetText(loaded, " / ", this.capacity);
        }

        public void UpdateValueAndSlider(int loaded, int unload)
        {
            UpdateValueAndSlider(loaded);
            SetRed(unload, loaded);
        }

        public void UpdateValue(int loaded)
        {
            SetText(loaded);
        }

        private void SetYellow(int value)
        {
            float size = maskYellow.rectTransform.rect.width;
            float result = (size * (float)value) / (float)capacity;
            result = size - result;
            //x - left z - right  w - top y - bottom
            maskYellow.padding = 
                new Vector4(
                    maskYellow.padding.x, //left
                    maskYellow.padding.y, // bottom
                    result,               //right
                    maskYellow.padding.w); // top
        }

        private void SetRed(int value, int mainValue)
        {
            float size = maskYellow.rectTransform.rect.width - maskYellow.padding.z;
            float result = (size * (float)value) / (float)mainValue;
            result = size - result;
            //result = Mathf.Abs(maskYellow.padding.y + result);
            //x - left z - right  w - top y - bottom
            maskRed.padding =
                new Vector4(
                    result,
                    maskRed.padding.y,
                    maskYellow.padding.z,  //от жёлтого
                    maskRed.padding.w);
        }


        private void SetText(params System.Object[] objs)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var ob in objs)
                sb.Append(ob);
            this.text.text = sb.ToString();
            
        }
    }
}