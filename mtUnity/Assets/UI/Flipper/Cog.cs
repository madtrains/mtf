﻿using UnityEngine;
namespace MTUi.Flipper
{
    public class Cog : UIElement
    {
        public override void Init()
        {
            base.Init();
        }

        public void CreateSplines(GameObject dockDown)
        {
            floatingGear.Init();
            floatingGear.SetDownDock(dockDown);
            floatingGear.CreateSplines();
        }

        public void IsActive(bool isActive)
        {
            //switcher.Activate(isActive);
            floatingGear.SetAnimate(!isActive);
        }

        [SerializeField] private ContentHolders switcher;
        [SerializeField] private FloatingGear floatingGear;
    }
}