﻿using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.Flipper
{
	public class Flipper : AnimUIElement
	{
        private enum ClickNTapType { RayCast, Short, Long }

        public static int hResurrect = Animator.StringToHash("resurrect");
		public static int hCoachCounter = Animator.StringToHash("coachCounter");

        #region Actions
        public UnityEngine.Events.UnityAction OnPress;
		public UnityEngine.Events.UnityAction OnTap;
		public UnityEngine.Events.UnityAction OnDoubleTap;
		public UnityEngine.Events.UnityAction<bool> OnTapHold;


        public UnityEngine.Events.UnityAction OnResurrect_Continue_Coins;
		public UnityEngine.Events.UnityAction OnResurrect_Continue_Tickets;
		public UnityEngine.Events.UnityAction OnResurrect_Return;
        #endregion

        #region Getters
        public DebugTable DebugTable { get { return debugInfo; } }

        public int Coins
		{
			set
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append("-");
				stringBuilder.Append(value);
				string r = stringBuilder.ToString();
				foreach (Text text in coinsMinus)
				{
					text.text = r;
				}
			}
		}

		public int CogCoins
		{
			set
			{

				cogs.text = value.ToString();
				StringBuilder stringBuilder = new StringBuilder();
				if (value > 0)
					stringBuilder.Append("-");
				stringBuilder.Append(value);
				string r = stringBuilder.ToString();
				foreach (Text text in cogsMinus)
				{
					text.text = r;
				}
			}
		}

		public int Tickets { set { tickets.text = value.ToString(); } get { return int.TryParse(tickets.text, out int value) ? value : 0; } }

		public CoachCounter CoachCounter { get { return coachCounter; } }

		public Horn Horn { get { return horn; } }

		public float Speed { set { speedometer.SetSpeed(value); } }
		#endregion

		#region Serializeble Fields
		[SerializeField] private RectTransform[] tapIgnore; //ректанглы, где игнорируется тап
		[SerializeField] private Horn horn;
		[SerializeField] private CogsHitPointsCounter cogsCounter;
		[SerializeField] private GameObject gearDockDown;
		[SerializeField] private FlipSign flipSign_l;
		[SerializeField] private FlipSign flipSign_r;

		[SerializeField] private CoachCounter coachCounter;
		[SerializeField] private Speedometer speedometer;
		[SerializeField] private WayProgress wayProgress;
		[SerializeField] private UberButton resurrect_continue_coins;
		[SerializeField] private UberButton resurrect_continue_tickets;
		[SerializeField] private UberButton resurrect_return;
		[SerializeField] private TimerBeh timerBeh;
		[SerializeField] private Gift.Gift gift;
		[SerializeField] private DebugTable debugInfo;

        private UnityEngine.Events.UnityAction OnRessurect;

		[SerializeField] private Text cogs;
		[SerializeField] private Text tickets;

		[SerializeField] private Text[] cogsMinus;
		[SerializeField] private Text[] coinsMinus;
		[SerializeField] private Disabler ticketResurrectionButton;

        #endregion

        #region Privates
        private bool isBLocked;        //запрет на касание 
		private bool stoffDisabled;
		private float lastClickTime;
		private ClickNTapType clickNTapType;
		#endregion

		#region Methods
		public override void Init()
		{
			base.Init();

            debugInfo.Init(10);
            debugInfo.IsActive =
				GameStarter.Instance.DebugManager.DebugSettings.isActive && 
				GameStarter.Instance.DebugManager.DebugSettings.FlipperAdditionalInfo;

			horn.Init();
			horn.Set(1, Horn.HornColors.Green);

			resurrect_continue_coins.Init(() =>
			{
				animator.SetBool(hResurrect, false);
				UnBlock();
				OnRessurect = OnResurrect_Continue_Coins;
			});

			resurrect_continue_tickets.Init(() =>
			{
				animator.SetBool(hResurrect, false);
				UnBlock();
				OnRessurect = OnResurrect_Continue_Tickets;
			});

			resurrect_return.Init(() =>
			{
				animator.SetBool(hResurrect, false);
				UnBlock();
				OnRessurect = OnResurrect_Return;
			});
			Root.OnChangeTickets += (value) =>
			{
				Tickets = value;
				ticketResurrectionButton.Enable = value > 0;
			};
			cogsCounter.OnResize += (value) =>
			{
				Horn.Resize(value);
			};
			this.gift.OnAnimationPlayed = () => { Activate(this.gift.gameObject, false); };
		}

		public int ActiveHitpoints { set { cogsCounter.SetActiveCogs(value); } }

        public void Ticket()
		{
            Activate(this.gift.gameObject, true);
            this.gift.Play(Gift.Collection.Ticket, 1, false, 1.5f);
		}

        public void GiftCoin(int value)
        {
            Activate(this.gift.gameObject, true);
            this.gift.Play(Gift.Collection.Coin, value, false, 2f);
        }

        public void SetInfo(bool flippable, bool doubleFlipEnabled, bool stoffDisabled)
		{
			if (flippable)
			{
				//green
				if (doubleFlipEnabled) 
				{ 
					flipSign_l.Color = FlipSign.Colors.Green;
                    flipSign_r.Color = FlipSign.Colors.Off;
				}
				//yellow
				else
				{
					flipSign_l.Color = FlipSign.Colors.Yellow;
                    flipSign_r.Color = FlipSign.Colors.Yellow;
                }
			}
			//red
			else
			{
                flipSign_l.Color = FlipSign.Colors.Off;
                flipSign_r.Color = FlipSign.Colors.Red;
            }
			
			this.stoffDisabled = stoffDisabled;
        }

		public void SetWayProgress(int currentStationIndex, float lerpedPosition)
		{
			this.wayProgress.Set(lerpedPosition, currentStationIndex);
		}

		public void Prepare(MTParameters.Train trainParameters)
		{
			cogsCounter.AssignCogs(trainParameters.HitPoints, this.gearDockDown);
		}

		public void Prepare(WaySystem.WayRootFlipper wayRoot)
		{
			wayProgress.Create(wayRoot.BlockManagersCount);
			CogCoins = 0;
		}

		/// <summary>
		/// показывает табличку загрузки вагона и убирает её через указанное время
		/// </summary>
		/// <param name="timer"></param>
		public void CoachCounterTrigger(float timer)
		{
			animator.SetBool(hCoachCounter, true);
            timerBeh.Launch(timer);
            timerBeh.OnEnd = () => { animator.SetBool(hCoachCounter, false); };
		}

		public void Block()
		{
			isBLocked = true;
		}

		public void UnBlock()
		{
			isBLocked = false;
		}

		public void EventResurrected()
		{
			OnRessurect?.Invoke();
		}

		public void ShowRestart()
		{
			animator.SetBool(hResurrect, true);
			Block();
		}

		public override void Reset()
		{
			base.Reset();
			horn.Set(1, Horn.HornColors.Green);
			Coins = 0;
			wayProgress.Reset();
        }

		public void CheckPoint()
		{

		}

		//используется для анимации счётчика монеток (пока не поддерживается интерфейсом)
		public void AddCoinEvent()
		{

		}

		public void ReceiveAbsoluteSpeed(float absoluteSpeed)
		{
			speedometer.SetSpeed(absoluteSpeed);
        }

        protected override void ShowHide(bool state)
        {
            base.ShowHide(state);
			if (state)
			{
                //Root.ClickTap.HoldThreshold = GameStarter.GameParameters.FlipperParameters.HornThreshold;
                Root.ClickTap.OnPress = MainTapPress;
                Root.ClickTap.OnHold = MainTapHold;
                Root.ClickTap.OnRelease = MainTapRelease;
                Root.ClickTap.OnRaycast = MainTapRaycast;
            }
			else
			{
                Root.ClickTap.OnPress = null;
                Root.ClickTap.OnHold = null;
                Root.ClickTap.OnRelease = null;
                Root.ClickTap.OnRaycast = null;
            }
        }

		private void MainTapRaycast(MTCore.RayCastTap rayCastTap)
		{
            clickNTapType = ClickNTapType.RayCast;
        }

        private void MainTapPress(Vector2 coordinates)
		{
			clickNTapType = ClickNTapType.Short;
			OnPress?.Invoke();
        }

		private void MainTapHold(Vector2 currentCoordinates, float time)
		{
			if (stoffDisabled)
			{
                Root.ClickTap.Cancel();
				return;
            }

			if (time > GameStarter.GameParameters.FlipperParameters.HornThreshold)
			{
                OnTapHold(true); //включаем гудок
                clickNTapType = ClickNTapType.Long;
            }
		}

		private void MainTapRelease(Vector2 pressCoordinates, Vector2 releaseCoordinates, float time)
		{
			//OnTap?.Invoke();

            if(clickNTapType == ClickNTapType.Short)
            {
                float twoClicksDelta = Time.realtimeSinceStartup - lastClickTime;
                if(twoClicksDelta <=
                    GameStarter.GameParameters.FlipperParameters.DoubleTapThreshold)
                {
                    OnDoubleTap();
                }
                else
                {
					OnTap();
                    lastClickTime = Time.realtimeSinceStartup;
                }
            }
            else if(clickNTapType == ClickNTapType.Long)
            {
                OnTapHold(false);
            }
        }
        #endregion
    }

    [System.Serializable]
	public class Disabler
	{
		public bool Enable
		{ 
			set 
			{
				image.color = value ? enabled : disabled;
				button.ButtonComponent.interactable = value;
			}
		}

        [SerializeField] private Image image;
        [SerializeField] private UberButton button;
        [SerializeField] private Color enabled;
        [SerializeField] private Color disabled;
    }
}