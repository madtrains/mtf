﻿using UnityEngine;

namespace MTUi.Flipper
{

    public class PostFlipper : AnimUIElement
	{
        public static int hWhistle = Animator.StringToHash("whistle");

        public UnityEngine.Events.UnityAction OnWhistleButton;
        public Bubble Bubble { get { return bubble; } }

        [SerializeField] ScorePanel.ScorePanel scorePanel;
        [SerializeField] private MTUi.TasksPanel.TasksPanel tasksPanel;
        [SerializeField] private PlayWhistleButton playButton;
        [SerializeField] private Bubble bubble;
        private bool whistle;

        public override void Init()
        {
            base.Init();
            playButton.Init(() => { OnWhistleButton?.Invoke(); });
            playButton.Set(PlayWhistleButton.PlayButtonModes.Enabled);
            
        }

        protected override void ShowHide(bool state)
        {
            if(state)
            {
                OnCommandShow?.Invoke();
                Activate(this.gameObject, state);
                scorePanel.Show();
                tasksPanel.Show();
                animator.SetBool(Hash_ON, true);
                Root.Tasks.OnHidden = Show;
                Root.Dialog.OnHidden = Show;
                animator.SetBool(hWhistle, whistle);
            }
            else //hide
            {
                OnCommandHide?.Invoke();
                scorePanel.Hide();
                tasksPanel.Hide();
                animator.SetBool(Hash_ON, false);
                
            }
        }

        public bool Whistle
        {
            set
            {
                whistle = value;
                animator.SetBool(hWhistle, whistle);
            }
        }
    }
}
