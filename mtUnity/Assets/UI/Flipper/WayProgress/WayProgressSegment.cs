﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
namespace MTUi.Flipper
{
    public class WayProgressSegment : UIElement
    {
        public void SetAsFirst()
        {
            Activate(first, true);
        }


        public void Set(float value)
        {
            slider.value = value;
        }

        public void Set(bool lamp)
        {
            this.lamp.Activate(lamp);
        }


        public void Reset()
        {
            Activate(first, false);
            lamp.Activate(false);
            slider.value = 0;
        }


        [SerializeField] private GameObject first;
        [SerializeField] private ContentHolders lamp;
        [SerializeField] private Slider slider;
    }
}