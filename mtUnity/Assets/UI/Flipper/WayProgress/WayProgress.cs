﻿using System.Text;
using UnityEngine;
namespace MTUi.Flipper
{
    public class WayProgress : UIElement
    {
        [SerializeField] private WayProgressSegment refSegment;
        private WayProgressSegment[] segments;
        private int index;

        public void Set(float value, int index)
        {
            segments[index].Set(value);
            if (index > this.index)
            {
                segments[this.index].Set(true);
                this.index = index;
                //this.index = Mathf.Clamp(this.index, 0, segments.Length - 1);
            }
        }

        public void Create(int count)
        {
            segments = new WayProgressSegment[count];
            for (int i = 0; i < count; i++)
            {
                StringBuilder stringBuilder = new StringBuilder();
                int realIndex = count - i - 1;
                stringBuilder.Append("Segment_");
                stringBuilder.Append(realIndex);
                WayProgressSegment current =
                    CloneUIElementByRef<WayProgressSegment>(refSegment, refSegment.transform.parent, stringBuilder.ToString(), true);
                if (realIndex == 0)
                {
                    current.SetAsFirst();
                }
                segments[realIndex] = current;
            }
        }

        public void Reset()
        {
            if (segments != null && segments.Length > 0)
            {
                for (int i = 0; i < segments.Length; i++)
                {
                    GameObject.Destroy(segments[i].gameObject);
                }
            }
            
        }
    }
}