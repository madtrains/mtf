﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
namespace MTUi.Flipper
{
    [CustomEditor(typeof(CoachCounter))]
    public class CoachCounterEditor : UberEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (GUILayout.Button("TestSet"))
            {
                CoachCounter coachCounter = (CoachCounter)target;
                coachCounter.SetStartValues(2, CoachCounter.Mode.CapacityLoadUnload, 20);
                coachCounter.UpdateValueAndSlider(16, 15);
            }
            
        }
    }
}