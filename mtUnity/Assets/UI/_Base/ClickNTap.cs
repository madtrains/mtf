using UnityEngine;

namespace MTUi
{

    public class ClickNTap
    {
        public bool IsPressed { get { return isPressed; } }
        //public float HoldThreshold { set { timeThreshold = value; } }
        //public float SwipeThreshold { set { swipeThreshold = value; } }

        public System.Action<Vector2> OnPress;
        public System.Action<Vector2, float> OnHold;
        public System.Action<Vector2, Vector2, float> OnRelease;
        public System.Action<MTCore.RayCastTap> OnRaycast;

        private float time;
        private Vector2 pressCoordinates;
        private bool isPressed;


        public void Press(Vector2 coordinates)
        {
            time = 0f;

            // ����������� ��������� � ���������
            MTCore.RayCastTap rayCastTap =
                GameStarter.Instance.MainComponents.GameCamera.RaycastComponent<MTCore.RayCastTap>(coordinates);

            // ��������� � ��������� (���������, �������������)
            if(rayCastTap != null)
            {
                rayCastTap.Tap();
                OnRaycast?.Invoke(rayCastTap);
                Cancel();
                return;
            }
            else
            {
                this.pressCoordinates = coordinates;
                OnPress?.Invoke(coordinates);
                isPressed = true;
                return;
            }
        }

        public void Release(Vector2 coordinates)
        {
            time = 0f;
            this.isPressed = false;
            OnRelease?.Invoke(pressCoordinates, coordinates, time);            
            //lastRelease = Time.realtimeSinceStartup;
        }

        public void Cancel()
        {
            isPressed = false;
            time = 0;
        }

        public void Hold(Vector2 coordinates)
        {
            if(!isPressed)
                return;

            time += Time.deltaTime;
            OnHold?.Invoke(coordinates, time);
        }
    }
}