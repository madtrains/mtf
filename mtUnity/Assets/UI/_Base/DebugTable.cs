﻿using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi
{
	[System.Serializable]
	public class DebugTable
	{
        [SerializeField] private Text text;
        [SerializeField] private GameObject root;
        private DebugTableString[] dts;
		private bool _active;

        public bool IsActive { set { _active = value; UberBehaviour.Activate(root, value); } }

		public void Init(int count)
		{
			dts = new DebugTableString[count];
			for (int i = 0; i < count; i++)
			{
				dts[i] = new DebugTableString();
			}
		}

		public void Set(int index, params object[] values)
		{
			if (!_active)
				return;
			dts[index].Set(values);
		}

		public void Print()
		{
            if (!_active)
                return;
            StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < dts.Length; i++)
			{
				if (i > 0)
					stringBuilder.Append("\n");
				stringBuilder.Append(dts[i].ToString());
			}
			this.text.text = stringBuilder.ToString();
		}
	}

	public class DebugTableString
	{
		public void Set(params object[] values)
		{
			this.values = values;
		}

		public new string ToString()
		{
            StringBuilder stringBuilder = new StringBuilder();
			if (values != null)
			{
                foreach (object value in values)
                {
                    stringBuilder.Append(value);
                }
            }
            return stringBuilder.ToString();
        }

		private object[] values;

	}
}