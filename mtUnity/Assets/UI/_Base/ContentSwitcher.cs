﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class ContentHolder
{
	public string Name { get { return name; } }
	public ContentHolder(GameObject[] gameobjects, string name)
	{
		this.name = name;
        this.gameobjects = gameobjects;
    }

    public bool IsActive
    {
        set
        {
			if(gameobjects == null || gameobjects.Length == 0)
				return;
            foreach (GameObject go in gameobjects)
            {
                UberBehaviour.Activate(go, value);
            }
        }
    }

    public void Delete()
    {
        foreach (GameObject go in gameobjects)
        {
            GameObject.Destroy(go);
        }
    }

    [SerializeField] protected string name;
    [SerializeField] protected GameObject[] gameobjects;
}


[System.Serializable]
public class ContentHolders
{
	#region Statica

	public static void DeactivateExcept(ContentHolder[] content, int index)
	{
		for (int i = 0; i < content.Length; i++)
		{
			if (i != index)
			{
				content[i].IsActive = false;
			}
		}
	}


	public static void ActivateOnly(ContentHolder[] content, int index)
	{
		for (int i = 0; i < content.Length; i++)
		{
			if (i == index)
			{
				content[i].IsActive = true;
			}
		}
	}


	public static void SetAll(ContentHolder[] content, bool value)
	{
		foreach (ContentHolder contentHolder in content)
		{
			contentHolder.IsActive = value;
		}
	}
	#endregion

	public int Count { get { return content.Length; } }

	[SerializeField] ContentHolder[] content;


	/// <summary>
	/// Deactivates all holders except specified
	/// </summary>
	/// <param name="index">target holder index</param>
	public void DeactivateExcept(int index)
	{
		ContentHolders.DeactivateExcept(content, index);
	}


	/// <summary>
	/// Activates specified holder, all others stay as is
	/// </summary>
	/// <param name="index">target holder index</param>
	public void ActivateOnly(int index)
	{
		ContentHolders.ActivateOnly(content, index);
	}

	public void Activate(string name)
	{
		int index = 0;
		for(int i = 0; i < content.Length; i++)
		{
			if(content[i].Name == name)
			{
				index = i; break;
			}
		}

		Activate(index);
	}

	/// <summary>
	/// Activates specified holder and deactivates all other
	/// </summary>
	/// <param name="index">target holder index</param>
	public void Activate(int index)
	{
		ContentHolders.DeactivateExcept(content, index);
		ContentHolders.ActivateOnly(content, index);
	}

	public void Activate(bool value)
    {
		int intValue = UberBehaviour.BoolToInt(value);
		Activate(intValue);
    }


	/// <summary>
	/// Activates all holders
	/// </summary>
	public void ActivateAll()
	{
		ContentHolders.SetAll(content, true);
	}



	/// <summary>
	/// Deactivates all holders
	/// </summary>
	public void DeactivateAll()
	{
		ContentHolders.SetAll(content, false);
	}

	public void DeleteAll()
    {
		foreach(ContentHolder ch in content)
        {
			ch.Delete();
        }
    }


	public void DeactivateOnly(int index)
    {
		for (int i = 0; i < content.Length; i++)
		{
			if (i == index)
			{
				content[i].IsActive = false;
				break;
			}
		}
	}


	public int Add(GameObject[] gos, string name)
    {
		List<ContentHolder> lst = new List<ContentHolder>(content);
		ContentHolder current = new ContentHolder(gos, name);
		lst.Add(current);
		content = lst.ToArray();
		return content.Length - 1;
    }
}

namespace MTUi
{
    public class ContentSwitcher : UberBehaviour
    {
		[SerializeField] ContentHolder[] content;

		public int Length { get { return content.Length; } }
		public string[] Names 
		{ 
			get
            {
				List<string> names = new List<string>();
                for (int i = 0; i < content.Length; i++)
                {
					names.Add(content[i].Name);
				}
				return names.ToArray();
            }
		}


		/// <summary>
		/// Activates specified holder and deactivates all other
		/// </summary>
		/// <param name="index">target holder index</param>
		public void Activate(int index)
		{
			ContentHolders.DeactivateExcept(content, index);
			ContentHolders.ActivateOnly(content, index);
		}


		/// <summary>
		/// Deactivates all holders except specified
		/// </summary>
		/// <param name="index">target holder index</param>
		public void DeactivateExcept(int index)
		{
			ContentHolders.DeactivateExcept(content, index);
		}


		/// <summary>
		/// Activates specified holder, all others stay as is
		/// </summary>
		/// <param name="index">target holder index</param>
		public void ActivateOnly(int index)
		{
			ContentHolders.ActivateOnly(content, index);
		}


		/// <summary>
		/// Activates all holders
		/// </summary>
		public void ActivateAll()
		{
			ContentHolders.SetAll(content, true);
		}


		/// <summary>
		/// Deactivates all holders
		/// </summary>
		public void DeactivateAll()
		{
			ContentHolders.SetAll(content, false);
		}


		/// <summary>
		/// Adds list of GameObjects with specified name to content switcher
		/// </summary>
		/// <param name="objects">list of objects to add</param>
		/// <param name="name">name of new holder</param>
		public void AddItem(GameObject[] objects, string name)
		{
			ContentHolder newHolder = new ContentHolder(objects, name);
			List<ContentHolder> newList = new List<ContentHolder>();
			if (content != null)
			{
				foreach(ContentHolder el in content)
				newList.Add(el);
			}

			newList.Add(newHolder);
			this.content = newList.ToArray();
		}
	}
}