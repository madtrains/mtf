﻿using MTUi.Town;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace MTUi
{
    public enum SwipeDirection { Left, Right, Up, Down}
	public partial class Root : UberBehaviour
	{
		

		public static UnityEngine.Events.UnityAction OnUIInited;
		public static UnityEngine.Events.UnityAction<SwipeDirection> OnSwipe;
		public static UnityEngine.Events.UnityAction<Vector2, float> OnHold;
		public static UnityEngine.Events.UnityAction<Vector2> OnPress;
        public static UnityEngine.Events.UnityAction<Vector2> OnRelease;
        public static UnityEngine.Events.UnityAction<Dictionary<int, UIResource>> OnChangeResources;
		public static UnityEngine.Events.UnityAction<int> OnChangeTrainRounds;
		public static UnityEngine.Events.UnityAction<int> OnChangeLanguage;
        public static UnityEngine.Events.UnityAction<int> OnChangeTickets;

        private static Root root;
		private static bool isInited;
		#region Fields


		//Screens
		public Town.Town Town { get { return town; } }
        public Flipper.Flipper Flipper { get { return flipper; } }
        public Flipper.PostFlipper PostFlipper { get { return postFlipper; } }
        public Dialog Dialog { get { return dialog; } }
		//Popups
		public Popup Popup { get { return popup; } }
        public Ads.Ads Ads { get { return ads; } }
		public Splash.SplashPopup Splash { get { return splash; } }
        public TasksPopup.TaskScreen Tasks { get { return tasks; } }
        public SettingsPanel Settings { get { return settings; } }
		public ClickNTap ClickTap { get { return clickTap; } }

        
        //public bool ClickTapDisabled { get { return clickTap.isDisabled; } set { clickTap.isDisabled = value; } }

        [SerializeField] private RectTransform mainRectTransform;
        [SerializeField] private Transform dockScreens;
        [SerializeField] private Transform dockPopups;
        [SerializeField] private Splash.SplashPopup splash;
        [SerializeField] private List<UIElement> screens;
        [SerializeField] private IconIDHolder iconIDHolder;

		//priv
        private UIElement currentScreen;
        private UIElement previousScreen;

		//screens
        private Town.Town town;
        private Flipper.Flipper flipper;
        private Flipper.PostFlipper postFlipper;
        private Dialog dialog;

		//popups
        private Ads.Ads ads;        
        private Popup popup;
        private TasksPopup.TaskScreen tasks;
        private SettingsPanel settings;

        /// <summary>
        /// <resourceID, Object(value, parameters)
        /// </summary>
        private Dictionary<int, UIResource> uiResourcesDict;
        private int[] uiResourcesOrder;


        private ClickNTap clickTap;
        private int tickets;

        public Sprite GetIconID(int id)
		{
			return iconIDHolder.Get(id);
		}

		public void SetIconIDSpriteTo(UnityEngine.UI.Image targetImage, Sprite iconIDSprite)
		{
			int x = (int)iconIDSprite.rect.size.x;
			int y = (int)iconIDSprite.rect.size.y;
			targetImage.sprite = iconIDSprite;
			targetImage.rectTransform.sizeDelta = new Vector2(x, y);
		}

		public void SetIconIDSpriteTo(UnityEngine.UI.Image targetImage, int iconID)
		{
			SetIconIDSpriteTo(targetImage, GetIconID(iconID));
		}

		public delegate void ProcessUIResource(UIResource resource);

        public void IterateUIResources(ProcessUIResource processor)
        {
            for (int i = 0; i < this.uiResourcesDict.Count; i++)
            {
				processor(uiResourcesDict[i]);
            }
        }

		public void IterateOrderedUIResources(ProcessUIResource processor)
		{
            for (int i = 0; i < this.uiResourcesOrder.Length; i++)
            {
				int index = uiResourcesOrder[i];
                processor(uiResourcesDict[index]);
            }
        }

        public UIResource GetResource(int index)
        {
            return this.uiResourcesDict[index];
        }

		public int GetResourcesLength { get { return this.uiResourcesDict.Count; } }
        #endregion

        #region Getters
        public float Width
		{
			get
			{
				return UnityEngine.Screen.width;
			}
		}


		public float Height
		{
			get
			{
				return UnityEngine.Screen.height;
			}
		}


		public int WidthUnscaled
		{
			get
			{
				return UnityEngine.Screen.currentResolution.width;
			}
		}

		public Vector2 RectSizeDelta { get { return GetComponent<RectTransform>().sizeDelta; } }


		public float HeightUnscaled
		{
			get
			{
				return UnityEngine.Screen.currentResolution.height;
			}
		}


		public float Ratio
		{
			get
			{
				return Width / Height;
			}
		}
		#endregion

		#region Methods
		public async Task DownloadAndInit()
		{
			try
			{
                //popups
                popup = await DownloadBundlePopup<MTUi.Popup>(MTAssetBundles.ui_popups_popup);
                ads = await DownloadBundlePopup<MTUi.Ads.Ads>(MTAssetBundles.ui_popups_ads);
				settings = await DownloadBundlePopup<MTUi.SettingsPanel>(MTAssetBundles.ui_popups_settingspanel);
                //screens
                town = await DownloadBundleScreen<MTUi.Town.Town>(MTAssetBundles.ui_screens_town);
                flipper = await DownloadBundleScreen<MTUi.Flipper.Flipper>(MTAssetBundles.ui_screens_flipperscreen);
                postFlipper = await DownloadBundleScreen<MTUi.Flipper.PostFlipper>(MTAssetBundles.ui_screens_postflipperscreen);
                dialog = await DownloadBundleScreen<MTUi.Town.Dialog>(MTAssetBundles.ui_screens_dialog);
                iconIDHolder = await GameStarter.Instance.ResourceManager.LoadByBundleName(MTAssetBundles.holders_iconidholder) as IconIDHolder;
                tasks = await DownloadBundleScreen<MTUi.TasksPopup.TaskScreen>(MTAssetBundles.ui_screens_taskscreen);
                Root.OnChangeResources +=
					(resources) =>
					{
						ISharedUIElement<ScorePanel.ScorePanel>.Process((scorePanel) => { scorePanel.SetResources(resources); });
					};
            }
			catch (System.Exception ex)
			{
				Debug.LogErrorFormat("Can not Download Bundle Screen {0}", ex);	
			}
		}

        protected virtual void Dock(Transform tr, Transform dock)
        {
            tr.SetParent(dock, false);
            tr.SetAsLastSibling();
        }

        public void PostCreation()
		{
			OnUIInited?.Invoke();
        }

		private void Awake()
		{
			clickTap = new ClickNTap();
			uiResourcesDict = new Dictionary<int, UIResource>();
        }

		public void InitResourcesOrder()
		{
            List<UIResource> resources = new List<UIResource>();
            foreach (KeyValuePair<int, UIResource> keyValuePair in this.uiResourcesDict)
            {
                resources.Add(keyValuePair.Value);
            }
            resources.Sort((x, y) => x.Parameters.ResultsOrder.CompareTo(y.Parameters.ResultsOrder));
            uiResourcesOrder = new int[resources.Count];
            for (int i = 0; i < resources.Count; i++)
            {
                uiResourcesOrder[i] = resources[i].Parameters.ID;
            }
        }

		public void ChangeRounds(int value)
        {
			if (OnChangeTrainRounds != null)
				OnChangeTrainRounds(value);
        }

		public async Task<T> DownloadBundleScreen<T>(MTAssetBundles bundle) where T : UIElement
		{
            Debug.LogFormat("Dowloading as Screen {0}", bundle);
            T screen = 
				await GameStarter.Instance.ResourceManager.InstantiateByBundleName<T>(bundle);
            ProcessInstantiatedScreen<T>(screen);
            return screen;
        }

		public async Task<T> DownloadBundlePopup<T>(MTAssetBundles bundle) where T : UIElement
        {
			Debug.LogFormat("Dowloading as Popup {0}", bundle);
			T popup = await GameStarter.Instance.ResourceManager.InstantiateByBundleName<T>(bundle);
            popup.transform.SetParent(dockPopups, false);
			popup.transform.SetAsLastSibling();
			popup.Init();
			popup.Hide();
			return popup;
		}

		public void ProcessInstantiatedScreen<T>(T screen) where T : UIElement
		{
			screen.transform.SetParent(dockScreens, false);
			screen.transform.SetAsLastSibling();
			screen.Init();
			screens.Add(screen);
			screen.OnCommandShow += () =>
			{
				HideAllScreens(screen);
				previousScreen = currentScreen;
				currentScreen = screen;
			};
			Activate(screen.gameObject, false);
		}

		public void HideAllScreens(UIElement exception)
		{
			foreach (UIElement screen in screens)
			{
				if (screen != exception)
				{
					screen.Hide();
				}
			}
		}

		public void UpdateResources()
        {
            for (int i = 0; i < GameStarter.GameParameters.Resources.Length; i++)
            {
                int resourceAmount = GameStarter.Instance.PlayerManager.ResourceGetAmount(i);
				MTParameters.Resource resourceParameters = GameStarter.GameParameters.Resources[i];
				uiResourcesDict[i] = new UIResource(resourceAmount, resourceParameters);
            }	
            OnChangeResources?.Invoke(uiResourcesDict);
		}

		public void UpdateTickets(int newValue)
		{
			tickets = newValue;
			if (OnChangeTickets != null)
				OnChangeTickets(newValue);
        }
        #endregion

        #region UI Utilities
        public Vector2 GetScaledSize(Vector2 input)
		{
			int horizontal = GetScaledWidth(input.x);
			int vertical = GetScaledHeight(input.y);
			return new Vector2(horizontal, vertical);
		}

		public int GetScaledWidth(float x)
		{
			return Mathf.RoundToInt(x * transform.localScale.x);
		}

		public int GetScaledHeight(float y)
		{
			return Mathf.RoundToInt(y * transform.localScale.y);
		}
		#endregion

		#region Update
		private void Update()
		{
#if (UNITY_ANDROID || UNITY_IPHONE) && (!UNITY_EDITOR)
			UpdateMobile();
#else
			UpdatePc();
#endif
		}

		private void UpdatePc()
		{
			//if (clickTap.isDisabled)
				//return;
			if (Input.GetMouseButtonDown(0))
				clickTap.Press(Input.mousePosition);
			if (clickTap.IsPressed)
				clickTap.Hold(Input.mousePosition);
			if (Input.GetMouseButtonUp(0))
				clickTap.Release(Input.mousePosition);
		}

		private void UpdateMobile()
		{
            //if (clickTap.isDisabled)
                //return;

            if (Input.touchCount > 0)
			{
				//перебор всех касаний то есть ровно 1 :-)
				for (int i = 0; i < 1; i++)
				{
					Touch touch = Input.touches[i];
					TouchPhase phase = touch.phase;
					Vector2 position = touch.position;
					if (phase == TouchPhase.Began)
					{
						clickTap.Press(position);
                    }

					else if (phase == TouchPhase.Stationary || phase == TouchPhase.Moved)
					{
                        if (clickTap.IsPressed)
							clickTap.Hold(touch.position);
					}

					else if (phase == TouchPhase.Ended || phase == TouchPhase.Canceled)
					{
						clickTap.Release(position);
                    }
                }
			}
		}
        #endregion
    }

	public class UIResource
	{
		public UIResource(MTParameters.Resource parameters) : this(0, parameters) { }

        public UIResource(int value, MTParameters.Resource parameters)
		{
			this.value = value; this.parameters = parameters;
		}
        

        public int Value { get { return value; } set { this.value = value; } }
        private int value;

        public MTParameters.Resource Parameters { get { return parameters; } }
        private MTParameters.Resource parameters;
    }
}