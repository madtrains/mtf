﻿using UnityEngine;
using UnityEngine.UI;

namespace MTUi
{
    [RequireComponent(typeof(Text))]
	public class UITextElement : UIElement
	{
        [SerializeField] protected Text textField;
        [SerializeField] protected string key;
        [SerializeField] protected string rawText;
        [SerializeField] protected object[] atrs;
        [SerializeField] protected bool fontOnly;

        public Text TextField { get { return textField; } set { textField = value; } }

        public string Key
        {
            get { return key; }
            set { key = value; }
        }

        public override void Init()
        {
            base.Init();
            GameStarter.Instance.TranslationManager.OnLanguageChanged += OnLanguageChanged;
        }

        /// <summary>
        /// Выставить все значения форматирования {0}, {1} и.т.д
        /// </summary>
        public void SetFormatValues(params object[] values)
        {
            atrs = values;
            Translate();
            SetTextField();
        }

        public virtual void Push()
        {
            Translate();
            SetTextField();
        }

        /// <summary>
        /// Заменить ключ словаря перевода и обновить текст на экране
        /// </summary>
        /// <param name="key"></param>
        public virtual void PushKey(string key, bool checkSameKey=true)
        {
            if (checkSameKey && this.key == key)
                return;
            Key = key;
            Translate();
            SetTextField();
        }


        protected virtual void Translate()
        {
            SetFont(GameStarter.Instance.TranslationPack.LanguagePack.font);
            if (fontOnly)
                return;
            this.rawText = GameStarter.Instance.TranslationManager.GetTableString(key);   
        }


        protected virtual void SetFont(Font font)
        {
            textField.font = font;
        }

        protected virtual void SetTextField()
        {
            if (fontOnly)
                return;
            if (atrs != null && atrs.Length > 0)
            {
                textField.text = string.Format(rawText, atrs);
            }
            else
            {
                textField.text = rawText;
            }
        }


        protected virtual void OnLanguageChanged(TranslationManager.Language language)
        {
            Translate();
            SetTextField();
        }

        private void OnDestroy()
        {
            if (GameStarter.Instance.TranslationManager.OnLanguageChanged != null)
                GameStarter.Instance.TranslationManager.OnLanguageChanged -= OnLanguageChanged;
        }
    }
}