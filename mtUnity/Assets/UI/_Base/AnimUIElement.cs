﻿using UnityEngine;

namespace MTUi
{
	public class AnimUIElement : UIElement
	{
        public static int Hash_ON = Animator.StringToHash("on");
        [SerializeField] protected Animator animator;
        protected bool isShownByAnimation;

        protected override void ShowHide(bool state)
        {
            if(state)
            {
                OnCommandShow?.Invoke();
                Activate(this.gameObject, state);
                animator.SetBool(Hash_ON, true);
            }
            else //hide
            {
                OnCommandHide?.Invoke();
                animator.SetBool(Hash_ON, false);
            }
        }

        protected override bool GetVisibility()
        {
            return isShownByAnimation;
        }

        public virtual void EventIsShownByAnimation()
        {
            isShownByAnimation = true;
            OnShown?.Invoke();
        }


        public virtual void EventIsHiddenByAnimation()
        {
            Activate(this.gameObject, false);
            isShownByAnimation = false;
            OnHidden?.Invoke();
            //Debug.LogWarningFormat("After Anim ShowHide UEl:\"{0}\" to:\"{1}\"", gameObject.name, false);
        }

        /// <summary>
        /// Для пустых анимационных эвентов
        /// </summary>
        public void Empty()
        {

        }
    }
}