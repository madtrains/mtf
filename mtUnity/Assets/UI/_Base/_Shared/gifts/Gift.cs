using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.Gift
{
    public enum Collection { Ticket, CogRainbow, Coin }

    public class Gift : AnimUIElement
    {
        public UnityEngine.Events.UnityAction OnAnimationPlayed;

        [SerializeField] private ContentHolders giftHolders;
        [SerializeField] private Text number;
        public static int hSpeed = Animator.StringToHash("speed");
        public static int hGift = Animator.StringToHash("gift");

        public void EventOff()
        {
            OnAnimationPlayed?.Invoke();
        }

        public void Play(Collection gift, int number, bool playMagic=true, float speed=1f)
        {
            //animator.SetTrigger(hMerch);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("+");
            stringBuilder.Append(number);
            this.number.text = stringBuilder.ToString();
            giftHolders.Activate((int)gift);
            animator.SetTrigger(hGift);
            animator.SetFloat(hSpeed, speed);
            if (playMagic)
            {
                GameStarter.Instance.MainComponents.SoundManager.Shared.Play(MTSound.SharedNames.Magic);
            }
        }
    }
}

