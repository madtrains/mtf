﻿using UnityEngine;

namespace MTUi
{
    public class FloatingSpriteTimer : FloatingSprite
	{
        [SerializeField] protected float lifeTime;
        [SerializeField] protected TimerBeh timerBeh;
        public delegate void End();

        public void Run(float lifeTime, MTSpline.SplineCubic spline, End end)
        {
            Run(lifeTime, spline);
            timerBeh.OnEnd += ()=> { end(); OnFloatEnd?.Invoke(); };
        }

        public void Run(float lifeTime, MTSpline.SplineCubic spline)
        {
            timerBeh.Launch(lifeTime);
            timerBeh.OnTimeChanged = Fly;
            this.spline = spline;
            transform.position = this.spline.GetPointNormalized(0);
            UberBehaviour.Activate(this.gameObject, true);
            timerBeh.OnEnd += () => { OnFloatEnd?.Invoke(); };
        }

        private void Fly(float t)
        {
            transform.position = spline.GetPointNormalized(timerBeh.TimeNormalized);
        }
    }
}