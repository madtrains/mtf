﻿using System.Collections.Generic;
using UnityEngine;

namespace MTUi
{
    public class SnakeFloatingSprite : FloatingSprite
	{
		
		protected Vector3 offset;
		
		
		protected GameObject pointStart;
		protected GameObject pointUp;
		protected GameObject pointDown;
		protected GameObject pointEnd;
		protected float lifeTime;
		protected TimerBeh timer;
		protected bool isLaunched;
		[SerializeField] protected Transform targetTransform;


		/// <summary>
		/// Inits a floating sprite correctly
		/// </summary>
		/// <param name="splineHeight">Height of a sinewave-like curve relative to its length. Zero makes a flat line.</param>
		/// <param name="splineScale">Relative distance of middle points. 0 - both points will stay in the middle of the spline, 1 - on both ends of the spline. Recomended values are laying betveen 0.5 and 0.7.</param>
		public virtual void Init(Transform target, float splineHeight, float splineScale, float lifeTime)
		{
			Init();
			this.lifeTime = lifeTime;

			pointStart = new GameObject("pointStart");
			pointUp = new GameObject("pointUp");
			pointDown = new GameObject("pointDown");
			pointEnd = new GameObject("pointEnd");

			pointStart.transform.SetParent(this.transform, false);
			pointUp.transform.SetParent(this.transform, false);
			pointDown.transform.SetParent(this.transform, false);
			pointEnd.transform.SetParent(this.transform, false);
			pointEnd.transform.position = targetTransform.position;

			Vector3 startPosition = this.transform.position;
			Vector3 middle = Vector3.Lerp(startPosition, targetTransform.position, 0.5f);

			pointStart.transform.position = startPosition;
			Vector3 startRelative = TransormSplinePoint(pointStart.transform, targetTransform.position);
			float relativeHeight = startRelative.magnitude * splineHeight;
			Vector3 offset = relativeHeight * Vector3.up;

			pointUp.transform.position = Vector3.Lerp(middle, startPosition, splineScale);
			pointDown.transform.position = Vector3.Lerp(middle, targetTransform.position, splineScale);
			TransormSplinePoint(pointUp.transform, targetTransform.position);
			TransormSplinePoint(pointDown.transform, targetTransform.position);
			pointUp.transform.position += pointUp.transform.TransformVector(offset);
			pointDown.transform.position += pointDown.transform.TransformVector(offset * -1);

			List<Vector3> listOfCoordinates = new List<Vector3>();
			listOfCoordinates.Add(startPosition);
			listOfCoordinates.Add(pointUp.transform.position);
			listOfCoordinates.Add(pointDown.transform.position);
			listOfCoordinates.Add(targetTransform.position);

			CreateSpline(listOfCoordinates.ToArray());

			if (timer == null)
				timer = gameObject.AddComponent<TimerBeh>();

			timer.OnTimeChanged += Floating;
			timer.OnEnd += FloatEnd;
		}


		public void Launch()
		{
			if (isLaunched)
				return;

			isLaunched = true;
			timer.Launch(lifeTime);
		}


		protected virtual void FloatEnd()
		{
			if (OnFloatEnd != null)
				OnFloatEnd();

			SelfDestroy();
		}
	}
}