﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MTUi
{
    public class SpriteStream : TimerBeh
	{
        [SerializeField] private float pause;
        [SerializeField] private Transform parentTransform;
        [SerializeField] private FloatingSpriteTimer spriteTimerRef;
        private MTSpline.SplineCubic spline;
        private List<FloatingSpriteTimer> elements;

        public UnityEngine.Events.UnityAction<FloatingSpriteTimer> OnElementCreated;
        public UnityEngine.Events.UnityAction<FloatingSpriteTimer> OnElementDone;
        public UnityEngine.Events.UnityAction OnBurstEnd;


        private bool isStreamActive;

        public void Init(MTSpline.SplineCubic spline)
        {
            this.spline = spline;
            this.OnEnd = DelayTimerEnd;
            this.elements = new List<FloatingSpriteTimer>();
        }

        public void Init(MTSpline.SplineCubic spline, FloatingSpriteTimer spriteRef)
        {
            Init(spline);
            this.spriteTimerRef = spriteRef;
        }

        public void Init(MTSpline.SplineCubic spline, FloatingSpriteTimer spriteRef, Transform parentTransform)
        {
            Init(spline, spriteRef);
            this.parentTransform = parentTransform;
            this.spriteTimerRef = spriteRef;
        }

        public void Cancel()
        {
            
        }

        public void StartGeneration()
        {
            isStreamActive = true;
            GearClone();
            Launch(pause);
        }

        private void DelayTimerEnd()
        {
            if (isStreamActive)
            {
                GearClone();
                Launch(pause);
            }
        }

        public void StopGeneration()
        {
            if (elements.Count > 0) 
            {
                isStreamActive = false;
                elements.Last<FloatingSprite>().OnFloatEnd += () => { OnBurstEnd?.Invoke();};

            }
        }

        private void GearClone()
        {
            FloatingSpriteTimer floatingSprite = UIElement.CloneUIElementByRef<FloatingSpriteTimer>(spriteTimerRef, parentTransform, "gear", false);
            elements.Add(floatingSprite);
            floatingSprite.Run(lifeTime, spline, () => { GearTransitionEnd(floatingSprite); });
            OnElementCreated?.Invoke(floatingSprite);
        }

        private void GearTransitionEnd(FloatingSpriteTimer floatingSprite)
        {
            OnElementDone?.Invoke(floatingSprite);
            elements.Remove(floatingSprite);
            floatingSprite.SelfDestroy();
        }
    }
}