﻿using MTSpline;
using System.Collections.Generic;
using UnityEngine;

namespace MTUi
{
    public class FloatingSprite : AnimUIElement
	{
		protected static readonly int DEBUG_LINE_RENDERER_DETALISATION = 32;
		public UnityEngine.Events.UnityAction OnFloatEnd;

        public int IntValue { get { return intValue; } set { intValue = value; } }

        [SerializeField] protected int intValue;
        [SerializeField] protected bool debugDraw;
		protected LineRenderer debugLineRenderer;
		protected SplineCubic spline;
		
        

		public static void DebugDraw(LineRenderer rend, SplineCubic spline)
		{
			if (spline == null)
			{
				Debug.LogError("No spline inited");
				return;
			}
			Vector3[] points = spline.GetDrawLinePoints(DEBUG_LINE_RENDERER_DETALISATION);
			rend.positionCount = DEBUG_LINE_RENDERER_DETALISATION;
			rend.SetPositions(points);
		}

		public LineRenderer DebugLineRenderer
		{
			get
			{
				if (debugLineRenderer == null)
				{
					debugLineRenderer = gameObject.AddComponent<LineRenderer>();
					debugLineRenderer.startWidth = 1f;
					debugLineRenderer.endWidth = 1f;
				}

				return debugLineRenderer;
			}
		}

		public static SplineCubic CreateCubicSplineByPoints(Vector3[] points, int numberOfSplineSegments = 10)
		{
            return new SplineCubic(points, numberOfSplineSegments);
        }


		public virtual SplineCubic CreateSpline(Vector3[] points, int numberOfSplineSegments = 10)
		{
			SplineCubic newSpline = new SplineCubic(points, numberOfSplineSegments);
			return newSpline;
		}


		/// <summary>
		/// point looks at target by X axis, not Z and returns relative Vector3
		/// </summary>
		protected Vector3 TransormSplinePoint(Transform point, Vector3 lookTarget)
		{
			Vector3 relative = lookTarget - point.position;
			Quaternion lookRotation = Quaternion.LookRotation(relative);
			point.rotation = lookRotation;
			point.transform.right = point.transform.TransformDirection(Vector3.forward);
			return relative;
		}


		/// <summary>
		/// use this method with some update to float sprite
		/// </summary>
		/// <param name="positionNormalised"></param>
		protected virtual void Floating(float positionNormalised)
		{
			this.transform.position = spline.GetPointNormalized(positionNormalised);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="start">start point</param>
		/// <param name="end">end point</param>
		/// <param name="height1">height of first point relative to length. length * height1</param> 
		/// <param name="height2">height of second point relative to length. length * height2</param>
		/// <param name="scale">relative distance from center. values from 0 to 1. Best values from 0.3 to 0.7</param>
		/// <returns></returns>
		protected SplineCubic Sinewave(Transform start, Transform end, float height1, float height2, float scale)
		{
			Vector3 startPosition = start.transform.position;
			Vector3 endPosition = end.transform.position;

			Vector3 relative = endPosition - startPosition;
			float length = relative.magnitude;

			Quaternion lookRotation = Quaternion.LookRotation(relative, Vector3.up);
			lookRotation = lookRotation * Quaternion.Euler(0f, -90f, 0f);
			Matrix4x4 m = Matrix4x4.TRS(startPosition, lookRotation, Vector3.one);
			Vector3 p2 =  m.MultiplyPoint(new Vector3(length * (0.5f - scale), height1 * length));
			Vector3 p3 = m.MultiplyPoint(new Vector3(length * (0.5f + scale), height2 * length * -1f));
			List<Vector3> pos = new List<Vector3>();
			pos.Add(startPosition);
			pos.Add(p2);
			pos.Add(p3);
			pos.Add(endPosition);
			return new SplineCubic(pos.ToArray(), 10);
		}
	}
}