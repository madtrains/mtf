﻿using UnityEngine;

namespace MTUi
{
    public class FloatingSpriteAnim : FloatingSprite
	{
		[Range(0f, 1f)] public float lerpValue;
        public bool lerpActive;

        [SerializeField] protected Transform targetTransform;


        protected virtual void Update()
        { 
            if (lerpActive)
            {
                targetTransform.position = spline.GetPointNormalized(lerpValue);
            }
        }
    }
}