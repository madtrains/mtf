﻿using UnityEngine;

namespace MTUi
{
    public class DebugIconSystem : UIElement
    {
		private int _value;
		private bool _boolValue;
		private ContentSwitcher switcher;

        public override void Init()
        {
            base.Init();
			this.switcher = gameObject.AddComponent<ContentSwitcher>();
			for (int i = 0; i < transform.childCount; i++)
			{
				Transform child = transform.GetChild(i);
				GameObject[] objects = new GameObject[1];
				objects[0] = child.gameObject;
				switcher.AddItem(objects, child.gameObject.name);
			}
		}


		public int IntValue
		{
			get
			{
				return _value;
			}

			set
			{
				_value = value;
				switcher.Activate(value);
			}
		}


		public bool BoolValue
		{
			get
			{
				return _boolValue;
			}

			set
			{
				_boolValue = value;
				int intValue = BoolToInt(value);
				IntValue = intValue;
			}
		}


		public bool EnabledAll
		{
			get
			{
				return _boolValue;
			}

			set
			{
				_boolValue = value;
				if (value)
				{
					switcher.ActivateAll();
				}

				else
				{
					switcher.DeactivateAll();
				}
			}
		}
	}
}