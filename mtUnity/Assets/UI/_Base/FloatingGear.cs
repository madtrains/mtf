﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi
{   
    [RequireComponent(typeof(Animator))]
	public class FloatingGear : FloatingSpriteAnim
	{
        public MTSpline.SplineCubic SplineUp { get { return splineUp; } }
        public MTSpline.SplineCubic SplineDown { get { return splineDown; } }

        public static int hIsDown = Animator.StringToHash("isDown");
        [SerializeField] private GameObject dockDown;
        [SerializeField] private Vector2 add;
        [SerializeField] private Vector2 add2;
        [Range(0f, 1f)] [SerializeField] private float height;
        [Range(0f, 1f)] [SerializeField] private float height2;
        [Range(0f, 0.5f)] [SerializeField] private float scale;

        private MTSpline.SplineCubic splineDown;
        private MTSpline.SplineCubic splineUp;

        private bool fallen;


        public void SetDownDock(GameObject dockDown)
        {
            this.dockDown = dockDown;
        }


        public void SetAnimate(bool fallen)
        {
            if (fallen != this.fallen)
            {
                this.fallen = fallen;
                RunAnimation(fallen);
            }
        }


        private void RunAnimation(bool animatorParameterValue)
        {
            if (animatorParameterValue)
            {
                this.spline = splineDown;
            }
            else
            {
                this.spline = splineUp;
            }

            animator.SetBool(hIsDown, animatorParameterValue);
        }


        protected override void Update()
        {
            base.Update();
            if (debugDraw)
                CreateSplines();
        }


        public void CreateSplines()
        {
            List<Vector3> points = new List<Vector3>();
            Vector3 point = transform.parent.position;
            points.Add(point);
            points.Add(point + new Vector3(add.x, add.y, 0));
            points.Add(point + new Vector3(add2.x, add2.y, 0));
            points.Add(dockDown.transform.position);
            splineDown = CreateSpline(points.ToArray());
            splineUp = Sinewave(dockDown.transform, transform.parent, this.height, this.height2, this.scale);
        }
    }
}