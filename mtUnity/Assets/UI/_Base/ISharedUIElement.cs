﻿namespace MTUi
{
    public interface ISharedUIElement<T> where T : UIElement
    {
        public delegate void ProcessDelegate(T el);

        private static System.Collections.Generic.List<T> instances;

        public static void Process(ProcessDelegate processDelegate)
        {
            if(instances == null || instances.Count == 0)
                return;
            foreach (T screen in instances)
            {
               processDelegate(screen);
            }
        }


        public static void AddInstance(T screen)
        {
            if (instances == null) { instances = new System.Collections.Generic.List<T> (); }
            instances.Add(screen);
        }
    }
}
