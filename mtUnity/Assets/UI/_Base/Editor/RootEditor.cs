using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace MTUi
{
    [CustomEditor(typeof(Root))]

    public class RootEditor : UberEditor
    {
        public Object workOnScreen;

        public Root Root { get { return target as Root; } }

        private string[] names = { "dockSplash", "DebugScreen" };
        
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();


            workOnScreen = EditorGUILayout.ObjectField(workOnScreen, typeof(Object), true);
            if (GUILayout.Button("Hide Built-in Components (For UI Screens Development)"))
            {
                ProcessScreens(false);
            }
            if (GUILayout.Button("Show Built-in Components (For UI Screens Development)"))
            {
                ProcessScreens(true);
            }
        }

        private void Set(GameObject trg, bool value)
        {
            trg.SetActive(value);
        }

        private void ProcessScreens(bool value)
        {
            for (int i = 0; i < Root.transform.childCount; i++)
            {
                Transform child = Root.transform.GetChild(i);
                foreach (string rName in names)
                {
                    if (child.name == rName)
                    {
                        Set(child.gameObject, value);
                    }
                }
            }
            if (workOnScreen == null)
                workOnScreen = Root.transform.Find("dockMain").GetChild(0).gameObject;
            if (workOnScreen != null)
                Set((workOnScreen as GameObject), !value);
        }
    }
}
