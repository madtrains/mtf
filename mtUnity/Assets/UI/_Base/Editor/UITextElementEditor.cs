﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MTParameters;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine.Experimental.GlobalIllumination;
using System.Drawing.Printing;
using MTUi;
using UnityEngine.UI;
using UnityEngine.Rendering;

[CustomEditor(typeof(MTUi.UITextElement))]
public class UITextElementEditor : UberEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        MTUi.UITextElement te = target as MTUi.UITextElement;
        if (GUILayout.Button("Pick"))
        {
            Text text = te.GetComponent<Text>();
            if (text != null)
                te.TextField = text;
            GameParameters _gp = GameParametersViewer.GetParameters();
            TranslationPicker.ShowWindow(_gp, te);
            
        }
    }
}

public class UITextValueElementEditor : UITextElementEditor { }

public class TranslationPicker : EditorWindow
{
    private static GameParameters _gp;
    private static string filter;
    private Vector2 scrollPos;
    private int languageIndex;
    private UITextElement link;



    public static void ShowWindow(GameParameters gp, UITextElement textEl)
    {
        var wnd = EditorWindow.GetWindow(typeof(TranslationPicker));
        TranslationPicker tp = wnd as TranslationPicker;
        tp.Link(textEl);
        _gp = gp;
    }

    private static Regex FilterRegex
    {
        get
        {
            string fl = ".*" + filter + ".*";
            return new Regex(fl, RegexOptions.Compiled | RegexOptions.IgnoreCase);
        }
    }

    public void Link(UITextElement el) { this.link = el; }

    private void OnGUI()
    {
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
        filter = EditorGUILayout.TextField("Filter:", filter);
        EditorGUILayout.BeginVertical();

        foreach (KeyValuePair<string, string> kvp in _gp.Languages[0].StringsDict)
        {
            
            Match match = FilterRegex.Match(kvp.Key);
            if (match != null && match.Value != "")
            {
                StringBuilder st = new StringBuilder();
                st.Append(kvp.Key);
                if (GUILayout.Button(st.ToString()))
                {
                    ButtonCommand(st.ToString());
                }
            }
        }

        EditorGUILayout.EndVertical();
        EditorGUILayout.EndScrollView();
    }

    private void ButtonCommand(string text)
    {
        this.Close();
        Undo.RecordObject(link, "Set Translation key");
        link.Key = text;
    }

    /*
    private void OnDestroy()
    {
        
    }
    */
}
