using UnityEditor;
using UnityEngine;

namespace MTUi
{

    [CustomPropertyDrawer(typeof(IconID))]
    class Icon : UberPropertyDrawer
    {
        protected override void Draw(Rect position, SerializedProperty property, GUIContent label, Rect main)
        {
            base.Draw(position, property, label, main);
            Color ok = new Color(0f, 0f, 0f, 0f);
            Color error = new Color(1f, 0.2f, 0f, 0.3f);
            //
            int IDWidth = 45;
            int space = 5;
            int mainWidth = (int)(main.width - IDWidth);
            Rect r1 = new Rect(main.x, main.y, IDWidth, main.height);
            Rect r2 = new Rect(r1.max.x, main.y, space, main.height);
            Rect r3 = new Rect(r2.max.x, main.y, mainWidth, main.height);

            

            //Rect r5 = new Rect(r4.max.x, main.y, left, main.height); //prop weight

            SerializedProperty id = property.FindPropertyRelative("_id");
            SerializedProperty sprite = property.FindPropertyRelative("_sprite");
            SerializedProperty duplicateID = property.FindPropertyRelative("_duplicateID");
            SerializedProperty duplicateSprite = property.FindPropertyRelative("_duplicateSprite");


            EditorGUI.PropertyField(r1, id, GUIContent.none, false);
            EditorGUI.PropertyField(r3, sprite, GUIContent.none, false);
            EditorGUI.DrawRect(r1, duplicateID.boolValue ? error : ok);
            EditorGUI.DrawRect(r3, duplicateSprite.boolValue || sprite.objectReferenceValue == null ? error : ok);
        }
    }
}