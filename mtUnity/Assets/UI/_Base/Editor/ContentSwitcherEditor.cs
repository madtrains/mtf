﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(MTUi.ContentSwitcher))]
public class ContentSwitcherEditor : UberEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        MTUi.ContentSwitcher switcher = target as MTUi.ContentSwitcher;
        

        if (switcher != null )
        {
            if (GUILayout.Button("Fill"))
            {
                Transform targetFiller = switcher.transform;
                for (int i = 0; i < targetFiller.childCount; i++)
                {
                    GameObject gameObject = targetFiller.GetChild(i).gameObject;
                    switcher.AddItem(new GameObject[] { gameObject }, gameObject.name);
                }
            }

            if (switcher.Length > 0)
            {
                for (int i = 0; i < switcher.Length; i++)
                {
                    string name = switcher.Names[i];
                    if (GUILayout.Button(name))
                    {
                        switcher.Activate(i);

                    }
                }
            }
        }
    }
}
