using UnityEngine;


namespace MTUi
{
    [System.Serializable]
    public class IconID
    {
        public int ID { get { return _id; } }
        public Sprite Sprite { get { return _sprite; } }

        public bool DuplicateID { get { return _duplicateID; } set { _duplicateID = value; } }
        public bool DuplicateSprite { get { return _duplicateSprite; } set { _duplicateSprite = value; } }

        [SerializeField] private int _id;
        [SerializeField] private Sprite _sprite;
        [SerializeField] [HideInInspector] private bool _duplicateID;
        [SerializeField] [HideInInspector] private bool _duplicateSprite;
    }
}
