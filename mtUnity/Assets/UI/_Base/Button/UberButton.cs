using UnityEngine;
using UnityEngine.UI;

namespace MTUi
{
    public delegate void CLickDelegate();

	public class UberButton : UIElement
	{
		public static UberButton Create(GameObject target, CLickDelegate @delegate)
		{
			return Create<UberButton>(target, @delegate);
		}

		public static T Create<T>(GameObject target, CLickDelegate @delegate) where T : UberButton
        {
			T result = target.AddComponent<T>();
			result.Init(@delegate);
			return result;
		}

		public UnityEngine.Events.UnityAction Command;
		public UnityEngine.Events.UnityAction CommandImmediate;

		public Button ButtonComponent { get { return buttonComponent; } }
		public string ButtonLabel
		{
			set
			{
				if (this.buttonLabel == null)
				{
					Debug.LogErrorFormat("Button label is null. {0}", gameObject.name);
				}
				buttonLabel.text = value;
			}
		}

		public Color ButtonLabelColor
		{
			set
			{
				if (this.buttonLabel == null)
				{
					Debug.LogErrorFormat("Button label is null. {0}", gameObject.name);
				}
				buttonLabel.color = value;
			}
		}

		public bool Toggle { get { return toggle; } set { toggle = value; } }
		public bool Interactable { set { SetInteractable(value); } }
		public float TransitionSpeed { set { transitionSpeed = value; } }
		public float PressedScale { set { pressedScale = value; } }
		public enum State { Idle, Press, Release }

		[Tooltip("Redirect target transform. If null, this transform will be used as target")]
		[SerializeField] protected Transform target;
		[SerializeField] protected Text buttonLabel;
		[SerializeField] protected float transitionSpeed = 6f;
		[SerializeField] protected float pressedScale = 0.85f;

		protected Button buttonComponent;
		protected float timer = 0f;
		protected State state;
		protected bool toggle;


		public override void Init()
		{
			base.Init();

			buttonComponent = gameObject.GetComponent<Button>();
			if (buttonComponent == null)
				buttonComponent = gameObject.AddComponent<Button>();

			if (target == null)
				target = this.transform;

			Image im = gameObject.GetComponent<Image>();
			if (im == null)
            {
				Debug.LogWarningFormat("No image for button {0}", gameObject.name);
			}
			else if (!im.raycastTarget)
            {
				Debug.LogWarningFormat("No raycast target for button {0}", gameObject.name);
			}
				
			buttonComponent.transition = Selectable.Transition.None;
			buttonComponent.onClick.AddListener(Press);
		}

		public void Init(CLickDelegate clickDelegate)
		{
			Init();
			SetCommand(clickDelegate);
		}

		public void SetCommand(CLickDelegate clickDelegate)
        {
			this.Command = () => { clickDelegate(); };
		}

        protected virtual void SetInteractable(bool value)
		{
			if (buttonComponent != null)
				buttonComponent.interactable = value;
		}


		protected virtual void Update()
		{
			if (state == State.Idle)
				return;

			timer += Time.deltaTime * transitionSpeed;

			if (state == State.Press)
			{
				Transition(Mathf.InverseLerp(0.5f, 0.0f, timer));
				if (timer >= 0.5f)
				{
					state = State.Release;
				}
			}

			else if (state == State.Release)
			{
				Transition(Mathf.InverseLerp(0.5f, 1.0f, timer));
				if (timer >= 1f)
				{
					state = State.Idle;
					timer = 0f;
					Release();
				}
			}
		}


		protected virtual void Press()
		{
			if (CommandImmediate != null)
				CommandImmediate();

			state = State.Press;
			GameStarter.Instance.MainComponents.SoundManager.Shared.Play(MTSound.SharedNames.Button);
		}


		protected virtual void Release()
		{
			ToggleCommand();
			if (Command != null)
				Command();
		}


		protected virtual void Transition(float t)
		{
			target.localScale = Vector3.Lerp(Vector3.one * pressedScale, Vector3.one, t);
		}


		protected virtual void ToggleCommand()
		{
			toggle = !toggle;
		}
	}
}
