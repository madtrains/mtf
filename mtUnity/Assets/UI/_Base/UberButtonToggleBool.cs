﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace MTUi
{
	public class UberButtonToggleBool : UberButton
	{
		[SerializeField] private ContentHolders content;
        [SerializeField] private bool value;

        public Action<bool> OnClick;

        public override void Init()
        {
            base.Init();
            this.SetCommand(() =>
            {
                value = !value;
                content.Activate(value);
                OnClick?.Invoke(value);
            });
        }

       
    }
}