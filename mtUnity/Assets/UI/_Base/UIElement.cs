﻿using UnityEngine;

namespace MTUi
{
	public class UIElement : UberBehaviour
	{
        public UnityEngine.Events.UnityAction OnCommandShow;
		public UnityEngine.Events.UnityAction OnCommandHide;

		public UnityEngine.Events.UnityAction OnShown;
        public UnityEngine.Events.UnityAction OnHidden;

        [SerializeField] protected bool ignoreVisibleState;
        [SerializeField] protected ContentHolder toHide;
        [SerializeField] protected ContentHolder toShow;

		[SerializeField] private UIElement[] other;
        [SerializeField] protected RectTransform rectTransform;
        

        public bool Visible
		{
			get { return GetVisibility(); }
		}

		public bool IsInited { get { return isInited; } }

		protected bool isInited;

		#region Getters
		public MTUi.Root Root { get { return GameStarter.Instance.UIRoot; } }

		/// <summary>
		/// Позиция этого элемента относительно центра экрана
		/// </summary>
		public Vector2 ScreenPosition
		{
			get
			{
				return GetScreenPosition(this.transform);
			}
		}

		public RectTransform RectTransform
		{
			get
			{
				if(rectTransform == null)
				{
					rectTransform = GetComponent<RectTransform>();
				}
				return rectTransform;
			}
		}
		#endregion

		#region Main Part
		/// <summary>
		/// Use this mehtod to Show element
		/// </summary>
		public void Show()
		{
			if(!Visible || ignoreVisibleState)
				ShowHide(true);
		}

		/// <summary>
		/// Use this mehtod to Hide element
		/// </summary>
		public void Hide()
		{
			if(Visible || ignoreVisibleState)
				ShowHide(false);
		}

		/// <summary>
		/// This method will be called when the visibility state changes
		/// </summary>
		/// <param name="state">show/hide</param>
		protected virtual void ShowHide(bool state)
		{
			if (state)
			{
				OnCommandShow?.Invoke();
				OnShown?.Invoke();
			}
			else
			{
				OnCommandHide?.Invoke();
				OnHidden?.Invoke();
			}
            Activate(this.gameObject, state);
        }

		

        /// <summary>
        /// Do not use this Inition if it is present override with attributes
        /// </summary>
        public virtual void Init()
		{
			if (other!= null && other.Length > 0)
			{
                foreach (UIElement oUIel in other)
                {
					if (oUIel == null)
					{
						Debug.LogErrorFormat("Can not init other {0}", this.gameObject.name);
						continue;
					}
                    oUIel.Init();
                }
            }
			if (toShow != null) 
			{ 
				toShow.IsActive = true; 
			}
            if(toHide != null)
			{ 
				toHide.IsActive = false; 
			}
            this.isInited = true;
		}

        protected virtual bool GetVisibility()
        {
            return gameObject.activeSelf;
        }

        protected virtual void Start()
		{
			this.rectTransform = GetComponent<RectTransform>();
        }

		protected virtual void OnValidate()
		{

		}
		#endregion

		#region Methods


		protected bool IsInRect(RectTransform target, Vector3 coordinates)
		{
			Vector3[] v = new Vector3[4];
			target.GetWorldCorners(v);
			if (coordinates.x > v[0].x && coordinates.x < v[2].x && coordinates.y > v[0].y && coordinates.y < v[2].y)
				return true;
			return false;
		}

		/// <summary>
		/// Позиция Transform относительно центра экрана
		/// </summary>
		protected Vector2 GetScreenPosition(Transform transform)
		{
			int x = Mathf.RoundToInt((transform.position.x / Root.transform.localScale.x) - Root.WidthUnscaled / 2f);
			int y = Mathf.RoundToInt((transform.position.y / Root.transform.localScale.y) - Root.HeightUnscaled / 2f);
			return new Vector2(x, y);
		}

        /// <summary>
        /// Этот метод используется для обнуления счётчиков
        /// </summary>
        public virtual void Reset()
        {

        }
        #endregion

        #region Utilities
        public static T CloneUIElementByRef<T>(T referenceElement, Transform parent, string name, bool isActive) where T : UIElement
		{
			T newUIElement = Instantiate<T>(referenceElement);
			newUIElement.gameObject.name = name;
			if (parent != null)
				newUIElement.transform.SetParent(parent, false);

			newUIElement.ActiveSelf = isActive;
			return newUIElement;
		}
		public static Vector2 CornerCoordinatesToCentric(float width, float height, float inputX, float inputY)
		{
			float x = inputX - width / 2f;
			float y = inputY - height / 2f;
			return new Vector2(x, y);
		}
		#endregion


		public void EventPlayMagic()
		{
			SharedSound(MTSound.SharedNames.Magic);
		}

		protected void SharedSound(MTSound.SharedNames soundSharedName)
		{
			GameStarter.Instance.MainComponents.SoundManager.Shared.Play(soundSharedName);
		}
	}
}