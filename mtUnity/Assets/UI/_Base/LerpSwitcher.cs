using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class LerpSwitcher
{
    public UnityAction<float> OnLerp;
    public UnityAction<float> OnLerpA;
    public UnityAction<float> OnLerpB;
    public UnityAction OnHalfDone;

    public float TA { get { return tA; } }
    public float TB { get { return tB; } }

    public bool Inversed { get { return inversed; }  set { inversed = value; } }
    [SerializeField] protected bool inversed;

    protected float tA;
    protected float tB;
    protected bool isHalfDone;


    public virtual void Switch(float t)
    {
        if (inversed)
            t = 1 - t;

        if (t < 0.5)
        {
            isHalfDone = false;
            this.tA = Mathf.InverseLerp(0f, 0.5f, t);
            if (OnLerpA != null)
                OnLerpA(tA);
        }

        else
        {
            if (!isHalfDone)
            {
                isHalfDone = true;
                if (OnHalfDone != null)
                {
                    OnHalfDone();
                }
            }
            this.tB = Mathf.InverseLerp(0.5f, 1f, t);
            if (OnLerpB != null)
                OnLerpB(tB);
        }

        if (OnLerp != null)
            OnLerp(t);
    }
}
