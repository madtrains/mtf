﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MTUi.ScorePanel
{
    public class ScorePanel : AnimUIElement, ISharedUIElement<ScorePanel>
    {
        public static int hFlip = Animator.StringToHash("flip");

        public int Coins { set { tCoins.text = value.ToString(); } }
        public int CogCoins { set { tCogCoins.text = value.ToString(); } }
        public RectTransform RectT { get { return rectT; } }

        [SerializeField] private Text tCoins;
        [SerializeField] private Text tCogCoins;
        [SerializeField] private CargosSwitcherPair cargoSwitcher;
        [SerializeField] private TextPair cargoValue;
        [SerializeField] private RectTransform rectT;
        [SerializeField] private int resourceSwitchTime;
        [SerializeField] private TimerBeh timer;
        [SerializeField] private UberButton menuButton;
        private int currentCargoIndex = -1;


        protected override void ShowHide(bool state)
        {
            if(state) //show
            {
                Activate(this.gameObject, true);
                animator.SetBool(Hash_ON, true);
                timer.Launch(resourceSwitchTime);
            }
            else //hide
            {
                animator.SetBool(Hash_ON, false);
            }
        }

        protected override bool GetVisibility()
        {
            return animator.GetBool(Hash_ON);
        }

        public override void Init()
        {
            ISharedUIElement<ScorePanel>.AddInstance(this);
            timer.OnEnd = () => 
            { 
                timer.ReLaunch();
                SetNextCargo(GameStarter.Instance.TownManager.Town.TownParameters.Resources);
            };
            menuButton.Init(() => { Root.Settings.Show(); });
            base.Init();
        }

        public void SetResources(Dictionary<int, UIResource> resources)
        {
            Coins = resources[0].Value;
            CogCoins = resources[9].Value;
            foreach(KeyValuePair<int, UIResource> keyValuePair in resources)
            {
                int resourceID = keyValuePair.Key;
                UIResource resource = keyValuePair.Value;
                if (resourceID == cargoSwitcher.CurrentSwitcherIndex)
                {
                    cargoValue.SetCurrent(resource.Value);
                }
            }
        }

        public void SetNextCargo(int[] array)
        {
            int next = UberBehaviour.CycleArray<int>(array, currentCargoIndex);
            currentCargoIndex = next;
            SetTargetAndFlip(currentCargoIndex, Root.GetResource(currentCargoIndex).Value);
        }

        public int GetActiveResource()
        {
            return cargoSwitcher.CurrentSwitcherIndex;
        }

        public void EventCargoFlipped()
        {
            cargoSwitcher.FlipDone();
        }

        public void EventValueFlipped()
        {
            cargoValue.FlipDone();
        }

        public void SetCurrent(int cargoIndex, int value)
        {
            cargoSwitcher.SetCurrent(cargoIndex);
            cargoValue.SetCurrent(value);
        }

        public void SetTargetAndFlip(int cargoTargetIndex, int cargoTargetValue)
        {
            cargoSwitcher.SetTarget(cargoTargetIndex);
            cargoValue.SetTarget(cargoTargetValue);
            animator.SetTrigger(hFlip);
        }
    }

    /// <summary>
    /// пара значений, в конце перелистывания целевое подменяет текущее
    /// </summary>
    public class FlipPair
    {
        protected int current;
        protected int target;

        public virtual void SetCurrent(int value)
        {
            current = value;
        }

        public virtual void SetTarget(int value)
        {
            target = value;
        }

        public virtual void FlipDone()
        {
            SetCurrent(target);
        }

        
    }

    [System.Serializable]
    public class TextPair : FlipPair
    {
        public RectTransform RectT { get { return currentField.rectTransform; } }

        [SerializeField] private Text currentField;
        [SerializeField] private Text targetField;
        

        public override void SetCurrent(int value)
        {
            base.SetCurrent(value);
            currentField.text = value.ToString();
        }

        public override void SetTarget(int value)
        {
            base.SetTarget(value);
            targetField.text = value.ToString();
        }
    }

    [System.Serializable]
    public class CargosSwitcherPair : FlipPair
    {
        public int CurrentSwitcherIndex { get { return switcherCurrent.Current; } }

        [SerializeField] CargosSwitcher switcherCurrent;
        [SerializeField] CargosSwitcher switcherTarget;

        public override void SetCurrent(int value)
        {
            base.SetCurrent(value);
            switcherCurrent.Set(value);
        }

        public override void SetTarget(int value)
        {
            base.SetTarget(value);
            switcherTarget.Set(value);
        }
    }
}