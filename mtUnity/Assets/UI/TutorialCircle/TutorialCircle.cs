﻿using UnityEngine;
using UnityEngine.Events;

namespace MTUi
{
    public class TutorialCircle : UIElement
    {
        public UnityAction OnCirclePressed;
        public float Transparency { get { return canvasGroup.alpha; } set { canvasGroup.alpha = value; } }

        [SerializeField] private RectTransform rect;
        [SerializeField] private RectTransform top;
        [SerializeField] private RectTransform bottom;
        [SerializeField] private RectTransform left;
        [SerializeField] private RectTransform right;

        [SerializeField] private RectTransform circle;
        [SerializeField] private RectTransform top_sh;
        [SerializeField] private RectTransform bottom_sh;
        [SerializeField] private RectTransform left_sh;
        [SerializeField] private RectTransform right_sh;
        [SerializeField] private RectTransform[] horizontal;
        [SerializeField] private CanvasGroup canvasGroup;
        private float defaultTransparency;



        [System.ObsoleteAttribute("Use methods with Parameters instead. Can not show dialog without text and delegates")]
        public new void Show()
        {
            throw new System.NotSupportedException("Dialog was shown without any parameters");
        }


        public void CommandButton()
        {
            OnCirclePressed?.Invoke();
        }

        public override void Init()
        {
            base.Init();
            defaultTransparency = Transparency;
            ScaleByScreenSize();
        }

        public void ResetTransparency()
        {
            Transparency = defaultTransparency;
        }

        public void ScaleByScreenSize()
        {
            float height = 1080 + 32f;
            float width = Root.Width;
            width = (width * 2f) + 32f;
            top_sh.sizeDelta = new Vector2(width, height);
            bottom_sh.sizeDelta = new Vector2(bottom_sh.sizeDelta.x, height);
        }


        public void Show(RectTransform target)
        {
            base.Show();
            Vector3 pos = target.TransformPoint(target.rect.center);
            transform.position = pos;
        }

        private Vector2 ScreenSize { get { return Root.RectSizeDelta; } }

        private void Calculate()
        {
            //Root.Width
        }

        //на случай если мы решим вернуть обратно блокировку остального интерфейса тут можно выставить блокирующие "шторки"
        /*
        private void AddCommand(Button target)
        {
            target.onClick.AddListener(()=> { Command(target); });
        }

        private void Command(Button target)
        {
            Hide();
            target.onClick.RemoveListener(() => { Command(target); });
        }

        private void SetAround(RectTransform target, RectTransform left, RectTransform right, RectTransform top, RectTransform bottom)
        {
            float width = ScreenSize.x - (rect.sizeDelta.x / 2f);
            float height = ScreenSize.y - (rect.sizeDelta.y / 2f);

            left.sizeDelta = new Vector2(width, rect.sizeDelta.y);
            right.sizeDelta = new Vector2(width, rect.sizeDelta.y);
            top.sizeDelta = new Vector2((width * 2) + (rect.sizeDelta.x), height);
            bottom.sizeDelta = new Vector2((width * 2) + (rect.sizeDelta.x), height);

            left.anchoredPosition = new Vector2(rect.sizeDelta.x / -2f, 0f);
            right.anchoredPosition = new Vector2(rect.sizeDelta.x / 2f, 0f);
            top.anchoredPosition = new Vector2(0f, rect.sizeDelta.y / 2f);
            bottom.anchoredPosition = new Vector2(0f, rect.sizeDelta.y / -2f);
        }
        */
    }
}